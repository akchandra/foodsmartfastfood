﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FoodSmart.Common
{
    public interface IOrder : IBase<Int32>, ICommon
    {
        string OrderRef { get; set; }
        string OrderType { get; set; }
        string OrdTypeDesc { get; set; }
        DateTime OrderDate { get; set; }
        decimal? Totalamt { get; set; }
        int LocID { get; set; }
        int FinYearID { get; set; }
        string Narration { get; set; }
        int UserID { get; set; }
        string Loc { get; set; }
        bool Status { get; set; }
        List<IOrderItem> Items { get; set; }
    }
}
