﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FoodSmart.Common
{
    public interface IInvoice : ICommon
    {
        string mode { get; set; }
        int InvoiceID { get; set; }
        DateTime BillDate { get; set; }
        int CustID { get; set; }
        int UserID { get; set; }
        int LocID { get; set; }
        int YearID { get; set; }
        int RestID { get; set; }
        int SerialNo { get; set; }
        string CardNoInside { get; set; }
        string PaymentMode { get; set; }
        string BillType { get; set; }
        decimal BillAmount { get; set; }
        decimal VATAmount { get; set; }
        decimal ServiceTaxAmount { get; set; }
        decimal SBC { get; set; }
        string MachineName { get; set; }
        decimal SecDeposit { get; set; }
        decimal DamageAmt { get; set; }
        decimal RoundOff { get; set; }
        int RefundReasonID { get; set; }
        decimal DiscountPer { get; set; }
        int DiscReasonID { get; set; }
        string MobileNo { get; set; }
        string CustName { get; set; }
        decimal CashAmount { get; set; }
        decimal CCAmount { get; set; }
        decimal MPAmount { get; set; }
        decimal eWalletAmount { get; set; }
        int CouponNo { get; set; }
        int CouponID { get; set; }
        string Prefix { get; set; }
        decimal CouponAmt { get; set; }
    }
}
