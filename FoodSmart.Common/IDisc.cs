﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FoodSmart.Common
{
    public interface IDisc : ICommon
    {
        int DiscAppID { get; set; }
        int DiscCalcID { get; set; }
        int DiscTypeID { get; set; }
        string DiscName { get; set; }
        DateTime? StartDate { get; set; }
        DateTime? EndDate { get; set; }
        decimal DiscPer { get; set; }
        int BaseItemID { get; set; }
        int DiscItemID { get; set; }
        decimal DiscAmount { get; set; }
        decimal ThresholeAmt { get; set; }
        int ThresholeQty { get; set; }
        bool DiscStatus { get; set; }
    }
}
