﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FoodSmart.Common
{
    public interface IRecipeItem : IBase<Int64>
    {
        Int64 RecipeId { get; set; }
        string Type { get; set; }
        string TypeDescription { get; set; }
        int? ItemId { get; set; }
        string Item { get; set; }
        decimal? Quantity { get; set; }
        string UOM { get; set; }
        decimal? Rate { get; set; }
    }
}
