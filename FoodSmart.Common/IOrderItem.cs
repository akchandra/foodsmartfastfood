﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FoodSmart.Common
{
    public interface IOrderItem : IBase<Int64>
    {
        Int64 OrderId { get; set; }
        int ItemId { get; set; }
        string ItemType { get; set; }
        string ItemTypeDesc { get; set; }
        string ItemName { get; set; }
        decimal? QtyWe { get; set; }
        decimal? QtyWd { get; set; }
        decimal? Rate { get; set; }
        string UOM { get; set; }
        
    }
}
