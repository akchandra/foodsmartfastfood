﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FoodSmart.Common
{
    public interface IRestaurant : ICommon
    {

        //int CardID { get; set; }
        string RestType { get; set; }
        string RestName { get; set; }
        string LocName { get; set; }
        string RestPass { get; set; }
        bool PrintBill { get; set; }
        int RestID { get; set; }
        decimal Ratio { get; set; }
        int LocID { get; set; }
        bool RestStatus { get; set; }
        string CalculationMethod { get; set; }
        string VATNo { get; set; }
        int UserID { get; set; }
        string ServTaxNo { get; set; }
        decimal ServTaxPer { get; set; }
        decimal SBCPer { get; set; }
        string Mode { get; set; }
        DateTime CurrDate { get; set; }
        string PrinterName { get; set; }
        string printerType { get; set; }
        string KOTPrinterType { get; set; }
        string KOTPrinterName { get; set; }
    }
}
