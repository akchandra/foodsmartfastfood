﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FoodSmart.Common
{
    public interface IItemGroup : IBase<Int32>
    {
        string RorF { get; set; }
        bool Status { get; set; }
    }
}
