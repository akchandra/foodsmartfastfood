﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FoodSmart.Common
{
    public interface ISearchCriteria
    {
        string SortExpression { get; set; }
        string SortDirection { get; set; }
        string StringOption1 { get; set; }
        string StringOption2 { get; set; }
        string StringOption3 { get; set; }
        List<string> StringParams { get; set; }
        int PageIndex
        {
            get;
            set;
        }

        int PageSize
        {
            get;
            set;
        }
    }
}
