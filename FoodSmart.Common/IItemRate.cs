﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FoodSmart.Common
{
    public interface IItemRate : IBase<Int64>
    {
        Int64 ItemId { get; set; }
        string ItemName { get; set; }
        int RateTypeId { get; set; }
        DateTime EffectiveDate { get; set; }
        decimal SellingRate { get; set; }
        decimal NewRate { get; set; }
        Int64? ComboItemId { get; set; }
        decimal? OtherCharges { get; set; }
        decimal? TransferPer { get; set; }
        decimal? TransferOthers { get; set; }
        int ItemGroupId { get; set; }
        string ItemGroupName { get; set; }
        string UOM { get; set; }
        decimal? GST { get; set; }
    }
}
