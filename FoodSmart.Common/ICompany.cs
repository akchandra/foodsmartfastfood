﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FoodSmart.Common
{
    public interface ICompany : ICommon
    {
        int CompID { get; set; }
        string CompName { get; set; }
        string CompAddr1 { get; set; }
        string CompAddr2 { get; set; }
        decimal ServiceTaxPer { get; set; }
        string CalculationMethod { get; set; }
        string btmLine1 { get; set; }
        string btmLine2 { get; set; }
        string btmLine3 { get; set; }
        string btmLine4 { get; set; }
    }
}
