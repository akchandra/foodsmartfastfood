﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FoodSmart.Common
{
    public interface IUser : ICommon
    {
        string Password { get; set; }
        string NewPassword { get; set; }
        string LoginID { get; set; }
        string UserFullName { get; set; }
        int LocID { get; set; }
        int Id {get; set;}
        int RoleID { get; set; }
        string RoleName { get; set; }
        string LocName { get; set; }
        string EmailId { get; set; }
        bool IsActive { get; set; }
        int RestID { get; set; }
        //int CardID { get; set; }
        string CardNoOutside { get; set; }
        string Mode { get; set; }
        bool Authorizer { get; set; }
        DateTime CurrDate { get; set; }
        bool UserLocked { get; set; }
        string RestName { get; set; }
        string CalcMethod { get; set; }
        decimal SecDeposit { get; set; }
        bool LocationExists { get; set; }
     }
}
