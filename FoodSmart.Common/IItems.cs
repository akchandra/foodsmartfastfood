﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FoodSmart.Common
{
    public interface IItems : ICommon
    {
        int ItemGroupID { get; set; }
        int RestID { get; set; }
        int ItemID { get; set; }
        int ItemRateID { get; set; }
        int LocID { get; set; }
        int VehicleTypeID { get; set; }
        int FirstSlabHrs { get; set; }
        int DayTypeID { get; set; }

        string LocName { get; set; }
        string RestName { get; set; }
        bool GroupStatus { get; set; }
        bool itemStatus { get; set; }
        string GroupDescription { get; set; }
        int RateTypeID { get; set; }
        string RorF { get; set; }

        string ItemDescription { get; set; }
        DateTime EffectiveDate { get; set; }
        bool mrpItems { get; set; }
        bool DiscountAllowed { get; set; }
        string UOM { get; set; }
        decimal CGSTPer { get; set; }
        decimal SGSTPer { get; set; }
        string HSNCode { get; set; }
        int itemType { get; set; }
        decimal SellingRate { get; set; }
        decimal iSellingRate { get; set; }
        int ItemStockID { get; set; }
        decimal Opqty { get; set; }
        int finYrID { get; set; }

        string ProcessedAt { get; set; }
        decimal MinStockLevel { get; set; }
        decimal MaxStockLevel { get; set; }
        decimal ReOrdStockLevel { get; set; }
        string GroupType { get; set; }
        string GroupName { get; set; }
        int RMGroupID { get; set; }
    }
}
