﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FoodSmart.Common
{
    public interface IeWallet : ICommon
    {
        int eWalletID { get; set; }
        string eWalletGateway { get; set; }
        string GatewayType { get; set; }
        string BankName { get; set; }
        string RegdMobileNo { get; set; }
        string RegdNo { get; set; }
        int LocID { get; set; }
        int UserID { get; set; }
        string LocName { get; set; }
    }
}
