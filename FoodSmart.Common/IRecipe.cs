﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FoodSmart.Common
{
    public interface IRecipe : IBase<int>
    {
        string Type { get; set; }
        decimal? Quantity { get; set; }
        string UOM { get; set; }
        string ProcessedAt { get; set; }
        decimal? Rate { get; set; }
        string RatePerUnit { get; set; }
        int? ItemId { get; set; }
        decimal? NoofPlates { get; set; }
        decimal TotalQuantity { get; set; }
        decimal TotalWeQty { get; set; }
        DateTime? DateIntroduced { get; set; }
        string RecipeDetail { get; set; }
        bool Status { get; set; }
        decimal? Amount { get; set; }
        List<IRecipeItem> RecipeItems { get; set; }
    }
}
