﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FoodSmart.Common
{
    public interface IVendor : ICommon
    {
        Int32 VendorID { get; set; }
        string VendorName { get; set; }
        string VendorAddress { get; set; }
        string ContactMob { get; set; }
        string ContactPerson { get; set; }
        string LandPhone { get; set; }
        string GSTNo { get; set; }
        int StateID { get; set; }
        string PANNo { get; set; }
        string emailID { get; set; }
    }
}
