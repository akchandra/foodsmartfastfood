﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FoodSmart.Common
{
    public interface IStore : ICommon
    {
        int StoreID { get; set; }
        string StoreName { get; set; }
        int RestID { get; set; }
    }
}
