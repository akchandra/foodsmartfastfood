﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FoodSmart.Common
{
    public interface ILocation : ICommon
    {
        int LocID { get; set; }
        string LocName { get; set; }
        string LocAddress1 { get; set; }
        string LocAddress2 { get; set; }
        string LocType { get; set; }
        string LocTypeDesc { get; set; }
        string OwnOrFran { get; set; }
        string OwnOrFranDesc { get; set; }
        int BillNoLength { get; set; }
        //decimal ServiceTaxPer { get; set; }
        //string CalculationMethod { get; set; }
        string VATNo { get; set; }
        int UserID { get; set; }
        string ServTaxNo { get; set; }
        //decimal ServTaxPer { get; set; }
        //decimal SBCPer { get; set; }
        //DateTime CurrDate { get; set; }
        string PrinterName { get; set; }
        string printerType { get; set; }
        bool PrintBill { get; set; }
        string btmLine1 { get; set; }
        string btmLine2 { get; set; }
        string btmLine3 { get; set; }
        string btmLine4 { get; set; }
        string btmLine5 { get; set; }
        string MachineName { get; set; }
        string GSTNo { get; set; }
        DateTime dtOpStock { get; set; }
        bool Composit { get; set; }
    }
}
