﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FoodSmart.Common
{
    public interface IInventory : ICommon
    {
        int RMID { get; set; }
        string RMName { get; set; }
        string RMUOM { get; set; }
        decimal RMRate { get; set; }
        string TranType { get; set; }
        decimal BalanceQty { get; set; }
        decimal Opbal { get; set; }
        decimal TranQty { get; set; }
        DateTime TranDate { get; set; }
        string RMCode { get; set; }
        decimal StockValue { get; set; }

        int GroupID { get; set; }
        string RMGroupName { get; set; }
        string GroupInitial { get; set; }

        int CCID { get; set; }
        string CostCenterName { get; set; }

        int TranID { get; set; }
        string TranNo { get; set; }
        string Narration { get; set; }
        int VendorID { get; set; }
        DateTime? RefDate { get; set; }
        string RefNo { get; set; }
        int CurrYearID { get; set; }
        
        string RecipeUnit { get; set; }
        int RecipeID { get; set; }
        string RecipeName { get; set; }
        decimal NoofPlates { get; set; }
        string RecipeDetail { get; set; }
        int fk_FGItemID { get; set; }

    }
}
