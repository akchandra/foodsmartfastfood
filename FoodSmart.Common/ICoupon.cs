﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FoodSmart.Common
{
    public interface ICoupon : ICommon
    {
        int CPID { get; set; }
        int CouponStartSerial { get; set; }
        int CouponEndSerial { get; set; }

        decimal CouponAmount { get; set; }

        string CouponPrefix { get; set; }
        string CouponName { get; set; }

        DateTime ActivateDate { get; set; }
        DateTime ExpiryDate { get; set; }
        Boolean CouponStatus { get; set; }

    }
}
