﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FoodSmart.Common
{
    public interface ICashierImprest : ICommon
    {
        int TranID { get; set; }
        string CashTranNo { get; set; }
        DateTime CashTranDate { get; set; }
        int LocID { get; set; }
        int CashierID { get; set; }
        string TranType { get; set; }
        decimal TranAmount { get; set; }
        string Narration { get; set; }
        bool TranStatus { get; set; }
        string Mode { get; set; }
        string LocName { get; set; }
    }
}
