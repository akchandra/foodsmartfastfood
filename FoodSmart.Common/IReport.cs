﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FoodSmart.Common
{
    public interface IReport
    {
        string ItemName { get; set; }
        string LocationName { get; set; }
        string RestaurantName { get; set; }
        string BillNo { get; set; }
        string TranDate { get; set; }
        string Reason { get; set; }
        string CardNoOutside { get; set; }
        int CurrentSerial { get; set; }
        string ItemType { get; set; }
        string CashierName { get; set; }

        decimal SaleRate { get; set; }
        decimal VATAmt { get; set; }
        decimal VATPer { get; set; }
        decimal StaxAmt { get; set; }
        decimal StaxPer { get; set; }
        decimal SBTPer { get; set; }
        decimal SBTAmt { get; set; }
        decimal TotalAmt { get; set; }
        decimal ActCash { get; set; }
        decimal RefillCash { get; set; }
        decimal RefundCash { get; set; }
        decimal ParkingAmt { get; set; }
        decimal SecDeposit { get; set; }
        decimal CardBalance { get; set; }
        decimal FoodAmount { get; set; }
        decimal BeverageAmt { get; set; }
        decimal OtherAmount { get; set; }
        decimal Roff { get; set; }
    }
}
