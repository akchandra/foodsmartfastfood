﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FoodSmart.Common
{
    public interface ITransaction : IBase<Int64>, ICommon
    {
        int VendorId { get; set; }
        string TranNo { get; set; }
        string TranType { get; set; }
        string TranTypeDesc { get; set; }
        DateTime TranDate { get; set; }
        string RefDoc { get; set; }
        DateTime? RefDate { get; set; }
        string Narration { get; set; }
        decimal? TranQty { get; set; }
        string VendorName { get; set; }
        int FromLocID { get; set; }
        int ToLocID { get; set; }
        int FinYearID { get; set; }
        int UserID { get; set; }
        string ToLoc { get; set; }
        string FromLoc { get; set; }
        bool Status { get; set; }
        decimal TranVal { get; set; }
        List<ITransactionItem> Items { get; set; }
    }
}
