﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FoodSmart.Common
{
    public interface ITransactionItem : IBase<Int64>
    {
        Int64 TransactionId { get; set; }
        Int64 Id { get; set; }
        string TransactionType { get; set; }
        int ItemId { get; set; }
        string ItemType { get; set; }
        string ItemTypeDesc { get; set; }
        string ItemName { get; set; }
        decimal? Quantity { get; set; }
        decimal? Rate { get; set; }
        decimal? Tax { get; set; }
        decimal? GSTPer { get; set; }
        string UOM { get; set; }
        decimal? Total { get; set; }
    }
}
