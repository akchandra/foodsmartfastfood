﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FoodSmart.Common
{
    public interface IInvTran : ICommon
    {
        int TranID { get; set; }
        int VendorId { get; set; }
        string TranNo { get; set; }
        string TranType { get; set; }
        DateTime TranDate { get; set; }
        string RefDoc { get; set; }
        DateTime RefDate { get; set; }
        string Narration { get; set; }
        decimal TranQty { get; set; }
        string VendorName { get; set; }
        int FromLocID { get; set; }
        int ToLocID { get; set; }
        int FinYearID { get; set; }
        int UserID { get; set; }
    }
}
