﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FoodSmart.Common;
using System.Data;
using System.Data.SqlClient;

namespace FoodSmart.Entity
{
    public class InvTranEntity : IInvTran
    {
        public int TranID { get; set; }
        public int VendorId { get; set; }
        public string TranNo { get; set; }
        public string TranType { get; set; }
        public DateTime TranDate { get; set; }
        public string RefDoc { get; set; }
        public DateTime RefDate { get; set; }
        public string Narration { get; set; }
        public decimal TranQty { get; set; }
        public string VendorName { get; set; }
        public int FromLocID { get; set; }
        public int ToLocID { get; set; }
        public int FinYearID { get; set; }
        public int UserID { get; set; }

        
        #region ICommon Members

        public int CreatedBy
        {
            get;
            set;
        }

        public DateTime CreatedOn
        {
            get;
            set;
        }

        public int ModifiedBy
        {
            get;
            set;
        }

        public DateTime ModifiedOn
        {
            get;
            set;
        }

        #endregion

        #region Constructors

        public InvTranEntity()
        {
            
        }

        public InvTranEntity(DataTableReader reader)
        {
            if (ColumnExists(reader, "TranID"))
                if (reader["TranID"] != DBNull.Value)
                    this.TranID = Convert.ToInt32(reader["TranID"]);

            if (ColumnExists(reader, "Trantype"))
                if (reader["Trantype"] != DBNull.Value)
                    this.TranType = Convert.ToString(reader["Trantype"]);

            if (ColumnExists(reader, "VendorName"))
                if (reader["VendorName"] != DBNull.Value)
                    this.VendorName = Convert.ToString(reader["VendorName"]);

            if (ColumnExists(reader, "Tranno"))
                if (reader["Tranno"] != DBNull.Value)
                    this.TranNo = Convert.ToString(reader["Tranno"]);

            if (ColumnExists(reader, "TranDate"))
                if (reader["TranDate"] != DBNull.Value)
                    this.TranDate = Convert.ToDateTime(reader["TranDate"]);

            if (ColumnExists(reader, "Narration"))
                if (reader["Narration"] != DBNull.Value)
                    this.Narration = Convert.ToString(reader["Narration"]);

            if (ColumnExists(reader, "VendorID"))
                if (reader["VendorID"] != DBNull.Value)
                    this.VendorId = Convert.ToInt32(reader["VendorID"]);

            if (ColumnExists(reader, "fk_FromLocID"))
                if (reader["fk_FromLocID"] != DBNull.Value)
                    this.FromLocID = Convert.ToInt32(reader["fk_FromLocID"]);

            if (ColumnExists(reader, "fk_ToLocID"))
                if (reader["fk_ToLocID"] != DBNull.Value)
                    this.ToLocID = Convert.ToInt32(reader["fk_ToLocID"]);

            if (ColumnExists(reader, "fk_FinYearId"))
                if (reader["fk_FinYearId"] != DBNull.Value)
                    this.FinYearID = Convert.ToInt32(reader["fk_FinYearId"]);

            if (ColumnExists(reader, "Refdoc"))
                if (reader["Refdoc"] != DBNull.Value)
                    this.RefDoc = Convert.ToString(reader["Refdoc"]);
        }

        #endregion

        #region Private Methods

        public bool ColumnExists(IDataReader reader, string columnName)
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                if (reader.GetName(i).ToUpper() == columnName.ToUpper())
                {
                    return true;
                }
            }

            return false;
        }

        #endregion
    }
}
