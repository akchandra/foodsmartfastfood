﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FoodSmart.Common;
using System.Data;
using System.Data.SqlClient;

namespace FoodSmart.Entity
{
    [Serializable]
    public class eWalletEntity : IeWallet
    {
        public int eWalletID { get; set; }
        public string eWalletGateway { get; set; }
        public string GatewayType { get; set; }
        public string BankName { get; set; }
        public string RegdMobileNo { get; set; }
        public string RegdNo { get; set; }
        public int LocID { get; set; }
        public int UserID { get; set; }
        public string LocName { get; set; }

        #region ICommon Members

        public int CreatedBy
        {
            get;
            set;
        }

        public DateTime CreatedOn
        {
            get;
            set;
        }

        public int ModifiedBy
        {
            get;
            set;
        }

        public DateTime ModifiedOn
        {
            get;
            set;
        }

        #endregion

        #region Constructors

        public eWalletEntity()
        {
            
        }

        public eWalletEntity(DataTableReader reader)
        {
            this.eWalletID = Convert.ToInt32(reader["pk_eWalletID"]);
            if (ColumnExists(reader, "eWalletGateway"))
                if (reader["eWalletGateway"] != DBNull.Value)
                    this.eWalletGateway = Convert.ToString(reader["eWalletGateway"]);

            if (ColumnExists(reader, "RegdMobileNo"))
                if (reader["RegdMobileNo"] != DBNull.Value)
                    this.RegdMobileNo = Convert.ToString(reader["RegdMobileNo"]);

            if (ColumnExists(reader, "RegdNo"))
                if (reader["RegdNo"] != DBNull.Value)
                    this.RegdNo = Convert.ToString(reader["RegdNo"]);

            if (ColumnExists(reader, "BankName"))
                if (reader["BankName"] != DBNull.Value)
                    this.BankName = Convert.ToString(reader["BankName"]);

            if (ColumnExists(reader, "GatewayType"))
                if (reader["GatewayType"] != DBNull.Value)
                    this.GatewayType = Convert.ToString(reader["GatewayType"]);

            if (ColumnExists(reader, "fk_LocID"))
                if (reader["fk_LocID"] != DBNull.Value)
                    this.LocID = Convert.ToInt32(reader["fk_LocID"]);

            if (ColumnExists(reader, "UserID"))
                if (reader["UserID"] != DBNull.Value)
                    this.UserID = Convert.ToInt32(reader["UserID"]);

            if (ColumnExists(reader, "LocationName"))
                if (reader["LocationName"] != DBNull.Value)
                    this.LocName = Convert.ToString(reader["LocationName"]);
        }

        #endregion

        #region Private Methods

        public bool ColumnExists(IDataReader reader, string columnName)
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                if (reader.GetName(i).ToUpper() == columnName.ToUpper())
                {
                    return true;
                }
            }

            return false;
        }

        #endregion
    }
}
