﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FoodSmart.Common;
using System.Data;
using System.Data.SqlClient;

namespace FoodSmart.Entity
{
    [Serializable]
    public class CompanyEntity : ICompany
    {
        public int CompID { get; set; }
        public string CompName { get; set; }
        public string CompAddr1 { get; set; }
        public string CompAddr2 { get; set; }
        public decimal ServiceTaxPer { get; set; }
        public string CalculationMethod { get; set; }
        public string btmLine1 { get; set; }
        public string btmLine2 { get; set; }
        public string btmLine3 { get; set; }
        public string btmLine4 { get; set; }
        public string btmLine5 { get; set; }

        #region ICommon Members

        public int CreatedBy
        {
            get;
            set;
        }

        public DateTime CreatedOn
        {
            get;
            set;
        }

        public int ModifiedBy
        {
            get;
            set;
        }

        public DateTime ModifiedOn
        {
            get;
            set;
        }

        #endregion

        #region Constructors

        public CompanyEntity()
        {
            
        }

        public CompanyEntity(DataTableReader reader)
        {
            //this.CompID = Convert.ToInt32(reader["pk_LocID"]);

            if (ColumnExists(reader, "Company_Name"))
                if (reader["Company_Name"] != DBNull.Value)
                    this.CompName = Convert.ToString(reader["Company_Name"]);

            if (ColumnExists(reader, "Address1"))
                if (reader["Address1"] != DBNull.Value)
                    this.CompAddr1 = Convert.ToString(reader["Address1"]);

            if (ColumnExists(reader, "Address2"))
                if (reader["Address2"] != DBNull.Value)
                    this.CompAddr2 = Convert.ToString(reader["Address2"]);

            if (ColumnExists(reader, "ServiceTaxPer"))
                if (reader["ServiceTaxPer"] != DBNull.Value)
                    this.ServiceTaxPer = Convert.ToDecimal(reader["ServiceTaxPer"]);

            if (ColumnExists(reader, "CalculationMethod"))
                if (reader["CalculationMethod"] != DBNull.Value)
                    this.CalculationMethod = Convert.ToString(reader["CalculationMethod"]);

            if (ColumnExists(reader, "ServiceTaxPer"))
                if (reader["ServiceTaxPer"] != DBNull.Value)
                    this.ServiceTaxPer = Convert.ToDecimal(reader["ServiceTaxPer"]);

            if (ColumnExists(reader, "CalculationMethod"))
                if (reader["CalculationMethod"] != DBNull.Value)
                    this.CalculationMethod = Convert.ToString(reader["CalculationMethod"]);

            if (ColumnExists(reader, "btmLine1"))
                if (reader["btmLine1"] != DBNull.Value)
                    this.btmLine1 = Convert.ToString(reader["btmLine1"]);

            if (ColumnExists(reader, "btmLine2"))
                if (reader["btmLine2"] != DBNull.Value)
                    this.btmLine2 = Convert.ToString(reader["btmLine2"]);

            if (ColumnExists(reader, "btmLine3"))
                if (reader["btmLine3"] != DBNull.Value)
                    this.btmLine3 = Convert.ToString(reader["btmLine3"]);

            if (ColumnExists(reader, "btmLine4"))
                if (reader["btmLine4"] != DBNull.Value)
                    this.btmLine4 = Convert.ToString(reader["btmLine4"]);

            if (ColumnExists(reader, "btmLine5"))
                if (reader["btmLine5"] != DBNull.Value)
                    this.btmLine5 = Convert.ToString(reader["btmLine5"]);

        }

        #endregion

        #region Private Methods

        public bool ColumnExists(IDataReader reader, string columnName)
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                if (reader.GetName(i).ToUpper() == columnName.ToUpper())
                {
                    return true;
                }
            }

            return false;
        }

        #endregion
    }
}
