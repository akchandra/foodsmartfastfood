﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FoodSmart.Utilities;
using System.Data;
using System.Data.SqlClient;

namespace FoodSmart.Entity
{
    [Serializable]
    public class ReportCriteria 
    {
        #region Public Properties

        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int LocId { get; set; }
        public string TransactionType { get; set; }
        public int CounterId { get; set; }
        public string LocName { get; set; }
        public string LocAddress { get; set; }
        public Int32 ItemGroupID { get; set; }
        public string RecipeName { get; set; }
        public int VendorID { get; set; }
        public string CalledFrom { get; set; }
        public int restID { get; set; }
        public int RMID { get; set; }
        public int RMGroupID { get; set; }
        #endregion

        #region Constructor

        #endregion

        #region Public Methods

        public void Clear()
        {

        }

        #endregion
    }
}
