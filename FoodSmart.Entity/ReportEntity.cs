﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FoodSmart.Common;
using System.Data;
using System.Data.SqlClient;

namespace FoodSmart.Entity
{
    [Serializable]
    public class ReportEntity : IReport
    {
        #region IReport Members

        public string ItemName { get; set; }
        public string LocationName { get; set; }
        public string RestaurantName { get; set; }
        public string BillNo { get; set; }
        public string TranDate { get; set; }
        public string Reason { get; set; }
        public string CardNoOutside { get; set; }
        public int CurrentSerial { get; set; }
        public string ItemType { get; set; }
        public string CashierName { get; set; }

        public decimal SaleRate { get; set; }
        public decimal VATAmt { get; set; }
        public decimal VATPer { get; set; }
        public decimal StaxAmt { get; set; }
        public decimal StaxPer { get; set; }
        public decimal SBTPer { get; set; }
        public decimal SBTAmt { get; set; }
        public decimal TotalAmt { get; set; }
        public decimal ActCash { get; set; }
        public decimal RefillCash { get; set; }
        public decimal RefundCash { get; set; }
        public decimal ParkingAmt { get; set; }
        public decimal SecDeposit { get; set; }
        public decimal CardBalance { get; set; }
        public decimal FoodAmount { get; set; }
        public decimal BeverageAmt { get; set; }
        public decimal OtherAmount { get; set; }
        public string UserName { get; set; }
        public decimal Roff { get; set; }

        #endregion

        #region Constructors

        public ReportEntity()
        {

        }

        public ReportEntity(DataTableReader reader)
        {

        }

        #endregion

        #region Public Methods

        public void CargoReport(DataTableReader reader)
        {

            if (ColumnExists(reader, "ItemName"))
                if (reader["ItemName"] != DBNull.Value)
                    this.ItemName = Convert.ToString(reader["ItemName"]);

            if (ColumnExists(reader, "CardNoOutside"))
                if (reader["CardNoOutside"] != DBNull.Value)
                    this.CardNoOutside = Convert.ToString(reader["CardNoOutside"]);

            if (ColumnExists(reader, "ItemType"))
                if (reader["ItemType"] != DBNull.Value)
                    this.ItemType = Convert.ToString(reader["ItemType"]);

            if (ColumnExists(reader, "RestaurantName"))
                if (reader["RestaurantName"] != DBNull.Value)
                    this.RestaurantName = Convert.ToString(reader["RestaurantName"]);

            if (ColumnExists(reader, "BillNo"))
                if (reader["BillNo"] != DBNull.Value)
                    this.BillNo = Convert.ToString(reader["BillNo"]);

            if (ColumnExists(reader, "CurrentSerial"))
                if (reader["CurrentSerial"] != DBNull.Value)
                    this.CurrentSerial = Convert.ToInt32(reader["CurrentSerial"]);

            if (ColumnExists(reader, "Reason"))
                if (reader["Reason"] != DBNull.Value)
                    this.Reason = Convert.ToString(reader["Reason"]);

            if (reader["CardBalance"] != DBNull.Value)
                this.CardBalance = Convert.ToDecimal(reader["CardBalance"]);

            if (ColumnExists(reader, "SecurityDeposit"))
                if (reader["SecurityDeposit"] != DBNull.Value)
                    this.SecDeposit = Convert.ToInt32(reader["SecurityDeposit"]);

            if (ColumnExists(reader, "CashierName"))
                if (reader["CashierName"] != DBNull.Value)
                    this.CashierName = Convert.ToString(reader["CashierName"]);

            if (ColumnExists(reader, "SaleRate"))
                if (reader["SaleRate"] != DBNull.Value)
                    this.SaleRate = Convert.ToDecimal(reader["SaleRate"]);

            if (ColumnExists(reader, "VATAmt"))
                if (reader["VATAmt"] != DBNull.Value)
                    this.VATAmt = Convert.ToDecimal(reader["VATAmt"]);

            if (ColumnExists(reader, "VATPer"))
                if (reader["VATPer"] != DBNull.Value)
                    this.VATPer = Convert.ToDecimal(reader["VATPer"]);

            if (ColumnExists(reader, "StaxAmt"))
                if (reader["StaxAmt"] != DBNull.Value)
                    this.StaxAmt = Convert.ToDecimal(reader["StaxAmt"]);

            if (ColumnExists(reader, "StaxPer"))
                if (reader["StaxPer"] != DBNull.Value)
                    this.StaxPer = Convert.ToDecimal(reader["StaxPer"]);

            if (ColumnExists(reader, "SBTAmt"))
                if (reader["SBTAmt"] != DBNull.Value)
                    this.SBTAmt = Convert.ToDecimal(reader["SBTAmt"]);

            if (ColumnExists(reader, "TotalAmt"))
                if (reader["TotalAmt"] != DBNull.Value)
                    this.TotalAmt = Convert.ToDecimal(reader["TotalAmt"]);

            if (ColumnExists(reader, "ActCash"))
                if (reader["ActCash"] != DBNull.Value)
                    this.ActCash = Convert.ToDecimal(reader["ActCash"]);

            if (ColumnExists(reader, "RefillCash"))
                if (reader["RefillCash"] != DBNull.Value)
                    this.RefillCash = Convert.ToDecimal(reader["RefillCash"]);

            if (ColumnExists(reader, "RefundCash"))
                if (reader["RefundCash"] != DBNull.Value)
                    this.RefundCash = Convert.ToDecimal(reader["RefundCash"]);

            if (ColumnExists(reader, "ParkingAmt"))
                if (reader["ParkingAmt"] != DBNull.Value)
                    this.ParkingAmt = Convert.ToDecimal(reader["ParkingAmt"]);

            if (ColumnExists(reader, "CardBalance"))
                if (reader["CardBalance"] != DBNull.Value)
                    this.CardBalance = Convert.ToDecimal(reader["CardBalance"]);

            if (ColumnExists(reader, "SecDeposit"))
                if (reader["SecDeposit"] != DBNull.Value)
                    this.SecDeposit = Convert.ToDecimal(reader["SecDeposit"]);

            if (ColumnExists(reader, "FoodAmount"))
                if (reader["FoodAmount"] != DBNull.Value)
                    this.FoodAmount = Convert.ToDecimal(reader["FoodAmount"]);

            if (ColumnExists(reader, "BeverageAmt"))
                if (reader["BeverageAmt"] != DBNull.Value)
                    this.BeverageAmt = Convert.ToDecimal(reader["BeverageAmt"]);

            if (ColumnExists(reader, "OtherAmount"))
                if (reader["OtherAmount"] != DBNull.Value)
                    this.OtherAmount = Convert.ToDecimal(reader["OtherAmount"]);

            if (ColumnExists(reader, "UserName"))
                if (reader["UserName"] != DBNull.Value)
                    this.UserName = Convert.ToString(reader["UserName"]);

            if (ColumnExists(reader, "RoundOff"))
                if (reader["RoundOff"] != DBNull.Value)
                    this.Roff = Convert.ToDecimal(reader["RoundOff"]);
        }

        #endregion

        #region Private Methods

        public bool ColumnExists(IDataReader reader, string columnName)
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                if (reader.GetName(i).ToUpper() == columnName.ToUpper())
                {
                    return true;
                }
            }

            return false;
        }

        #endregion
    }

    #region "POS"
    [Serializable]
    public class BillRefundRegEntity
    {
        public string BillNo { get; set; }
        public DateTime BillDate { get; set; }
        public string Location { get; set; }
        public decimal BillAmount { get; set; }
        public decimal Discount { get; set; }
        public decimal BasicAmt { get; set; }
        public decimal DiscountAmt { get; set; }
        public decimal CGSTAmt { get; set; }
        public decimal SGSTAmt { get; set; }
        public decimal TotAmt { get; set; }
        public string BillType { get; set; }
        public decimal Roff { get; set; }

        public BillRefundRegEntity()
        {

        }

        public BillRefundRegEntity(DataTableReader reader)
        {
            this.BillNo = Convert.ToString(reader["BillNo"]);
            this.BillType = Convert.ToString(reader["BillType"]);
            this.Location = Convert.ToString(reader["Location"]);
            this.BillDate = Convert.ToDateTime(reader["BillDate"]);
            this.BasicAmt = Convert.ToDecimal(reader["BasicAmt"]);
            this.DiscountAmt = Convert.ToDecimal(reader["DiscAmount"]);
            this.CGSTAmt = Convert.ToDecimal(reader["CGSTAmt"]);
            this.SGSTAmt = Convert.ToDecimal(reader["SGSTAmt"]);
            this.BillAmount = Convert.ToDecimal(reader["BillAmount"]);
            this.Roff = Convert.ToDecimal(reader["RoundOff"]);
        }
    }

    [Serializable]
    public class CounterSummaryEntity
    {
        public string Location { get; set; }
        public decimal BillAmount { get; set; }
        public decimal Discount { get; set; }
        public decimal BasicAmt { get; set; }
        public decimal GSTAmt { get; set; }
        public decimal CGSTAmt { get; set; }
        public decimal SGSTAmt { get; set; }
        public decimal TaxableAmt { get; set; }
        public decimal Roff { get; set; }

        public CounterSummaryEntity()
        {

        }

        public CounterSummaryEntity(DataTableReader reader)
        {
            this.Location = Convert.ToString(reader["LocationName"]);
            this.BasicAmt = Convert.ToDecimal(reader["BasicAmt"]);
            this.Discount = Convert.ToDecimal(reader["DiscAmt"]);
            this.CGSTAmt = Convert.ToDecimal(reader["CGSTAmt"]);
            this.SGSTAmt = Convert.ToDecimal(reader["SGSTAmt"]);
            this.GSTAmt = Convert.ToDecimal(reader["GSTAmt"]);
            this.BillAmount = Convert.ToDecimal(reader["BillAmt"]);
            this.TaxableAmt = Convert.ToDecimal(reader["TaxableAmt"]);
            this.Roff = Convert.ToDecimal(reader["Roff"]);
        }
    }

    [Serializable]
    public class GroupWiseSaleEntity
    {
        public string Location { get; set; }
        public decimal BillAmount { get; set; }
        public decimal Discount { get; set; }
        public decimal BasicAmt { get; set; }
        public decimal GSTAmt { get; set; }
        public decimal CGSTAmt { get; set; }
        public decimal SGSTAmt { get; set; }
        public decimal TaxableAmt { get; set; }
        public string ItemName { get; set; }
        public decimal Qty { get; set; }

        public GroupWiseSaleEntity()
        {

        }

        public GroupWiseSaleEntity(DataTableReader reader)
        {
            this.Location = Convert.ToString(reader["LocationName"]);
            this.BasicAmt = Convert.ToDecimal(reader["BasicAmt"]);
            this.Discount = Convert.ToDecimal(reader["DiscAmt"]);
            this.CGSTAmt = Convert.ToDecimal(reader["CGSTAmt"]);
            this.SGSTAmt = Convert.ToDecimal(reader["SGSTAmt"]);
            this.GSTAmt = Convert.ToDecimal(reader["GSTAmt"]);
            this.BillAmount = Convert.ToDecimal(reader["BillAmt"]);
            this.TaxableAmt = Convert.ToDecimal(reader["TaxableAmt"]);
            this.ItemName = Convert.ToString(reader["ItemDescr"]);
            this.Qty = Convert.ToDecimal(reader["Tqty"]);
        }
    }

    [Serializable]
    public class BillWiseSaleEntity
    {
        public string BillNo { get; set; }
        public DateTime BillDate { get; set; }
        public string Location { get; set; }
        public decimal BillAmount { get; set; }
        public decimal Discount { get; set; }
        public decimal BasicAmt { get; set; }
        public decimal GSTAmt { get; set; }
        public decimal CGSTAmt { get; set; }
        public decimal SGSTAmt { get; set; }
        public decimal TaxableAmt { get; set; }
        public string ItemName { get; set; }
        public decimal Qty { get; set; }
        public string BillType { get; set; }

        public BillWiseSaleEntity()
        {

        }

        public BillWiseSaleEntity(DataTableReader reader)
        {
            this.BillNo = Convert.ToString(reader["BillNo"]);
            this.BillDate = Convert.ToDateTime(reader["BillDate"]);
            this.BillType = Convert.ToString(reader["BillType"]);
            this.Location = Convert.ToString(reader["LocationName"]);
            this.BasicAmt = Convert.ToDecimal(reader["BasicAmt"]);
            this.Discount = Convert.ToDecimal(reader["DiscAmt"]);
            this.CGSTAmt = Convert.ToDecimal(reader["CGSTAmt"]);
            this.SGSTAmt = Convert.ToDecimal(reader["SGSTAmt"]);
            this.GSTAmt = Convert.ToDecimal(reader["GSTAmt"]);
            this.BillAmount = Convert.ToDecimal(reader["BillAmt"]);
            //this.TaxableAmt = Convert.ToDecimal(reader["TaxableAmt"]);
            this.ItemName = Convert.ToString(reader["ItemDescr"]);
            this.Qty = Convert.ToDecimal(reader["Qty"]);
        }
    }

    [Serializable]
    public class CashierSummaryEntity
    {
        public string CashierName { get; set; }
        public string Location { get; set; }
        public decimal CashBillAmt { get; set; }
        public decimal CashRefAmt { get; set; }
        public decimal CCBillAmt { get; set; }
        public decimal CCRefAmt { get; set; }
        public int NoOfBills { get; set; }
        public int NoOfRef { get; set; }
        public decimal MealBillAmt { get; set; }
        public decimal EWalletAmt { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal Roff { get; set; }

        public CashierSummaryEntity()
        {

        }

        public CashierSummaryEntity(DataTableReader reader)
        {
            this.CashierName = Convert.ToString(reader["CashierName"]);
            this.Location = Convert.ToString(reader["LocationName"]);
            this.CashBillAmt = Convert.ToDecimal(reader["ActCashAmount"]);
            this.CashRefAmt = Convert.ToDecimal(reader["RefundCashAmount"]);
            this.CCBillAmt = Convert.ToDecimal(reader["ActCCAmount"]);
            this.CCRefAmt = Convert.ToDecimal(reader["RefundCCAmount"]);
            this.NoOfBills = Convert.ToInt32(reader["BillNos"]);
            this.NoOfRef = Convert.ToInt32(reader["RefundNos"]);
            this.MealBillAmt = Convert.ToDecimal(reader["ActMealAmount"]);
            this.EWalletAmt = Convert.ToDecimal(reader["ActEWalletAmount"]);
            this.TotalAmount = Convert.ToDecimal(reader["TotalAmount"]);
            this.Roff = Convert.ToDecimal(reader["Roff"]);
        }
    }
    #endregion

    #region "Inventory"
    [Serializable]
    public class TransactionRegisterEntity
    {
        public DateTime TransactionDate { get; set; }
        public string TransactionType { get; set; }
        public string TransactionNo { get; set; }
        public string Item { get; set; }
        public string FromLocation { get; set; }
        public string ToLocation { get; set; }
        public decimal Quantity { get; set; }
        public decimal Rate { get; set; }
        public decimal Amount { get; set; }

        public TransactionRegisterEntity()
        {

        }

        public TransactionRegisterEntity(DataTableReader reader)
        {
            this.TransactionType = Convert.ToString(reader["TranType"]);
            this.TransactionNo = Convert.ToString(reader["DocNo"]);
            this.TransactionDate = Convert.ToDateTime(reader["DocDate"]);
            this.Item = Convert.ToString(reader["ItemDescr"]);
            this.FromLocation = Convert.ToString(reader["FromLoc"]);
            this.ToLocation = Convert.ToString(reader["ToLoc"]);
            this.Quantity = Convert.ToDecimal(reader["Qty"]);
            this.Rate = Convert.ToDecimal(reader["Rate"]);
            this.Amount = Convert.ToDecimal(reader["Amount"]);
        }
    }

    [Serializable]
    public class StockDetailEntity
    {
        public string TransactionRef { get; set; }
        public DateTime TransactionDate { get; set; }
        public string TransactionType { get; set; }
        public decimal Quantity { get; set; }
        public decimal Rate { get; set; }
        public decimal Amount { get; set; }

        public StockDetailEntity()
        {

        }

        public StockDetailEntity(DataTableReader reader)
        {
            this.TransactionRef = Convert.ToString(reader[""]);
            this.TransactionDate = Convert.ToDateTime(reader[""]);
            this.TransactionType = Convert.ToString(reader[""]);
            this.Quantity = Convert.ToDecimal(reader[""]);
            this.Rate = Convert.ToDecimal(reader[""]);
            //this.Amount = Convert.ToDecimal(reader[""]);
        }
    }

    [Serializable]
    public class StockSummaryEntity
    {
        public string ItemGroup { get; set; }
        public string Item { get; set; }
        public decimal OpeningBalance { get; set; }
        public decimal Received { get; set; }
        public decimal Issued { get; set; }
        public decimal Sold { get; set; }
        public decimal Wastage { get; set; }
        public decimal Closing { get; set; }

        public StockSummaryEntity()
        {

        }

        public StockSummaryEntity(DataTableReader reader)
        {
            this.ItemGroup = Convert.ToString(reader[""]);
            this.Item = Convert.ToString(reader[""]);
            this.OpeningBalance = Convert.ToDecimal(reader[""]);
            this.Received = Convert.ToDecimal(reader[""]);
            this.Issued = Convert.ToDecimal(reader[""]);
            this.Sold = Convert.ToDecimal(reader[""]);
            this.Wastage = Convert.ToDecimal(reader[""]);
            this.Closing = Convert.ToDecimal(reader[""]);
        }
    }

    [Serializable]
    public class ItemListEntity
    {
        public string Item { get; set; }
        public decimal Quantity { get; set; }
        public decimal Rate { get; set; }
        public decimal CGSTPer { get; set; }
        public decimal SGSTPer { get; set; }
        public decimal Stock { get; set; }
        public string UOM { get; set; }
        public string ItemGroup { get; set; }

        public ItemListEntity()
        {

        }

        public ItemListEntity(DataTableReader reader)
        {
            this.Item = Convert.ToString(reader["ItemDesc"]);
            this.ItemGroup = Convert.ToString(reader["ItemGroup"]);
            this.UOM = Convert.ToString(reader["UOM"]);
            this.Rate = Convert.ToDecimal(reader["SellingRate"]);
            this.CGSTPer = Convert.ToDecimal(reader["CGSTPer"]);
            this.SGSTPer = Convert.ToDecimal(reader["SGSTPer"]);
            if (ColumnExists(reader, "Stock"))
                if (reader["Stock"] != DBNull.Value)
                    this.Stock =  (Convert.ToDecimal(reader["Stock"]));
        }

        #region Private Methods

        public bool ColumnExists(IDataReader reader, string columnName)
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                if (reader.GetName(i).ToUpper() == columnName.ToUpper())
                {
                    return true;
                }
            }

            return false;
        }

        #endregion
    }
    #endregion

}
