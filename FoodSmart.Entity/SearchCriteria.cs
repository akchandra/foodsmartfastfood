﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FoodSmart.Common;
using FoodSmart.Utilities;

namespace FoodSmart.Entity
{
    [Serializable]
    public class SearchCriteria : ISearchCriteria
    {
        #region Public Properties

        public string StringOption1 { get; set; }
        public string StringOption2 { get; set; }
        public string StringOption3 { get; set; }

        public int IntegerOption1 { get; set; }
        public int IntegerOption2 { get; set; }
        public int IntegerOption3 { get; set; }

        public string RoleName { get; set; }
        public DateTime? Date { get; set; }
        public string Location { get; set; }
        public int LocID { get; set; }
        public string LoginID { get; set; }
        public string QueryStat { get; set; }

        public string CardName { get; set; }
        public string CardNoExternal { get; set; }
        public string CardType { get; set; }

        //public string GroupName { get; set; }
        public string ItemGroupName { get; set; }

        public string ItemName { get; set; }
        public string RestName { get; set; }
        public string RorF { get; set; }
        public string BillType { get; set; }
        public int BillID { get; set; }
        public string BillNo { get; set; }
        public string SelYear { get; set; }
	
        public List<string> StringParams { get; set; }
        public string eWalletGateway { get; set; }
        public string RegMobNo { get; set; }
        public string GatewayType { get; set; }
        public string RegdNo { get; set; }
        public string CouponPrefix { get; set; }
        public string CouponName { get; set; }
        public string Type { get; set; }
        public string RecipeName { get; set; }
        public string VendorName { get; set; }
        public int FinancialYearId { get; set; }
        public int CurrLoc { get; set; }

        public string mode { get; set; }
        public string rmItemType { get; set; }
        public string rmItem { get; set; }
        public int rmGroupID { get; set; }
        public string RMGroupName { get; set; }

        public string LocAbbr
        {
            get;
            set;
        }

        public string LocName
        {
            get;
            set;
        }

        public string GroupName
        {
            get;
            set;
        }

        public string CCName
        {
            get;
            set;
        }

        public string UserName
        {
            get;
            set;
        }

        public string FirstName
        {
            get;
            set;
        }

        public int UserId
        {
            get;
            set;
        }

        public string SortExpression
        {
            get;
            set;
        }

        public string SortDirection
        {
            get;
            set;
        }

        public PageName CurrentPage
        {
            get;
            set;
        }

        public int PageIndex
        {
            get;
            set;
        }

        public int PageSize
        {
            get;
            set;
        }

        public string OrderNo { get; set; }

        #endregion

        #region Constructor

        public SearchCriteria()
        {
            StringParams = new List<string>();
        }

        #endregion

        #region Public Methods

        public void Clear()
        {
            this.RoleName = string.Empty;
            this.FirstName = string.Empty;
            this.GroupName = string.Empty;
            this.ItemName = string.Empty;
            this.LocAbbr = string.Empty;
            this.LocName = string.Empty;
            this.SortDirection = string.Empty;
            this.SortExpression = string.Empty;
            this.UserId = 0;
            this.UserName = string.Empty;
            this.CurrentPage = 0;
            this.PageIndex = 0;
            this.PageSize = 0;
            this.LoginID = string.Empty;
            this.QueryStat = string.Empty;
            this.SelYear = string.Empty;
            this.Type = string.Empty;
            this.RecipeName = string.Empty;
            this.VendorName = string.Empty;
            this.FinancialYearId = 0;
            this.mode = string.Empty;
            this.rmItemType = string.Empty;
            this.rmItem = string.Empty;
            this.rmGroupID = 0;
            this.RMGroupName = string.Empty;
        }

        #endregion      
    }
}
