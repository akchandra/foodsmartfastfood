﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FoodSmart.Common;
using System.Data;
using System.Data.SqlClient;

namespace FoodSmart.Entity
{
    [Serializable]
    public class CashierImprestEntity : ICashierImprest
    {
        public int TranID { get; set; }
        public string CashTranNo { get; set; }
        public DateTime CashTranDate { get; set; }
        public int LocID { get; set; }
        public string LocName { get; set; }
        public int CashierID { get; set; }
        public string TranType { get; set; }
        public decimal TranAmount { get; set; }
        public string Narration { get; set; }
        public bool TranStatus { get; set; }
        public string Mode { get; set; }
        public int UserID { get; set; }

        #region ICommon Members

        public int CreatedBy
        {
            get;
            set;
        }

        public DateTime CreatedOn
        {
            get;
            set;
        }

        public int ModifiedBy
        {
            get;
            set;
        }

        public DateTime ModifiedOn
        {
            get;
            set;
        }

        #endregion

        #region Constructors

        public CashierImprestEntity()
        {
            
        }

        public CashierImprestEntity(DataTableReader reader)
        {

            if (ColumnExists(reader, "CashTranNo"))
                if (reader["CashTranNo"] != DBNull.Value)
                    this.CashTranNo = Convert.ToString(reader["CashTranNo"]);

            if (ColumnExists(reader, "LocationName"))
                if (reader["LocationName"] != DBNull.Value)
                    this.LocName = Convert.ToString(reader["LocationName"]);

            if (ColumnExists(reader, "RorP"))
                if (reader["RorP"] != DBNull.Value)
                    this.TranType = Convert.ToString(reader["RorP"]);

            if (ColumnExists(reader, "CashTranDate"))
                if (reader["CashTranDate"] != DBNull.Value)
                    this.CashTranDate = Convert.ToDateTime(reader["CashTranDate"]);

            if (ColumnExists(reader, "TranAmount"))
                if (reader["TranAmount"] != DBNull.Value)
                    this.TranAmount = Convert.ToDecimal(reader["TranAmount"]);

            if (ColumnExists(reader, "Narration"))
                if (reader["Narration"] != DBNull.Value)
                    this.Narration = Convert.ToString(reader["Narration"]);

            if (ColumnExists(reader, "fk_LocID"))
                if (reader["fk_LocID"] != DBNull.Value)
                    this.LocID = Convert.ToInt32(reader["fk_LocID"]);

            if (ColumnExists(reader, "fk_CashierID"))
                if (reader["fk_CashierID"] != DBNull.Value)
                    this.CashierID = Convert.ToInt32(reader["fk_CashierID"]);

            if (ColumnExists(reader, "TranStatus"))
                if (reader["TranStatus"] != DBNull.Value)
                    this.TranStatus = Convert.ToBoolean(reader["TranStatus"]);

            if (ColumnExists(reader, "UserID"))
                if (reader["UserID"] != DBNull.Value)
                    this.UserID = Convert.ToInt32(reader["UserID"]);

            if (ColumnExists(reader, "pk_TranID"))
                if (reader["pk_TranID"] != DBNull.Value)
                    this.TranID = Convert.ToInt32(reader["pk_TranID"]);


        }

        #endregion

        #region Private Methods

        public bool ColumnExists(IDataReader reader, string columnName)
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                if (reader.GetName(i).ToUpper() == columnName.ToUpper())
                {
                    return true;
                }
            }

            return false;
        }

        #endregion
    }
}
