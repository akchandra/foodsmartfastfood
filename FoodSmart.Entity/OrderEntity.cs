﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FoodSmart.Common;

namespace FoodSmart.Entity
{
    public class OrderEntity : IOrder
    {
        #region Constructors

        public OrderEntity()
        {
            this.Items = new List<IOrderItem>();
        }

        public OrderEntity(DataTableReader reader)
        {
            this.Id = Convert.ToInt32(reader["pk_OrderID"]);
            this.OrderType = Convert.ToString(reader["Ordertype"]);
            this.OrdTypeDesc = Convert.ToString(reader["OrderTypeDesc"]);
            this.OrderRef = Convert.ToString(reader["OrderNo"]);
            this.LocID = Convert.ToInt32(reader["fk_LocID"]);
            this.OrderDate = Convert.ToDateTime(reader["OrderDate"]);
            this.Narration = Convert.ToString(reader["Narration"]);
            this.Items = new List<IOrderItem>();
        }

        public string OrderRef { get; set; }
        public string OrderType { get; set; }
        public string OrdTypeDesc { get; set; }
        public DateTime OrderDate { get; set; }
        public decimal? Totalamt { get; set; }
        public int LocID { get; set; }
        public int FinYearID { get; set; }
        public int UserID { get; set; }
        public string Loc { get; set; }
        public bool Status { get; set; }
        public string Narration { get; set; }
        public Int32 Id { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string Name { get; set; }
        

        public List<IOrderItem> Items { get; set; }

        #endregion
    }
}
