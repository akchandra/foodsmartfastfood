﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FoodSmart.Common;

namespace FoodSmart.Entity
{
    public class TransactionItemEntity : ITransactionItem
    {
        #region Constructors

        public TransactionItemEntity()
        {

        }

        public TransactionItemEntity(DataTableReader reader)
        {
            this.Id = Convert.ToInt64(reader["pk_TranDetailID"]);
            this.TransactionId = Convert.ToInt64(reader["fk_TranID"]);
            //this.TransactionType = Convert.ToString(reader["ItemType"]);
            this.ItemId = Convert.ToInt32(reader["fk_RMID"]);
            if (reader["Qty"] != DBNull.Value)
                this.Quantity = Convert.ToDecimal(reader["Qty"]);
            if (reader["Rate"] != DBNull.Value)
                this.Rate = Convert.ToDecimal(reader["Rate"]);
            if (reader["Taxes"] != DBNull.Value)
                this.Tax = Convert.ToDecimal(reader["Taxes"]);
            if (reader["GSTPer"] != DBNull.Value)
                this.GSTPer = Convert.ToDecimal(reader["GSTPer"]);
            this.ItemType = Convert.ToString(reader["ItemType"]);

            this.ItemTypeDesc = Convert.ToString(reader["ItemTypeDesc"]);
            this.ItemName = Convert.ToString(reader["ItemDescr"]);
            this.UOM = Convert.ToString(reader["UOM"]);
            this.Total = Convert.ToDecimal(reader["Total"]);
        }

        public Int64 Id { get; set; }
        public string Name { get; set; }
        public Int64 TransactionId { get; set; }
        public string TransactionType { get; set; }
        public int ItemId { get; set; }
        public string ItemType { get; set; }
        public string ItemTypeDesc { get; set; }
        public string ItemName { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? Rate { get; set; }
        public decimal? Tax { get; set; }
        public decimal? GSTPer { get; set; }
        public string UOM { get; set; }
        public decimal? Total { get; set; }
        #endregion
    }
}