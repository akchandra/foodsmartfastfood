﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FoodSmart.Common;

namespace FoodSmart.Entity
{
    public class RecipeItemEntity : IRecipeItem
    {

        #region Constructors

        public RecipeItemEntity()
        {

        }

        public RecipeItemEntity(DataTableReader reader)
        {
            this.Id = Convert.ToInt64(reader["pk_RecipeItemID"]);
            this.RecipeId = Convert.ToInt64(reader["fk_RecipeID"]);
            this.Type = Convert.ToString(reader["ItemType"]);
            if (ColumnExists(reader, "fk_ItemID"))
                if (reader["fk_ItemID"] != DBNull.Value)
                    this.ItemId = Convert.ToInt32(reader["fk_ItemID"]);

            if (reader["QtyReq"] != DBNull.Value)
                this.Quantity = Convert.ToDecimal(reader["QtyReq"]);

            this.UOM = Convert.ToString(reader["UOM"]);

            if (reader["ItemRate"] != DBNull.Value)
                this.Rate = Convert.ToDecimal(reader["ItemRate"]);

            this.TypeDescription = Convert.ToString(reader["ItemTypeDesc"]);
            this.Item = Convert.ToString(reader["Item"]);
        }

        public Int64 Id { get; set; }
        public string Name { get; set; }
        public Int64 RecipeId { get; set; }
        public string Type { get; set; }
        public string TypeDescription { get; set; }
        public int? ItemId { get; set; }
        public string Item { get; set; }
        public decimal? Quantity { get; set; }
        public string UOM { get; set; }
        public decimal? Rate { get; set; }
        
        #endregion

        #region Private Methods

        public bool ColumnExists(IDataReader reader, string columnName)
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                if (reader.GetName(i).ToUpper() == columnName.ToUpper())
                {
                    return true;
                }
            }

            return false;
        }

        #endregion
    }
}