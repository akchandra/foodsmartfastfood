﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FoodSmart.Common;
using System.Data;
using System.Data.SqlClient;

namespace FoodSmart.Entity
{
    [Serializable]
    public class ItemGroupEntity : IItemGroup
    {
        public string RorF { get; set; }

        public bool Status { get; set; }

        public int Id { get; set; }

        public string Name { get; set; }

        public string DataValue
        {
            get
            {
                return this. Id.ToString() + "|" + this.RorF;
            }
        }

        #region Constructors

        public ItemGroupEntity()
        {
            
        }

        public ItemGroupEntity(DataTableReader reader)
        {
            this.Id = Convert.ToInt32(reader["pk_ItemGroupID"]);
            this.Name = Convert.ToString(reader["Descr"]);
            this.RorF = Convert.ToString(reader["RorF"]);
        }

        #endregion

        #region Private Methods

        public bool ColumnExists(IDataReader reader, string columnName)
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                if (reader.GetName(i).ToUpper() == columnName.ToUpper())
                {
                    return true;
                }
            }

            return false;
        }

        #endregion
    }
}
