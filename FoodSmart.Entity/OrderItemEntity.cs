﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FoodSmart.Common;

namespace FoodSmart.Entity
{
    public class OrderItemEntity : IOrderItem
    {
        #region Constructors

        public OrderItemEntity()
        {

        }

        public OrderItemEntity(DataTableReader reader)
        {
            this.Id = Convert.ToInt64(reader["pk_OrderItemID"]);
            this.OrderId = Convert.ToInt32(reader["fk_OrderID"]);
            this.ItemId = Convert.ToInt32(reader["fk_ItemID"]);
            if (reader["QtyWd"] != DBNull.Value)
                this.QtyWd = Convert.ToDecimal(reader["QtyWd"]);
            if (reader["QtyWe"] != DBNull.Value)
                this.QtyWe = Convert.ToDecimal(reader["QtyWe"]);
            this.ItemTypeDesc = Convert.ToString(reader["ItemTypeDesc"]);
            this.ItemName = Convert.ToString(reader["ItemDescr"]);
            this.UOM = Convert.ToString(reader["UOM"]);
        }

        public Int64 Id { get; set; }
        public string Name { get; set; }
        public Int64 OrderId { get; set; }
        public string OrderType { get; set; }
        public int ItemId { get; set; }
        public string ItemType { get; set; }
        public string ItemTypeDesc { get; set; }
        public string ItemName { get; set; }
        public decimal? QtyWe { get; set; }
        public decimal? QtyWd { get; set; }
        public decimal? Rate { get; set; }
        public string UOM { get; set; }
        #endregion
    }
}
