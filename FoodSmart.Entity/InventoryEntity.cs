﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FoodSmart.Common;
using System.Data;
using System.Data.SqlClient;

namespace FoodSmart.Entity
{

    public class InventoryEntity : IInventory
    {
        
        public int RMID { get; set; }
        public string RMName { get; set; }
        public string RMUOM { get; set; }
        public decimal RMRate { get; set; }
        public string TranType { get; set; }
        public decimal BalanceQty { get; set; }
        public decimal TranQty { get; set; }
        public DateTime TranDate { get; set; }
        public decimal Opbal { get; set; }
        public string RMCode { get; set; }
        public decimal StockValue { get; set; }

        public int GroupID { get; set; }
        public string RMGroupName { get; set; }
        public string GroupInitial { get; set; }

        public int CCID { get; set; }
        public string CostCenterName { get; set; }

        public int TranID { get; set; }
        public string TranNo { get; set; }
        public string Narration { get; set; }
        public int VendorID { get; set; }
        public DateTime? RefDate { get; set; }
        public string RefNo { get; set; }
        public int CurrYearID { get; set; }

        public string RecipeUnit { get; set; }
        public int RecipeID { get; set; }
        public string RecipeName { get; set; }
        public decimal NoofPlates { get; set; }
        public string RecipeDetail { get; set; }
        public int fk_FGItemID { get; set; }

        #region ICommon Members

        public int CreatedBy
        {
            get;
            set;
        }

        public DateTime CreatedOn
        {
            get;
            set;
        }

        public int ModifiedBy
        {
            get;
            set;
        }

        public DateTime ModifiedOn
        {
            get;
            set;
        }

        #endregion

        #region Constructors

        public InventoryEntity()
        {
            
        }

        public InventoryEntity(DataTableReader reader)
        {
            if (ColumnExists(reader, "RMID"))
                if (reader["RMID"] != DBNull.Value)
                    this.RMID = Convert.ToInt32(reader["RMID"]);

            if (ColumnExists(reader, "ItemCode"))
                if (reader["ItemCode"] != DBNull.Value)
                    this.RMCode = Convert.ToString(reader["ItemCode"]);

            if (ColumnExists(reader, "ItemCode"))
                if (reader["ItemCode"] != DBNull.Value)
                    this.RMCode = Convert.ToString(reader["ItemCode"]);

            if (ColumnExists(reader, "ItemName"))
                if (reader["ItemName"] != DBNull.Value)
                    this.RMName = Convert.ToString(reader["ItemName"]);

            if (ColumnExists(reader, "UOM"))
                if (reader["UOM"] != DBNull.Value)
                    this.RMUOM = Convert.ToString(reader["UOM"]);

            if (ColumnExists(reader, "CurrentRate"))
                if (reader["CurrentRate"] != DBNull.Value)
                    this.RMRate = Convert.ToInt32(reader["CurrentRate"]);

            if (ColumnExists(reader, "BalanceQty"))
                if (reader["BalanceQty"] != DBNull.Value)
                    this.BalanceQty = Convert.ToInt32(reader["BalanceQty"]);

            if (ColumnExists(reader, "TranDate"))
                if (reader["TranDate"] != DBNull.Value)
                    this.TranDate = Convert.ToDateTime(reader["TranDate"]);

            if (ColumnExists(reader, "TranQty"))
                if (reader["TranQty"] != DBNull.Value)
                    this.TranQty = Convert.ToInt32(reader["TranQty"]);

            if (ColumnExists(reader, "StockValue"))
                if (reader["StockValue"] != DBNull.Value)
                    this.StockValue = Convert.ToDecimal(reader["StockValue"]);

            if (ColumnExists(reader, "GroupType"))
                if (reader["GroupType"] != DBNull.Value)
                    this.TranType = Convert.ToString(reader["GroupType"]);

            if (ColumnExists(reader, "GroupID"))
                if (reader["GroupID"] != DBNull.Value)
                    this.GroupID = Convert.ToInt32(reader["GroupID"]);

            if (ColumnExists(reader, "RMGroupDescr"))
                if (reader["RMGroupDescr"] != DBNull.Value)
                    this.RMGroupName = Convert.ToString(reader["RMGroupDescr"]);

            if (ColumnExists(reader, "GroupInitials"))
                if (reader["GroupInitials"] != DBNull.Value)
                    this.GroupInitial = Convert.ToString(reader["GroupInitials"]);

            if (ColumnExists(reader, "fk_CCID"))
                if (reader["fk_CCID"] != DBNull.Value)
                    this.CCID = Convert.ToInt32(reader["fk_CCID"]);

            if (ColumnExists(reader, "fk_VendorID"))
                if (reader["fk_VendorID"] != DBNull.Value)
                    this.VendorID = Convert.ToInt32(reader["fk_VendorID"]);

            if (ColumnExists(reader, "CostCenterName"))
                if (reader["CostCenterName"] != DBNull.Value)
                    this.CostCenterName = Convert.ToString(reader["CostCenterName"]);

            if (ColumnExists(reader, "TranID"))
                if (reader["TranID"] != DBNull.Value)
                    this.TranID = Convert.ToInt32(reader["TranID"]);

            if (ColumnExists(reader, "TranNo"))
                if (reader["TranNo"] != DBNull.Value)
                    this.TranNo = Convert.ToString(reader["TranNo"]);

            if (ColumnExists(reader, "RefDate"))
                if (reader["RefDate"] != DBNull.Value)
                    this.RefDate = Convert.ToDateTime(reader["RefDate"]);

            if (ColumnExists(reader, "Narration"))
                if (reader["Narration"] != DBNull.Value)
                    this.Narration = Convert.ToString(reader["Narration"]);

            if (ColumnExists(reader, "RefNo"))
                if (reader["RefNo"] != DBNull.Value)
                    this.RefNo = Convert.ToString(reader["RefNo"]);

            if (ColumnExists(reader, "RUOM"))
                if (reader["RUOM"] != DBNull.Value)
                    this.RecipeUnit = Convert.ToString(reader["RUOM"]);

            if (ColumnExists(reader, "pk_RecipeID"))
                if (reader["pk_RecipeID"] != DBNull.Value)
                    this.RecipeID = Convert.ToInt32(reader["pk_RecipeID"]);

            if (ColumnExists(reader, "fk_ItemID"))
                if (reader["fk_ItemID"] != DBNull.Value)
                    this.fk_FGItemID = Convert.ToInt32(reader["fk_ItemID"]);

            if (ColumnExists(reader, "RecipeName"))
                if (reader["RecipeName"] != DBNull.Value)
                    this.RecipeName = Convert.ToString(reader["RecipeName"]);

            if (ColumnExists(reader, "recipeDetail"))
                if (reader["recipeDetail"] != DBNull.Value)
                    this.RecipeDetail = Convert.ToString(reader["recipeDetail"]);

            if (ColumnExists(reader, "noofPlates"))
                if (reader["noofPlates"] != DBNull.Value)
                    this.NoofPlates = Convert.ToDecimal(reader["noofPlates"]);

            if (ColumnExists(reader, "TranType"))
                if (reader["TranType"] != DBNull.Value)
                    this.TranType = Convert.ToString(reader["TranType"]);

            if (ColumnExists(reader, "DateIntroduced"))
                if (reader["DateIntroduced"] != DBNull.Value)
                    this.TranDate = Convert.ToDateTime(reader["DateIntroduced"]);
        }

        #endregion

        #region Private Methods

        public bool ColumnExists(IDataReader reader, string columnName)
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                if (reader.GetName(i).ToUpper() == columnName.ToUpper())
                {
                    return true;
                }
            }

            return false;
        }

        #endregion
    }
}
