﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FoodSmart.Common;

namespace FoodSmart.Entity
{
    public class ItemRateEntity : IItemRate
    {

        #region Constructors

        public ItemRateEntity()
        {

        }

        public ItemRateEntity(DataTableReader reader)
        {
            this.Id = Convert.ToInt64(reader["pk_RATEID"]);
            this.ItemId = Convert.ToInt64(reader["pk_ItemID"]);
            this.EffectiveDate = Convert.ToDateTime(reader["EffectiveDate"]);
            this.SellingRate = Convert.ToDecimal(reader["SellingRate"]);
            this.ItemName = Convert.ToString(reader["ItemDescr"]);
            this.ItemGroupName = Convert.ToString(reader["GroupName"]);
            this.UOM = Convert.ToString(reader["UOM"]);
            if (reader["GSTPer"] != DBNull.Value)
                this.GST = Convert.ToDecimal(reader["GSTPer"]);
            //if (reader["fk_ItemGroupID"] != DBNull.Value)
            //    this.ItemGroupId = Convert.ToInt32(reader["fk_ItemGroupID"]);
        }

        #endregion

        public Int64 Id { get; set; }
        public string Name { get; set; }
        public Int64 ItemId { get; set; }
        public string ItemName { get; set; }
        public int RateTypeId { get; set; }
        public DateTime EffectiveDate { get; set; }
        public decimal SellingRate { get; set; }
        public decimal NewRate { get; set; }
        public Int64? ComboItemId { get; set; }
        public decimal? OtherCharges { get; set; }
        public decimal? TransferPer { get; set; }
        public decimal? TransferOthers { get; set; }
        public int ItemGroupId { get; set; }
        public string ItemGroupName { get; set; }
        public string UOM { get; set; }
        public decimal? GST { get; set; }
    }
}