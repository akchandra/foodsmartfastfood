﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FoodSmart.Common;

namespace FoodSmart.Entity
{
    public class TransactionEntity : ITransaction
    {

        #region Constructors

        public TransactionEntity()
        {
            this.Items = new List<ITransactionItem>();
        }

        public TransactionEntity(DataTableReader reader)
        {
            this.Id = Convert.ToInt64(reader["pk_TranID"]);
            this.TranType = Convert.ToString(reader["Trantype"]);
            //this.TranTypeDesc = Convert.ToString(reader["TranTypeDesc"]);
            this.TranNo = Convert.ToString(reader["Tranno"]);
            this.FromLocID = Convert.ToInt32(reader["fk_FromStoreID"]);
            this.ToLocID = Convert.ToInt32(reader["fk_ToStoreID"]);
            this.TranDate = Convert.ToDateTime(reader["TranDate"]);
            this.RefDoc = Convert.ToString(reader["Refdoc"]);

            if (reader["RefdocDate"] != DBNull.Value)
                this.RefDate = Convert.ToDateTime(reader["RefdocDate"]);
            this.Narration = Convert.ToString(reader["Narration"]);
            if (reader["totqty"] != DBNull.Value)
                this.TranQty = Convert.ToDecimal(reader["totqty"]);
            this.FromLoc = Convert.ToString(reader["FromLoc"]);
            this.ToLoc = Convert.ToString(reader["ToLoc"]);
            this.VendorId = Convert.ToInt32(reader["fk_VendorID"]);
            this.VendorName = Convert.ToString(reader["VendorName"]);
            this.TranVal = Convert.ToDecimal(reader["Tot"]);
            //this.FromLoc = Convert.ToString(reader["FromLocation"]);
            //this.ToLoc = Convert.ToString(reader["ToLocation"]);
            this.Items = new List<ITransactionItem>();
        }

        public int CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public int FinYearID { get; set; }

        public string FromLoc { get; set; }

        public int FromLocID { get; set; }

        public Int64 Id { get; set; }
        public int ModifiedBy { get; set; }

        public DateTime ModifiedOn { get; set; }

        public string Name { get; set; }

        public string Narration { get; set; }

        public DateTime? RefDate { get; set; }
        public string TranTypeDesc { get; set; }
        public string RefDoc { get; set; }

        public string ToLoc { get; set; }
        public int ToLocID { get; set; }

        public DateTime TranDate { get; set; }

        public string TranNo { get; set; }

        public decimal? TranQty { get; set; }

        public string TranType { get; set; }

        public int UserID { get; set; }

        public int VendorId { get; set; }

        public string VendorName { get; set; }

        public bool Status { get; set; }

        public decimal TranVal { get; set; }

        public List<ITransactionItem> Items { get; set; }

        #endregion
    }
}