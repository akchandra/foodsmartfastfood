﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FoodSmart.Common;
using System.Data;
using System.Data.SqlClient;

namespace FoodSmart.Entity
{
    [Serializable]
    public class LocationEntity : ILocation
    {
        public int LocID { get; set; }
        public string LocName { get; set; }
        public string LocAddress1 { get; set; }
        public string LocAddress2 { get; set; }
        public string LocType { get; set; }
        public string LocTypeDesc { get; set; }
        public string OwnOrFran { get; set; }
        public string OwnOrFranDesc { get; set; }
        public int BillNoLength { get; set; }
        //public decimal ServiceTaxPer { get; set; }
        //public string CalculationMethod { get; set; }
        public string VATNo { get; set; }
        public int UserID { get; set; }
        public string ServTaxNo { get; set; }
        //public decimal SBCPer { get; set; }
        //public DateTime CurrDate { get; set; }
        public string printerType { get; set; }
        public string PrinterName { get; set; }
        public bool PrintBill { get; set; }

        public string btmLine1 { get; set; }
        public string btmLine2 { get; set; }
        public string btmLine3 { get; set; }
        public string btmLine4 { get; set; }
        public string btmLine5 { get; set; }
        public string MachineName { get; set; }
        public string GSTNo { get; set; }
        public DateTime dtOpStock { get; set; }
        public bool Composit { get; set; }

        #region ICommon Members

        public int CreatedBy
        {
            get;
            set;
        }

        public DateTime CreatedOn
        {
            get;
            set;
        }

        public int ModifiedBy
        {
            get;
            set;
        }

        public DateTime ModifiedOn
        {
            get;
            set;
        }

        #endregion

        #region Constructors

        public LocationEntity()
        {
            
        }

        public LocationEntity(DataTableReader reader)
        {
            this.LocID = Convert.ToInt32(reader["pk_LocID"]);

            if (ColumnExists(reader, "LocationName"))
                if (reader["LocationName"] != DBNull.Value)
                    this.LocName = Convert.ToString(reader["LocationName"]);

            if (ColumnExists(reader, "LocAddress1"))
                if (reader["LocAddress1"] != DBNull.Value)
                    this.LocAddress1 = Convert.ToString(reader["LocAddress1"]);

            if (ColumnExists(reader, "LocAddress2"))
                if (reader["LocAddress2"] != DBNull.Value)
                    this.LocAddress2 = Convert.ToString(reader["LocAddress2"]);

            if (ColumnExists(reader, "LocType"))
                if (reader["LocType"] != DBNull.Value)
                    this.LocType = Convert.ToString(reader["LocType"]);

            if (ColumnExists(reader, "LocTypeDesc"))
                if (reader["LocTypeDesc"] != DBNull.Value)
                    this.LocTypeDesc = Convert.ToString(reader["LocTypeDesc"]);

            if (ColumnExists(reader, "OwnOrFran"))
                if (reader["OwnOrFran"] != DBNull.Value)
                    this.OwnOrFran = Convert.ToString(reader["OwnOrFran"]);

            if (ColumnExists(reader, "OwnOrFranDesc"))
                if (reader["OwnOrFranDesc"] != DBNull.Value)
                    this.OwnOrFranDesc = Convert.ToString(reader["OwnOrFranDesc"]);

            if (ColumnExists(reader, "BillNoLength"))
                if (reader["BillNoLength"] != DBNull.Value)
                    this.BillNoLength = Convert.ToInt32(reader["BillNoLength"]);

            if (ColumnExists(reader, "btmLine1"))
                if (reader["btmLine1"] != DBNull.Value)
                    this.btmLine1 = Convert.ToString(reader["btmLine1"]);

            if (ColumnExists(reader, "btmLine2"))
                if (reader["btmLine2"] != DBNull.Value)
                    this.btmLine2 = Convert.ToString(reader["btmLine2"]);

            if (ColumnExists(reader, "btmLine3"))
                if (reader["btmLine3"] != DBNull.Value)
                    this.btmLine3 = Convert.ToString(reader["btmLine3"]);

            if (ColumnExists(reader, "btmLine4"))
                if (reader["btmLine4"] != DBNull.Value)
                    this.btmLine4 = Convert.ToString(reader["btmLine4"]);

            if (ColumnExists(reader, "btmLine5"))
                if (reader["btmLine5"] != DBNull.Value)
                    this.btmLine5 = Convert.ToString(reader["btmLine5"]);

            if (ColumnExists(reader, "GSTNo"))
                if (reader["GSTNo"] != DBNull.Value)
                    this.GSTNo = Convert.ToString(reader["GSTNo"]);

            if (ColumnExists(reader, "UserID"))
                if (reader["UserID"] != DBNull.Value)
                    this.UserID = Convert.ToInt32(reader["UserID"]);

            if (ColumnExists(reader, "PrinterName"))
                if (reader["PrinterName"] != DBNull.Value)
                    this.PrinterName = Convert.ToString(reader["PrinterName"]);

            if (ColumnExists(reader, "PrinterType"))
                if (reader["PrinterType"] != DBNull.Value)
                    this.printerType = Convert.ToString(reader["PrinterType"]);

            //if (ColumnExists(reader, "CurrDate"))
            //    if (reader["CurrDate"] != DBNull.Value)
            //        this.CurrDate = Convert.ToDateTime(reader["CurrDate"]);

            if (ColumnExists(reader, "PrinterBill"))
                if (reader["PrinterBill"] != DBNull.Value)
                    this.PrintBill = Convert.ToBoolean(reader["PrinterBill"]);

            if (ColumnExists(reader, "Composit"))
                if (reader["Composit"] != DBNull.Value)
                    this.Composit = Convert.ToBoolean(reader["Composit"]);

            if (ColumnExists(reader, "OpStockDate"))
                if (reader["OpStockDate"] != DBNull.Value)
                    this.dtOpStock = Convert.ToDateTime(reader["OpStockDate"]);
        }

        #endregion

        #region Private Methods

        public bool ColumnExists(IDataReader reader, string columnName)
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                if (reader.GetName(i).ToUpper() == columnName.ToUpper())
                {
                    return true;
                }
            }

            return false;
        }

        #endregion
    }
}
