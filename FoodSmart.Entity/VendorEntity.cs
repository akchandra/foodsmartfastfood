﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FoodSmart.Common;
using System.Data;
using System.Data.SqlClient;

namespace FoodSmart.Entity
{
     public class VendorEntity : IVendor
    {
        public Int32 VendorID { get; set; }
        public string VendorName { get; set; }
        public string VendorAddress { get; set; }
        public string ContactMob { get; set; }
        public string ContactPerson { get; set; }
        public string LandPhone { get; set; }
        public string GSTNo { get; set; }
        public int StateID { get; set; }
        public string PANNo { get; set; }
        public string emailID { get; set; }

         #region ICommon Members

        public int CreatedBy
        {
            get;
            set;
        }

        public DateTime CreatedOn
        {
            get;
            set;
        }

        public int ModifiedBy
        {
            get;
            set;
        }

        public DateTime ModifiedOn
        {
            get;
            set;
        }

        #endregion

        #region Constructors

        public VendorEntity()
        {
            
        }

        public VendorEntity(DataTableReader reader)
        {
            if (ColumnExists(reader, "pk_VendorID"))
                if (reader["pk_VendorID"] != DBNull.Value)
                    this.VendorID = Convert.ToInt32(reader["pk_VendorID"]);

            if (ColumnExists(reader, "VendorName"))
                if (reader["VendorName"] != DBNull.Value)
                    this.VendorName = Convert.ToString(reader["VendorName"]);

            if (ColumnExists(reader, "VendorAddress"))
                if (reader["VendorAddress"] != DBNull.Value)
                    this.VendorAddress = Convert.ToString(reader["VendorAddress"]);

            if (ColumnExists(reader, "LandPhone"))
                if (reader["LandPhone"] != DBNull.Value)
                    this.LandPhone = Convert.ToString(reader["LandPhone"]);

            if (ColumnExists(reader, "ContactPerson"))
                if (reader["ContactPerson"] != DBNull.Value)
                    this.ContactPerson = Convert.ToString(reader["ContactPerson"]);

            if (ColumnExists(reader, "ContactMob"))
                if (reader["ContactMob"] != DBNull.Value)
                    this.ContactMob = Convert.ToString(reader["ContactMob"]);

            if (ColumnExists(reader, "GSTNo"))
                if (reader["GSTNo"] != DBNull.Value)
                    this.GSTNo = Convert.ToString(reader["GSTNo"]);

            if (ColumnExists(reader, "StateID"))
                if (reader["StateID"] != DBNull.Value)
                    this.StateID = Convert.ToInt32(reader["StateID"]);

            if (ColumnExists(reader, "PANNo"))
                if (reader["PANNo"] != DBNull.Value)
                    this.PANNo = Convert.ToString(reader["PANNo"]);

            if (ColumnExists(reader, "emailid"))
                if (reader["emailid"] != DBNull.Value)
                    this.emailID = Convert.ToString(reader["emailid"]);
        }

        #endregion

        #region Private Methods

        public bool ColumnExists(IDataReader reader, string columnName)
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                if (reader.GetName(i).ToUpper() == columnName.ToUpper())
                {
                    return true;
                }
            }

            return false;
        }

        #endregion
    }


}
