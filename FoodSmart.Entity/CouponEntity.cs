﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FoodSmart.Common;
using System.Data;
using System.Data.SqlClient;

namespace FoodSmart.Entity
{
    [Serializable]
    public class CouponEntity :ICoupon
    {
        public int CPID { get; set; }
        public int CouponStartSerial { get; set; }
        public int CouponEndSerial { get; set; }

        public decimal CouponAmount { get; set; }

        public string CouponPrefix { get; set; }
        public string CouponName { get; set; }

        public DateTime ActivateDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        public  Boolean CouponStatus { get; set; }


        #region ICommon Members

        public int CreatedBy
        {
            get;
            set;
        }

        public DateTime CreatedOn
        {
            get;
            set;
        }

        public int ModifiedBy
        {
            get;
            set;
        }

        public DateTime ModifiedOn
        {
            get;
            set;
        }

        #endregion

        #region Constructors

        public CouponEntity()
        {
            
        }

        public CouponEntity(DataTableReader reader)
        {
            if (ColumnExists(reader, "pk_CouponID"))
                if (reader["pk_CouponID"] != DBNull.Value)
                    this.CPID = Convert.ToInt32(reader["pk_CouponID"]);
            if (ColumnExists(reader, "CouponPrefix"))
                if (reader["CouponPrefix"] != DBNull.Value)
                    this.CouponPrefix = Convert.ToString(reader["CouponPrefix"]);
            if (ColumnExists(reader, "CouponStartSerial"))
                if (reader["CouponStartSerial"] != DBNull.Value)
                    this.CouponStartSerial = Convert.ToInt32(reader["CouponStartSerial"]);
            if (ColumnExists(reader, "CouponEndSerial"))
                if (reader["CouponEndSerial"] != DBNull.Value)
                    this.CouponEndSerial = Convert.ToInt32(reader["CouponEndSerial"]);
            if (ColumnExists(reader, "CouponName"))
                if (reader["CouponName"] != DBNull.Value)
                    this.CouponName = Convert.ToString(reader["CouponName"]);
            if (ColumnExists(reader, "ActivateDate"))
                if (reader["ActivateDate"] != DBNull.Value)
                    this.ActivateDate = Convert.ToDateTime(reader["ActivateDate"]);
            if (ColumnExists(reader, "ExpiryDate"))
                if (reader["ExpiryDate"] != DBNull.Value)
                    this.ExpiryDate = Convert.ToDateTime(reader["ExpiryDate"]);
            if (ColumnExists(reader, "CouponAmount"))
                if (reader["CouponAmount"] != DBNull.Value)
                    this.CouponAmount = Convert.ToDecimal(reader["CouponAmount"]);
            if (ColumnExists(reader, "CouponStatus"))
                if (reader["CouponStatus"] != DBNull.Value)
                    this.CouponStatus = Convert.ToBoolean(reader["CouponStatus"]);
        }

        #endregion

        #region Private Methods

        public bool ColumnExists(IDataReader reader, string columnName)
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                if (reader.GetName(i).ToUpper() == columnName.ToUpper())
                {
                    return true;
                }
            }

            return false;
        }

        #endregion
    }
}
