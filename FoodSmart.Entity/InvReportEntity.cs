﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FoodSmart.Common;
using System.Data.SqlClient;
using System.Data.Common;
using System.Data;

namespace FoodSmart.Entity
{
    [Serializable]
    public class InvReportEntity
    {
    }

    #region "Inventory

    [Serializable]
    public class RecipeListEntity
    {
        public string RecipeName { get; set; }
        public decimal RecipeQty { get; set; }
        public string RecipeType { get; set; }
        public decimal RecipeNoOfPlt { get; set; }
        public string RecipeUOM { get; set; }
        public int RecipeID { get; set; }
        //public string ItemName { get; set; }
        //public decimal ItemQty { get; set; }
        //public decimal Item { get; set; }
        //public string UOM { get; set; }

        public RecipeListEntity()
        {

        }

        public RecipeListEntity(DataTableReader reader)
        {
            this.RecipeName = Convert.ToString(reader["RecipeName"]);
            this.RecipeType = Convert.ToString(reader["RecipeType"]);
            this.RecipeUOM = Convert.ToString(reader["RUOM"]);
            this.RecipeNoOfPlt = Convert.ToDecimal(reader["NoofPlates"]);
            this.RecipeID = Convert.ToInt32(reader["pk_RecipeID"]);
            //this.ItemQty = Convert.ToDecimal(reader["QtyReq"]);
            //this.UOM = Convert.ToString(reader["UOM"]);
            //this.ItemName = Convert.ToString(reader["ItemName"]);
        }


    }
    [Serializable]
    public class RecipeDetailEntity
    {
        public string itemName { get; set; }
        public decimal ItemQty { get; set; }
        public string ItemType { get; set; }
        public decimal ItemRate { get; set; }
        public string ItemUOM { get; set; }
        public int RecipeID { get; set; }

        public RecipeDetailEntity()
        {

        }

        public RecipeDetailEntity(DataTableReader reader)
        {
            this.itemName = Convert.ToString(reader["ItemName"]);
            this.RecipeID = Convert.ToInt32(reader["fk_RecipeID"]);
            if (ColumnExists(reader, "QtyReq"))
                if (reader["QtyReq"] != DBNull.Value)
                    this.ItemQty = Convert.ToDecimal(reader["QtyReq"]);
            if (ColumnExists(reader, "ItemType"))
                if (reader["ItemType"] != DBNull.Value)
                    this.ItemType = Convert.ToString(reader["ItemType"]);
            if (ColumnExists(reader, "ItemRate"))
                if (reader["ItemRate"] != DBNull.Value)
                    this.ItemRate = Convert.ToDecimal(reader["ItemRate"]);
            if (ColumnExists(reader, "UOM"))
                if (reader["UOM"] != DBNull.Value)
                    this.ItemUOM = Convert.ToString(reader["UOM"]);
        }

        #region Private Methods

        public bool ColumnExists(IDataReader reader, string columnName)
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                if (reader.GetName(i).ToUpper() == columnName.ToUpper())
                {
                    return true;
                }
            }

            return false;
        }

        #endregion
    }

    [Serializable]
    public class ReceiptRegEntity
    {
        public int fk_InvTranID { get; set; }
        public string TranNo { get; set; }
        public DateTime TranDate { get; set; }
        public string TranType { get; set; }
        public decimal Gtotal { get; set; }
        public string RefDocNo { get; set; }
        public int RecipeID { get; set; }
        public DateTime? RefDate { get; set; }
        public string VendorName { get; set; }
        public string Narration { get; set; }


        public ReceiptRegEntity()
        {

        }

        public ReceiptRegEntity(DataTableReader reader)
        {
            this.fk_InvTranID = Convert.ToInt32(reader["pk_TranID"]);
            this.TranType = Convert.ToString(reader["TranType"]);
            this.TranNo = Convert.ToString(reader["TranNo"]);
            this.TranDate = Convert.ToDateTime(reader["TranDate"]);
            if (ColumnExists(reader, "RefDoc"))
                if (reader["RefDoc"] != DBNull.Value)
                    this.RefDocNo = Convert.ToString(reader["RefDoc"]);
            if (ColumnExists(reader, "RefDate"))
                if (reader["RefDate"] != DBNull.Value)
                    this.RefDate = Convert.ToDateTime(reader["RefDate"]);
            this.VendorName = Convert.ToString(reader["VendorName"]);
            if (ColumnExists(reader, "Narration"))
                if (reader["Narration"] != DBNull.Value)
                    this.Narration = Convert.ToString(reader["Narration"]);
            if (ColumnExists(reader, "GTotal"))
                if (reader["GTotal"] != DBNull.Value)
                    this.Gtotal = Convert.ToDecimal(reader["GTotal"]);
        }

        #region Private Methods

        public bool ColumnExists(IDataReader reader, string columnName)
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                if (reader.GetName(i).ToUpper() == columnName.ToUpper())
                {
                    return true;
                }
            }

            return false;
        }

        #endregion
    }

    [Serializable]
    public class RcptIssueEntity
    {
        public int fk_InvTranID { get; set; }
        public string TranNo { get; set; }
        public DateTime TranDate { get; set; }
        public string TranType { get; set; }
        public string ItemName { get; set; }
        public decimal Qty { get; set; }
        public decimal Rate { get; set; }
        public decimal Taxes { get; set; }
        public decimal Gtotal { get; set; }
        public string GroupName { get; set; }


        public RcptIssueEntity()
        {

        }

        public RcptIssueEntity(DataTableReader reader)
        {
            if (ColumnExists(reader, "fk_TranID"))
                if (reader["fk_TranID"] != DBNull.Value)
                    this.fk_InvTranID = Convert.ToInt32(reader["fk_TranID"]);

            if (ColumnExists(reader, "TranType"))
                if (reader["TranType"] != DBNull.Value)
                    this.TranType = Convert.ToString(reader["TranType"]);

            if (ColumnExists(reader, "ItemName"))
                if (reader["ItemName"] != DBNull.Value)
                    this.ItemName = Convert.ToString(reader["ItemName"]);

            if (ColumnExists(reader, "Qty"))
                if (reader["Qty"] != DBNull.Value)
                    this.Qty = Convert.ToDecimal(reader["Qty"]);

            if (ColumnExists(reader, "Rate"))
                if (reader["Rate"] != DBNull.Value)
                    this.Rate = Convert.ToDecimal(reader["Rate"]);

            if (ColumnExists(reader, "RefDoc"))
                if (reader["RefDoc"] != DBNull.Value)
                    this.Taxes = Convert.ToDecimal(reader["Taxes"]);

            if (ColumnExists(reader, "rmGroupDescr"))
                if (reader["rmGroupDescr"] != DBNull.Value)
                    this.GroupName = Convert.ToString(reader["rmGroupDescr"]);

            if (ColumnExists(reader, "TotalAmt"))
                if (reader["TotalAmt"] != DBNull.Value)
                    this.Gtotal = Convert.ToDecimal(reader["TotalAmt"]);

        }

        #region Private Methods

        public bool ColumnExists(IDataReader reader, string columnName)
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                if (reader.GetName(i).ToUpper() == columnName.ToUpper())
                {
                    return true;
                }
            }

            return false;
        }

        #endregion
    }
    #region Stock Ledger Item entity
    [Serializable]
    public class StockTranItemEntity
    {
        //public int fk_InvTranID { get; set; }
        public string TranNo { get; set; }
        public DateTime TranDate { get; set; }
        public string TranType { get; set; }
        public decimal Gtotal { get; set; }
        //public string RefDocNo { get; set; }
        //public int RecipeID { get; set; }
        //public DateTime? RefDate { get; set; }
        public string VendorName { get; set; }
        public string RMName { get; set; }
        //public string Narration { get; set; }
        //public string GroupName { get; set; }
        public string UOM { get; set; }
        public decimal Qty { get; set; }
        public decimal Rate { get; set; }
        public int RMID { get; set; }

        public StockTranItemEntity()
        {

        }

        public StockTranItemEntity(DataTableReader reader)
        {
            if (ColumnExists(reader, "TranType"))
                if (reader["TranType"] != DBNull.Value)
                    this.TranType = Convert.ToString(reader["TranType"]);
            if (ColumnExists(reader, "TranNo"))
                if (reader["TranNo"] != DBNull.Value)
                    this.TranNo = Convert.ToString(reader["TranNo"]);
            if (ColumnExists(reader, "TranDate"))
                if (reader["TranDate"] != DBNull.Value)
                    this.TranDate = Convert.ToDateTime(reader["TranDate"]);
            if (ColumnExists(reader, "VendorName"))
                if (reader["VendorName"] != DBNull.Value)
                    this.VendorName = Convert.ToString(reader["VendorName"]);
            if (ColumnExists(reader, "Qty"))
                if (reader["Qty"] != DBNull.Value)
                    this.Qty = Convert.ToDecimal(reader["Qty"]);
            if (ColumnExists(reader, "Rate"))
                if (reader["Rate"] != DBNull.Value)
                    this.Rate = Convert.ToDecimal(reader["Rate"]);
            if (ColumnExists(reader, "Totals"))
                if (reader["Totals"] != DBNull.Value)
                    this.Gtotal = Convert.ToDecimal(reader["Totals"]);
            if (ColumnExists(reader, "RMID"))
                if (reader["RMID"] != DBNull.Value)
                    this.RMID = Convert.ToInt32(reader["RMID"]);
        }

        #region Private Methods

        public bool ColumnExists(IDataReader reader, string columnName)
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                if (reader.GetName(i).ToUpper() == columnName.ToUpper())
                {
                    return true;
                }
            }

            return false;
        }

        #endregion
    }
    #endregion

    #region Receipt Register Item wise
    [Serializable]
    public class ItemwiseReceiptRegEntity
    {
        public int fk_InvTranID { get; set; }
        public string TranNo { get; set; }
        public DateTime TranDate { get; set; }
        public string TranType { get; set; }
        public decimal Gtotal { get; set; }
        public string RefDocNo { get; set; }
        public int RecipeID { get; set; }
        public DateTime? RefDate { get; set; }
        public string VendorName { get; set; }
        public string Narration { get; set; }
        public string GroupName { get; set; }
        public string ItemName { get; set; }
        public decimal Qty { get; set; }
        public decimal Rate { get; set; }
        public decimal Taxes { get; set; }

        public ItemwiseReceiptRegEntity()
        {

        }
        public ItemwiseReceiptRegEntity(DataTableReader reader)
        {
            if (ColumnExists(reader, "pk_TranID"))
                if (reader["pk_TranID"] != DBNull.Value)
                    this.fk_InvTranID = Convert.ToInt32(reader["pk_TranID"]);
            if (ColumnExists(reader, "TranType"))
                if (reader["TranType"] != DBNull.Value)
                    this.TranType = Convert.ToString(reader["TranType"]);
            if (ColumnExists(reader, "TranNo"))
                if (reader["TranNo"] != DBNull.Value)
                    this.TranNo = Convert.ToString(reader["TranNo"]);
            if (ColumnExists(reader, "TranDate"))
                if (reader["TranDate"] != DBNull.Value)
                    this.TranDate = Convert.ToDateTime(reader["TranDate"]);
            if (ColumnExists(reader, "RefDoc"))
                if (reader["RefDoc"] != DBNull.Value)
                    this.RefDocNo = Convert.ToString(reader["RefDoc"]);
            if (ColumnExists(reader, "rmGroupName"))
                if (reader["rmGroupName"] != DBNull.Value)
                    this.GroupName = Convert.ToString(reader["rmGroupName"]);
            if (ColumnExists(reader, "RMName"))
                if (reader["RMName"] != DBNull.Value)
                    this.ItemName = Convert.ToString(reader["RMName"]);
            if (ColumnExists(reader, "RefDate"))
                if (reader["RefDate"] != DBNull.Value)
                    this.RefDate = Convert.ToDateTime(reader["RefDate"]);
            if (ColumnExists(reader, "VendorName"))
                if (reader["VendorName"] != DBNull.Value)
                    this.VendorName = Convert.ToString(reader["VendorName"]);
            if (ColumnExists(reader, "Narration"))
                if (reader["Narration"] != DBNull.Value)
                    this.Narration = Convert.ToString(reader["Narration"]);
            if (ColumnExists(reader, "GTotal"))
                if (reader["GTotal"] != DBNull.Value)
                    this.Gtotal = Convert.ToDecimal(reader["GTotal"]);
            if (ColumnExists(reader, "QTY"))
                if (reader["QTY"] != DBNull.Value)
                    this.Qty = Convert.ToDecimal(reader["QTY"]);
            if (ColumnExists(reader, "ItemRate"))
                if (reader["ItemRate"] != DBNull.Value)
                    this.Rate = Convert.ToDecimal(reader["ItemRate"]);
            if (ColumnExists(reader, "GSTAmt"))
                if (reader["GSTAmt"] != DBNull.Value)
                    this.Taxes = Convert.ToDecimal(reader["GSTAmt"]);
        }

        #region Private Methods

        public bool ColumnExists(IDataReader reader, string columnName)
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                if (reader.GetName(i).ToUpper() == columnName.ToUpper())
                {
                    return true;
                }
            }

            return false;
        }

        #endregion
    }
    #endregion 
    #region Issue Register entity
    [Serializable]
    public class IssueRegEntity
    {
        public int fk_InvTranID { get; set; }
        public string TranNo { get; set; }
        public DateTime TranDate { get; set; }
        public string TranType { get; set; }
        public decimal Gtotal { get; set; }
        public string RefDocNo { get; set; }
        public int RecipeID { get; set; }
        public DateTime? RefDate { get; set; }
        public string RestName { get; set; }
        public string Narration { get; set; }


        public IssueRegEntity()
        {

        }

        public IssueRegEntity(DataTableReader reader)
        {
            if (ColumnExists(reader, "pk_TranID"))
                if (reader["pk_TranID"] != DBNull.Value)
                    this.fk_InvTranID = Convert.ToInt32(reader["pk_TranID"]);
            if (ColumnExists(reader, "TranType"))
                if (reader["TranType"] != DBNull.Value)
                    this.TranType = Convert.ToString(reader["TranType"]);
            if (ColumnExists(reader, "TranNo"))
                if (reader["TranNo"] != DBNull.Value)
                    this.TranNo = Convert.ToString(reader["TranNo"]);
            if (ColumnExists(reader, "TranDate"))
                if (reader["TranDate"] != DBNull.Value)
                    this.TranDate = Convert.ToDateTime(reader["TranDate"]);
            if (ColumnExists(reader, "RefDoc"))
                if (reader["RefDoc"] != DBNull.Value)
                    this.RefDocNo = Convert.ToString(reader["RefDoc"]);
            if (ColumnExists(reader, "RefDate"))
                if (reader["RefDate"] != DBNull.Value)
                    this.RefDate = Convert.ToDateTime(reader["RefDate"]);
            if (ColumnExists(reader, "RestName"))
                if (reader["RestName"] != DBNull.Value)
                    this.RestName = Convert.ToString(reader["RestName"]);
            if (ColumnExists(reader, "Narration"))
                if (reader["Narration"] != DBNull.Value)
                    this.Narration = Convert.ToString(reader["Narration"]);
            if (ColumnExists(reader, "GTotal"))
                if (reader["GTotal"] != DBNull.Value)
                    this.Gtotal = Convert.ToDecimal(reader["GTotal"]);
        }

        #region Private Methods

        public bool ColumnExists(IDataReader reader, string columnName)
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                if (reader.GetName(i).ToUpper() == columnName.ToUpper())
                {
                    return true;
                }
            }

            return false;
        }

        #endregion
    }
    #endregion
    #endregion

}
