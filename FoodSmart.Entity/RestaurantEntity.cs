﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FoodSmart.Common;
using System.Data;
using System.Data.SqlClient;

namespace FoodSmart.Entity
{
    [Serializable]
    public class RestaurantEntity : IRestaurant
    {
        //public int CardID { get; set; }
        public string RestType { get; set; }
        public string RestName { get; set; }
        public string LocName { get; set; }
        public string RestPass { get; set; }
        public bool PrintBill { get; set; }
        public int RestID { get; set; }
        public decimal Ratio { get; set; }
        public int LocID { get; set; }
        public bool RestStatus { get; set; }
        public string CalculationMethod { get; set; }
        public string VATNo { get; set; }
        public int UserID { get; set; }
        public string ServTaxNo { get; set; }
        public decimal ServTaxPer { get; set; }
        public decimal SBCPer { get; set; }
        public string Mode { get; set; }
        public DateTime CurrDate { get; set; }
        public string printerType { get; set; }
        public string PrinterName { get; set; }
        public string KOTPrinterType { get; set; }
        public string KOTPrinterName { get; set; }

        #region ICommon Members

        public int CreatedBy
        {
            get;
            set;
        }

        public DateTime CreatedOn
        {
            get;
            set;
        }

        public int ModifiedBy
        {
            get;
            set;
        }

        public DateTime ModifiedOn
        {
            get;
            set;
        }

        #endregion

        #region Constructors

        public RestaurantEntity()
        {
            
        }

        public RestaurantEntity(DataTableReader reader)
        {
            //if (ColumnExists(reader, "pk_CardID"))
            //    if (reader["pk_CardID"] != DBNull.Value)
            //        this.CardID = Convert.ToInt32(reader["pk_CardID"]);

            if (ColumnExists(reader, "RestName"))
                if (reader["RestName"] != DBNull.Value)
                    this.RestName = Convert.ToString(reader["RestName"]);

            if (ColumnExists(reader, "LocationName"))
                if (reader["LocationName"] != DBNull.Value)
                    this.LocName = Convert.ToString(reader["LocationName"]);

            if (ColumnExists(reader, "CurrDate"))
                if (reader["CurrDate"] != DBNull.Value)
                    this.CurrDate = Convert.ToDateTime(reader["CurrDate"]);

            if (ColumnExists(reader, "Ratio"))
                if (reader["Ratio"] != DBNull.Value)
                    this.Ratio = Convert.ToDecimal(reader["Ratio"]);

            if (ColumnExists(reader, "RestPassword"))
                if (reader["RestPassword"] != DBNull.Value)
                    this.RestPass = Convert.ToString(reader["RestPassword"]);

            if (ColumnExists(reader, "pk_RestID"))
                if (reader["pk_RestID"] != DBNull.Value)
                    this.RestID = Convert.ToInt32(reader["pk_RestID"]);

            if (ColumnExists(reader, "fk_LocID"))
                if (reader["fk_LocID"] != DBNull.Value)
                    this.LocID = Convert.ToInt32(reader["fk_LocID"]);

            if (ColumnExists(reader, "RestStatus"))
                if (reader["RestStatus"] != DBNull.Value)
                    this.RestStatus = Convert.ToBoolean(reader["RestStatus"]);

            if (ColumnExists(reader, "VATNo"))
                if (reader["VATNo"] != DBNull.Value)
                    this.VATNo = Convert.ToString(reader["VATNo"]);

            if (ColumnExists(reader, "RestType"))
                if (reader["RestType"] != DBNull.Value)
                    this.RestType = Convert.ToString(reader["RestType"]);

            if (ColumnExists(reader, "ServiceTaxPer"))
                if (reader["ServiceTaxPer"] != DBNull.Value)
                    this.ServTaxPer = Convert.ToDecimal(reader["ServiceTaxPer"]);

            if (ColumnExists(reader, "SBCPer"))
                if (reader["SBCPer"] != DBNull.Value)
                    this.SBCPer = Convert.ToDecimal(reader["SBCPer"]);

            if (ColumnExists(reader, "ServiceTaxNo"))
                if (reader["ServiceTaxNo"] != DBNull.Value)
                    this.ServTaxNo = Convert.ToString(reader["ServiceTaxNo"]);

            if (ColumnExists(reader, "CalculationMethod"))
                if (reader["CalculationMethod"] != DBNull.Value)
                    this.CalculationMethod = Convert.ToString(reader["CalculationMethod"]);

            if (ColumnExists(reader, "UserID"))
                if (reader["UserID"] != DBNull.Value)
                    this.UserID = Convert.ToInt32(reader["UserID"]);

            if (ColumnExists(reader, "PrinterName"))
                if (reader["PrinterName"] != DBNull.Value)
                    this.PrinterName = Convert.ToString(reader["PrinterName"]);

            if (ColumnExists(reader, "PrinterType"))
                if (reader["PrinterType"] != DBNull.Value)
                    this.printerType = Convert.ToString(reader["PrinterType"]);

            if (ColumnExists(reader, "KOTPrinterType"))
                if (reader["KOTPrinterType"] != DBNull.Value)
                    this.KOTPrinterType = Convert.ToString(reader["KOTPrinterType"]);

            if (ColumnExists(reader, "KOTPrinterName"))
                if (reader["KOTPrinterName"] != DBNull.Value)
                    this.KOTPrinterName = Convert.ToString(reader["KOTPrinterName"]);

        }

        #endregion

        #region Private Methods

        public bool ColumnExists(IDataReader reader, string columnName)
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                if (reader.GetName(i).ToUpper() == columnName.ToUpper())
                {
                    return true;
                }
            }

            return false;
        }

        #endregion
    }
}
