﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FoodSmart.Common;
using System.Data;
using System.Data.SqlClient;

namespace FoodSmart.Entity
{
    public class InvoiceEntity : IInvoice
    {
        public string mode { get; set; }
        public int InvoiceID { get; set; }
        public int CustID { get; set; }
        public int UserID { get; set; }
        public int LocID { get; set; }
        public int YearID { get; set; }
        public int RestID { get; set; }
        public int SerialNo { get; set; }
        public string CardNoInside { get; set; }
        public string PaymentMode { get; set; }
        public string BillType { get; set; }
        public decimal BillAmount { get; set; }
        public DateTime BillDate { get; set; }
        public string MachineName { get; set; }
        public decimal VATAmount { get; set; }
        public decimal ServiceTaxAmount { get; set; }
        public decimal SBC { get; set; }
        public decimal SecDeposit { get; set; }
        public decimal DamageAmt { get; set; }
        public decimal RoundOff { get; set; }
        public int RefundReasonID { get; set; }
        public decimal DiscountPer { get; set; }
        public int DiscReasonID { get; set; }
        public string MobileNo { get; set; }
        public string CustName { get; set; }
        public decimal CashAmount { get; set; }
        public decimal CCAmount { get; set; }
        public decimal MPAmount { get; set; }
        public decimal eWalletAmount { get; set; }
        public int CouponNo { get; set; }
        public int CouponID { get; set; }
        public decimal CouponAmt { get; set; }
        public string Prefix { get; set; }

        #region ICommon Members

        public int CreatedBy
        {
            get;
            set;
        }

        public DateTime CreatedOn
        {
            get;
            set;
        }

        public int ModifiedBy
        {
            get;
            set;
        }

        public DateTime ModifiedOn
        {
            get;
            set;
        }

        #endregion

        #region Constructors

        public InvoiceEntity()
        {
            
        }

        public InvoiceEntity(DataTableReader reader)
        {
            this.InvoiceID = Convert.ToInt32(reader["pk_InvoiceID"]);
            if (ColumnExists(reader, "fk_CustID"))
                if (reader["fk_CustID"] != DBNull.Value)
                    this.CustID = Convert.ToInt32(reader["fk_CustID"]);
            this.BillType = Convert.ToString(reader["BillType"]);
            this.BillDate = Convert.ToDateTime(reader["BillDate"]);

            //if (ColumnExists(reader, "SerialNo"))
            //    if (reader["SerialNo"] != DBNull.Value)
            //        this.SerialNo = Convert.ToInt32(reader["SerialNo"]);

            if (ColumnExists(reader, "BillAmount"))
                if (reader["BillAmount"] != DBNull.Value)
                    this.BillAmount = Convert.ToInt32(reader["BillAmount"]);

            if (ColumnExists(reader, "SecDeposit"))
                if (reader["SecDeposit"] != DBNull.Value)
                    this.SecDeposit = Convert.ToInt32(reader["SecDeposit"]);

            if (ColumnExists(reader, "DamageAmount"))
                if (reader["DamageAmount"] != DBNull.Value)
                    this.DamageAmt = Convert.ToInt32(reader["DamageAmount"]);

            if (ColumnExists(reader, "MachineName"))
                if (reader["MachineName"] != DBNull.Value)
                    this.MachineName = Convert.ToString(reader["MachineName"]);

            if (reader["fk_LocID"] != DBNull.Value)
                if (reader["fk_LocID"] != DBNull.Value)
                    this.LocID = Convert.ToInt32(reader["fk_LocID"]);

            if (ColumnExists(reader, "PaymentMode"))
                if (reader["PaymentMode"] != DBNull.Value)
                    this.PaymentMode = Convert.ToString(reader["PaymentMode"]);

            if (ColumnExists(reader, "RoundOff"))
                if (reader["RoundOff"] != DBNull.Value)
                    this.RoundOff = Convert.ToDecimal(reader["RoundOff"]);

            if (reader["fk_RefundReasonID"] != DBNull.Value)
                if (reader["fk_RefundReasonID"] != DBNull.Value)
                    this.RefundReasonID = Convert.ToInt32(reader["fk_RefundReasonID"]);

            if (reader["fk_DiscReason"] != DBNull.Value)
                if (reader["fk_DiscReason"] != DBNull.Value)
                    this.DiscReasonID = Convert.ToInt32(reader["fk_DiscReason"]);

            if (ColumnExists(reader, "VATAmount"))
                if (reader["VATAmount"] != DBNull.Value)
                    this.VATAmount = Convert.ToDecimal(reader["VATAmount"]);

            if (ColumnExists(reader, "ServiceTaxAmount"))
                if (reader["ServiceTaxAmount"] != DBNull.Value)
                    this.ServiceTaxAmount = Convert.ToDecimal(reader["ServiceTaxAmount"]);

           if (ColumnExists(reader, "SBC"))
                if (reader["SBC"] != DBNull.Value)
                    this.SBC = Convert.ToDecimal(reader["SBC"]);

            if (ColumnExists(reader, "mode"))
                if (reader["mode"] != DBNull.Value)
                    this.mode = Convert.ToString(reader["mode"]);

            if (ColumnExists(reader, "MobileNo"))
                if (reader["MobileNo"] != DBNull.Value)
                    this.MobileNo = Convert.ToString(reader["MobileNo"]);

            if (ColumnExists(reader, "CustName"))
                if (reader["CustName"] != DBNull.Value)
                    this.CustName = Convert.ToString(reader["CustName"]);

            if (ColumnExists(reader, "CashAmount"))
                if (reader["CashAmount"] != DBNull.Value)
                    this.CashAmount = Convert.ToDecimal(reader["CashAmount"]);
            if (ColumnExists(reader, "CCAmount"))
                if (reader["CCAmount"] != DBNull.Value)
                    this.CCAmount = Convert.ToDecimal(reader["CCAmount"]);
            if (ColumnExists(reader, "MPAmount"))
                if (reader["MPAmount"] != DBNull.Value)
                    this.MPAmount = Convert.ToDecimal(reader["MPAmount"]);
            if (ColumnExists(reader, "eWalletAmount"))
                if (reader["eWalletAmount"] != DBNull.Value)
                    this.eWalletAmount = Convert.ToDecimal(reader["eWalletAmount"]);
            if (reader["fk_CouponID"] != DBNull.Value)
                if (reader["fk_CouponID"] != DBNull.Value)
                    this.CouponID = Convert.ToInt32(reader["fk_CouponID"]);
            if (reader["CouponNo"] != DBNull.Value)
                if (reader["CouponNo"] != DBNull.Value)
                    this.CouponNo = Convert.ToInt32(reader["CouponNo"]);
            if (reader["Prefix"] != DBNull.Value)
                if (reader["Prefix"] != DBNull.Value)
                    this.Prefix = Convert.ToString(reader["Prefix"]);
            if (ColumnExists(reader, "CouponAmt"))
                if (reader["CouponAmt"] != DBNull.Value)
                    this.CouponAmt = Convert.ToDecimal(reader["CouponAmt"]);

        }

        #endregion

        #region Private Methods

        public bool ColumnExists(IDataReader reader, string columnName)
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                if (reader.GetName(i).ToUpper() == columnName.ToUpper())
                {
                    return true;
                }
            }

            return false;
        }

        #endregion
    }
}
