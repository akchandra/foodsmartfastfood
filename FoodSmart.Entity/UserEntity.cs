﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FoodSmart.Common;
using System.Data;
using System.Data.SqlClient;

namespace FoodSmart.Entity
{
    public class UserEntity : IUser
    {
        #region IUser Members

        public string Password
        {
            get;
            set;
        }

        public string NewPassword
        {
            get;
            set;
        }

        public string LoginID
        {
            get;
            set;
        }

        public string UserFullName
        {
            get;
            set;
        }

        public string EmailId
        {
            get;
            set;
        }

        public bool IsActive
        {
            get;
            set;
        }

        public int LocID
        {
            get;
            set;
        }

        public string RestName { get; set; }
        public bool Authorizer { get; set; }
        public DateTime CurrDate { get; set; }
        public bool UserLocked { get; set; }
        public string CalcMethod { get; set; }
        public decimal SecDeposit { get; set; }
        public bool LocationExists { get; set; }

        public string Mode
        {
            get;
            set;
        }

        public string CardNoOutside
        {
            get;
            set;
        }

        public string CardNoInside
        {
            get;
            set;
        }

        public int RestID
        {
            get;
            set;
        }

        public string LocName
        {
            get;
            set;
        }

        public int RoleID
        {
            get;
            set;
        }

        public string RoleName
        {
            get;
            set;
        }
        #endregion

      

        #region IBase<int> Members

        public int Id
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        #endregion

        #region ICommon Members

        public int CreatedBy
        {
            get;
            set;
        }

        public DateTime CreatedOn
        {
            get;
            set;
        }

        public int ModifiedBy
        {
            get;
            set;
        }

        public DateTime ModifiedOn
        {
            get;
            set;
        }

        #endregion

        #region Constructors

        public UserEntity()
        {
            
        }

        public UserEntity(DataTableReader reader)
        {

            if (ColumnExists(reader, "UserId"))
                if (reader["UserId"] != DBNull.Value)
                    this.Id = Convert.ToInt32(reader["UserId"]);

            //if (ColumnExists(reader, "fk_CardId"))
            //    if (reader["fk_CardId"] != DBNull.Value)
            //        this.CardID = Convert.ToInt32(reader["fk_CardId"]);

            if (ColumnExists(reader, "UserName"))
                if (reader["UserName"] != DBNull.Value)
                    this.LoginID = Convert.ToString(reader["UserName"]);

            if (ColumnExists(reader, "LoginID"))
                if (reader["LoginID"] != DBNull.Value)
                    this.LoginID = Convert.ToString(reader["LoginID"]);

            if (ColumnExists(reader, "FirstName"))
                if (reader["FirstName"] != DBNull.Value)
                    this.UserFullName = Convert.ToString(reader["FirstName"]);

            if (ColumnExists(reader, "fk_RoleId"))
                if (reader["fk_RoleId"] != DBNull.Value)
                    this.RoleID = Convert.ToInt32(reader["fk_RoleId"]);

            if (ColumnExists(reader, "RoleId"))
                if (reader["RoleId"] != DBNull.Value)
                    this.RoleID = Convert.ToInt32(reader["RoleId"]);

            if (ColumnExists(reader, "RoleName"))
                if (reader["RoleName"] != DBNull.Value)
                    this.RoleName = Convert.ToString(reader["RoleName"]);

            if (ColumnExists(reader, "CurrDate"))
                if (reader["CurrDate"] != DBNull.Value)
                    this.CurrDate = Convert.ToDateTime(reader["CurrDate"]);

            if (reader["EmailId"] != DBNull.Value)
                this.EmailId = Convert.ToString(reader["EmailId"]);

            //if (reader["fk_RestId"] != DBNull.Value)
            //    this.RestID = Convert.ToInt32(reader["fk_RestId"]);

            if (ColumnExists(reader, "LocName"))
                if (reader["LocName"] != DBNull.Value)
                    this.LocName = Convert.ToString(reader["LocName"]);

            //if (ColumnExists(reader, "CardNoOutside"))
            //    if (reader["CardNoOutside"] != DBNull.Value)
            //        this.CardNoOutside = Convert.ToString(reader["CardNoOutside"]);

            //if (ColumnExists(reader, "CardNoInside"))
            //    if (reader["CardNoInside"] != DBNull.Value)
            //        this.CardNoInside = Convert.ToString(reader["CardNoInside"]);

            if (ColumnExists(reader, "fk_LocID"))
                if (reader["fk_LocID"] != DBNull.Value)
                    this.LocID = Convert.ToInt32(reader["fk_LocID"]);

            //if (ColumnExists(reader, "Authorizer"))
            //    if (reader["Authorizer"] != DBNull.Value)
            //        this.Authorizer = Convert.ToBoolean(reader["Authorizer"]);

            if (ColumnExists(reader, "CalcMethod"))
                if (reader["CalcMethod"] != DBNull.Value)
                    this.CalcMethod = Convert.ToString(reader["CalcMethod"]);

            if (ColumnExists(reader, "RestName"))
                if (reader["RestName"] != DBNull.Value)
                    this.RestName = Convert.ToString(reader["RestName"]);

            //if (ColumnExists(reader, "SecurityDeposit"))
            //    if (reader["SecurityDeposit"] != DBNull.Value)
            //        this.SecDeposit = Convert.ToInt32(reader["SecurityDeposit"]);

            if (ColumnExists(reader, "UserStatus") && reader["UserStatus"] != DBNull.Value) 
                this.IsActive = Convert.ToBoolean(reader["UserStatus"]);

            if (ColumnExists(reader, "LocationExists"))
                if (reader["LocationExists"] != DBNull.Value)
                    this.LocationExists = Convert.ToBoolean(reader["LocationExists"]);

        }

        #endregion

        #region Private Methods

        private bool HasColumn(DataTableReader reader, string columnName)
        {
            try
            {
                return reader.GetOrdinal(columnName) >= 0;
            }
            catch (ArgumentException)
            {
                return false;
            }
        }

        public bool ColumnExists(IDataReader reader, string columnName)
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                if (reader.GetName(i).ToUpper() == columnName.ToUpper())
                {
                    return true;
                }
            }

            return false;
        }

        #endregion
    }
}
