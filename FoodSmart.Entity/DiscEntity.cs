﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FoodSmart.Common;
using System.Data;
using System.Data.SqlClient;

namespace FoodSmart.Entity
{
    [Serializable]
    public class DiscEntity : IDisc
    {
        public int DiscAppID { get; set; }
        public int DiscCalcID { get; set; }
        public int DiscTypeID { get; set; }
        public string DiscName { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal DiscPer { get; set; }
        public int BaseItemID { get; set; }
        public int DiscItemID { get; set; }
        public decimal DiscAmount { get; set; }
        public decimal ThresholeAmt { get; set; }
        public int ThresholeQty { get; set; }
        public string DiscTypeName { get; set; }
        public string DiscCalcMethod { get; set; }
        public bool DiscStatus { get; set; }

        #region ICommon Members

        public int CreatedBy
        {
            get;
            set;
        }

        public DateTime CreatedOn
        {
            get;
            set;
        }

        public int ModifiedBy
        {
            get;
            set;
        }

        public DateTime ModifiedOn
        {
            get;
            set;
        }

        #endregion

        #region Constructors

        public DiscEntity()
        {
            
        }

        public DiscEntity(DataTableReader reader)
        {
            if (ColumnExists(reader, "pk_DiscAppID"))
                if (reader["pk_DiscAppID"] != DBNull.Value)
                    this.DiscAppID = Convert.ToInt32(reader["pk_DiscAppID"]);
            if (ColumnExists(reader, "fk_DiscTypeID"))
                if (reader["fk_DiscTypeID"] != DBNull.Value)
                    this.DiscTypeID = Convert.ToInt32(reader["fk_DiscTypeID"]);
            if (ColumnExists(reader, "fk_DiscCalcID"))
                if (reader["fk_DiscCalcID"] != DBNull.Value)
                    this.DiscCalcID = Convert.ToInt32(reader["fk_DiscCalcID"]);
            if (ColumnExists(reader, "StartDate"))
                if (reader["StartDate"] != DBNull.Value)
                    this.StartDate = Convert.ToDateTime(reader["StartDate"]);
            if (ColumnExists(reader, "EndDate"))
                if (reader["EndDate"] != DBNull.Value)
                    this.EndDate = Convert.ToDateTime(reader["EndDate"]);
            if (ColumnExists(reader, "DiscPer"))
                if (reader["DiscPer"] != DBNull.Value)
                    this.DiscPer = Convert.ToDecimal(reader["DiscPer"]);
            if (ColumnExists(reader, "fk_BaseItemID"))
                if (reader["fk_BaseItemID"] != DBNull.Value)
                    this.BaseItemID = Convert.ToInt32(reader["fk_BaseItemID"]);
            if (ColumnExists(reader, "fk_FreeItemID"))
                if (reader["fk_FreeItemID"] != DBNull.Value)
                    this.BaseItemID = Convert.ToInt32(reader["fk_FreeItemID"]);
            if (ColumnExists(reader, "ThresholdAmount"))
                if (reader["ThresholdAmount"] != DBNull.Value)
                    this.ThresholeAmt = Convert.ToDecimal(reader["ThresholdAmount"]);
            if (ColumnExists(reader, "ThresholdQty"))
                if (reader["ThresholdQty"] != DBNull.Value)
                    this.ThresholeQty = Convert.ToInt32(reader["ThresholdQty"]);
            if (ColumnExists(reader, "DiscountName"))
                if (reader["DiscountName"] != DBNull.Value)
                    this.DiscName = Convert.ToString(reader["DiscountName"]);
            if (ColumnExists(reader, "DiscTypeName"))
                if (reader["DiscTypeName"] != DBNull.Value)
                    this.DiscTypeName = Convert.ToString(reader["DiscTypeName"]);
            if (ColumnExists(reader, "CalcDisc"))
                if (reader["CalcDisc"] != DBNull.Value)
                    this.DiscCalcMethod = Convert.ToString(reader["CalcDisc"]);
            if (ColumnExists(reader, "DiscStatus"))
                if (reader["DiscStatus"] != DBNull.Value)
                    this.DiscStatus = Convert.ToBoolean(reader["DiscStatus"]);
        }

        #endregion

        #region Private Methods

        public bool ColumnExists(IDataReader reader, string columnName)
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                if (reader.GetName(i).ToUpper() == columnName.ToUpper())
                {
                    return true;
                }
            }

            return false;
        }
        #endregion
    }
}
