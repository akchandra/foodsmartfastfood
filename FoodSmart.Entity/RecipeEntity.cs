﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FoodSmart.Common;

namespace FoodSmart.Entity
{
    public class RecipeEntity : IRecipe
    {

        #region Constructors

        public RecipeEntity()
        {
            this.RecipeItems = new List<IRecipeItem>();
        }

        public RecipeEntity(DataTableReader reader)
        {
            this.RecipeItems = new List<IRecipeItem>();
            this.Id = Convert.ToInt32(reader["pk_RecipeID"]);
            this.Name = Convert.ToString(reader["RecipeName"]);
            this.Type = Convert.ToString(reader["RecipeType"]);
            this.NoofPlates = Convert.ToDecimal(reader["NoofPlates"]);
            if (reader["TotalQty"] != DBNull.Value)
                this.Quantity = Convert.ToDecimal(reader["TotalQty"]);

            //if (reader["TotalWeQty"] != DBNull.Value)
            //    this.TotalWeQty = Convert.ToDecimal(reader["TotalWeQty"]);

            this.UOM = Convert.ToString(reader["UOM"]);
            this.ProcessedAt = Convert.ToString(reader["ProcessedAt"]);

            if (reader["DateIntroduced"] != DBNull.Value)
                this.DateIntroduced = Convert.ToDateTime(reader["DateIntroduced"]);
            this.RecipeDetail = Convert.ToString(reader["RecipeDetail"]);
            //this.Rate = Convert.ToString(reader["ProcessedAt"]);
            //this.RatePerUnit = Convert.ToString(reader["ProcessedAt"]);
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public string ProcessedAt { get; set; }

        public decimal? Quantity { get; set; }

        public string Type { get; set; }

        public string UOM { get; set; }
        public decimal? Rate { get; set; }
        public string RatePerUnit { get; set; }
        public int? ItemId { get; set; }
        public decimal? NoofPlates { get; set; }
        public decimal TotalQuantity { get; set; }
        public DateTime? DateIntroduced { get; set; }
        public string RecipeDetail { get; set; }
        public bool Status { get; set; }
        public decimal? Amount { get; set; }
        public List<IRecipeItem> RecipeItems { get; set; }
        public decimal TotalWeQty { get; set; }
        #endregion
    }
}