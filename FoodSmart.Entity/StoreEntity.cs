﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FoodSmart.Common;
using System.Data.SqlClient;
using System.Data.Common;
namespace FoodSmart.Entity
{
    public class StoreEntity : IStore
    {
        #region IItemGroup Members

        public int StoreID { get; set; }
        public string StoreName { get; set; }
        public int RestID { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }

        #endregion

        #region IBase<int> Members

        public int Id
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        #endregion

        #region Constructors

        public StoreEntity()
        {

        }

        public StoreEntity(DbDataReader reader)
        {
            this.StoreID = Convert.ToInt32(reader["pk_StoreID"]);
            this.StoreName = Convert.ToString(reader["StoreName"]);
            this.RestID = Convert.ToInt32(reader["fk_RestID"]);
        }

        #endregion
    }
}
