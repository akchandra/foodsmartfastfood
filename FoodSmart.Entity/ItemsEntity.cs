﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FoodSmart.Common;
using System.Data;
using System.Data.SqlClient;

namespace FoodSmart.Entity
{
    [Serializable]
    public class ItemsEntity : IItems
    {
        public int ItemGroupID { get; set; }
        public int RestID { get; set; }
        public string RestName { get; set; }
        public bool GroupStatus { get; set; }
        public string GroupDescription { get; set; }
        public DateTime CurrDate { get; set; }
        public int ItemID { get; set; }
        public int LocID { get; set; }
        public string LocName { get; set; }
        public string ItemDescription { get; set; }
        public DateTime EffectiveDate { get; set; }
        public bool mrpItems { get; set; }
        public bool DiscountAllowed { get; set; }
        public string UOM { get; set; }
        public decimal CGSTPer { get; set; }
        public decimal SGSTPer { get; set; }
        public string HSNCode { get; set; }
        public int itemType { get; set; }
        public int ItemRateID { get; set; }
        public decimal SellingRate { get; set; }
        public decimal iSellingRate { get; set; }
        public int RateTypeID { get; set; }
        public int VehicleTypeID { get; set; }
        public int FirstSlabHrs { get; set; }
        public int DayTypeID { get; set; }
        public bool itemStatus { get; set; }
        public int ItemStockID { get; set; }
        public decimal Opqty { get; set; }
        public int finYrID { get; set; }
        public string RorF { get; set; }
        public string ProcessedAt { get; set; }
        public decimal MinStockLevel { get; set; }
        public decimal MaxStockLevel { get; set; }
        public decimal ReOrdStockLevel { get; set; }
        public string GroupType { get; set; }
        public string GroupName { get; set; }
        public int RMGroupID { get; set; }

        #region ICommon Members

        public int CreatedBy
        {
            get;
            set;
        }

        public DateTime CreatedOn
        {
            get;
            set;
        }

        public int ModifiedBy
        {
            get;
            set;
        }

        public DateTime ModifiedOn
        {
            get;
            set;
        }

        #endregion

        #region Constructors

        public ItemsEntity()
        {
            
        }

        public ItemsEntity(DataTableReader reader)
        {
            if (ColumnExists(reader, "pk_GROUPID"))
                if (reader["pk_GROUPID"] != DBNull.Value)
                    this.ItemGroupID = Convert.ToInt32(reader["pk_GROUPID"]);
            if (ColumnExists(reader, "RestName"))
                if (reader["RestName"] != DBNull.Value)
                    this.RestName = Convert.ToString(reader["RestName"]);
            if (ColumnExists(reader, "Descr"))
                if (reader["Descr"] != DBNull.Value)
                    this.GroupDescription = Convert.ToString(reader["Descr"]);
            if (ColumnExists(reader, "LocationName"))
                if (reader["LocationName"] != DBNull.Value)
                    this.LocName = Convert.ToString(reader["LocationName"]);
            if (ColumnExists(reader, "CurrDate"))
                if (reader["CurrDate"] != DBNull.Value)
                    this.CurrDate = Convert.ToDateTime(reader["CurrDate"]);
            if (ColumnExists(reader, "RestID"))
                if (reader["RestID"] != DBNull.Value)
                    this.RestID = Convert.ToInt32(reader["RestID"]);
            if (ColumnExists(reader, "LocID"))
                if (reader["LocID"] != DBNull.Value)
                    this.LocID = Convert.ToInt32(reader["LocID"]);
            if (ColumnExists(reader, "ItemID"))
                if (reader["ItemID"] != DBNull.Value)
                    this.ItemID = Convert.ToInt32(reader["ItemID"]);
            if (ColumnExists(reader, "ItemDescr"))
                if (reader["ItemDescr"] != DBNull.Value)
                    this.ItemDescription = Convert.ToString(reader["ItemDescr"]);
            if (ColumnExists(reader, "UOM"))
                if (reader["UOM"] != DBNull.Value)
                    this.UOM = Convert.ToString(reader["UOM"]);
            if (ColumnExists(reader, "EffectiveDate"))
                if (reader["EffectiveDate"] != DBNull.Value)
                    this.EffectiveDate = Convert.ToDateTime(reader["EffectiveDate"]);
            if (ColumnExists(reader, "mrpItems"))
                if (reader["mrpItems"] != DBNull.Value)
                    this.mrpItems = Convert.ToBoolean(reader["mrpItems"]);
            if (ColumnExists(reader, "DiscountAllowed"))
                if (reader["DiscountAllowed"] != DBNull.Value)
                    this.DiscountAllowed = Convert.ToBoolean(reader["DiscountAllowed"]);
            if (ColumnExists(reader, "SGSTPer"))
                if (reader["SGSTPer"] != DBNull.Value)
                    this.SGSTPer = Convert.ToDecimal(reader["SGSTPer"]);
            if (ColumnExists(reader, "CGSTPer"))
                if (reader["CGSTPer"] != DBNull.Value)
                    this.CGSTPer = Convert.ToDecimal(reader["CGSTPer"]);
            if (ColumnExists(reader, "HSNCode"))
                if (reader["HSNCode"] != DBNull.Value)
                    this.HSNCode = Convert.ToString(reader["HSNCode"]);
            if (ColumnExists(reader, "SellingRate"))
                if (reader["SellingRate"] != DBNull.Value)
                    this.SellingRate = Convert.ToDecimal(reader["SellingRate"]);
            if (ColumnExists(reader, "iSellingRate"))
                if (reader["iSellingRate"] != DBNull.Value)
                    this.iSellingRate = Convert.ToDecimal(reader["iSellingRate"]);
            if (ColumnExists(reader, "RateTypeID"))
                if (reader["RateTypeID"] != DBNull.Value)
                    this.RateTypeID = Convert.ToInt32(reader["RateTypeID"]);
            if (ColumnExists(reader, "UOM"))
                if (reader["UOM"] != DBNull.Value)
                    this.UOM = Convert.ToString(reader["UOM"]);
            if (ColumnExists(reader, "GroupStatus"))
                if (reader["GroupStatus"] != DBNull.Value)
                    this.GroupStatus = Convert.ToBoolean(reader["GroupStatus"]);
            if (ColumnExists(reader, "ItemStatus"))
                if (reader["ItemStatus"] != DBNull.Value)
                    this.itemStatus = Convert.ToBoolean(reader["ItemStatus"]);
            if (ColumnExists(reader, "VehicleTypeID"))
                if (reader["VehicleTypeID"] != DBNull.Value)
                    this.GroupStatus = Convert.ToBoolean(reader["VehicleTypeID"]);
            if (ColumnExists(reader, "FirstSlabHrs"))
                if (reader["FirstSlabHrs"] != DBNull.Value)
                    this.GroupStatus = Convert.ToBoolean(reader["FirstSlabHrs"]);
            if (ColumnExists(reader, "DayTypeID"))
                if (reader["DayTypeID"] != DBNull.Value)
                    this.GroupStatus = Convert.ToBoolean(reader["DayTypeID"]);
            if (ColumnExists(reader, "Opqty"))
                if (reader["Opqty"] != DBNull.Value)
                    this.Opqty = Convert.ToInt32(reader["Opqty"]);
            if (ColumnExists(reader, "pk_StockID"))
                if (reader["pk_StockID"] != DBNull.Value)
                    this.ItemStockID = Convert.ToInt32(reader["pk_StockID"]);
            if (ColumnExists(reader, "fk_FinYrID"))
                if (reader["fk_FinYrID"] != DBNull.Value)
                    this.finYrID = Convert.ToInt32(reader["fk_FinYrID"]);
            if (ColumnExists(reader, "RorF"))
                if (reader["RorF"] != DBNull.Value)
                    this.RorF = Convert.ToString(reader["RorF"]);
            if (ColumnExists(reader, "ProcessedAt"))
                if (reader["ProcessedAt"] != DBNull.Value)
                    this.ProcessedAt = Convert.ToString(reader["ProcessedAt"]);
            if (ColumnExists(reader, "MinOrderLevel"))
                if (reader["MinOrderLevel"] != DBNull.Value)
                    this.MinStockLevel = Convert.ToDecimal(reader["MinOrderLevel"]);
            if (ColumnExists(reader, "MaxOrderLevel"))
                if (reader["MaxOrderLevel"] != DBNull.Value)
                    this.MaxStockLevel = Convert.ToDecimal(reader["MaxOrderLevel"]);
            if (ColumnExists(reader, "ReOrderLevel"))
                if (reader["ReOrderLevel"] != DBNull.Value)
                    this.ReOrdStockLevel = Convert.ToDecimal(reader["ReOrderLevel"]);
            if (ColumnExists(reader, "GroupType"))
                if (reader["GroupType"] != DBNull.Value)
                    this.GroupType = Convert.ToString(reader["GroupType"]);
            if (ColumnExists(reader, "RMGroupName"))
                if (reader["RMGroupName"] != DBNull.Value)
                    this.GroupName = Convert.ToString(reader["RMGroupName"]);
            if (ColumnExists(reader, "pk_RMGroupID"))
                if (reader["pk_RMGroupID"] != DBNull.Value)
                    this.RMGroupID = Convert.ToInt32(reader["pk_RMGroupID"]);
        }

        #endregion

        #region Private Methods

        public bool ColumnExists(IDataReader reader, string columnName)
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                if (reader.GetName(i).ToUpper() == columnName.ToUpper())
                {
                    return true;
                }
            }

            return false;
        }

        #endregion
    }
}
