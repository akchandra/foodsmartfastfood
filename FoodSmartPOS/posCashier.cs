using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using System.Linq;
using System.Xml.Linq;
using Microsoft.CSharp;
using System.Drawing.Text;
using System.Data.OleDb;
using System.Configuration;
using System.Drawing.Drawing2D;

namespace DCardPOS
{
	public partial class posCashier:Form
	{
		DBUtility objUtility;
		BusUtility objBUtility;
		DataSet Ds;
		int g_retCode;
		int CardAction;
		string vData = "";
		string vData1 = "";
		string vData2 = "";
		TextBox activeTextBox;
		int fk_Cardid;
		BindingSource source1 = new BindingSource();
		//Protected Overrides Sub OnPaint(ByVal e As System.Windows.Forms.PaintEventArgs)
		//    Dim formGraphics As Graphics = e.Graphics
		//    Dim gradientBrush As New LinearGradientBrush(New Point(0, 0), New Point(Width, 0), Color.White, Color.LightGray)
		//    formGraphics.FillRectangle(gradientBrush, ClientRectangle)

		//End Sub

		protected override void OnResizeBegin(EventArgs e)
		{
			SuspendLayout();
			base.OnResizeBegin(e);
		}
		protected override void OnResizeEnd(EventArgs e)
		{
			ResumeLayout();
			base.OnResizeEnd(e);
		}

		public string NewRefillData {
			get { return vData; }
			set {
				vData = value;

				if (string.IsNullOrEmpty(value)) {
					txtNewRefill.Text = "0";
				} else {
					txtNewRefill.Text = value;
				}
			}
		}

		public string RefillData {
			get { return vData1; }
			set {
				vData1 = value;

				if (string.IsNullOrEmpty(value)) {
					txtRefillAmount.Text = "0";
				} else {
					txtRefillAmount.Text = value;
				}
			}
		}

		public string CardNoForHistoryData {
			get { return vData2; }
			set {
				vData2 = value;

				if (string.IsNullOrEmpty(value)) {
					txtCardNoHistory.Text = "";
				} else {
					txtCardNoHistory.Text = value;
				}
			}
		}


		private void btnNewCard_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			var btn = (Button)sender;
			var drawBrush = new SolidBrush(btn.ForeColor);
			var sf = new StringFormat {
				Alignment = StringAlignment.Center,
				LineAlignment = StringAlignment.Center
			};
			btnNewCard.Text = string.Empty;
			e.Graphics.DrawString("NEW CARD", btn.Font, drawBrush, e.ClipRectangle, sf);
			drawBrush.Dispose();
			sf.Dispose();

		}


		private void btnRefillCard_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			var btn = (Button)sender;
			var drawBrush = new SolidBrush(btn.ForeColor);
			var sf = new StringFormat {
				Alignment = StringAlignment.Center,
				LineAlignment = StringAlignment.Center
			};
			btnNewCard.Text = string.Empty;
			e.Graphics.DrawString("REFILL CARD", btn.Font, drawBrush, e.ClipRectangle, sf);
			drawBrush.Dispose();
			sf.Dispose();

		}


		private void btnRefundCard_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			var btn = (Button)sender;
			var drawBrush = new SolidBrush(btn.ForeColor);
			var sf = new StringFormat {
				Alignment = StringAlignment.Center,
				LineAlignment = StringAlignment.Center
			};
			btnNewCard.Text = string.Empty;
			e.Graphics.DrawString("REFUND CARD", btn.Font, drawBrush, e.ClipRectangle, sf);
			drawBrush.Dispose();
			sf.Dispose();

		}


		//Private Sub posCashier_Paint(ByVal sender As Object, ByVal e As PaintEventArgs) Handles MyBase.Paint
		//    Dim g As Graphics = e.Graphics
		//    Dim pen As New Pen(Color.Red, 2.0)
		//    For Each ctr As Control In Me.Controls
		//        If TypeOf ctr Is TextBox Then
		//            g.DrawRectangle(pen, New Rectangle(ctr.Location, ctr.Size))
		//        End If
		//    Next
		//    pen.Dispose()
		//End Sub

		private void posCashier_Load(System.Object sender, System.EventArgs e)
		{
			//SetResolution()
			//Global_module.UserName = "Tapas"
			lblUserName.Text = "WELCOME " + Global_module.UserName.ToUpper();
			EnableDisableControl(1);
			tmrUserCard.Enabled = true;
			lblCardNo.Text = "";
			lblSerial.Text = "";
			//dtCurrent.Value = Today
			dtCurrent.Enabled = false;
			lblSecDeposit.Text = "";
			lblUsableBalance.Text = "";

			txtCardNoHistory.Visible = false;
			cmbCardSerialHistory.Visible = false;

			lblMessage.Visible = false;
			imgCloseReport.Visible = false;

			txtNewRefill.Enabled = false;
			txtNewSecurityDep.Enabled = false;
			txtNewUsableBalance.Enabled = false;

			txtRefillCardBalance.Enabled = false;
			txtRefillAmount.Enabled = false;
			txtRefillFinalBalance.Enabled = false;

			txtRefundBalance.Enabled = false;

			txtNewCardNo.Enabled = false;
			txtRefillCardNo.Enabled = false;
			txtRefundCardNo.Enabled = false;

			btnNewCard.Enabled = false;
			btnRefillCard.Enabled = false;
			btnRefundCard.Enabled = false;
			objUtility = new DBUtility();
			Ds = objUtility.GetCurrDate();
			dtCurrent.Value = Ds.Tables[0].Rows[0]["DT"];
			objUtility = new DBUtility();
			Ds = objUtility.GetSecurityDeposit();
			Global_module.SecurtyDepositAmount = Ds.Tables[0].Rows[0]["SecurityDeposit"];
			ControlBox = false;
			Initialize();

		}

		private void EnableDisableControl(int Type)
		{
			if (Type == 1) {
				btnNewCard.BackColor = Color.Gray;
				btnRefillCard.BackColor = Color.Gray;
				btnRefundCard.BackColor = Color.Gray;
				gbNewCard.Enabled = false;
				gbRefillCard.Enabled = false;
				gbRefundCard.Enabled = false;
				gbRupees.Enabled = false;
				gbNumbers.Enabled = false;
				btnCash.Enabled = false;
				btnDiscard.Enabled = false;
				CardAction = 0;
			} else {
				btnCash.Enabled = true;
				btnDiscard.Enabled = true;
				gbRupees.Enabled = true;
				gbNumbers.Enabled = true;
				if (CardAction == 1) {
					gbNewCard.Enabled = true;

				}
			}
			gbDisplay.Enabled = false;

		}

		private void Initialize()
		{
			txtNewCardNo.Text = "";
			txtNewRefill.Text = "";
			txtNewSecurityDep.Text = "";
			txtNewUsableBalance.Text = "";
			txtRefillAmount.Text = "";
			txtRefillCardNo.Text = "";
			txtRefillFinalBalance.Text = "";
			txtRefillCardBalance.Text = "";
			txtRefundBalance.Text = "";
			txtRefundCardNo.Text = "";
			lblInternalNo.Text = "";
			lblCardNo.Text = "";
			lblSerial.Text = "";
			lblUsableBalance.Text = "";
			lblSecDeposit.Text = "";
		}

		bool static_tmrUserCard_Tick_bDummy;
		private void tmrUserCard_Tick(System.Object sender, System.EventArgs e)
		{
			string[] txtData = new string[4];
			byte[] TagType = new byte[51];
			int ctr = 0;
			int dwoc = 0;
			byte[] ResultSN = new byte[11];
			byte ResultTag = 0x0;
			BusUtility.CardExists Data = BusUtility.SelectCard();
			if (Data.CardReaderPresent) {
				if (Data.CardPresent) {
					lblInternalNo.Text = Data.CardNo;
					objBUtility = new BusUtility();
					dwoc = objBUtility.DataReadFromCard(g_retCode, 3, ref txtData[0], ref txtData[1], ref txtData[2]);
					if (txtData[0] == "R") {
						lblCardNo.Text = txtData[1];
						//tmrUserCard.Enabled = False
						objUtility = new DBUtility();
						Ds = objUtility.GetCardBalance(lblInternalNo.Text, 0, "I");
						fk_Cardid = Ds.Tables[0].Rows[0]["CardID"];
						lblSerial.Text = Ds.Tables[0].Rows[0]["CurrentSerial"];

						if (Ds.Tables[0].Rows[0]["CardLocked"] == true) {
							Label18.Visible = true;
							btnRefillCard.Enabled = false;
							btnRefundCard.Enabled = false;
							lblUsableBalance.Text = Ds.Tables[0].Rows[0]["CARDBALANCE"];
							lblSecDeposit.Text = Ds.Tables[0].Rows[0]["SECURITYDEPOSIT"];
							return;
						} else {
							Label18.Visible = false;
						}

						if ((Ds.Tables[0].Rows[0]["Active"] == 2 | Ds.Tables[0].Rows[0]["Active"] == 3)) {
							static_tmrUserCard_Tick_bDummy = !static_tmrUserCard_Tick_bDummy;
							if (static_tmrUserCard_Tick_bDummy) {
								Label17.BackColor = Color.Yellow;
							} else {
								Label17.BackColor = Color.Green;
							}
							Label17.Visible = true;
						} else {
							Label17.Visible = false;
						}

						if ((Ds.Tables[0].Rows[0]["Active"] == 0 | Ds.Tables[0].Rows[0]["Active"] == 2) & Ds.Tables[0].Rows[0]["SecurityDeposit"] == 0) {
							btnNewCard.BackColor = Color.MediumBlue;
							CardAction = 1;
							ActInacScreen(CardAction);
							txtNewSecurityDep.BackColor = SystemColors.Window;
							EnableReportButton(false);

						} else if (Ds.Tables[0].Rows[0]["Active"] == 1 | Ds.Tables[0].Rows[0]["Active"] == 3) {
							if (CardAction != 2 & CardAction != 3) {
								lblSecDeposit.Text = Ds.Tables[0].Rows[0]["SecurityDeposit"];
								lblUsableBalance.Text = Ds.Tables[0].Rows[0]["CARDBALANCE"];
								btnRefillCard.BackColor = Color.SaddleBrown;
								btnRefundCard.BackColor = Color.Red;
								btnRefillCard.Enabled = true;
								btnRefundCard.Enabled = true;
								btnDiscard.Enabled = true;
							}
						} else {
							CardAction = 0;
							tmrUserCard.Enabled = false;
							MessageBox.Show("Card Not activated at BURP. Remove Guest Card", "Remove", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							tmrUserCard.Enabled = true;
							return;
						}

						if (Ds.Tables[0].Rows[0]["TempLock"] == 1) {
							MessageBox.Show("Card Locked from Other terminal. Unlocking and Updating Balance", "Remove", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							objBUtility = new BusUtility();
							dwoc = objBUtility.DataReadFromCard(g_retCode, 6, ref txtData[0], ref txtData[1], ref txtData[2]);
							objBUtility.DataWrittenOnCard(Data.CardHandle, 6, Ds.Tables[0].Rows[0]["CardBalance"], txtData[1], txtData[2]);

						}

						if (Ds.Tables[0].Rows[0]["CardLocked"] == 1) {
							MessageBox.Show("Card Locked. No Transaction Possible", "Remove Card", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							return;
						}

						//Global_module.UserName = Convert.ToString(Ds.Tables(0).Rows(0).Item("UserName"))
						//EnableDisableControl(2)

						txtNewRefill.Focus();
						// load cashier module
						//Me.Hide()
					} else {
						//If txtData(0) = "C" Then
						//    tmrUserCard.Enabled = False
						//    MessageBox.Show("Please Remove Cashier Card", "Remove", MessageBoxButtons.OK, MessageBoxIcon.Hand)
						//    tmrUserCard.Enabled = True
						//Else
						//    tmrUserCard.Enabled = False
						//    MessageBox.Show("Only Guest Card Allowed", "Login Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
						//    tmrUserCard.Enabled = True
						//End If
						Initialize();
						EnableDisableControl(1);
						EnableReportButton(true);

					}
				} else {
					Initialize();
					EnableDisableControl(1);
					EnableReportButton(1);
					DataGridView1.DataSource = null;
					Label17.Visible = false;
					Label18.Visible = false;
				}
			} else {
				MessageBox.Show("Card Reader Disconnected. No Operation possible", "Reader Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				System.Environment.Exit(0);
			}
		}

		private void ActInacScreen(int Action)
		{
			if (Action == 1) {
				gbNewCard.Enabled = true;
				txtNewCardNo.Text = Ds.Tables[0].Rows[0]["CardNoOutside"];
				txtNewRefill.Enabled = true;
				gbRupees.Enabled = true;
				gbNumbers.Enabled = true;
				//txtNewRefill.ReadOnly = True
			} else if (Action == 2) {
				gbRefillCard.Enabled = true;
				txtRefillCardNo.Text = Ds.Tables[0].Rows[0]["CardNoOutside"];
				txtRefillAmount.Enabled = true;
				gbRupees.Enabled = true;
				gbNumbers.Enabled = true;
				//txtRefillAmount.ReadOnly = True
			} else if (Action == 3) {
				gbRefundCard.Enabled = true;
				txtRefundCardNo.Text = Ds.Tables[0].Rows[0]["CardNoOutside"];
				txtRefundBalance.Enabled = true;
				gbRupees.Enabled = false;
				gbNumbers.Enabled = false;
				//txtRefundBalance.ReadOnly = True
			}

			btnCash.Enabled = true;
			btnDiscard.Enabled = true;
		}

		private void btnDiscard_Click(System.Object sender, System.EventArgs e)
		{
			MessageBox.Show("Please Remove Guest Card", "Remove", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			Initialize();
			EnableDisableControl(1);
			tmrUserCard.Enabled = true;
			objUtility = new DBUtility();
			RefillData = "";
			NewRefillData = "";
			Label17.Visible = false;
			EnableReportButton(true);
			//objBUtility = New BusUtility
			//objBUtility.writetolpt1()
		}

		private void btnCash_Click(System.Object sender, System.EventArgs e)
		{
			int Retval = 0;

			BusUtility.CardExists Data = BusUtility.SelectCard();
			string InternalNo = null;
			InternalNo = Data.CardNo;
			Label17.Visible = false;
			if (CheckCard("R", lblInternalNo.Text)) {
				tmrUserCard.Enabled = false;
				if (CardAction == 1) {
					objBUtility = new BusUtility();
					if (objBUtility.DataWrittenOnCard(Data.CardHandle, 6, txtNewUsableBalance.Text, txtNewRefill.Text, txtNewSecurityDep.Text)) {
						Retval = objUtility.SaveCashierTran(CardAction, lblInternalNo.Text, "A", fk_Cardid, txtNewRefill.Text, "C");

						if (Global_module.PrinterType == 0) {
							objBUtility.PrintCashSlipSerial(Retval, 1, 0);
						} else {
							objBUtility.PrintCashSlipUSB(Retval, 1, 0);
						}
						MessageBox.Show("Data Saved. Remove Guest Card", "Remove Card", MessageBoxButtons.OK, MessageBoxIcon.Information);
					} else {
						MessageBox.Show("Problem writing Card. Amount not updated", "Remove Card", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					Initialize();
					EnableDisableControl(1);
					tmrUserCard.Enabled = true;
				} else if (CardAction == 2) {
					objBUtility = new BusUtility();

					if (objBUtility.DataWrittenOnCard(Data.CardHandle, 6, txtRefillFinalBalance.Text, txtRefillAmount.Text, lblSecDeposit.Text)) {
						Retval = objUtility.SaveCashierTran(CardAction, lblInternalNo.Text, "R", fk_Cardid, txtRefillAmount.Text, "C");
						if (Global_module.PrinterType == 0) {
							objBUtility.PrintCashSlipSerial(Retval, 2, Convert.ToDecimal(lblUsableBalance.Text));
						} else {
							objBUtility.PrintCashSlipUSB(Retval, 2, Convert.ToDecimal(lblUsableBalance.Text));
						}

						MessageBox.Show("Data Saved. Remove Guest Card", "Remove Card", MessageBoxButtons.OK, MessageBoxIcon.Information);
					} else {
						MessageBox.Show("Problem writing Card. Amount not updated", "Remove Card", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					Initialize();
					EnableDisableControl(1);
					tmrUserCard.Enabled = true;
				} else if (CardAction == 3) {
					objBUtility = new BusUtility();
					if (objBUtility.DataWrittenOnCard(Data.CardHandle, 6, txtRefundBalance.Text, 0, 0)) {
						Retval = objUtility.SaveCashierTran(CardAction, lblInternalNo.Text, "U", fk_Cardid, txtRefundBalance.Text, "C");
						if (Global_module.PrinterType == 0) {
							objBUtility.PrintCashSlipSerial(Retval, 3, 0);
						} else {
							objBUtility.PrintCashSlipUSB(Retval, 3, 0);
							//objBUtility.PrintCashSlipUSB(Retval, 3, Convert.ToDecimal(lblUsableBalance.Text))
						}

						if (objUtility.ChkForParkingMarker(lblInternalNo.Text)) {
							//Dim SplMsg As New frmMsgBox
							//SplMsg.ShowDialog()
							My.MyProject.Forms.splDialog.ShowDialog();
							//MessageBox.Show("Data Saved. Card No." + txtRefundCardNo.Text + " used in Parking. Return Card to Customer", "Remove Card", MessageBoxButtons.OK, MessageBoxIcon.Warning)
						} else {
							MessageBox.Show("Data Saved. Remove Guest Card", "Remove Card", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
					} else {
						MessageBox.Show("Problem writing Card. Amount not updated", "Remove Card", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					Initialize();
					EnableDisableControl(1);
					tmrUserCard.Enabled = true;
					EnableReportButton(true);
				}

			} else {
				MessageBox.Show("Card changed. Operation Aborted. Remove Card.", "Abort", MessageBoxButtons.OK, MessageBoxIcon.Error);
				Initialize();
				EnableDisableControl(1);
				EnableReportButton(true);
				tmrUserCard.Enabled = true;
			}
			RefillData = "";
			NewRefillData = "";

			//If InternalNo <> lblInternalNo.Text Then
			//    MessageBox.Show("Card changed. Operation Aborted. Remove Card.", "Abort", MessageBoxButtons.OK, MessageBoxIcon.Error)
			//    Initialize()
			//    EnableDisableControl(1)
			//    tmrUserCard.Enabled = True
			//Else

			//    Retval = objUtility.SaveCashierTran(CardAction, fk_Cardid, txtNewRefill.Text, 'C', Global_module.UserID)
			//    MessageBox.Show("Data Saved. Remove Guest Card", "Remove Card", MessageBoxButtons.OK, MessageBoxIcon.Information)
			//    Initialize()
			//    EnableDisableControl(1)
			//    tmrUserCard.Enabled = True
			//End If
		}
		private void btnExit_Click(System.Object sender, System.EventArgs e)
		{
			int Reply = 0;
			Reply = MessageBox.Show("Want to Leave Cashier Operation", "Exit Cashier", MessageBoxButtons.YesNo, MessageBoxIcon.Hand);
			if (Reply == Constants.vbYes) {
				System.Environment.Exit(0);
			}
		}

		public void NumericMouseUp(object sender, EventArgs e)
		{
			if (activeTextBox.Name == "txtNewRefill") {
				NewRefillData += sender.tag;
			} else if (activeTextBox.Name == "txtRefillAmount") {
				RefillData += sender.tag;
			} else if (activeTextBox.Name == "txtCardNoHistory") {
				CardNoForHistoryData += sender.tag;

			} else {
			}
		}

		private void RupeeMouseUp(System.Object sender, System.EventArgs e)
		{
			if (activeTextBox.Name == "txtNewRefill") {
				NewRefillData = sender.tag;
			} else if (activeTextBox.Name == "txtRefillAmount") {
				RefillData = sender.tag;
			}

		}
		//Private Sub btnback_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnback.MouseUp
		//    If activeTextBox.Name = "txtNewRefill" Then
		//        NewRefillData = ""
		//    Else
		//        Data1 = ""
		//    End If
		//End Sub
		private void TextBox_Enter(System.Object sender, System.EventArgs e)
		{
			activeTextBox = (TextBox)sender;
		}

		private void btnback_MouseUp1(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			if (activeTextBox.Name == "txtNewRefill") {
				if (NewRefillData.Length == 0) {
					NewRefillData = "";
				} else {
					NewRefillData = NewRefillData.Substring(0, NewRefillData.Length - 1);
				}
			} else if (activeTextBox.Name == "txtRefillAmount") {
				if (RefillData.Length == 0) {
					RefillData = "";
				} else {
					RefillData = RefillData.Substring(0, RefillData.Length - 1);
				}
			} else if (activeTextBox.Name == "txtCardNoHistory") {
				if (CardNoForHistoryData.Length == 0) {
					CardNoForHistoryData = "";
				} else {
					CardNoForHistoryData = CardNoForHistoryData.Substring(0, RefillData.Length - 1);
				}
			}
		}

		private void txtNewRefill_TextChanged(System.Object sender, System.EventArgs e)
		{
			if (Conversion.Val(txtNewRefill.Text) > Global_module.SecurtyDepositAmount) {
				txtNewSecurityDep.Text = Global_module.SecurtyDepositAmount;
				txtNewUsableBalance.Text = Conversion.Val(txtNewRefill.Text) - Global_module.SecurtyDepositAmount;
			} else {
				txtNewSecurityDep.Text = "";
				txtNewUsableBalance.Text = "";
			}
		}

		public bool CheckCard(string CardType, string CardNoInternal)
		{
			bool functionReturnValue = false;
			string[] txtData = new string[4];
			int RetVal = 0;

			BusUtility.CardExists Data = BusUtility.SelectCard();
			if (Data.CardReaderPresent == false) {
				MessageBox.Show("Card Reader Not Connected", "Reader Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);

				return false;
				return functionReturnValue;
			}
			if (Data.CardPresent == false) {
				MessageBox.Show("Card Not present on the Reader", "Reader Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);

				return false;
				return functionReturnValue;
			}
			objBUtility = new BusUtility();
			RetVal = objBUtility.DataReadFromCard(Data.CardHandle, 3, ref txtData[0], ref txtData[1], ref txtData[2]);
			if (txtData[0] != "R" | Data.CardNo != lblInternalNo.Text) {
				return false;
			}
			return true;
			return functionReturnValue;
		}

		private void btnRefillCard_Click(System.Object sender, System.EventArgs e)
		{
			string[] txtData = new string[4];
			byte[] TagType = new byte[51];
			int ctr = 0;
			int dwoc = 0;
			byte[] ResultSN = new byte[11];
			byte ResultTag = 0x0;
			BusUtility.CardExists Data = BusUtility.SelectCard();
			btnRefundCard.Enabled = false;
			btnRefundCard.BackColor = Color.Gray;
			EnableReportButton(false);
			if (Data.CardReaderPresent & Data.CardPresent) {

				lblInternalNo.Text = Data.CardNo;
				objBUtility = new BusUtility();
				dwoc = objBUtility.DataReadFromCard(g_retCode, 3, ref txtData[0], ref txtData[1], ref txtData[2]);
				if (txtData[0] == "R") {
					lblCardNo.Text = txtData[1];
					tmrUserCard.Enabled = true;
					//dwoc = objBUtility.DataReadFromCard(g_retCode, 4, txtData(0), txtData(1), txtData(2))
					//Global_module.UserName = txtData(0)
					objUtility = new DBUtility();
					Ds = objUtility.GetCardBalance(lblInternalNo.Text, 0, "I");
					fk_Cardid = Ds.Tables[0].Rows[0]["CardID"];
					txtRefillCardBalance.Text = Ds.Tables[0].Rows[0]["CARDBALANCE"];
					lblSerial.Text = Ds.Tables[0].Rows[0]["CurrentSerial"];
					if ((Ds.Tables[0].Rows[0]["Active"] == 1 | Ds.Tables[0].Rows[0]["Active"] == 3)) {
						CardAction = 2;
						btnRefundCard.BackColor = Color.Gray;
						ActInacScreen(CardAction);
					} else {
						CardAction = 0;
						MessageBox.Show("Card Not activated at BURP. Remove Guest Card", "Remove", MessageBoxButtons.OK, MessageBoxIcon.Error);
						tmrUserCard.Enabled = true;
						return;
					}

					if (Ds.Tables[0].Rows[0]["TempLock"] == 1) {
						MessageBox.Show("Card Locked from Other terminal. Unlocking and Updating Balance", "Remove", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						objBUtility = new BusUtility();
						dwoc = objBUtility.DataReadFromCard(g_retCode, 6, ref txtData[0], ref txtData[1], ref txtData[2]);
						objBUtility.DataWrittenOnCard(Data.CardHandle, 6, Ds.Tables[0].Rows[0]["CardBalance"], txtData[1], txtData[2]);

					}

					//Global_module.UserName = Convert.ToString(Ds.Tables(0).Rows(0).Item("UserName"))
					//EnableDisableControl(2)

					txtRefillAmount.Focus();
					// load cashier module
					//Me.Hide()
				} else {
					tmrUserCard.Enabled = false;
					MessageBox.Show("Only Guest Card Allowed", "Login Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
					tmrUserCard.Enabled = true;
				}
			}
		}

		private void txtRefillAmount_TextChanged(System.Object sender, System.EventArgs e)
		{
			txtRefillFinalBalance.Text = Conversion.Val(txtRefillAmount.Text) + Conversion.Val(txtRefillCardBalance.Text);
		}

		private void btnRefundCard_Click(System.Object sender, System.EventArgs e)
		{
			string[] txtData = new string[4];
			byte[] TagType = new byte[51];
			int ctr = 0;
			int dwoc = 0;
			byte[] ResultSN = new byte[11];
			byte ResultTag = 0x0;
			BusUtility.CardExists Data = BusUtility.SelectCard();
			btnRefundCard.Enabled = false;
			btnRefundCard.BackColor = Color.Gray;
			btnCashierReport.Enabled = false;
			if (Data.CardReaderPresent & Data.CardPresent) {
				lblInternalNo.Text = Data.CardNo;
				objBUtility = new BusUtility();
				dwoc = objBUtility.DataReadFromCard(g_retCode, 3, ref txtData[0], ref txtData[1], ref txtData[2]);
				if (txtData[0] == "R") {
					lblCardNo.Text = txtData[1];
					tmrUserCard.Enabled = true;
					//dwoc = objBUtility.DataReadFromCard(g_retCode, 4, txtData(0), txtData(1), txtData(2))
					//Global_module.UserName = txtData(0)
					objUtility = new DBUtility();
					Ds = objUtility.GetCardBalance(lblInternalNo.Text, 0, "I");
					fk_Cardid = Ds.Tables[0].Rows[0]["CardID"];
					txtRefundBalance.Text = Ds.Tables[0].Rows[0]["CARDBALANCE"] + Ds.Tables[0].Rows[0]["SECURITYDEPOSIT"];
					lblSerial.Text = Ds.Tables[0].Rows[0]["CurrentSerial"];
					if ((Ds.Tables[0].Rows[0]["Active"] == 1 | Ds.Tables[0].Rows[0]["Active"] == 3)) {
						CardAction = 3;
						btnRefillCard.BackColor = Color.Gray;
						ActInacScreen(CardAction);
					} else {
						CardAction = 0;
						MessageBox.Show("Card Not activated at BURP. Remove Guest Card", "Remove", MessageBoxButtons.OK, MessageBoxIcon.Error);
						tmrUserCard.Enabled = true;
						return;
					}

					if (Ds.Tables[0].Rows[0]["TempLock"] == 1) {
						MessageBox.Show("Card Locked from Other terminal. Unlocking and Updating Balance", "Remove", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						objBUtility = new BusUtility();
						dwoc = objBUtility.DataReadFromCard(g_retCode, 6, ref txtData[0], ref txtData[1], ref txtData[2]);
						objBUtility.DataWrittenOnCard(Data.CardHandle, 6, Ds.Tables[0].Rows[0]["CardBalance"], txtData[1], txtData[2]);

					}

					//Global_module.UserName = Convert.ToString(Ds.Tables(0).Rows(0).Item("UserName"))
					//EnableDisableControl(2)

					txtRefillAmount.Focus();
					// load cashier module
					//Me.Hide()
				} else {
					tmrUserCard.Enabled = false;
					MessageBox.Show("Only Guest Card Allowed", "Login Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
					tmrUserCard.Enabled = true;
				}
			}
		}

		private void CreateGrid(string CardNoInside, int Serial)
		{
			Ds = new DataSet();
			objUtility = new DBUtility();
			Ds = objUtility.CardHistory(CardNoInside, Serial);
			DataGridView1.DataSource = Ds.Tables["CardTran"];
			DataTableCollection tables = Ds.Tables;
			DataView view1 = new DataView(tables[0]);
			view1 = new DataView(tables[0]);
			source1 = new BindingSource();
			source1.DataSource = view1;

			DataGridView1.DataSource = source1;

			DataGridView1.Columns[0].HeaderText = "Type";
			DataGridView1.Columns[1].HeaderText = "Date";
			DataGridView1.Columns[2].HeaderText = "Restaurant Name";
			DataGridView1.Columns[3].HeaderText = "Amount";

			DataGridView1.Columns[0].Width = 60;
			DataGridView1.Columns[1].Width = 90;
			DataGridView1.Columns[2].Width = 250;
			DataGridView1.Columns[3].Width = 80;

			DataGridView1.Columns[1].DefaultCellStyle.Format = "dd-MM-yy hh:mm";
			foreach (DataGridViewColumn c in DataGridView1.Columns) {
				c.ReadOnly = true;
				c.Resizable = DataGridViewTriState.False;
			}

			DataGridView1.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
			DataGridView1.Columns[3].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
			DataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
			DataGridView1.AllowUserToAddRows = false;
		}

		private void btnCardHistory_Click(System.Object sender, System.EventArgs e)
		{
			if (string.IsNullOrEmpty(lblInternalNo.Text)) {
				txtCardNoHistory.Visible = true;
				cmbCardSerialHistory.Visible = true;
				lblMessage.Visible = true;
				tmrUserCard.Enabled = false;
				imgCloseReport.Visible = true;
				gbDisplay.Enabled = true;
				gbNumbers.Enabled = true;
				gbDisplay.Focus();
				txtCardNoHistory.Enabled = true;
				cmbCardSerialHistory.Enabled = false;
				txtCardNoHistory.Focus();
			} else {
				imgCloseReport.Visible = true;
				CreateGrid(lblInternalNo.Text, Conversion.Val(lblSerial.Text));
			}

		}

		private void imgCloseReport_Click(System.Object sender, System.EventArgs e)
		{
			//If lblInternalNo.Text = "" Then
			Label16.Text = "SECURITY DEPOSIT :";
			txtCardNoHistory.Visible = false;
			cmbCardSerialHistory.Visible = false;
			tmrUserCard.Enabled = true;
			lblMessage.Visible = false;
			tmrUserCard.Enabled = true;
			imgCloseReport.Visible = false;
			gbNumbers.Enabled = true;
			gbDisplay.Enabled = false;
			DataGridView1.DataSource = null;
			//End If
			DataGridView1.DataSource = null;
		}


		private void NumericMouseUp(System.Object sender, System.Windows.Forms.MouseEventArgs e)
		{
		}


		private void RupeeMouseUp(System.Object sender, System.Windows.Forms.MouseEventArgs e)
		{
		}

		private void txtCardNoHistory_TextChanged(System.Object sender, System.EventArgs e)
		{
			if (Strings.Len(txtCardNoHistory.Text) == 6) {
				FillCardSerial(txtCardNoHistory.Text);
				cmbCardSerialHistory.Enabled = true;
				cmbCardSerialHistory.SelectedIndex = 0;
			}
			//Ds = objUtility.GetCardBalance(txtCardNoHistory.Text, "O")
			//If Ds.Tables(0).Rows().Count = 0 Then
			//    MessageBox.Show("Invalid Card No", "Card No Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
			//    txtCardNoHistory.Text = ""
			//    vData2 = ""
			//    txtCardNoHistory.Focus()
			//    Exit Sub
			//End If
			//lblUsableBalance.Text = Ds.Tables(0).Rows(0).Item("CARDBALANCE")
			//lblSecDeposit.Text = Ds.Tables(0).Rows(0).Item("SecurityDeposit")
			//lblInternalNo.Text = Ds.Tables(0).Rows(0).Item("CardNoInside")
			//CreateGrid(lblInternalNo.Text)

			//End If
		}

		private void FillCardSerial(string ct)
		{
			Ds = new DataSet();
			objUtility = new DBUtility();
			Ds = objUtility.FillCardSerial(ct);
			cmbCardSerialHistory.DataSource = Ds.Tables["mst_CardSerial"];
			cmbCardSerialHistory.DisplayMember = "Serial";
			cmbCardSerialHistory.ValueMember = "Serial";
			cmbCardSerialHistory.SelectedValue = 0;
		}
		private void CreateGridCashier(int UserID)
		{
			Ds = new DataSet();
			objUtility = new DBUtility();
			Ds = objUtility.CashierBalance(UserID);
			DataGridView1.DataSource = Ds.Tables["CashierTran"];
			DataTableCollection tables = Ds.Tables;
			DataView view1 = new DataView(tables[0]);
			view1 = new DataView(tables[0]);
			source1 = new BindingSource();
			source1.DataSource = view1;

			DataGridView1.DataSource = source1;

			DataGridView1.Columns[0].HeaderText = "Tran Type";
			DataGridView1.Columns[1].HeaderText = "Time";
			DataGridView1.Columns[2].HeaderText = "Card No";
			DataGridView1.Columns[3].HeaderText = "Serial";
			DataGridView1.Columns[4].HeaderText = "Amount";

			DataGridView1.Columns[0].Width = 80;
			DataGridView1.Columns[1].Width = 70;
			DataGridView1.Columns[2].Width = 100;
			DataGridView1.Columns[3].Width = 100;
			DataGridView1.Columns[4].Width = 100;

			DataGridView1.Columns[1].DefaultCellStyle.Format = "hh:mm";
			DataGridView1.Columns[4].DefaultCellStyle.Format = "#########0.00";

			foreach (DataGridViewColumn c in DataGridView1.Columns) {
				c.ReadOnly = true;
				c.Resizable = DataGridViewTriState.False;
			}

			DataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
			DataGridView1.AllowUserToAddRows = false;

			Label16.Text = "CASHIER BALANCE:";
			//lblSecDeposit.Visible = True
			CalcCashierBalance();

		}

		private void CalcCashierBalance()
		{
			lblSecDeposit.Text = "";
			for (int j = 0; j <= DataGridView1.RowCount - 1; j++) {
				if (DataGridView1.Rows[j].Cells["TRANTYPE"].Value == "ADDED") {
					lblSecDeposit.Text = Conversion.Val(lblSecDeposit.Text) + DataGridView1.Rows[j].Cells["Tranamount"].Value;
				}
				if (DataGridView1.Rows[j].Cells["TRANTYPE"].Value == "REFILL") {
					lblSecDeposit.Text = Conversion.Val(lblSecDeposit.Text) + DataGridView1.Rows[j].Cells["Tranamount"].Value;
				}
				if (DataGridView1.Rows[j].Cells["TRANTYPE"].Value == "REFUND") {
					lblSecDeposit.Text = Conversion.Val(lblSecDeposit.Text) - DataGridView1.Rows[j].Cells["Tranamount"].Value;
				}

			}
		}
		private void btnCashierReport_Click(System.Object sender, System.EventArgs e)
		{
			tmrUserCard.Enabled = false;
			imgCloseReport.Visible = true;
			CreateGridCashier(Global_module.UserID);
		}

		private void EnableReportButton(bool stat)
		{
			if (stat == true) {
				btnCardHistory.Enabled = true;
				btnCashierReport.Enabled = true;
			} else {
				btnCardHistory.Enabled = false;
				btnCashierReport.Enabled = false;
			}
		}

		private void cmbCardSerialHistory_SelectedIndexChanged(System.Object sender, System.EventArgs e)
		{
			Ds = objUtility.GetCardBalance(txtCardNoHistory.Text, Conversion.Val(cmbCardSerialHistory.Text), "O");
			if (Ds.Tables[0].Rows().Count() == 0) {
				MessageBox.Show("Invalid Card No", "Card No Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				txtCardNoHistory.Text = "";
				vData2 = "";
				txtCardNoHistory.Focus();
				return;
			}
			if (cmbCardSerialHistory.Items.Count() == Conversion.Val(cmbCardSerialHistory.Text)) {
				lblUsableBalance.Text = Ds.Tables[0].Rows[0]["CARDBALANCE"];
				lblSecDeposit.Text = Ds.Tables[0].Rows[0]["SecurityDeposit"];
			} else {
				lblUsableBalance.Text = "";
				lblSecDeposit.Text = "";
			}
			lblInternalNo.Text = Ds.Tables[0].Rows[0]["CardNoInside"];
			CreateGrid(lblInternalNo.Text, Conversion.Val(cmbCardSerialHistory.Text));
		}

		private void Button1_Click(System.Object sender, System.EventArgs e)
		{
			if (Global_module.PrinterType == 0) {
				objBUtility.PrintCashierSummarySerial(Global_module.UserID);
			} else {
				objBUtility.PrintCashierSummaryUSB(Global_module.UserID);
			}
		}

		private void DataGridView1_RowPostPaint(object sender, System.Windows.Forms.DataGridViewRowPostPaintEventArgs e)
		{
			if (e.RowIndex < DataGridView1.RowCount - 1) {
				DataGridViewRow dgvRow = this.DataGridView1.Rows[e.RowIndex];
				if (dgvRow.Cells["TranType"].Value.ToString().ToUpper() == "REFUND") {
					dgvRow.DefaultCellStyle.ForeColor = Color.Red;
				} else {
					dgvRow.DefaultCellStyle.ForeColor = Color.Black;
				}
			}
		}
		public posCashier()
		{
			Load += posCashier_Load;
			InitializeComponent();
		}
	}
}
