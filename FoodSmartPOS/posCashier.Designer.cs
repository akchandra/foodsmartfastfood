namespace DCardPOS
{
	partial class posCashier
	{

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(posCashier));
			this.btnCash = new System.Windows.Forms.Button();
			this.btnDiscard = new System.Windows.Forms.Button();
			this.btnExit = new System.Windows.Forms.Button();
			this.lblUserName = new System.Windows.Forms.Label();
			this.tmrUserCard = new System.Windows.Forms.Timer(this.components);
			this.dtCurrent = new System.Windows.Forms.DateTimePicker();
			this.btnNewCard = new System.Windows.Forms.Button();
			this.btnRefillCard = new System.Windows.Forms.Button();
			this.btnRefundCard = new System.Windows.Forms.Button();
			this.GroupBox1 = new System.Windows.Forms.GroupBox();
			this.Button1 = new System.Windows.Forms.Button();
			this.imgCloseReport = new System.Windows.Forms.PictureBox();
			this.btnCashierReport = new System.Windows.Forms.Button();
			this.btnCardHistory = new System.Windows.Forms.Button();
			this.DataGridView1 = new System.Windows.Forms.DataGridView();
			this.txtCardNoHistory = new System.Windows.Forms.TextBox();
			this.Label8 = new System.Windows.Forms.Label();
			this.lblCardNo = new System.Windows.Forms.Label();
			this.Label15 = new System.Windows.Forms.Label();
			this.Label16 = new System.Windows.Forms.Label();
			this.lblUsableBalance = new System.Windows.Forms.Label();
			this.lblSecDeposit = new System.Windows.Forms.Label();
			this.lblMessage = new System.Windows.Forms.Label();
			this.lblprivateNo = new System.Windows.Forms.Label();
			this.Label17 = new System.Windows.Forms.Label();
			this.gbDisplay = new System.Windows.Forms.GroupBox();
			this.cmbCardSerialHistory = new System.Windows.Forms.ComboBox();
			this.Label18 = new System.Windows.Forms.Label();
            this.gbRupees = new System.Windows.Forms.GroupBox();
			this.btnR1000 = new System.Windows.Forms.Button();
			this.btnR500 = new System.Windows.Forms.Button();
			this.btnR100 = new System.Windows.Forms.Button();
			this.btnR50 = new System.Windows.Forms.Button();
            this.gbNumbers = new System.Windows.Forms.GroupBox();
			this.btnback = new System.Windows.Forms.Button();
			this.btn00 = new System.Windows.Forms.Button();
			this.btn9 = new System.Windows.Forms.Button();
			this.btn8 = new System.Windows.Forms.Button();
			this.btn7 = new System.Windows.Forms.Button();
			this.btn6 = new System.Windows.Forms.Button();
			this.btn5 = new System.Windows.Forms.Button();
			this.btn4 = new System.Windows.Forms.Button();
			this.btn3 = new System.Windows.Forms.Button();
			this.btn2 = new System.Windows.Forms.Button();
			this.btn1 = new System.Windows.Forms.Button();
			this.btn0 = new System.Windows.Forms.Button();
            this.gbRefundCard = new System.Windows.Forms.GroupBox(); 
			this.Label7 = new System.Windows.Forms.Label();
			this.Label14 = new System.Windows.Forms.Label();
			this.txtRefundCardNo = new System.Windows.Forms.TextBox();
            this.MyGroupBox5 = new System.Windows.Forms.GroupBox();
			this.Label5 = new System.Windows.Forms.Label();
			this.TextBox4 = new System.Windows.Forms.TextBox();
			this.Label6 = new System.Windows.Forms.Label();
			this.txtRefundBalance = new System.Windows.Forms.TextBox();
            this.gbRefillCard = new System.Windows.Forms.GroupBox();
			this.Label13 = new System.Windows.Forms.Label();
			this.txtRefillFinalBalance = new System.Windows.Forms.TextBox();
			this.Label12 = new System.Windows.Forms.Label();
			this.txtRefillAmount = new System.Windows.Forms.TextBox();
			this.Label11 = new System.Windows.Forms.Label();
			this.txtRefillCardNo = new System.Windows.Forms.TextBox();
            this.MyGroupBox3 = new System.Windows.Forms.GroupBox();
			this.Label3 = new System.Windows.Forms.Label();
			this.TextBox2 = new System.Windows.Forms.TextBox();
			this.Label4 = new System.Windows.Forms.Label();
			this.txtRefillCardBalance = new System.Windows.Forms.TextBox();
            this.gbNewCard = new System.Windows.Forms.GroupBox();
			this.Label10 = new System.Windows.Forms.Label();
			this.txtNewSecurityDep = new System.Windows.Forms.TextBox();
			this.Label9 = new System.Windows.Forms.Label();
			this.txtNewCardNo = new System.Windows.Forms.TextBox();
			this.Label2 = new System.Windows.Forms.Label();
			this.txtNewUsableBalance = new System.Windows.Forms.TextBox();
			this.Label1 = new System.Windows.Forms.Label();
			this.txtNewRefill = new System.Windows.Forms.TextBox();
			this.lblSerial = new System.Windows.Forms.Label();
			this.GroupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.imgCloseReport).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.DataGridView1).BeginInit();
			this.gbDisplay.SuspendLayout();
			this.gbRupees.SuspendLayout();
			this.gbNumbers.SuspendLayout();
			this.gbRefundCard.SuspendLayout();
			this.MyGroupBox5.SuspendLayout();
			this.gbRefillCard.SuspendLayout();
			this.MyGroupBox3.SuspendLayout();
			this.gbNewCard.SuspendLayout();
			this.SuspendLayout();
			//
			//btnCash
			//
			this.btnCash.Font = new System.Drawing.Font("Calibri", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (byte)(0));
			this.btnCash.Location = new System.Drawing.Point(12, 543);
			this.btnCash.Name = "btnCash";
			this.btnCash.Size = new System.Drawing.Size(115, 39);
			this.btnCash.TabIndex = 6;
			this.btnCash.Text = "CASH";
			this.btnCash.UseVisualStyleBackColor = true;
            this.btnCash.Click += new System.EventHandler(this.btnCash_Click);
			//
			//btnDiscard
			//
			this.btnDiscard.Font = new System.Drawing.Font("Calibri", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (byte)(0));
			this.btnDiscard.Location = new System.Drawing.Point(171, 543);
			this.btnDiscard.Name = "btnDiscard";
			this.btnDiscard.Size = new System.Drawing.Size(115, 39);
			this.btnDiscard.TabIndex = 7;
			this.btnDiscard.Text = "DISCARD";
			this.btnDiscard.UseVisualStyleBackColor = true;
            this.btnDiscard.Click += new System.EventHandler(this.btnDiscard_Click);
            //
			//btnExit
			//
			this.btnExit.Font = new System.Drawing.Font("Calibri", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (byte)(0));
			this.btnExit.Location = new System.Drawing.Point(330, 543);
			this.btnExit.Name = "btnExit";
			this.btnExit.Size = new System.Drawing.Size(115, 39);
			this.btnExit.TabIndex = 8;
			this.btnExit.Text = "EXIT";
			this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            //
			//lblUserName
			//
			this.lblUserName.AutoSize = true;
			this.lblUserName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.lblUserName.Font = new System.Drawing.Font("Calibri", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (byte)(0));
			this.lblUserName.Location = new System.Drawing.Point(454, 9);
			this.lblUserName.Name = "lblUserName";
			this.lblUserName.Size = new System.Drawing.Size(62, 15);
			this.lblUserName.TabIndex = 0;
			this.lblUserName.Text = "Welcome ";
			this.lblUserName.TextAlign = System.Drawing.ContentAlignment.TopRight;
			//
			//tmrUserCard
			//
            this.tmrUserCard.Tick += new System.EventHandler(this.tmrUserCard_Tick);

			//
			//dtCurrent
			//
			this.dtCurrent.CalendarFont = new System.Drawing.Font("Calibri", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (byte)(0));
			this.dtCurrent.Checked = false;
			this.dtCurrent.CustomFormat = "dd-MMM-yyyy";
			this.dtCurrent.Font = new System.Drawing.Font("Calibri", 12f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (byte)(0));
			this.dtCurrent.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
			this.dtCurrent.Location = new System.Drawing.Point(832, 8);
			this.dtCurrent.Name = "dtCurrent";
			this.dtCurrent.Size = new System.Drawing.Size(130, 27);
			this.dtCurrent.TabIndex = 12;
			//
			//btnNewCard
			//
			this.btnNewCard.BackColor = System.Drawing.Color.Gray;
			this.btnNewCard.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.btnNewCard.Font = new System.Drawing.Font("Calibri", 12f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (byte)(0));
			this.btnNewCard.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.btnNewCard.Location = new System.Drawing.Point(129, 18);
			this.btnNewCard.Name = "btnNewCard";
			this.btnNewCard.Size = new System.Drawing.Size(157, 29);
			this.btnNewCard.TabIndex = 8;
			this.btnNewCard.Text = "NEW CARD";
			this.btnNewCard.UseVisualStyleBackColor = false;
            this.btnNewCard.Paint += new System.Windows.Forms.PaintEventHandler(this.btnNewCard_Paint);

            //
			//btnRefillCard
			//
			this.btnRefillCard.BackColor = System.Drawing.Color.Gray;
			this.btnRefillCard.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.btnRefillCard.Font = new System.Drawing.Font("Calibri", 12f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (byte)(0));
			this.btnRefillCard.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.btnRefillCard.Location = new System.Drawing.Point(129, 199);
			this.btnRefillCard.Name = "btnRefillCard";
			this.btnRefillCard.Size = new System.Drawing.Size(157, 29);
			this.btnRefillCard.TabIndex = 12;
			this.btnRefillCard.Text = "REFILL CARD";
			this.btnRefillCard.UseVisualStyleBackColor = false;
            this.btnRefillCard.Paint += new System.Windows.Forms.PaintEventHandler(this.btnRefillCard_Paint);
            this.btnRefillCard.Click += new System.EventHandler(this.btnRefillCard_Click);
            //
			//btnRefundCard
			//
			this.btnRefundCard.BackColor = System.Drawing.Color.Gray;
			this.btnRefundCard.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.btnRefundCard.Font = new System.Drawing.Font("Calibri", 12f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (byte)(0));
			this.btnRefundCard.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.btnRefundCard.Location = new System.Drawing.Point(129, 372);
			this.btnRefundCard.Name = "btnRefundCard";
			this.btnRefundCard.Size = new System.Drawing.Size(157, 29);
			this.btnRefundCard.TabIndex = 13;
			this.btnRefundCard.Text = "REFUND CARD";
			this.btnRefundCard.UseVisualStyleBackColor = false;
            this.btnRefundCard.Paint += new System.Windows.Forms.PaintEventHandler(this.btnRefundCard_Paint);
            this.btnRefundCard.Click += new System.EventHandler(this.btnRefundCard_Click);
            //
			//GroupBox1
			//
			this.GroupBox1.Controls.Add(this.Button1);
			this.GroupBox1.Controls.Add(this.imgCloseReport);
			this.GroupBox1.Controls.Add(this.btnCashierReport);
			this.GroupBox1.Controls.Add(this.btnCardHistory);
			this.GroupBox1.Location = new System.Drawing.Point(457, 145);
			this.GroupBox1.Name = "GroupBox1";
			this.GroupBox1.Size = new System.Drawing.Size(504, 54);
			this.GroupBox1.TabIndex = 14;
			this.GroupBox1.TabStop = false;
			//
			//Button1
			//
			this.Button1.Font = new System.Drawing.Font("Calibri", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (byte)(0));
			this.Button1.Location = new System.Drawing.Point(308, 11);
			this.Button1.Name = "Button1";
			this.Button1.Size = new System.Drawing.Size(137, 33);
			this.Button1.TabIndex = 3;
			this.Button1.Text = "CASHIER (PRINT)";
			this.Button1.UseVisualStyleBackColor = true;
            this.Button1.Click += new System.EventHandler(this.Button1_Click);
            //
			//imgCloseReport
			//
			this.imgCloseReport.ErrorImage = null;
			this.imgCloseReport.Image = (System.Drawing.Image)resources.GetObject("imgCloseReport.Image");
			this.imgCloseReport.InitialImage = null;
			this.imgCloseReport.Location = new System.Drawing.Point(452, 10);
			this.imgCloseReport.Name = "imgCloseReport";
			this.imgCloseReport.Size = new System.Drawing.Size(46, 33);
			this.imgCloseReport.TabIndex = 2;
			this.imgCloseReport.TabStop = false;
            this.imgCloseReport.Click += new System.EventHandler(this.imgCloseReport_Click);
            //
			//btnCashierReport
			//
			this.btnCashierReport.Font = new System.Drawing.Font("Calibri", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (byte)(0));
			this.btnCashierReport.Location = new System.Drawing.Point(156, 11);
			this.btnCashierReport.Name = "btnCashierReport";
			this.btnCashierReport.Size = new System.Drawing.Size(137, 33);
			this.btnCashierReport.TabIndex = 1;
			this.btnCashierReport.Text = "CASHIER (SCREEN)";
			this.btnCashierReport.UseVisualStyleBackColor = true;
            this.btnCashierReport.Click += new System.EventHandler(this.btnCashierReport_Click);
            //
			//btnCardHistory
			//
			this.btnCardHistory.Font = new System.Drawing.Font("Calibri", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (byte)(0));
			this.btnCardHistory.Location = new System.Drawing.Point(4, 11);
			this.btnCardHistory.Name = "btnCardHistory";
			this.btnCardHistory.Size = new System.Drawing.Size(137, 33);
			this.btnCardHistory.TabIndex = 0;
			this.btnCardHistory.Text = "CARD HISTORY";
			this.btnCardHistory.UseVisualStyleBackColor = true;
            this.btnCardHistory.Click += new System.EventHandler(this.btnCardHistory_Click);
            //
			//DataGridView1
			//
			this.DataGridView1.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
			this.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.DataGridView1.Location = new System.Drawing.Point(457, 205);
			this.DataGridView1.Name = "DataGridView1";
			this.DataGridView1.RowHeadersVisible = false;
			this.DataGridView1.ShowCellToolTips = false;
			this.DataGridView1.Size = new System.Drawing.Size(504, 114);
			this.DataGridView1.TabIndex = 15;
            this.DataGridView1.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.DataGridView1_RowPostPaint);
            //
			//txtCardNoHistory
			//
			this.txtCardNoHistory.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtCardNoHistory.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (byte)(0));
			this.txtCardNoHistory.Location = new System.Drawing.Point(190, 16);
			this.txtCardNoHistory.MaxLength = 6;
			this.txtCardNoHistory.Name = "txtCardNoHistory";
			this.txtCardNoHistory.Size = new System.Drawing.Size(92, 22);
			this.txtCardNoHistory.TabIndex = 17;
            this.txtCardNoHistory.Enter += new System.EventHandler(this.TextBox_Enter);
            this.txtCardNoHistory.TextChanged += new System.EventHandler(this.txtCardNoHistory_TextChanged);
            //
			//Label8
			//
			this.Label8.AutoSize = true;
			this.Label8.Font = new System.Drawing.Font("Calibri", 12f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (byte)(0));
			this.Label8.Location = new System.Drawing.Point(18, 16);
			this.Label8.Name = "Label8";
			this.Label8.Size = new System.Drawing.Size(83, 19);
			this.Label8.TabIndex = 6;
			this.Label8.Text = "CARD NO. :";
			//
			//lblCardNo
			//
			this.lblCardNo.AutoSize = true;
			this.lblCardNo.Font = new System.Drawing.Font("Calibri", 12f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (byte)(0));
			this.lblCardNo.Location = new System.Drawing.Point(187, 16);
			this.lblCardNo.Name = "lblCardNo";
			this.lblCardNo.Size = new System.Drawing.Size(60, 19);
			this.lblCardNo.TabIndex = 7;
			this.lblCardNo.Text = "CardNo";
			//
			//Label15
			//
			this.Label15.AutoSize = true;
			this.Label15.Font = new System.Drawing.Font("Calibri", 12f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (byte)(0));
			this.Label15.Location = new System.Drawing.Point(18, 46);
			this.Label15.Name = "Label15";
			this.Label15.Size = new System.Drawing.Size(132, 19);
			this.Label15.TabIndex = 8;
			this.Label15.Text = "USABLE BALANCE :";
			//
			//Label16
			//
			this.Label16.AutoSize = true;
			this.Label16.Font = new System.Drawing.Font("Calibri", 12f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (byte)(0));
			this.Label16.Location = new System.Drawing.Point(18, 78);
			this.Label16.Name = "Label16";
			this.Label16.Size = new System.Drawing.Size(140, 19);
			this.Label16.TabIndex = 9;
			this.Label16.Text = "SECURITY DEPOSIT :";
			//
			//lblUsableBalance
			//
			this.lblUsableBalance.AutoSize = true;
			this.lblUsableBalance.Font = new System.Drawing.Font("Calibri", 12f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (byte)(0));
			this.lblUsableBalance.Location = new System.Drawing.Point(187, 46);
			this.lblUsableBalance.Name = "lblUsableBalance";
			this.lblUsableBalance.Size = new System.Drawing.Size(111, 19);
			this.lblUsableBalance.TabIndex = 10;
			this.lblUsableBalance.Text = "Usable Balance";
			//
			//lblSecDeposit
			//
			this.lblSecDeposit.AutoSize = true;
			this.lblSecDeposit.Font = new System.Drawing.Font("Calibri", 12f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (byte)(0));
			this.lblSecDeposit.Location = new System.Drawing.Point(191, 78);
			this.lblSecDeposit.Name = "lblSecDeposit";
			this.lblSecDeposit.Size = new System.Drawing.Size(114, 19);
			this.lblSecDeposit.TabIndex = 11;
			this.lblSecDeposit.Text = "Security Deposit";
			//
			//lblMessage
			//
			this.lblMessage.AutoSize = true;
			this.lblMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, (byte)(0));
			this.lblMessage.Location = new System.Drawing.Point(378, 20);
			this.lblMessage.Name = "lblMessage";
			this.lblMessage.Size = new System.Drawing.Size(117, 13);
			this.lblMessage.TabIndex = 19;
			this.lblMessage.Text = "(Use Numeric Key Pad)";
			//
			//lblprivateNo
			//
			this.lblprivateNo.AutoSize = true;
			this.lblprivateNo.Font = new System.Drawing.Font("Calibri", 12f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (byte)(0));
			this.lblprivateNo.Location = new System.Drawing.Point(389, 16);
			this.lblprivateNo.Name = "lblprivateNo";
			this.lblprivateNo.Size = new System.Drawing.Size(83, 19);
			this.lblprivateNo.TabIndex = 12;
			this.lblprivateNo.Text = "privateNo";
			this.lblprivateNo.Visible = false;
			//
			//Label17
			//
			this.Label17.AutoSize = true;
			this.Label17.Font = new System.Drawing.Font("Calibri", 12f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (byte)(0));
			this.Label17.ForeColor = System.Drawing.Color.Red;
			this.Label17.Location = new System.Drawing.Point(390, 46);
			this.Label17.Name = "Label17";
			this.Label17.Size = new System.Drawing.Size(70, 19);
			this.Label17.TabIndex = 20;
			this.Label17.Text = "PARKING";
			this.Label17.Visible = false;
			//
			//gbDisplay
			//
			this.gbDisplay.Controls.Add(this.lblSerial);
			this.gbDisplay.Controls.Add(this.cmbCardSerialHistory);
			this.gbDisplay.Controls.Add(this.Label18);
			this.gbDisplay.Controls.Add(this.Label17);
			this.gbDisplay.Controls.Add(this.lblprivateNo);
			this.gbDisplay.Controls.Add(this.lblMessage);
			this.gbDisplay.Controls.Add(this.lblSecDeposit);
			this.gbDisplay.Controls.Add(this.lblUsableBalance);
			this.gbDisplay.Controls.Add(this.Label16);
			this.gbDisplay.Controls.Add(this.Label15);
			this.gbDisplay.Controls.Add(this.lblCardNo);
			this.gbDisplay.Controls.Add(this.Label8);
			this.gbDisplay.Controls.Add(this.txtCardNoHistory);
			this.gbDisplay.Location = new System.Drawing.Point(457, 35);
			this.gbDisplay.Name = "gbDisplay";
			this.gbDisplay.Size = new System.Drawing.Size(504, 109);
			this.gbDisplay.TabIndex = 11;
			this.gbDisplay.TabStop = false;
			//
			//cmbCardSerialHistory
			//
			this.cmbCardSerialHistory.Font = new System.Drawing.Font("Calibri", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (byte)(0));
			this.cmbCardSerialHistory.FormattingEnabled = true;
			this.cmbCardSerialHistory.Location = new System.Drawing.Point(288, 16);
			this.cmbCardSerialHistory.Name = "cmbCardSerialHistory";
			this.cmbCardSerialHistory.Size = new System.Drawing.Size(54, 23);
			this.cmbCardSerialHistory.TabIndex = 22;
            this.cmbCardSerialHistory.SelectedIndexChanged += new System.EventHandler(this.cmbCardSerialHistory_SelectedIndexChanged);
            //
			//Label18
			//
			this.Label18.AutoSize = true;
			this.Label18.Font = new System.Drawing.Font("Calibri", 12f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (byte)(0));
			this.Label18.ForeColor = System.Drawing.Color.Red;
			this.Label18.Location = new System.Drawing.Point(390, 78);
			this.Label18.Name = "Label18";
			this.Label18.Size = new System.Drawing.Size(62, 19);
			this.Label18.TabIndex = 21;
			this.Label18.Text = "LOCKED";
			this.Label18.Visible = false;
			//
			//gbRupees
			//
			this.gbRupees.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			//this.gbRupees.BorderColor = System.Drawing.Color.Black;
			this.gbRupees.Controls.Add(this.btnR1000);
			this.gbRupees.Controls.Add(this.btnR500);
			this.gbRupees.Controls.Add(this.btnR100);
			this.gbRupees.Controls.Add(this.btnR50);
			this.gbRupees.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.gbRupees.Font = new System.Drawing.Font("Calibri", 12f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (byte)(0));
			this.gbRupees.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
			this.gbRupees.Location = new System.Drawing.Point(457, 324);
			this.gbRupees.Name = "gbRupees";
			this.gbRupees.Size = new System.Drawing.Size(505, 54);
			this.gbRupees.TabIndex = 10;
			this.gbRupees.TabStop = false;
			//
			//btnR1000
			//
			this.btnR1000.Image = (System.Drawing.Image)resources.GetObject("btnR1000.Image");
			this.btnR1000.Location = new System.Drawing.Point(402, 6);
			this.btnR1000.Name = "btnR1000";
			this.btnR1000.Size = new System.Drawing.Size(93, 40);
			this.btnR1000.TabIndex = 4;
			this.btnR1000.Tag = "1000";
			this.btnR1000.UseVisualStyleBackColor = true;
            this.btnR1000.MouseUp += new System.Windows.Forms.MouseEventHandler(this.RupeeMouseUp);

            //
			//btnR500
			//
			this.btnR500.Image = (System.Drawing.Image)resources.GetObject("btnR500.Image");
			this.btnR500.Location = new System.Drawing.Point(270, 6);
			this.btnR500.Name = "btnR500";
			this.btnR500.Size = new System.Drawing.Size(93, 40);
			this.btnR500.TabIndex = 3;
			this.btnR500.Tag = "500";
			this.btnR500.UseVisualStyleBackColor = true;
            this.btnR500.MouseUp += new System.Windows.Forms.MouseEventHandler(this.RupeeMouseUp);
            //
			//btnR100
			//
			this.btnR100.Image = (System.Drawing.Image)resources.GetObject("btnR100.Image");
			this.btnR100.Location = new System.Drawing.Point(138, 6);
			this.btnR100.Name = "btnR100";
			this.btnR100.Size = new System.Drawing.Size(93, 40);
			this.btnR100.TabIndex = 2;
			this.btnR100.Tag = "100";
			this.btnR100.UseVisualStyleBackColor = true;
            this.btnR100.MouseUp += new System.Windows.Forms.MouseEventHandler(this.RupeeMouseUp);
            //
			//btnR50
			//
			this.btnR50.Image = (System.Drawing.Image)resources.GetObject("btnR50.Image");
			this.btnR50.Location = new System.Drawing.Point(6, 6);
			this.btnR50.Name = "btnR50";
			this.btnR50.Size = new System.Drawing.Size(93, 40);
			this.btnR50.TabIndex = 1;
			this.btnR50.Tag = "50";
			this.btnR50.UseVisualStyleBackColor = true;
            this.btnR50.MouseUp += new System.Windows.Forms.MouseEventHandler(this.RupeeMouseUp);
            //
			//gbNumbers
			//
			this.gbNumbers.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			//this.gbNumbers.BorderColor = System.Drawing.Color.Black;
			this.gbNumbers.Controls.Add(this.btnback);
			this.gbNumbers.Controls.Add(this.btn00);
			this.gbNumbers.Controls.Add(this.btn9);
			this.gbNumbers.Controls.Add(this.btn8);
			this.gbNumbers.Controls.Add(this.btn7);
			this.gbNumbers.Controls.Add(this.btn6);
			this.gbNumbers.Controls.Add(this.btn5);
			this.gbNumbers.Controls.Add(this.btn4);
			this.gbNumbers.Controls.Add(this.btn3);
			this.gbNumbers.Controls.Add(this.btn2);
			this.gbNumbers.Controls.Add(this.btn1);
			this.gbNumbers.Controls.Add(this.btn0);
			this.gbNumbers.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.gbNumbers.Font = new System.Drawing.Font("Calibri", 12f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (byte)(0));
			this.gbNumbers.ForeColor = System.Drawing.Color.Black;
			this.gbNumbers.Location = new System.Drawing.Point(457, 384);
			this.gbNumbers.Name = "gbNumbers";
			this.gbNumbers.Size = new System.Drawing.Size(505, 144);
			this.gbNumbers.TabIndex = 9;
			this.gbNumbers.TabStop = false;
			//
			//btnback
			//
			this.btnback.Image = (System.Drawing.Image)resources.GetObject("btnback.Image");
			this.btnback.Location = new System.Drawing.Point(427, 74);
			this.btnback.Name = "btnback";
			this.btnback.Size = new System.Drawing.Size(56, 54);
			this.btnback.TabIndex = 11;
			this.btnback.UseVisualStyleBackColor = true;
            this.btnR50.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnback_MouseUp1);
            //
			//btn00
			//
			this.btn00.Image = (System.Drawing.Image)resources.GetObject("btn00.Image");
			this.btn00.Location = new System.Drawing.Point(427, 6);
			this.btn00.Name = "btn00";
			this.btn00.Size = new System.Drawing.Size(56, 54);
			this.btn00.TabIndex = 10;
			this.btn00.Tag = "00";
			this.btn00.UseVisualStyleBackColor = true;
            this.btnR50.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NumericMouseUp);
            //
			//btn9
			//
			this.btn9.Image = (System.Drawing.Image)resources.GetObject("btn9.Image");
			this.btn9.Location = new System.Drawing.Point(344, 74);
			this.btn9.Name = "btn9";
			this.btn9.Size = new System.Drawing.Size(56, 54);
			this.btn9.TabIndex = 9;
			this.btn9.Tag = "9";
			this.btn9.UseVisualStyleBackColor = true;
            this.btn9.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NumericMouseUp);
            //
			//btn8
			//
			this.btn8.Image = (System.Drawing.Image)resources.GetObject("btn8.Image");
			this.btn8.Location = new System.Drawing.Point(261, 74);
			this.btn8.Name = "btn8";
			this.btn8.Size = new System.Drawing.Size(56, 54);
			this.btn8.TabIndex = 8;
			this.btn8.Tag = "8";
			this.btn8.UseVisualStyleBackColor = true;
            this.btn8.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NumericMouseUp);
            //
			//btn7
			//
			this.btn7.Image = (System.Drawing.Image)resources.GetObject("btn7.Image");
			this.btn7.Location = new System.Drawing.Point(178, 74);
			this.btn7.Name = "btn7";
			this.btn7.Size = new System.Drawing.Size(56, 54);
			this.btn7.TabIndex = 7;
			this.btn7.Tag = "7";
			this.btn7.UseVisualStyleBackColor = true;
            this.btn7.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NumericMouseUp);
            //
			//btn6
			//
			this.btn6.Image = (System.Drawing.Image)resources.GetObject("btn6.Image");
			this.btn6.Location = new System.Drawing.Point(95, 74);
			this.btn6.Name = "btn6";
			this.btn6.Size = new System.Drawing.Size(56, 54);
			this.btn6.TabIndex = 6;
			this.btn6.Tag = "6";
			this.btn6.UseVisualStyleBackColor = true;
            this.btn6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NumericMouseUp);
            //
			//btn5
			//
			this.btn5.Image = (System.Drawing.Image)resources.GetObject("btn5.Image");
			this.btn5.Location = new System.Drawing.Point(12, 74);
			this.btn5.Name = "btn5";
			this.btn5.Size = new System.Drawing.Size(56, 54);
			this.btn5.TabIndex = 5;
			this.btn5.Tag = "5";
			this.btn5.UseVisualStyleBackColor = true;
            this.btn5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NumericMouseUp);
            //
			//btn4
			//
			this.btn4.Image = (System.Drawing.Image)resources.GetObject("btn4.Image");
			this.btn4.Location = new System.Drawing.Point(344, 6);
			this.btn4.Name = "btn4";
			this.btn4.Size = new System.Drawing.Size(56, 54);
			this.btn4.TabIndex = 4;
			this.btn4.Tag = "4";
			this.btn4.UseVisualStyleBackColor = true;
            this.btn4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NumericMouseUp);
            //
			//btn3
			//
			this.btn3.Image = (System.Drawing.Image)resources.GetObject("btn3.Image");
			this.btn3.Location = new System.Drawing.Point(261, 6);
			this.btn3.Name = "btn3";
			this.btn3.Size = new System.Drawing.Size(56, 54);
			this.btn3.TabIndex = 3;
			this.btn3.Tag = "3";
			this.btn3.UseVisualStyleBackColor = true;
            this.btn3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NumericMouseUp);
            //
			//btn2
			//
			this.btn2.Image = (System.Drawing.Image)resources.GetObject("btn2.Image");
			this.btn2.Location = new System.Drawing.Point(178, 6);
			this.btn2.Name = "btn2";
			this.btn2.Size = new System.Drawing.Size(56, 54);
			this.btn2.TabIndex = 2;
			this.btn2.Tag = "2";
			this.btn2.UseVisualStyleBackColor = true;
            this.btn2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NumericMouseUp);
            //
			//btn1
			//
			this.btn1.Image = (System.Drawing.Image)resources.GetObject("btn1.Image");
			this.btn1.Location = new System.Drawing.Point(95, 6);
			this.btn1.Name = "btn1";
			this.btn1.Size = new System.Drawing.Size(56, 54);
			this.btn1.TabIndex = 1;
			this.btn1.Tag = "1";
			this.btn1.UseVisualStyleBackColor = true;
            this.btn1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NumericMouseUp);
            //
			//btn0
			//
			this.btn0.BackColor = System.Drawing.SystemColors.Info;
			this.btn0.Image = (System.Drawing.Image)resources.GetObject("btn0.Image");
			this.btn0.Location = new System.Drawing.Point(12, 6);
			this.btn0.Name = "btn0";
			this.btn0.Size = new System.Drawing.Size(56, 54);
			this.btn0.TabIndex = 0;
			this.btn0.Tag = "0";
			this.btn0.UseVisualStyleBackColor = false;
            this.btn0.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NumericMouseUp);
            //
			//gbRefundCard
			//
			this.gbRefundCard.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			//this.gbRefundCard.BorderColor = System.Drawing.Color.Red;
			this.gbRefundCard.Controls.Add(this.Label7);
			this.gbRefundCard.Controls.Add(this.Label14);
			this.gbRefundCard.Controls.Add(this.txtRefundCardNo);
			this.gbRefundCard.Controls.Add(this.MyGroupBox5);
			this.gbRefundCard.Controls.Add(this.Label6);
			this.gbRefundCard.Controls.Add(this.txtRefundBalance);
			this.gbRefundCard.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.gbRefundCard.Font = new System.Drawing.Font("Calibri", 12f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (byte)(0));
			this.gbRefundCard.ForeColor = System.Drawing.Color.Red;
			this.gbRefundCard.Location = new System.Drawing.Point(12, 388);
			this.gbRefundCard.Name = "gbRefundCard";
			this.gbRefundCard.Size = new System.Drawing.Size(433, 140);
			this.gbRefundCard.TabIndex = 5;
			this.gbRefundCard.TabStop = false;
			//
			//Label7
			//
			this.Label7.AutoSize = true;
			this.Label7.Font = new System.Drawing.Font("Calibri", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (byte)(0));
			this.Label7.Location = new System.Drawing.Point(21, 111);
			this.Label7.Name = "Label7";
			this.Label7.Size = new System.Drawing.Size(206, 15);
			this.Label7.TabIndex = 10;
			this.Label7.Text = "** Amount includes Security Deposit";
			//
			//Label14
			//
			this.Label14.AutoSize = true;
			this.Label14.Font = new System.Drawing.Font("Calibri", 12f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (byte)(0));
			this.Label14.Location = new System.Drawing.Point(20, 23);
			this.Label14.Name = "Label14";
			this.Label14.Size = new System.Drawing.Size(83, 19);
			this.Label14.TabIndex = 9;
			this.Label14.Text = "CARD NO. :";
			//
			//txtRefundCardNo
			//
			this.txtRefundCardNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtRefundCardNo.Location = new System.Drawing.Point(273, 23);
			this.txtRefundCardNo.Name = "txtRefundCardNo";
			this.txtRefundCardNo.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
			this.txtRefundCardNo.Size = new System.Drawing.Size(141, 27);
			this.txtRefundCardNo.TabIndex = 8;
			//
			//MyGroupBox5
			//
			this.MyGroupBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			//this.MyGroupBox5.BorderColor = System.Drawing.Color.Blue;
			this.MyGroupBox5.Controls.Add(this.Label5);
			this.MyGroupBox5.Controls.Add(this.TextBox4);
			this.MyGroupBox5.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.MyGroupBox5.Font = new System.Drawing.Font("Calibri", 12f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (byte)(0));
			this.MyGroupBox5.Location = new System.Drawing.Point(6, 204);
			this.MyGroupBox5.Name = "MyGroupBox5";
			this.MyGroupBox5.Size = new System.Drawing.Size(433, 198);
			this.MyGroupBox5.TabIndex = 4;
			this.MyGroupBox5.TabStop = false;
			this.MyGroupBox5.Text = "NEW CARD";
			//
			//Label5
			//
			this.Label5.AutoSize = true;
			this.Label5.Font = new System.Drawing.Font("Calibri", 12f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (byte)(0));
			this.Label5.Location = new System.Drawing.Point(19, 42);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(78, 19);
			this.Label5.TabIndex = 1;
			this.Label5.Text = "AMOUNT :";
			//
			//TextBox4
			//
			this.TextBox4.Location = new System.Drawing.Point(272, 42);
			this.TextBox4.Name = "TextBox4";
			this.TextBox4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
			this.TextBox4.Size = new System.Drawing.Size(141, 27);
			this.TextBox4.TabIndex = 0;
			//
			//Label6
			//
			this.Label6.AutoSize = true;
			this.Label6.Font = new System.Drawing.Font("Calibri", 12f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (byte)(0));
			this.Label6.Location = new System.Drawing.Point(19, 58);
			this.Label6.Name = "Label6";
			this.Label6.Size = new System.Drawing.Size(169, 19);
			this.Label6.TabIndex = 1;
			this.Label6.Text = "REFUNDABLE BALANCE :";
			//
			//txtRefundBalance
			//
			this.txtRefundBalance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtRefundBalance.Location = new System.Drawing.Point(272, 56);
			this.txtRefundBalance.Name = "txtRefundBalance";
			this.txtRefundBalance.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
			this.txtRefundBalance.Size = new System.Drawing.Size(141, 27);
			this.txtRefundBalance.TabIndex = 0;
			//
			//gbRefillCard
			//
			this.gbRefillCard.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			//this.gbRefillCard.BorderColor = System.Drawing.Color.SaddleBrown;
			this.gbRefillCard.Controls.Add(this.Label13);
			this.gbRefillCard.Controls.Add(this.txtRefillFinalBalance);
			this.gbRefillCard.Controls.Add(this.Label12);
			this.gbRefillCard.Controls.Add(this.txtRefillAmount);
			this.gbRefillCard.Controls.Add(this.Label11);
			this.gbRefillCard.Controls.Add(this.txtRefillCardNo);
			this.gbRefillCard.Controls.Add(this.MyGroupBox3);
			this.gbRefillCard.Controls.Add(this.Label4);
			this.gbRefillCard.Controls.Add(this.txtRefillCardBalance);
			this.gbRefillCard.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.gbRefillCard.Font = new System.Drawing.Font("Calibri", 12f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (byte)(0));
			this.gbRefillCard.ForeColor = System.Drawing.Color.SaddleBrown;
			this.gbRefillCard.Location = new System.Drawing.Point(12, 215);
			this.gbRefillCard.Name = "gbRefillCard";
			this.gbRefillCard.Size = new System.Drawing.Size(433, 151);
			this.gbRefillCard.TabIndex = 4;
			this.gbRefillCard.TabStop = false;
			//
			//Label13
			//
			this.Label13.AutoSize = true;
			this.Label13.Font = new System.Drawing.Font("Calibri", 12f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (byte)(0));
			this.Label13.Location = new System.Drawing.Point(20, 115);
			this.Label13.Name = "Label13";
			this.Label13.Size = new System.Drawing.Size(114, 19);
			this.Label13.TabIndex = 11;
			this.Label13.Text = "NEW BALANCE :";
			//
			//txtRefillFinalBalance
			//
			this.txtRefillFinalBalance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtRefillFinalBalance.Location = new System.Drawing.Point(273, 113);
			this.txtRefillFinalBalance.Name = "txtRefillFinalBalance";
			this.txtRefillFinalBalance.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
			this.txtRefillFinalBalance.Size = new System.Drawing.Size(141, 27);
			this.txtRefillFinalBalance.TabIndex = 10;
			//
			//Label12
			//
			this.Label12.AutoSize = true;
			this.Label12.Font = new System.Drawing.Font("Calibri", 12f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (byte)(0));
			this.Label12.Location = new System.Drawing.Point(19, 85);
			this.Label12.Name = "Label12";
			this.Label12.Size = new System.Drawing.Size(142, 19);
			this.Label12.TabIndex = 9;
			this.Label12.Text = "AMOUNT REFILLED :";
			//
			//txtRefillAmount
			//
			this.txtRefillAmount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtRefillAmount.Location = new System.Drawing.Point(273, 83);
			this.txtRefillAmount.Name = "txtRefillAmount";
			this.txtRefillAmount.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
			this.txtRefillAmount.Size = new System.Drawing.Size(141, 27);
			this.txtRefillAmount.TabIndex = 8;
            this.txtRefillAmount.Enter += new System.EventHandler(this.TextBox_Enter);
            this.txtRefillAmount.TextChanged += new System.EventHandler(this.txtRefillAmount_TextChanged);
            //
			//Label11
			//
			this.Label11.AutoSize = true;
			this.Label11.Font = new System.Drawing.Font("Calibri", 12f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (byte)(0));
			this.Label11.Location = new System.Drawing.Point(20, 25);
			this.Label11.Name = "Label11";
			this.Label11.Size = new System.Drawing.Size(83, 19);
			this.Label11.TabIndex = 7;
			this.Label11.Text = "CARD NO. :";
			//
			//txtRefillCardNo
			//
			this.txtRefillCardNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtRefillCardNo.Location = new System.Drawing.Point(273, 23);
			this.txtRefillCardNo.Name = "txtRefillCardNo";
			this.txtRefillCardNo.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
			this.txtRefillCardNo.Size = new System.Drawing.Size(141, 27);
			this.txtRefillCardNo.TabIndex = 6;
			//
			//MyGroupBox3
			//
			this.MyGroupBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			//this.MyGroupBox3.BorderColor = System.Drawing.Color.Blue;
			this.MyGroupBox3.Controls.Add(this.Label3);
			this.MyGroupBox3.Controls.Add(this.TextBox2);
			this.MyGroupBox3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.MyGroupBox3.Font = new System.Drawing.Font("Calibri", 12f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (byte)(0));
			this.MyGroupBox3.Location = new System.Drawing.Point(6, 204);
			this.MyGroupBox3.Name = "MyGroupBox3";
			this.MyGroupBox3.Size = new System.Drawing.Size(433, 198);
			this.MyGroupBox3.TabIndex = 4;
			this.MyGroupBox3.TabStop = false;
			this.MyGroupBox3.Text = "NEW CARD";
			//
			//Label3
			//
			this.Label3.AutoSize = true;
			this.Label3.Font = new System.Drawing.Font("Calibri", 12f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (byte)(0));
			this.Label3.Location = new System.Drawing.Point(19, 42);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(78, 19);
			this.Label3.TabIndex = 1;
			this.Label3.Text = "AMOUNT :";
			//
			//TextBox2
			//
			this.TextBox2.Location = new System.Drawing.Point(272, 42);
			this.TextBox2.Name = "TextBox2";
			this.TextBox2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
			this.TextBox2.Size = new System.Drawing.Size(141, 27);
			this.TextBox2.TabIndex = 0;
			//
			//Label4
			//
			this.Label4.AutoSize = true;
			this.Label4.Font = new System.Drawing.Font("Calibri", 12f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (byte)(0));
			this.Label4.Location = new System.Drawing.Point(20, 55);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(145, 19);
			this.Label4.TabIndex = 1;
			this.Label4.Text = "CURRENT BALANCE :";
			//
			//txtRefillCardBalance
			//
			this.txtRefillCardBalance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtRefillCardBalance.Location = new System.Drawing.Point(273, 53);
			this.txtRefillCardBalance.Name = "txtRefillCardBalance";
			this.txtRefillCardBalance.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
			this.txtRefillCardBalance.Size = new System.Drawing.Size(141, 27);
			this.txtRefillCardBalance.TabIndex = 0;
			//
			//gbNewCard
			//
			this.gbNewCard.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			//this.gbNewCard.BorderColor = System.Drawing.Color.Blue;
			this.gbNewCard.Controls.Add(this.Label10);
			this.gbNewCard.Controls.Add(this.txtNewSecurityDep);
			this.gbNewCard.Controls.Add(this.Label9);
			this.gbNewCard.Controls.Add(this.txtNewCardNo);
			this.gbNewCard.Controls.Add(this.Label2);
			this.gbNewCard.Controls.Add(this.txtNewUsableBalance);
			this.gbNewCard.Controls.Add(this.Label1);
			this.gbNewCard.Controls.Add(this.txtNewRefill);
			this.gbNewCard.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.gbNewCard.Font = new System.Drawing.Font("Calibri", 12f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (byte)(0));
			this.gbNewCard.ForeColor = System.Drawing.SystemColors.HotTrack;
			this.gbNewCard.Location = new System.Drawing.Point(12, 35);
			this.gbNewCard.Name = "gbNewCard";
			this.gbNewCard.Size = new System.Drawing.Size(433, 158);
			this.gbNewCard.TabIndex = 3;
			this.gbNewCard.TabStop = false;
			//
			//Label10
			//
			this.Label10.AutoSize = true;
			this.Label10.Font = new System.Drawing.Font("Calibri", 12f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (byte)(0));
			this.Label10.Location = new System.Drawing.Point(19, 121);
			this.Label10.Name = "Label10";
			this.Label10.Size = new System.Drawing.Size(140, 19);
			this.Label10.TabIndex = 7;
			this.Label10.Text = "SECURITY DEPOSIT :";
			//
			//txtNewSecurityDep
			//
			this.txtNewSecurityDep.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtNewSecurityDep.Location = new System.Drawing.Point(272, 119);
			this.txtNewSecurityDep.Name = "txtNewSecurityDep";
			this.txtNewSecurityDep.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
			this.txtNewSecurityDep.Size = new System.Drawing.Size(141, 27);
			this.txtNewSecurityDep.TabIndex = 6;
			//
			//Label9
			//
			this.Label9.AutoSize = true;
			this.Label9.Font = new System.Drawing.Font("Calibri", 12f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (byte)(0));
			this.Label9.Location = new System.Drawing.Point(19, 28);
			this.Label9.Name = "Label9";
			this.Label9.Size = new System.Drawing.Size(83, 19);
			this.Label9.TabIndex = 5;
			this.Label9.Text = "CARD NO. :";
			//
			//txtNewCardNo
			//
			this.txtNewCardNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtNewCardNo.Location = new System.Drawing.Point(272, 26);
			this.txtNewCardNo.Name = "txtNewCardNo";
			this.txtNewCardNo.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
			this.txtNewCardNo.Size = new System.Drawing.Size(141, 27);
			this.txtNewCardNo.TabIndex = 4;
			//
			//Label2
			//
			this.Label2.AutoSize = true;
			this.Label2.Font = new System.Drawing.Font("Calibri", 12f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (byte)(0));
			this.Label2.Location = new System.Drawing.Point(19, 90);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(132, 19);
			this.Label2.TabIndex = 3;
			this.Label2.Text = "USABLE BALANCE :";
			//
			//txtNewUsableBalance
			//
			this.txtNewUsableBalance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtNewUsableBalance.Location = new System.Drawing.Point(272, 88);
			this.txtNewUsableBalance.Name = "txtNewUsableBalance";
			this.txtNewUsableBalance.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
			this.txtNewUsableBalance.Size = new System.Drawing.Size(141, 27);
			this.txtNewUsableBalance.TabIndex = 2;
			//
			//Label1
			//
			this.Label1.AutoSize = true;
			this.Label1.Font = new System.Drawing.Font("Calibri", 12f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, (byte)(0));
			this.Label1.Location = new System.Drawing.Point(19, 59);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(142, 19);
			this.Label1.TabIndex = 1;
			this.Label1.Text = "AMOUNT REFILLED :";
			//
			//txtNewRefill
			//
			this.txtNewRefill.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.txtNewRefill.Location = new System.Drawing.Point(272, 57);
			this.txtNewRefill.Name = "txtNewRefill";
			this.txtNewRefill.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
			this.txtNewRefill.Size = new System.Drawing.Size(141, 27);
			this.txtNewRefill.TabIndex = 0;
            this.txtNewRefill.Enter += new System.EventHandler(this.TextBox_Enter);
            this.txtNewRefill.TextChanged += new System.EventHandler(this.txtNewRefill_TextChanged);
            //
			//lblSerial
			//
			this.lblSerial.AutoSize = true;
			this.lblSerial.Font = new System.Drawing.Font("Calibri", 12f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (byte)(0));
			this.lblSerial.Location = new System.Drawing.Point(253, 16);
			this.lblSerial.Name = "lblSerial";
			this.lblSerial.Size = new System.Drawing.Size(27, 19);
			this.lblSerial.TabIndex = 23;
			this.lblSerial.Text = "Srl";
			//
			//posCashier
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.HighlightText;
			this.ClientSize = new System.Drawing.Size(984, 594);
			this.Controls.Add(this.DataGridView1);
			this.Controls.Add(this.GroupBox1);
			this.Controls.Add(this.btnRefundCard);
			this.Controls.Add(this.btnRefillCard);
			this.Controls.Add(this.btnNewCard);
			this.Controls.Add(this.dtCurrent);
			this.Controls.Add(this.lblUserName);
			this.Controls.Add(this.gbDisplay);
			this.Controls.Add(this.gbRupees);
			this.Controls.Add(this.gbNumbers);
			this.Controls.Add(this.btnExit);
			this.Controls.Add(this.btnDiscard);
			this.Controls.Add(this.btnCash);
			this.Controls.Add(this.gbRefundCard);
			this.Controls.Add(this.gbRefillCard);
			this.Controls.Add(this.gbNewCard);
			this.Name = "posCashier";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "CASHIER";
			this.GroupBox1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)this.imgCloseReport).EndInit();
			((System.ComponentModel.ISupportInitialize)this.DataGridView1).EndInit();
			this.gbDisplay.ResumeLayout(false);
			this.gbDisplay.PerformLayout();
			this.gbRupees.ResumeLayout(false);
			this.gbNumbers.ResumeLayout(false);
			this.gbRefundCard.ResumeLayout(false);
			this.gbRefundCard.PerformLayout();
			this.MyGroupBox5.ResumeLayout(false);
			this.MyGroupBox5.PerformLayout();
			this.gbRefillCard.ResumeLayout(false);
			this.gbRefillCard.PerformLayout();
			this.MyGroupBox3.ResumeLayout(false);
			this.MyGroupBox3.PerformLayout();
			this.gbNewCard.ResumeLayout(false);
			this.gbNewCard.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		//private DCardPOS.MyGroupBox gbRefillCard;
        private System.Windows.Forms.GroupBox gbRefillCard; //
        //private DCardPOS.MyGroupBox gbRefillCard;
        private System.Windows.Forms.GroupBox MyGroupBox3; //
		private System.Windows.Forms.Label Label3;
		private System.Windows.Forms.TextBox TextBox2;
		private System.Windows.Forms.Label Label4;
		private System.Windows.Forms.TextBox txtRefillCardBalance;
        private System.Windows.Forms.GroupBox gbRefundCard; //
        private System.Windows.Forms.GroupBox MyGroupBox5; //
		private System.Windows.Forms.Label Label5;
		private System.Windows.Forms.TextBox TextBox4;
		private System.Windows.Forms.Label Label6;
		private System.Windows.Forms.TextBox txtRefundBalance;
		private System.Windows.Forms.Button btnCash;
		private System.Windows.Forms.Button btnDiscard;
		private System.Windows.Forms.Button btnExit;
		private System.Windows.Forms.Label Label11;
		private System.Windows.Forms.TextBox txtRefillCardNo;
        private System.Windows.Forms.GroupBox gbNumbers; //
		private System.Windows.Forms.Button btn00;
		private System.Windows.Forms.Button btn9;
		private System.Windows.Forms.Button btn8;
		private System.Windows.Forms.Button btn7;
		private System.Windows.Forms.Button btn6;
		private System.Windows.Forms.Button btn5;
		private System.Windows.Forms.Button btn4;
		private System.Windows.Forms.Button btn3;
		private System.Windows.Forms.Button btn2;
		private System.Windows.Forms.Button btn1;
		private System.Windows.Forms.Button btn0;
		private System.Windows.Forms.Label Label7;
		private System.Windows.Forms.Label Label14;
		private System.Windows.Forms.TextBox txtRefundCardNo;
		private System.Windows.Forms.Label Label13;
		private System.Windows.Forms.TextBox txtRefillFinalBalance;
		private System.Windows.Forms.Label Label12;
		private System.Windows.Forms.TextBox txtRefillAmount;
        private System.Windows.Forms.GroupBox gbRupees;//
		private System.Windows.Forms.Button btnR1000;
		private System.Windows.Forms.Button btnR500;
		private System.Windows.Forms.Button btnR100;
		private System.Windows.Forms.Button btnR50;
		private System.Windows.Forms.Button btnback;
		private System.Windows.Forms.Label lblUserName;
		private System.Windows.Forms.Timer tmrUserCard;
		private System.Windows.Forms.DateTimePicker dtCurrent;
		private System.Windows.Forms.Button btnNewCard;
		private System.Windows.Forms.Button btnRefillCard;
		private System.Windows.Forms.Button btnRefundCard;
		private System.Windows.Forms.GroupBox GroupBox1;
		private System.Windows.Forms.Button btnCashierReport;
		private System.Windows.Forms.DataGridView DataGridView1;
		private System.Windows.Forms.PictureBox imgCloseReport;
        private System.Windows.Forms.GroupBox gbNewCard; //
		private System.Windows.Forms.Label Label10;
		private System.Windows.Forms.TextBox txtNewSecurityDep;
		private System.Windows.Forms.Label Label9;
		private System.Windows.Forms.TextBox txtNewCardNo;
		private System.Windows.Forms.Label Label2;
		private System.Windows.Forms.TextBox txtNewUsableBalance;
		private System.Windows.Forms.Label Label1;
		private System.Windows.Forms.TextBox txtNewRefill;
		private System.Windows.Forms.TextBox txtCardNoHistory;
		private System.Windows.Forms.Label Label8;
		private System.Windows.Forms.Label lblCardNo;
		private System.Windows.Forms.Label Label15;
		private System.Windows.Forms.Label Label16;
		private System.Windows.Forms.Label lblUsableBalance;
		private System.Windows.Forms.Label lblSecDeposit;
		private System.Windows.Forms.Label lblMessage;
		private System.Windows.Forms.Label lblprivateNo;
		private System.Windows.Forms.Label Label17;
		private System.Windows.Forms.GroupBox gbDisplay;
		private System.Windows.Forms.Label Label18;
		private System.Windows.Forms.ComboBox cmbCardSerialHistory;
		private System.Windows.Forms.Button Button1;
		private System.Windows.Forms.Button btnCardHistory;
		private System.Windows.Forms.Label lblSerial;
	}
}
