﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using FoodSmart.DAL;
using FoodSmart.Entity;

namespace FoodSmart.BLL
{
    public class ReportBAL
    {
        public DataSet GetBillRefundRegister(DateTime StartDate, DateTime EndDate, int LocationId, string BillType)
        {
            return new ReportDAL().GetBillRefundRegister(StartDate, EndDate, LocationId, BillType);
        }

        public DataSet GetBillwiseConsumption(DateTime StartDate, DateTime EndDate, int LocationId, string BillType, string Restaurant)
        { return new ReportDAL().GetBillwiseConsumption(StartDate, EndDate, LocationId, BillType, Restaurant); }
        public DataSet GroupItemWiseSalesRegister(DateTime StartDate, DateTime EndDate, int LocationId, string Restaurant)
        { return new ReportDAL().GroupItemWiseSalesRegister(StartDate, EndDate, LocationId, Restaurant); }

        public DataSet GetCardSummary(DateTime StartDate, DateTime EndDate, int LocationId)
        { return new ReportDAL().GetCardSummary(StartDate, EndDate, LocationId); }
        public DataSet GetCashierDaySummary(DateTime StartDate, DateTime EndDate, int LocationId)
        { return new ReportDAL().GetCashierDaySummary(StartDate, EndDate, LocationId); }
        public DataSet GetDaySummary(DateTime StartDate, DateTime EndDate, int LocationId)
        { return new ReportDAL().GetDaySummary(StartDate, EndDate, LocationId); }
        public DataSet GetLocation(int Id = 0, string LocType = "")
        { return new ReportDAL().GetLocation(Id, LocType); }
        public DataSet GetOutletLocation(int Id = 0)
        { return new ReportDAL().GetOutletLocation(Id); }
        //public DataSet GetRestaurant(int Id = 0, int LocId = 0)
        //{ return new ReportDAL().GetRestaurant(Id, LocId); }
        public static DataSet Getcompany()
        { return new ReportDAL().Getcompany(); }

        //public DataSet GetStockSummary()
        //{
        //    return new ReportDAL().GetStockSummary();
        //}

        //public DataSet GetStockDetail()
        //{
        //    return new ReportDAL().GetStockDetail();
        //}

        //public DataSet GetTransactionRegister()
        //{
        //    return new ReportDAL().GetTransactionRegister();
        //}

        public static List<BillRefundRegEntity> GetBillRefundRegister(ReportCriteria criteria)
        {
            return ReportDAL.GetBillRefundRegister(criteria);
        }


        public static List<TransactionRegisterEntity> GetTransactionRegister(DateTime fromDate, DateTime toDate, string itemType, string tranType, Int32 groupId, int finYr, int locId, int itemId)
        {
            return ReportDAL.GetTransactionRegister(fromDate, toDate, itemType, tranType, groupId, finYr, locId, itemId);
        }

        public static List<ItemListEntity> GetItemList(ReportCriteria criteria)
        {
            return ReportDAL.GetItemList(criteria);
        }

        public static List<StockDetailEntity> GetStockDetail(DateTime fromDate, DateTime toDate, List<int> groups, List<Int64> items)
        {
            return ReportDAL.GetStockDetail(fromDate, toDate, groups, items);
        }

        public static List<StockSummaryEntity> GetStockSummary(DateTime asOnDate, List<int> groups, List<Int64> items)
        {
            return ReportDAL.GetStockSummary(asOnDate, groups, items);
        }

        public static List<CounterSummaryEntity> GetDaySummaryGST(ReportCriteria criteria)
        {
            return ReportDAL.GetDaySummaryGST(criteria);
        }

        public static List<GroupWiseSaleEntity> GetItemGroupSummaryGST(ReportCriteria criteria)
        {
            return ReportDAL.GetItemGroupSummaryGST(criteria);
        }

        public static List<BillWiseSaleEntity> GetBillWiseConsumption(ReportCriteria criteria)
        {
            return ReportDAL.GetBillWiseConsumption(criteria);
        }

        public static List<CashierSummaryEntity> GetCashierSummary(ReportCriteria criteria)
        {
            return ReportDAL.GetCashierSummary(criteria);
        }
    }
}
