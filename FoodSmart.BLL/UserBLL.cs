﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FoodSmart.DAL;
using FoodSmart.Common;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using System.Web;
using FoodSmart.Utilities.ResourceManager;
using FoodSmart.Utilities.Cryptography;
using System.Configuration;
using System.Data;
using System.Reflection;
using FoodSmart.BLL;


namespace FoodSmart.BLL
{
    public class UserBLL
    {
       
        #region User

        public DataSet GetAllUsers(SearchCriteria searchCriteria)
        {
            return UserDAL.GetAllUsers(searchCriteria);
        }

        public DataSet GetAllRoles()
        {
            return UserDAL.GetAllRoles();
        }

        public IUser GetUserForEdit(int ID, string CalledFrom, int UserID)
        {
            return UserDAL.GetUserForEdit(ID, CalledFrom);

        }

        public static string GetDefaultPassword()
        {
            return Encryption.Encrypt(Constants.DEFAULT_PASSWORD);
        }

        public bool ValidateUser(IUser user)
        {
            UserDAL.ValidateUser(user);
            return (user.Id > 0) ? true : false;

        }

        public bool ValidateWebUser(IUser user)
        {
            UserDAL.ValidateWebUser(user);
            return (user.Id > 0) ? true : false;

        }

        public static int GetLoggedInUserId()
        {
            int userId = 0;

            if (!ReferenceEquals(System.Web.HttpContext.Current.Session[Constants.SESSION_USER_INFO], null))
            {
                IUser user = (IUser)System.Web.HttpContext.Current.Session[Constants.SESSION_USER_INFO];

                if (!ReferenceEquals(user, null))
                {
                    userId = user.Id;
                }
            }

            return userId;
        }

        public static bool GetUserLocationSpecific()
        {
            bool UserLocSpecific = false;

            if (!ReferenceEquals(System.Web.HttpContext.Current.Session[Constants.SESSION_USER_INFO], null))
            {
                IUser user = (IUser)System.Web.HttpContext.Current.Session[Constants.SESSION_USER_INFO];

                if (!ReferenceEquals(user, null))
                {
                    //UserLocSpecific = user.UserlocationSpecific;
                }
            }

            return UserLocSpecific;
        }

        public static int GetUserLocation()
        {
            int uloc = 0;

            if (!ReferenceEquals(System.Web.HttpContext.Current.Session[Constants.SESSION_USER_INFO], null))
            {
                IUser user = (IUser)System.Web.HttpContext.Current.Session[Constants.SESSION_USER_INFO];

                if (!ReferenceEquals(user, null))
                {
                    uloc = user.LocID;
                    //uloc = user.PortID;
                }
            }

            return uloc;
        }

        public static int GetLoggedInUserRoleId()
        {
            int roleId = 0;

            if (!ReferenceEquals(System.Web.HttpContext.Current.Session[Constants.SESSION_USER_INFO], null))
            {
                IUser user = (IUser)System.Web.HttpContext.Current.Session[Constants.SESSION_USER_INFO];

                if (!ReferenceEquals(user, null) && user.Id > 0)
                {
                    if (!ReferenceEquals(user.RoleID, null))
                    {
                        roleId = user.RoleID;
                    }
                }
            }

            return roleId;
        }

        public DataSet GetUserByLoc(int LocID)
        {
            return UserDAL.GetUserByLoc(LocID);
        }

        private static void SetDefaultSearchCriteriaForUser(SearchCriteria searchCriteria)
        {
            searchCriteria.SortExpression = "UserName";
            searchCriteria.SortDirection = "ASC";
        }

        public static List<IUser> GetAllUserList(SearchCriteria searchCriteria)
        {
            return UserDAL.GetUserList(false, searchCriteria);
            
        }

        public List<IUser> GetActiveUserList()
        {
            SearchCriteria searchCriteria = new SearchCriteria();
            SetDefaultSearchCriteriaForUser(searchCriteria);
            return UserDAL.GetUserList(true, searchCriteria);
            
        }

        //public IUser GetUser(int userId)
        //{
        //    SearchCriteria searchCriteria = new SearchCriteria();
        //    SetDefaultSearchCriteriaForUser(searchCriteria);
        //    return UserDAL.GetUser(userId, false, searchCriteria);
            
        //}

        public static IUser GetUser(int userId)
        {
            SearchCriteria searchCriteria = new SearchCriteria();
            SetDefaultSearchCriteriaForUser(searchCriteria);
            return UserDAL.GetUser(userId, false, searchCriteria);
        }


        public string GetUserLoc(int userID)
        {
            return UserDAL.GetUserLoc(userID);
        }

        public DataSet GetUserById(string LoginId)
        {
            return UserDAL.GetUserById(LoginId);
        }

        public static IActionResult SaveUser(IUser user, int modifiedBy)
        {
            string message = string.Empty;
            int result = UserDAL.SaveUser(user, modifiedBy);
            IActionResult actionResult = new ActionResult(result);

            if (result == -3) //Duplicate record exists
                message = ResourceManager.GetStringWithoutName("R00039");
            else if (result == -1) //Transaction error
                message = ResourceManager.GetStringWithoutName("R00020");
            else if (result == 0) //No records saved
                message = ResourceManager.GetStringWithoutName("R00016");
            else if (result > 0) //Successfully saved
                message = ResourceManager.GetStringWithoutName("R00014");

            actionResult.Message = message;
            return actionResult;
        }

        //public string SaveUser(IUser user, int modifiedBy)
        //{
        //    int result = 0;
        //    string errMessage = string.Empty;
        //    result = UserDAL.SaveUser(user, modifiedBy);

        //    switch (result)
        //    {
        //        case 1:
        //            errMessage = "Save Error";
        //            break;
        //        default:
        //            break;
        //    }

        //    return errMessage;

        //}

        public static void DeleteUser(int userId, int modifiedBy)
        {
            UserDAL.DeleteUser(userId, modifiedBy);
            //fSmartWS.DeleteUser(userId, modifiedBy);
        }

        public static bool ChangePassword(IUser user)
        {
            return UserDAL.ChangePassword(user);
            
        }

        public bool CheckDuplicateLogin(string LoginID)
        {
            return UserDAL.CheckDuplicateLogin(LoginID);
        }

        public int AddEditUser(IUser user, string mode, int CreatedBy)
        {
            return UserDAL.AddEditUser(user, mode, CreatedBy);

        }

        public static void ResetPassword(IUser user, int modifiedBy)
        {
            user.Password = GetDefaultPassword();
            UserDAL.ResetPassword(user, modifiedBy);
            
        }

        public DataSet GetAdminUsers(int LoggedUserId, int locationId)
        {
            return FoodSmart.DAL.UserDAL.GetAdminUsers(LoggedUserId, locationId);
            //return fSmartWS.GetAdminUsers(LoggedUserId, locationId);
        }


        public void SavePOSUserLogin(IUser usr, string UpdateType)
        {
            UserDAL.SavePOSUserLogin(usr, UpdateType);
            
        }

        public static DataSet GetRoleWithUserID(int LoggedUserId)
        {
            return UserDAL.GetRoleWithUserID(LoggedUserId);
        }

        #endregion

        #region Role

        public static List<IRole> GetActiveRole()
        {
            SearchCriteria searchCriteria = new SearchCriteria();
            SetDefaultSearchCriteriaForRole(searchCriteria);
            return UserDAL.GetRole(true, searchCriteria);
        }

        private static void SetDefaultSearchCriteriaForRole(SearchCriteria searchCriteria)
        {
            searchCriteria.SortExpression = "Role";
            searchCriteria.SortDirection = "ASC";
        }

        public List<IRole> GetAllRole(SearchCriteria searchCriteria)
        {
            return UserDAL.GetRole(false, searchCriteria);
            //dynamic roleList = fSmartWS.GetAllRole(searchCriteria);

            //List<IRole> lstRole = new List<IRole>();
            //IRole role = new FoodSmart.Entity.RoleEntity();
            //foreach (dynamic rl in roleList)
            //{
            //    foreach (dynamic prop in typeof(IUser).GetProperties())
            //    {
            //        dynamic val = rl.GetType().GetProperty(prop.Name).GetValue(rl, null);
            //        PropertyInfo UEProp = role.GetType().GetProperty(prop.Name);
            //        UEProp.SetValue(role, val, null);
            //    }
            //    lstRole.Add(role);
            //}

            //return lstRole;
        }

        public static IRole GetRole(int roleId)
        {

            SearchCriteria searchCriteria = new SearchCriteria();
            SetDefaultSearchCriteriaForRole(searchCriteria);
            return UserDAL.GetRole(roleId, false, searchCriteria);
            
        }

        public string SaveRole(List<RoleMenuEntity> lstRoleMenu, IRole role, int modifiedBy)
        {
            int result = 0;
            string errMessage = string.Empty;
            string xmlDoc = GeneralFunctions.Serialize(lstRoleMenu);
            result = UserDAL.SaveRole(role, Constants.DEFAULT_COMPANY_ID, xmlDoc, modifiedBy);
            //FoodSmart.BLL.UserService.RoleEntity RE = new RoleEntity();
            //if (role != null)
            //{
            //    foreach (dynamic prop in typeof(IRole).GetProperties())
            //    {
            //        dynamic val = role.GetType().GetProperty(prop.Name).GetValue(role, null);
            //        PropertyInfo REProp = RE.GetType().GetProperty(prop.Name);
            //        REProp.SetValue(RE, val, null);
            //    }
            //}
            //result = fSmartWS.SaveRole(RE, xmlDoc, Constants.DEFAULT_COMPANY_ID, modifiedBy);

            switch (result)
            {
                case 1:
                    errMessage = ResourceManager.GetStringWithoutName("ERR00072");
                    break;
                default:
                    break;
            }

            return errMessage;
        }

        public void DeleteRole(int roleId, int modifiedBy)
        {
            UserDAL.DeleteRole(roleId, modifiedBy);
        }

        public void ChangeRoleStatus(int roleId, bool status, int modifiedBy)
        {
            UserDAL.ChangeRoleStatus(roleId, status, modifiedBy);
        }

        public List<IRoleMenu> GetMenuByRole(int roleId, int mainId)
        {
            return UserDAL.GetMenuByRole(roleId, mainId);
            
        }

        public IUserPermission GetMenuAccessByUser(int userId, int menuId)
        {
            IUserPermission userPermission = new FoodSmart.Entity.UserPermission();

            IRoleMenu roleMenuAccess = UserDAL.GetMenuAccessByUser(userId, menuId);
            if (!ReferenceEquals(roleMenuAccess, null))
            {
                userPermission.CanAdd = roleMenuAccess.CanAdd;
                userPermission.CanEdit = roleMenuAccess.CanEdit;
                userPermission.CanDelete = roleMenuAccess.CanDelete;
                userPermission.CanView = roleMenuAccess.CanView;
            }
           
            return userPermission;
        }

        public static void GetUserPermission(out bool canAdd, out bool canEdit, out bool canDelete, out bool canView)
        {
            canAdd = false;
            canEdit = false;
            canDelete = false;
            canView = false;

            if (!ReferenceEquals(System.Web.HttpContext.Current.Session[Constants.SESSION_USER_PERMISSION], null))
            {
                IUserPermission userPermission = (IUserPermission)System.Web.HttpContext.Current.Session[Constants.SESSION_USER_PERMISSION];

                if (!ReferenceEquals(userPermission, null))
                {
                    canAdd = userPermission.CanAdd;
                    canEdit = userPermission.CanEdit;
                    canDelete = userPermission.CanDelete;
                    canView = userPermission.CanView;
                }
            }
        }

        public DataTable GetUserSpecificMenuList(int UserID)
        {
            return UserDAL.GetUserSpecificMenuList(UserID);
        }

        #endregion
    }
}
