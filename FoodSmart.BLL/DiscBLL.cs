﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FoodSmart.DAL;
using FoodSmart.Common;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using System.Web;
using FoodSmart.Utilities.ResourceManager;
using FoodSmart.Utilities.Cryptography;
using System.Data;
using System.Configuration;

namespace FoodSmart.BLL
{
    public class DiscBLL
    {
        public DataSet GetAllDiscounts(SearchCriteria searchCriteria)
        {
            return DiscDAL.GetAllDiscounts(searchCriteria);
        }

        public int SaveDiscounts(IDisc Disc, string Stat)
        {
            return DiscDAL.SaveDiscount(Disc, Stat);
        }

        public IDisc GetDiscForEdit(int ID)
        {
            return DiscDAL.GetDiscForEdit(ID);
        }
        public void DeleteDiscount(int DiscId)
        {
            DiscDAL.DeleteDiscount(DiscId);
        }

        public DataSet GetAllDiscType()
        {
            return DiscDAL.GetAllDiscType();
        }

        public DataSet GetAllDiscCalcMethod(int DisTypeID, int CalcID)
        {
            return DiscDAL.GetAllDiscCalcMethod(DisTypeID, CalcID);
        }

    }
}
