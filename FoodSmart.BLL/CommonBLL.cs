﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using FoodSmart.DAL;
using FoodSmart.Common;
using FoodSmart.Entity;
using FoodSmart.Utilities.ResourceManager;
using FoodSmart.Utilities;
using FoodSmart.Utilities.Cryptography;
using FoodSmart.DAL.DbManager;
using System.Net.Mail;
using System.Data;
using System.Web.UI.WebControls;
using FoodSmart.BLL;

namespace FoodSmart.BLL
{
    public class CommonBLL
    {
     
        #region Common

        public static int SaveCompanyDetail(ICompany user, int modifiedBy)
        {
            int result = CommonDAL.SaveCompanyDetail(user, modifiedBy);
            return result;
        }

        
        
        /// <summary>
        /// Handles the exception.
        /// </summary>
        /// <param name="ex">The <see cref="System.Exception"/> object.</param>
        /// <param name="logFilePath">The log file path.</param>
        /// <createdby>Amit Kumar Chandra</createdby>
        /// <createddate>02/12/2012</createddate>
        public static void HandleException(Exception ex, string logFilePath)
        {
            int userId = 0;
            string userDetail = string.Empty;
            string baseException = string.Empty;

            if (ex.GetType() != typeof(System.Threading.ThreadAbortException))
            {
                if (System.Web.HttpContext.Current.Session[Constants.SESSION_USER_INFO] != null)
                {
                    IUser user = (IUser)System.Web.HttpContext.Current.Session[Constants.SESSION_USER_INFO];

                    if (!ReferenceEquals(user, null))
                    {
                        userId = user.Id;
                        //userDetail = user.Id.ToString() + ", " + user.FirstName + " " + user.LastName;
                    }
                }

                if (ex.GetBaseException() != null)
                {
                    baseException = ex.GetBaseException().ToString();
                }
                else
                {
                    baseException = ex.StackTrace;
                }

                try
                {
                    CommonDAL.SaveErrorLog(userId, ex.Message, baseException);
                }
                catch
                {
                    //try
                    //{
                    //    string message = DateTime.UtcNow.ToShortDateString().ToString() + " "
                    //            + DateTime.UtcNow.ToLongTimeString().ToString() + " ==> " + "User Id: " + userDetail + "\r\n"
                    //            + ex.GetBaseException().ToString();

                    //    GeneralFunctions.WriteErrorLog(logFilePath + LogFileName, message);
                    //}
                    //catch
                    //{
                    //    // Consume the exception.
                    //}
                }
            }
        }

        /// <summary>
        /// Common method to populate all the dropdownlist throughout the application
        /// </summary>
        /// <param name="Number">Unique Number</param>
        /// <returns>DataTable</returns>
        /// <createdby>Rajen Saha</createdby>
        /// <createddate>01/12/2012</createddate>
        public static void PopulateDropdown(int Number, DropDownList ddl, int? Filter1, int? Filter2)
        {
            ddl.DataSource = CommonDAL.PopulateDropdown(Number, Filter1, Filter2);
            ddl.DataValueField = "Value";
            ddl.DataTextField = "Text";
            ddl.DataBind();
        }

        #endregion


        #region Import Export From Local

        //+AR 07/06/2017
        public static DataTable GetPOSDayEnd()
        {
            return CommonDAL.GetPOSDayEnd();
        }

        public static DataTable UpDatePOSDayEnd()
        {
            return CommonDAL.UpDatePOSDayEnd();
        }
        //-AR 07/06/2017

        public static DataTable MasterUpdateAllowed()
        {
            return CommonDAL.MasterUpdateAllowed("MST_MASTERUPDATESTATUS");
        }

        public static void UpdateStatus(bool needInsert, string status)
        {

            CommonDAL.UpdateStatus("MST_MASTERUPDATESTATUS", status, (needInsert == true ? "A" : "E"));
        }

        public static void ExportFromLocal()
        {
            DataSet dsPosWeb;
            DataTable dtPosBillHdr = new DataTable();
            DataTable dtPosBillDtl = new DataTable();
            DataRow drHdr;
            DataRow[] drDetail;

            DBInteractionForExpImp.DBInteractionService expService = new DBInteractionForExpImp.DBInteractionService();
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["WsURL"]))
            {
                expService.Url = ConfigurationManager.AppSettings["WsURL"].ToString() + "/DBInteraction.asmx";
            }

            try
            {
                DataSet ds = CommonDAL.ExportFromLocal();
                for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                {
                    dsPosWeb = new DataSet();
                    dtPosBillHdr = ds.Tables[1].Clone();
                    dtPosBillDtl = ds.Tables[0].Clone();

                    drHdr = ds.Tables[1].Rows[i];
                    dtPosBillHdr.ImportRow(drHdr);

                    drDetail = ds.Tables[0].Select("fk_BILLID=" + drHdr["pk_BILLID"]);
                    foreach (DataRow dr in drDetail)
                    {
                        dtPosBillDtl.ImportRow(dr);
                    }
                    dtPosBillHdr.TableName = "POSHdr";
                    dtPosBillDtl.TableName = "POSDtl";

                    dsPosWeb.Tables.Add(dtPosBillDtl);
                    dsPosWeb.Tables.Add(dtPosBillHdr);

                    bool retVal = expService.ExportFromLocal(dsPosWeb);
                    if (retVal)
                    {
                        CommonDAL.UpdateLocalAfterTransfer(drHdr["pk_BILLID"].ToInt());
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public static bool ImportFromWeb()
        {
            bool retVal = false;
            try
            {
                DBInteractionForExpImp.DBInteractionService expService = new DBInteractionForExpImp.DBInteractionService();
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["WsURL"]))
                {
                    expService.Url = ConfigurationManager.AppSettings["WsURL"].ToString() + "/DBInteraction.asmx";
                }
                DataSet dsMasters = expService.ImportFromWeb();
                dsMasters.Tables[0].TableName = "mstITEMGROUPS";
                dsMasters.Tables[1].TableName = "mstITEMS";
                dsMasters.Tables[2].TableName = "mstITEMRATES";
                CommonDAL.ImportFromWeb(dsMasters);
                retVal = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return retVal;
        }

        #endregion

        #region Import Export From Web

        public static bool ImportFromLocal(DataSet dsPosWeb)
        {
            IInvoice inv = new FoodSmart.Entity.InvoiceEntity();


            DataRow dr = dsPosWeb.Tables["POSHdr"].Rows[0];

            inv.BillDate = Convert.ToDateTime(dr["BillDate"]);
            inv.CreatedOn = Convert.ToDateTime(dr["AddDate"]);

            inv.BillType = dr["BillType"].ToString();
            inv.LocID = dr["fk_LocId"].ToInt();
            inv.PaymentMode = dr["PaymentMode"].ToString();
            inv.RefundReasonID = dr["fk_RefundReason"].ToInt();

            inv.DiscountPer = dr["DiscPer"].ToDecimal();
            inv.DiscReasonID = dr["fk_DiscReasonID"].ToInt();
            inv.MobileNo = dr["MobileNo"].ToString();
            inv.CustName = dr["CustName"].ToString();

            inv.RoundOff = dr["RoundOff"].ToDecimal();
            //inv.RestID = dr["FK_RESTID"].ToInt();

            //inv.SerialNo = card.CurrentSerial;
            inv.YearID = dr["fk_YearID"].ToInt();
            inv.UserID = dr["fk_UserID"].ToInt();
            inv.MachineName = dr["MachineName"].ToString();

            inv.CashAmount = dr["CashAmount"].ToDecimal();
            inv.CCAmount = dr["CCAmount"].ToDecimal();
            inv.eWalletAmount = dr["EWalletAmount"].ToDecimal();
            inv.MPAmount = dr["MPAmount"].ToDecimal();
            inv.CouponAmt = dr["CouponAmt"].ToDecimal();

            inv.Prefix = dr["Prefix"].ToString();
            inv.CouponID = dr["fk_CouponID"].ToInt();
            inv.CouponNo = dr["CouponNo"].ToInt();

            InvoiceBLL invBill = new InvoiceBLL();
            int retval = invBill.SaveInvoiceServer(inv, "A", dsPosWeb);

            return (retval >= 1) ? true : false;
        }

        public static DataSet ExportMasterFromWeb()
        {
            return CommonDAL.ExportMasterFromWeb();
        }

        //public static DataSet CheckCoupon()
        //{
        //    DBInteractionForExpImp.DBInteractionService expService = new DBInteractionForExpImp.DBInteractionService();
        //    if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["WsURL"]))
        //    {
        //        expService.Url = ConfigurationManager.AppSettings["WsURL"].ToString() + "/DBInteraction.asmx";
        //    }
        //    DataSet dsMasters = expService.ImportFromWeb();
        //    return CommonDAL.CheckCoupon();
        //}

        #endregion


       
    }
}
