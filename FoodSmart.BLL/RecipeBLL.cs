﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FoodSmart.DAL;
using FoodSmart.Entity;
using System.Web;
using System.Data;
using FoodSmart.Common;

namespace FoodSmart.BLL
{
    public class RecipeBLL    {
        public static List<RecipeEntity> GetAllRecipies(SearchCriteria criteria)
        {
            criteria.Type = (criteria.Type == "A") ? null : criteria.Type;
            return RecipeDAL.GetAllRecipies(criteria);
        }

        public static RecipeEntity GetRecipe(int id)
        {
            return RecipeDAL.GetRecipe(id);
        }

        public static List<IRecipeItem> GetAllRecipieItems(Int64 id)
        {
            return RecipeDAL.GetAllRecipieItems(id);
        }

        public static int DeleteRecipe(int id)
        {
            return RecipeDAL.DeleteRecipe(id);
        }

        public static int SaveRecipe(IRecipe recipe, string mode, DataSet ds)
        {
            return RecipeDAL.SaveRecipe(recipe,mode,ds);
        }

        public static List<IRecipe> GetItems(string itemType)
        {
            return RecipeDAL.GetItems(itemType);
        }

        public static IRecipe GetItem(string itemType, int itemId)
        {
            return RecipeDAL.GetItem(itemType, itemId);
        }

        public static List<IItemGroup> GetItemGroups()
        {
            return RecipeDAL.GetItemGroups();
        }

        public static List<IItemGroup> GetItemGroupByType(string type)
        {
            return RecipeDAL.GetItemGroupByType(type);
        }

        public static List<IRecipe> GetItemsByGroup(string itemType, int itemGroupId)
        {
            return RecipeDAL.GetItemsByGroup(itemType, itemGroupId);
        }
    }
}
