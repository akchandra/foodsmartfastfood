﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FoodSmart.DAL;
using FoodSmart.Entity;
using System.Web;
using System.Data;
using FoodSmart.Common;

namespace FoodSmart.BLL
{
    public class TransactionBLL
    {
        public static List<TransactionEntity> GetAllTransactions(SearchCriteria criteria, int UserID)
        {
            criteria.Type = (criteria.Type == "A") ? string.Empty : criteria.Type;
            return TransactionDAL.GetAllTransactions(criteria, UserID);
        }

        public static TransactionEntity GetTransaction(Int64 id, int UserID)
        {
            return TransactionDAL.GetTransaction(id, UserID);
        }

  
        public static int DeleteTransaction(Int64 id)
        {
            return TransactionDAL.DeleteTransaction(id);
        }

        public static int SaveTransaction(ITransaction transaction, string mode, DataSet ds, int UserID)
        {
            return TransactionDAL.SaveTransaction(transaction, mode, ds, UserID);
        }

        public static List<ITransactionItem> GetAllTransactionItems(Int64 tranId)
        {
            return TransactionDAL.GetAllTransactionItems(tranId);
        }

        public static List<ITransactionItem> GetAllTransactionItems(string tranNo)
        {
            return TransactionDAL.GetAllTransactionItems(tranNo);
        }

        public static List<ILocation> GetLocations()
        {
            return TransactionDAL.GetLocations();
        }

        public static List<IVendor> GetVendors()
        {
            return TransactionDAL.GetVendors();
        }

        public static List<IStore> GetAllStore(string calledFrom)
        {
            return TransactionDAL.GetAllStore(calledFrom);
        }

        public static int DeleteTransaction(Int64 id, int UserID)
        {
            return TransactionDAL.DeleteTransaction(id, UserID);
        }
    }
}
