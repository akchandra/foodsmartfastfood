﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FoodSmart.DAL;
using FoodSmart.DAL.DBManager;
using FoodSmart.Common;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using System.Web;
using FoodSmart.Utilities.ResourceManager;
using FoodSmart.Utilities.Cryptography;
using System.Data;
using System.Configuration;
using System.Reflection;

namespace FoodSmart.BLL
{
    public class InvoiceBLL
    {
        public int SaveInvoiceTran(IInvoice Inv, string Mode, DataSet ds)
        {
            return InvoiceDAL.SaveInvoiceTran(Inv, Mode, ds);
        }

        public int SaveInvoiceServer(IInvoice Inv, string Mode, DataSet ds)
        {
            return InvoiceDAL.SaveInvoiceServer(Inv, Mode, ds);
        }

        public bool CheckRefundPossible(string CardNoInside, int Serial)
        {
            return InvoiceDAL.CheckRefundPossible(CardNoInside, Serial);
        }

        public DataSet CheckItemRefundPossible(int CardID, int Serial, int fk_ItemID, int RestID, DateTime Cdt)
        {
            return InvoiceDAL.CheckItemRefundPossible(CardID, Serial, fk_ItemID, RestID, Cdt);
        }

        public DataSet GetAllReason()
        {
            return InvoiceDAL.GetAllReason();
        }

        public DataSet GetDiscountReason()
        {
            return InvoiceDAL.GetDiscountReason();
        }

        public DataSet GetAllBills(int RestID)
        {
            return InvoiceDAL.GetAllBills(RestID);
        }

        public DataTable GetHotItems()
        {
            return InvoiceDAL.GetHotItems();
        }

        public DataSet DisplayDetailItems(SearchCriteria searchCriteria)
        {
            return InvoiceDAL.DisplayDetailItems(searchCriteria);
        }

        public DataSet GetCashTranForPrinting(int CashID)
        {
            return InvoiceDAL.GetCashTranForPrinting(CashID);
        }

        public DataSet GetInvoiceForPrinting(int InvoiceID, string InvType)
        {
            return InvoiceDAL.GetInvoiceForPrinting(InvoiceID, InvType);
        }

        public DataSet GetCashierSummaryForPrinting(int CashierID)
        {
            return InvoiceDAL.GetCashierSummaryForPrinting(CashierID);
        }

        public void SavePOSUserLogin(int userID)
        {
            InvoiceDAL.SavePOSUserLogin(userID);
        }

        public void UpdateTempCardStatus(string CardNoInside, bool stat)
        {
            InvoiceDAL.UpdateTempCardStatus(CardNoInside, stat);
        }

        public DataSet ItemWiseSalesRegisterScreen()
        {
            return InvoiceDAL.ItemWiseSalesRegisterScreen();
        }

        public DataSet GetBillWithDate(DateTime dt)
        {
            return InvoiceDAL.GetBillWithDate(dt);
        }

        public DataSet ItemWiseSalesRegister(int RestID)
        {
            return InvoiceDAL.ItemWiseSalesRegister(RestID);
        }

        public DataSet GetMobileNo(string MobileNo)
        {
            return InvoiceDAL.GetMobileNo(MobileNo);
        }
    }
}
