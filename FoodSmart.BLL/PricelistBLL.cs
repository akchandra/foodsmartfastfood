﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Data;
using System.Transactions;
using FoodSmart.DAL;
using FoodSmart.Entity;

using FoodSmart.Common;


namespace FoodSmart.BLL
{
    public class PricelistBLL
    {
        public static List<IItemRate> GetPriceList(SearchCriteria criteria)
        {
            return PricelistDAL.GetPriceList(criteria);
        }
        public static void SavePriceList(string mode, DateTime effectiveDate, DataSet ds)
        {
            using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew))
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Int64 itemId = Convert.ToInt64(ds.Tables[0].Rows[i]["pk_ItemID"]);
                    Int64 rateId = Convert.ToInt64(ds.Tables[0].Rows[i]["Id"]);
                    decimal price = Convert.ToDecimal(ds.Tables[0].Rows[i]["NewRate"]);
                    PricelistDAL.SavePriceList(mode, itemId, rateId, price, effectiveDate);
                }

                scope.Complete();
            }
        }
    }
}
