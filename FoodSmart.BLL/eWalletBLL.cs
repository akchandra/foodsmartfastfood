﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FoodSmart.DAL;
using FoodSmart.Common;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using System.Web;
using FoodSmart.Utilities.ResourceManager;
using FoodSmart.Utilities.Cryptography;
using System.Configuration;
using System.Data;
using System.Reflection;

namespace FoodSmart.BLL
{
    public class eWalletBLL
    {
        
        public DataSet GetAlleWallet(SearchCriteria searchCriteria)
        {
            return eWalletDAL.GetAlleWallets(searchCriteria);
        }

        public void DeleteEWallet(int WalletID, int UserID)
        {
            eWalletDAL.DeleteEWallet(WalletID, UserID);
        }

        public int SaveEWallet(IeWallet item, string Stat, int UserID)
        {
            return eWalletDAL.SaveEWallet(item, Stat, UserID);
        }

        public IeWallet GeteWalletForEdit(int ID, int UserID)
        {
            return eWalletDAL.GeteWalletForEdit(ID);
            
        }
    }
}
