﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FoodSmart.DAL;
using FoodSmart.DAL.DBManager;
using FoodSmart.Common;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using System.Web;
using FoodSmart.Utilities.ResourceManager;
using FoodSmart.Utilities.Cryptography;
using System.Data;
using System.Configuration;
using System.Reflection;

namespace FoodSmart.BLL
{
    public class InventoryBLL
    {
        

        #region Raw Material
        //public DataSet GetAllRM(SearchCriteria searchCriteria)
        //{
        //    return InventoryDAL.GetAllRM(searchCriteria);
        //}

        //public int SaveRM(IInventory itemgrp, string Stat, int userid)
        //{
        //    return InventoryDAL.SaveRM(itemgrp, Stat, userid);
        //}

        //public void DeleteRM(int RMID)
        //{
        //    InventoryDAL.DeleteRM(RMID);
        //}

        //public IInventory GetRMForEdit(int ID, int UserID)
        //{
        //    return InventoryDAL.GetRMForEdit(ID, "E");
        //}

        #endregion

        #region Raw Material Group

        //public DataSet GetAllRMGroup(SearchCriteria searchCriteria)
        //{
        //    return InventoryDAL.GetAllRMGroup(searchCriteria);
        //}

        //public int SaveRMGroup(IInventory itemgrp, string Stat)
        //{
        //    return InventoryDAL.SaveRMGroup(itemgrp, Stat);
        //}

        //public void DeleteRMGroup(int RMID)
        //{
        //    InventoryDAL.DeleteRMGroup(RMID);
        //}

        //public IInventory GetRMGroupForEdit(int ID, int UserID)
        //{
        //    return InventoryDAL.GetRMGroupForEdit(ID, "E");
            
        //}

        #endregion

        #region Inventory Transaction
        public DataSet GetAllTransaction(SearchCriteria searchCriteria, int UserID)
        {
            return InventoryDAL.GetAllInvTran(searchCriteria, UserID);
        }

        public int SaveInvTran(IInvTran itemgrp, string Stat, DataSet ds)
        {
            return InventoryDAL.SaveInvTran(itemgrp, Stat, ds);
        }
        public void DeleteInvTran(int RMID, int UserID)
        {
            InventoryDAL.DeleteInvTran(RMID, UserID);
        }

        public IInventory GetInvTranForEdit(int ID, int UserID)
        {
            return InventoryDAL.GetInvTranForEdit(ID, "E");
        }

        public DataSet GetAllInvItem(int TranID)
        {
            return InventoryDAL.GetAllInvItem(TranID);
        }
        #endregion

        #region Recipe
        public DataSet GetAllRecipe(SearchCriteria searchCriteria)
        {
            return InventoryDAL.GetAllRecipe(searchCriteria);
        }

        public DataSet GetAllRecipeItem(int TranID)
        {
            return InventoryDAL.GetAllRecipeItem(TranID);
        }

        public int SaveRecipe(IInventory itemgrp, string Stat, DataSet ds)
        {
            return InventoryDAL.SaveRecipe(itemgrp, Stat, ds);
        }

        public DataSet GetAllSF()
        {
            return InventoryDAL.GetAllSF();
        }

        public void DeleteRecipe(int RMID)
        {
            InventoryDAL.DeleteRecipe(RMID);
        }

        public IInventory GetSFForEdit(int ID, int UserID)
        {
            return InventoryDAL.GetSFForEdit(ID, "E");
        }

        public IInventory GetRecipeForEdit(int ID, int UserID)
        {
            return InventoryDAL.GetRecipeForEdit(ID, "E");
        }
        #endregion
    }
}
