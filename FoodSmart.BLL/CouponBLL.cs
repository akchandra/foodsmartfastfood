﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FoodSmart.DAL;
using FoodSmart.Common;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using System.Web;
using FoodSmart.Utilities.ResourceManager;
using FoodSmart.Utilities.Cryptography;
using System.Configuration;
using System.Data;
using System.Reflection;

namespace FoodSmart.BLL
{
    public class CouponBLL
    {
        
            
        #region item 
        public DataSet GetAllCoupons(SearchCriteria searchCriteria)
        {
            return CouponDAL.GetAllCoupons(searchCriteria);
        }

      
        public void DeleteCoupon(int CPID)
        {
            CouponDAL.DeleteCoupon(CPID);
        }

        public int SaveCoupon(ICoupon coupon, string Stat)
        {
            return CouponDAL.SaveCoupon(coupon, Stat);
        }

        public ICoupon GetCouponForEdit(int CPID)
        {
            return CouponDAL.GetCouponForEdit(CPID);
        }
        #endregion

        #region Webservice call 

        public DataSet CheckForCoupon(string prefix, int CouponNo, string QueryType)
        {
            DBInteractionForExpImp.DBInteractionService expService = new DBInteractionForExpImp.DBInteractionService();
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["WsURL"]))
            {
                expService.Url = ConfigurationManager.AppSettings["WsURL"].ToString() + "/DBInteraction.asmx";
            }
            DataSet PrefixExist = expService.CheckForCoupon(prefix, CouponNo, QueryType);
            return PrefixExist;
        }

        #endregion
    }
}
