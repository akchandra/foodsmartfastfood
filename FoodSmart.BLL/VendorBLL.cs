﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FoodSmart.DAL;
using FoodSmart.DAL.DBManager;
using FoodSmart.Common;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using System.Web;
using FoodSmart.Utilities.ResourceManager;
using FoodSmart.Utilities.Cryptography;
using System.Data;
using System.Configuration;
using System.Reflection;

namespace FoodSmart.BLL
{
    public class VendorBLL
    {
        

        #region Vendor
        public static DataSet GetAllVendor(SearchCriteria searchCriteria)
        {
            return VendorDAL.GetAllVendor(searchCriteria);
        }

        public static int SaveVendor(IVendor ven, string Stat)
        {
            return VendorDAL.SaveVendor(ven, Stat);
        }

        public static void DeleteVendor(int RMID)
        {
            //UserDAL.DeleteUser(userId, modifiedBy);
            VendorDAL.DeleteVendor(RMID);
        }

        public IVendor GetVendorForEdit(int ID)
        {
            return VendorDAL.GetVendorEdit(ID, "M");
            
        }

        #endregion


    }
}
