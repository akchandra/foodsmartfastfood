﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using FoodSmart.Entity;
using FoodSmart.BLL;

namespace FoodSmart.BLL
{
    public class DBInteraction
    {
        #region General
        public bool IsUnique(string tableName, string colName, string val)
        {

            DAL.DbManager.DbQuery dquery = new DAL.DbManager.DbQuery("Select count(*) from " + tableName + " where " + colName + " = '" + val + "'", true);
            bool returnval = false;
            try
            {
                returnval = Convert.ToInt32(dquery.GetScalar()) > 0 ? false : true;
            }
            catch (Exception)
            {


            }

            return returnval;

        }


        public DataSet PopulateDDLDS(string tableName, string textField, string valuefield)
        {

            DAL.DbManager.DbQuery dquery = new DAL.DbManager.DbQuery("Select [" + textField + "] ListItemValue, [" + valuefield + "] ListItemText from dbo." + tableName + " order by ListItemText", true);
            return dquery.GetTables();
        }

        public DataSet PopulateDDLDS(string tableName, string textField, string valuefield, bool isDSR)
        {
            DAL.DbManager.DbQuery dquery = new DAL.DbManager.DbQuery("Select [" + textField + "] ListItemValue, [" + valuefield + "] ListItemText from " + tableName + " order by ListItemText", true);
            return dquery.GetTables();
        }

        public DataSet PopulateDDLDS(string tableName, string textField, string valuefield, string WhereClause)
        {
            DAL.DbManager.DbQuery dquery = new DAL.DbManager.DbQuery("Select [" + textField + "] ListItemValue, [" + valuefield + "] ListItemText from dbo." + tableName + " " + WhereClause, true);
            return dquery.GetTables();
        }

        public DataSet PopulateDDLDS(string tableName, string textField, string valuefield, string WhereClause, bool isDSR)
        {

            DAL.DbManager.DbQuery dquery = new DAL.DbManager.DbQuery("Select [" + textField + "] ListItemValue, [" + valuefield + "] ListItemText from " + tableName + " " + WhereClause + ") order by ListItemText", true);
            return dquery.GetTables();

        }

        public int GetId(string ItemName, string ItemValue)
        {
            DAL.DbManager.DbQuery dquery = new DAL.DbManager.DbQuery("prcGetId");
            dquery.AddVarcharParam("@ItemName", 15, ItemName);
            dquery.AddVarcharParam("@ItemValue", 50, ItemValue);
            return Convert.ToInt32(dquery.GetScalar());
        }

        public void DeleteGeneral(string formName, int pk_tableID)
        {
            string ProcName = "prcDeleteGeneral";
            DAL.DbManager.DbQuery dquery = new DAL.DbManager.DbQuery(ProcName);
            dquery.AddIntegerParam("@pk_tableID", pk_tableID);
            dquery.AddVarcharParam("@formName", 20, formName);
            dquery.RunActionQuery();
        }

        public int InvoiceDateCheck(DateTime dt)
        {
            DAL.DbManager.DbQuery dquery = new DAL.DbManager.DbQuery("prcInvoiceDateCheck");
            dquery.AddDateTimeParam("@StaxDate", dt);
            return Convert.ToInt32(dquery.GetScalar());
        }
        #endregion

        #region Financial Year

        public DataSet GetFinancialYear(int QueryType, int YearID)
        {
            string ProcName = "dbo.SP_SELECTYEAR";
            DAL.DbManager.DbQuery dquery = new DAL.DbManager.DbQuery(ProcName);

            dquery.AddIntegerParam("@YrID", YearID);
            dquery.AddIntegerParam("@Query", QueryType);

            return dquery.GetTables();
            //return fSmartWS.GetFinancialYear(QueryType, YearID);
        }

        public DataSet GetCompany()
        {
            string ProcName = "[admin].[sp_GetCompanyDetail]";
            DAL.DbManager.DbQuery dquery = new DAL.DbManager.DbQuery(ProcName);
            return dquery.GetTables();
        }

        public int SaveParameters(int SecDep, string WebRptURL, int GraceTime, int NoChargePeriod)
        {
            return SaveParameters(SecDep, WebRptURL, GraceTime, NoChargePeriod);
        }

        public DataSet GetAllYear()
        {
            string ProcName = "[admin].[sp_ListYears]";
            DAL.DbManager.DbQuery dquery = new DAL.DbManager.DbQuery(ProcName);
            return dquery.GetTables();
        }

        #endregion

        #region STax

        public DataSet GetSTaxDate(DateTime? Startdt)
        {
            string ProcName = "admin.prcGetSTaxDate";
            DAL.DbManager.DbQuery dquery = new DAL.DbManager.DbQuery(ProcName);
            dquery.AddDateTimeParam("@StartDate", Startdt);
            return dquery.GetTables();
        }

        public DataSet GetSTax(int pk_StaxID, DateTime? StartDate)
        {
            string ProcName = "admin.prcGetSTax";
            DAL.DbManager.DbQuery dquery = new DAL.DbManager.DbQuery(ProcName);

            dquery.AddIntegerParam("@pk_StaxID", pk_StaxID);
            dquery.AddDateTimeParam("@StartDate", StartDate);

            return dquery.GetTables();
        }

        public int AddEditSTax(int userID, int pk_StaxID, DateTime? StartDate, decimal TaxAddCess, decimal TaxCess, decimal TaxPer, bool TaxStatus, bool isEdit)
        {
            string ProcName = "admin.prcAddEditSTax";
            DAL.DbManager.DbQuery dquery = new DAL.DbManager.DbQuery(ProcName);
            dquery.AddIntegerParam("@userID", userID);
            dquery.AddIntegerParam("@pk_StaxID", pk_StaxID);
            dquery.AddDateTimeParam("@StartDate", StartDate);
            dquery.AddDecimalParam("@TaxAddCess", 6, 2, TaxAddCess);
            dquery.AddDecimalParam("@TaxCess", 6, 2, TaxCess);
            dquery.AddDecimalParam("@TaxPer", 6, 2, TaxPer);
            dquery.AddBooleanParam("@TaxStatus", TaxStatus);
            dquery.AddBooleanParam("@isEdit", isEdit);

            return dquery.RunActionQuery();

        }

        #endregion
    }
}
