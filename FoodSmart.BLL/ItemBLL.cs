﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Configuration;
using System.Data;
using System.Reflection;
using FoodSmart.DAL;
using FoodSmart.Common;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using FoodSmart.Utilities.ResourceManager;
using FoodSmart.Utilities.Cryptography;


namespace FoodSmart.BLL
{
    public class ItemBLL
    {
        

        #region item Group
        public DataSet GetAllItemGroup(SearchCriteria searchCriteria)
        {
            return ItemDAL.GetAllItemGroup(searchCriteria);
        }

        public int SaveItemGroup(IItems itemgrp, string Stat)
        {
            return ItemDAL.SaveItemGroup(itemgrp, Stat);
        }

        public IItems GetItemGroupForEdit(int ID)
        {
            return ItemDAL.GetItemGroupForEdit(ID);
        }
        public void DeleteItemgroup(int ItemGroupId)
        {
            ItemDAL.DeleteItemGroup(ItemGroupId);
        }

        public int ChkItemGroupDelPossible(int CardID)
        {
            return ItemDAL.ChkItemGroupDelPossible(CardID);
        }
        #endregion
        #region item 
        public DataSet GetAllItems(SearchCriteria searchCriteria)
        {
            return ItemDAL.GetAllItems(searchCriteria);
        }

        public DataSet GetHotItems(SearchCriteria searchCriteria)
        {
            return ItemDAL.GetHotItems(searchCriteria);
        }

        public void DeleteItem(int ItemId)
        {
            ItemDAL.DeleteItem(ItemId);
        }

        public int SaveItem(IItems item, string Stat)
        {
            return ItemDAL.SaveItem(item, Stat);
        }

        public IItems GetItemForEdit(int ID)
        {
            return ItemDAL.GetItemForEdit(ID);
        }
        #endregion
        # region Item Rate
        public DataSet GetAllRateDate(string Action, int ItemID, DateTime EffDate)
        {
            return ItemDAL.GetAllRateDate(Action, ItemID, EffDate);
        }
        public DataSet GetAllItemRates(SearchCriteria searchCriteria)
        {
            //return UserDAL.GetAllUsers(searchCriteria);
            return ItemDAL.GetAllItemRates(searchCriteria);
        }

        public int SaveItemRate(IItems item, string Stat, int UserAdded)
        {
            return ItemDAL.SaveItemRate(item, Stat, UserAdded);
        }
        #endregion
        #region item Opening Balance
        public DataSet GetAllItemOpbal(SearchCriteria searchCriteria, int Curyr)
        {
            //return UserDAL.GetAllUsers(searchCriteria);
            return ItemDAL.GetAllItemOpbal(searchCriteria, Curyr);
            //return ItemDAL.GetAllItemGroups(searchCriteria);
        }

        public int SaveItemOpbal(IItems itemgrp, string Stat)
        {
            //RetailSmart.BLL.itemService.ItemsEntity IE = new ItemsEntity();
            //if (itemgrp != null)
            //{
            //    foreach (dynamic prop in typeof(IItems).GetProperties())
            //    {
            //        dynamic val = itemgrp.GetType().GetProperty(prop.Name).GetValue(itemgrp, null);
            //        PropertyInfo UEProp = IE.GetType().GetProperty(prop.Name);
            //        UEProp.SetValue(IE, val, null);
            //    }
            //}
            return ItemDAL.SaveItemOpbal(itemgrp, Stat);
        }


        public IItems GetItemOpbalForEdit(int ID)
        {
            return ItemDAL.GetItemOpbalForEdit(ID);
            //RetailSmart.BLL.itemService.ItemsEntity grp = fSmartWS.GetItemGroupForEdit(ID);
            //IItems IC = new Entity.ItemsEntity();
            //if (grp != null)
            //{
            //    foreach (var prop in typeof(RetailSmart.BLL.itemService.ItemsEntity).GetProperties())
            //    {
            //        PropertyInfo ICProp = IC.GetType().GetProperty(prop.Name);
            //        ICProp.SetValue(IC, prop.GetValue(grp, null), null);
            //    }
            //}
            //return IC;

        }
        public void DeleteItemOpbal(int ItemGroupId)
        {
            //UserDAL.DeleteUser(userId, modifiedBy);
            ItemDAL.DeleteItemOpbal(ItemGroupId);
        }

        //public int ChkItemGroupDelPossible(int CardID)
        //{
        //    //return CardDAL.ChkDelPossible(CardID);
        //    return ItemDAL.ChkItemGroupDelPossible(CardID);
        //}
        #endregion

        #region RM item
        public DataSet GetAllRMItems(SearchCriteria searchCriteria)
        {
            return ItemDAL.GetAllRMItems(searchCriteria);
        }

        public void DeleteRM(int ItemId, int UserID)
        {
            ItemDAL.DeleteRM(ItemId, UserID);
        }

        public int SaveRM(IItems item, string Stat)
        {
            return ItemDAL.SaveRM(item, Stat);
        }

        public IItems GetRMForEdit(int ID)
        {
            return ItemDAL.GetRMForEdit(ID);
        }

        public DataSet GetRMForStockTake(SearchCriteria searchCriteria)
        {
            return ItemDAL.GetRMForStockTake(searchCriteria);
        }

        #endregion 

        #region item Group
        public DataSet GetAllRMGroup(SearchCriteria searchCriteria)
        {
            return ItemDAL.GetAllRMGroup(searchCriteria);
        }

        public int SaveRMGroup(IItems itemgrp, string Stat)
        {
            return ItemDAL.SaveRMGroup(itemgrp, Stat);
        }

        public IItems GetRMGroupForEdit(int ID)
        {
            return ItemDAL.GetRMGroupForEdit(ID);
        }
        public void DeleteRMGroup(int ItemGroupId, int userID)
        {
            ItemDAL.DeleteRMGroup(ItemGroupId, userID);
        }

        #endregion
    }
}
