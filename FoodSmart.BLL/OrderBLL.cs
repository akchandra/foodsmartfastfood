﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FoodSmart.DAL;
using FoodSmart.Entity;
using System.Web;
using System.Data;
using FoodSmart.Common;

namespace FoodSmart.BLL
{
    public class OrderBLL
    {
        public static List<OrderEntity> GetAllOrders(SearchCriteria criteria)
        {
            criteria.Type = (criteria.Type == "A") ? string.Empty : criteria.Type;
            return OrderDAL.GetAllOrders(criteria);
        }

        public static OrderEntity GetOrder(int id)
        {
            return OrderDAL.GetOrder(id);
        }

        public static List<IOrderItem> GetAllTransactionItems(int tranId)
        {
            return OrderDAL.GetAllOrderItems(tranId);
        }

        public static int SaveOrders(IOrder transaction, string mode, DataSet ds)
        {
            return OrderDAL.SaveOrders(transaction, mode, ds);
        }

        public void DeleteRecipe(int RMID)
        {
            OrderDAL.DeleteOrder(RMID);
        }

        public static string GetStandardOrderForLoc(int RMID)
        {
            return OrderDAL.GetStandardOrderForLoc(RMID);
        }
    }
}
