﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FoodSmart.DAL;
using FoodSmart.Common;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using System.Web;
using FoodSmart.Utilities.ResourceManager;
using FoodSmart.Utilities.Cryptography;
using System.Configuration;
using System.Data;
using System.Reflection;

namespace FoodSmart.BLL
{
    public class ImprestTranBLL
    {
        public DataSet GetAllTransactions(SearchCriteria searchCriteria)
        {
            return ImprestTranDAL.GetAllTransactions(searchCriteria);
        }

        public ICashierImprest GetImprestData(int ImpID)
        {
            return ImprestTranDAL.GetImprestData(ImpID);
        }

        public int SaveImprest(ICashierImprest Imprest, int UserID)
        {
            int result = 0;
            result = ImprestTranDAL.SaveImprest(Imprest, UserID);
            return result;

        }
    }
}
