﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FoodSmart.DAL;
using FoodSmart.Common;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using System.Web;
using FoodSmart.Utilities.ResourceManager;
using FoodSmart.Utilities.Cryptography;
using System.Data;
using System.Configuration;

namespace FoodSmart.BLL
{
    public class LocationBLL
    {
        //+AR 07/06/2017
        public DataSet GetAndUpdateLocFromWeb()
        {
            DataSet ds = new DataSet();
            int defaultLoc = 0;
            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.CurrLoc = 1;

            try
            {
                ILocation loc = new LocationEntity();

                DataTable dtLocLocal = GetAllLoc(searchCriteria).Tables[0].Copy();
                dtLocLocal.TableName = "LocLocal";
                ds.Tables.Add(dtLocLocal);

                if (dtLocLocal.Rows.Count > 0)
                {
                    defaultLoc = dtLocLocal.Rows[0]["pk_LocID"].ToInt();
                }

                DBInteractionForExpImp.DBInteractionService expService = new DBInteractionForExpImp.DBInteractionService();
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["WsURL"]))
                {
                    expService.Url = ConfigurationManager.AppSettings["WsURL"].ToString() + "/DBInteraction.asmx";
                }
                DataSet ds1 = expService.ImportLocationFromWeb();
                DataSet ds2 = expService.ImportUsersFromWeb();
                DataSet ds3 = expService.GetCompany();

                ds.Tables.Add(expService.ImportLocationFromWeb().Tables[0].Copy());
                ds.Tables[1].TableName = "LocWeb";
                ds.Tables[1].Columns.Remove("LoctypeDesc");
                ds.Tables[1].Columns.Remove("OwnOrFranDesc");
                
                SaveAllLocation(ds.Tables["LocWeb"], defaultLoc, "A");

                ds.Tables.Add(expService.ImportUsersFromWeb().Tables[0].Copy());
                ds.Tables[2].TableName = "UserWeb";

                SaveAllUsers(ds.Tables["UserWeb"], "A", defaultLoc);

                SaveCompCalcMethod(ds3.Tables[0].Rows[0]["CalculationMethod"].ToString());

                //ds.Tables[1].Columns.Remove("LoctypeDesc");
                //ds.Tables[1].Columns.Remove("OwnOrFranDesc");


            }
            catch (Exception ex)
            {
                throw new Exception(defaultLoc.ToString());
            }
            return ds;
        }

        public void SaveAllLocation(DataTable dt, int defaultLoc, string Mode)
        {
            LocationDAL.SaveAllLocation(dt, defaultLoc, Mode);
        }

        public void SaveAllUsers(DataTable dt, string Mode, int LocID)
        {
            LocationDAL.SaveAllUsers(dt, Mode, LocID);
        }

        //-AR 07/06/2017
        public static List<ILocation> GetAllWebLoc(SearchCriteria searchCriteria)
        {
            return LocationDAL.GetAllWebLoc(searchCriteria);
        }

        public DataSet GetAllLoc(SearchCriteria searchCriteria)
        {
            return LocationDAL.GetAllLoc(searchCriteria);
        }

        public ILocation GetLocData(int LocID)
        {
            return LocationDAL.GetLocData(LocID);
        }

        public static int DeleteLoc(int LocID, int modifiedBy)
        {
            return LocationDAL.DeleteLoc(LocID, modifiedBy);
            //fSmartWS.DeleteUser(userId, modifiedBy);
        }

        public static int SaveLocation(ILocation Loc, int modifiedBy, string mode)
        {
            int result = LocationDAL.SaveLocation(Loc, modifiedBy, mode);
            return result;
        }

        public void SaveCompCalcMethod(string CalcMethod)
        {
            LocationDAL.SaveCompCalcMethod(CalcMethod);
        }
        //public int SaveLocation(ILocation loc, string Mode)
        //{
        //    int result = 0;
        //    result = LocationDAL.SaveLocation(loc, Mode);
        //    return result;
        //}

        //public int DeleteLoc(int locID, int UserID)
        //{
        //    int result = 0;
        //    result = LocationDAL.DeleteLoc(locID);
        //    return result;
        //}

        //public static int ChkDelPossible(int LocID)
        //{
        //    int result = 0;
        //    result = LocationDAL.ChkLocDelPossible(LocID);
        //    return result;
        //}

    }
}
