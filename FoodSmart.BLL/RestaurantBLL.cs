﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FoodSmart.DAL;
using FoodSmart.Common;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using System.Web;
using FoodSmart.Utilities.ResourceManager;
using FoodSmart.Utilities.Cryptography;
using System.Configuration;
using System.Data;
using System.Reflection;

namespace FoodSmart.BLL
{
    public class RestaurantBLL
    {
        
        public DataSet GetAllRestaurant(SearchCriteria searchCriteria)
        {
            return RestaurantDAL.GetAllRestaurant(searchCriteria);
        }

        public int SaveRestautant(IRestaurant rest, string mode, int UserID)
        {
            int result = 0;
            result = RestaurantDAL.SaveRestaurant(rest, mode, UserID);
            return result;
            
        }

        public IRestaurant GetRestData(int RestID)
        {
            return RestaurantDAL.GetRestData(RestID);
        }

        public void DeleteRest(int RestID)
        {
            RestaurantDAL.DeleteRest(RestID);

        }

    }
}
