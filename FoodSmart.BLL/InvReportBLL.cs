﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using FoodSmart.Common;
using FoodSmart.DAL;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using FoodSmart.Utilities.ResourceManager;

namespace FoodSmart.BLL
{
    public class InvReportBLL
    {
        public static List<RecipeListEntity> GetRecipeList(ReportCriteria criteria)
        {
            return InvReportDAL.GetRecipeList(criteria);
        }
        public static List<RecipeDetailEntity> GetRecipeItem(ReportCriteria criteria)
        {
            return InvReportDAL.GetRecipeItem(criteria);
        }

        public static List<ReceiptRegEntity> GetInvReceiptRegister(ReportCriteria criteria)
        {
            return InvReportDAL.GetInvReceiptRegister(criteria);
        }

        public static List<RcptIssueEntity> GetAllInvItem(ReportCriteria criteria, string CalledFrom)
        {
            return InvReportDAL.GetAllInvItem(criteria, CalledFrom);
        }

        public DataSet GetVendors(int VendId = 0)
        { return new InvReportDAL().GetVendors(VendId); }

        public DataSet GetRMGroups(int GrpID = 0)
        {
            return new InvReportDAL().GetRMGroups(GrpID);
        }

        public DataSet GetRMItems(int ItemID = 0, int GroupID = 0)
        {
            return new InvReportDAL().GetRMItems(ItemID, GroupID);
        }

        public DataSet GetStores(int StoreID, string CalledFrom)
        {
            return new InvReportDAL().GetStores(StoreID, CalledFrom);
        }

        public static List<StockSummaryEntity> GetInvStockSummary(ReportCriteria criteria, string CalledFrom)
        {
            return InvReportDAL.GetInvStockSummary(criteria, CalledFrom);
        }

        public static List<StockSummaryEntity> GetInvStockRegister(ReportCriteria criteria)
        {
            return InvReportDAL.GetInvStockRegister(criteria);
        }

        public static List<StockTranItemEntity> GetAllStockTransaction(ReportCriteria criteria)
        {
            return InvReportDAL.GetAllStockTransaction(criteria);
        }

        public static List<StockSummaryEntity> GetInvStockValue(ReportCriteria criteria)
        {
            return InvReportDAL.GetInvStockValue(criteria);
        }

        public static List<StockSummaryEntity> GetInvStockAlert(ReportCriteria criteria, string AlertType)
        {
            return InvReportDAL.GetInvStockAlert(criteria, AlertType);
        }

        public static List<ItemwiseReceiptRegEntity> GetInvItemRcptIssue(ReportCriteria criteria, string CalledFrom)
        {
            return InvReportDAL.GetInvItemRcptIssue(criteria, CalledFrom);
        }

        public static List<IssueRegEntity> GetInvIssueRegister(ReportCriteria criteria)
        {
            return InvReportDAL.GetInvIssueRegister(criteria);
        }
    }
}
