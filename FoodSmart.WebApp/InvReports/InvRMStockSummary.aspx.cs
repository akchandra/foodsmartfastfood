﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using Microsoft.Reporting.WebForms;
using FoodSmart.BLL;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using FoodSmart.Utilities.ReportManager;
using FoodSmart.Utilities.ResourceManager;

namespace FoodSmart.WebApp.InvReports
{
    public partial class InvRMStockSummary : System.Web.UI.Page
    {
        #region Private Member Variables
        private IFormatProvider _culture = new CultureInfo(ConfigurationManager.AppSettings["Culture"].ToString());
        private int _userId = 0;
        private bool _canAdd = false;
        private bool _canEdit = false;
        private bool _canDelete = false;
        private bool _canView = false;
        private string _RoleStat;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                RetriveParameters();
                CheckUserAccess();
                SetAttributes();
                if (!IsPostBack)
                {
                    var vend = new InvReportBLL();
                    Filler.FillData(ddlRM, vend.GetRMItems(0).Tables[0], "RMName", "pk_RMID", "--All--", "0");
                    Filler.FillData(ddlRMGroup, vend.GetRMGroups().Tables[0], "RMGroupName", "pk_RMGroupID", "--All--", "0");
                    Filler.FillData(ddlLoc, vend.GetStores(0, "B").Tables[0], "StoreName", "pk_StoreID");
                    ddlLoc.SelectedValue = "1";
                }
            }
            catch (Exception ex)
            {
                CommonBLL.HandleException(ex, this.Server.MapPath(this.Request.ApplicationPath).Replace("/", "\\"));
                ToggleErrorPanel(true, ex.Message);
            }
        }
        protected void btnShow_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlType.SelectedValue == "S")
                    GenerateReport();
                else if (ddlType.SelectedValue == "D")
                    GenerateStockDetailReport();
                else
                    StockValuation();

            }
            catch (Exception ex)
            {
                CommonBLL.HandleException(ex, this.Server.MapPath(this.Request.ApplicationPath).Replace("/", "\\"));
                ToggleErrorPanel(true, ex.Message);
            }
        }

        #region Private Methods

        private void SetAttributes()
        {
            ToggleErrorPanel(false, string.Empty);
            if (!IsPostBack)
            {
                btnShow.ToolTip = ResourceManager.GetStringWithoutName("R00058");
                ceFromDt.Format = Convert.ToString(ConfigurationManager.AppSettings["DateFormat"]);
                ceToDt.Format = Convert.ToString(ConfigurationManager.AppSettings["DateFormat"]);
                rfvFromDt.ErrorMessage = ResourceManager.GetStringWithoutName("R00062");
                rfvToDt.ErrorMessage = ResourceManager.GetStringWithoutName("R00063");
            }
        }

        private void RetriveParameters()
        {
            DataSet ds = new DataSet();
            _userId = UserBLL.GetLoggedInUserId();
            ds = UserBLL.GetRoleWithUserID(_userId);
            _RoleStat = ds.Tables[0].Rows[0]["printOption"].ToString();
            UserBLL.GetUserPermission(out _canAdd, out _canEdit, out _canDelete, out _canView);
        }
        private void CheckUserAccess()
        {
            if (!_canView)
            {
                Response.Redirect("~/Unauthorized.aspx");
            }
        }

        private void ToggleErrorPanel(bool isVisible, string errorMessage)
        {
            if (isVisible)
            {
                dvSync.Style["display"] = "";
                dvErrMsg.InnerHtml = GeneralFunctions.FormatErrorMessage(errorMessage);
            }
            else
            {
                dvSync.Style["display"] = "none";
                dvErrMsg.InnerHtml = string.Empty;
            }
        }

        private void GenerateReport()
        {
            lblError.Text = "";
            //ReportBLL cls = new ReportBLL();
            LocalReportManager reportManager = new LocalReportManager(rptViewer, "InvRMStockSumm", ConfigurationManager.AppSettings["ReportNamespace"].ToString(), ConfigurationManager.AppSettings["ReportPath"].ToString());
            ReportCriteria criteria = new ReportCriteria();
            BuildCriteria(criteria);
            List<StockSummaryEntity> lstData = InvReportBLL.GetInvStockSummary(criteria, "S");

            DataSet _dsCompany = ReportBAL.Getcompany();
            if (_RoleStat == "P")
            {
                DisableUnwantedExportFormat(rptViewer, "Excel");
                DisableUnwantedExportFormat(rptViewer, "WORD");
            }

            reportManager.AddParameter("StartDate", txtFromDt.Text.Trim());
            reportManager.AddParameter("EndDate", txtToDt.Text.Trim());
            reportManager.AddParameter("Address", _dsCompany.Tables[0].Rows[0]["Address1"].ToString());
            reportManager.AddParameter("CompanyName", _dsCompany.Tables[0].Rows[0]["Company_Name"].ToString());
            reportManager.AddParameter("ItemGroup", ddlRMGroup.SelectedItem.ToString());
            //reportManager.AddParameter("Item", ddlRM.SelectedItem.ToString());

            reportManager.AddDataSource(new ReportDataSource("FoodSmartDataSet", lstData));
            reportManager.Show();
        }

        private void GenerateStockDetailReport()
        {
            lblError.Text = "";
            //ReportBLL cls = new ReportBLL();
            LocalReportManager reportManager = new LocalReportManager(rptViewer, "InvStockReg", ConfigurationManager.AppSettings["ReportNamespace"].ToString(), ConfigurationManager.AppSettings["ReportPath"].ToString());
            reportManager.HasSubReport = true;
            ReportCriteria criteria = new ReportCriteria();
            BuildCriteria(criteria);
            List<StockSummaryEntity> lstData = InvReportBLL.GetInvStockRegister(criteria);
            List<StockTranItemEntity> SubData = InvReportBLL.GetAllStockTransaction(criteria);

            DataSet _dsCompany = ReportBAL.Getcompany();
            if (_RoleStat == "P")
            {
                DisableUnwantedExportFormat(rptViewer, "Excel");
                DisableUnwantedExportFormat(rptViewer, "WORD");
            }

            reportManager.AddParameter("StartDate", txtFromDt.Text.Trim());
            reportManager.AddParameter("EndDate", txtToDt.Text.Trim());
            reportManager.AddParameter("Address", _dsCompany.Tables[0].Rows[0]["Address1"].ToString());
            reportManager.AddParameter("CompanyName", _dsCompany.Tables[0].Rows[0]["Company_Name"].ToString());
            //reportManager.AddParameter("ItemGroup", ddlRMGroup.SelectedItem.ToString());
            //reportManager.AddParameter("Item", ddlRM.SelectedItem.ToString());

            reportManager.AddDataSource(new ReportDataSource("FoodSmartDataSet", lstData));
            reportManager.AddSubReportDataSource(new ReportDataSource("dsRMItems", SubData));

            reportManager.Show();
        }

        private void StockValuation()
        {
            lblError.Text = "";
            //ReportBLL cls = new ReportBLL();
            LocalReportManager reportManager = new LocalReportManager(rptViewer, "InvRMStockVal", ConfigurationManager.AppSettings["ReportNamespace"].ToString(), ConfigurationManager.AppSettings["ReportPath"].ToString());
            ReportCriteria criteria = new ReportCriteria();
            BuildCriteria(criteria);
            List<StockSummaryEntity> lstData = InvReportBLL.GetInvStockValue(criteria);

            DataSet _dsCompany = ReportBAL.Getcompany();
            if (_RoleStat == "P")
            {
                DisableUnwantedExportFormat(rptViewer, "Excel");
                DisableUnwantedExportFormat(rptViewer, "WORD");
            }

            reportManager.AddParameter("EndDate", txtToDt.Text.Trim());
            reportManager.AddParameter("Address", _dsCompany.Tables[0].Rows[0]["Address1"].ToString());
            reportManager.AddParameter("CompanyName", _dsCompany.Tables[0].Rows[0]["Company_Name"].ToString());
            //reportManager.AddParameter("Item", ddlRM.SelectedItem.ToString());

            reportManager.AddDataSource(new ReportDataSource("FoodSmartDataSet", lstData));
            reportManager.Show();
        }

        //private void GenerateSummary()
        //{
        //    lblError.Text = "";
        //    ReportBLL cls = new ReportBLL();
        //    LocalReportManager reportManager = new LocalReportManager(rptViewer, "InvRMStockSummary", ConfigurationManager.AppSettings["ReportNamespace"].ToString(), ConfigurationManager.AppSettings["ReportPath"].ToString());
        //    ReportCriteria criteria = new ReportCriteria();
        //    BuildCriteria(criteria);
        //    List<InventoryReportEntity> lstData = ReportBLL.GetInvStockSummary(criteria, "S");

        //    DataSet _dsCompany = ReportBLL.Getcompany();
        //    if (_RoleStat == "P")
        //    {
        //        DisableUnwantedExportFormat(rptViewer, "Excel");
        //        DisableUnwantedExportFormat(rptViewer, "WORD");
        //    }

        //    reportManager.AddParameter("StartDate", txtFromDt.Text.Trim());
        //    reportManager.AddParameter("EndDate", txtToDt.Text.Trim());
        //    reportManager.AddParameter("Address", _dsCompany.Tables[0].Rows[0]["Address1"].ToString());
        //    reportManager.AddParameter("CompanyName", _dsCompany.Tables[0].Rows[0]["Company_Name"].ToString());
        //    reportManager.AddParameter("ItemGroup", ddlRMGroup.SelectedItem.ToString());
        //    //reportManager.AddParameter("Item", ddlRM.SelectedItem.ToString());

        //    reportManager.AddDataSource(new ReportDataSource("FoodSmartDataSet", lstData));
        //    reportManager.Show();
        //}

        private void BuildCriteria(ReportCriteria criteria)
        {
            if (txtFromDt.Text.Trim() != string.Empty) criteria.FromDate = Convert.ToDateTime(txtFromDt.Text, _culture);
            if (txtToDt.Text.Trim() != string.Empty) criteria.ToDate = Convert.ToDateTime(txtToDt.Text, _culture);
            criteria.RMID = ddlRM.SelectedValue.ToInt();
            criteria.RMGroupID = ddlRMGroup.SelectedValue.ToInt();
            criteria.restID = ddlLoc.SelectedValue.ToInt();
        }

        public void DisableUnwantedExportFormat(ReportViewer ReportViewerID, string strFormatName)
        {
            FieldInfo info;
            foreach (RenderingExtension extension in ReportViewerID.LocalReport.ListRenderingExtensions())
            {
                if (extension.Name == strFormatName)
                {
                    info = extension.GetType().GetField("m_isVisible", BindingFlags.Instance | BindingFlags.NonPublic);
                    info.SetValue(extension, false);
                }
            }
        }
        #endregion

        protected void ddlRMGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            var vend = new InvReportBLL();
            Filler.FillData(ddlRM, vend.GetRMItems(0, ddlRMGroup.SelectedValue.ToInt()).Tables[0], "RMName", "pk_RMID", "--All--", "0");
        }

    }
}