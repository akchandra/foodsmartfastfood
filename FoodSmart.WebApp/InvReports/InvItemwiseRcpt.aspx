﻿<%@ Page Title=":: INVENTORY :: Itemwise Receipt Register" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="InvItemwiseRcpt.aspx.cs" Inherits="FoodSmart.WebApp.InvReports.InvItemwiseRcpt" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../../Scripts/Common.js" type="text/javascript"></script>
    <script type="text/javascript">
        LoadScript();
    </script>
    <style type="text/css">
        .style1
        {
            width: 278px;
        }
        .style2
        {
            width: 461px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="container" runat="server">
    <div id="headercaption">Raw Material Receipt Register</div>
    <div id="dvSync" runat="server" style="padding: 5px 0px 5px 0px; display: none;">
        <table width="100%" class="synpanel">
            <tr>
                <td>
                    <div id="dvErrMsg" runat="server"></div>
                </td>
                <td style="text-align: right; width: 2%;">
                    <img alt="Click to close" src="../../Images/Close-Button.bmp" title="Click to close" onclick="closeErrorPanel()" />
                </td>
            </tr>
        </table>
    </div>
    <div style="width: 900px; margin: 0 auto;">
        <fieldset style="width: 878px;">
            <legend>Search Criteria</legend>
            <div style="padding:10px;">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="padding-right:10px;" class="style2">Start Date:<span class="errormessage">*</span></td>
                        <td style="padding-right:30px;">
                            <asp:TextBox ID="txtFromDt" runat="server" Width="150" MaxLength="10"></asp:TextBox>
                            <cc1:calendarextender ID="ceFromDt" runat="server" TargetControlID="txtFromDt"></cc1:calendarextender>                                            
                        </td>
                        <td style="padding-right:10px;" class="style1">End Date:<span class="errormessage">*</span></td>
                        <td style="padding-right:30px;" class="style2">
                            <asp:TextBox ID="txtToDt" runat="server" Width="150" MaxLength="10"></asp:TextBox>
                            <cc1:calendarextender ID="ceToDt" runat="server" TargetControlID="txtToDt"></cc1:calendarextender>                                            
                        </td>
                        <td style="padding-right:10px;" width="200px">Vendor:</td>
                            <td style="padding-right:30px;">
                            <asp:DropDownList ID="ddlVendor" runat="server"  Width="150">
                            </asp:DropDownList> 
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-right:10px;" class="style2">RM Group:</td>
                        <td style="padding-right:30px;">
                        <asp:DropDownList ID="ddlRMGroup" runat="server"  Width="150" 
                                onselectedindexchanged="ddlRMGroup_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList> 
                        </td>

                        <td style="padding-right:10px;" class="style1">RM Items:</td>
                            <td style="padding-right:30px;">
                            <asp:DropDownList ID="ddlRM" runat="server"  Width="150">
                            </asp:DropDownList> 
                        </td>
                        <td style="padding-right:10px; width="200px"">Criteria:</td>
                        <td style="padding-right:30px;" colspan="3">
                             <asp:RadioButtonList ID="rdoReportType" runat="server" RepeatDirection="Horizontal"  Width="150">
                             <asp:ListItem Text="Summary" Value="S" Selected="True"></asp:ListItem>
                             <asp:ListItem Text="Detail" Value="D"></asp:ListItem>
                             </asp:RadioButtonList>
                        </td>
                        <td>
                            <asp:Label ID="lblError" runat="server" CssClass="errormessage"></asp:Label>
                        </td>
                         
                        
                     </tr>
                    <tr>
                        <td><asp:Button ID="btnShow" runat="server" Text="Show" CssClass="button" ValidationGroup="Show" OnClick="btnShow_Click" /></td>
                        <td><asp:RequiredFieldValidator ID="rfvFromDt" runat="server" CssClass="errormessage" ControlToValidate="txtFromDt" Display="Dynamic" ForeColor="" SetFocusOnError="True" ValidationGroup="Show"></asp:RequiredFieldValidator></td>
                        <td class="style1">&nbsp;</td>
                        <td colspan="4"><asp:RequiredFieldValidator ID="rfvToDt" runat="server" CssClass="errormessage" ControlToValidate="txtToDt" Display="Dynamic" ForeColor="" SetFocusOnError="True" ValidationGroup="Show"></asp:RequiredFieldValidator></td>
                    </tr>
                </table>
            </div>
        </fieldset>
    </div>
    <div class="reportpanel" style="width:894px;margin:0 auto;">
        <rsweb:ReportViewer ID="rptViewer" runat="server" CssClass="rptviewer" Width="100%">
        </rsweb:ReportViewer>
    </div>
</asp:Content>

