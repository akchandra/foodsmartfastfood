﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using Microsoft.Reporting.WebForms;
using FoodSmart.BLL;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using FoodSmart.Utilities.ReportManager;
using FoodSmart.Utilities.ResourceManager;

namespace FoodSmart.WebApp.Reports
{
    public partial class GroupItemWiseSalesRegister : System.Web.UI.Page
    {
        #region Private Member Variables

        private IFormatProvider _culture = new CultureInfo(ConfigurationManager.AppSettings["Culture"].ToString());
        private int _userId = 0;
        private bool _canAdd = false;
        private bool _canEdit = false;
        private bool _canDelete = false;
        private bool _canView = false;
        private int _userLoc = 0;
        private string _RoleStat;

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            RetriveParameters();
            CheckUserAccess();
            SetAttributes();
            if (!IsPostBack)
            {
                var bal = new ReportBAL();
                Filler.FillData(ddlLocation, bal.GetLocation().Tables[0], "LocationName", "pk_LocID", "All","0"); 
            }
        }
        //private void SetData()
        //{
        //    StartDate = Convert.ToDateTime(txtStartDate.Text);
        //    EndDate = Convert.ToDateTime(txtEndDate.Text);
        //    LocationId = Convert.ToInt32(ddlLocation.SelectedValue);
        //    Location = ddlLocation.SelectedItem.Text;
        //    Restaurant = ddlRestaurant.SelectedValue;
        //}


        private void SetAttributes()
        {
            ToggleErrorPanel(false, string.Empty);

            if (!IsPostBack)
            {
                btnShow.ToolTip = ResourceManager.GetStringWithoutName("R00058");
                ceFromDt.Format = Convert.ToString(ConfigurationManager.AppSettings["DateFormat"]);
                ceToDt.Format = Convert.ToString(ConfigurationManager.AppSettings["DateFormat"]);
                rfvFromDt.ErrorMessage = ResourceManager.GetStringWithoutName("R00062");
                rfvToDt.ErrorMessage = ResourceManager.GetStringWithoutName("R00063");
            }
        }

        private void RetriveParameters()
        {
            DataSet ds = new DataSet();
            _userId = UserBLL.GetLoggedInUserId();
            ds = UserBLL.GetRoleWithUserID(_userId);
            _RoleStat = ds.Tables[0].Rows[0]["printOption"].ToString();
            _userLoc = ds.Tables[0].Rows[0]["fk_LocID"].ToInt();

            //Get user permission.
            UserBLL.GetUserPermission(out _canAdd, out _canEdit, out _canDelete, out _canView);
        }

        private void CheckUserAccess()
        {
            if (!_canView)
            {
                Response.Redirect("~/Unauthorized.aspx");
            }
        }

        private void ToggleErrorPanel(bool isVisible, string errorMessage)
        {
            if (isVisible)
            {
                dvSync.Style["display"] = "";
                dvErrMsg.InnerHtml = GeneralFunctions.FormatErrorMessage(errorMessage);
            }
            else
            {
                dvSync.Style["display"] = "none";
                dvErrMsg.InnerHtml = string.Empty;
            }
        }
        private void GenerateReport()
        {
            //if (ddlLocation.SelectedValue == "0")
            //{
            //    lblError.Text = "Location Mandatory";
            //    return;
            //}
            //else

            lblError.Text = "";
            ReportBAL cls = new ReportBAL();
            LocalReportManager reportManager = new LocalReportManager(rptViewer, "GroupItemWiseSalesRegister", ConfigurationManager.AppSettings["ReportNamespace"].ToString(), ConfigurationManager.AppSettings["ReportPath"].ToString());
            ReportCriteria criteria = new ReportCriteria();
            BuildCriteria(criteria);
            List<GroupWiseSaleEntity> lstData;
            DataSet _dsCompany = ReportBAL.Getcompany();

            lstData = ReportBAL.GetItemGroupSummaryGST(criteria);

            if (_RoleStat == "P")
            {
                DisableUnwantedExportFormat(rptViewer, "Excel");
                DisableUnwantedExportFormat(rptViewer, "WORD");
            }
            //string compname = "BURP!";
            ReportDataSource dsGeneral = new ReportDataSource("FoodSmartDataSet", lstData);
            reportManager.AddParameter("StartDate", txtStartDate.Text.Trim());
            reportManager.AddParameter("EndDate", txtEndDate.Text.Trim());
            reportManager.AddParameter("Address", _dsCompany.Tables[0].Rows[0]["Address1"].ToString());
            reportManager.AddParameter("CompanyName", _dsCompany.Tables[0].Rows[0]["Company_Name"].ToString());


            reportManager.AddDataSource(dsGeneral);
            reportManager.Show();




            //List<BillRefundRegEntity> lstData;
            //DataSet _dsCompany = ReportBAL.Getcompany();

            //lstData = ReportBAL.GetItemGroupSummaryGST(StartDate.Value, EndDate.Value, LocationId);

            ////SetData();
            ////var reportBAL = new ReportBAL();

            ////lstData = reportBAL.GetItemGroupSummaryGST(StartDate.Value, EndDate.Value, LocationId);
            //rptViewer.SizeToReportContent = true;
            //rptViewer.LocalReport.ReportPath = Server.MapPath("~/RDLC/GroupItemWiseSalesRegister.rdlc");
            //rptViewer.LocalReport.SetParameters(new ReportParameter("StartDate", StartDate.Value.ToString("dd-MM-yy")));
            //rptViewer.LocalReport.SetParameters(new ReportParameter("EndDate", EndDate.Value.ToString("dd-MM-yy")));
            //rptViewer.LocalReport.SetParameters(new ReportParameter("LocID", Location.ToUpper())); 
            //rptViewer.LocalReport.SetParameters(new ReportParameter("Restaurant", Restaurant));
            //rptViewer.LocalReport.DataSources.Clear();
            //ReportDataSource rsource = new ReportDataSource("GroupItemWiseSalesRegister", _dsReport.Tables[0]);
            //rptViewer.LocalReport.DataSources.Add(rsource);
            ////this.rptViewer.DataBind();
            //rptViewer.LocalReport.Refresh();
        }

        private void BuildCriteria(ReportCriteria criteria)
        {
            if (txtStartDate.Text.Trim() != string.Empty) criteria.FromDate = Convert.ToDateTime(txtStartDate.Text, _culture);
            if (txtEndDate.Text.Trim() != string.Empty) criteria.ToDate = Convert.ToDateTime(txtEndDate.Text, _culture);
            criteria.LocId = ddlLocation.SelectedValue.ToInt();
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            try
            {
                GenerateReport();
            }
            catch (Exception ex)
            {
                //ReportBAL.HandleException(ex, this.Server.MapPath(this.Request.ApplicationPath).Replace("/", "\\"));
                //ToggleErrorPanel(true, ex.Message);
            }
        }

        public void DisableUnwantedExportFormat(ReportViewer ReportViewerID, string strFormatName)
        {
            FieldInfo info;

            foreach (RenderingExtension extension in ReportViewerID.LocalReport.ListRenderingExtensions())
            {
                if (extension.Name == strFormatName)
                {
                    info = extension.GetType().GetField("m_isVisible", BindingFlags.Instance | BindingFlags.NonPublic);
                    info.SetValue(extension, false);
                }
            }
        }
    }
}