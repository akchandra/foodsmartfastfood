﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ItemList.aspx.cs" Inherits="FoodSmart.WebApp.Reports.ItemList" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../../Scripts/Common.js" type="text/javascript"></script>
    <script type="text/javascript">
        LoadScript();
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="container" runat="server">
    <div id="headercaption">ITEM MASTER LIST</div>
    <div id="dvSync" runat="server" style="padding: 5px 0px 5px 0px; display: none;">
        <table width="100%" class="synpanel">
            <tr>
                <td>
                    <div id="dvErrMsg" runat="server"></div>
                </td>
                <td style="text-align: right; width: 2%;">
                    <img alt="Click to close" src="../../Images/Close-Button.bmp" title="Click to close" onclick="closeErrorPanel()" />
                </td>
            </tr>
        </table>
    </div>
    <div style="width: 900px; margin: 0 auto; margin-top: 30px;">
        <fieldset style="width: 100%;">
            <legend>Item list</legend>
            <table border="0" cellpadding="5" cellspacing="5" width="100%">
                <tr>
                    <td>Item type:</td>
                    <td>
                        <asp:DropDownList ID="ddlItemType" runat="server" AutoPostBack="true" CssClass="chzn-select medium-select single" OnSelectedIndexChanged="ddlItemType_SelectedIndexChanged">
                            <asp:ListItem Text="Raw Material" Value="R" />
                            <asp:ListItem Text="Finished Goods" Value="F" />
                            <asp:ListItem Text="Semi finished Goods" Value="S" />
                        </asp:DropDownList>
                    </td>
                    <td>As on date:</td>
                    <td>
                        <asp:TextBox ID="txtAsOnDate" runat="server" CssClass="textboxuppercase" TabIndex="5" MaxLength="30" Width="102px" Height="20px" Style="text-align: right"></asp:TextBox><br />
                        <cc1:CalendarExtender ID="ceAsOnDate" TargetControlID="txtAsOnDate" runat="server" Format="dd/MM/yyyy" Enabled="True" />
                        <asp:RequiredFieldValidator ID="rfvAsOnDate" runat="server" ControlToValidate="txtAsOnDate" ErrorMessage="This field is required*" CssClass="errormessage" ValidationGroup="Save" Display="Dynamic"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>Item group:</td>
                    <td colspan="3">
                        <asp:DropDownList ID="ddlItemGroup" runat="server" AutoPostBack="true" CssClass="chzn-select medium-select single">
                            <asp:ListItem Text="All" Value="0" Selected="True" />
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:Button ID="btnShow" runat="server" Text="Show Report" OnClick="btnShow_Click" ValidationGroup="Save" />&nbsp;
                    <asp:Button ID="btnReset" runat="server" Text="Reset" OnClick="btnReset_Click" />
                    </td>
                </tr>
                <tr>
                        <td>
                            <asp:Label ID="lblError" runat="server" CssClass="errormessage"></asp:Label>
                        </td>
                    </tr>
            </table>
        </fieldset>
    </div>
    <div style="width: 894px; margin: 0 auto;">
        <rsweb:ReportViewer ID="rptViewer" runat="server" Width="100%">
        </rsweb:ReportViewer>
    </div>
</asp:Content>
