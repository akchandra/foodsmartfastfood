﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using FoodSmart.BLL;
using FoodSmart.Entity;
using System.Data;
using Microsoft.Reporting.WebForms;
using FoodSmart.Common;

namespace FoodSmart.WebApp.Reports
{
    public partial class TransactionRegister : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadLocation();
                LoadItemGroup("0");
                SetDefaultValue();
                LoadItem();
            }
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            GenerateReport();
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            SetDefaultValue();
            LoadItem();
        }

        protected void ddlItemGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadItem();
        }

        protected void ddlItemType_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadItemGroup(ddlItemType.SelectedValue);
            LoadItem();
        }

        private void GenerateReport()
        {
            DateTime fromDate = Convert.ToDateTime(txtFromDate.Text);
            DateTime toDate = Convert.ToDateTime(txtToDate.Text);
            List<Int32> groups = new List<int>();
            List<Int64> items = new List<Int64>();
            List<TransactionRegisterEntity> lstItems = ReportBAL.GetTransactionRegister(fromDate, toDate, ddlItemType.SelectedValue, ddlType.SelectedValue, Convert.ToInt32(ddlItemGroup.SelectedValue), 1, Convert.ToInt32(ddlFromLoc.SelectedValue), Convert.ToInt32(ddlItem.SelectedValue));

            rptViewer.SizeToReportContent = true;
            rptViewer.LocalReport.DataSources.Clear();
            rptViewer.LocalReport.ReportPath = Server.MapPath("~/RDLC/TransactionRegister.rdlc");
            rptViewer.LocalReport.SetParameters(new ReportParameter("StartDate", txtFromDate.Text));
            rptViewer.LocalReport.SetParameters(new ReportParameter("EndDate", txtToDate.Text));
            rptViewer.LocalReport.SetParameters(new ReportParameter("LocationName", ddlFromLoc.SelectedItem.Text));
            rptViewer.LocalReport.SetParameters(new ReportParameter("TransactionType", ddlType.SelectedItem.Text));

            ReportDataSource rsource = new ReportDataSource("FoodSmartDataSet", lstItems);
            rptViewer.LocalReport.DataSources.Add(rsource);

            //this.rptViewer.DataBind();
            rptViewer.LocalReport.Refresh();
        }

        private void LoadItemGroup(string type)
        {
            List<IItemGroup> groups = RecipeBLL.GetItemGroupByType(type);
            ddlItemGroup.DataValueField = "Id";
            ddlItemGroup.DataTextField = "Name";
            ddlItemGroup.DataSource = groups;
            ddlItemGroup.DataBind();
            ddlItemGroup.Items.Insert(0, new ListItem("All", "0"));
        }

        private void LoadItem()
        {
            List<IRecipe> items = RecipeBLL.GetItemsByGroup(ddlItemType.SelectedValue, Convert.ToInt32(ddlItemGroup.SelectedValue));
            ddlItem.DataValueField = "Id";
            ddlItem.DataTextField = "Name";
            ddlItem.DataSource = items;
            ddlItem.DataBind();
            ddlItem.Items.Insert(0, new ListItem("All", "0"));
        }

        private void LoadLocation()
        {
            List<ILocation> locations = TransactionBLL.GetLocations();
            ddlFromLoc.DataValueField = "LocID";
            ddlFromLoc.DataTextField = "LocName";
            ddlFromLoc.DataSource = locations;
            ddlFromLoc.DataBind();
            ddlFromLoc.Items.Insert(0, new ListItem("All", "0"));
        }

        private void SetDefaultValue()
        {
            ddlItemType.SelectedValue = "R";
            ddlType.SelectedValue = "0";
            ddlItemGroup.SelectedValue = "0";
            ddlFromLoc.SelectedValue = "0";
            txtFromDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
            txtToDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
        }
    }
}