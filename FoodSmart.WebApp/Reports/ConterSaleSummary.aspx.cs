﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Reflection;
using FoodSmart.BLL;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using FoodSmart.Utilities.ReportManager;
using FoodSmart.Utilities.ResourceManager;

namespace FoodSmart.WebApp.Reports
{
    public partial class ConterSaleSummary : System.Web.UI.Page
    {
        #region Private Member Variables

        private IFormatProvider _culture = new CultureInfo(ConfigurationManager.AppSettings["Culture"].ToString());
        private int _userId = 0;
        private bool _canAdd = false;
        private bool _canEdit = false;
        private bool _canDelete = false;
        private int _userLoc = 0;
        private bool _canView = false;
        private string _RoleStat;
        #endregion

        #region Protected Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                RetriveParameters();
                CheckUserAccess();
                SetAttributes();
                //PopulateTransactionType();
                if (!IsPostBack)
                {
                    var bal = new ReportBAL();
                    Filler.FillData(ddlLocation, bal.GetLocation().Tables[0], "LocationName", "pk_LocID", "All");
                    //ddlLocation.SelectedIndex = 0;
                    ddlLocation.SelectedValue = _userLoc.ToString();
                    if (_userLoc == 0)
                    {
                        ddlLocation.Enabled = true;
                    }
                    else
                    {
                        ddlLocation.Enabled = false;
                        //ddlLocation_SelectedIndexChanged(null, null);
                    }
                    
                    //ddlLocation_SelectedIndexChanged(null, null);
                }
                //txtFromDt.Text = DateTime.Now.ToShortDateString();
                //txtToDt.Text = DateTime.Now.ToShortDateString();
            }
            catch (Exception ex)
            {
                CommonBLL.HandleException(ex, this.Server.MapPath(this.Request.ApplicationPath).Replace("/", "\\"));
                ToggleErrorPanel(true, ex.Message);
            }
        }


        protected void btnShow_Click(object sender, EventArgs e)
        {
            try
            {
                GenerateReport();
            }
            catch (Exception ex)
            {
                CommonBLL.HandleException(ex, this.Server.MapPath(this.Request.ApplicationPath).Replace("/", "\\"));
                ToggleErrorPanel(true, ex.Message);
            }
        }

        #endregion

        #region Private Methods

        private void SetAttributes()
        {
            ToggleErrorPanel(false, string.Empty);

            if (!IsPostBack)
            {
                btnShow.ToolTip = ResourceManager.GetStringWithoutName("R00058");
                ceFromDt.Format = Convert.ToString(ConfigurationManager.AppSettings["DateFormat"]);
                ceToDt.Format = Convert.ToString(ConfigurationManager.AppSettings["DateFormat"]);
                rfvFromDt.ErrorMessage = ResourceManager.GetStringWithoutName("R00062");
                rfvToDt.ErrorMessage = ResourceManager.GetStringWithoutName("R00063");
            }
        }

        private void RetriveParameters()
        {
            DataSet ds = new DataSet();
            _userId = UserBLL.GetLoggedInUserId();
            ds = UserBLL.GetRoleWithUserID(_userId);
            _RoleStat = ds.Tables[0].Rows[0]["printOption"].ToString();
            _userLoc = ds.Tables[0].Rows[0]["fk_LocID"].ToInt();
            //Get user permission.
            UserBLL.GetUserPermission(out _canAdd, out _canEdit, out _canDelete, out _canView);
        }

        private void CheckUserAccess()
        {
            if (!_canView)
            {
                Response.Redirect("~/Unauthorized.aspx");
            }
        }

        //private void PopulateTransactionType()
        //{
        //    List<BaseEntity<string>> lstType = CommonBLL.GetTransactionType();
        //    //GeneralFunctions.PopulateDropDownList<BaseEntity<string>>(ddlTxnType, lstType, "Id", "Desc", false);
        //}

        private void GenerateReport()
        {
            //ReportBLL cls = new ReportBLL();
            //if (ddlLocation.SelectedIndex == 0 || ddlRestaurant.SelectedIndex == 0)
            //    return;
            if (_RoleStat == "P")
            {
                DisableUnwantedExportFormat(rptViewer, "Excel");
                DisableUnwantedExportFormat(rptViewer, "WORD");
            }
            LocalReportManager reportManager = new LocalReportManager(rptViewer, "CounterSummary", ConfigurationManager.AppSettings["ReportNamespace"].ToString(), ConfigurationManager.AppSettings["ReportPath"].ToString());
            ReportCriteria criteria = new ReportCriteria();
            BuildCriteria(criteria);
            List<CounterSummaryEntity> lstData;
            DataSet _dsCompany = ReportBAL.Getcompany();
            lstData = ReportBAL.GetDaySummaryGST(criteria);

            ReportDataSource dsGeneral = new ReportDataSource("DaySummaryDataSet", lstData);
            reportManager.AddParameter("StartDate", txtFromDt.Text.Trim());
            reportManager.AddParameter("EndDate", txtToDt.Text.Trim());
            //reportManager.AddParameter("BillType", rdoBillType.SelectedValue);
            reportManager.AddParameter("Address", _dsCompany.Tables[0].Rows[0]["Address1"].ToString());
            reportManager.AddParameter("CompanyName", _dsCompany.Tables[0].Rows[0]["Company_Name"].ToString());
            //reportManager.AddParameter("Counters", ddlRestaurant.SelectedItem.ToString());
            //reportManager.AddParameter("Location", ddlLocation.SelectedItem.ToString());

            reportManager.AddDataSource(dsGeneral);
            reportManager.Show();
        }

        private void BuildCriteria(ReportCriteria criteria)
        {
            if (txtFromDt.Text.Trim() != string.Empty) criteria.FromDate = Convert.ToDateTime(txtFromDt.Text, _culture);
            if (txtToDt.Text.Trim() != string.Empty) criteria.ToDate = Convert.ToDateTime(txtToDt.Text, _culture);
            criteria.LocId = ddlLocation.SelectedValue.ToInt();
            //criteria.CounterId = ddlRestaurant.SelectedValue.ToInt();
            //criteria.TransactionType = rdoBillType.SelectedValue;

        }

        private void ToggleErrorPanel(bool isVisible, string errorMessage)
        {
            if (isVisible)
            {
                dvSync.Style["display"] = "";
                dvErrMsg.InnerHtml = GeneralFunctions.FormatErrorMessage(errorMessage);
            }
            else
            {
                dvSync.Style["display"] = "none";
                dvErrMsg.InnerHtml = string.Empty;
            }
        }


        //protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    var bal = new ReportBAL();
        //    Filler.FillData(ddlRestaurant, bal.GetRestaurant(LocId: Convert.ToInt32(ddlLocation.SelectedValue)).Tables[0], "RestName", "pk_RestID");
        //    ddlRestaurant.SelectedIndex = 0;

        //}

        public void DisableUnwantedExportFormat(ReportViewer ReportViewerID, string strFormatName)
        {
            FieldInfo info;

            foreach (RenderingExtension extension in ReportViewerID.LocalReport.ListRenderingExtensions())
            {
                if (extension.Name == strFormatName)
                {
                    info = extension.GetType().GetField("m_isVisible", BindingFlags.Instance | BindingFlags.NonPublic);
                    info.SetValue(extension, false);
                }
            }
        }
        #endregion
    }
}