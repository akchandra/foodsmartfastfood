﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using Microsoft.Reporting.WebForms;
using FoodSmart.BLL;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using FoodSmart.Common;
using FoodSmart.Utilities.ReportManager;
using FoodSmart.Utilities.ResourceManager;

namespace FoodSmart.WebApp.Reports
{
    public partial class ItemList : System.Web.UI.Page
    {
        #region Private Member Variables

        private IFormatProvider _culture = new CultureInfo(ConfigurationManager.AppSettings["Culture"].ToString());
        private int _userId = 0;
        private bool _canAdd = false;
        private bool _canEdit = false;
        private bool _canDelete = false;
        private bool _canView = false;
        private int _userLoc = 0;
        private string _RoleStat;

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            RetriveParameters();
            CheckUserAccess();
            //SetAttributes();

            if (!IsPostBack)
            {
                LoadItemGroup("0");
                SetDefaultValue();
            }
        }

        
        private void RetriveParameters()
        {
            DataSet ds = new DataSet();
            _userId = UserBLL.GetLoggedInUserId();
            ds = UserBLL.GetRoleWithUserID(_userId);
            _RoleStat = ds.Tables[0].Rows[0]["printOption"].ToString();
            _userLoc = ds.Tables[0].Rows[0]["fk_LocID"].ToInt();

            //Get user permission.
            UserBLL.GetUserPermission(out _canAdd, out _canEdit, out _canDelete, out _canView);
        }

        private void CheckUserAccess()
        {
            if (!_canView)
            {
                Response.Redirect("~/Unauthorized.aspx");
            }
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            GenerateReport();
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            SetDefaultValue();
        }

        protected void ddlItemType_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadItemGroup(ddlItemType.SelectedValue);
        }

        private void GenerateReport()
        {
            DateTime asOnDate = Convert.ToDateTime(txtAsOnDate.Text);

            lblError.Text = "";
            ReportBAL cls = new ReportBAL();
            LocalReportManager reportManager = new LocalReportManager(rptViewer, "ItemList", ConfigurationManager.AppSettings["ReportNamespace"].ToString(), ConfigurationManager.AppSettings["ReportPath"].ToString());
            ReportCriteria criteria = new ReportCriteria();
            BuildCriteria(criteria);
            List<ItemListEntity> lstData;
            DataSet _dsCompany = ReportBAL.Getcompany();

            lstData = ReportBAL.GetItemList(criteria);

            if (_RoleStat == "P")
            {
                DisableUnwantedExportFormat(rptViewer, "Excel");
                DisableUnwantedExportFormat(rptViewer, "WORD");
            }
            //string compname = "BURP!";
            ReportDataSource dsGeneral = new ReportDataSource("FoodSmartDataSet", lstData);
            reportManager.AddParameter("EndDate", txtAsOnDate.Text.Trim());
            reportManager.AddParameter("TransactionType", ddlItemType.SelectedValue);
            reportManager.AddParameter("Address", _dsCompany.Tables[0].Rows[0]["Address1"].ToString());
            reportManager.AddParameter("CompanyName", _dsCompany.Tables[0].Rows[0]["Company_Name"].ToString());


            reportManager.AddDataSource(dsGeneral);
            reportManager.Show();


            //rptViewer.SizeToReportContent = true;
            //rptViewer.LocalReport.DataSources.Clear();
            //rptViewer.LocalReport.ReportPath = Server.MapPath("~/RDLC/ItemList.rdlc");
            //rptViewer.LocalReport.SetParameters(new ReportParameter("EndDate", txtAsOnDate.Text));
            //rptViewer.LocalReport.SetParameters(new ReportParameter("TransactionType", ddlItemType.SelectedItem.Text));

            //ReportDataSource rsource = new ReportDataSource("FoodSmartDataSet", lstItems);
            //rptViewer.LocalReport.DataSources.Add(rsource);
            //rptViewer.LocalReport.Refresh();
        }

        private void BuildCriteria(ReportCriteria criteria)
        {
            if (txtAsOnDate.Text.Trim() != string.Empty) criteria.ToDate = Convert.ToDateTime(txtAsOnDate.Text, _culture);
            criteria.TransactionType = ddlItemType.SelectedValue;
            criteria.ItemGroupID = ddlItemGroup.SelectedValue.ToInt();
        }

        private void LoadItemGroup(string type)
        {
            List<IItemGroup> groups = RecipeBLL.GetItemGroupByType(type);
            ddlItemGroup.DataValueField = "Id";
            ddlItemGroup.DataTextField = "Name";
            ddlItemGroup.DataSource = groups;
            ddlItemGroup.DataBind();
            ddlItemGroup.Items.Insert(0, new ListItem("All", "0"));
        }

        private void SetDefaultValue()
        {
            ddlItemType.SelectedValue = "0";
            ddlItemGroup.SelectedValue = "0";
            txtAsOnDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
        }

        public void DisableUnwantedExportFormat(ReportViewer ReportViewerID, string strFormatName)
        {
            FieldInfo info;

            foreach (RenderingExtension extension in ReportViewerID.LocalReport.ListRenderingExtensions())
            {
                if (extension.Name == strFormatName)
                {
                    info = extension.GetType().GetField("m_isVisible", BindingFlags.Instance | BindingFlags.NonPublic);
                    info.SetValue(extension, false);
                }
            }
        }
    }
}