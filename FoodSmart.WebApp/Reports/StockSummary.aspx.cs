﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FoodSmart.Utilities.ReportManager;
using System.Configuration;
using FoodSmart.BLL;
using FoodSmart.Entity;
using System.Data;
using Microsoft.Reporting.WebForms;
using FoodSmart.Utilities;

namespace FoodSmart.WebApp.Reports
{
    public partial class StockSummary : System.Web.UI.Page
    {
        DataSet _dsReport = null;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            GenerateReport();
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {

        }

        private void GenerateReport()
        {
            DateTime asOnDate = Convert.ToDateTime(txtAsOnDate.Text);
            List<Int32> groups = new List<int>();
            List<Int64> items = new List<Int64>();
            List<StockSummaryEntity> lstItems = ReportBAL.GetStockSummary(asOnDate, groups, items);
            rptViewer.SizeToReportContent = true;
            rptViewer.LocalReport.ReportPath = Server.MapPath("~/RDLC/StockSummary.rdlc");
            rptViewer.LocalReport.DataSources.Clear();
            ReportDataSource rsource = new ReportDataSource("FoodSmartDataSet", lstItems);
            rptViewer.LocalReport.DataSources.Add(rsource);
            //this.rptViewer.DataBind();
            rptViewer.LocalReport.Refresh();
        }
    }
}