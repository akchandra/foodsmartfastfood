﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="StockDetail.aspx.cs" Inherits="FoodSmart.WebApp.Reports.StockDetail" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="container" runat="server">
    <div style="width: 894px; margin: 0 auto;">
         <table border="0" cellpadding="3" cellspacing="3" width="100%">
            <tr>
                <td>From date:</td>
                <td>
                    <asp:TextBox ID="txtFromDate" runat="server" CssClass="textboxuppercase" TabIndex="5" MaxLength="30" Width="102px" Height="20px" Style="text-align: right"></asp:TextBox><br />
                    <cc1:CalendarExtender ID="ceFromDate" TargetControlID="txtFromDate" runat="server" Format="dd/MM/yyyy" Enabled="True" />
                    <asp:RequiredFieldValidator ID="rfvFromDate" runat="server" ControlToValidate="txtFromDate" ErrorMessage="This field is required*" CssClass="errormessage" ValidationGroup="Save" Display="Dynamic"></asp:RequiredFieldValidator>
                </td>
                <td>To date:</td>
                <td>
                    <asp:TextBox ID="txtToDate" runat="server" CssClass="textboxuppercase" TabIndex="5" MaxLength="30" Width="102px" Height="20px" Style="text-align: right"></asp:TextBox><br />
                    <cc1:CalendarExtender ID="ceToDate" TargetControlID="txtToDate" runat="server" Format="dd/MM/yyyy" Enabled="True" />
                    <asp:RequiredFieldValidator ID="rfvToDate" runat="server" ControlToValidate="txtToDate" ErrorMessage="This field is required*" CssClass="errormessage" ValidationGroup="Save" Display="Dynamic"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>Item group:</td>
                <td>
                    <asp:CheckBoxList ID="chkGroup" runat="server" RepeatDirection="Vertical" RepeatColumns="3"></asp:CheckBoxList>
                </td>
                <td>Item:</td>
                <td><asp:CheckBoxList ID="chkItem" runat="server" RepeatDirection="Vertical" RepeatColumns="3"></asp:CheckBoxList></td>
            </tr>
            <tr>
                <td colspan="4">
                    <asp:Button ID="btnShow" runat="server" Text="Show Report" OnClick="btnShow_Click" ValidationGroup="Save" />&nbsp;
                    <asp:Button ID="btnReset" runat="server" Text="Show Report" OnClick="btnReset_Click" />
                </td>
            </tr>
        </table>
    </div>
    <div style="width: 894px; margin: 0 auto;">
        <rsweb:ReportViewer ID="rptViewer" runat="server" Width="100%">
        </rsweb:ReportViewer>
    </div>
</asp:Content>
