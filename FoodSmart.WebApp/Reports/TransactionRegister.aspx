﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TransactionRegister.aspx.cs" Inherits="FoodSmart.WebApp.Reports.TransactionRegister" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="container" runat="server">

    <div id="headercaption">TRANSACTION REGISTER</div>
    <div style="width: 900px; margin: 0 auto; margin-top: 30px;">
        <fieldset style="width: 100%;">
            <legend>Transaction register</legend>
            <table border="0" cellpadding="5" cellspacing="5" width="100%">
                <tr>
                    <td>From date:</td>
                    <td>
                        <asp:TextBox ID="txtFromDate" runat="server" CssClass="textboxuppercase" TabIndex="5" MaxLength="30" Width="102px" Height="20px" Style="text-align: right"></asp:TextBox><br />
                        <cc1:CalendarExtender ID="ceFromDate" TargetControlID="txtFromDate" runat="server" Format="dd/MM/yyyy" Enabled="True" />
                        <asp:RequiredFieldValidator ID="rfvFromDate" runat="server" ControlToValidate="txtFromDate" ErrorMessage="This field is required*" CssClass="errormessage" ValidationGroup="Save" Display="Dynamic"></asp:RequiredFieldValidator>
                    </td>
                    <td>To date:</td>
                    <td>
                        <asp:TextBox ID="txtToDate" runat="server" CssClass="textboxuppercase" TabIndex="5" MaxLength="30" Width="102px" Height="20px" Style="text-align: right"></asp:TextBox><br />
                        <cc1:CalendarExtender ID="ceToDate" TargetControlID="txtToDate" runat="server" Format="dd/MM/yyyy" Enabled="True" />
                        <asp:RequiredFieldValidator ID="rfvToDate" runat="server" ControlToValidate="txtToDate" ErrorMessage="This field is required*" CssClass="errormessage" ValidationGroup="Save" Display="Dynamic"></asp:RequiredFieldValidator>

                    </td>
                </tr>
                <tr>
                    <td>Transaction type:</td>
                    <td>
                        <asp:DropDownList ID="ddlType" runat="server" AutoPostBack="false" CssClass="chzn-select medium-select single">
                            <asp:ListItem Text="All" Value="0" Selected="True" />
                            <asp:ListItem Text="Receipt" Value="R" />
                            <asp:ListItem Text="Issue" Value="I" />
                            <asp:ListItem Text="Wastage" Value="W" />
                        </asp:DropDownList>
                    </td>
                    <td>Item type:</td>
                    <td>
                        <asp:DropDownList ID="ddlItemType" runat="server" AutoPostBack="true" CssClass="chzn-select medium-select single" OnSelectedIndexChanged="ddlItemType_SelectedIndexChanged">
                            <asp:ListItem Text="Raw Material" Value="R" />
                            <asp:ListItem Text="Finished Goods" Value="F" />
                            <asp:ListItem Text="Semi finished Goods" Value="S" />
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>Item group:</td>
                    <td>
                        <asp:DropDownList ID="ddlItemGroup" runat="server" AutoPostBack="true" CssClass="chzn-select medium-select single" OnSelectedIndexChanged="ddlItemGroup_SelectedIndexChanged">
                            <asp:ListItem Text="All" Value="0" Selected="True" />
                        </asp:DropDownList>
                    </td>
                    <td>Item:</td>
                    <td>
                        <asp:DropDownList ID="ddlItem" runat="server" AutoPostBack="false" CssClass="chzn-select medium-select single">
                            <asp:ListItem Text="All" Value="0" Selected="True" />
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>From location:</td>
                    <td colspan="3">
                        <asp:DropDownList ID="ddlFromLoc" runat="server" AutoPostBack="false" CssClass="chzn-select medium-select single">
                            <asp:ListItem Text="All" Value="0" Selected="True" />
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:Button ID="btnShow" runat="server" Text="Show Report" OnClick="btnShow_Click" ValidationGroup="Save" />&nbsp;
                    <asp:Button ID="btnReset" runat="server" Text="Reset" OnClick="btnReset_Click" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </div>
    <div style="width: 894px; margin: 0 auto;">
        <rsweb:ReportViewer ID="rptViewer" runat="server" Width="100%">
        </rsweb:ReportViewer>
    </div>
</asp:Content>
