﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using Microsoft.Reporting.WebForms;
using FoodSmart.BLL;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using FoodSmart.Utilities.ReportManager;
using FoodSmart.Utilities.ResourceManager;

namespace FoodSmart.WebApp.Reports
{
    public partial class InvRecipeListPrint : System.Web.UI.Page
    {
        #region Private Member Variables

        private IFormatProvider _culture = new CultureInfo(ConfigurationManager.AppSettings["Culture"].ToString());
        private int _userId = 0;
        private bool _canAdd = false;
        private bool _canEdit = false;
        private bool _canDelete = false;
        private bool _canView = false;
        private string _RoleStat;
        private int _Roleid = 0;
        private int _userLoc = 0;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                RetriveParameters();
                CheckUserAccess();
                SetAttributes();
                if (!IsPostBack)
                {
                    var bal = new InvReportBLL();
                    
                }
                //txtFromDt.Text = DateTime.Now.ToShortDateString();
                //txtToDt.Text = DateTime.Now.ToShortDateString();
            }
            catch (Exception ex)
            {
                CommonBLL.HandleException(ex, this.Server.MapPath(this.Request.ApplicationPath).Replace("/", "\\"));
                ToggleErrorPanel(true, ex.Message);
            }
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {
            try
            {
                GenerateReport();
            }
            catch (Exception ex)
            {
                CommonBLL.HandleException(ex, this.Server.MapPath(this.Request.ApplicationPath).Replace("/", "\\"));
                ToggleErrorPanel(true, ex.Message);
            }
        }

        #region Private Methods

        private void SetAttributes()
        {
            ToggleErrorPanel(false, string.Empty);

            if (!IsPostBack)
            {
                btnShow.ToolTip = ResourceManager.GetStringWithoutName("R00058");
                
            }
        }

        private void RetriveParameters()
        {

            DataSet ds = new DataSet();
            _userId = UserBLL.GetLoggedInUserId();
            ds = UserBLL.GetRoleWithUserID(_userId);
            _RoleStat = ds.Tables[0].Rows[0]["printOption"].ToString();
            _Roleid = ds.Tables[0].Rows[0]["pk_RoleID"].ToInt();
            _userLoc = ds.Tables[0].Rows[0]["fk_LocID"].ToInt();
            //Get user permission.
            UserBLL.GetUserPermission(out _canAdd, out _canEdit, out _canDelete, out _canView);
        }

        private void CheckUserAccess()
        {
            if (!_canView)
            {
                Response.Redirect("~/Unauthorized.aspx");
            }
        }

        
        private void GenerateReport()
        {
            //ReportBLL cls = new ReportBLL();
            LocalReportManager reportManager = new LocalReportManager(rptViewer, "InvRecipeList", ConfigurationManager.AppSettings["ReportNamespace"].ToString(), ConfigurationManager.AppSettings["ReportPath"].ToString());
            ReportCriteria criteria = new ReportCriteria();
            BuildCriteria(criteria);
            //List<ReportEntity> lstData = ReportBLL.ItemWiseSalesRegister(criteria);
            //DataSet _dsCompany = ReportBLL.Getcompany();
            reportManager.HasSubReport = true;

            List<RecipeListEntity> lstData;
            List<RecipeDetailEntity> lstItem;
            DataSet _dsCompany = ReportBAL.Getcompany();
            lstData = InvReportBLL.GetRecipeList(criteria);
            lstItem = InvReportBLL.GetRecipeItem(criteria);

            if (_RoleStat == "P")
            {
                DisableUnwantedExportFormat(rptViewer, "Excel");
                DisableUnwantedExportFormat(rptViewer, "WORD");
            }
            //string compname = "BURP!";
            //ReportDataSource dsGeneral = new ReportDataSource("InvFoodSmartDataSet", lstData);

            reportManager.AddDataSource(new ReportDataSource("FoodSmartDataSet", lstData));
            reportManager.AddSubReportDataSource(new ReportDataSource("dsRMItems", lstItem));
            //reportManager.AddParameter("BillType", rdoBillType.SelectedValue);
            reportManager.AddParameter("Address", _dsCompany.Tables[0].Rows[0]["Address1"].ToString());
            reportManager.AddParameter("CompanyName", _dsCompany.Tables[0].Rows[0]["Company_Name"].ToString());

            //reportManager.AddDataSource(dsGeneral);
            reportManager.Show();
        }

        private void BuildCriteria(ReportCriteria criteria)
        {
            criteria.TransactionType = ddlType.SelectedValue;
            criteria.RecipeName = txtRecipeName.Text;
        }

        private void ToggleErrorPanel(bool isVisible, string errorMessage)
        {
            if (isVisible)
            {
                dvSync.Style["display"] = "";
                dvErrMsg.InnerHtml = GeneralFunctions.FormatErrorMessage(errorMessage);
            }
            else
            {
                dvSync.Style["display"] = "none";
                dvErrMsg.InnerHtml = string.Empty;
            }
        }

        public void DisableUnwantedExportFormat(ReportViewer ReportViewerID, string strFormatName)
        {
            FieldInfo info;

            foreach (RenderingExtension extension in ReportViewerID.LocalReport.ListRenderingExtensions())
            {
                if (extension.Name == strFormatName)
                {
                    info = extension.GetType().GetField("m_isVisible", BindingFlags.Instance | BindingFlags.NonPublic);
                    info.SetValue(extension, false);
                }
            }
        }

        #endregion
    }
}