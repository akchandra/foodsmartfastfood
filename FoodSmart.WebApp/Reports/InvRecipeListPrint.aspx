﻿<%@ Page Title=":: POS :: Recipe List" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="InvRecipeListPrint.aspx.cs" Inherits="FoodSmart.WebApp.Reports.InvRecipeListPrint" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../../Scripts/Common.js" type="text/javascript"></script>
    <script type="text/javascript">
        LoadScript();
    </script>
    <style type="text/css">
        .style1
        {
            width: 22px;
        }
        .style2
        {
            width: 70px;
        }
        .button
        {
            margin-left: 0px;
        }
        .style4
        {
            width: 68px;
        }
        .style5
        {
            width: 164px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="container" runat="server">
    <div id="headercaption">ITEM WISE SALE</div>
    <div id="dvSync" runat="server" style="padding: 5px 0px 5px 0px; display: none;">
        <table width="100%" class="synpanel">
            <tr>
                <td>
                    <div id="dvErrMsg" runat="server"></div>
                </td>
                <td style="text-align: right; width: 2%;">
                    <img alt="Click to close" src="../../Images/Close-Button.bmp" title="Click to close" onclick="closeErrorPanel()" />
                </td>
            </tr>
        </table>
    </div>
    <div style="width: 900px; margin: 0 auto;">
        <fieldset style="width: 878px;">
            <legend>Search Criteria</legend>
            <div style="padding:10px;">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="padding-right:10px;" class="style5">Recipe Type:</td>
                            <td style="padding-right:10px; " class="style1">
                            <asp:DropDownList ID="ddlType" runat="server" Height="26px" 
                                    Width="122px" style="margin-left: 0px">
                                <asp:ListItem Text="Finished Item" Value="F" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Semi-Finished" Value="S"></asp:ListItem>
                            </asp:DropDownList> 
                        </td>
                        <td style="padding-right:10px;">Recipe:</td>
                        <td style="padding-right:30px;">
                            <asp:TextBox ID="txtRecipeName" runat="server" Width="285px" MaxLength="10"></asp:TextBox>
                        </td>
                         <td><asp:Button ID="btnShow" runat="server" Text="Show" CssClass="button" ValidationGroup="Show" OnClick="btnShow_Click" /></td>
                    </tr>

                    <tr>
                        <td class="style5"> <span class="errormessage">*</span> Mandatory</td>
                    </tr>
                </table>
            </div>
        </fieldset>
    </div>
    <div class="reportpanel" style="width:894px;margin:0 auto;">
        <rsweb:ReportViewer ID="rptViewer" runat="server" CssClass="rptviewer" Width="100%">
        </rsweb:ReportViewer>
    </div>
</asp:Content>
