﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FoodSmart.Utilities.ReportManager;
using System.Configuration;
using FoodSmart.BLL;
using FoodSmart.Entity;
using System.Data;
using Microsoft.Reporting.WebForms;

namespace FoodSmart.WebApp.Reports
{
    public partial class Test : System.Web.UI.Page
    {
        DataSet _dsReport = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) {
                btnShow_Click(null, null);
            }
        }
        private void GenerateReport()
        {
            var reportBAL = new ReportBAL(); DateTime StartDate=Convert.ToDateTime("2015-05-01");
            DateTime EndDate=Convert.ToDateTime("2015-05-01");
            int LocationId = 0;

            string Location = "ELGIN ROAD"; 
            string BillType = "";
            _dsReport = reportBAL.GetBillRefundRegister(StartDate,EndDate ,LocationId, BillType,""); 
            rptViewer.SizeToReportContent = true;
            rptViewer.LocalReport.ReportPath = Server.MapPath("~/RDLC/BillRefundRegister.rdlc"); 
            rptViewer.LocalReport.SetParameters(new ReportParameter("StartDate",  StartDate.ToString("dd-MM-yy")  ));
            rptViewer.LocalReport.SetParameters(new ReportParameter("EndDate", EndDate.ToString("dd-MM-yy")));
            rptViewer.LocalReport.SetParameters(new ReportParameter("LocID", Location.ToUpper() ));
            rptViewer.LocalReport.SetParameters(new ReportParameter("BillType", BillType));
            rptViewer.LocalReport.DataSources.Clear();
            ReportDataSource rsource = new ReportDataSource("FoodSmartDataSet", _dsReport.Tables[0]);
            rptViewer.LocalReport.DataSources.Add(rsource);
            //this.rptViewer.DataBind();
            rptViewer.LocalReport.Refresh();
        }
        protected void btnShow_Click(object sender, EventArgs e)
        {
            try
            {
                GenerateReport();
            }
            catch (Exception ex)
            {
                //ReportBAL.HandleException(ex, this.Server.MapPath(this.Request.ApplicationPath).Replace("/", "\\"));
                //ToggleErrorPanel(true, ex.Message);
            }
        }
    }
}