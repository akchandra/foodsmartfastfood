﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RMEdit.aspx.cs" Inherits="FoodSmart.WebApp.MasterModule.RMEdit" MasterPageFile="~/Site.Master" Title=":: FoodSmart :: Add / Edit Item" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FoodSmart.WebApp" Namespace="FoodSmart.WebApp.CustomControls" Tagprefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">    
    <script type="text/javascript">
        LoadScript();
    </script>
    <style type="text/css">
        .style7
        {
            height: 52px;
            width: 50%;
        }
        .style10
        {
            width: 47px;
        }
        .style11
        {
            width: 35%;
        }
        .style13
        {
            height: 52px;
            width: 71%;
        }
        .style14
        {
            width: 71%;
        }
        .style15
        {
            width: 50%;
        }
        .custtable
        {
            width: 107%;
        }
        .style16
        {
            width: 40%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="container" runat="Server">
    <div id="headercaption">ADD / EDIT RM ITEM</div>
    <div style="width:886px; padding-left:300px;">
        <fieldset style="width:100%;">
            <legend>Add / Edit Item</legend>
            <table style="height: 300px;vertical-align:top">
                <tr>
                    <td>
                        <table border="0" cellpadding="1" cellspacing="0" class="custtable">
                        <tr>
                            <td style="padding-bottom:5px;" class="style16">Item Type<span class="errormessage">*</span></td>
                            <td style="padding-bottom:5px;" class="style11">
                                <asp:DropDownList ID="ddlRorF" runat="server"  Width="200px" Height="20px" 
                                    TabIndex="1" onselectedindexchanged="ddlRorF_SelectedIndexChanged" AutoPostBack="true">
                                    <asp:ListItem Value="R">Raw Material</asp:ListItem>
                                    <asp:ListItem Value="C">Consumables</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td></td>
                            <td></td>
                            <td class="style10"></td>
                            <td style="padding-bottom:5px;" class="style14">Group<span class="errormessage">*</span></td>
                            <td style="padding-bottom:5px;" class="style15">
                                <asp:DropDownList ID="ddlGroup" runat="server" Width="250px" 
                                    CssClass="dropdownlist" TabIndex="2"><asp:ListItem Value="0" Text="--Select--"></asp:ListItem></asp:DropDownList>
                                <br /><asp:RequiredFieldValidator ID="rfvGroup" runat="server" ControlToValidate="ddlGroup" InitialValue="0" ErrorMessage="This field is required*" CssClass="errormessage" ValidationGroup="Save" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-bottom:5px;" class="style16">Item Name:<span class="errormessage">*</span></td>
                            <td style="padding-bottom:5px;" class="style11">
                                <br />
                                <asp:TextBox ID="txtName" runat="server" CssClass="textboxuppercase" 
                                    MaxLength="30" Width="250px" height="20px" TabIndex="3"></asp:TextBox>
                                <br />
                                <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName" InitialValue="" ErrorMessage="This field is required*" CssClass="errormessage" ValidationGroup="Save" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                            <td></td>
                            <td></td>
                            <td class="style10"></td>
                            <td style="padding-bottom:5px;" class="style14">UOM<span class="errormessage">*</span></td>
                            <td style="padding-bottom:5px;" class="style15">
                                <br />
                                <asp:TextBox ID="txtUOM" runat="server" CssClass="textboxuppercase" 
                                    MaxLength="30" Width="100px" height="20px" TabIndex="4"></asp:TextBox>
                                <br />
                                <asp:RequiredFieldValidator ID="rfvUom" runat="server" ControlToValidate="txtUOM" InitialValue="" ErrorMessage="This field is required*" CssClass="errormessage" ValidationGroup="Save" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        
                        
                        <tr>
                            <td style="padding-bottom:5px;" class="style16">Max Stock Level<span class="errormessage">*</span></td>
                            <td style="padding-bottom:5px;" class="style11">
                                <cc2:CustomTextBox ID="txtMaxStock" runat="server" CssClass="numerictextbox" TabIndex="3" Width="100px" Height="20px"
                                    Type="Decimal" MaxLength="10" Precision="10" Scale="3">
                                </cc2:CustomTextBox>
                            </td>
                            <td></td>
                            <td></td>
                            <td class="style10"></td>
                            <td class="style13">Min Stock Level<span class="errormessage">*</span></td>
                            <td class="style7">
                                <cc2:CustomTextBox ID="txtMinStock" runat="server" CssClass="numerictextbox" TabIndex="3" Width="100px" Height="20px"
                                    Type="Decimal" MaxLength="10" Precision="10" Scale="3">
                                </cc2:CustomTextBox>
                            </td>
                        </tr>

                        <tr>
                            <td style="padding-bottom:5px;" class="style16">Re-Order Level<span class="errormessage">*</span></td>
                            <td style="padding-bottom:5px;" class="style11">
                                <cc2:CustomTextBox ID="txtReorder" runat="server" CssClass="numerictextbox" TabIndex="3" Width="100px" Height="20px"
                                    Type="Decimal" MaxLength="10" Precision="10" Scale="3">
                                </cc2:CustomTextBox>
                            </td>
                            <td></td>
                            
                        </tr>
                        
                    </table>

                    </td>
                </tr>
                   
                <tr>
                    <td colspan="2">
                        <asp:Button ID="btnSave" runat="server" Text="Save" ValidationGroup="Save" 
                            OnClick="btnSave_Click" TabIndex="11" />&nbsp;&nbsp;
                        <asp:Button ID="btnBack" runat="server" Text="Back"/>&nbsp;&nbsp;<asp:Label ID="msgbox" runat="server" Text="" ForeColor="Red" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </div>

</asp:Content>
