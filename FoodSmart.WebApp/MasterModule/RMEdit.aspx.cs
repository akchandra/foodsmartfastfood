﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FoodSmart.BLL;
using FoodSmart.Common;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using FoodSmart.Utilities.ResourceManager;
using System.Data;

namespace FoodSmart.WebApp.MasterModule
{
    public partial class RMEdit : System.Web.UI.Page
    {
        #region Private Member Variables

        private int _userId = 0;
        private int _itemID = 0;
        private bool _canAdd = false;
        private bool _canEdit = false;
        private bool _canDelete = false;
        private bool _canView = false;

        #endregion

        #region Protected Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            RetriveParameters();
            CheckUserAccess();
            SetAttributes();


            if (!IsPostBack)
            {
                LoadItemGroup();
                //txtEffectiveDate.Text = DateTime.Now.AddDays(1).ToString("dd/MM/yyyy");
                LoadData();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            SaveRecord();
        }

        #endregion

        #region Private Methods

        private void RetriveParameters()
        {
            _userId = UserBLL.GetLoggedInUserId();

            //Get user permission.
            UserBLL.GetUserPermission(out _canAdd, out _canEdit, out _canDelete, out _canView);

            if (!ReferenceEquals(Request.QueryString["id"], null))
            {
                Int32.TryParse(GeneralFunctions.DecryptQueryString(Request.QueryString["id"].ToString()), out _itemID);
            }
        }

        private void SetAttributes()
        {

            if (!IsPostBack)
            {
                //txtHSN.MaxLength = 8;
                if (_itemID == -1) //Add mode
                {
                    if (!_canAdd) btnSave.Visible = false;
                }
                else
                {
                    if (!_canEdit) btnSave.Visible = false;
                }

                btnSave.ToolTip = ResourceManager.GetStringWithoutName("R00060");
                btnBack.ToolTip = ResourceManager.GetStringWithoutName("R00061");
                btnBack.OnClientClick = "javascript:return RedirectAfterCancelClick('RMList.aspx','" + ResourceManager.GetStringWithoutName("R00025") + "')";
                //spnName.InnerText = ResourceManager.GetStringWithoutName("R00046");
            }
        }

        private void CheckUserAccess()
        {
            if (!ReferenceEquals(Session[Constants.SESSION_USER_INFO], null))
            {
                IUser user = (IUser)Session[Constants.SESSION_USER_INFO];

                if (ReferenceEquals(user, null) || user.Id == 0)
                {
                    Response.Redirect("~/Login.aspx");
                }

                if (user.Id != (int)UserRole.Admin)
                {
                    Response.Redirect("~/Unauthorized.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Login.aspx");
            }

            if (_itemID == 0)
                Response.Redirect("~/Views/ManageUser.aspx");

            if (!_canView)
            {
                Response.Redirect("~/Unauthorized.aspx");
            }
        }

        private void LoadData()
        {
            ItemBLL oItembll = new ItemBLL();

            IItems oitem = (ItemsEntity)oItembll.GetRMForEdit(_itemID);

            if (!ReferenceEquals(oitem, null))
            {
                ddlRorF.SelectedValue = oitem.GroupType;
                LoadItemGroup();
                txtName.Text = oitem.ItemDescription;
                ddlGroup.SelectedValue = oitem.RMGroupID.ToString();
                txtUOM.Text = oitem.UOM;
               
                //ddlGroup.SelectedValue = oitem.ItemGroupID.ToString();
                txtMaxStock.Text = oitem.MaxStockLevel.ToString();
                txtMinStock.Text = oitem.MinStockLevel.ToString();
                txtReorder.Text = oitem.ReOrdStockLevel.ToString();
            }
        }

        private bool ValidateControls(IItems item, string mode)
        {
            bool isValid = true;
            return isValid;
        }

        private void SaveRecord()
        {
            IItems item = new ItemsEntity();
            BuildUserEntity(item);
            string strMode = "A";
            if (item.ItemID > 0)
            {
                strMode = "E";
            }
            if (ValidateControls(item, strMode))
            {

                ItemBLL itemBLL = new ItemBLL();
                int result = itemBLL.SaveRM(item, strMode);

                if (strMode == "E")
                {
                    //SendEmail(user);
                    Response.Redirect("~/MasterModule/RMList.aspx");
                }
                else
                {
                    txtName.Text = string.Empty;
                    txtUOM.Text = string.Empty;
                    txtMaxStock.Text = string.Empty;
                    txtMinStock.Text = string.Empty;
                    txtReorder.Text = string.Empty;
                    item.ItemID = -1;
                }
            }
        }

        private void BuildUserEntity(IItems item)
        {
            item.ItemID = _itemID;
            item.ItemDescription = txtName.Text.ToUpper();
            item.MaxStockLevel = txtMaxStock.Text.ToDecimal();
            item.MinStockLevel = txtMinStock.Text.ToDecimal();
            item.ReOrdStockLevel = txtReorder.Text.ToDecimal();
            item.UOM = txtUOM.Text.ToUpper();
            item.ItemGroupID = Convert.ToInt32(ddlGroup.SelectedValue);
            item.CreatedBy = _userId;
        }

        private void LoadItemGroup()
        {
            //DataRow row = new DataRow();
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            //POSWebRpt.BLL.itemService.SearchCriteria searchcriteria = new POSWebRpt.BLL.itemService.SearchCriteria();

            SearchCriteria searchcriteria = new SearchCriteria();
            searchcriteria.mode = "L";
            searchcriteria.rmItemType = ddlRorF.SelectedValue;
            ddlGroup.DataSource = null;

            ddlGroup.Items.Insert(0, "Select Group");
            ds = new ItemBLL().GetAllRMGroup(searchcriteria);
            dt = ds.Tables[0];

            DataRow dr = dt.NewRow();
            dr["RMGroupName"] = "--Select--";
            dr["pk_RMGroupID"] = 0;

            dt.Rows.InsertAt(dr, 0);

            if (dt.Rows.Count > 0)
            {
                ddlGroup.DataSource = dt;
                ddlGroup.DataTextField = "RMGroupName";
                ddlGroup.DataValueField = "pk_RMGroupID";
                ddlGroup.DataBind();
                ddlGroup.SelectedValue = "0";
            }

        }
        #endregion

        protected void ddlRorF_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadItemGroup();
        }

       
    }
}