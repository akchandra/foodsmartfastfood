﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FoodSmart.BLL;
using FoodSmart.Common;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using FoodSmart.Utilities.ResourceManager;
using System.Data;

namespace FoodSmart.WebApp.MasterModule
{
    public partial class EditLocation : System.Web.UI.Page
    {
        #region Private Member Variables

        private int _userId = 0;
        private int _LocId = 0;
        private bool _canAdd = false;
        private bool _canEdit = false;
        private bool _canDelete = false;
        private bool _canView = false;

        #endregion

        #region Protected Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            RetriveParameters();
            CheckUserAccess();
            SetAttributes();
            
            if (!IsPostBack)
            {
                LoadData();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            SaveUser();
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {

        }

        #endregion

        #region Private Methods

        private void RetriveParameters()
        {
            _userId = UserBLL.GetLoggedInUserId();

            //Get user permission.
            UserBLL.GetUserPermission(out _canAdd, out _canEdit, out _canDelete, out _canView);

            if (!ReferenceEquals(Request.QueryString["id"], null))
            {
                Int32.TryParse(GeneralFunctions.DecryptQueryString(Request.QueryString["id"].ToString()), out _LocId);
            }
        }

        private void SetAttributes()
        {
            spnName.Style["display"] = "none";
            spnAddr1.Style["display"] = "none";
            spnAddr2.Style["display"] = "none";
            spnLocStat.Style["display"] = "none";
            spnLocType.Style["display"] = "none";
            //SpnSTNo.Style["display"] = "none";
            spnVATNo.Style["display"] = "none";
            SpnPrinterName.Style["display"] = "none";

            if (!IsPostBack)
            {
                if (_LocId == -1) //Add mode
                {
                    if (!_canAdd) btnSave.Visible = false;
                }
                else
                {
                    if (!_canEdit) btnSave.Visible = false;
                }

                btnSave.ToolTip = ResourceManager.GetStringWithoutName("R00060");
                btnBack.ToolTip = ResourceManager.GetStringWithoutName("R00061");
                btnBack.OnClientClick = "javascript:return RedirectAfterCancelClick('ListLocation.aspx','" + ResourceManager.GetStringWithoutName("R00025") + "')";
            }

        }

        private void CheckUserAccess()
        {
            if (!ReferenceEquals(Session[Constants.SESSION_USER_INFO], null))
            {
                IUser user = (IUser)Session[Constants.SESSION_USER_INFO];

                if (ReferenceEquals(user, null) || user.Id == 0)
                {
                    Response.Redirect("~/Login.aspx");
                }

                if (user.RoleID != (int)UserRole.Admin)
                {
                    Response.Redirect("~/Unauthorized.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Login.aspx");
            }

            if (_LocId == 0)
                Response.Redirect("~/MasterModule/ListLocation.aspx");

            if (!_canView)
            {
                Response.Redirect("~/Unauthorized.aspx");
            }
        }

        private void LoadData()
        {
            ILocation Locn = new LocationBLL().GetLocData(_LocId);

            if (!ReferenceEquals(Locn, null))
            {
                txtAddress1.Text = Locn.LocAddress1;
                txtAddress2.Text = Locn.LocAddress2;
                txtLocName.Text = Locn.LocName;
                //txtServTaxNo.Text = Locn.ServTaxNo;
                txtVATNo.Text = Locn.GSTNo;
                ddlLocType.SelectedValue = Locn.LocType;
                ddlOF.SelectedValue = Locn.OwnOrFran;
                txtPrinterName.Text = Locn.PrinterName;
                txtOBDate.Text = Locn.dtOpStock.ToString("dd-MM-yyyy");
                chkComposit.Checked = Locn.Composit;
            }
        }

        private bool ValidateControls(IUser user)
        {
            bool isValid = true;

            return isValid;
        }

        private void SaveUser()
        {
            ILocation loc = new LocationEntity();
            BuildLocEntity(loc);
            int result = LocationBLL.SaveLocation(loc, _userId, "E");
            Response.Redirect("~/MasterModule/ListLocation.aspx");
        }

        private void BuildLocEntity(ILocation loc)
        {
            loc.LocID = _LocId;
            loc.LocName = txtLocName.Text;
            loc.LocAddress1 = txtAddress1.Text;
            loc.LocAddress2 = txtAddress2.Text;
            loc.LocType = ddlLocType.SelectedValue.ToString();
            loc.OwnOrFran = ddlOF.SelectedValue.ToString();
            loc.PrinterName = txtPrinterName.Text;
            loc.GSTNo = txtVATNo.Text;
            loc.Composit = chkComposit.Checked;
            loc.dtOpStock = txtOBDate.Text.ToDateTime();
            //loc.ServTaxNo = txtServTaxNo.Text;

        }


        #endregion

    }
}