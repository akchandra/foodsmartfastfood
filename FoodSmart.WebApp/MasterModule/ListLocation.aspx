﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ListLocation.aspx.cs" Inherits="FoodSmart.WebApp.MasterModule.ListLocation" MasterPageFile="~/Site.Master" Title=":: FoodSmart :: Add / Edit User" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="container" runat="Server">
    <div id="dvAsync" style="padding: 5px; display: none;">
        <div class="asynpanel">
            <div id="dvAsyncClose">
                <img alt="" src="../../Images/Close-Button.bmp" style="cursor: pointer;" onclick="ClearErrorState()" /></div>
            <div id="dvAsyncMessage">
            </div>
        </div>
    </div>
    <div id="headercaption">MANAGE LOCATION</div>
    <div style="width:850px;margin:0 auto;margin-top:30px;">        
        <fieldset style="width:100%;">
            <legend>Search Location</legend>
            <table>
                <tr>
                    <td>
                        <asp:TextBox ID="txtLocName" runat="server" CssClass="watermark" Width="240px" ForeColor="#747862"></asp:TextBox>
                        <%--<cc1:TextBoxWatermarkExtender ID="txtWMEUserName" runat="server" TargetControlID="txtUserName" WatermarkText="Type Username" WatermarkCssClass="watermark"></cc1:TextBoxWatermarkExtender>--%>
                    </td>
                   
                    <td><asp:Button ID="btnSearch" runat="server" Text="Search" Width="100px" OnClick="btnSearch_Click" />&nbsp;<asp:Button ID="btnReset" runat="server" Text="Reset" Width="100px" OnClick="btnReset_Click" /></td>
                </tr>
            </table>              
        </fieldset>
        <asp:UpdateProgress ID="uProgressUser" runat="server" AssociatedUpdatePanelID="upUser">
            <ProgressTemplate>
                <div class="progress">
                    <div id="image">
                        <img src="../../Images/PleaseWait.gif" alt="" /></div>
                    <div id="text">
                        Please Wait...</div>
                </div>
            </ProgressTemplate>        
        </asp:UpdateProgress>
        <fieldset id="fsList" runat="server" style="width:100%;min-height:100px;margin-top:10px;">
            <legend>User List</legend>
            <div style="float:right;padding-bottom:5px;">                
                Results Per Page:<asp:DropDownList ID="ddlPaging" runat="server" Width="75px" AutoPostBack="true" CssClass="chzn-select medium-select single"
                    OnSelectedIndexChanged="ddlPaging_SelectedIndexChanged">
                    <asp:ListItem Text="10" Value="10" />
                    <asp:ListItem Text="30" Value="30" />
                    <asp:ListItem Text="50" Value="50" />
                    <asp:ListItem Text="100" Value="100" />
                </asp:DropDownList>&nbsp;&nbsp;            
<%--                <asp:Button ID="btnAdd" runat="server" Text="Add New User" Width="130px" OnClick="btnAdd_Click" />--%>
            </div>
            <div>
                <span class="errormessage">* Indicates Inactive Location(s)</span>
            </div><br />            
            <div>
                <asp:UpdatePanel ID="upUser" runat="server" UpdateMode="Conditional">
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ddlPaging" EventName="SelectedIndexChanged" />
                    </Triggers>
                    <ContentTemplate>
                        <script type="text/javascript">
                            Sys.Application.add_load(LoadScript);
                        </script>
                        <asp:GridView ID="gvwLoc" runat="server" AutoGenerateColumns="false" 
                            AllowPaging="true" BorderStyle="None" BorderWidth="0" 
                            OnPageIndexChanging="gvwLoc_PageIndexChanging" 
                            OnRowDataBound="gvwLoc_RowDataBound" OnRowCommand="gvwLoc_RowCommand" 
                            Width="100%">
                        <PagerSettings Mode="NumericFirstLast" Position="Bottom" />
                        <PagerStyle CssClass="gridviewpager" />
                        <EmptyDataRowStyle CssClass="gridviewemptydatarow" />
                        <EmptyDataTemplate>No Location(s) Found</EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="Sl#">
                                <HeaderStyle CssClass="gridviewheader" />
                                <ItemStyle CssClass="gridviewitem" Width="5%" />                                    
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="gridviewheader" />
                                <ItemStyle CssClass="gridviewitem" Width="18%" />    
                                <HeaderTemplate><asp:LinkButton ID="lnkHName" runat="server" CommandName="Sort" CommandArgument="LocationName" Text="Location Name"></asp:LinkButton></HeaderTemplate>                                
                                <ItemTemplate>
                                    <asp:Label ID="lblName" runat="server"></asp:Label><asp:Label ID="lblInActive" runat="server" CssClass="errormessage" Font-Bold="true" Text=" *"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="gridviewheader" />
                                <ItemStyle CssClass="gridviewitem" Width="13%" />
                                <HeaderTemplate><asp:LinkButton ID="lnkHRole" runat="server" CommandName="Sort" CommandArgument="LocType" Text="Type"></asp:LinkButton></HeaderTemplate>                                    
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="gridviewheader" />
                                <ItemStyle CssClass="gridviewitem" Width="17%" />           
                                <HeaderTemplate><asp:LinkButton ID="lnkHFN" runat="server" CommandName="Sort" CommandArgument="OF" Text="Status"></asp:LinkButton></HeaderTemplate>                         
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="gridviewheader" />
                                <ItemStyle CssClass="gridviewitem" Width="13%" />   
                                <HeaderTemplate><asp:LinkButton ID="lnkHLN" runat="server" CommandName="Sort" CommandArgument="GSTNo" Text="GST No"></asp:LinkButton></HeaderTemplate>                                 
                            </asp:TemplateField>
                            <%--<asp:TemplateField>
                                <HeaderStyle CssClass="gridviewheader" />
                                <ItemStyle CssClass="gridviewitem" Width="13%" />
                                <HeaderTemplate><asp:LinkButton ID="lnkLoc" runat="server" CommandName="Sort" CommandArgument="SeviceTaxNo" Text="Service Tax No"></asp:LinkButton></HeaderTemplate>                                 
                            </asp:TemplateField>--%>
                            
                            <asp:TemplateField>
                                <HeaderStyle CssClass="gridviewheader" />
                                <ItemStyle CssClass="gridviewitem" Width="5%" HorizontalAlign="Center" VerticalAlign="Middle"/>   
                                <HeaderTemplate><asp:Label ID="Order" Text="Order" runat="server"></asp:Label></HeaderTemplate>                                 
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnOrder" runat="server" CommandName="Order" ImageUrl="~/Images/add.jpg" Height="16" Width="16"/>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="gridviewheader" />
                                <ItemStyle CssClass="gridviewitem" Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />                                    
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnEdit" runat="server" CommandName="Edit" ImageUrl="~/Images/edit.png" Height="16" Width="16" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="gridviewheader" />
                                <ItemStyle CssClass="gridviewitem" Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />                                    
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnRemove" runat="server" CommandName="Remove" ImageUrl="~/Images/remove.png" Height="16" Width="16" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </fieldset>
    </div>
</asp:Content>