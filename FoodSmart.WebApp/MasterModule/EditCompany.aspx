﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditCompany.aspx.cs" Inherits="FoodSmart.WebApp.MasterModule.EditCompany" MasterPageFile="~/Site.Master" Title=":: FoodSmart :: Update Parameters" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register Assembly="FoodSmart.WebApp" Namespace="FoodSmart.WebApp.CustomControls" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style1
        {
            width: 85px;
        }
        .textbox
        {}
        .style2
        {
            width: 149px;
        }
        .style3
        {
            width: 258px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="container" runat="Server">
    <div id="headercaption">MODIFY PARAMETERS</div>
    <center>
        <fieldset style="width:800px;">
            <legend>Modify Parameters</legend>
            <table border="0" cellpadding="3" cellspacing="3" style="width: 766px">
                <tr>
                    <td class="style3">Company Name:</td>
                    <td class="style1">
                        <asp:Label ID="lblCompName" runat="server" Width="600"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style3">Company Address1:</td>
                    <td class="style1">
                        <asp:Label ID="lblCompAddress1" runat="server" Width="600"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style3">Company Address2:</td>
                    <td class="style1">
                        <asp:Label ID="lblCompAddress2" runat="server" Width="600"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style3">Item Includes Tax</td>
                    <td class="style1">
                        <asp:RadioButtonList ID="rdoItemInclTax" runat="server" 
                            RepeatDirection="Horizontal" TextAlign="Left" style="margin-left: 0px">
                        <asp:ListItem Text="Yes" Value="Y" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="No" Value="N"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <%--<tr>
                    <td class="style3">Service Tax %:<span class="errormessage">*</span></td>
                    <td class="style1">
                        <cc2:CustomTextBox ID="txtStax" runat="server" CssClass="numerictextbox" TabIndex="13" ToolTip="Service Tax %"
                        Width="250px" Type="Decimal" MaxLength="15" Precision="8" Scale="2">
                        </cc2:CustomTextBox>
<%--

                        <asp:TextBox ID="txtStax" MaxLength="10" runat="server" ToolTip="Service Tax %" CssClass="textbox"></asp:TextBox><br />
                        <asp:RequiredFieldValidator ID="rfvStax" runat="server" CssClass="errormessage" ControlToValidate="txtStax" Display="Dynamic" ValidationGroup="Submit"></asp:RequiredFieldValidator>
--%>                </td>
                </tr>--%>
                <tr>
                    <td class="style3">
                        Invoice Bottom 1:
                    </td>
                    <td class="style2">
                        <asp:TextBox ID="txtInformation1" runat="server" CssClass="textboxuppercase" MaxLength="100" Width="250"></asp:TextBox><br />
                    </td>
                </tr>
                <tr>
                    <td class="style3">
                        Invoice Bottom 2:
                    </td>
                    <td class="style2">
                        <asp:TextBox ID="txtInformation2" runat="server" CssClass="textboxuppercase" MaxLength="100" Width="250"></asp:TextBox><br />
                    </td>
                </tr>
                <tr>
                    <td class="style3">
                        Invoice Bottom 3:
                    </td>
                    <td class="style2">
                        <asp:TextBox ID="txtInformation3" runat="server" CssClass="textboxuppercase" MaxLength="100" Width="250"></asp:TextBox><br />
                    </td>
                </tr>
                <tr>
                    <td class="style3">
                        Invoice Bottom 4:
                    </td>
                    <td class="style2">
                        <asp:TextBox ID="txtInformation4" runat="server" CssClass="textboxuppercase" MaxLength="100" Width="250"></asp:TextBox><br />
                    </td>
                </tr>

                <tr>
                    <td colspan="2"><asp:Button ID="btnSaveParameter" runat="server" Text="Save"
                            OnClick="btnSave_Click" ValidationGroup="Save" /></td>
                </tr>
            </table>
        </fieldset>
    </center>
</asp:Content>