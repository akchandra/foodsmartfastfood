﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ItemEdit.aspx.cs" Inherits="FoodSmart.WebApp.Views.Security.ItemEdit" MasterPageFile="~/Site.Master" Title=":: FoodSmart :: Add / Edit Item" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FoodSmart.WebApp" Namespace="FoodSmart.WebApp.CustomControls" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">    
    <script type="text/javascript">
        LoadScript();
    </script>
    <style type="text/css">
        .style7
        {
            height: 52px;
            width: 50%;
        }
        .style10
        {
            width: 47px;
        }
        .style11
        {
            width: 35%;
        }
        .style13
        {
            height: 52px;
            width: 71%;
        }
        .style14
        {
            width: 71%;
        }
        .style15
        {
            width: 50%;
        }
        .custtable
        {
            width: 107%;
        }
        .style16
        {
            width: 40%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="container" runat="Server">
    <div id="headercaption">ADD / EDIT ITEM</div>
    <div style="width:886px; padding-left:300px;">
        <fieldset style="width:100%;">
            <legend>Add / Edit Item</legend>
            <table style="height: 300px;vertical-align:top">
                <tr>
                    <td>
                        <table border="0" cellpadding="1" cellspacing="0" class="custtable">
                        <tr>
                            <td style="padding-bottom:5px;" class="style16">Item Type<span class="errormessage">*</span></td>
                            <td style="padding-bottom:5px;" class="style11">
                                <asp:DropDownList ID="ddlRorF" runat="server"  Width="200px" Height="20px" 
                                    AutoPostBack="true" onselectedindexchanged="ddlRorF_SelectedIndexChanged" 
                                    TabIndex="1" Enabled="false">
                                    <asp:ListItem Value="F">Finished Goods</asp:ListItem>
                                    <asp:ListItem Value="R">Raw Material</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td></td>
                            <td></td>
                            <td class="style10"></td>
                            <td style="padding-bottom:5px;" class="style14">Group<span class="errormessage">*</span></td>
                            <td style="padding-bottom:5px;" class="style15">
                                <asp:DropDownList ID="ddlGroup" runat="server" Width="250px" 
                                    CssClass="dropdownlist" TabIndex="2"><asp:ListItem Value="0" Text="--Select--"></asp:ListItem></asp:DropDownList>
                                <br /><asp:RequiredFieldValidator ID="rfvGroup" runat="server" ControlToValidate="ddlGroup" InitialValue="0" ErrorMessage="This field is required*" CssClass="errormessage" ValidationGroup="Save" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-bottom:5px;" class="style16">Item Name:<span class="errormessage">*</span></td>
                            <td style="padding-bottom:5px;" class="style11">
                                <br />
                                <asp:TextBox ID="txtName" runat="server" CssClass="textboxuppercase" 
                                    MaxLength="30" Width="250px" height="20px" TabIndex="3"></asp:TextBox>
                                <br /><asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName" InitialValue="" ErrorMessage="This field is required*" CssClass="errormessage" ValidationGroup="Save" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                            <td></td>
                            <td></td>
                            <td class="style10"></td>
                            <td style="padding-bottom:5px;" class="style14">UOM<span class="errormessage">*</span></td>
                            <td style="padding-bottom:5px;" class="style15">
                                <br />
                                <asp:TextBox ID="txtUOM" runat="server" CssClass="textboxuppercase" 
                                    MaxLength="30" Width="100px" height="20px" TabIndex="4"></asp:TextBox>
                                <br /><asp:RequiredFieldValidator ID="rfvUom" runat="server" ControlToValidate="txtUOM" InitialValue="" ErrorMessage="This field is required*" CssClass="errormessage" ValidationGroup="Save" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-bottom:5px;" class="style16">CGST %<span class="errormessage">*</span></td>
                            <td style="padding-bottom:5px;" class="style11">
                                <br />
                                <cc2:CustomTextBox ID="txtCGSTPer" runat="server" CssClass="numerictextbox" 
                                    TabIndex="5" Width="150px" Type="Decimal" MaxLength="15" Precision="6" 
                                    Scale="2"></cc2:CustomTextBox>
                                <asp:RequiredFieldValidator ID="rfvCGSTPer" runat="server" ControlToValidate="txtCGSTPer" InitialValue="" ErrorMessage="This field is required*" CssClass="errormessage" ValidationGroup="Save" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td style="padding-bottom:5px;" class="style14">SGST %<span class="errormessage">*</span></td>
                            <td style="padding-bottom:5px;" class="style15">
                                <br />
                                <cc2:CustomTextBox ID="txtSGSTPer" runat="server" CssClass="numerictextbox" 
                                    TabIndex="5" Width="150px" Type="Decimal" MaxLength="15" Precision="6" 
                                    Scale="2"></cc2:CustomTextBox>
                                <asp:RequiredFieldValidator ID="rfvSGSTPer" runat="server" ControlToValidate="txtSGSTPer" InitialValue="" ErrorMessage="This field is required*" CssClass="errormessage" ValidationGroup="Save" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                            
                        </tr>
                        
                        <tr>

                            <td style="padding-bottom:5px;" class="style16">HSN Code<span class="errormessage">*</span></td>
                            <td style="padding-bottom:5px;" class="style11">
                                <br />
                                <asp:TextBox ID="txtHSN" runat="server" CssClass="textboxuppercase" 
                                    MaxLength="30" Width="100px" height="20px" TabIndex="4"></asp:TextBox>
                                <br /><asp:RequiredFieldValidator ID="rfvHSN" runat="server" ControlToValidate="txtHSN" InitialValue="" ErrorMessage="This field is required*" CssClass="errormessage" ValidationGroup="Save" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                            <td></td>
                            <td></td>
                            <td class="style10"></td>
                            <td style="padding-bottom:5px;" class="style14">Effective Date<span class="errormessage">*</span></td>
                            <td style="padding-bottom:5px;" class="style15">
                                <br />
                                <asp:TextBox ID="txtEffectiveDate" runat="server" CssClass="textboxuppercase" MaxLength="50" Width="80px" TabIndex="6"></asp:TextBox>
                                    <cc1:CalendarExtender ID="rfvEffectiveDate" TargetControlID="txtEffectiveDate" runat="server" Format="dd/MM/yyyy" Enabled="True" />
                                    <br />
                                    <asp:RequiredFieldValidator ID="rfvEffDate" runat="server" ControlToValidate="txtEffectiveDate" ErrorMessage="This field is required*" CssClass="errormessage" ValidationGroup="Save" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                            <%--<td style="padding-bottom:5px;" class="style4">Service Tax<span class="errormessage">*</span></td>
                            <td style="padding-bottom:5px;" class="style5">
                                <asp:DropDownList ID="DropDownList1" runat="server"  Width="200px" Height="20px" CssClass="chzn-select medium-select single">
                                    <asp:ListItem Selected="True" Value="0">Not Applicable</asp:ListItem>
                                    <asp:ListItem Value="1">Applicable</asp:ListItem>
                                </asp:DropDownList>
                            </td>--%>
                            
                        </tr>
                        <tr ID="trProcessedAt" runat="server">
                            <td style="padding-bottom:5px;" class="style16">Processed At<span class="errormessage">*</span></td>
                            <td style="padding-bottom:5px;" class="style11">
                                <asp:DropDownList ID="ddlProcessed" runat="server"  Width="200px" Height="20px" 
                                    TabIndex="7">
                                    <asp:ListItem Value="O">Outlets</asp:ListItem>
                                    <asp:ListItem Selected="True" Value="F">Kitchen</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td></td>
                            <td></td>
                            <td class="style10"></td>
                            <td class="style13">Discount<span class="errormessage">*</span></td>
                            <td class="style7">
                            <asp:DropDownList ID="ddlDisc" runat="server" Width="250px" height="20px" 
                                    TabIndex="8">
                                <asp:ListItem Value="0">Not Allowed</asp:ListItem>
                                <asp:ListItem Selected="True" Value="1">Allowed</asp:ListItem>
                            </asp:DropDownList><br />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-bottom:5px;" class="style16">Introductory Rate<span class="errormessage">*</span></td>
                            <td style="padding-bottom:5px;" class="style11">
                                <br />
                                <cc2:CustomTextBox ID="txtiSellingRate" runat="server" 
                                    CssClass="numerictextbox" TabIndex="9" Width="150px" Type="Decimal" 
                                    MaxLength="15" Precision="6" Scale="2"></cc2:CustomTextBox>
                                <asp:RequiredFieldValidator ID="rfvisellingRate" runat="server" ControlToValidate="txtiSellingRate" InitialValue="" ErrorMessage="This field is required*" CssClass="errormessage" ValidationGroup="Save" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                            <td></td>
                            <td></td>
                            <td class="style10"></td>
                            <td style="padding-bottom:5px;" class="style14">Existing Rate<span class="errormessage">*</span></td>
                            <td style="padding-bottom:5px;" class="style15">
                                <br />
                                <cc2:CustomTextBox ID="txtSellingRate" runat="server" CssClass="numerictextbox" 
                                    TabIndex="10" Width="150px" Type="Decimal" MaxLength="15" Precision="6" 
                                    Scale="2" Enabled="false"></cc2:CustomTextBox>
                            </td>
                        </tr>
                    </table>

                    </td>
                </tr>
                   
                <tr>
                    <td colspan="2">
                        <asp:Button ID="btnSave" runat="server" Text="Save" ValidationGroup="Save" 
                            OnClick="btnSave_Click" TabIndex="11" />&nbsp;&nbsp;
                        <asp:Button ID="btnBack" runat="server" Text="Back"/>&nbsp;&nbsp;<asp:Label ID="msgbox" runat="server" Text="" ForeColor="Red" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </div>

</asp:Content>