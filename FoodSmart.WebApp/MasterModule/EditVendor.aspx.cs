﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FoodSmart.BLL;
using FoodSmart.Common;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using FoodSmart.Utilities.ResourceManager;
using System.Data;

namespace FoodSmart.WebApp.MasterModule
{
    public partial class EditVendor : System.Web.UI.Page
    {
        #region Private Member Variables

        private int _userId = 0;
        private int _VendorId = 0;
        private bool _canAdd = false;
        private bool _canEdit = false;
        private bool _canDelete = false;
        private bool _canView = false;
        private string _Stat = "";

        #endregion

        #region Protected Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            RetriveParameters();
            CheckUserAccess();
            SetAttributes();
            //LoadState();

            if (!IsPostBack)
            {
                LoadData();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            SaveVendor();
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {

        }

        #endregion

        #region Private Methods

        private void RetriveParameters()
        {
            _userId = UserBLL.GetLoggedInUserId();

            //Get user permission.
            UserBLL.GetUserPermission(out _canAdd, out _canEdit, out _canDelete, out _canView);

            if (!ReferenceEquals(Request.QueryString["id"], null))
            {
                Int32.TryParse(GeneralFunctions.DecryptQueryString(Request.QueryString["id"].ToString()), out _VendorId);
            }

            _Stat = "E";
            if (_VendorId == -1)
                _Stat = "A";
        }

        private void SetAttributes()
        {
            spnName.Style["display"] = "none";
            spnAddr1.Style["display"] = "none";
            //SpnSTNo.Style["display"] = "none";
            spnVATNo.Style["display"] = "none";
            //SpnPrinterName.Style["display"] = "none";

            if (!IsPostBack)
            {
                if (_VendorId == -1) //Add mode
                {
                    if (!_canAdd) btnSave.Visible = false;
                }
                else
                {
                    if (!_canEdit) btnSave.Visible = false;
                }

                btnSave.ToolTip = ResourceManager.GetStringWithoutName("R00060");
                btnBack.ToolTip = ResourceManager.GetStringWithoutName("R00061");
                btnBack.OnClientClick = "javascript:return RedirectAfterCancelClick('ListVendor.aspx','" + ResourceManager.GetStringWithoutName("R00025") + "')";
            }

        }

        private void CheckUserAccess()
        {
            if (!ReferenceEquals(Session[Constants.SESSION_USER_INFO], null))
            {
                IUser user = (IUser)Session[Constants.SESSION_USER_INFO];

                if (ReferenceEquals(user, null) || user.Id == 0)
                {
                    Response.Redirect("~/Login.aspx");
                }

                if (user.RoleID != (int)UserRole.Admin)
                {
                    Response.Redirect("~/Unauthorized.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Login.aspx");
            }

            if (_VendorId == 0)
                Response.Redirect("~/MasterModule/ListLocation.aspx");

            if (!_canView)
            {
                Response.Redirect("~/Unauthorized.aspx");
            }
        }

        private void LoadData()
        {
            IVendor ven = new VendorBLL().GetVendorForEdit(_VendorId);

            if (!ReferenceEquals(ven, null))
            {
                txtAddress.Text = ven.VendorAddress;
                txtVendorName.Text = ven.VendorName;
                txtGSTNo.Text = ven.GSTNo;
                txtPhone.Text = ven.LandPhone;
                txtPAN.Text = ven.PANNo;
                //ddlState.SelectedValue = ven.StateID.ToString();
            }
        }

        private bool ValidateControls(IUser user)
        {
            bool isValid = true;

            return isValid;
        }

        private void SaveVendor()
        {
            IVendor ven = new VendorEntity();
            BuildVendorEntity(ven);
            int result = VendorBLL.SaveVendor(ven, _Stat);
            Response.Redirect("~/MasterModule/ListVendor.aspx");
        }

        private void BuildVendorEntity(IVendor ven)
        {
            ven.VendorID = _VendorId;
            ven.VendorName = txtVendorName.Text;
            ven.VendorAddress = txtAddress.Text;
            ven.PANNo = txtPAN.Text;
            ven.LandPhone = txtPhone.Text;
            //ven.StateID = ddlState.SelectedValue.ToInt();
            ven.GSTNo = txtGSTNo.Text;
        }
        #endregion

    }
}