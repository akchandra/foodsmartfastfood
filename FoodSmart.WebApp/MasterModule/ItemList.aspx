﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ItemList.aspx.cs" Inherits="FoodSmart.WebApp.Views.Security.ItemList" MasterPageFile="~/Site.Master" Title=":: FoodSmart :: Item Master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style1
        {
            width: 244px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="container" runat="Server">
    <div id="dvAsync" style="padding: 5px; display: none;">
        <div class="asynpanel">
            <div id="dvAsyncClose">
                <img alt="" src="../../Images/Close-Button.bmp" style="cursor: pointer;" onclick="ClearErrorState()" /></div>
            <div id="dvAsyncMessage">
            </div>
        </div>
    </div>
    <div id="headercaption">MANAGE ITEM&nbsp;&nbsp;&nbsp; </div>
    <div style="width:850px;margin:0 auto;margin-top:30px;">        
        <fieldset style="width:100%;">
            <legend>Search Item</legend>
            <table style="width: 719px">
                <tr>
                    <td style="margin-left: 40px;" class="style1">
                        <asp:TextBox ID="txtItemName" runat="server" CssClass="watermark" Width="232px" 
                            ForeColor="#747862" Height="23px"></asp:TextBox>
                        <cc1:TextBoxWatermarkExtender ID="txtWMEItemName" runat="server" TargetControlID="txtItemName"  WatermarkText="Type Item Name" WatermarkCssClass="watermark"></cc1:TextBoxWatermarkExtender>
                    </td>
                    <%--<td>
                        <asp:DropDownList ID="ddlRorF" runat="server" Width="200px" AutoPostBack="true" 
                            onselectedindexchanged="ddlRorF_SelectedIndexChanged">
                            <asp:ListItem Selected="True" Value="F">Finished Goods</asp:ListItem>
                            <asp:ListItem Value="R">Raw Materials</asp:ListItem>
                        </asp:DropDownList>
                    </td>--%>
                    <td>
                        <asp:DropDownList ID="ddlGroup" runat="server" Width="300px" AutoPostBack="true" 
                            onselectedindexchanged="ddlGroup_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <%--<td><asp:Button ID="btnSearch" runat="server" Text="Search" Width="100px" OnClick="btnSearch_Click" />&nbsp;<asp:Button ID="btnReset" runat="server" Text="Reset" Width="100px" OnClick="btnReset_Click" /></td>--%>
                    <td><asp:Button ID="btnSearch" runat="server" Text="Search" Width="100px" OnClick="btnSearch_Click" /></td>
                    <td><asp:Button ID="btnReset" runat="server" Text="Reset" Width="100px" OnClick="btnReset_Click" /></td>
                </tr>
            </table>              
        </fieldset>
        <asp:UpdateProgress ID="uProgressItem" runat="server" AssociatedUpdatePanelID="upItem">
            <ProgressTemplate>
                <div class="progress">
                    <div id="image">
                        <img src="../../Images/PleaseWait.gif" alt="" /></div>
                    <div id="text">
                        Please Wait...</div>
                </div>
            </ProgressTemplate>        
        </asp:UpdateProgress>
        <fieldset id="fsList" runat="server" style="width:100%;min-height:100px;margin-top:10px;">
            <legend>Item Group List</legend>
            <div style="float:right;padding-bottom:5px;">                
                Results Per Page:<asp:DropDownList ID="ddlPaging" runat="server" Width="75px" AutoPostBack="true" CssClass="chzn-select medium-select single"
                    OnSelectedIndexChanged="ddlPaging_SelectedIndexChanged">
                    <asp:ListItem Text="10" Value="10" />
                    <asp:ListItem Text="30" Value="30" />
                    <asp:ListItem Text="50" Value="50" />
                    <asp:ListItem Text="100" Value="100" />
                </asp:DropDownList>&nbsp;&nbsp;            
                <asp:Button ID="btnAdd" runat="server" Text="Add New Item" Width="130px" OnClick="btnAdd_Click" />
            </div>
            <div>
                <%--<span class="errormessage">* Indicates Inactive User(s)</span>--%>
            </div><br />            
            <div>
                <asp:UpdatePanel ID="upItem" runat="server" UpdateMode="Conditional">
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ddlPaging" EventName="SelectedIndexChanged" />
                    </Triggers>
                    <ContentTemplate>
                        <script type="text/javascript">
                            Sys.Application.add_load(LoadScript);
                        </script>
                        <asp:GridView ID="gvwItem" runat="server" AutoGenerateColumns="false" AllowPaging="true" BorderStyle="None" BorderWidth="0" OnPageIndexChanging="gvwItem_PageIndexChanging" OnRowDataBound="gvwItem_RowDataBound" OnRowCommand="gvwItem_RowCommand" Width="100%">
                        <PagerSettings Mode="NumericFirstLast" Position="Bottom" />
                        <PagerStyle CssClass="gridviewpager" />
                        <EmptyDataRowStyle CssClass="gridviewemptydatarow" />
                        <EmptyDataTemplate>No Record Found</EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="Sl#">
                                <HeaderStyle CssClass="gridviewheader" />
                                <ItemStyle CssClass="gridviewitem" Width="5%" />                                    
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="gridviewheader" />
                                <ItemStyle CssClass="gridviewitem" Width="35%" />           
                                <HeaderTemplate><asp:LinkButton ID="lnkHItem" runat="server" CommandName="Sort" CommandArgument="ItemDescr" Text="Item"></asp:LinkButton></HeaderTemplate>                         
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="gridviewheader" />
                                <ItemStyle CssClass="gridviewitem" Width="30%" />   
                                <HeaderTemplate><asp:LinkButton ID="lnkHGrpName" runat="server" CommandName="Sort" CommandArgument="GroupName" Text="Item Group"></asp:LinkButton></HeaderTemplate>                         
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="gridviewheader" />
                                <ItemStyle CssClass="gridviewitem" Width="10%" />   
                                <HeaderTemplate><asp:LinkButton ID="lnkHVAT" runat="server" CommandName="Sort" CommandArgument="CGSTPer" Text="CGST %"></asp:LinkButton></HeaderTemplate>                         
                            </asp:TemplateField>
                             <asp:TemplateField>
                                <HeaderStyle CssClass="gridviewheader" />
                                <ItemStyle CssClass="gridviewitem" Width="10%" />   
                                <HeaderTemplate><asp:LinkButton ID="lnkHVAT" runat="server" CommandName="Sort" CommandArgument="SGSTPer" Text="SGST %"></asp:LinkButton></HeaderTemplate>                         
                            </asp:TemplateField>
                             <asp:TemplateField>
                                <HeaderStyle CssClass="gridviewheader" />
                                <ItemStyle CssClass="gridviewitem" Width="10%" />   
                                <HeaderTemplate><asp:LinkButton ID="lnkHVAT" runat="server" CommandName="Sort" CommandArgument="HSNCode" Text="HSN Code"></asp:LinkButton></HeaderTemplate>                         
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="gridviewheader" />
                                <ItemStyle CssClass="gridviewitem" Width="15%" />   
                                <HeaderTemplate><asp:LinkButton ID="lnkHEFFDT" runat="server" CommandName="Sort" CommandArgument="EffectiveDate" Text="Introduced"></asp:LinkButton></HeaderTemplate>                         
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="gridviewheader" />
                                <ItemStyle CssClass="gridviewitem" Width="0%" />   
                                <HeaderTemplate><asp:LinkButton ID="lnkHITMID" runat="server" CommandName="Sort" CommandArgument="ItemID" Text=""></asp:LinkButton></HeaderTemplate>                         
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="gridviewheader" />
                                <ItemStyle CssClass="gridviewitem" Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />                                    
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnEdit" runat="server" CommandName="Edit" ImageUrl="~/Images/edit.png" Height="16" Width="16" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="gridviewheader" />
                                <ItemStyle CssClass="gridviewitem" Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />                                    
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnRemove" runat="server" CommandName="Remove" ImageUrl="~/Images/remove.png" Height="16" Width="16" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </fieldset>
    </div>
    <script type = "text/javascript">
        function Confirm(msg) 
        {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm(msg)) 
            {
                confirm_value.value = "Yes";
            } 
            else 
            {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }
    </script>
</asp:Content>