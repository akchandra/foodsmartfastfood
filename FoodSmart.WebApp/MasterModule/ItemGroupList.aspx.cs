﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FoodSmart.BLL;
using FoodSmart.Common;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using FoodSmart.Utilities.ResourceManager;

namespace FoodSmart.WebApp.Views.Security
{
    public partial class ItemGroupList : System.Web.UI.Page
    {
        #region Private Member Variables

        private int _userId = 0;
        private bool _canAdd = false;
        private bool _canEdit = false;
        private bool _canDelete = false;
        private bool _canView = false;

        #endregion

        #region Protected Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            RetriveParameters();
            CheckUserAccess();
            SetAttributes();

            if (!IsPostBack)
            {
                RetrieveSearchCriteria();
                LoadItemGroup();
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            RedirecToAddEditPage(-1);
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            SaveNewPageIndex(0);
            LoadItemGroup();
            upGrp.Update();
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
 
            txtGrpName.Text = string.Empty;
            cmbRorF.SelectedIndex = 0;
            
            SaveNewPageIndex(0);
            LoadItemGroup();
            upGrp.Update();
        }

        protected void gvwItemGrp_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            int newIndex = e.NewPageIndex;
            gvwItemGrp.PageIndex = e.NewPageIndex;
            SaveNewPageIndex(e.NewPageIndex);
            LoadItemGroup();
        }
        protected void gvwItemGrp_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Sort"))
            {
                if (ViewState[Constants.SORT_EXPRESSION] == null)
                {
                    ViewState[Constants.SORT_EXPRESSION] = e.CommandArgument.ToString();
                    ViewState[Constants.SORT_DIRECTION] = "ASC";
                }
                else
                {
                    if (ViewState[Constants.SORT_EXPRESSION].ToString() == e.CommandArgument.ToString())
                    {
                        if (ViewState[Constants.SORT_DIRECTION].ToString() == "ASC")
                            ViewState[Constants.SORT_DIRECTION] = "DESC";
                        else
                            ViewState[Constants.SORT_DIRECTION] = "ASC";
                    }
                    else
                    {
                        ViewState[Constants.SORT_DIRECTION] = "ASC";
                        ViewState[Constants.SORT_EXPRESSION] = e.CommandArgument.ToString();
                    }
                }

                LoadItemGroup();
            }
            else if (e.CommandName == "Edit")
            {
                RedirecToAddEditPage(Convert.ToInt32(e.CommandArgument));
            }
            else if (e.CommandName == "Remove")
            {
                DeleteItemGroup(Convert.ToInt32(e.CommandArgument));
            }
            
        }

        protected void gvwItemGrp_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                GeneralFunctions.ApplyGridViewAlternateItemStyle(e.Row, 4);

                ScriptManager sManager = ScriptManager.GetCurrent(this);

                e.Row.Cells[0].Text = ((gvwItemGrp.PageSize * gvwItemGrp.PageIndex) + e.Row.RowIndex + 1).ToString();
                //e.Row.Cells[1].Text = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Name"));

                //e.Row.Cells[2].Text = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "pk_ItemGroupID"));
                e.Row.Cells[1].Text = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "RorF"));
                e.Row.Cells[2].Text = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Descr"));
                
                //e.Row.Cells[2].Visible = false;

                //e.Row.Cells[3].Visible = false;

                
                // Edit link
                ImageButton btnEdit = (ImageButton)e.Row.FindControl("btnEdit");
                btnEdit.ToolTip = ResourceManager.GetStringWithoutName("R00035");
                btnEdit.CommandArgument = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "pk_ItemGroupID"));

                //Delete link
                ImageButton btnRemove = (ImageButton)e.Row.FindControl("btnRemove");
                btnRemove.ToolTip = ResourceManager.GetStringWithoutName("R00034");
                btnRemove.CommandArgument = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "pk_ItemGroupID"));
               
                if (Convert.ToString(DataBinder.Eval(e.Row.DataItem, "pk_ItemGroupID")) == "1")
                {
                    btnRemove.OnClientClick = "javascript:alert('" + ResourceManager.GetStringWithoutName("R00043") + "');return false;";
                }
                else
                {
                    if (_canDelete)
                    {
                        btnRemove.OnClientClick = "javascript:return confirm('" + ResourceManager.GetStringWithoutName("R00021") + "');";
                    }
                    else
                    {
                        btnRemove.OnClientClick = "javascript:alert('" + ResourceManager.GetStringWithoutName("R00013") + "');return false;";
                    }
                }
            }
        }

        protected void ddlPaging_SelectedIndexChanged(object sender, EventArgs e)
        {
            int newPageSize = Convert.ToInt32(ddlPaging.SelectedValue);
            SaveNewPageSize(newPageSize);
            LoadItemGroup();
            upGrp.Update();
        }

        protected void cmbRorF_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadItemGroup();
        }

        #endregion

        #region Private Methods

        private void RetriveParameters()
        {
            _userId = UserBLL.GetLoggedInUserId();

            //Get user permission.
            UserBLL.GetUserPermission(out _canAdd, out _canEdit, out _canDelete, out _canView);
        }

        private void CheckUserAccess()
        {
            if (!ReferenceEquals(Session[Constants.SESSION_USER_INFO], null))
            {
                IUser user = (IUser)Session[Constants.SESSION_USER_INFO];

                if (ReferenceEquals(user, null) || user.Id == 0)
                {
                    Response.Redirect("~/Login.aspx");
                }

                if (user.RoleID != (int)UserRole.Admin)
                {
                    Response.Redirect("~/Unauthorized.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Login.aspx");
            }

            if (!_canView)
            {
                Response.Redirect("~/Unauthorized.aspx");
            }
        }

        private void SetAttributes()
        {
            if (!IsPostBack)
            {
               // txtWMEGrpName.WatermarkText = ResourceManager.GetStringWithoutName("R00065");
                 btnSearch.ToolTip = ResourceManager.GetStringWithoutName("R00055");
                //btnReset.ToolTip = ResourceManager.GetStringWithoutName("R00054");
                btnAdd.ToolTip = ResourceManager.GetStringWithoutName("R00057");
                gvwItemGrp.PageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
                gvwItemGrp.PagerSettings.PageButtonCount = Convert.ToInt32(ConfigurationManager.AppSettings["PageButtonCount"]);
            }
        }

        private void DeleteItemGroup(int groupID)
        {
            string message = string.Empty;
            string confirmValue = "";

            if (!CheckForDeleted(Convert.ToInt32(groupID)))
            {
               message= "Record Already Deleted.";
               ScriptManager.RegisterStartupScript(this, typeof(Page), "alert", "<script>javascript:void alert('" + message + "');</script>", false);
               return;
             }

            if (!CheckDeletePossible((Convert.ToInt32(groupID))))
            {
                message = "Item Group Present on Item Master. Deletion on Group will Delete all Items within the Group";
                //ScriptManager.RegisterStartupScript(this, typeof(Page), "confirm", "<script>javascript:return Confirm('" + message + "');</script>", true);
                //confirmValue = Request.Form["confirm_value"];
                //if (confirmValue != "Yes")
                //{
                //    return;
                //}
                ScriptManager.RegisterStartupScript(this, typeof(Page), "alert", "<script>javascript:void alert('" + message + "');</script>", false);
                return;
            }
            ItemBLL itembll = new ItemBLL();
            itembll.DeleteItemgroup(Convert.ToInt32(groupID));

            SearchCriteria searchCriteria = new SearchCriteria();
            BuildSearchCriteria(searchCriteria);
            LoadItemGroup();
                //foreach (DataGridViewRow row in dvgItemGroup.Rows)
                //{
                //    row.Height = 25;
                //}
         
        }

        private bool CheckForDeleted(int groupID)
        {
            ItemBLL itemgrpbll = new ItemBLL();
            FoodSmart.Entity.ItemsEntity oitemGrpEntity = (FoodSmart.Entity.ItemsEntity)itemgrpbll.GetItemGroupForEdit(groupID);
            if (oitemGrpEntity.GroupStatus)
            {
                return true;
            }
            else
                return false;
        }

        private bool CheckDeletePossible(int groupID)
        {
            ItemBLL itemGroupbll = new ItemBLL();

            int stat = itemGroupbll.ChkItemGroupDelPossible(groupID);
            if (stat == 1)
            {
                return false;
            }
            else
                return true;
        }

        private void LoadItemGroup()
        {
            if (!ReferenceEquals(Session[Constants.SESSION_SEARCH_CRITERIA], null))
            {
                SearchCriteria searchCriteria = (SearchCriteria)Session[Constants.SESSION_SEARCH_CRITERIA];

                if (!ReferenceEquals(searchCriteria, null))
                {
                    BuildSearchCriteria(searchCriteria);
                    gvwItemGrp.PageIndex = searchCriteria.PageIndex;
                    if (searchCriteria.PageSize > 0) gvwItemGrp.PageSize = searchCriteria.PageSize;

                    ItemBLL itembll = new ItemBLL();
                    gvwItemGrp.DataSource = itembll.GetAllItemGroup(searchCriteria);  //UserBLL.GetAllUserList(searchCriteria);
                    gvwItemGrp.DataBind();
                }
            }
        }

        private void RedirecToAddEditPage(int id)
        {
            string encryptedId = GeneralFunctions.EncryptQueryString(id.ToString());
            Response.Redirect("~/MasterModule/ItemGroupEdit.aspx?id=" + encryptedId);
        }

        private void BuildSearchCriteria(SearchCriteria criteria)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (!ReferenceEquals(ViewState[Constants.SORT_EXPRESSION], null) && !ReferenceEquals(ViewState[Constants.SORT_DIRECTION], null))
            {
                sortExpression = Convert.ToString(ViewState[Constants.SORT_EXPRESSION]);
                sortDirection = Convert.ToString(ViewState[Constants.SORT_DIRECTION]);
            }
            else
            {
                sortExpression = "Descr";
                sortDirection = "ASC";
            }

            criteria.SortExpression = sortExpression;
            criteria.SortDirection = sortDirection;
            criteria.ItemGroupName = (txtGrpName.Text == ResourceManager.GetStringWithoutName("R00065")) ? string.Empty : txtGrpName.Text.Trim();
            criteria.RorF = "F";
            Session[Constants.SESSION_SEARCH_CRITERIA] = criteria;
        }

        private void RetrieveSearchCriteria()
        {
            bool isCriteriaExists = false;

            if (!ReferenceEquals(Session[Constants.SESSION_SEARCH_CRITERIA], null))
            {
                SearchCriteria criteria = (SearchCriteria)Session[Constants.SESSION_SEARCH_CRITERIA];

                if (!ReferenceEquals(criteria, null))
                {
                    if (criteria.CurrentPage != PageName.UserMaster)
                    {
                        criteria.Clear();
                        SetDefaultSearchCriteria(criteria);
                    }
                    else
                    {
                        txtGrpName.Text = criteria.ItemGroupName;
                        cmbRorF.SelectedIndex = criteria.RorF == "F" ? 0 : 1;
                        gvwItemGrp.PageIndex = criteria.PageIndex;
                        gvwItemGrp.PageSize = criteria.PageSize;
                        ddlPaging.SelectedValue = criteria.PageSize.ToString();
                        isCriteriaExists = true;
                    }
                }
            }

            if (!isCriteriaExists)
            {
                SearchCriteria newcriteria = new SearchCriteria();
                SetDefaultSearchCriteria(newcriteria);
            }
        }

        private void SetDefaultSearchCriteria(SearchCriteria criteria)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            criteria.SortExpression = sortExpression;
            criteria.SortDirection = sortDirection;
            criteria.CurrentPage = PageName.UserMaster;
            criteria.PageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
            Session[Constants.SESSION_SEARCH_CRITERIA] = criteria;
        }

        private void SaveNewPageIndex(int newIndex)
        {
            if (!ReferenceEquals(Session[Constants.SESSION_SEARCH_CRITERIA], null))
            {
                SearchCriteria criteria = (SearchCriteria)Session[Constants.SESSION_SEARCH_CRITERIA];

                if (!ReferenceEquals(criteria, null))
                {
                    criteria.PageIndex = newIndex;
                }
            }
        }

        private void SaveNewPageSize(int newPageSize)
        {
            if (!ReferenceEquals(Session[Constants.SESSION_SEARCH_CRITERIA], null))
            {
                SearchCriteria criteria = (SearchCriteria)Session[Constants.SESSION_SEARCH_CRITERIA];

                if (!ReferenceEquals(criteria, null))
                {
                    criteria.PageSize = newPageSize;
                }
            }
        }

    

        
        #endregion

       
        

        
    }
}