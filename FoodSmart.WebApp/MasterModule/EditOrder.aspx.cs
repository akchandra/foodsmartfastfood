﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FoodSmart.BLL;
using FoodSmart.Common;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using FoodSmart.Utilities.ResourceManager;
using System.Data;

namespace FoodSmart.WebApp.MasterModule
{
    public partial class EditOrder : System.Web.UI.Page
    {
        #region Private Member Variables

        private int _userId = 0;
        private int _rId = 0;
        private string CalledFrom;
        private bool _canAdd = false;
        private bool _canEdit = false;
        private bool _canDelete = false;
        private bool _canView = false;

        #endregion

        #region Protected Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            RetriveParameters();
            //CheckUserAccess();
            SetAttributes();

            if (!IsPostBack)
            {
                LoadLocations();
                if (CalledFrom == "L")
                {
                    ddlLoc.Enabled = false;
                    ddlLoc.SelectedValue = _rId.ToString();
                    ddlOrdType.SelectedValue = "R";
                }
                else
                {
                    ddlLoc.Enabled = true;
                    ddlOrdType.SelectedValue = "R";
                    ddlOrdType.Enabled = true;
                }
                LoadSubItems();
                LoadData();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            SaveTransaction();
        }

        protected void btnSaveItem_Click(object sender, EventArgs e)
        {
            if (ddlSubItem.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "alert", "<script>javascript:void alert('Please select an item');</script>", false);
                return;
            }
            AddSubItem();
            ddlSubItemType.SelectedValue = "S";
            LoadSubItems();
            txtItemWdQty.Text = string.Empty;
            txtItemWeQty.Text = string.Empty;
            txtItemRate.Text = string.Empty;
            txtItemTot.Text = string.Empty;
            hdnMode.Value = string.Empty;
        }

        protected void gvwOrder_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }

        protected void gvwOrder_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Modify")
            {
                EditItem(Convert.ToInt32(e.CommandArgument));
            }
            else if (e.CommandName == "Remove")
            {
                RemoveItem(Convert.ToInt32(e.CommandArgument));
            }
        }

        protected void gvwOrder_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                GeneralFunctions.ApplyGridViewAlternateItemStyle(e.Row, 10);
                ((Label)e.Row.FindControl("lblSlNo")).Text = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Id"));
                ((HiddenField)e.Row.FindControl("hdnOrderTranId")).Value = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "pk_OrderitemID"));
                //((HiddenField)e.Row.FindControl("hdnTranId")).Value = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "fk_TranID"));
                //((HiddenField)e.Row.FindControl("hdnTranType")).Value = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Order"));
                ((HiddenField)e.Row.FindControl("hdnItemId")).Value = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "fk_ItemID"));
                ((HiddenField)e.Row.FindControl("hdnType")).Value = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "ItemType"));

                ((Label)e.Row.FindControl("lblType")).Text = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "ItemTypeDesc"));
                ((Label)e.Row.FindControl("lblItem")).Text = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Item"));
                ((Label)e.Row.FindControl("lblWdQty")).Text = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "QtyWe"));
                ((Label)e.Row.FindControl("lblweQty")).Text = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "QtyWd"));
                ((Label)e.Row.FindControl("lblRate")).Text = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Rate"));
                ((Label)e.Row.FindControl("lblAmt")).Text = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Total"));

                // Edit link
                ImageButton btnEdit = (ImageButton)e.Row.FindControl("btnEdit");
                btnEdit.ToolTip = ResourceManager.GetStringWithoutName("R00035");

                //Delete link
                ImageButton btnRemove = (ImageButton)e.Row.FindControl("btnRemove");
                btnRemove.ToolTip = ResourceManager.GetStringWithoutName("R00034");

                btnEdit.CommandArgument = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Id"));
                btnRemove.CommandArgument = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Id"));
            }
        }

        protected void ddlSubItemType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlSubItem.Items.Clear();
            LoadSubItems();
        }

        protected void ddlSubItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadSubItemDetails();
        }

        #endregion

        #region Private Methods

        private void LoadLocations()
        {
            List<ILocation> locations = TransactionBLL.GetLocations();
            ddlLoc.DataValueField = "LocID";
            ddlLoc.DataTextField = "LocName";
            ddlLoc.DataSource = locations;
            ddlLoc.DataBind();
            ddlLoc.Items.Insert(0, new ListItem("Select", "0"));
            //ddlLoc.Items.Insert(0, new ListItem("Select", "0"));
        }

       
        private void LoadSubItems()
        {
            List<IRecipe> items = RecipeBLL.GetItems(ddlSubItemType.SelectedValue);
            ddlSubItem.DataValueField = "Id";
            ddlSubItem.DataTextField = "Name";
            ddlSubItem.DataSource = items;
            ddlSubItem.DataBind();
            ddlSubItem.Items.Insert(0, new ListItem("All", "0"));
        }


        private void LoadSubItemDetails()
        {
            if (ddlSubItem.SelectedValue != "0")
            {
                IRecipe item = RecipeBLL.GetItem(ddlSubItemType.SelectedValue, Convert.ToInt32(ddlSubItem.SelectedValue));

                if (!ReferenceEquals(item, null))
                {
                    txtItemWeQty.Text = item.TotalWeQty.ToString();
                    txtItemWdQty.Text = Convert.ToString(item.Quantity);
                    txtItemRate.Text = Convert.ToString(item.Rate);
                    //txtItemTax.Text = "0";
                    txtItemTot.Text = Convert.ToString(item.Amount);
                }
            }
            else
            {
                txtItemWeQty.Text = string.Empty;
                txtItemWdQty.Text = string.Empty;
                txtItemRate.Text = string.Empty;
                txtItemTot.Text = string.Empty;
            }
        }

        private void RetriveParameters()
        {
            _userId = UserBLL.GetLoggedInUserId();

            //Get user permission.
            UserBLL.GetUserPermission(out _canAdd, out _canEdit, out _canDelete, out _canView);

            if (!ReferenceEquals(Request.QueryString["id"], null))
            {
                Int32.TryParse(GeneralFunctions.DecryptQueryString(Request.QueryString["id"].ToString()), out _rId);
                CalledFrom = Request.QueryString["frmod"].ToString();
            }
        }

        private void SetAttributes()
        {
            //spnNarration.Style["display"] = "none";

            if (!IsPostBack)
            {
                //if (_rId == -1) //Add mode
                //{
                //    if (!_canAdd) btnSave.Visible = false;
                //}
                //else
                //{
                //    if (!_canEdit) btnSave.Visible = false;
                //}

                btnSave.ToolTip = ResourceManager.GetStringWithoutName("R00060");
                btnBack.ToolTip = ResourceManager.GetStringWithoutName("R00061");
                btnBack.OnClientClick = "javascript:return RedirectAfterCancelClick('ListLocation.aspx','" + ResourceManager.GetStringWithoutName("R00025") + "')";
            }
        }

        private void CheckUserAccess()
        {
            if (!ReferenceEquals(Session[Constants.SESSION_USER_INFO], null))
            {
                IUser user = (IUser)Session[Constants.SESSION_USER_INFO];

                if (ReferenceEquals(user, null) || user.Id == 0)
                {
                    Response.Redirect("~/Login.aspx");
                }

                if (user.RoleID != (int)UserRole.Admin)
                {
                    Response.Redirect("~/Unauthorized.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Login.aspx");
            }

            if (_rId == 0)
                Response.Redirect("~/Transaction/TranList.aspx");

            if (!_canView)
            {
                Response.Redirect("~/Unauthorized.aspx");
            }
        }

        private DataSet GetGridData()
        {
            DataSet ds = CreateDataSetSchema(false);
            for (int j = 0; j < gvwOrder.Rows.Count; j++)
            {
                DataRow dr;
                GridViewRow row = gvwOrder.Rows[j];
                dr = ds.Tables[0].NewRow();

                dr["pk_OrderItemID"] = Convert.ToInt32(((HiddenField)row.FindControl("hdnOrderTranId")).Value);
                //dr["fk_TranID"] = Convert.ToInt64(((HiddenField)row.FindControl("hdnTranId")).Value);
                //dr["TranType"] = Convert.ToString(((HiddenField)row.FindControl("hdnTranType")).Value);
                dr["fk_ItemID"] = Convert.ToInt32(((HiddenField)row.FindControl("hdnItemId")).Value);
                dr["ItemType"] = ((HiddenField)row.FindControl("hdnType")).Value;
                dr["QtyWd"] = ((Label)row.FindControl("lblWdQty")).Text;
                dr["QtyWe"] = Convert.ToDecimal(((Label)row.FindControl("lblWeQty")).Text);
                //dr["Taxes"] = Convert.ToDecimal(((Label)row.FindControl("lblTax")).Text);
                dr["Rate"] = Convert.ToDecimal(((Label)row.FindControl("lblRate")).Text);
                //dr["Taxes"] = Convert.ToDecimal(((Label)row.FindControl("lblRate")).Text);
                ds.Tables[0].Rows.Add(dr);
            }

            return ds;
        }

        private void AddSubItem()
        {
            DataSet ds = CreateDataSetSchema(true);
            int id = 0;
            int.TryParse(hdnMode.Value, out id);

            int index = 0;
            for (int j = 0; j < gvwOrder.Rows.Count; j++)
            {
                index++;
                DataRow dr;
                GridViewRow row = gvwOrder.Rows[j];
                dr = ds.Tables[0].NewRow();

                if (index == id)
                {
                    var tranItemId = (!string.IsNullOrEmpty(((HiddenField)row.FindControl("hdnOrderTranId")).Value)) ? Convert.ToInt64(((HiddenField)row.FindControl("hdnItemTranId")).Value) : 0;
                    //var tranId = (!string.IsNullOrEmpty(((HiddenField)row.FindControl("hdnTranId")).Value)) ? Convert.ToInt64(((HiddenField)row.FindControl("hdnTranId")).Value) : 0;
                    var tranType = Convert.ToString(((HiddenField)row.FindControl("hdnType")).Value);

                    FillExistingRow(dr, index, tranItemId, 0, tranType);
                }

                else
                    LoadGridRow(row, dr, index);

                ds.Tables[0].Rows.Add(dr);
            }

            //Add new row

            if (id == 0)
                FillNewRow(ds, index);


            BindGrid(ds);
        }

        private decimal CalculateTotal(decimal qty, decimal rate)
        {
            decimal total = qty * rate;
            return Math.Round(total, 2);
        }

        private void FillExistingRow(DataRow row, int index, Int64 tranItemId, Int64 tranId, string tranType)
        {
            row["Id"] = index;
            row["pk_OrderItemID"] = tranItemId;
            //row["fk_TranID"] = tranId;
            //row["OrderType"] = tranType;
            row["fk_ItemID"] = Convert.ToInt64(ddlSubItem.SelectedValue);
            row["ItemType"] = ddlSubItemType.SelectedValue;


            if (ddlSubItemType.SelectedValue == "R")
                row["ItemTypeDesc"] = "Raw material";
            else if (ddlSubItemType.SelectedValue == "F")
                row["ItemTypeDesc"] = "Finished";
            else if (ddlSubItemType.SelectedValue == "S")
                row["ItemTypeDesc"] = "Semi finished";

            row["Item"] = ddlSubItem.SelectedItem.Text;
            row["QtyWd"] = (!string.IsNullOrEmpty(txtItemWdQty.Text)) ? Convert.ToDecimal(txtItemWdQty.Text) : 0;
            row["QtyWe"] = (!string.IsNullOrEmpty(txtItemWeQty.Text)) ? Convert.ToDecimal(txtItemWeQty.Text) : 0;
            row["Rate"] = (!string.IsNullOrEmpty(txtItemRate.Text)) ? Convert.ToDecimal(txtItemRate.Text) : 0;
            //row["Taxes"] = (!string.IsNullOrEmpty(txtItemTax.Text)) ? Convert.ToDecimal(txtItemTax.Text) : 0;
            row["Total"] = CalculateTotal(Convert.ToDecimal(row["Qty"]), Convert.ToDecimal(row["Rate"]));
        }

        private void FillNewRow(DataSet ds, int index)
        {
            DataRow row = ds.Tables[0].NewRow();
            row["Id"] = index + 1;
            row["pk_OrderItemID"] = 0;
            //row["fk_TranID"] = 0;
            row["ItemType"] = ddlSubItemType.SelectedValue;

            if (ddlSubItemType.SelectedValue == "R")
                row["ItemTypeDesc"] = "Raw material";
            else if (ddlSubItemType.SelectedValue == "F")
                row["ItemTypeDesc"] = "Finished";
            else if (ddlSubItemType.SelectedValue == "S")
                row["ItemTypeDesc"] = "Semi finished";

            row["fk_ItemID"] = Convert.ToInt32(ddlSubItem.SelectedValue);
            row["Item"] = ddlSubItem.SelectedItem.Text;
            row["QtyWd"] = (!string.IsNullOrEmpty(txtItemWdQty.Text)) ? Convert.ToDecimal(txtItemWdQty.Text) : 0;
            row["QtyWe"] = (!string.IsNullOrEmpty(txtItemWeQty.Text)) ? Convert.ToDecimal(txtItemWeQty.Text) : 0;
            row["Rate"] = (!string.IsNullOrEmpty(txtItemRate.Text)) ? Convert.ToDecimal(txtItemRate.Text) : 0;
            //row["Taxes"] = (!string.IsNullOrEmpty(txtItemTax.Text)) ? Convert.ToDecimal(txtItemTax.Text) : 0;
            row["Total"] = CalculateTotal(Convert.ToDecimal(row["QtyWd"]), Convert.ToDecimal(row["Rate"]));

            ds.Tables[0].Rows.Add(row);
        }

        private void LoadData()
        {
            if (_rId > 0)
            {
                ddlOrdType.Enabled = false;
            }

            IOrder transaction = OrderBLL.GetOrder(_rId);


            if (!ReferenceEquals(transaction, null))
            {

                ddlOrdType.SelectedValue = Convert.ToString(transaction.OrderType);
                ddlLoc.SelectedValue = Convert.ToString(transaction.LocID);
                txtOrderRef.Text = transaction.OrderRef;
                txtOrderDate.Text = transaction.OrderDate.ToString("dd/MM/yyyy");


                List<IOrderItem> items = OrderBLL.GetAllTransactionItems(transaction.Id);
                if (items.Count > 0)
                {
                    LoadOrderItems(items);
                }
            }
        }

        //private void LoadRefTransaction()
        //{
        //    List<ITransactionItem> items = TransactionBLL.GetAllTransactionItems(txtOriginalTranRef.Text);
        //    if (items.Count > 0)
        //    {
        //        LoadTransactionItems(items);
        //        upItem.Update();
        //    }

        //}
        private void LoadOrderItems(List<IOrderItem> items)
        {
            DataSet ds = CreateDataSetSchema(true);
            int index = 1;
            foreach (var item in items)
            {
                DataRow row = ds.Tables[0].NewRow();

                row["Id"] = index;

                row["pk_OrderItemID"] = item.Id;
                //row["fk_TranID"] = item.TransactionId;
                //row["TranType"] = item.ItemType;
                row["fk_ItemID"] = item.ItemId;
                if (item.QtyWd.HasValue)
                    row["QtyWd"] = item.QtyWd.Value;
                else
                    row["QtyWd"] = 0;

                if (item.QtyWe.HasValue)
                    row["QtyWe"] = item.QtyWe.Value;
                else
                    row["QtyWe"] = 0;

                if (item.Rate.HasValue)
                    row["Rate"] = item.Rate.Value;
                else
                    row["Rate"] = 0;

                //if (item.Tax.HasValue)
                //    row["Taxes"] = item.Tax.Value;
                //else
                //    row["Taxes"] = 0;

                row["ItemType"] = item.ItemType;

                //For display purpose
                //row["UOM"] = item.UOM;
                row["ItemTypeDesc"] = item.ItemTypeDesc;
                row["Item"] = item.ItemName;
                row["Total"] = CalculateTotal(Convert.ToDecimal(row["QtyWd"]), Convert.ToDecimal(row["Rate"]));

                ds.Tables[0].Rows.Add(row);


                index++;
            }

            BindGrid(ds);
        }

        private bool ValidateControls(IOrder tran)
        {
            bool isValid = true;
            //spnVendor.Style["display"] = "none";

            //if (ddlType.SelectedValue == "R" || ddlType.SelectedValue == "X")
            //{
            //    if (ddlVendor.SelectedValue == "0")
            //    {
            //        isValid = false;
            //        spnVendor.Style["display"] = "block";
            //        spnVendor.InnerText = "Please select vendor";
            //    }
            //}

            return isValid;
        }

        private void SaveTransaction()
        {
            IOrder tran = new OrderEntity();
            DataSet ds = GetGridData();
            BuildEntity(tran);
            string Mode = CheckForStandardOrderForLoc(_rId);
            if (ValidateControls(tran))
            {
                int result = OrderBLL.SaveOrders(tran, Mode, ds);
                Response.Redirect("~/Transaction/TranList.aspx");
            }
        }
        private string CheckForStandardOrderForLoc(int id)
        {
            string a;
            a = OrderBLL.GetStandardOrderForLoc(id);
            return a;
        }
        private void BuildEntity(IOrder transaction)
        {
            transaction.Id = _rId;
            transaction.OrderType = ddlOrdType.SelectedValue;
            transaction.LocID = Convert.ToInt32(ddlLoc.SelectedValue);
            transaction.OrderDate = Convert.ToDateTime(txtOrderDate.Text);
            transaction.OrderRef = txtOrderRef.Text;
            //transaction.Narration
        }

        private DataSet CreateDataSetSchema(bool allColumns)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            if (allColumns)
            {
                dt.Columns.Add(new DataColumn("Id", System.Type.GetType("System.Int32")));
                dt.Columns.Add(new DataColumn("OrderType", System.Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("Narration", System.Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("OrderNo", System.Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("OrderDate", System.Type.GetType("System.DateTime")));
                dt.Columns.Add(new DataColumn("LocID", System.Type.GetType("System.Int32")));
            }
            dt.Columns.Add(new DataColumn("pk_OrderItemID", System.Type.GetType("System.Int32")));
            dt.Columns.Add(new DataColumn("fk_OrderID", System.Type.GetType("System.Int32")));
            dt.Columns.Add(new DataColumn("fk_ItemID", System.Type.GetType("System.Int32")));
            dt.Columns.Add(new DataColumn("QtyWd", System.Type.GetType("System.Decimal")));
            dt.Columns.Add(new DataColumn("QtyWe", System.Type.GetType("System.Decimal")));
            dt.Columns.Add(new DataColumn("Rate", System.Type.GetType("System.Decimal")));
            dt.Columns.Add(new DataColumn("ItemType", System.Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("ItemTypeDesc", System.Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("Item", System.Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("Total", System.Type.GetType("System.Decimal")));
            ds.Tables.Add(dt);
            return ds;
        }

        private void BindGrid(DataSet ds)
        {
            CalaculateTotalAmount(ds);
            gvwOrder.DataSource = ds;
            gvwOrder.DataBind();
        }

        private void EditItem(int id)
        {
            hdnMode.Value = id.ToString();
            ddlSubItem.Items.Clear();
            txtOrderRef.Text = string.Empty;
            txtOrderDate.Text = string.Empty;
            txtItemRate.Text = string.Empty;
            txtItemTot.Text = string.Empty;

            for (int j = 0; j < gvwOrder.Rows.Count; j++)
            {
                GridViewRow row = gvwOrder.Rows[j];
                int slNo = Convert.ToInt32(((Label)row.FindControl("lblSlNo")).Text);

                if (slNo == id)
                {
                    //dr["pk_RecipeItemID"] = ((HiddenField)row.FindControl("hdnItemTranId")).Value;
                    //dr["fk_RecipeID"] = ((HiddenField)row.FindControl("hdnTranId")).Value;
                    //dr["fk_ItemID"] = ((HiddenField)row.FindControl("hdnItemId")).Value;
                    ddlSubItemType.SelectedValue = ((HiddenField)row.FindControl("hdnType")).Value;
                    LoadSubItems();
                    ddlSubItem.SelectedValue = ((HiddenField)row.FindControl("hdnItemId")).Value;
                    txtItemWdQty.Text = ((Label)row.FindControl("lblWdQty")).Text;
                    txtItemWeQty.Text = ((Label)row.FindControl("lblWeQty")).Text;
                    txtItemRate.Text = ((Label)row.FindControl("lblRate")).Text;
                    txtItemTot.Text = ((Label)row.FindControl("lblAmt")).Text;
                }
            }
        }

        private void RemoveItem(int id)
        {
            DataSet ds = CreateDataSetSchema(true);
            int index = 0;
            for (int j = 0; j < gvwOrder.Rows.Count; j++)
            {
                DataRow dr;
                GridViewRow row = gvwOrder.Rows[j];
                dr = ds.Tables[0].NewRow();

                int slNo = Convert.ToInt32(((Label)row.FindControl("lblSlNo")).Text);

                if (slNo != id)
                {
                    index++;
                    LoadGridRow(row, dr, index);
                    ds.Tables[0].Rows.Add(dr);
                }
            }

            BindGrid(ds);
        }
        private void LoadGridRow(GridViewRow row, DataRow dr, int id)
        {
            dr["Id"] = id;
            dr["pk_ItemTranID"] = Convert.ToInt64(((HiddenField)row.FindControl("hdnItemTranId")).Value);
            //dr["fk_TranID"] = Convert.ToInt64(((HiddenField)row.FindControl("hdnTranId")).Value);
            dr["fk_ItemID"] = Convert.ToInt32(((HiddenField)row.FindControl("hdnItemId")).Value);
            dr["ItemType"] = ((HiddenField)row.FindControl("hdnType")).Value;
            dr["ItemTypeDesc"] = ((Label)row.FindControl("lblType")).Text;
            dr["Item"] = ((Label)row.FindControl("lblItem")).Text;
            //dr["UOM"] = ((Label)row.FindControl("lblUnit")).Text;
            dr["QtyWd"] = Convert.ToDecimal(((Label)row.FindControl("lblQty")).Text);
            dr["Rate"] = Convert.ToDecimal(((Label)row.FindControl("lblRate")).Text);
            dr["QtyWe"] = Convert.ToDecimal(((Label)row.FindControl("lblTax")).Text);
            dr["Total"] = CalculateTotal(Convert.ToDecimal(dr["Qty"]), Convert.ToDecimal(dr["Rate"]));
        }

        private void CalaculateTotalAmount(DataSet ds)
        {
            if (ds.Tables.Count > 0)
            {
                decimal total = 0;

                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    total += Convert.ToDecimal(row["Total"]);
                }

                txtAmt.Text = Convert.ToString(Math.Round(total, 2));
                upItem.Update();
            }

        }

        #endregion

        private void DisplayItemTotal()
        {
            decimal qtywd = (!string.IsNullOrEmpty(txtItemWdQty.Text)) ? Convert.ToDecimal(txtItemWdQty.Text) : 0;
            decimal qtywe = (!string.IsNullOrEmpty(txtItemWeQty.Text)) ? Convert.ToDecimal(txtItemWeQty.Text) : 0;
            decimal rate = (!string.IsNullOrEmpty(txtItemRate.Text)) ? Convert.ToDecimal(txtItemRate.Text) : 0;
            txtItemTot.Text = Convert.ToString(CalculateTotal(qtywd, rate));
            upItem.Update();
        }

        protected void txtItemRate_TextChanged(object sender, EventArgs e)
        {
            txtItemTot.Text = (txtItemRate.Text.ToDecimal() * txtItemWdQty.Text.ToDecimal()).ToString();
        }

        
    }
}