﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditVendor.aspx.cs" Inherits="FoodSmart.WebApp.MasterModule.EditVendor" MasterPageFile="~/Site.Master" Title=":: FoodSmart :: Add / Edit Vendor" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">    
    <script type="text/javascript">
        LoadScript();
    </script>
    <style type="text/css">
        .style1
        {
            width: 200px;
            height: 1px;
        }
        .style2
        {
            height: 1px;
        }
        .style3
        {
            height: 46px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="container" runat="Server">
    <div id="headercaption">ADD / EDIT VENDOR</div>
    <div style="margin:0px auto;width:550px;">
        <fieldset style="width:100%;">
            <legend>Edit Vendor</legend>
            <table border="0" cellpadding="1" cellspacing="1" width="100%">
                <tr>
                    <td class="style1">Vendor Name:<span class="errormessage1">*</span></td>
                    <td class="style2">
                        <asp:TextBox ID="txtVendorName" runat="server" MaxLength="10" Width="350" 
                            Enabled="true"></asp:TextBox><br />
                        <span id="spnName" runat="server" class="errormessage" style="display:none;"></span>
                        <asp:RequiredFieldValidator ID="rfvVendorName" runat="server" CssClass="errormessage" ControlToValidate="txtVendorName" Display="Dynamic" ValidationGroup="Save"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="style3">Address Line:<span class="errormessage1">*</span></td>
                    <td class="style3">
                        <asp:TextBox ID="txtAddress" runat="server" CssClass="textboxuppercase" MaxLength="3000" Width="350" TextMode="MultiLine" Enabled="true" Height="100">
                        </asp:TextBox><br />
                        <span id="spnAddr1" runat="server" class="errormessage" style="display:none;"></span>
                        <asp:RequiredFieldValidator ID="rfvAddress" runat="server" CssClass="errormessage" ControlToValidate="txtAddress" Display="Dynamic" ValidationGroup="Save"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="style3">Phone:<span class="errormessage1">*</span></td>
                    <td class="style3">
                        <asp:TextBox ID="txtPhone" runat="server" CssClass="textboxuppercase" MaxLength="3000" Width="350">
                        </asp:TextBox><br />
                        <span id="Span1" runat="server" class="errormessage" style="display:none;"></span>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" CssClass="errormessage" ControlToValidate="txtAddress" Display="Dynamic" ValidationGroup="Save"></asp:RequiredFieldValidator>
--%>                    </td>
                </tr>

                <tr>
                    <td class="style3">PAN:<span class="errormessage1">*</span></td>
                    <td class="style3">
                        <asp:TextBox ID="txtPAN" runat="server" CssClass="textboxuppercase" MaxLength="3000" Width="350">
                        </asp:TextBox><br />
                        <span id="Span2" runat="server" class="errormessage" style="display:none;"></span>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" CssClass="errormessage" ControlToValidate="txtAddress" Display="Dynamic" ValidationGroup="Save"></asp:RequiredFieldValidator>
--%>                    </td>
                </tr>
                <tr>
                    <td>GST No:</td>
                    <td>
                        <asp:TextBox ID="txtGSTNo" runat="server" CssClass="textboxuppercase" MaxLength="50" Width="250"></asp:TextBox><br />
                        <span id="spnVATNo" runat="server" class="errormessage" style="display:none;"></span>
                        
                    </td>
                </tr>
                <%--<tr>
                    <td>State Code:<span class="errormessage1">*</span></td>
                    <td style="width: 20%;">
                        <asp:DropDownList ID="ddlState" runat="server" AutoPostBack="true" CssClass="chzn-select medium-select single">
                            <asp:ListItem Text="Select" Value="0" />
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvState" runat="server" CssClass="errormessage" ControlToValidate="ddlState" Display="Dynamic" ValidationGroup="Save"></asp:RequiredFieldValidator>
                    </td>
                </tr>--%>
                
                <tr>
                    <td colspan="2">
                        <asp:Button ID="btnSave" runat="server" Text="Save" ValidationGroup="Save" OnClick="btnSave_Click" />&nbsp;&nbsp;<asp:Button 
                            ID="btnBack" runat="server" Text="Back" onclick="btnBack_Click"/>&nbsp;&nbsp;<asp:Label ID="msgbox" runat="server" Text="" ForeColor="Red" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </div>
</asp:Content>