﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FoodSmart.BLL;
using FoodSmart.Common;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using FoodSmart.Utilities.ResourceManager;
using System.Data;

namespace FoodSmart.WebApp.Views.Security
{
    public partial class ItemEdit : System.Web.UI.Page
    {
        #region Private Member Variables

        private int _userId = 0;
        private int _uId = 0;
        private bool _canAdd = false;
        private bool _canEdit = false;
        private bool _canDelete = false;
        private bool _canView = false;

        #endregion

        #region Protected Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            RetriveParameters();
            CheckUserAccess();
            SetAttributes();
            

            if (!IsPostBack)
            {
                LoadItemGroup();
                txtEffectiveDate.Text = DateTime.Now.AddDays(1).ToString("dd/MM/yyyy");
                LoadData();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            SaveRecord();
        }

        #endregion

        #region Private Methods

        private void RetriveParameters()
        {
            _userId = UserBLL.GetLoggedInUserId();

            //Get user permission.
            UserBLL.GetUserPermission(out _canAdd, out _canEdit, out _canDelete, out _canView);

            if (!ReferenceEquals(Request.QueryString["id"], null))
            {
                Int32.TryParse(GeneralFunctions.DecryptQueryString(Request.QueryString["id"].ToString()), out _uId);
            }
        }

        private void SetAttributes()
        {
            //spnName.Style["display"] = "none";

            if (!IsPostBack)
            {
                txtHSN.MaxLength = 8;
                if (_uId == -1) //Add mode
                {
                    if (!_canAdd) btnSave.Visible = false;
                }
                else
                {
                    if (!_canEdit) btnSave.Visible = false;
                }

                btnSave.ToolTip = ResourceManager.GetStringWithoutName("R00060");
                btnBack.ToolTip = ResourceManager.GetStringWithoutName("R00061");
                btnBack.OnClientClick = "javascript:return RedirectAfterCancelClick('ItemList.aspx','" + ResourceManager.GetStringWithoutName("R00025") + "')";
                //spnName.InnerText = ResourceManager.GetStringWithoutName("R00046");
            }

        }

        private void CheckUserAccess()
        {
            if (!ReferenceEquals(Session[Constants.SESSION_USER_INFO], null))
            {
                IUser user = (IUser)Session[Constants.SESSION_USER_INFO];

                if (ReferenceEquals(user, null) || user.Id == 0)
                {
                    Response.Redirect("~/Login.aspx");
                }

                if (user.RoleID != (int)UserRole.Admin)
                {
                    Response.Redirect("~/Unauthorized.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Login.aspx");
            }

            if (_uId == 0)
                Response.Redirect("~/Views/ManageUser.aspx");

            if (!_canView)
            {
                Response.Redirect("~/Unauthorized.aspx");
            }
        }

       

        private void LoadData()
        {
            ItemBLL oItembll = new ItemBLL();

            IItems oitem = (ItemsEntity)oItembll.GetItemForEdit(_uId);

            if (!ReferenceEquals(oitem, null))
            {
                txtName.Text = oitem.ItemDescription;
                //txtId.Text = oitem.ItemID.ToString();
                ddlGroup.SelectedValue = oitem.ItemGroupID.ToString();
                txtUOM.Text = oitem.UOM;
                ddlRorF.SelectedValue = oitem.RorF;
                ddlGroup.SelectedValue = oitem.ItemGroupID.ToString();
                txtCGSTPer.Text = oitem.CGSTPer.ToString();
                txtSGSTPer.Text = oitem.SGSTPer.ToString();
                txtHSN.Text = oitem.HSNCode.ToString();
                ddlDisc.SelectedIndex = Convert.ToInt32(oitem.DiscountAllowed);
                txtEffectiveDate.Text = oitem.EffectiveDate.ToString("dd/MM/yyyy");
                txtiSellingRate.Text = oitem.iSellingRate.ToString();
                txtSellingRate.Text = oitem.SellingRate.ToString("######0.00");
                if (!string.IsNullOrEmpty(oitem.ProcessedAt))
                    ddlProcessed.SelectedValue = oitem.ProcessedAt.ToString();
            }
        }

        private bool ValidateControls(IItems item, string mode)
        {
            bool isValid = true;
            if (item.EffectiveDate <= DateTime.Now && mode == "A")
            {
                isValid = false;
            }
            return isValid;
        }

        private void SaveRecord()
        {
            IItems item = new ItemsEntity();
            BuildUserEntity(item);
            string strMode = "A";
            if (item.ItemID > 0)
            {
                strMode = "E";
            }
            if (ValidateControls(item, strMode))
            {
    
                ItemBLL itemBLL = new ItemBLL();
                int result = itemBLL.SaveItem(item, strMode);

                if (strMode == "E")
                {
                    //SendEmail(user);
                    Response.Redirect("~/MasterModule/ItemList.aspx");
                }
                else
                {
                    txtName.Text = string.Empty;
                    txtUOM.Text = string.Empty;
                    txtCGSTPer.Text = string.Empty;
                    txtSGSTPer.Text = string.Empty;
                    txtHSN.Text = string.Empty;
                    ddlDisc.SelectedIndex = 0;
                    txtEffectiveDate.Text = DateTime.Now.AddDays(1).ToString("dd/MM/yyyy");
                    txtiSellingRate.Text = string.Empty;
                    txtSellingRate.Text = string.Empty;
                    item.ItemID = -1;
                    //ddlProcessed.Enabled = true;
                    ddlProcessed.SelectedIndex = -1;
                }
            }
        }

        private void BuildUserEntity(IItems item)
        {
            item.ItemID = _uId;
            item.ItemDescription = txtName.Text.ToUpper();
            item.EffectiveDate = Convert.ToDateTime(txtEffectiveDate.Text);
            item.DiscountAllowed = Convert.ToBoolean(ddlDisc.SelectedIndex);
            item.UOM = txtUOM.Text.ToUpper();
            if (ddlRorF.SelectedValue == "F")
                item.ProcessedAt = ddlProcessed.SelectedValue.ToString();
            else
                item.ProcessedAt = string.Empty;
            item.CGSTPer = Convert.ToDecimal(txtCGSTPer.Text == string.Empty ? "0.00" : txtCGSTPer.Text);
            item.SGSTPer = Convert.ToDecimal(txtSGSTPer.Text == string.Empty ? "0.00" : txtSGSTPer.Text);
            item.HSNCode = txtHSN.Text;
            item.ItemGroupID = Convert.ToInt32(ddlGroup.SelectedValue);
            if (item.ItemID <= 0 )
                item.SellingRate = Convert.ToDecimal(txtiSellingRate.Text);
            item.CreatedBy = _userId;
            item.ItemID = _uId;
        }

        private void LoadItemGroup()
        {
            //DataRow row = new DataRow();
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            //FoodSmart.BLL.itemService.SearchCriteria searchcriteria = new FoodSmart.BLL.itemService.SearchCriteria();

            SearchCriteria searchcriteria = new SearchCriteria();
            searchcriteria.QueryStat = "L";
            searchcriteria.RorF = ddlRorF.SelectedValue;
            ddlGroup.DataSource = null;

            ddlGroup.Items.Insert(0, "Select Category");
            ds = new ItemBLL().GetAllItemGroup(searchcriteria);
            dt = ds.Tables[0];

            DataRow dr = dt.NewRow();
            dr["Descr"] = "--Select--";
            dr["pk_ItemGroupID"] = 0;

            dt.Rows.InsertAt(dr, 0);

            if (dt.Rows.Count > 0)
            {
                ddlGroup.DataSource = dt;
                ddlGroup.DataTextField = "descr";
                ddlGroup.DataValueField = "pk_ItemGroupID";
                ddlGroup.DataBind();
                ddlGroup.SelectedValue = "0";
            }

        }
        #endregion

        protected void txtVATPer_TextChanged(object sender, EventArgs e)
        {

        }

        protected void ddlRorF_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadItemGroup();
            if (ddlRorF.SelectedValue == "F")
            {
                trProcessedAt.Visible = true;
                ddlProcessed.Enabled = true;
                ddlProcessed.SelectedIndex = 0;
            }
            else
            {
                trProcessedAt.Visible = false;
                ddlProcessed.Enabled = false;
                ddlProcessed.SelectedIndex = -1;
            }
        }
    }
}