﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditOrder.aspx.cs" Inherits="FoodSmart.WebApp.MasterModule.EditOrder" MasterPageFile="~/Site.Master" Title=":: POS :: Add / Edit User" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FoodSmart.WebApp" Namespace="FoodSmart.WebApp.CustomControls" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        LoadScript();
        function toggleAddPanel(param) {
            var tdAddItem = document.getElementById('tdAddItem');

            if (param)
                tdAddItem.style.display = 'block';
            else
                tdAddItem.style.display = 'none';
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="container" runat="Server">
    <div id="dvAsync" style="padding: 5px; display: none;">
        <div class="asynpanel">
            <div id="dvAsyncClose">
                <img alt="" src="../../Images/Close-Button.bmp" style="cursor: pointer;" onclick="ClearErrorState()" />
            </div>
            <div id="dvAsyncMessage">
            </div>
        </div>
    </div>
    <asp:UpdateProgress ID="uProgressOrder" runat="server" AssociatedUpdatePanelID="upOrder">
        <ProgressTemplate>
            <div class="progress">
                <div id="image">
                    <img src="../../Images/PleaseWait.gif" alt="" />
                </div>
                <div id="text">
                    Please Wait...
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdateProgress ID="uProgressItem" runat="server" AssociatedUpdatePanelID="upItem">
        <ProgressTemplate>
            <div class="progress">
                <div id="image">
                    <img src="../../Images/PleaseWait.gif" alt="" />
                </div>
                <div id="text">
                    Please Wait...
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div id="headercaption">ADD / EDIT STANDARD ORDER</div>
    <div style="width: 900px; margin: 0 auto; margin-top: 30px;">
        <fieldset style="width: 100%;">
            <legend>Add / Edit Recipe</legend>
            <asp:UpdatePanel ID="upOrder" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <table border="0" cellpadding="3" cellspacing="3" width="100%">
                        <tr>
                            <td style="width: 15%;">Order type:<span class="errormessage1">*</span></td>
                            <td style="width: 30%;">
                                <asp:DropDownList ID="ddlOrdType" runat="server" Width="175px" AutoPostBack="true" CssClass="chzn-select medium-select single">
                                    <asp:ListItem Text="Standard" Value="R" />
                                    <asp:ListItem Text="Special" Value="S" />
                                </asp:DropDownList>
                            </td>
                            <td>Order location:<span class="errormessage1">*</span></td>
                            <td>
                                <asp:DropDownList ID="ddlLoc" runat="server" Width="175px" AutoPostBack="false" CssClass="chzn-select medium-select single"></asp:DropDownList>
                                <br />
                                <span id="spnToLoc" runat="server" class="errormessage" style="display: none;"></span>
                            </td>
                        </tr>
               
                        <tr>
                            <td>Order Refererence:</td>
                            <td style="width: 30%;">
                                 <asp:TextBox ID="txtOrderRef" runat="server" CssClass="textboxuppercase" TabIndex="5" MaxLength="30" Width="175px" Height="20px" Style="text-align: right"></asp:TextBox><br />
                                 <span id="Span1" runat="server" class="errormessage" style="display: none;"></span>
                            </td>
                            <td style="width: 15%;">Order Created ON:<span class="errormessage1">*</span></td>
                            <td style="width: 30%;">
                                 <asp:TextBox ID="txtOrderDate" runat="server" CssClass="textboxuppercase" TabIndex="5" MaxLength="30" Width="175px" Height="20px" Style="text-align: right"></asp:TextBox><br />
                                        <cc1:CalendarExtender ID="cdOrderDate" TargetControlID="txtOrderDate" runat="server" Format="dd/MM/yyyy" Enabled="True" />
                                        <asp:RequiredFieldValidator ID="rfvOrderDate" runat="server" ControlToValidate="txtOrderDate" ErrorMessage="This field is required*" CssClass="errormessage" ValidationGroup="Save" Display="Dynamic"></asp:RequiredFieldValidator>
                                        <span id="spnOrderDate" runat="server" class="errormessage" style="display: none;"></span>

                            </td>
                        </tr>
                        <tr>
                            <td>Total amount:</td>
                            <td>
                                <cc2:CustomTextBox ID="txtAmt" runat="server" CssClass="numerictextbox" TabIndex="3" Width="175px" Height="20px"
                                    Type="Numeric" MaxLength="10" Precision="10" Scale="2">
                                </cc2:CustomTextBox><br />
                                <span id="spnAmt" runat="server" class="errormessage" style="display: none;"></span>
                            </td>
                            <td>Rate / Unit:</td>
                            <td>
                                <cc2:CustomTextBox ID="txtRPU" runat="server" CssClass="numerictextbox" TabIndex="3" Width="175px" Height="20px"
                                    Type="Numeric" MaxLength="10" Precision="10" Scale="2">
                                </cc2:CustomTextBox><br />
                                <span id="spnRPU" runat="server" class="errormessage" style="display: none;"></span>
                            </td>
                    
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel ID="upItem" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div style="width: 100%;padding-top:20px;">
                        <table border="0" cellpadding="3" cellspacing="3" width="100%">
                            <tr>
                                <td style="width: 5%;padding:10px 0px 10px 0px;" class="gridviewheader">Sl#</td>
                                <td style="width: 15%;padding:10px 0px 10px 0px;" class="gridviewheader">Item type</td>
                                <td style="width: 25%;padding:10px 0px 10px 0px;" class="gridviewheader">Item</td>
                                <td style="width: 10%;padding:10px 0px 10px 0px;" class="gridviewheader_right">Week Day Order</td>
                                <td style="width: 10%;padding:10px 0px 10px 0px;" class="gridviewheader_right">Week End Order</td>
                                <td style="width: 10%;padding:10px 0px 10px 0px;" class="gridviewheader_right">Rate</td>
                                <td style="width: 10%;padding:10px 0px 10px 0px;" class="gridviewheader_right">
                                    Weekday Amount</td>
                                <td style="width: 10%;padding:10px 0px 10px 0px;" class="gridviewheader"></td>
                            </tr>
                            <tr>
                                <td style="width: 5%;">&nbsp;</td>
                                <td style="width: 15%;">
                                    <asp:HiddenField ID="hdnMode" runat="server" />
                                    <asp:DropDownList ID="ddlSubItemType" runat="server" AutoPostBack="true" CssClass="chzn-select medium-select single" OnSelectedIndexChanged="ddlSubItemType_SelectedIndexChanged">
                                        <asp:ListItem Text="Raw material" Value="R" />
                                        <asp:ListItem Text="Finished" Value="F" />
                                        <asp:ListItem Text="Semi finished" Value="S" />
                                    </asp:DropDownList>
                                </td>
                                <td style="width: 20%;">
                                    <asp:DropDownList ID="ddlSubItem" runat="server" AutoPostBack="true" CssClass="chzn-select medium-select single" OnSelectedIndexChanged="ddlSubItem_SelectedIndexChanged">
                                        <asp:ListItem Text="Select" Value="0" />
                                    </asp:DropDownList>
                                </td>
                                <td style="width: 15%;">
                                    <asp:TextBox ID="txtItemWdQty" runat="server" CssClass="numerictextbox" MaxLength="100" Width="90px"></asp:TextBox>
                                </td>
                                <td style="width: 10%;">
                                    <asp:TextBox ID="txtItemWeQty" runat="server" CssClass="numerictextbox" MaxLength="100" Width="90px"></asp:TextBox>
                                </td>
                                <td style="width: 10%;">
                                    <asp:TextBox ID="txtItemRate" runat="server" CssClass="numerictextbox" 
                                        MaxLength="100" Width="90px" ontextchanged="txtItemRate_TextChanged" AutoPostBack="true"></asp:TextBox>
                                </td>
                                <td style="width: 10%;">
                                    <asp:TextBox ID="txtItemTot" runat="server" CssClass="numerictextbox" MaxLength="100" Width="90px" Enabled="false"></asp:TextBox>
                                </td>
                                <td style="width: 10%;">
                                    <asp:Button ID="btnSaveItem" runat="server" Text="Add item" OnClick="btnSaveItem_Click" /></td>
                            </tr>
                        </table>
                    </div>
                    <div style="padding-top: 10px;">

                        <asp:GridView ID="gvwOrder" runat="server" AutoGenerateColumns="false" ShowHeader="false" AutoGenerateEditButton="false" AllowPaging="true" BorderStyle="None" BorderWidth="0" OnPageIndexChanging="gvwOrder_PageIndexChanging" OnRowDataBound="gvwOrder_RowDataBound" OnRowCommand="gvwOrder_RowCommand" Width="100%">
                            <PagerSettings Mode="NumericFirstLast" Position="Bottom" />
                            <PagerStyle CssClass="gridviewpager" />
                            <EmptyDataRowStyle CssClass="gridviewemptydatarow" />
                            <EmptyDataTemplate>No recipies Found</EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="Sl#">
                                    <HeaderStyle CssClass="gridviewheader" />
                                    <ItemStyle CssClass="gridviewitem" Width="5%" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblSlNo" runat="server"></asp:Label>
                                        <asp:HiddenField ID="hdnOrderTranId" runat="server"></asp:HiddenField>
                                        <asp:HiddenField ID="hdnType" runat="server"></asp:HiddenField>
                                        <asp:HiddenField ID="hdnItemId" runat="server"></asp:HiddenField>
                                        <asp:HiddenField ID="hdnTranType" runat="server"></asp:HiddenField>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Item type">
                                    <HeaderStyle CssClass="gridviewheader" />
                                    <ItemStyle CssClass="gridviewitem" Width="15%" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblType" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Item">
                                    <HeaderStyle CssClass="gridviewheader" />
                                    <ItemStyle CssClass="gridviewitem" Width="25%" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblItem" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Qty (Week Days)">
                                    <HeaderStyle CssClass="gridviewheader" />
                                    <ItemStyle CssClass="gridviewitem" Width="10%" HorizontalAlign="Right"/>
                                    <ItemTemplate>
                                        <asp:Label ID="lblWdQty" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Qty (Week End)">
                                    <HeaderStyle CssClass="gridviewheader" HorizontalAlign="Right" />
                                    <ItemStyle CssClass="gridviewitem" Width="10%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblWeQty" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Rate">
                                    <HeaderStyle CssClass="gridviewheader" HorizontalAlign="Right" />
                                    <ItemStyle CssClass="gridviewitem" Width="10%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblRate" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Total amount">
                                    <HeaderStyle CssClass="gridviewheader" HorizontalAlign="Right" />
                                    <ItemStyle CssClass="gridviewitem" Width="15%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblAmt" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="gridviewheader" />
                                    <ItemStyle CssClass="gridviewitem" Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnEdit" runat="server" CommandName="Modify" ImageUrl="~/Images/edit.png" Height="16" Width="16" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="gridviewheader" />
                                    <ItemStyle CssClass="gridviewitem" Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnRemove" runat="server" CommandName="Remove" ImageUrl="~/Images/remove.png" Height="16" Width="16" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

            <div style="padding-top: 10px;">
                <asp:Button ID="btnSave" runat="server" Text="Save" ValidationGroup="Save" OnClick="btnSave_Click" />&nbsp;&nbsp;<asp:Button ID="btnBack" runat="server" Text="Back" />&nbsp;&nbsp;<asp:Label ID="msgbox" runat="server" Text="" ForeColor="Red" />
            </div>
        </fieldset>
    </div>
</asp:Content>
