﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RMGroupEdit.aspx.cs" Inherits="FoodSmart.WebApp.MasterModule.RMGroupEdit" MasterPageFile="~/Site.Master" Title=":: POSWebRpt :: Add / Edit RM Item Group" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">    
    <script type="text/javascript">
        LoadScript();
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="container" runat="Server">
    <div id="headercaption">ADD / EDIT RM ITEM GROUP</div>
    <div style="margin:0px auto;width:450px;">
        <fieldset style="width:100%;">
            <legend>Add / Edit RM Item Group</legend>
            <table border="0" cellpadding="3" cellspacing="3" width="100%">
                
                <tr>
                    <td>Group Name:<span class="errormessage1">*</span></td>
                    <td>
                        <asp:TextBox ID="txtName" runat="server" CssClass="textboxuppercase" MaxLength="30" Width="250"></asp:TextBox><br />
                        <span id="spnName" runat="server" class="errormessage" style="display:none;"></span>
                        <%--<asp:RequiredFieldValidator ID="rfvFName" runat="server" CssClass="errormessage" ControlToValidate="txtFName" Display="Dynamic" ValidationGroup="Save"></asp:RequiredFieldValidator>--%>
                    </td>
                    <td style="display:none">
                        <asp:TextBox ID="txtId" runat="server" CssClass="textboxuppercase" MaxLength="30" Width="250"></asp:TextBox><br />
                        <span id="Span1" runat="server" class="errormessage" style="display:none;"></span>
                        <%--<asp:RequiredFieldValidator ID="rfvFName" runat="server" CssClass="errormessage" ControlToValidate="txtFName" Display="Dynamic" ValidationGroup="Save"></asp:RequiredFieldValidator>--%>
                    </td>
                </tr>
                
                <tr>
                    <td>Type:<span class="errormessage1">*</span></td>
                    <td>
                        <asp:DropDownList ID="ddRorF" runat="server"  
                            Width="150px" CssClass="chzn-select medium-select single">
                            <asp:ListItem Selected="True" Value="R">Raw Material</asp:ListItem>
                            <asp:ListItem Value="C">Consumables</asp:ListItem>
                        </asp:DropDownList><br />
                        <%--<asp:RequiredFieldValidator ID="rfvRole" runat="server" CssClass="errormessage" ControlToValidate="ddlRole" InitialValue="0" Display="Dynamic" ValidationGroup="Save"></asp:RequiredFieldValidator>--%>
                    </td>
                </tr>
                
                <tr>
                    <td colspan="2">
                        <asp:Button ID="btnSave" runat="server" Text="Save" ValidationGroup="Save" OnClick="btnSave_Click" />&nbsp;&nbsp;<asp:Button 
                            ID="btnBack" runat="server" Text="Back"/>&nbsp;&nbsp;<asp:Label ID="msgbox" runat="server" Text="" ForeColor="Red" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </div>
</asp:Content>
