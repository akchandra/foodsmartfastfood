﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditLocation.aspx.cs" Inherits="FoodSmart.WebApp.MasterModule.EditLocation" MasterPageFile="~/Site.Master" Title=":: FoodSmart :: Add / Edit User" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">    
    <script type="text/javascript">
        LoadScript();
    </script>
    <style type="text/css">
        .style1
        {
            width: 200px;
            height: 1px;
        }
        .style2
        {
            height: 1px;
        }
        .style3
        {
            height: 46px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="container" runat="Server">
    <div id="headercaption">ADD / EDIT USER</div>
    <div style="margin:0px auto;width:550px;">
        <fieldset style="width:100%;">
            <legend>Edit Location</legend>
            <table border="0" cellpadding="1" cellspacing="1" width="100%">
                <tr>
                    <td class="style1">Location Name:<span class="errormessage1">*</span></td>
                    <td class="style2">
                        <asp:TextBox ID="txtLocName" runat="server" MaxLength="10" Width="350" Enabled="false"></asp:TextBox><br />
                        <span id="spnName" runat="server" class="errormessage" style="display:none;"></span>
                        <%--<asp:RequiredFieldValidator ID="rfvUserName" runat="server" CssClass="errormessage" ControlToValidate="txtUserName" Display="Dynamic" ValidationGroup="Save"></asp:RequiredFieldValidator>--%>
                    </td>
                </tr>
                <tr>
                    <td class="style3">Address Line1:<span class="errormessage1">*</span></td>
                    <td class="style3">
                        <asp:TextBox ID="txtAddress1" runat="server" CssClass="textboxuppercase" MaxLength="30" Width="350" TextMode="MultiLine" Enabled="false">
                        </asp:TextBox><br />
                        <span id="spnAddr1" runat="server" class="errormessage" style="display:none;"></span>
                        <%--<asp:RequiredFieldValidator ID="rfvFName" runat="server" CssClass="errormessage" ControlToValidate="txtFName" Display="Dynamic" ValidationGroup="Save"></asp:RequiredFieldValidator>--%>
                    </td>
                </tr>
                <tr>
                    <td>Address Line2:<span class="errormessage1">*</span></td>
                    <td>
                        <asp:TextBox ID="txtAddress2" runat="server" CssClass="textboxuppercase" MaxLength="30" Width="350" TextMode="MultiLine" Enabled="false">
                        </asp:TextBox><br />
                        <span id="spnAddr2" runat="server" class="errormessage" style="display:none;"></span>
                    </td>
                </tr>
                <tr>
                    <td>Location Type:<span class="errormessage1">*</span></td>
                    <td>
                        <asp:DropDownList ID="ddlLocType" runat="server" AutoPostBack="false" Width="250px" CssClass="chzn-select medium-select single" Enabled="false">
                            <asp:ListItem Text="HO" Value="H"></asp:ListItem>
                            <asp:ListItem Text="Central Kitchen" Value="K"></asp:ListItem>
                            <asp:ListItem Text="Stock Point" Value="S"></asp:ListItem>
                            <asp:ListItem Text="Outlet" Value="O"></asp:ListItem>
                        </asp:DropDownList><br />
                        <span id="spnLocType" runat="server" class="errormessage" style="display:none;"></span>
                        
                    </td>
                </tr>
                <tr>
                    <td>Location Status:<span class="errormessage1">*</span></td>
                    <td>
                        <asp:DropDownList ID="ddlOF" runat="server" AutoPostBack="true" Width="250px" CssClass="chzn-select medium-select single" Enabled="false">
                            <asp:ListItem Text="Own" Value="O"></asp:ListItem>
                            <asp:ListItem Text="Franchise" Value="F"></asp:ListItem>
                        </asp:DropDownList><br />
                        <span id="spnLocStat" runat="server" class="errormessage" style="display:none;"></span>
                        <%--<asp:RequiredFieldValidator ID="rfvRole" runat="server" CssClass="errormessage" ControlToValidate="ddlRole" InitialValue="0" Display="Dynamic" ValidationGroup="Save"></asp:RequiredFieldValidator>--%>
                    </td>
                </tr>
                <tr>
                    <td>GST No:<span class="errormessage1">*</span></td>
                    <td>
                        <asp:TextBox ID="txtVATNo" runat="server" CssClass="textboxuppercase" MaxLength="50" Width="250"></asp:TextBox><br />
                        <span id="spnVATNo" runat="server" class="errormessage" style="display:none;"></span>
                        
                    </td>
                </tr>
                <tr>
                    <td>Printer Name:<span class="errormessage1">*</span></td>
                    <td>
                        <asp:TextBox ID="txtPrinterName" runat="server" CssClass="textboxuppercase" MaxLength="50" Width="250"></asp:TextBox><br />
                        <span id="SpnPrinterName" runat="server" class="errormessage" style="display:none;"></span>                    
                    </td>
                </tr>
                <tr>
                    <td>Opening Stock Date:<span class="errormessage1">*</span></td>
                    <td>
                        <asp:TextBox ID="txtOBDate" runat="server" MaxLength="50" Width="250"></asp:TextBox><br />
                        <span id="Span2" runat="server" class="errormessage" style="display:none;"></span>
                        <cc1:CalendarExtender ID="cdTranDate" TargetControlID="txtOBDate" runat="server" Format="dd/MM/yyyy" Enabled="True" />                    
                    </td>
                </tr>
                <tr>
                    <td>Composit:<span class="errormessage1">*</span></td>
                    <td>
                        <asp:CheckBox ID="chkComposit" runat="server"/>
                        <span id="Span1" runat="server" class="errormessage" style="display:none;"></span>                    
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Button ID="btnSave" runat="server" Text="Save" ValidationGroup="Save" OnClick="btnSave_Click" />&nbsp;&nbsp;<asp:Button 
                            ID="btnBack" runat="server" Text="Back" onclick="btnBack_Click"/>&nbsp;&nbsp;<asp:Label ID="msgbox" runat="server" Text="" ForeColor="Red" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </div>
</asp:Content>