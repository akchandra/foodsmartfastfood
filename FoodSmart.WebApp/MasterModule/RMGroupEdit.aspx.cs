﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FoodSmart.BLL;
using FoodSmart.Common;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using FoodSmart.Utilities.ResourceManager;
using System.Data;

namespace FoodSmart.WebApp.MasterModule
{
    public partial class RMGroupEdit : System.Web.UI.Page
    {
        #region Private Member Variables

        private int _userId = 0;
        private int _GroupId = 0;
        private bool _canAdd = false;
        private bool _canEdit = false;
        private bool _canDelete = false;
        private bool _canView = false;

        #endregion

        #region Protected Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            RetriveParameters();
            CheckUserAccess();
            SetAttributes();
            if (!IsPostBack)
            {
                LoadData();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            SaveRecord();
        }

        #endregion

        #region Private Methods

        private void RetriveParameters()
        {
            _userId = UserBLL.GetLoggedInUserId();

            //Get user permission.
            UserBLL.GetUserPermission(out _canAdd, out _canEdit, out _canDelete, out _canView);

            if (!ReferenceEquals(Request.QueryString["id"], null))
            {
                Int32.TryParse(GeneralFunctions.DecryptQueryString(Request.QueryString["id"].ToString()), out _GroupId);
            }
        }

        private void SetAttributes()
        {
            spnName.Style["display"] = "none";

            if (!IsPostBack)
            {
                //if (_GroupId == -1) //Add mode
                //{
                //    if (!_canAdd) btnSave.Visible = false;
                //}
                //else
                //{
                //    if (!_canEdit) btnSave.Visible = false;
                //}

                btnSave.ToolTip = ResourceManager.GetStringWithoutName("R00060");
                btnBack.ToolTip = ResourceManager.GetStringWithoutName("R00061");
                btnBack.OnClientClick = "javascript:return RedirectAfterCancelClick('RMGroupList.aspx','" + ResourceManager.GetStringWithoutName("R00025") + "')";

                spnName.InnerText = ResourceManager.GetStringWithoutName("R00046");

            }

        }

        private void CheckUserAccess()
        {
            if (!ReferenceEquals(Session[Constants.SESSION_USER_INFO], null))
            {
                IUser user = (IUser)Session[Constants.SESSION_USER_INFO];

                if (ReferenceEquals(user, null) || user.Id == 0)
                {
                    Response.Redirect("~/Login.aspx");
                }

                if (user.Id != (int)UserRole.Admin)
                {
                    Response.Redirect("~/Unauthorized.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Login.aspx");
            }

            if (_GroupId == 0)
                Response.Redirect("~/MasterModule/ItemGroupList.aspx");

            if (!_canView)
            {
                Response.Redirect("~/Unauthorized.aspx");
            }
        }



        private void LoadData()
        {
            ItemBLL oItembll = new ItemBLL();

            IItems oitem = (ItemsEntity)oItembll.GetRMGroupForEdit(_GroupId);

            if (!ReferenceEquals(oitem, null))
            {
                txtName.Text = oitem.GroupName;
                txtId.Text = oitem.RMGroupID.ToString();
                //ddRorF.SelectedIndex = oitem.RorF == "F" ? 0 : 1;
                ddRorF.SelectedValue = oitem.GroupType;
            }
        }

        private bool ValidateControls(IItems item)
        {
            bool isValid = true;

            if (item.GroupDescription == string.Empty)
            {
                isValid = false;
                spnName.Style["display"] = "";
            }


            return isValid;
        }

        private void SaveRecord()
        {
            IItems item = new ItemsEntity();
            BuildUserEntity(item);

            if (ValidateControls(item))
            {
                string strMode = "A";
                if (item.ItemGroupID > 0)
                {
                    strMode = "E";
                }
                ItemBLL itemBLL = new ItemBLL();
                int result = itemBLL.SaveRMGroup(item, strMode);

                //if (_uId == -1)
                if (strMode == "E")
                {
                    //SendEmail(user);
                    Response.Redirect("~/MasterModule/RMGroupList.aspx");
                }
                else
                {
                    txtName.Text = "";
                    item.ItemID = 0;
                    ddRorF.SelectedIndex = 0;
                }
            }
        }

        private void BuildUserEntity(IItems item)
        {
            item.ItemGroupID = _GroupId;
            item.GroupDescription = txtName.Text.ToUpper();
            item.GroupType = ddRorF.SelectedValue;
        }


        #endregion

    }
}