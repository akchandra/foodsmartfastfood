﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FoodSmart.BLL;
using FoodSmart.Common;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using FoodSmart.Utilities.ResourceManager;

namespace FoodSmart.WebApp.MasterModule
{
    public partial class EditCompany : System.Web.UI.Page
    {
        #region Private Member Variables

        private int _userId = 0;

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            RetriveParameters();
            CheckUserAccess();
            if (!Page.IsPostBack) 
                Loadcompany();
            //SetAttributes();
        }

     

        #region Private Methods

        private void RetriveParameters()
        {
            if (!ReferenceEquals(Request.QueryString["id"], null) && Convert.ToString(Request.QueryString["id"]) != string.Empty)
            {
                int.TryParse(GeneralFunctions.DecryptQueryString(Convert.ToString(Request.QueryString["id"])), out _userId);
            }
            else
            {
                _userId = UserBLL.GetLoggedInUserId();
            }
        }

        private void CheckUserAccess()
        {
            if (_userId != 1)
            {
                Response.Redirect("~/Login.aspx");
            }
        }

        private void Loadcompany()
        {
            FoodSmart.BLL.DBInteraction dbinteract = new FoodSmart.BLL.DBInteraction();
            System.Data.DataSet ds = dbinteract.GetCompany();

            txtInformation1.Text = ds.Tables[0].Rows[0]["btmLine1"].ToString();
            txtInformation2.Text = ds.Tables[0].Rows[0]["btmLine2"].ToString();
            txtInformation3.Text = ds.Tables[0].Rows[0]["btmLine3"].ToString();
            txtInformation4.Text = ds.Tables[0].Rows[0]["btmLine4"].ToString();
            lblCompName.Text = ds.Tables[0].Rows[0]["Company_Name"].ToString();
            lblCompAddress1.Text = ds.Tables[0].Rows[0]["Address1"].ToString();
            lblCompAddress2.Text = ds.Tables[0].Rows[0]["Address2"].ToString();
            rdoItemInclTax.SelectedValue = ds.Tables[0].Rows[0]["CalculationMethod"].ToString();
            //txtStax.Text = ds.Tables[0].Rows[0]["ServiceTaxPer"].ToString();
        }

        
        #endregion

        protected void btnSave_Click(object sender, EventArgs e)
        {
            ICompany comp = new CompanyEntity();
            comp.btmLine1 = txtInformation1.Text;
            comp.btmLine2 = txtInformation2.Text;
            comp.btmLine3 = txtInformation3.Text;
            comp.btmLine4 = txtInformation4.Text;
            comp.CalculationMethod = rdoItemInclTax.SelectedValue;
            //comp.ServiceTaxPer = txtStax.Text.ToDecimal();
            //IActionResult result = UserBLL.ChangePassword(user);
            int result = CommonBLL.SaveCompanyDetail(comp, _userId);
            

            //if (!result)
            //{
            //    Session.Abandon();
            //    Page.ClientScript.RegisterStartupScript(typeof(Page), "alert", "<script>javascript:alert('" + ResourceManager.GetStringWithoutName("R00032") + "');window.location.href='../Login.aspx'</script>");
            //}
        }


    }
}