﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TranEdit.aspx.cs" Inherits="FoodSmart.WebApp.Transaction.TranEdit" MasterPageFile="~/Site.Master" Title=":: FoodSmart :: Add / Edit Transaction" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FoodSmart.WebApp" Namespace="FoodSmart.WebApp.CustomControls" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%--<script type="text/javascript">
        LoadScript();
        function toggleAddPanel(param) {
            var tdAddItem = document.getElementById('tdAddItem');

            if (param)
                tdAddItem.style.display = 'block';
            else
                tdAddItem.style.display = 'none';
        }
        
    </script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="container" runat="Server">
    <div id="dvAsync" style="padding: 5px; display: none;">
        <div class="asynpanel">
            <div id="dvAsyncClose">
                <img alt="" src="../../Images/Close-Button.bmp" style="cursor: pointer;" onclick="ClearErrorState()" />
            </div>
            <div id="dvAsyncMessage">An error has occurred. Please try again
            </div>
        </div>
    </div>
    <asp:UpdateProgress ID="uTransaction" runat="server" AssociatedUpdatePanelID="upTran">
        <ProgressTemplate>
            <div class="progress">
                <div id="image">
                    <img src="../../Images/PleaseWait.gif" alt="" />
                </div>
                <div id="text">
                    Please Wait...
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdateProgress ID="uProgressItem" runat="server" AssociatedUpdatePanelID="upItem">
        <ProgressTemplate>
            <div class="progress">
                <div id="image">
                    <img src="../../Images/PleaseWait.gif" alt="" />
                </div>
                <div id="text">
                    Please Wait...
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div id="headercaption">ADD / EDIT TRANSACTION</div>
    <div style="width: 900px; margin: 0 auto; margin-top: 30px;">
        <fieldset style="width: 100%;">
            <legend>Add / Edit transaction</legend>
            <asp:UpdatePanel ID="upTran" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <table border="0" cellpadding="3" cellspacing="3" width="100%">
                        <tr>
                            <td style="width: 15%;">Transaction type:<span class="errormessage1">*</span></td>
                            <td style="width: 30%;">
                                <asp:DropDownList ID="ddlType" runat="server" Width="175px" AutoPostBack="true" 
                                CssClass="chzn-select medium-select single" OnSelectedIndexChanged="ddlType_SelectedIndexChanged">
                                    <asp:ListItem Text="Purchase" Value="P" />
                                    <asp:ListItem Text="Transfer" Value="T" />
                                    <asp:ListItem Text="Wastage" Value="W" />
                                    <asp:ListItem Text="Purchase return" Value="R" />
<%--                                    <asp:ListItem Text="Issue return" Value="Y" />
                                    <asp:ListItem Text="Transfer" Value="T" />--%>
                                </asp:DropDownList>
                            </td>
                            <td style="width: 15%;">From Store:<span class="errormessage1">*</span></td>
                            <td style="width: 30%;">
                                <div id="dvNameDDL" runat="server">
                                    <asp:DropDownList ID="ddlFromLoc" runat="server" Width="175px" AutoPostBack="true" CssClass="chzn-select medium-select single" OnSelectedIndexChanged="ddlFromLoc_SelectedIndexChanged"></asp:DropDownList><br />
                                    <asp:RequiredFieldValidator ID="rfvFromLoc" runat="server" ControlToValidate="ddlFromLoc" ErrorMessage="This field is required*" CssClass="errormessage" ValidationGroup="Save" Display="Dynamic" InitialValue="0"></asp:RequiredFieldValidator>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Transaction no:</td>
                            <td>
                                <asp:TextBox ID="txtTranNo" runat="server" CssClass="textboxuppercase" TabIndex="5" MaxLength="30" Width="102px" Height="20px" Enabled="false"></asp:TextBox><br />
                            </td>
                            <td>Transaction date:<span class="errormessage1">*</span></td>
                            <td>
                                <asp:TextBox ID="txtTranDate" runat="server" CssClass="textboxuppercase" TabIndex="5" MaxLength="30" Width="102px" Height="20px" Style="text-align: left"></asp:TextBox><br />
                                <cc1:CalendarExtender ID="cdTranDate" TargetControlID="txtTranDate" runat="server" Format="dd/MM/yyyy" Enabled="True" />
                                <asp:RequiredFieldValidator ID="rfvTranDate" runat="server" ControlToValidate="txtTranDate" ErrorMessage="This field is required*" CssClass="errormessage" ValidationGroup="Save" Display="Dynamic"></asp:RequiredFieldValidator>
                                <span id="spnTranDate" runat="server" class="errormessage" style="display: none;"></span>
                            </td>
                        </tr>
                        <tr>
                            <td>To Store:<span class="errormessage1">*</span></td>
                            <td>
                                <asp:DropDownList ID="ddlToLoc" runat="server" Width="175px" AutoPostBack="false" CssClass="chzn-select medium-select single"></asp:DropDownList>
                                <br />
                                <span id="spnToLoc" runat="server" class="errormessage" style="display: none;"></span>
                            </td>
                            <td>Total amount:</td>
                            <td>
                                <cc2:CustomTextBox ID="txtTotalAmount" runat="server" CssClass="numerictextbox" TabIndex="3" Width="100px" Height="20px" Type="Numeric" MaxLength="10" Precision="10" Scale="2" Enabled="false"></cc2:CustomTextBox><br />
                                <span id="spnTotalAmount" runat="server" class="errormessage" style="display: none;"></span>
                            </td>
                        </tr>
                        <tr>
                            <td>Vendor:<span class="errormessage1">*</span></td>
                            <td>
                                <asp:DropDownList ID="ddlVendor" runat="server" Width="250px" AutoPostBack="false" CssClass="chzn-select medium-select single"></asp:DropDownList>
                                <br />
                                <span id="spnVendor" runat="server" class="errormessage" style="display: none;"></span>
                            </td>
                            <td>Vendor ref:</td>
                            <td>
                                <asp:TextBox ID="txtVendorRef" runat="server" CssClass="textboxuppercase" TabIndex="5" MaxLength="30" Width="102px" Height="20px"></asp:TextBox><br />
                                <span id="spnVendorRef" runat="server" class="errormessage" style="display: none;"></span>
                            </td>
                        </tr>
                        <tr>
                            <td>Original trans ref:</td>
                            <td>
                                <asp:TextBox ID="txtOriginalTranRef" runat="server" CssClass="textboxuppercase" TabIndex="5" MaxLength="30" Width="102px" Height="20px"></asp:TextBox>
                                <asp:Button ID="btnTranRef" runat="server" Text="Search" OnClick="btnTranRef_Click" /></td>
                        </tr>
                        <caption>
                            <br />
                            <span ID="spnOriginalTranRef" runat="server" class="errormessage" 
                                style="display: none;"></span>
                            </td>
                            <tr>
                                <td>
                                    Narration:</td>
                                <td>
                                    <asp:TextBox ID="txtNarration" runat="server" CssClass="textboxuppercase" 
                                        MaxLength="100" TextMode="MultiLine" Width="250"></asp:TextBox>
                                    <br />
                                    <span ID="spnNarration" runat="server" class="errormessage" 
                                        style="display: none;"></span>
                                </td>
                            </tr>
                        </caption>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdatePanel ID="upItem" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div style="width: 100%; padding-top: 20px;">
                        <table border="0" cellpadding="3" cellspacing="3" width="100%">
                            <tr>
                                <td style="width: 5%; padding: 10px 0px 10px 0px;" class="gridviewheader">Sl#</td>
                                <td style="width: 15%; padding: 10px 0px 10px 0px;" class="gridviewheader">Item type</td>
                                <td style="width: 30%; padding: 10px 0px 10px 0px;" class="gridviewheader">Item</td>
                                <td style="width: 10%; padding: 10px 0px 10px 0px;" class="gridviewheader">Unit</td>
                                <td style="width: 7%; padding: 10px 0px 10px 0px; text-align: right;" 
                                    class="gridviewheader">Quantity</td>
                                <td style="width: 8%; padding: 10px 0px 10px 0px; text-align: right;" 
                                    class="gridviewheader" HorizontalAlign="Right">Rate</td>
                                <td style="width: 5%; padding: 10px 0px 10px 0px;" class="gridviewheader" HorizontalAlign="Right">GST%</td>
                                <td style="width: 8%; padding: 10px 0px 10px 0px; text-align: right;" 
                                    class="gridviewheader" HorizontalAlign="Right">GST</td>
                                <td style="width: 9%; padding: 10px 0px 10px 0px; text-align: right;" 
                                    class="gridviewheader" HorizontalAlign="Right">Total</td>
                                <td style="width: 10%; padding: 10px 0px 10px 0px;" class="gridviewheader"></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>
                                    <asp:HiddenField ID="hdnMode" runat="server" />
                                    <asp:DropDownList ID="ddlSubItemType" runat="server" AutoPostBack="true" CssClass="chzn-select medium-select single" OnSelectedIndexChanged="ddlSubItemType_SelectedIndexChanged">
                                        <asp:ListItem Text="Raw material" Value="R"/>
                                        <asp:ListItem Text="Finished" Value="F"/>
                                        <asp:ListItem Text="Semi finished" Value="S"/>
                                        <asp:ListItem Text="Consumables" Value="C" />
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlSubItem" runat="server" AutoPostBack="true" Width="100%" CssClass="chzn-select medium-select single" 
                                        OnSelectedIndexChanged="ddlSubItem_SelectedIndexChanged">
                                        <asp:ListItem Text="Select" Value="0" />
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtItemUnit" runat="server" CssClass="textboxuppercase"  Height="20px"
                                        MaxLength="100" Width="100%" Enabled="False"></asp:TextBox>
                                </td>
                                <td>
                                    <cc2:CustomTextBox ID="txtItemQty" runat="server" CssClass="numerictextbox" 
                                        Width="100%" Height="20px" Type="Decimal" MaxLength="13" Precision="10" 
                                        Scale="2" ontextchanged="txtItemQty_TextChanged" AutoPostBack="true"></cc2:CustomTextBox>
                                </td>
                                <td>
                                    <cc2:CustomTextBox ID="txtItemRate" runat="server" CssClass="numerictextbox" 
                                        Width="100%" Height="20px" Type="Decimal" MaxLength="13" Precision="10" 
                                        Scale="2" ontextchanged="txtItemRate_TextChanged" AutoPostBack="true"></cc2:CustomTextBox>
                                </td>
                                <td>
                                    <cc2:CustomTextBox ID="txtGSTPer" runat="server" CssClass="numerictextbox" 
                                        Width="100%" Height="20px" Type="Decimal" MaxLength="13" Precision="10" 
                                        Scale="2" ontextchanged="txtItemRate_TextChanged" AutoPostBack="true"></cc2:CustomTextBox>
                                </td>
                                <td>
                                    <cc2:CustomTextBox ID="txtItemTax" runat="server" CssClass="numerictextbox" 
                                        Width="100%" Height="20px" Type="Decimal" MaxLength="13" Precision="10" 
                                        Scale="2" ontextchanged="txtItemTax_TextChanged" AutoPostBack="true"></cc2:CustomTextBox>
                                </td>
                                <td>
                                    <cc2:CustomTextBox ID="txtItemTot" runat="server" CssClass="numerictextbox" 
                                        Width="100%" Height="20px" Type="Decimal" MaxLength="13" Precision="10" 
                                        Scale="2" Enabled="False"></cc2:CustomTextBox>
                                   <%-- <asp:TextBox ID="txtItemTot" runat="server" CssClass="textboxuppercase" MaxLength="100" Width="100%" Enabled="false"></asp:TextBox>--%>
                                </td>
                                <td>
                                    <asp:Button ID="btnSaveItem" runat="server" Text="Add item" OnClick="btnSaveItem_Click" /></td>
                            </tr>
                        </table>
                    </div>
                    <div style="padding-top: 10px;">
                        <asp:GridView ID="gvwItem" runat="server" AutoGenerateColumns="false" ShowHeader="false" AutoGenerateEditButton="false" AllowPaging="false" BorderStyle="None" BorderWidth="0" OnPageIndexChanging="gvwItem_PageIndexChanging" OnRowDataBound="gvwItem_RowDataBound" OnRowCommand="gvwItem_RowCommand" Width="100%">
                            <PagerSettings Mode="NumericFirstLast" Position="Bottom" />
                            <PagerStyle CssClass="gridviewpager" />
                            <EmptyDataRowStyle CssClass="gridviewemptydatarow" />
                            <EmptyDataTemplate>No items Found</EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="Sl#">
                                    <HeaderStyle CssClass="gridviewheader" />
                                    <ItemStyle CssClass="gridviewitem" Width="5%" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblSlNo" runat="server"></asp:Label>
                                        <asp:HiddenField ID="hdnItemTranId" runat="server"></asp:HiddenField>
                                        <%--<asp:HiddenField ID="hdnTranId" runat="server"></asp:HiddenField>--%>
                                        <asp:HiddenField ID="hdnItemId" runat="server"></asp:HiddenField>
                                        <asp:HiddenField ID="hdnType" runat="server"></asp:HiddenField>
                                        <asp:HiddenField ID="hdnTranType" runat="server"></asp:HiddenField>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Item type">
                                    <HeaderStyle CssClass="gridviewheader" />
                                    <ItemStyle CssClass="gridviewitem" Width="15%" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblType" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Item">
                                    <HeaderStyle CssClass="gridviewheader" />
                                    <ItemStyle CssClass="gridviewitem" Width="28%" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblItem" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Unit">
                                    <HeaderStyle CssClass="gridviewheader" />
                                    <ItemStyle CssClass="gridviewitem" Width="10%" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblUnit" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Quantity">
                                    <HeaderStyle CssClass="gridviewheader" HorizontalAlign="Right" />
                                    <ItemStyle CssClass="gridviewitem" Width="7%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblQty" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Rate">
                                    <HeaderStyle CssClass="gridviewheader" HorizontalAlign="Right" />
                                    <ItemStyle CssClass="gridviewitem" Width="8%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblRate" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="GSTPer">
                                    <HeaderStyle CssClass="gridviewheader" HorizontalAlign="Right" />
                                    <ItemStyle CssClass="gridviewitem" Width="5%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblGSTPer" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Tax">
                                    <HeaderStyle CssClass="gridviewheader" HorizontalAlign="Right" />
                                    <ItemStyle CssClass="gridviewitem" Width="8%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblTax" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Total amount">
                                    <HeaderStyle CssClass="gridviewheader" HorizontalAlign="Right" />
                                    <ItemStyle CssClass="gridviewitem" Width="9%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblAmt" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="gridviewheader" />
                                    <ItemStyle CssClass="gridviewitem" Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnEdit" runat="server" CommandName="Modify" ImageUrl="~/Images/edit.png" Height="16" Width="16" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="gridviewheader" />
                                    <ItemStyle CssClass="gridviewitem" Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnRemove" runat="server" CommandName="Remove" ImageUrl="~/Images/remove.png" Height="16" Width="16" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

            <div style="padding-top: 10px;">
                <asp:Button ID="btnSave" runat="server" Text="Save" ValidationGroup="Save" OnClick="btnSave_Click" />&nbsp;&nbsp;<asp:Button ID="btnBack" runat="server" Text="Back" />&nbsp;&nbsp;<asp:Label ID="msgbox" runat="server" Text="" ForeColor="Red" />
            </div>
        </fieldset>
    </div>
</asp:Content>
