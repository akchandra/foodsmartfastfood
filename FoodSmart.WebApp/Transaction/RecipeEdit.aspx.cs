﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FoodSmart.BLL;
using FoodSmart.Common;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using FoodSmart.Utilities.ResourceManager;
using System.Data;

namespace FoodSmart.WebApp.Transaction
{
    public partial class RecipeEdit : System.Web.UI.Page
    {
        #region Private Member Variables

        private int _userId = 0;
        private int _rId = 0;
        private bool _canAdd = false;
        private bool _canEdit = false;
        private bool _canDelete = false;
        private bool _canView = false;

        #endregion

        #region Protected Event Handlers
         
        protected void Page_Load(object sender, EventArgs e)
        {
            RetriveParameters();
            //CheckUserAccess();
            SetAttributes();

            if (!IsPostBack)
            {
                LoadItems();
                LoadSubItems();
                LoadData();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            SaveRecipe();
        }

        protected void btnSaveItem_Click(object sender, EventArgs e)
        {
            if (ddlSubItem.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "alert", "<script>javascript:void alert('Please select an item');</script>", false);
                return;
            }
            AddSubItem();
            ddlSubItemType.SelectedValue = "R";
            LoadSubItems();
            txtItemUnit.Text = string.Empty;
            txtItemQty.Text = string.Empty;
            txtItemRate.Text = string.Empty;
            txtItemTot.Text = string.Empty;
            hdnMode.Value = string.Empty;
        }

        protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlType.SelectedValue == "F")
            {
                dvNameDDL.Style["display"] = "block";
                dvNametxt.Style["display"] = "none";
            }
            else
            {
                dvNameDDL.Style["display"] = "none";
                dvNametxt.Style["display"] = "block";
            }
        }

        protected void gvwRecipe_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }

        protected void gvwRecipe_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Modify")
            {
                EditItem(Convert.ToInt32(e.CommandArgument));
            }
            else if (e.CommandName == "Remove")
            {
                RemoveItem(Convert.ToInt32(e.CommandArgument));
            }
        }

        protected void gvwRecipe_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                GeneralFunctions.ApplyGridViewAlternateItemStyle(e.Row, 9);
                ((Label)e.Row.FindControl("lblSlNo")).Text = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Id"));
                ((HiddenField)e.Row.FindControl("hdnRecipeItemId")).Value = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "pk_RecipeItemID"));
                ((HiddenField)e.Row.FindControl("hdnRecipeId")).Value = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "fk_RecipeID"));
                ((HiddenField)e.Row.FindControl("hdnItemId")).Value = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "fk_ItemID"));
                ((HiddenField)e.Row.FindControl("hdnType")).Value = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "ItemType"));
                ((Label)e.Row.FindControl("lblType")).Text = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "ItemTypeDesc"));
                ((Label)e.Row.FindControl("lblItem")).Text = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Item"));
                ((Label)e.Row.FindControl("lblUnit")).Text = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "UOM"));
                ((Label)e.Row.FindControl("lblQty")).Text = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "QtyReq"));
                ((Label)e.Row.FindControl("lblRate")).Text = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "ItemRate"));
                ((Label)e.Row.FindControl("lblAmt")).Text = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Total"));

                // Edit link
                ImageButton btnEdit = (ImageButton)e.Row.FindControl("btnEdit");
                btnEdit.ToolTip = ResourceManager.GetStringWithoutName("R00035");

                //Delete link
                ImageButton btnRemove = (ImageButton)e.Row.FindControl("btnRemove");
                btnRemove.ToolTip = ResourceManager.GetStringWithoutName("R00034");

                btnEdit.CommandArgument = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Id"));
                btnRemove.CommandArgument = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Id"));
            }
        }

        protected void ddlSubItemType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlSubItem.Items.Clear();
            LoadSubItems();
        }

        protected void ddlSubItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadSubItemDetails();
        }

        protected void ddlName_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadItemDetails();
        }

        protected void txtItemQty_TextChanged(object sender, EventArgs e)
        {
            DisplayItemTotal();
        }
        #endregion

        #region Private Methods

        private void LoadItems()
        {
            List<IRecipe> items = RecipeBLL.GetItems("F");
            ddlName.DataValueField = "Id";
            ddlName.DataTextField = "Name";
            ddlName.DataSource = items;
            ddlName.DataBind();
        }

        private void LoadSubItems()
        {
            List<IRecipe> items = RecipeBLL.GetItems(ddlSubItemType.SelectedValue);
            ddlSubItem.DataValueField = "Id";
            ddlSubItem.DataTextField = "Name";
            ddlSubItem.DataSource = items;
            ddlSubItem.DataBind();
            ddlSubItem.Items.Insert(0, new ListItem("All", "0"));
        }


        private void LoadSubItemDetails()
        {
            if (ddlSubItem.SelectedValue != "0")
            {
                IRecipe item = RecipeBLL.GetItem(ddlSubItemType.SelectedValue, Convert.ToInt32(ddlSubItem.SelectedValue));

                if (!ReferenceEquals(item, null))
                {
                    txtItemUnit.Text = item.UOM;
                    txtItemQty.Text = Convert.ToString(item.Quantity);
                    txtItemRate.Text = Convert.ToString(item.Rate);
                    txtItemTot.Text = Convert.ToString(item.Amount);
                }
            }
            else
            {
                txtItemUnit.Text = string.Empty;
                txtItemQty.Text = string.Empty;
                txtItemRate.Text = string.Empty;
                txtItemTot.Text = string.Empty;
            }
        }

        private void LoadItemDetails()
        {
            if (ddlName.SelectedValue != "0")
            {
                IRecipe item = RecipeBLL.GetItem(ddlType.SelectedValue, Convert.ToInt32(ddlName.SelectedValue));

                if (!ReferenceEquals(item, null))
                {
                    txtUnit.Text = item.UOM;
                }
            }
            else
            {
                txtUnit.Text = string.Empty;
            }
        }

        private void RetriveParameters()
        {
            dvNameDDL.Style["display"] = "block";
            dvNametxt.Style["display"] = "none";

            _userId = UserBLL.GetLoggedInUserId();

            //Get user permission.
            UserBLL.GetUserPermission(out _canAdd, out _canEdit, out _canDelete, out _canView);

            if (!ReferenceEquals(Request.QueryString["id"], null))
            {
                Int32.TryParse(GeneralFunctions.DecryptQueryString(Request.QueryString["id"].ToString()), out _rId);
            }
        }

        private void SetAttributes()
        {
            spnName.Style["display"] = "none";

            if (!IsPostBack)
            {
                //if (_rId == -1) //Add mode
                //{
                //    if (!_canAdd) btnSave.Visible = false;
                //}
                //else
                //{
                //    if (!_canEdit) btnSave.Visible = false;
                //}

                btnSave.ToolTip = ResourceManager.GetStringWithoutName("R00060");
                btnBack.ToolTip = ResourceManager.GetStringWithoutName("R00061");
                btnBack.OnClientClick = "javascript:return RedirectAfterCancelClick('RecipeList.aspx','" + ResourceManager.GetStringWithoutName("R00025") + "')";
                //revEmail.ValidationExpression = Constants.EMAIL_REGX_EXP;
                //revEmail.ErrorMessage = ResourceManager.GetStringWithoutName("R00051");

                //spnName.InnerText = ResourceManager.GetStringWithoutName("R00046");
                //spnFName.InnerText = ResourceManager.GetStringWithoutName("R00047");
                ////spnLName.InnerText = ResourceManager.GetStringWithoutName("R00048");
                //spnEmail.InnerText = ResourceManager.GetStringWithoutName("R00049");
                //spnRole.InnerText = ResourceManager.GetStringWithoutName("R00050");
                //spnLoc.InnerText = ResourceManager.GetStringWithoutName("R00064");

            }
        }

        private void CheckUserAccess()
        {
            if (!ReferenceEquals(Session[Constants.SESSION_USER_INFO], null))
            {
                IUser user = (IUser)Session[Constants.SESSION_USER_INFO];

                if (ReferenceEquals(user, null) || user.Id == 0)
                {
                    Response.Redirect("~/Login.aspx");
                }

                if (user.RoleID != (int)UserRole.Admin)
                {
                    Response.Redirect("~/Unauthorized.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Login.aspx");
            }

            if (_rId == 0)
                Response.Redirect("~/Transaction/ManageRecipe.aspx");

            if (!_canView)
            {
                Response.Redirect("~/Unauthorized.aspx");
            }
        }

        private DataSet GetGridData()
        {
            DataSet ds = CreateDataSetSchema(false);
            for (int j = 0; j < gvwRecipe.Rows.Count; j++)
            {
                DataRow dr;
                GridViewRow row = gvwRecipe.Rows[j];
                dr = ds.Tables[0].NewRow();

                dr["pk_RecipeItemID"] = Convert.ToInt64(((HiddenField)row.FindControl("hdnRecipeItemId")).Value);
                dr["fk_RecipeID"] = Convert.ToInt64(((HiddenField)row.FindControl("hdnRecipeId")).Value);
                dr["ItemType"] = ((HiddenField)row.FindControl("hdnType")).Value;
                dr["fk_ItemID"] = Convert.ToInt32(((HiddenField)row.FindControl("hdnItemId")).Value);
                dr["UOM"] = ((Label)row.FindControl("lblUnit")).Text;
                dr["QtyReq"] = Convert.ToDecimal(((Label)row.FindControl("lblQty")).Text);
                dr["ItemRate"] = Convert.ToDecimal(((Label)row.FindControl("lblRate")).Text);
                ds.Tables[0].Rows.Add(dr);
            }

            return ds;
        }

        private void AddSubItem()
        {
            DataSet ds = CreateDataSetSchema(true);
            int id = 0;
            int.TryParse(hdnMode.Value, out id);

            int index = 0;
            for (int j = 0; j < gvwRecipe.Rows.Count; j++)
            {
                index++;
                DataRow dr;
                GridViewRow row = gvwRecipe.Rows[j];
                dr = ds.Tables[0].NewRow();

                if (index == id)
                {
                    var recipeItemId = (!string.IsNullOrEmpty(((HiddenField)row.FindControl("hdnRecipeItemId")).Value)) ? Convert.ToInt64(((HiddenField)row.FindControl("hdnRecipeItemId")).Value) : 0;
                    var recipeId = (!string.IsNullOrEmpty(((HiddenField)row.FindControl("hdnRecipeId")).Value)) ? Convert.ToInt64(((HiddenField)row.FindControl("hdnRecipeId")).Value) : 0;
                   
                    FillExistingRow(dr, index, recipeItemId, recipeId);
                }

                else
                    LoadGridRow(row, dr, index);

                ds.Tables[0].Rows.Add(dr);
            }

            //Add new row

            if (id == 0)
                FillNewRow(ds, index);


            BindGrid(ds);
        }

        private void FillExistingRow(DataRow row, int index, Int64 recipeItemId, Int64 recipeId)
        {
            row["Id"] = index;
            row["pk_RecipeItemID"] = recipeId;

            if (ddlSubItemType.SelectedValue == "S")
            {
                row["fk_RecipeID"] = Convert.ToInt64(ddlSubItem.SelectedValue);
                row["fk_ItemID"] = 0;
            }
            else
            {
                row["fk_RecipeID"] = recipeId;
                row["fk_ItemID"] = Convert.ToInt64(ddlSubItem.SelectedValue);
            }

            row["ItemType"] = ddlSubItemType.SelectedValue;


            if (ddlSubItemType.SelectedValue == "R")
                row["ItemTypeDesc"] = "Raw material";
            else if (ddlSubItemType.SelectedValue == "F")
                row["ItemTypeDesc"] = "Finished";
            else if (ddlSubItemType.SelectedValue == "S")
                row["ItemTypeDesc"] = "Semi finished";

            row["Item"] = ddlSubItem.SelectedItem.Text;
            row["UOM"] = txtItemUnit.Text;
            row["QtyReq"] = (!string.IsNullOrEmpty(txtItemQty.Text)) ? Convert.ToDecimal(txtItemQty.Text) : 0;
            row["ItemRate"] = (!string.IsNullOrEmpty(txtItemRate.Text)) ? Convert.ToDecimal(txtItemRate.Text) : 0;
            row["Total"] = (!string.IsNullOrEmpty(txtItemTot.Text)) ? Convert.ToDecimal(txtItemTot.Text) : 0;
        }

        private void FillNewRow(DataSet ds, int index)
        {
            DataRow row = ds.Tables[0].NewRow();
            row["Id"] = index + 1;
            row["pk_RecipeItemID"] = 0;

            if (ddlSubItemType.SelectedValue == "S")
            {
                row["fk_RecipeID"] = Convert.ToInt64(ddlSubItem.SelectedValue);
                row["fk_ItemID"] = 0;
            }
            else
            {
                row["fk_RecipeID"] = 0;
                row["fk_ItemID"] = Convert.ToInt64(ddlSubItem.SelectedValue);
            }

            row["ItemType"] = ddlSubItemType.SelectedValue;


            if (ddlSubItemType.SelectedValue == "R")
                row["ItemTypeDesc"] = "Raw material";
            else if (ddlSubItemType.SelectedValue == "F")
                row["ItemTypeDesc"] = "Finished";
            else if (ddlSubItemType.SelectedValue == "S")
                row["ItemTypeDesc"] = "Semi finished";

            row["fk_ItemID"] = Convert.ToInt32(ddlSubItem.SelectedValue);
            row["Item"] = ddlSubItem.SelectedItem.Text;
            row["UOM"] = txtItemUnit.Text;
            row["QtyReq"] = (!string.IsNullOrEmpty(txtItemQty.Text)) ? Convert.ToDecimal(txtItemQty.Text) : 0;
            row["ItemRate"] = (!string.IsNullOrEmpty(txtItemRate.Text)) ? Convert.ToDecimal(txtItemRate.Text) : 0;
            row["Total"] = (!string.IsNullOrEmpty(txtItemTot.Text)) ? Convert.ToDecimal(txtItemTot.Text) : 0;

            ds.Tables[0].Rows.Add(row);
        }

        private void LoadData()
        {
            IRecipe recipe = RecipeBLL.GetRecipe(_rId);

            if (!ReferenceEquals(recipe, null))
            {
                ddlType.SelectedValue = recipe.Type;
                if (recipe.ItemId.HasValue)
                {
                    ddlName.SelectedValue = Convert.ToString(recipe.ItemId.Value);
                    dvNameDDL.Style["display"] = "block";
                    dvNametxt.Style["display"] = "none";
                }
                else
                {
                    txtName.Text = recipe.Name;
                    dvNameDDL.Style["display"] = "none";
                    dvNametxt.Style["display"] = "block";
                    txtName.Enabled = false;
                    ddlType.Enabled = false;
                }

                if (recipe.NoofPlates.HasValue)
                    txtNoofPlates.Text = Convert.ToString(recipe.NoofPlates.Value);

                if (recipe.Quantity.HasValue)
                    txtQty.Text = Convert.ToString(recipe.Quantity.Value);

                //txtAmt.Text = recipe.amount;
                txtRPU.Text = recipe.RatePerUnit;
                txtUnit.Text = recipe.UOM;

                if (recipe.DateIntroduced.HasValue)
                    txtIntro.Text = recipe.DateIntroduced.Value.ToString("dd/MM/yyyy");

                ddlProcessed.SelectedValue = recipe.ProcessedAt;
                txtSummary.Text = recipe.RecipeDetail;


                List<IRecipeItem> items = RecipeBLL.GetAllRecipieItems(_rId);
                if (items.Count > 0)
                {
                    LoadRecipeItems(items);
                }
            }
        }

        private void LoadRecipeItems(List<IRecipeItem> items)
        {
            DataSet ds = CreateDataSetSchema(true);
            int index = 1;
            double TotQty = 0.00;
            double TotAmt = 0.00;
            foreach (var item in items)
            {
                DataRow row = ds.Tables[0].NewRow();

                row["Id"] = index;
                row["pk_RecipeItemID"] = item.Id;
                row["fk_RecipeID"] = item.RecipeId;

                if (item.ItemId.HasValue)
                    row["fk_ItemID"] = item.ItemId.Value;

                row["ItemTypeDesc"] = item.TypeDescription;
                row["ItemType"] = item.Type;
                row["Item"] = item.Item;
                row["UOM"] = item.UOM;

                if (item.Quantity.HasValue)
                    row["QtyReq"] = item.Quantity.Value;
                if (item.Rate.HasValue)
                    row["ItemRate"] = item.Rate.Value;
                row["Total"] =  Math.Round(item.Quantity.Value * item.Rate.Value, 2);
                TotQty = TotQty + Math.Round(item.Quantity.Value, 3).ToDouble();
                TotAmt = TotAmt + Math.Round(item.Quantity.Value * item.Rate.Value, 2).ToDouble();

                ds.Tables[0].Rows.Add(row);
                index++;
            }

            txtQty.Text = TotQty.ToString();
            txtAmt.Text = TotAmt.ToString();
            BindGrid(ds);
        }

        private bool ValidateControls(IRecipe recipe)
        {
            bool isValid = true;

            return isValid;
        }

        private void SaveRecipe()
        {
            IRecipe recipe = new RecipeEntity();
            DataSet ds = GetGridData();
            BuildEntity(recipe);

            if (ValidateControls(recipe))
            {
                int result = RecipeBLL.SaveRecipe(recipe, (_rId > 0) ? "E" : "A", ds);
                Response.Redirect("~/Transaction/RecipeList.aspx");
            }
        }

        private void BuildEntity(IRecipe recipe)
        {
            recipe.Id = _rId;
            recipe.Type = ddlType.SelectedValue;
            if (ddlType.SelectedValue == "F")
                recipe.ItemId = Convert.ToInt32(ddlName.SelectedValue);
            else
                recipe.Name = txtName.Text;

            if (!string.IsNullOrEmpty(txtNoofPlates.Text))
                recipe.NoofPlates = Convert.ToDecimal(txtNoofPlates.Text);

            if (!string.IsNullOrEmpty(txtQty.Text))
                recipe.Quantity = Convert.ToDecimal(txtQty.Text);

            //recipe.amount = txtAmt.Text;
            recipe.RatePerUnit = txtRPU.Text;
            recipe.UOM = txtUnit.Text;

            if (!string.IsNullOrEmpty(txtIntro.Text))
                recipe.DateIntroduced = Convert.ToDateTime(txtIntro.Text);

            recipe.ProcessedAt = ddlProcessed.SelectedValue;
            recipe.RecipeDetail = txtSummary.Text;
        }

        private DataSet CreateDataSetSchema(bool allColumns)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            if (allColumns)
            {
                dt.Columns.Add(new DataColumn("Id", System.Type.GetType("System.Int32")));
                dt.Columns.Add(new DataColumn("ItemTypeDesc", System.Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("Item", System.Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("Total", System.Type.GetType("System.String")));
            }
            dt.Columns.Add(new DataColumn("pk_RecipeItemID", System.Type.GetType("System.Int64")));
            dt.Columns.Add(new DataColumn("fk_RecipeID", System.Type.GetType("System.Int64")));
            dt.Columns.Add(new DataColumn("ItemType", System.Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("fk_ItemID", System.Type.GetType("System.Int32")));
            dt.Columns.Add(new DataColumn("UOM", System.Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("QtyReq", System.Type.GetType("System.Decimal")));
            dt.Columns.Add(new DataColumn("ItemRate", System.Type.GetType("System.Decimal")));

            ds.Tables.Add(dt);
            return ds;
        }

        private void BindGrid(DataSet ds)
        {
            gvwRecipe.DataSource = ds;
            gvwRecipe.DataBind();
        }

        private void EditItem(int id)
        {
            hdnMode.Value = id.ToString();
            ddlSubItem.Items.Clear();
            txtItemUnit.Text = string.Empty;
            txtItemQty.Text = string.Empty;
            txtItemRate.Text = string.Empty;
            txtItemTot.Text = string.Empty;

            for (int j = 0; j < gvwRecipe.Rows.Count; j++)
            {
                GridViewRow row = gvwRecipe.Rows[j];
                int slNo = Convert.ToInt32(((Label)row.FindControl("lblSlNo")).Text);

                if (slNo == id)
                {
                    //dr["pk_RecipeItemID"] = ((HiddenField)row.FindControl("hdnRecipeItemId")).Value;
                    //dr["fk_RecipeID"] = ((HiddenField)row.FindControl("hdnRecipeId")).Value;
                    //dr["fk_ItemID"] = ((HiddenField)row.FindControl("hdnItemId")).Value;
                    ddlSubItemType.SelectedValue = ((HiddenField)row.FindControl("hdnType")).Value;
                    LoadSubItems();
                    if (ddlSubItemType.SelectedValue == "S")
                    {
                        ddlSubItem.SelectedValue = ((HiddenField)row.FindControl("hdnRecipeId")).Value;
                    }
                    else
                    {
                        ddlSubItem.SelectedValue = ((HiddenField)row.FindControl("hdnItemId")).Value;
                    }
                    txtItemUnit.Text = ((Label)row.FindControl("lblUnit")).Text;
                    txtItemQty.Text = ((Label)row.FindControl("lblQty")).Text;
                    txtItemRate.Text = ((Label)row.FindControl("lblRate")).Text;
                    txtItemTot.Text = ((Label)row.FindControl("lblAmt")).Text;
                }
            }
        }

        private void RemoveItem(int id)
        {
            DataSet ds = CreateDataSetSchema(true);
            int index = 0;
            for (int j = 0; j < gvwRecipe.Rows.Count; j++)
            {
                DataRow dr;
                GridViewRow row = gvwRecipe.Rows[j];
                dr = ds.Tables[0].NewRow();

                int slNo = Convert.ToInt32(((Label)row.FindControl("lblSlNo")).Text);

                if (slNo != id)
                {
                    index++;
                    LoadGridRow(row, dr, index);
                    ds.Tables[0].Rows.Add(dr);
                }
            }

            BindGrid(ds);
        }
        private void LoadGridRow(GridViewRow row, DataRow dr, int id)
        {
            dr["Id"] = id;
            dr["pk_RecipeItemID"] = Convert.ToInt64(((HiddenField)row.FindControl("hdnRecipeItemId")).Value);
            dr["fk_RecipeID"] = Convert.ToInt64(((HiddenField)row.FindControl("hdnRecipeId")).Value);
            dr["fk_ItemID"] = Convert.ToInt32(((HiddenField)row.FindControl("hdnItemId")).Value);
            dr["ItemType"] = ((HiddenField)row.FindControl("hdnType")).Value;
            dr["ItemTypeDesc"] = ((Label)row.FindControl("lblType")).Text;
            dr["Item"] = ((Label)row.FindControl("lblItem")).Text;
            dr["UOM"] = ((Label)row.FindControl("lblUnit")).Text;
            dr["QtyReq"] = Convert.ToDecimal(((Label)row.FindControl("lblQty")).Text);
            dr["ItemRate"] = Convert.ToDecimal(((Label)row.FindControl("lblRate")).Text);
            dr["Total"] = Convert.ToDecimal(((Label)row.FindControl("lblAmt")).Text);
        }

        private void DisplayItemTotal()
        {
            decimal qty = (!string.IsNullOrEmpty(txtItemQty.Text)) ? Convert.ToDecimal(txtItemQty.Text) : 0;
            decimal rate = (!string.IsNullOrEmpty(txtItemRate.Text)) ? Convert.ToDecimal(txtItemRate.Text) : 0;
            //decimal tax = (!string.IsNullOrEmpty(txtItemTax.Text)) ? Convert.ToDecimal(txtItemTax.Text) : 0;
            txtItemTot.Text = Convert.ToString(CalculateTotal(qty, rate));
            upItem.Update();
        }

        private decimal CalculateTotal(decimal qty, decimal rate)
        {
            decimal total = qty * rate;
            return Math.Round(total, 2);
        }
        #endregion

        protected void gvwRecipe_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}