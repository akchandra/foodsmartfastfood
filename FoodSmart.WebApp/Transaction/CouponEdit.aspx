﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CouponEdit.aspx.cs" Inherits="FoodSmart.WebApp.Transaction.CouponEdit" MasterPageFile="~/Site.Master" Title=":: POS :: Add / Edit Coupon" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FoodSmart.WebApp" Namespace="FoodSmart.WebApp.CustomControls" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">    
    <script type="text/javascript">
        LoadScript();
    </script>

    <script type="text/javascript">
        function validateForm() {
            var x = document.getElementById('<%= txtStartNo.ClientID %>').value;
            var y = document.getElementById('<%= txtEndNo.ClientID %>').value;
            if (Number(x) > Number(y)) {
                alert("Sorry, End number should be greater");
                document.getElementById('<%= txtEndNo.ClientID %>').value = "";
            }
        }
    </script>
    <style type="text/css">
        .style1
        {
            width: 200px;
            height: 25px;
        }
        .style2
        {
            height: 25px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="container" runat="Server">
    <div id="headercaption">ADD / EDIT COUPON</div>
    <div style="width:702px; padding-left:300px;">
        <fieldset style="width:100%;">
            <legend>Add / Edit Coupon</legend>
            <table style="height: 300px;vertical-align:top; width: 647px;">
                <tr>
                    <td>

                        <table  border="0" cellpadding="3" cellspacing="3" 
                            style="width: 105%; height: 248px">
                            <tr>
                                <td>Coupon Name:<span class="errormessage1">*</span></td>
                                <td>
                                    <asp:TextBox ID="txtName" runat="server" CssClass="textboxuppercase" TabIndex="1"
                                        MaxLength="30" Width="323px" height="20px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName" InitialValue="" ErrorMessage="This field is required*" CssClass="errormessage" ValidationGroup="Save" Display="Dynamic"></asp:RequiredFieldValidator>
                                    <span id="spnName" runat="server" class="errormessage" style="display:none;"></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="style1">Prefix:<span class="errormessage1">*</span></td>
                                <td class="style2">
                                    <asp:TextBox ID="txtPrefix" runat="server" CssClass="textboxuppercase" TabIndex="2"
                                        MaxLength="3" Width="102px" height="20px">
                                    </asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvPrefix" runat="server" ControlToValidate="txtPrefix" InitialValue="" ErrorMessage="This field is required*" CssClass="errormessage" ValidationGroup="Save" Display="Dynamic"></asp:RequiredFieldValidator>
                                    <span id="spnPrefix" runat="server" class="errormessage" style="display:none;"></span>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">Start No:<span class="errormessage1">*</span></td>
                                <td>
                                    <cc2:CustomTextBox ID="txtStartNo" runat="server" CssClass="numerictextbox" TabIndex="3" Width="102px" Height="20px"
                                        Type="Numeric" MaxLength="10" Precision="10" Scale="0">
                                    </cc2:CustomTextBox>

                                    <%--<asp:TextBox ID="txtStartNo" runat="server" CssClass="textboxuppercase" 
                                        MaxLength="30" Width="102px" height="20px">
                                    </asp:TextBox>--%>
                                    <asp:RequiredFieldValidator ID="rfvStartNo" runat="server" ControlToValidate="txtStartNo" InitialValue="" ErrorMessage="This field is required*" CssClass="errormessage" ValidationGroup="Save" Display="Dynamic"></asp:RequiredFieldValidator>
                                    <span id="spnStartNo" runat="server" class="errormessage" style="display:none;"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>End No:<span class="errormessage1">*</span></td>
                                <td>
                                    <cc2:CustomTextBox ID="txtEndNo" runat="server" CssClass="numerictextbox" TabIndex="4" Width="102px" Height="20px" onblur="validateForm();"
                                        Type="Numeric" MaxLength="10" Precision="10" Scale="0">
                                    </cc2:CustomTextBox>
                                    <%--<asp:TextBox ID="txtEndNo" runat="server" CssClass="textboxuppercase" 
                                        MaxLength="30" Width="102px" height="20px">
                                    </asp:TextBox>--%>
                                    <asp:RequiredFieldValidator ID="rfvEndNo" runat="server" ControlToValidate="txtEndNo" InitialValue="" ErrorMessage="This field is required*" CssClass="errormessage" ValidationGroup="Save" Display="Dynamic"></asp:RequiredFieldValidator>
                                    <span id="spnEndNo" runat="server" class="errormessage" style="display:none;"></span>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">Activated On:<span class="errormessage1">*</span></td>
                                <td>
                                    <asp:TextBox ID="txtActDt" runat="server" CssClass="textboxuppercase" TabIndex="5"
                                        MaxLength="30" Width="102px" height="20px" Style="text-align: right">
                                    </asp:TextBox>
                                    <cc1:CalendarExtender ID="ceActDate" TargetControlID="txtActDt" runat="server" Format="dd/MM/yyyy" Enabled="True" />
                                    <asp:RequiredFieldValidator ID="rfvActDate" runat="server" ControlToValidate="txtActDt" ErrorMessage="This field is required*" CssClass="errormessage" ValidationGroup="Save" Display="Dynamic"></asp:RequiredFieldValidator>
                                    <span id="spnActDt" runat="server" class="errormessage" style="display:none;"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>Expired On:</td>
                                <td>
                                    <asp:TextBox ID="txtExpDt" runat="server" CssClass="textboxuppercase" TabIndex="6"
                                        MaxLength="30" Width="102px" height="20px" Style="text-align: right">
                                    </asp:TextBox>
                                    <cc1:CalendarExtender ID="ceExpDate" TargetControlID="txtExpDt" runat="server" Format="dd/MM/yyyy" Enabled="True" />
<%--                                    <asp:RequiredFieldValidator ID="rfvExpDate" runat="server" ControlToValidate="txtExpDt" ErrorMessage="This field is required*" CssClass="errormessage" ValidationGroup="Save" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                                    <span id="spnExpDt" runat="server" class="errormessage" style="display:none;"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>Discount Amount:<span class="errormessage1">*</span></td>
                                <td>
                                    <cc2:CustomTextBox ID="txtCPDisc" runat="server" CssClass="numerictextbox" TabIndex="7" Width="102px" Type="Decimal" MaxLength="13" Precision="10" Scale="2">
                                    </cc2:CustomTextBox>
                                    <asp:RequiredFieldValidator ID="rfvCPDisc" runat="server" ControlToValidate="txtCPDisc" InitialValue="" ErrorMessage="This field is required*" CssClass="errormessage" ValidationGroup="Save" Display="Dynamic"></asp:RequiredFieldValidator>
                                    <span id="spnCPDisc" runat="server" class="errormessage" style="display:none;"></span>
                                </td>
                            </tr>

                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="display:none" class="style1">
                        <br />
                    </td>
                </tr>                
                <tr>
                    <td colspan="2">
                        <asp:Button ID="btnSave" runat="server" Text="Save" ValidationGroup="Save" OnClick="btnSave_Click" />&nbsp;&nbsp;
                        <asp:Button ID="btnBack" runat="server" Text="Back"/>&nbsp;&nbsp;<asp:Label ID="msgbox" runat="server" Text="" ForeColor="Red" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </div>
</asp:Content>