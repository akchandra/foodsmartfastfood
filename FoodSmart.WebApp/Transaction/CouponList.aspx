﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CouponList.aspx.cs" Inherits="FoodSmart.WebApp.Transaction.CouponList" MasterPageFile="~/Site.Master" Title=":: POS :: Coupon Master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style1
        {
            width: 244px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="container" runat="Server">
    <div id="dvAsync" style="padding: 5px; display: none;">
        <div class="asynpanel">
            <div id="dvAsyncClose">
                <img alt="" src="../../Images/Close-Button.bmp" style="cursor: pointer;" onclick="ClearErrorState()" /></div>
            <div id="dvAsyncMessage">
            </div>
        </div>
    </div>
    <div id="headercaption">MANAGE COUPON&nbsp;&nbsp;&nbsp; </div>
    <div style="width:850px;margin:0 auto;margin-top:30px;">        
        <fieldset style="width:100%;">
            <legend>Search Coupon</legend>
            <table style="width: 719px">
                <tr>
                    <td style="margin-left: 40px;" class="style1">
                        <asp:TextBox ID="txtPrefix" runat="server" CssClass="watermark" Width="232px" ForeColor="#747862" Height="23px"></asp:TextBox>
                        <cc1:TextBoxWatermarkExtender ID="txtWMEPrefix" runat="server" TargetControlID="txtPrefix"  WatermarkText="Type Coupon Prefix" WatermarkCssClass="watermark"></cc1:TextBoxWatermarkExtender>
                    </td>
                    <td style="margin-left: 40px;" class="style1">
                        <asp:TextBox ID="txtName" runat="server" CssClass="watermark" Width="232px" ForeColor="#747862" Height="23px"></asp:TextBox>
                        <cc1:TextBoxWatermarkExtender ID="txtWMEName" runat="server" TargetControlID="txtName"  WatermarkText="Type Coupon Name" WatermarkCssClass="watermark"></cc1:TextBoxWatermarkExtender>
                    </td>
                   
                    <td><asp:Button ID="btnSearch" runat="server" Text="Search" Width="100px" OnClick="btnSearch_Click" /></td>
                    <td><asp:Button ID="btnReset" runat="server" Text="Reset" Width="100px" OnClick="btnReset_Click" /></td>
                </tr>
            </table>              
        </fieldset>
        <asp:UpdateProgress ID="uProgressCP" runat="server" AssociatedUpdatePanelID="upCP">
            <ProgressTemplate>
                <div class="progress">
                    <div id="image">
                        <img src="../../Images/PleaseWait.gif" alt="" /></div>
                    <div id="text">
                        Please Wait...</div>
                </div>
            </ProgressTemplate>        
        </asp:UpdateProgress>
        <fieldset id="fsList" runat="server" style="width:100%;min-height:100px;margin-top:10px;">
            <legend>Coupon List</legend>
            <div style="float:right;padding-bottom:5px;">                
                Results Per Page:<asp:DropDownList ID="ddlPaging" runat="server" Width="75px" AutoPostBack="true" CssClass="chzn-select medium-select single"
                    OnSelectedIndexChanged="ddlPaging_SelectedIndexChanged">
                    <asp:ListItem Text="10" Value="10" />
                    <asp:ListItem Text="30" Value="30" />
                    <asp:ListItem Text="50" Value="50" />
                    <asp:ListItem Text="100" Value="100" />
                </asp:DropDownList>&nbsp;&nbsp;            
                <asp:Button ID="btnAdd" runat="server" Text="Add New Coupon" Width="130px" OnClick="btnAdd_Click" />
            </div>
            <div>
                <%--<span class="errormessage">* Indicates Inactive User(s)</span>--%>
            </div><br />            
            <div>
                <asp:UpdatePanel ID="upCP" runat="server" UpdateMode="Conditional">
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ddlPaging" EventName="SelectedIndexChanged" />
                    </Triggers>
                    <ContentTemplate>
                        <script type="text/javascript">
                            Sys.Application.add_load(LoadScript);
                        </script>
                        <asp:GridView ID="gvwItem" runat="server" AutoGenerateColumns="false" AllowPaging="true" BorderStyle="None" BorderWidth="0" OnPageIndexChanging="gvwItem_PageIndexChanging" OnRowDataBound="gvwItem_RowDataBound" OnRowCommand="gvwItem_RowCommand" Width="100%">
                        <PagerSettings Mode="NumericFirstLast" Position="Bottom" />
                        <PagerStyle CssClass="gridviewpager" />
                        <EmptyDataRowStyle CssClass="gridviewemptydatarow" />
                        <EmptyDataTemplate>No Record Found</EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="Sl#">
                                <HeaderStyle CssClass="gridviewheader" />
                                <ItemStyle CssClass="gridviewitem" Width="5%" />                                    
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="gridviewheader" />
                                <ItemStyle CssClass="gridviewitem" Width="5%" />           
                                <HeaderTemplate><asp:LinkButton ID="lnkHPrefix" runat="server" CommandName="Sort" CommandArgument="CouponPrefix" Text="Prefix"></asp:LinkButton></HeaderTemplate>                         
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="gridviewheader" />
                                <ItemStyle CssClass="gridviewitem" Width="10%" />   
                                <HeaderTemplate><asp:LinkButton ID="lnkHSSrl" runat="server" CommandName="Sort" CommandArgument="CouponStartSerial" Text="Start No."></asp:LinkButton></HeaderTemplate>                         
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="gridviewheader" />
                                <ItemStyle CssClass="gridviewitem" Width="10%" />   
                                <HeaderTemplate><asp:LinkButton ID="lnkHESrl" runat="server" CommandName="Sort" CommandArgument="CouponEndSerial" Text="End No."></asp:LinkButton></HeaderTemplate>                         
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="gridviewheader" />
                                <ItemStyle CssClass="gridviewitem" Width="25%" />   
                                <HeaderTemplate><asp:LinkButton ID="lnkHCPName" runat="server" CommandName="Sort" CommandArgument="CouponName" Text="Coupon Name"></asp:LinkButton></HeaderTemplate>                         
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="gridviewheader_num" />
                                <ItemStyle CssClass="gridviewitem" Width="15%" HorizontalAlign="Right"/>   
                                <HeaderTemplate><asp:LinkButton ID="lnkHCPAmt" runat="server" CommandName="Sort" CommandArgument="CouponAmount" Text="Coupon Disc"></asp:LinkButton></HeaderTemplate>                         
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="gridviewheader" />
                                <ItemStyle CssClass="gridviewitem" Width="20%" />   
                                <HeaderTemplate><asp:LinkButton ID="lnkHActDT" runat="server" CommandName="Sort" CommandArgument="ActivateDate" Text="Activated On"></asp:LinkButton></HeaderTemplate>                         
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="gridviewheader" />
                                <ItemStyle CssClass="gridviewitem" Width="35%" />   
                                <HeaderTemplate><asp:LinkButton ID="lnkHExpDT" runat="server" CommandName="Sort" CommandArgument="ExpiryDate" Text="Expired On"></asp:LinkButton></HeaderTemplate>                         
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="gridviewheader" />
                                <ItemStyle CssClass="gridviewitem" Width="0%" />   
                                <HeaderTemplate><asp:LinkButton ID="lnkHCPID" runat="server" CommandName="Sort" CommandArgument="pk_CouponID" Text=""></asp:LinkButton></HeaderTemplate>                         
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="gridviewheader" />
                                <ItemStyle CssClass="gridviewitem" Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />                                    
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnEdit" runat="server" CommandName="Edit" ImageUrl="~/Images/edit.png" Height="16" Width="16" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderStyle CssClass="gridviewheader" />
                                <ItemStyle CssClass="gridviewitem" Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />                                    
                                <ItemTemplate>
                                    <asp:ImageButton ID="btnRemove" runat="server" CommandName="Remove" ImageUrl="~/Images/remove.png" Height="16" Width="16" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </fieldset>
    </div>
    <script type = "text/javascript">
        function Confirm(msg) 
        {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm(msg)) 
            {
                confirm_value.value = "Yes";
            } 
            else 
            {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }
    </script>
</asp:Content>