﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Pricelist.aspx.cs" Inherits="FoodSmart.WebApp.Transaction.Pricelist" MasterPageFile="~/Site.Master" Title=":: FoodSmart :: Pricelist" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FoodSmart.WebApp" Namespace="FoodSmart.WebApp.CustomControls" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="container" runat="Server">
    <div id="dvAsync" style="padding: 5px; display: none;">
        <div class="asynpanel">
            <div id="dvAsyncClose">
                <img alt="" src="../../Images/Close-Button.bmp" style="cursor: pointer;" onclick="ClearErrorState()" />
            </div>
            <div id="dvAsyncMessage">
                An error has occurred. Please try again
            </div>
        </div>
    </div>
    <div id="headercaption">MANAGE PRICE</div>
    <div style="width: 950px; margin: 0 auto; margin-top: 30px;">
        <asp:UpdateProgress ID="uProgressSearch" runat="server" AssociatedUpdatePanelID="upSearch">
            <ProgressTemplate>
                <div class="progress">
                    <div id="image">
                        <img src="../../Images/PleaseWait.gif" alt="" />
                    </div>
                    <div id="text">
                        Please Wait...
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:UpdateProgress ID="uProgressPriceList" runat="server" AssociatedUpdatePanelID="upPriceList">
            <ProgressTemplate>
                <div class="progress">
                    <div id="image">
                        <img src="../../Images/PleaseWait.gif" alt="" />
                    </div>
                    <div id="text">
                        Please Wait...
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:UpdatePanel ID="upSearch" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <fieldset style="width: 100%;">
                    <legend>Search</legend>
                    <table>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtItem" runat="server" CssClass="watermark" Width="240px" ForeColor="#747862"></asp:TextBox>
                                <cc1:TextBoxWatermarkExtender ID="txtWMEItem" runat="server" TargetControlID="txtItem" WatermarkText="Item" WatermarkCssClass="watermark"></cc1:TextBoxWatermarkExtender>
                            </td>

                            <td>
                                <asp:DropDownList ID="ddlGroup" runat="server" Width="200px" AutoPostBack="true" 
                                    onselectedindexchanged="ddlGroup_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>

                            <%--<td>
                                <asp:DropDownList ID="ddlGroup" runat="server" Width="75px" AutoPostBack="false" CssClass="chzn-select medium-select single">
                                    <asp:ListItem Text="All" Value="A" />
                                    <asp:ListItem Text="Receipt" Value="R" />
                                    <asp:ListItem Text="Issue" Value="I" />
                                </asp:DropDownList>
                            </td>--%>
                            <td>
                                <asp:TextBox ID="txtDate" runat="server" CssClass="watermark" MaxLength="10" Width="120px" Height="20px" Style="text-align: right"></asp:TextBox>
                                <cc1:CalendarExtender ID="ceDate" TargetControlID="txtDate" runat="server" Format="dd/MM/yyyy" Enabled="True" />
                                <cc1:TextBoxWatermarkExtender ID="txtWMEDate" runat="server" TargetControlID="txtDate" WatermarkText="Date" WatermarkCssClass="watermark"></cc1:TextBoxWatermarkExtender>
                                <span id="spnDate" runat="server" class="errormessage" style="display:none;">Please select date</span>
                            </td>
                            <td>
                                <asp:Button ID="btnSearch" runat="server" Text="Search" Width="100px" OnClick="btnSearch_Click" />&nbsp;<asp:Button ID="btnReset" runat="server" Text="Reset" Width="100px" OnClick="btnReset_Click" />
                            </td>
                        </tr>
                    </table>
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="lblNewDate" runat="server" Text="New date:"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtNewDate" runat="server" CssClass="textboxuppercase" TabIndex="5" MaxLength="30" Width="102px" Height="20px" Style="text-align: right"></asp:TextBox>
                                <cc1:CalendarExtender ID="ceNewRate" TargetControlID="txtNewDate" runat="server" Format="dd/MM/yyyy" Enabled="True" />
                                <span id="spnNewDate" runat="server" class="errormessage" style="display:none;">Please select date</span>
                            </td>
                            <td>
                                <asp:HiddenField runat="server" ID="hdnMode" />
                                <asp:Button ID="btnNew" runat="server" Text="New rate" Width="100px" OnClick="btnNew_Click" />&nbsp;
                                <asp:Button ID="btnUpdate" runat="server" Text="Update rate" Width="100px" OnClick="btnUpdate_Click" />&nbsp;
                                <asp:Button ID="btnSave" runat="server" Text="Save" Width="100px" OnClick="btnSave_Click" />&nbsp;
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="100px" OnClick="btnCancel_Click" />&nbsp;
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </ContentTemplate>
        </asp:UpdatePanel>
        <fieldset id="fsList" runat="server" style="width: 100%; min-height: 100px; margin-top: 10px;">
            <legend>Pricelist</legend>
            <div style="float: right; padding-bottom: 5px;">
                Results Per Page:<asp:DropDownList ID="ddlPaging" runat="server" Width="75px" AutoPostBack="true" CssClass="chzn-select medium-select single"
                    OnSelectedIndexChanged="ddlPaging_SelectedIndexChanged">
                    <asp:ListItem Text="10" Value="10" />
                    <asp:ListItem Text="30" Value="30" />
                    <asp:ListItem Text="50" Value="50" />
                    <asp:ListItem Text="100" Value="100" />
                </asp:DropDownList>
            </div>
            <br />
            <div>
                <asp:UpdatePanel ID="upPriceList" runat="server" UpdateMode="Conditional">
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ddlPaging" EventName="SelectedIndexChanged" />
                    </Triggers>
                    <ContentTemplate>
                        <script type="text/javascript">
                            Sys.Application.add_load(LoadScript);
                        </script>
                        <asp:GridView ID="gvwPrice" runat="server" AutoGenerateColumns="false" AllowPaging="true" BorderStyle="None" BorderWidth="0" OnPageIndexChanging="gvwPrice_PageIndexChanging" OnRowDataBound="gvwPrice_RowDataBound" OnRowCommand="gvwPrice_RowCommand" Width="100%">
                            <PagerSettings Mode="NumericFirstLast" Position="Bottom" />
                            <PagerStyle CssClass="gridviewpager" />
                            <EmptyDataRowStyle CssClass="gridviewemptydatarow" />
                            <EmptyDataTemplate>No items(s) Found</EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="Sl#">
                                    <HeaderStyle CssClass="gridviewheader" />
                                    <ItemStyle CssClass="gridviewitem" Width="5%" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblSlNo" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Item">
                                    <HeaderStyle CssClass="gridviewheader" />
                                    <ItemStyle CssClass="gridviewitem" Width="25%" />
                                    <ItemTemplate>
                                        <asp:HiddenField runat="server" ID="hdnItemId" />
                                        <asp:HiddenField runat="server" ID="hdnRateId" />
                                        <asp:Label ID="lblItem" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Item group">
                                    <HeaderStyle CssClass="gridviewheader" />
                                    <ItemStyle CssClass="gridviewitem" Width="25%" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblGroup" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="UOM">
                                    <HeaderStyle CssClass="gridviewheader" />
                                    <ItemStyle CssClass="gridviewitem" Width="10%" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblUOM" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="GST %">
                                    <HeaderStyle CssClass="gridviewheader_right" />
                                    <ItemStyle CssClass="gridviewitem" Width="10%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblGST" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Existing rate">
                                    <HeaderStyle CssClass="gridviewheader_right" />
                                    <ItemStyle CssClass="gridviewitem" Width="10%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblExistingRate" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="New rate">
                                    <HeaderStyle CssClass="gridviewheader_right" />
                                    <ItemStyle CssClass="gridviewitem" Width="15%" />
                                    <ItemTemplate>
                                        <cc2:CustomTextBox ID="txtNewRate" runat="server" CssClass="numerictextbox" Width="100%" Height="20px" Type="Numeric" MaxLength="10" Precision="10" Scale="2"></cc2:CustomTextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </fieldset>
    </div>
</asp:Content>
