﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TranList.aspx.cs" Inherits="FoodSmart.WebApp.Transaction.TranList" MasterPageFile="~/Site.Master" Title=":: FoodSmart :: Transaction Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="container" runat="Server">
    <div id="dvAsync" style="padding: 5px; display: none;">
        <div class="asynpanel">
            <div id="dvAsyncClose">
                <img alt="" src="../../Images/Close-Button.bmp" style="cursor: pointer;" onclick="ClearErrorState()" />
            </div>
            <div id="dvAsyncMessage">An error has occurred. Please try again
            </div>
        </div>
    </div>
    <div id="headercaption">MANAGE TRANSACTION</div>
    <div style="width: 950px; margin: 0 auto; margin-top: 30px;">
        <fieldset style="width: 100%;">
            <legend>Search transaction</legend>
            <table>
                <tr>
                    <td>Select type:</td>
                    <td>
                        <asp:DropDownList ID="ddlType" runat="server" Width="125px" AutoPostBack="false" 
                            CssClass="chzn-select medium-select single" 
                            onselectedindexchanged="ddlType_SelectedIndexChanged">
                            <asp:ListItem Text="All" Value="A" />
                            <asp:ListItem Text="Purchase" Value="P" />
                            <asp:ListItem Text="Transfer" Value="T" />
                            <asp:ListItem Text="Wastage" Value="W" />
                            <asp:ListItem Text="Purchase return" Value="R" />
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:TextBox ID="txtRef" runat="server" CssClass="watermark" Width="240px" ForeColor="#747862"></asp:TextBox>
                        <cc1:TextBoxWatermarkExtender ID="txtWMERef" runat="server" TargetControlID="txtRef" WatermarkText="Vendor ref" WatermarkCssClass="watermark"></cc1:TextBoxWatermarkExtender>
                    </td>

                    <td>
                        <asp:TextBox ID="txtTranDate" runat="server" MaxLength="30" Width="102px" ForeColor="#747862" ></asp:TextBox>
                        <cc1:CalendarExtender ID="cdTranDate" TargetControlID="txtTranDate" runat="server" Format="dd/MM/yyyy" Enabled="True" />
                        <cc1:TextBoxWatermarkExtender ID="txtwmRef" runat="server" TargetControlID="txtTranDate" WatermarkText="Tran Date" WatermarkCssClass="watermark"></cc1:TextBoxWatermarkExtender>

                    </td>

                    <td>
                        <asp:Button ID="btnSearch" runat="server" Text="Search" Width="100px" OnClick="btnSearch_Click" />&nbsp;<asp:Button ID="btnReset" runat="server" Text="Reset" Width="100px" OnClick="btnReset_Click" />
                    </td>
                </tr>
            </table>
        </fieldset>
        <asp:UpdateProgress ID="uProgressTran" runat="server" AssociatedUpdatePanelID="upTran">
            <ProgressTemplate>
                <div class="progress">
                    <div id="image">
                        <img src="../../Images/PleaseWait.gif" alt="" />
                    </div>
                    <div id="text">
                        Please Wait...
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <fieldset id="fsList" runat="server" style="width: 100%; min-height: 100px; margin-top: 10px;">
            <legend>Transaction list</legend>
            <div style="float: right; padding-bottom: 5px;">
                Results Per Page:<asp:DropDownList ID="ddlPaging" runat="server" Width="75px" AutoPostBack="true" CssClass="chzn-select medium-select single"
                    OnSelectedIndexChanged="ddlPaging_SelectedIndexChanged">
                    <asp:ListItem Text="10" Value="10" />
                    <asp:ListItem Text="30" Value="30" />
                    <asp:ListItem Text="50" Value="50" />
                    <asp:ListItem Text="100" Value="100" />
                </asp:DropDownList>&nbsp;&nbsp;            
                <asp:Button ID="btnAdd" runat="server" Text="Add" Width="130px" OnClick="btnAdd_Click" />
            </div>
            <br />
            <div>
                <asp:UpdatePanel ID="upTran" runat="server" UpdateMode="Conditional">
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ddlPaging" EventName="SelectedIndexChanged" />
                    </Triggers>
                    <ContentTemplate>
                        <script type="text/javascript">
                            Sys.Application.add_load(LoadScript);
                        </script>
                        <asp:GridView ID="gvwTran" runat="server" AutoGenerateColumns="false" AllowPaging="true" BorderStyle="None" BorderWidth="0" OnPageIndexChanging="gvwTran_PageIndexChanging" OnRowDataBound="gvwTran_RowDataBound" OnRowCommand="gvwTran_RowCommand" Width="100%">
                            <PagerSettings Mode="NumericFirstLast" Position="Bottom" />
                            <PagerStyle CssClass="gridviewpager" />
                            <EmptyDataRowStyle CssClass="gridviewemptydatarow" />
                            <EmptyDataTemplate>No transaction(s) Found</EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="Sl#">
                                    <HeaderStyle CssClass="gridviewheader" />
                                    <ItemStyle CssClass="gridviewitem" Width="5%" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="gridviewheader" />
                                    <ItemStyle CssClass="gridviewitem" Width="10%" />
                                    <HeaderTemplate>
                                        <asp:LinkButton ID="lnkHType" runat="server" CommandName="Sort" CommandArgument="Type" Text="Type"></asp:LinkButton>
                                    </HeaderTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="gridviewheader" />
                                    <ItemStyle CssClass="gridviewitem" Width="15%" />
                                    <HeaderTemplate>
                                        <asp:LinkButton ID="lnkHNo" runat="server" CommandName="Sort" CommandArgument="Number" Text="Transaction no"></asp:LinkButton>
                                    </HeaderTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="gridviewheader" />
                                    <ItemStyle CssClass="gridviewitem" Width="10%" />
                                    <HeaderTemplate>
                                        <asp:LinkButton ID="lnkHDate" runat="server" CommandName="Sort" CommandArgument="TrnDate" Text="Transaction date"></asp:LinkButton>
                                    </HeaderTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="gridviewheader" />
                                    <ItemStyle CssClass="gridviewitem" Width="15%" />
                                    <HeaderTemplate>
                                        <asp:LinkButton ID="lnkHVendorRef" runat="server" CommandName="Sort" CommandArgument="VendorRef" Text="Vendor reference"></asp:LinkButton>
                                    </HeaderTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="gridviewheader" />
                                    <ItemStyle CssClass="gridviewitem" Width="12%" />
                                    <HeaderTemplate>
                                        <asp:LinkButton ID="lnkHFronLoc" runat="server" CommandName="Sort" CommandArgument="FromLoc" Text="From"></asp:LinkButton>
                                    </HeaderTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="gridviewheader" />
                                    <ItemStyle CssClass="gridviewitem" Width="12%" />
                                    <HeaderTemplate>
                                        <asp:LinkButton ID="lnkHToLoc" runat="server" CommandName="Sort" CommandArgument="ToLoc" Text="To"></asp:LinkButton>
                                    </HeaderTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="gridviewheader" HorizontalAlign="Right"/>
                                    <ItemStyle CssClass="gridviewitem" Width="11%" />
                                    <HeaderTemplate>
                                        <asp:LinkButton ID="lnkHValue" runat="server" CommandName="Sort" CommandArgument="TranValue" Text="Transaction value"></asp:LinkButton>
                                    </HeaderTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="gridviewheader" />
                                    <ItemStyle CssClass="gridviewitem" Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnEdit" runat="server" CommandName="Edit" ImageUrl="~/Images/edit.png" Height="16" Width="16" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="gridviewheader" />
                                    <ItemStyle CssClass="gridviewitem" Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnRemove" runat="server" CommandName="Remove" ImageUrl="~/Images/remove.png" Height="16" Width="16" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </fieldset>
    </div>
</asp:Content>
