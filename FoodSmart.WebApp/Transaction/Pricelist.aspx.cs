﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FoodSmart.BLL;
using FoodSmart.Common;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using FoodSmart.Utilities.ResourceManager;
using FoodSmart.WebApp.CustomControls;

namespace FoodSmart.WebApp.Transaction
{
    public partial class Pricelist : System.Web.UI.Page
    {
        #region Private Member Variables

        private int _userId = 0;
        private bool _canAdd = false;
        private bool _canEdit = false;
        private bool _canDelete = false;
        private bool _canView = false;

        #endregion

        #region Protected Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            RetriveParameters();
            //CheckUserAccess();
            SetAttributes();

            if (!IsPostBack)
            {
                txtDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                LoadItemGroup();
                ToggleSaveButtons(true);
                RetrieveSearchCriteria();
                LoadPricelist();
                TogglePriceModification(false);
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            hdnMode.Value = string.Empty;
            SaveNewPageIndex(0);
            LoadPricelist();
            TogglePriceModification(false);
            ToggleSaveButtons(true);
            upPriceList.Update();
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            hdnMode.Value = string.Empty;
            ddlGroup.SelectedValue = "0";
            txtItem.Text = string.Empty;
            txtDate.Text = string.Empty;
            SaveNewPageIndex(0);
            LoadPricelist();
            TogglePriceModification(false);
            ToggleSaveButtons(true);
            upPriceList.Update();
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            hdnMode.Value = "A";
            ToggleSaveButtons(false);
            TogglePriceModification(true);
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            hdnMode.Value = "E";
            ToggleSaveButtons(false);
            TogglePriceModification(true);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            SavePriceList(hdnMode.Value);
            hdnMode.Value = String.Empty;
            ToggleSaveButtons(true);
            TogglePriceModification(false);
            LoadPricelist();
            ScriptManager.RegisterStartupScript(this, typeof(Page), "alert", "<script>javascript:void alert('Rate saved successfully');</script>", false);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            hdnMode.Value = String.Empty;
            ToggleSaveButtons(true);
            TogglePriceModification(false);
        }

        protected void gvwPrice_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            int newIndex = e.NewPageIndex;
            gvwPrice.PageIndex = e.NewPageIndex;
            SaveNewPageIndex(e.NewPageIndex);
            LoadPricelist();
        }
        protected void gvwPrice_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Sort"))
            {
                if (ViewState[Constants.SORT_EXPRESSION] == null)
                {
                    ViewState[Constants.SORT_EXPRESSION] = e.CommandArgument.ToString();
                    ViewState[Constants.SORT_DIRECTION] = "ASC";
                }
                else
                {
                    if (ViewState[Constants.SORT_EXPRESSION].ToString() == e.CommandArgument.ToString())
                    {
                        if (ViewState[Constants.SORT_DIRECTION].ToString() == "ASC")
                            ViewState[Constants.SORT_DIRECTION] = "DESC";
                        else
                            ViewState[Constants.SORT_DIRECTION] = "ASC";
                    }
                    else
                    {
                        ViewState[Constants.SORT_DIRECTION] = "ASC";
                        ViewState[Constants.SORT_EXPRESSION] = e.CommandArgument.ToString();
                    }
                }

                LoadPricelist();
            }
        }

        protected void gvwPrice_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                GeneralFunctions.ApplyGridViewAlternateItemStyle(e.Row, 7);

                ScriptManager sManager = ScriptManager.GetCurrent(this);

                ((Label)e.Row.Cells[0].FindControl("lblSlNo")).Text = ((gvwPrice.PageSize * gvwPrice.PageIndex) + e.Row.RowIndex + 1).ToString();
                ((Label)e.Row.Cells[0].FindControl("lblItem")).Text = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "ItemName"));

                ((HiddenField)e.Row.Cells[0].FindControl("hdnItemId")).Value = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "ItemId"));
                ((HiddenField)e.Row.Cells[0].FindControl("hdnRateId")).Value = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Id"));

                ((Label)e.Row.Cells[0].FindControl("lblGroup")).Text = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "ItemGroupName"));
                ((Label)e.Row.Cells[0].FindControl("lblUOM")).Text = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "UOM"));
                ((Label)e.Row.Cells[0].FindControl("lblGST")).Text = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "GST"));
                ((Label)e.Row.Cells[0].FindControl("lblExistingRate")).Text = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "SellingRate"));
                ((CustomTextBox)e.Row.Cells[0].FindControl("txtNewRate")).Text = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "NewRate"));

            }
        }

        protected void ddlPaging_SelectedIndexChanged(object sender, EventArgs e)
        {
            int newPageSize = Convert.ToInt32(ddlPaging.SelectedValue);
            SaveNewPageSize(newPageSize);
            LoadPricelist();
            upPriceList.Update();
        }

        #endregion

        #region Private Methods

        private void LoadItemGroup()
        {
            DataSet ds = new DataSet();

            DataTable dt = new DataTable();

            SearchCriteria searchcriteria = new SearchCriteria();
            searchcriteria.QueryStat = "L";
            searchcriteria.RorF = "F";
            //searchcriteria.RorF = ddlRorF.SelectedValue.ToString();

            ddlGroup.DataSource = null;

            ds = new ItemBLL().GetAllItemGroup(searchcriteria);
            dt = ds.Tables[0];

            DataRow nullrow = dt.NewRow();
            nullrow["pk_ItemGroupID"] = 0;
            nullrow["descr"] = "All Groups";
            dt.Rows.Add(nullrow);
            if (dt.Rows.Count > 0)
            {
                ddlGroup.DataSource = dt;
                ddlGroup.DataTextField = "descr";
                ddlGroup.DataValueField = "pk_ItemGroupID";
                ddlGroup.DataBind();
                ddlGroup.SelectedValue = "0";
            }
        }

        private void RetriveParameters()
        {
            _userId = UserBLL.GetLoggedInUserId();

            //Get user permission.
            UserBLL.GetUserPermission(out _canAdd, out _canEdit, out _canDelete, out _canView);
        }

        private void CheckUserAccess()
        {
            if (!ReferenceEquals(Session[Constants.SESSION_USER_INFO], null))
            {
                IUser user = (IUser)Session[Constants.SESSION_USER_INFO];

                if (ReferenceEquals(user, null) || user.Id == 0)
                {
                    Response.Redirect("~/Login.aspx");
                }

                if (user.RoleID != (int)UserRole.Admin)
                {
                    Response.Redirect("~/Unauthorized.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Login.aspx");
            }

            if (!_canView)
            {
                Response.Redirect("~/Unauthorized.aspx");
            }
        }

        private void SetAttributes()
        {
            spnDate.Style["display"] = "none";
            spnNewDate.Style["display"] = "none";

            if (!IsPostBack)
            {
                txtWMEItem.WatermarkText = ResourceManager.GetStringWithoutName("R00068");
                txtWMEDate.WatermarkText = ResourceManager.GetStringWithoutName("R00067");
                btnSearch.ToolTip = ResourceManager.GetStringWithoutName("R00055");
                btnReset.ToolTip = ResourceManager.GetStringWithoutName("R00054");
                gvwPrice.PageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
                gvwPrice.PagerSettings.PageButtonCount = Convert.ToInt32(ConfigurationManager.AppSettings["PageButtonCount"]);
            }
        }

        private void DeleteTransaction(Int64 id)
        {
            string message = string.Empty;

            int result = TransactionBLL.DeleteTransaction(id);

            if (result == 0)
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "alert", "<script>javascript:void alert('Transaction deleted successfully');</script>", false);
                LoadPricelist();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "alert", "<script>javascript:void alert('Unable to delete transaction');</script>", false);
            }
        }

        private void LoadPricelist()
        {
            if (!ReferenceEquals(Session[Constants.SESSION_SEARCH_CRITERIA], null))
            {
                SearchCriteria searchCriteria = (SearchCriteria)Session[Constants.SESSION_SEARCH_CRITERIA];

                if (!ReferenceEquals(searchCriteria, null))
                {
                    BuildSearchCriteria(searchCriteria);
                    gvwPrice.PageIndex = searchCriteria.PageIndex;
                    if (searchCriteria.PageSize > 0) gvwPrice.PageSize = searchCriteria.PageSize;

                    gvwPrice.DataSource = PricelistBLL.GetPriceList(searchCriteria);
                    gvwPrice.DataBind();
                }
            }
        }

        private void BuildSearchCriteria(SearchCriteria criteria)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (!ReferenceEquals(ViewState[Constants.SORT_EXPRESSION], null) && !ReferenceEquals(ViewState[Constants.SORT_DIRECTION], null))
            {
                sortExpression = Convert.ToString(ViewState[Constants.SORT_EXPRESSION]);
                sortDirection = Convert.ToString(ViewState[Constants.SORT_DIRECTION]);
            }
            else
            {
                sortExpression = "Type";
                sortDirection = "ASC";
            }

            criteria.FinancialYearId = 1;
            criteria.SortExpression = sortExpression;
            criteria.SortDirection = sortDirection;
            criteria.IntegerOption3 = Convert.ToInt32(ddlGroup.SelectedValue);

            if (!string.IsNullOrEmpty(txtDate.Text))
                criteria.Date = Convert.ToDateTime(txtDate.Text);

            Session[Constants.SESSION_SEARCH_CRITERIA] = criteria;
        }

        private void RetrieveSearchCriteria()
        {
            bool isCriteriaExists = false;

            if (!ReferenceEquals(Session[Constants.SESSION_SEARCH_CRITERIA], null))
            {
                SearchCriteria criteria = (SearchCriteria)Session[Constants.SESSION_SEARCH_CRITERIA];

                if (!ReferenceEquals(criteria, null))
                {
                    if (criteria.CurrentPage != PageName.UserMaster)
                    {
                        criteria.Clear();
                        SetDefaultSearchCriteria(criteria);
                    }
                    else
                    {
                        //txtRef.Text = criteria.VendorName;
                        txtDate.Text = criteria.Date.Value.ToString("dd/MM/yyyy");
                        gvwPrice.PageIndex = criteria.PageIndex;
                        gvwPrice.PageSize = criteria.PageSize;
                        ddlPaging.SelectedValue = criteria.PageSize.ToString();
                        isCriteriaExists = true;
                    }
                }
            }

            if (!isCriteriaExists)
            {
                SearchCriteria newcriteria = new SearchCriteria();
                SetDefaultSearchCriteria(newcriteria);
            }
        }

        private void SetDefaultSearchCriteria(SearchCriteria criteria)
        {
            string sortExpression = string.Empty;
            string sortDirection = string.Empty;

            if (!string.IsNullOrEmpty(txtDate.Text))
            {
                criteria.Date = Convert.ToDateTime(txtDate.Text);
            }
            criteria.SortExpression = sortExpression;
            criteria.SortDirection = sortDirection;
            criteria.CurrentPage = PageName.UserMaster;
            criteria.PageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
            Session[Constants.SESSION_SEARCH_CRITERIA] = criteria;
        }

        private void SaveNewPageIndex(int newIndex)
        {
            if (!ReferenceEquals(Session[Constants.SESSION_SEARCH_CRITERIA], null))
            {
                SearchCriteria criteria = (SearchCriteria)Session[Constants.SESSION_SEARCH_CRITERIA];

                if (!ReferenceEquals(criteria, null))
                {
                    criteria.PageIndex = newIndex;
                }
            }
        }

        private void SaveNewPageSize(int newPageSize)
        {
            if (!ReferenceEquals(Session[Constants.SESSION_SEARCH_CRITERIA], null))
            {
                SearchCriteria criteria = (SearchCriteria)Session[Constants.SESSION_SEARCH_CRITERIA];

                if (!ReferenceEquals(criteria, null))
                {
                    criteria.PageSize = newPageSize;
                }
            }
        }

        private void ToggleSaveButtons(bool visible)
        {
            btnNew.Visible = visible;
            btnUpdate.Visible = visible;
            btnSave.Visible = !visible;
            btnCancel.Visible = !visible;
            lblNewDate.Visible = !visible;
            txtNewDate.Visible = !visible;

            if (hdnMode.Value == "E")
            {
                txtNewDate.Enabled = false;
                txtDate.Enabled = false;
            }
            else if(hdnMode.Value == "A")
            {
                txtNewDate.Enabled = true;
                txtDate.Enabled = false;
            }
            else
            {
                txtNewDate.Enabled = true;
                txtDate.Enabled = true;
            }
        }

        private void SavePriceList(string mode)
        {
            DataSet ds = CreateDataSetSchema();
            BuildEntity(ds);
            DateTime effectiveDate = (hdnMode.Value == "A") ? Convert.ToDateTime(txtNewDate.Text) : Convert.ToDateTime(txtDate.Text);
            PricelistBLL.SavePriceList(mode, effectiveDate, ds);
        }

        private void BuildEntity(DataSet ds)
        {
            for (int i = 0; i < gvwPrice.Rows.Count; i++)
            {
                DataRow dr;
                GridViewRow row = gvwPrice.Rows[i];
                dr = ds.Tables[0].NewRow();

                if (hdnMode.Value == "E")
                {
                    dr["Id"] = Convert.ToInt64(((HiddenField)row.FindControl("hdnRateId")).Value);
                }
                else
                {
                    dr["Id"] = 0;
                }
                    

                dr["pk_ItemID"] = Convert.ToInt64(((HiddenField)row.FindControl("hdnItemId")).Value);
                dr["GST"] = Convert.ToDecimal(((Label)row.FindControl("lblGST")).Text);
                dr["ExistingRate"] = Convert.ToDecimal(((Label)row.FindControl("lblExistingRate")).Text);

                if (!string.IsNullOrEmpty(((CustomTextBox)row.FindControl("txtNewRate")).Text))
                    dr["NewRate"] = Convert.ToDecimal(((CustomTextBox)row.FindControl("txtNewRate")).Text);
                ds.Tables[0].Rows.Add(dr);
            }
        }

        private DataSet CreateDataSetSchema()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            dt.Columns.Add(new DataColumn("Id", System.Type.GetType("System.Int64")));
            dt.Columns.Add(new DataColumn("pk_ItemID", System.Type.GetType("System.Int64")));
            dt.Columns.Add(new DataColumn("GST", System.Type.GetType("System.Decimal")));
            dt.Columns.Add(new DataColumn("ExistingRate", System.Type.GetType("System.Decimal")));
            dt.Columns.Add(new DataColumn("NewRate", System.Type.GetType("System.Decimal")));
            ds.Tables.Add(dt);
            return ds;
        }

        private void TogglePriceModification(bool isEnabled)
        {
            for (int i = 0; i < gvwPrice.Rows.Count; i++)
            {
                GridViewRow row = gvwPrice.Rows[i];
                ((CustomTextBox)row.FindControl("txtNewRate")).Enabled = isEnabled;

                if (hdnMode.Value == "E")
                {
                    ((CustomTextBox)row.FindControl("txtNewRate")).Text = ((Label)row.FindControl("lblExistingRate")).Text;
                }
                else
                {
                    ((CustomTextBox)row.FindControl("txtNewRate")).Text = string.Empty;
                }
            }
            upPriceList.Update();

        }

        private bool ValidatePriceList()
        {
            bool isValid = true;

            if (hdnMode.Value == "A" && string.IsNullOrEmpty(txtNewDate.Text))
            {
                isValid = false;
                spnNewDate.Style["display"] = "";
            }

            if (hdnMode.Value == "E" && string.IsNullOrEmpty(txtDate.Text))
            {
                isValid = false;
                spnDate.Style["display"] = "";
            }

            return isValid;
        }
        #endregion

        protected void ddlGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            gvwPrice.DataSource = null;
            gvwPrice.DataBind();
            LoadPricelist();
        }
    }
}