﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RecipeEdit.aspx.cs" Inherits="FoodSmart.WebApp.Transaction.RecipeEdit" MasterPageFile="~/Site.Master" Title=":: FoodSmart :: Add / Edit User" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FoodSmart.WebApp" Namespace="FoodSmart.WebApp.CustomControls" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        function toggleAddPanel(param) {
            var tdAddItem = document.getElementById('tdAddItem');

            if (param)
                tdAddItem.style.display = 'block';
            else
                tdAddItem.style.display = 'none';
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="container" runat="Server">
    <div id="dvAsync" style="padding: 5px; display: none;">
        <div class="asynpanel">
            <div id="dvAsyncClose">
                <img alt="" src="../../Images/Close-Button.bmp" style="cursor: pointer;" onclick="ClearErrorState()" />
            </div>
            <div id="dvAsyncMessage">An error has occurred. Please try again
            </div>
        </div>
    </div>
    <asp:UpdateProgress ID="uProgressRecipe" runat="server" AssociatedUpdatePanelID="upRecipe">
        <ProgressTemplate>
            <div class="progress">
                <div id="image">
                    <img src="../../Images/PleaseWait.gif" alt="" />
                </div>
                <div id="text">
                    Please Wait...
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdateProgress ID="uProgressItem" runat="server" AssociatedUpdatePanelID="upItem">
        <ProgressTemplate>
            <div class="progress">
                <div id="image">
                    <img src="../../Images/PleaseWait.gif" alt="" />
                </div>
                <div id="text">
                    Please Wait...
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div id="headercaption">ADD / EDIT RECIPE</div>
    <div style="width: 900px; margin: 0 auto; margin-top: 30px;">
        <fieldset style="width: 100%;">
            <legend>Add / Edit Recipe</legend>
            <table border="0" cellpadding="3" cellspacing="3" width="100%">
                <tr>
                    <td style="width: 15%;">Recipe type:<span class="errormessage1">*</span></td>
                    <td style="width: 30%;">
                        <asp:DropDownList ID="ddlType" runat="server" Width="175px" AutoPostBack="true" CssClass="chzn-select medium-select single" OnSelectedIndexChanged="ddlType_SelectedIndexChanged">
                            <asp:ListItem Text="Finished" Value="F" />
                            <asp:ListItem Text="Semi finished" Value="S" />
                        </asp:DropDownList>
                    </td>
                    <td style="width: 15%;">Recipe /item name:<span class="errormessage1">*</span></td>
                    <td style="width: 30%;">
                        <asp:UpdatePanel ID="upRecipe" runat="server" UpdateMode="Conditional">
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlType" EventName="SelectedIndexChanged" />
                            </Triggers>
                            <ContentTemplate>
                                <div id="dvNameDDL" runat="server">
                                    <asp:DropDownList ID="ddlName" runat="server" Width="250px" AutoPostBack="true" CssClass="chzn-select medium-select single" OnSelectedIndexChanged="ddlName_SelectedIndexChanged"></asp:DropDownList>
                                </div>
                                <div id="dvNametxt" runat="server">
                                    <asp:TextBox ID="txtName" runat="server" CssClass="textboxuppercase" MaxLength="30" Width="250"></asp:TextBox><br />
                                    <span id="spnName" runat="server" class="errormessage" style="display: none;"></span>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td>Total plates:<span class="errormessage1">*</span></td>
                    <td>
                        <cc2:CustomTextBox ID="txtNoofPlates" runat="server" CssClass="numerictextbox" TabIndex="3" Width="100px" Height="20px"
                            Type="Decimal" MaxLength="10" Precision="10" Scale="2">
                        </cc2:CustomTextBox><br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtNoofPlates" ErrorMessage="This field is required*" CssClass="errormessage" ValidationGroup="Save" Display="Dynamic"></asp:RequiredFieldValidator>
                        <span id="spnNoofPlates" runat="server" class="errormessage" style="display: none;"></span>
                    </td>
                    <td>Unit:<span class="errormessage1">*</span></td>
                    <td>
                        <asp:TextBox ID="txtUnit" runat="server" CssClass="textboxuppercase" MaxLength="100" Width="150"></asp:TextBox><br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtUnit" ErrorMessage="This field is required*" CssClass="errormessage" ValidationGroup="Save" Display="Dynamic"></asp:RequiredFieldValidator>
                        <span id="spnUnit" runat="server" class="errormessage" style="display: none;"></span>
                    </td>
                </tr>
                <tr>
                    <td>Total quantity:</td>
                    <td>
                        <cc2:CustomTextBox ID="txtQty" runat="server" CssClass="numerictextbox" TabIndex="3" Width="100px" Height="20px"
                            Type="Decimal" MaxLength="12" Precision="10" Scale="3" Enabled="false">
                        </cc2:CustomTextBox><br />
                        <span id="spnQty" runat="server" class="errormessage" style="display: none;"></span>
                    </td>
                    <td>Introduced on:<span class="errormessage1">*</span></td>
                    <td>
                        <asp:TextBox ID="txtIntro" runat="server" CssClass="textboxuppercase" TabIndex="5" MaxLength="30" Width="102px" Height="20px" Style="text-align: left"></asp:TextBox><br />
                        <cc1:CalendarExtender ID="ceIntroDate" TargetControlID="txtIntro" runat="server" Format="dd/MM/yyyy" Enabled="True" />
                        <asp:RequiredFieldValidator ID="rfvIntroDate" runat="server" ControlToValidate="txtIntro" ErrorMessage="This field is required*" CssClass="errormessage" ValidationGroup="Save" Display="Dynamic"></asp:RequiredFieldValidator>
                        <span id="spnIntro" runat="server" class="errormessage" style="display: none;"></span>
                    </td>
                </tr>
                <tr>
                    <td>Total amount:</td>
                    <td>
                        <cc2:CustomTextBox ID="txtAmt" runat="server" CssClass="numerictextbox" TabIndex="3" Width="100px" Height="20px"
                            Type="Numeric" MaxLength="10" Precision="10" Scale="2" Enabled="false">
                        </cc2:CustomTextBox><br />
                        <span id="spnAmt" runat="server" class="errormessage" style="display: none;"></span>
                    </td>
                    <td>Processed at:<span class="errormessage1">*</span></td>
                    <td>
                        <asp:DropDownList ID="ddlProcessed" runat="server" Width="175px" AutoPostBack="false" CssClass="chzn-select medium-select single">
                            <asp:ListItem Text="Kitchen" Value="K" />
                            <asp:ListItem Text="Outlet" Value="O" />
                            <asp:ListItem Text="Outlet combo" Value="C" />
                        </asp:DropDownList>
                        <span id="spnProcessed" runat="server" class="errormessage" style="display: none;"></span>
                    </td>
                </tr>
                <tr>
                    <td>Rate / Unit:</td>
                    <td>
                        <cc2:CustomTextBox ID="txtRPU" runat="server" CssClass="numerictextbox" TabIndex="3" Width="100px" Height="20px"
                            Type="Decimal" MaxLength="10" Precision="10" Scale="2" Enabled="false">
                        </cc2:CustomTextBox><br />
                        <span id="spnRPU" runat="server" class="errormessage" style="display: none;"></span>
                    </td>
                    <td>Recipe summary:</td>
                    <td>
                        <asp:TextBox ID="txtSummary" runat="server" CssClass="textboxuppercase" MaxLength="100" Width="250" TextMode="MultiLine"></asp:TextBox><br />
                        <span id="spnSummary" runat="server" class="errormessage" style="display: none;"></span>
                    </td>
                </tr>
            </table>
            <asp:UpdatePanel ID="upItem" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div style="width: 100%;padding-top:20px;">
                        <table border="0" cellpadding="3" cellspacing="3" width="100%">
                            <tr>
                                <td style="width: 5%;padding:10px 0px 10px 0px;" class="gridviewheader">Sl#</td>
                                <td style="width: 15%;padding:10px 0px 10px 0px;" class="gridviewheader">Item type</td>
                                <td style="width: 20%;padding:10px 0px 10px 0px;" class="gridviewheader">Item</td>
                                <td style="width: 15%;padding:10px 0px 10px 0px;" class="gridviewheader">Unit</td>
                                <td style="width: 10%;padding:10px 0px 10px 0px;" class="gridviewheader_right">Quantity</td>
                                <td style="width: 10%;padding:10px 0px 10px 0px;" class="gridviewheader_right">Rate</td>
                                <td style="width: 10%;padding:10px 0px 10px 0px;" class="gridviewheader_right">Total amount</td>
                                <td style="width: 10%;padding:10px 0px 10px 0px;" class="gridviewheader"></td>
                            </tr>
                            <tr>
                                <td style="width: 2%;">&nbsp;</td>
                                <td style="width: 10%; margin-left: 40px;">
                                    <asp:HiddenField ID="hdnMode" runat="server" />
                                    <asp:DropDownList ID="ddlSubItemType" runat="server" AutoPostBack="true" CssClass="chzn-select medium-select single" OnSelectedIndexChanged="ddlSubItemType_SelectedIndexChanged">
                                        <asp:ListItem Text="Raw material" Value="R" />
                                        <asp:ListItem Text="Finished" Value="F" />
                                        <asp:ListItem Text="Semi finished" Value="S" />
                                    </asp:DropDownList>
                                </td>
                                <td style="width: 20%;">
                                    <asp:DropDownList ID="ddlSubItem" runat="server" AutoPostBack="true" Width="250px" CssClass="chzn-select medium-select single" OnSelectedIndexChanged="ddlSubItem_SelectedIndexChanged">
                                        <asp:ListItem Text="Select" Value="0" />
                                    </asp:DropDownList>
                                </td>
                                <td style="width: 15%;">
                                    <asp:TextBox ID="txtItemUnit" runat="server" CssClass="textboxuppercase" MaxLength="100" Width="80px"></asp:TextBox>
                                </td>
                                <td style="width: 10%;">
                                    <cc2:CustomTextBox ID="txtItemQty" runat="server" CssClass="numerictextbox" 
                                        Width="100%" Height="20px" Type="Decimal" MaxLength="13" Precision="10" 
                                        Scale="3" ontextchanged="txtItemQty_TextChanged" AutoPostBack="true">
                                    </cc2:CustomTextBox>
                                    <%--<asp:TextBox ID="txtItemQty" runat="server" CssClass="textboxuppercase" MaxLength="100" Width="80px"></asp:TextBox>--%>
                                </td>
                                <td style="width: 10%;">
                                    <cc2:CustomTextBox ID="txtItemRate" runat="server" CssClass="numerictextbox" 
                                        Width="100%" Height="20px" Type="Decimal" MaxLength="13" Precision="10" 
                                        Scale="2" Enabled="false">
                                    </cc2:CustomTextBox>
                                    <%--<asp:TextBox ID="txtItemRate" runat="server" CssClass="textboxuppercase" MaxLength="100" Width="80px"></asp:TextBox>--%>
                                </td>
                                <td style="width: 10%;">
                                    <cc2:CustomTextBox ID="txtItemTot" runat="server" CssClass="numerictextbox" 
                                        Width="100%" Height="20px" Type="Decimal" MaxLength="13" Precision="10" 
                                        Scale="2" Enabled="false">
                                    </cc2:CustomTextBox>
                                    <%--<asp:TextBox ID="txtItemTot" runat="server" CssClass="textboxuppercase" MaxLength="100" Width="80px"></asp:TextBox>--%>
                                </td>
                                <td style="width: 10%;">
                                    <asp:Button ID="btnSaveItem" runat="server" Text="Add item" OnClick="btnSaveItem_Click" /></td>
                            </tr>
                        </table>
                    </div>
                    <div style="padding-top: 10px;">

                        <asp:GridView ID="gvwRecipe" runat="server" AutoGenerateColumns="false" 
                            ShowHeader="false" AutoGenerateEditButton="false" AllowPaging="true" 
                            BorderStyle="None" BorderWidth="0" 
                            OnPageIndexChanging="gvwRecipe_PageIndexChanging" 
                            OnRowDataBound="gvwRecipe_RowDataBound" OnRowCommand="gvwRecipe_RowCommand" 
                            Width="100%" onselectedindexchanged="gvwRecipe_SelectedIndexChanged">
                            <PagerSettings Mode="NumericFirstLast" Position="Bottom" />
                            <PagerStyle CssClass="gridviewpager" />
                            <EmptyDataRowStyle CssClass="gridviewemptydatarow" />
                            <EmptyDataTemplate>No recipies Found</EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="Sl#">
                                    <HeaderStyle CssClass="gridviewheader" />
                                    <ItemStyle CssClass="gridviewitem" Width="5%" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblSlNo" runat="server"></asp:Label>
                                        <asp:HiddenField ID="hdnRecipeItemId" runat="server"></asp:HiddenField>
                                        <asp:HiddenField ID="hdnRecipeId" runat="server"></asp:HiddenField>
                                        <asp:HiddenField ID="hdnItemId" runat="server"></asp:HiddenField>
                                        <asp:HiddenField ID="hdnType" runat="server"></asp:HiddenField>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Item type">
                                    <HeaderStyle CssClass="gridviewheader" />
                                    <ItemStyle CssClass="gridviewitem" Width="15%" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblType" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Item">
                                    <HeaderStyle CssClass="gridviewheader" />
                                    <ItemStyle CssClass="gridviewitem" Width="28%" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblItem" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Unit">
                                    <HeaderStyle CssClass="gridviewheader" />
                                    <ItemStyle CssClass="gridviewitem" Width="14%" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblUnit" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Quantity">
                                    <HeaderStyle CssClass="gridviewheader" HorizontalAlign="Right" />
                                    <ItemStyle CssClass="gridviewitem" Width="10%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblQty" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Rate">
                                    <HeaderStyle CssClass="gridviewheader" HorizontalAlign="Right" />
                                    <ItemStyle CssClass="gridviewitem" Width="9%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblRate" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Total amount">
                                    <HeaderStyle CssClass="gridviewheader" HorizontalAlign="Right" />
                                    <ItemStyle CssClass="gridviewitem" Width="9%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblAmt" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="gridviewheader" />
                                    <ItemStyle CssClass="gridviewitem" Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnEdit" runat="server" CommandName="Modify" ImageUrl="~/Images/edit.png" Height="16" Width="16" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="gridviewheader" />
                                    <ItemStyle CssClass="gridviewitem" Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnRemove" runat="server" CommandName="Remove" ImageUrl="~/Images/remove.png" Height="16" Width="16" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

            <div style="padding-top: 10px;">
                <asp:Button ID="btnSave" runat="server" Text="Save" ValidationGroup="Save" OnClick="btnSave_Click" />&nbsp;&nbsp;<asp:Button ID="btnBack" runat="server" Text="Back" />&nbsp;&nbsp;<asp:Label ID="msgbox" runat="server" Text="" ForeColor="Red" />
            </div>
        </fieldset>
    </div>
</asp:Content>
