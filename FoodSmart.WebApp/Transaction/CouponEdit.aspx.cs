﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FoodSmart.BLL;
using FoodSmart.Common;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using FoodSmart.Utilities.ResourceManager;
using System.Data;

namespace FoodSmart.WebApp.Transaction
{
    public partial class CouponEdit : System.Web.UI.Page
    {
        #region Private Member Variables

        private int _userId = 0;
        private int _uId = 0;
        private bool _canAdd = false;
        private bool _canEdit = false;
        private bool _canDelete = false;
        private bool _canView = false;

        #endregion

        #region Protected Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            RetriveParameters();
            CheckUserAccess();
            SetAttributes();
            if (!IsPostBack)
            {
                LoadData();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            SaveRecord();
        }

        #endregion

        #region Private Methods

        private void RetriveParameters()
        {
            _userId = UserBLL.GetLoggedInUserId();

            //Get user permission.
            UserBLL.GetUserPermission(out _canAdd, out _canEdit, out _canDelete, out _canView);

            if (!ReferenceEquals(Request.QueryString["id"], null))
            {
                Int32.TryParse(GeneralFunctions.DecryptQueryString(Request.QueryString["id"].ToString()), out _uId);
            }
        }

        private void SetAttributes()
        {
            spnName.Style["display"] = "none";

            if (!IsPostBack)
            {
                if (_uId == -1) //Add mode
                {
                    if (!_canAdd) btnSave.Visible = false;
                }
                else
                {
                    if (!_canEdit) btnSave.Visible = false;
                }

                btnSave.ToolTip = ResourceManager.GetStringWithoutName("R00060");
                btnBack.ToolTip = ResourceManager.GetStringWithoutName("R00061");
                btnBack.OnClientClick = "javascript:return RedirectAfterCancelClick('CouponList.aspx','" + ResourceManager.GetStringWithoutName("R00025") + "')";
                spnName.InnerText = ResourceManager.GetStringWithoutName("R00046");
                
            }

        }

        private void CheckUserAccess()
        {
            if (!ReferenceEquals(Session[Constants.SESSION_USER_INFO], null))
            {
                IUser user = (IUser)Session[Constants.SESSION_USER_INFO];

                if (ReferenceEquals(user, null) || user.Id == 0)
                {
                    Response.Redirect("~/Login.aspx");
                }

                if (user.RoleID != (int)UserRole.Admin)
                {
                    Response.Redirect("~/Unauthorized.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Login.aspx");
            }

            if (_uId == 0)
                Response.Redirect("~/Views/ManageUser.aspx");

            if (!_canView)
            {
                Response.Redirect("~/Unauthorized.aspx");
            }
        }

       

        private void LoadData()
        {
            CouponBLL oCouponbll = new CouponBLL(); 

            ICoupon oCP = (CouponEntity)oCouponbll.GetCouponForEdit(_uId);

            if (!ReferenceEquals(oCP, null))
            {
                
                txtPrefix.Text = oCP.CouponPrefix;
                txtName.Text = oCP.CouponName;
                txtActDt.Text = oCP.ActivateDate.ToShortDateString();
                txtExpDt.Text = oCP.ExpiryDate.ToShortDateString();
                txtCPDisc.Text = oCP.CouponAmount.ToString();
                txtStartNo.Text = oCP.CouponStartSerial.ToString();
                txtEndNo.Text = oCP.CouponEndSerial.ToString();
                //txtId.Text = oCP.CPID.ToString();
            }
        }

        private bool ValidateControls(ICoupon cp)
        {
            bool isValid = true;

            if (cp.CouponPrefix == string.Empty)
            {
                isValid = false;
                spnPrefix.Style["display"] = "";
            }
            if (cp.CouponName == string.Empty)
            {
                isValid = false;
                spnName.Style["display"] = "";
            }

            if (cp.CouponStartSerial.ToString() == string.Empty)
            {
                isValid = false;
                spnStartNo.Style["display"] = "";
            }

            if (cp.CouponEndSerial.ToString() == string.Empty)
            {
                isValid = false;
                spnEndNo.Style["display"] = "";
            }

            if (cp.ActivateDate.ToString() == string.Empty)
            {
                isValid = false;
                spnActDt.Style["display"] = "";
            }

            if (cp.ExpiryDate.ToString() == string.Empty)
            {
                isValid = false;
                spnExpDt.Style["display"] = "";
            }

            
            return isValid;
        }

        private void SaveRecord()
        {
            ICoupon oCP = new CouponEntity();
            BuildUserEntity(oCP);

            if (ValidateControls(oCP))
            {
                string strMode="A";
                if (oCP.CPID > 0)
                {
                    strMode="E";
                }
                CouponBLL cpBLL = new CouponBLL();
                int result = cpBLL.SaveCoupon(oCP, strMode);

                if (result == 0)
                {
                    msgbox.Text = "Error in Coupon Range. Record Not saved";
                    return;
                }
                else
                {
                    msgbox.Text = "";
                }
                if (strMode == "E")
                {
                    //SendEmail(user);
                    Response.Redirect("~/Transaction/CouponList.aspx");
                }
                else
                {
                    txtPrefix.Text = string.Empty;
                    txtName.Text = string.Empty;
                    txtStartNo.Text = string.Empty;
                    txtEndNo.Text = string.Empty;
                    txtExpDt.Text = string.Empty;
                    txtActDt.Text = string.Empty;
                    txtCPDisc.Text = string.Empty;
                }
                //    else
                //    {
                //        GeneralFunctions.RegisterAlertScript(this, result.Message);
                //    }
                //}
                //else
                //{
                //    msgbox.Text = "Error in User ID";
                //}
            }
        }

        private void BuildUserEntity(ICoupon oCP)
        {
            oCP.CPID = _uId;
            oCP.CouponPrefix = txtPrefix.Text.ToUpper();
            oCP.CouponName = txtName.Text.ToUpper();
            oCP.ActivateDate = Convert.ToDateTime(txtActDt.Text);
            oCP.ExpiryDate = Convert.ToDateTime(txtExpDt.Text);
            oCP.CouponStartSerial = txtStartNo.Text.ToInt();
            oCP.CouponEndSerial = txtEndNo.Text.ToInt();
            oCP.CouponAmount = txtCPDisc.Text.ToDecimal();
            oCP.CouponStatus = true;
            oCP.CreatedBy = _userId; 
        }

        
        #endregion
       
    }
}