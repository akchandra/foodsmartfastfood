﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FoodSmart.BLL;
using FoodSmart.Common;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using FoodSmart.Utilities.ResourceManager;
using System.Data;

namespace FoodSmart.WebApp.Transaction
{
    public partial class TranEdit : System.Web.UI.Page
    {
        #region Private Member Variables

        private int _userId = 0;
        private int _rId = 0;
        private bool _canAdd = false;
        private bool _canEdit = false;
        private bool _canDelete = false;
        private bool _canView = false;

        #endregion

        #region Protected Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            RetriveParameters();
            SetAttributes();

            if (!IsPostBack)
            {
                txtOriginalTranRef.Enabled = false;
                btnTranRef.Enabled = false;
                ddlFromLoc.Enabled = false;
                ddlToLoc.Enabled = true;
                LoadToStore();
                LoadVendor();
                LoadSubItems();
                LoadData();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            SaveTransaction();
        }

        protected void btnSaveItem_Click(object sender, EventArgs e)
        {
            decimal qty = txtItemQty.Text.ToDecimal();
            decimal itmRate = txtItemRate.Text.ToDecimal();

            if (ddlSubItem.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "alert", "<script>javascript:void alert('Please select an item');</script>", false);
                return;
            }
            if ((ddlType.SelectedValue == "R" || ddlType.SelectedValue == "Y") && (string.IsNullOrEmpty(txtItemQty.Text) || string.IsNullOrEmpty(txtItemRate.Text) || qty == 0 || itmRate == 0))
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "alert", "<script>javascript:void alert('Qty and Rate is compulsory');</script>", false);
                return;
            }
            AddSubItem();
            ddlSubItemType.SelectedValue = "R";
            LoadSubItems();
            txtItemUnit.Text = string.Empty;
            txtItemQty.Text = string.Empty;
            txtItemRate.Text = string.Empty;
            txtGSTPer.Text = string.Empty;
            txtItemTot.Text = string.Empty;
            hdnMode.Value = string.Empty;
            ddlSubItemType.Enabled = true;
            ddlSubItem.Enabled = true;
        }

        protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtOriginalTranRef.Text = string.Empty;
            ClearTran();
            DataSet ds = null;
            gvwItem.DataSource = ds;
            gvwItem.DataBind();
            upItem.Update();

            if (ddlType.SelectedValue == "I")
            {
                ddlFromLoc.Enabled = true;
                ddlToLoc.Enabled = true;
                ddlVendor.Enabled = false;
                ddlVendor.SelectedValue = "0";
                txtVendorRef.Enabled = false;
                txtOriginalTranRef.Enabled = false;
                btnTranRef.Enabled = false;
                RateVisible(false);
                LoadFromStore();
            }
            else if (ddlType.SelectedValue == "Y")
            {
                ddlFromLoc.Enabled = true;
                ddlToLoc.Enabled = true;
                ddlVendor.Enabled = false;
                ddlVendor.SelectedValue = "0";
                txtVendorRef.Enabled = false;
                txtOriginalTranRef.Enabled = true;
                btnTranRef.Enabled = true;
                RateVisible(true);
                LoadFromStore();
            }
            else if (ddlType.SelectedValue == "R")
            {
                ddlFromLoc.SelectedValue = "0";
                ddlFromLoc.Enabled = false;
                ddlToLoc.Enabled = true;
                ddlVendor.Enabled = true;
                txtVendorRef.Enabled = true;
                txtOriginalTranRef.Enabled = true;
                btnTranRef.Visible = true;
                txtOriginalTranRef.Enabled = false;
                btnTranRef.Enabled = false;
                RateVisible(true);
                LoadFromStore();
            }
            else if (ddlType.SelectedValue == "X")
            {
                ddlToLoc.SelectedValue = "0";
                ddlToLoc.Enabled = false;
                ddlVendor.Enabled = true;
                txtVendorRef.Enabled = true;
                txtOriginalTranRef.Enabled = true;
                btnTranRef.Visible = true;
                txtOriginalTranRef.Enabled = true;
                btnTranRef.Enabled = true;
                RateVisible(false);
                LoadFromStore();
            }
            else if (ddlType.SelectedValue == "W")
            {
                //ddlFromLoc.SelectedValue = "0";
                ddlFromLoc.Enabled = true;
                ddlToLoc.Enabled = false;
                ddlVendor.Enabled = false;
                txtVendorRef.Enabled = false;
                txtOriginalTranRef.Enabled = false;
                btnTranRef.Visible = true;
                txtOriginalTranRef.Enabled = true;
                btnTranRef.Enabled = true;
                RateVisible(false);
                LoadFromStore();
            }
            else
            {
                ddlToLoc.SelectedValue = "0";
                ddlToLoc.Enabled = false;
                txtOriginalTranRef.Enabled = false;
                btnTranRef.Visible = false;
                ddlVendor.Enabled = false;
                ddlVendor.SelectedValue = "0";
                txtVendorRef.Enabled = false;
                txtOriginalTranRef.Enabled = false;
                btnTranRef.Enabled = false;
                txtItemRate.Enabled = false;
                txtItemTax.Enabled = false;
            }
        }

        protected void ddlFromLoc_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadToStore();
        }

        protected void gvwItem_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
           
        }

        private void SaveNewPageIndex(int newIndex)
        {
            if (!ReferenceEquals(Session[Constants.SESSION_SEARCH_CRITERIA], null))
            {
                SearchCriteria criteria = (SearchCriteria)Session[Constants.SESSION_SEARCH_CRITERIA];

                if (!ReferenceEquals(criteria, null))
                {
                    criteria.PageIndex = newIndex;
                }
            }
        }
        protected void gvwItem_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Modify")
            {
                EditItem(Convert.ToInt32(e.CommandArgument));
            }
            else if (e.CommandName == "Remove")
            {
                RemoveItem(Convert.ToInt32(e.CommandArgument));
            }
        }

        protected void gvwItem_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                GeneralFunctions.ApplyGridViewAlternateItemStyle(e.Row, 10);
                ((Label)e.Row.FindControl("lblSlNo")).Text = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Id"));
                ((HiddenField)e.Row.FindControl("hdnItemTranId")).Value = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "pk_TranDetailID"));
                ((HiddenField)e.Row.FindControl("hdnType")).Value = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "ItemType"));
                ((HiddenField)e.Row.FindControl("hdnItemId")).Value = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "fk_RMID"));
                ((Label)e.Row.FindControl("lblType")).Text = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "ItemTypeDesc"));
                ((Label)e.Row.FindControl("lblItem")).Text = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Item"));
                ((Label)e.Row.FindControl("lblUnit")).Text = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "UOM"));
                ((Label)e.Row.FindControl("lblQty")).Text = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Qty"));
                ((Label)e.Row.FindControl("lblRate")).Text = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "ItemRate"));
                ((Label)e.Row.FindControl("lblGSTPer")).Text = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "GSTPer"));
                ((Label)e.Row.FindControl("lblTax")).Text = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "GSTAmt"));
                ((Label)e.Row.FindControl("lblAmt")).Text = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Total"));

                // Edit link
                ImageButton btnEdit = (ImageButton)e.Row.FindControl("btnEdit");
                btnEdit.ToolTip = ResourceManager.GetStringWithoutName("R00035");
                if (_rId <= 0)
                    btnEdit.Visible = false;
                //Delete link
                ImageButton btnRemove = (ImageButton)e.Row.FindControl("btnRemove");
                btnRemove.ToolTip = ResourceManager.GetStringWithoutName("R00034");

                btnEdit.CommandArgument = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Id"));
                btnRemove.CommandArgument = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Id"));
            }
        }

        protected void ddlSubItemType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlSubItem.Items.Clear();
            LoadSubItems();
        }

        protected void ddlSubItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadSubItemDetails();
            txtItemQty.Focus();
            txtItemQty.Attributes.Add("onfocus", "this.select();");
        }

        protected void btnTranRef_Click(object sender, EventArgs e)
        {
            LoadRefTransaction();
        }
        #endregion

        #region Private Methods

        private void LoadFromStore()
        {
            ddlFromLoc.DataSource = null;
            List<IStore> locations;
            if (ddlType.SelectedValue == "R" || ddlType.SelectedValue == "X" || ddlType.SelectedValue == "I")
                locations = TransactionBLL.GetAllStore("S");
            else if (ddlType.SelectedValue == "T" || ddlType.SelectedValue == "Y")
                locations = TransactionBLL.GetAllStore("R");
            else
                locations = TransactionBLL.GetAllStore("W");
            ddlFromLoc.DataValueField = "StoreID";
            ddlFromLoc.DataTextField = "StoreName";
            ddlFromLoc.DataSource = locations;
            ddlFromLoc.DataBind();
            ddlFromLoc.Items.Insert(0, new ListItem("Select", "0"));
            //ddlToLoc.Items.Insert(0, new ListItem("Select", "0"));
        }

        private void LoadToStore()
        {
            List<IStore> locations;
            List<IStore> resultList=null;
            //var resultList = new List<> { };
            if (ddlType.SelectedValue == "T")
                locations = TransactionBLL.GetAllStore("R");
            else if (ddlType.SelectedValue == "Y")
                locations = TransactionBLL.GetAllStore("S");
            else if (ddlType.SelectedValue == "P")
                locations = TransactionBLL.GetAllStore("S");
            else
                locations = TransactionBLL.GetAllStore("W");
            if (ddlType.SelectedValue == "T")
                resultList = locations.Where(x => x.StoreID != Convert.ToInt32(ddlFromLoc.SelectedValue)).ToList();
            else
                resultList = locations;
            ddlToLoc.DataValueField = "StoreID";
            ddlToLoc.DataTextField = "StoreName";
            ddlToLoc.DataSource = resultList;
            ddlToLoc.DataBind();
            ddlToLoc.Items.Insert(0, new ListItem("Select", "0"));
        }

        private void LoadVendor()
        {
            List<IVendor> vendors = TransactionBLL.GetVendors();
            ddlVendor.DataValueField = "VendorID";
            ddlVendor.DataTextField = "VendorName";
            ddlVendor.DataSource = vendors;
            ddlVendor.DataBind();
            ddlVendor.Items.Insert(0, new ListItem("Select", "0"));
        }

        private void LoadSubItems()
        {
            List<IRecipe> items = RecipeBLL.GetItems(ddlSubItemType.SelectedValue);
            ddlSubItem.DataValueField = "Id";
            ddlSubItem.DataTextField = "Name";
            ddlSubItem.DataSource = items;
            ddlSubItem.DataBind();
            ddlSubItem.Items.Insert(0, new ListItem("Select", "0"));
        }

        private void LoadSubItemDetails()
        {
            if (ddlSubItem.SelectedValue != "0")
            {
                IRecipe item = RecipeBLL.GetItem(ddlSubItemType.SelectedValue, Convert.ToInt32(ddlSubItem.SelectedValue));

                if (!ReferenceEquals(item, null))
                {
                    txtItemUnit.Text = item.UOM;
                    txtItemQty.Text = Convert.ToString(item.Quantity);
                    txtItemRate.Text = Convert.ToString(item.Rate);
                    txtItemTax.Text = "0";
                    txtItemTot.Text = Convert.ToString(item.Amount);
                    txtGSTPer.Text = "0";
                }
            }
            else
            {
                txtItemUnit.Text = string.Empty;
                txtItemQty.Text = string.Empty;
                txtItemRate.Text = string.Empty;
                txtItemTot.Text = string.Empty;
            }
        }

        private void RetriveParameters()
        {
            _userId = UserBLL.GetLoggedInUserId();
            //Get user permission.
            UserBLL.GetUserPermission(out _canAdd, out _canEdit, out _canDelete, out _canView);
            if (!ReferenceEquals(Request.QueryString["id"], null))
            {
                Int32.TryParse(GeneralFunctions.DecryptQueryString(Request.QueryString["id"].ToString()), out _rId);
            }
        }

        private void SetAttributes()
        {
            spnNarration.Style["display"] = "none";

            if (!IsPostBack)
            {
                btnSave.ToolTip = ResourceManager.GetStringWithoutName("R00060");
                btnBack.ToolTip = ResourceManager.GetStringWithoutName("R00061");
                btnBack.OnClientClick = "javascript:return RedirectAfterCancelClick('TranList.aspx','" + ResourceManager.GetStringWithoutName("R00025") + "')";

            }
        }

        //private void CheckUserAccess()
        //{
        //    if (!ReferenceEquals(Session[Constants.SESSION_USER_INFO], null))
        //    {
        //        IUser user = (IUser)Session[Constants.SESSION_USER_INFO];

        //        if (ReferenceEquals(user, null) || user.Id == 0)
        //        {
        //            Response.Redirect("~/Login.aspx");
        //        }

        //        if (user.UserRole.Id != (int)UserRole.Admin)
        //        {
        //            Response.Redirect("~/Unauthorized.aspx");
        //        }
        //    }
        //    else
        //    {
        //        Response.Redirect("~/Login.aspx");
        //    }

        //    if (_rId == 0)
        //        Response.Redirect("~/Transaction/TranList.aspx");

        //    if (!_canView)
        //    {
        //        Response.Redirect("~/Unauthorized.aspx");
        //    }
        //}

        private void ClearTran()
        {
           
            //ddlType.SelectedIndex = 0;
            //ddlFromLoc.SelectedIndex = -1;
            //ddlToLoc.SelectedIndex = -1;
            //ddlToLoc.Enabled = false;
            txtGSTPer.Text = string.Empty;
            txtItemQty.Text = string.Empty;
            txtItemRate.Text = string.Empty;
            txtItemTax.Text = string.Empty;
            txtItemTot.Text = string.Empty;
            txtItemUnit.Text = string.Empty;
            txtNarration.Text = string.Empty;
            txtOriginalTranRef.Text = string.Empty;
            txtTotalAmount.Text = string.Empty;
            //txtTranDate.Text = string.Empty;
            txtTranNo.Text = string.Empty;
            txtVendorRef.Text = string.Empty;
            gvwItem.DataSource = null;
            gvwItem.DataBind();
            ddlFromLoc.DataSource = null;
            ddlFromLoc.DataBind();
            ddlToLoc.DataSource = null;
            ddlToLoc.DataBind();

        }
        private DataSet GetGridData()
        {
            DataSet ds = CreateDataSetSchema(false);
            for (int j = 0; j < gvwItem.Rows.Count; j++)
            {
                DataRow dr;
                GridViewRow row = gvwItem.Rows[j];
                dr = ds.Tables[0].NewRow();

                dr["pk_TranDetailID"] = Convert.ToInt64(((HiddenField)row.FindControl("hdnItemTranId")).Value);
                if (_rId != 0)
                    dr["fk_TranID"] = _rId;
                dr["ItemType"] = ((HiddenField)row.FindControl("hdnType")).Value;
                dr["fk_RMID"] = Convert.ToInt32(((HiddenField)row.FindControl("hdnItemId")).Value);
                dr["Qty"] = Convert.ToDecimal(((Label)row.FindControl("lblQty")).Text);
                dr["ItemRate"] = Convert.ToDecimal(((Label)row.FindControl("lblRate")).Text);
                dr["TaxableBasic"] = Convert.ToDecimal(((Label)row.FindControl("lblQty")).Text) + Convert.ToDecimal(((Label)row.FindControl("lblRate")).Text);
                dr["GSTPer"] = Convert.ToDecimal(((Label)row.FindControl("lblGSTPer")).Text);
                dr["GSTAmt"] = Convert.ToDecimal(((Label)row.FindControl("lblTax")).Text);
                //dr["Taxes"] = Convert.ToDecimal(((Label)row.FindControl("lblRate")).Text);
                ds.Tables[0].Rows.Add(dr);
            }

            return ds;
        }

        private void AddSubItem()
        {
            DataSet ds = CreateDataSetSchema(true);
            int id = 0;
            int.TryParse(hdnMode.Value, out id);
            
            foreach (GridViewRow row in gvwItem.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    if (ddlSubItem.SelectedValue.ToInt() == Convert.ToInt32(((HiddenField)row.FindControl("hdnItemId")).Value)) // row.FindControl("fk_RMID").ToInt())
                    {
                        //DataTable sourceData = (DataTable)gvwItem.DataSource;
                        //sourceData.Rows[row.RowIndex].Delete();
                        int slNo = Convert.ToInt32(((Label)row.FindControl("lblSlNo")).Text);
                        if (_rId <= 0)
                            RemoveItem(slNo);
                        break;
                    }
                }
                
            }

            int index = 0;
            for (int j = 0; j < gvwItem.Rows.Count; j++)
            {
                index++;
                DataRow dr;
                GridViewRow row = gvwItem.Rows[j];

                //dr = ds.Tables[0].NewRow();


                dr = ds.Tables[0].NewRow();

                if (index == id)
                {
                    var tranItemId = (!string.IsNullOrEmpty(((HiddenField)row.FindControl("hdnItemTranId")).Value)) ? Convert.ToInt64(((HiddenField)row.FindControl("hdnItemTranId")).Value) : 0;
                    //var tranId = (!string.IsNullOrEmpty(((HiddenField)row.FindControl("hdnTranId")).Value)) ? Convert.ToInt64(((HiddenField)row.FindControl("hdnTranId")).Value) : 0;
                    //var tranType = Convert.ToString(((HiddenField)row.FindControl("hdnTranType")).Value);

                    FillExistingRow(dr, index, tranItemId, 0);
                }

                else
                    LoadGridRow(row, dr, index);

                ds.Tables[0].Rows.Add(dr);
            }

            //Add new row

            if (id == 0)
                FillNewRow(ds, index);


            BindGrid(ds);
        }

        private decimal CalculateTotal(decimal qty, decimal rate, decimal tax, decimal GstPer)
        {
            
            tax = Math.Round(qty * rate * (GstPer / 100), 2);
            decimal total = qty * rate + tax;
            return Math.Round(total, 2);
        }

        private decimal CalculateGST(decimal qty, decimal rate, decimal GstPer)
        {

            decimal tax = Math.Round(qty * rate * (GstPer / 100), 2);
            return Math.Round(tax, 2);
        }
        private void FillExistingRow(DataRow row, int index, Int64 tranItemId, Int64 tranId)
        {
            row["Id"] = index;
            row["pk_TranDetailID"] = tranItemId;
            //row["fk_TranID"] = tranId;
            //row["TranType"] = tranType;
            row["fk_RMID"] = Convert.ToInt64(ddlSubItem.SelectedValue);
            row["ItemType"] = ddlSubItemType.SelectedValue;

            if (ddlSubItemType.SelectedValue == "R")
                row["ItemTypeDesc"] = "Raw material";
            else if (ddlSubItemType.SelectedValue == "F")
                row["ItemTypeDesc"] = "Finished";
            else if (ddlSubItemType.SelectedValue == "S")
                row["ItemTypeDesc"] = "Semi finished";

            row["Item"] = ddlSubItem.SelectedItem.Text;
            row["UOM"] = txtItemUnit.Text;
            row["Qty"] = (!string.IsNullOrEmpty(txtItemQty.Text)) ? Convert.ToDecimal(txtItemQty.Text) : 0;
            row["ItemRate"] = (!string.IsNullOrEmpty(txtItemRate.Text)) ? Convert.ToDecimal(txtItemRate.Text) : 0;
            row["GSTPer"] = (!string.IsNullOrEmpty(txtGSTPer.Text)) ? Convert.ToDecimal(txtGSTPer.Text) : 0;
            row["GSTAmt"] = CalculateGST(Convert.ToDecimal(row["Qty"]), Convert.ToDecimal(row["ItemRate"]), Convert.ToDecimal(row["GSTPer"]));  // (!string.IsNullOrEmpty(txtItemTax.Text)) ? Convert.ToDecimal(txtItemTax.Text) : 0;
            row["Total"] = CalculateTotal(Convert.ToDecimal(row["Qty"]), Convert.ToDecimal(row["ItemRate"]), Convert.ToDecimal(row["GSTAmt"]), Convert.ToDecimal(row["GSTPer"]));
        }

        private void FillNewRow(DataSet ds, int index)
        {
            DataRow row = ds.Tables[0].NewRow();
            row["Id"] = index + 1;
            row["pk_TranDetailID"] = 0;
            //row["fk_TranID"] = 0;
            row["ItemType"] = ddlSubItemType.SelectedValue;

            if (ddlSubItemType.SelectedValue == "R")
                row["ItemTypeDesc"] = "Raw material";
            else if (ddlSubItemType.SelectedValue == "F")
                row["ItemTypeDesc"] = "Finished";
            else if (ddlSubItemType.SelectedValue == "S")
                row["ItemTypeDesc"] = "Semi finished";

            row["fk_RMID"] = Convert.ToInt32(ddlSubItem.SelectedValue);
            row["Item"] = ddlSubItem.SelectedItem.Text;
            row["UOM"] = txtItemUnit.Text;
            row["Qty"] = (!string.IsNullOrEmpty(txtItemQty.Text)) ? Convert.ToDecimal(txtItemQty.Text) : 0;
            row["ItemRate"] = (!string.IsNullOrEmpty(txtItemRate.Text)) ? Convert.ToDecimal(txtItemRate.Text) : 0;
            row["GSTAmt"] = (!string.IsNullOrEmpty(txtItemTax.Text)) ? Convert.ToDecimal(txtItemTax.Text) : 0;
            row["GSTPer"] = (!string.IsNullOrEmpty(txtGSTPer.Text)) ? Convert.ToDecimal(txtGSTPer.Text) : 0;
            //row["Taxes"] = CalculateGST(Convert.ToDecimal(row["Qty"]), Convert.ToDecimal(row["Rate"]), Convert.ToDecimal(row["GSTPer"]));
            row["Total"] = CalculateTotal(Convert.ToDecimal(row["Qty"]), Convert.ToDecimal(row["ItemRate"]), Convert.ToDecimal(row["GSTAmt"]), Convert.ToDecimal(row["GSTPer"]));

            ds.Tables[0].Rows.Add(row);
        }

        private void LoadData()
        {
            if (_rId > 0)
            {
                ddlType.Enabled = false;
            }

            ITransaction transaction = TransactionBLL.GetTransaction(_rId, _userId);

            if (!ReferenceEquals(transaction, null))
            {

                ddlType.SelectedValue = Convert.ToString(transaction.TranType);

                if (transaction.TranType == "T")
                {
                    LoadFromStore();
                    ddlFromLoc.SelectedValue = Convert.ToString(transaction.FromLocID);
                    LoadToStore();
                    ddlToLoc.SelectedValue = Convert.ToString(transaction.ToLocID);
                    //ddlFromLoc.SelectedValue = Convert.ToString(transaction.FromLocID);
                    RateVisible(false);
                    ddlToLoc.Enabled = true;
                    ddlVendor.Enabled = false;
                }
                else
                {
                    ddlVendor.Enabled = true;
                    LoadToStore();
                    ddlToLoc.SelectedValue = Convert.ToString(transaction.ToLocID);
                    RateVisible(true);
                    ddlToLoc.Enabled = false;
                }

                txtTranNo.Text = transaction.TranNo;
                txtTranDate.Text = transaction.TranDate.ToString("dd/MM/yyyy");
                ddlVendor.SelectedValue = Convert.ToString(transaction.VendorId);
                txtVendorRef.Text = transaction.RefDoc;
                txtOriginalTranRef.Text = transaction.RefDoc;
                txtNarration.Text = transaction.Narration;

                List<ITransactionItem> items = TransactionBLL.GetAllTransactionItems(_rId);
                if (items.Count > 0)
                {
                    LoadTransactionItems(items);
                }
            }
            else
            {
                txtTranDate.Text = DateTime.Today.ToString("dd/MM/yyyy");
            }
        }

        private void LoadRefTransaction()
        {
            List<ITransactionItem> items = TransactionBLL.GetAllTransactionItems(txtOriginalTranRef.Text);
            if (items.Count > 0)
            {
                LoadTransactionItems(items);
                upItem.Update();
            }

        }
        private void LoadTransactionItems(List<ITransactionItem> items)
        {
            DataSet ds = CreateDataSetSchema(true);
            int index = 1;
            foreach (var item in items)
            {
                DataRow row = ds.Tables[0].NewRow();

                row["Id"] = index;

                row["pk_TranDetailID"] = item.Id;
                row["fk_TranID"] = item.TransactionId;
                //row["TranType"] = item.TransactionType;
                row["fk_RMID"] = item.ItemId;
                if (item.Quantity.HasValue)
                    row["Qty"] = item.Quantity.Value;
                else
                    row["Qty"] = 0;

                if (item.Rate.HasValue)
                    row["ItemRate"] = item.Rate.Value;
                else
                    row["ItemRate"] = 0;

                if (item.Tax.HasValue)
                    row["GSTAmt"] = item.Tax.Value;
                else
                    row["GSTAmt"] = 0;

                if (item.GSTPer.HasValue)
                    row["GSTPer"] = item.GSTPer.Value;
                else
                    row["GSTPer"] = 0;

                row["ItemType"] = item.ItemType;

                row["UOM"] = item.UOM;
                row["ItemTypeDesc"] = item.ItemTypeDesc;
                row["Item"] = item.ItemName;
                row["Total"] = item.Total;
                //row["Total"] = CalculateTotal(Convert.ToDecimal(row["Qty"]), Convert.ToDecimal(row["ItemRate"]), Convert.ToDecimal(row["GSTAmt"]), Convert.ToDecimal(row["GSTPer"]));

                ds.Tables[0].Rows.Add(row);

                index++;
            }

            BindGrid(ds);
        }

        private bool ValidateControls(ITransaction tran)
        {
            bool isValid = true;
            spnVendor.Style["display"] = "none";

            if (ddlType.SelectedValue == "R" || ddlType.SelectedValue == "X")
            {
                if (ddlVendor.SelectedValue == "0")
                {
                    isValid = false;
                    spnVendor.Style["display"] = "block";
                    spnVendor.InnerText = "Please select vendor";
                }
            }

            return isValid;
        }

        private void SaveTransaction()
        {
            ITransaction tran = new TransactionEntity();
            DataSet ds = GetGridData();
            BuildEntity(tran);

            if (ValidateControls(tran))
            {
                int result = TransactionBLL.SaveTransaction(tran, (_rId > 0) ? "E" : "A", ds, _userId);
                if (_rId > 0)
                    Response.Redirect("~/Transaction/TranList.aspx");
                else
                    ClearTran();
            }
        }

        private void BuildEntity(ITransaction transaction)
        {
            transaction.Id = _rId;
            transaction.TranType = ddlType.SelectedValue;
            if (ddlFromLoc.SelectedIndex != -1)
                transaction.FromLocID = Convert.ToInt32(ddlFromLoc.SelectedValue);
            if (ddlToLoc.SelectedIndex != -1)
                transaction.ToLocID = Convert.ToInt32(ddlToLoc.SelectedValue);
            transaction.TranDate = Convert.ToDateTime(txtTranDate.Text);
            transaction.VendorId = Convert.ToInt32(ddlVendor.SelectedValue);
            transaction.RefDoc = txtVendorRef.Text;
            transaction.Narration = txtNarration.Text;
            transaction.TranNo = txtTranNo.Text;

        }

        private DataSet CreateDataSetSchema(bool allColumns)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            if (allColumns)
            {
                dt.Columns.Add(new DataColumn("Id", System.Type.GetType("System.Int32")));
                dt.Columns.Add(new DataColumn("ItemTypeDesc", System.Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("Item", System.Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("Total", System.Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn("UOM", System.Type.GetType("System.String")));
            }
            dt.Columns.Add(new DataColumn("pk_TranDetailID", System.Type.GetType("System.Int64")));
            dt.Columns.Add(new DataColumn("fk_TranID", System.Type.GetType("System.Int64")));
            dt.Columns.Add(new DataColumn("ItemType", System.Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("fk_rmID", System.Type.GetType("System.Int32")));
            dt.Columns.Add(new DataColumn("Qty", System.Type.GetType("System.Decimal")));
            dt.Columns.Add(new DataColumn("ItemRate", System.Type.GetType("System.Decimal")));
            dt.Columns.Add(new DataColumn("TaxableBasic", System.Type.GetType("System.Decimal")));
            dt.Columns.Add(new DataColumn("GSTPer", System.Type.GetType("System.Decimal")));
            dt.Columns.Add(new DataColumn("GSTAmt", System.Type.GetType("System.Decimal")));
            
            ds.Tables.Add(dt);
            return ds;
        }

        private void BindGrid(DataSet ds)
        {
            CalaculateTotalAmount(ds);
            gvwItem.DataSource = ds;
            gvwItem.DataBind();
        }

        private void EditItem(int id)
        {

            ddlSubItem.Enabled = false;
            ddlSubItemType.Enabled = false;

            hdnMode.Value = id.ToString();
            ddlSubItem.Items.Clear();
            txtItemUnit.Text = string.Empty;
            txtItemQty.Text = string.Empty;
            txtItemRate.Text = string.Empty;
            txtItemTot.Text = string.Empty;
            
            for (int j = 0; j < gvwItem.Rows.Count; j++)
            {
                GridViewRow row = gvwItem.Rows[j];
                int slNo = Convert.ToInt32(((Label)row.FindControl("lblSlNo")).Text);

                if (slNo == id)
                {
                    //dr["pk_RecipeItemID"] = ((HiddenField)row.FindControl("hdnItemTranId")).Value;
                    //dr["fk_RecipeID"] = ((HiddenField)row.FindControl("hdnTranId")).Value;
                    //dr["fk_ItemID"] = ((HiddenField)row.FindControl("hdnItemId")).Value;
                    ddlSubItemType.SelectedValue = ((HiddenField)row.FindControl("hdnType")).Value;
                    LoadSubItems();
                    ddlSubItem.SelectedValue = ((HiddenField)row.FindControl("hdnItemId")).Value;
                    txtItemUnit.Text = ((Label)row.FindControl("lblUnit")).Text;
                    txtItemQty.Text = ((Label)row.FindControl("lblQty")).Text;
                    txtItemRate.Text = ((Label)row.FindControl("lblRate")).Text;
                    txtGSTPer.Text = ((Label)row.FindControl("lblGSTPer")).Text;
                    txtItemTax.Text = ((Label)row.FindControl("lblTax")).Text;
                    txtItemTot.Text = ((Label)row.FindControl("lblAmt")).Text;
                }
            }
            //RemoveItem(id);
        }

        private void RemoveItem(int id)
        {
            DataSet ds = CreateDataSetSchema(true);
            int index = 0;
            for (int j = 0; j < gvwItem.Rows.Count; j++)
            {
                DataRow dr;
                GridViewRow row = gvwItem.Rows[j];
                dr = ds.Tables[0].NewRow();

                int slNo = Convert.ToInt32(((Label)row.FindControl("lblSlNo")).Text);

                if (slNo != id)
                {
                    index++;
                    LoadGridRow(row, dr, index);
                    ds.Tables[0].Rows.Add(dr);
                }
            }

            BindGrid(ds);
        }
        private void LoadGridRow(GridViewRow row, DataRow dr, int id)
        {
            dr["Id"] = id;
            dr["pk_TranDetailID"] = Convert.ToInt64(((HiddenField)row.FindControl("hdnItemTranId")).Value);
            //dr["fk_TranID"] = Convert.ToInt64(((HiddenField)row.FindControl("hdnTranId")).Value);
            dr["fk_RMID"] = Convert.ToInt32(((HiddenField)row.FindControl("hdnItemId")).Value);
            dr["ItemType"] = ((HiddenField)row.FindControl("hdnType")).Value;
            dr["ItemTypeDesc"] = ((Label)row.FindControl("lblType")).Text;
            dr["Item"] = ((Label)row.FindControl("lblItem")).Text;
            dr["UOM"] = ((Label)row.FindControl("lblUnit")).Text;
            dr["Qty"] = Convert.ToDecimal(((Label)row.FindControl("lblQty")).Text);
            dr["ItemRate"] = Convert.ToDecimal(((Label)row.FindControl("lblRate")).Text);
            dr["GSTPer"] = Convert.ToDecimal(((Label)row.FindControl("lblGSTPer")).Text);
            dr["GSTAmt"] = Convert.ToDecimal(((Label)row.FindControl("lblTax")).Text);
            dr["GSTPer"] = Convert.ToDecimal(((Label)row.FindControl("lblGSTPer")).Text);
            dr["TaxableBasic"] = Math.Round(Convert.ToDecimal(dr["Qty"]) * Convert.ToDecimal(dr["ItemRate"]), 2);
            dr["Total"] = CalculateTotal(Convert.ToDecimal(dr["Qty"]), Convert.ToDecimal(dr["ItemRate"]), Convert.ToDecimal(dr["GSTAmt"]), Convert.ToDecimal(dr["GSTPer"]));
        }

        private void CalaculateTotalAmount(DataSet ds)
        {
            if (ds.Tables.Count > 0)
            {
                decimal total = 0;
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    total += Convert.ToDecimal(row["Total"]);
                }
                txtTotalAmount.Text = Convert.ToString(Math.Round(total, 2));
                upTran.Update();
            }
        }

        #endregion

        private void DisplayItemTotal()
        {
            decimal qty = (!string.IsNullOrEmpty(txtItemQty.Text)) ? Convert.ToDecimal(txtItemQty.Text) : 0;
            decimal rate = (!string.IsNullOrEmpty(txtItemRate.Text)) ? Convert.ToDecimal(txtItemRate.Text) : 0;
            decimal GSTPer = (!string.IsNullOrEmpty(txtGSTPer.Text)) ? Convert.ToDecimal(txtGSTPer.Text) : 0;
            txtItemTax.Text = Convert.ToString(CalculateGST(qty, rate, GSTPer)); // (!string.IsNullOrEmpty(txtItemTax.Text)) ? Convert.ToDecimal(txtItemTax.Text) : 0;
            decimal tax = txtItemTax.Text.ToDecimal();
            txtItemTot.Text = Convert.ToString(CalculateTotal(qty, rate, tax, GSTPer));
            upItem.Update();
        }

        private void RateVisible(bool vis)
        {
            if (vis)
            {
                txtItemRate.Visible = true;
                txtItemTax.Visible = true;
                txtGSTPer.Visible = true;
                txtGSTPer.Visible = true;
                txtItemTot.Visible = true;
            }
            else
            {
                txtItemRate.Visible = false;
                txtItemTax.Visible = false;
                txtGSTPer.Visible = false;
                txtGSTPer.Visible = false;
                txtItemTot.Visible = false;
            }
        }
        protected void txtItemRate_TextChanged(object sender, EventArgs e)
        {
            DisplayItemTotal();
        }

        protected void txtItemTax_TextChanged(object sender, EventArgs e)
        {
            DisplayItemTotal();
        }

        protected void txtItemQty_TextChanged(object sender, EventArgs e)
        {
            DisplayItemTotal();
            txtItemRate.Focus();
            txtItemRate.Attributes.Add("onfocus", "this.select();");
        }
    }
}