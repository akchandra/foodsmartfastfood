﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RecipeList.aspx.cs" Inherits="FoodSmart.WebApp.Transaction.RecipeList" MasterPageFile="~/Site.Master" Title=":: FoodSmart :: Recipe Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="container" runat="Server">
    <div id="dvAsync" style="padding: 5px; display: none;">
        <div class="asynpanel">
            <div id="dvAsyncClose">
                <img alt="" src="../../Images/Close-Button.bmp" style="cursor: pointer;" onclick="ClearErrorState()" />
            </div>
            <div id="dvAsyncMessage">
            </div>
        </div>
    </div>
    <div id="headercaption">MANAGE RECIPE</div>
    <div style="width: 850px; margin: 0 auto; margin-top: 30px;">
        <fieldset style="width: 100%;">
            <legend>Search recipe</legend>
            <table>
                <tr>
                    <td>Select type:</td>
                    <td>
                        <asp:DropDownList ID="ddlType" runat="server" Width="75px" AutoPostBack="false" CssClass="chzn-select medium-select single">
                            <asp:ListItem Text="All" Value="A" />
                            <asp:ListItem Text="Finished" Value="F" />
                            <asp:ListItem Text="Semi finished" Value="S" />
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:TextBox ID="txtName" runat="server" CssClass="watermark" Width="240px" ForeColor="#747862"></asp:TextBox>
                        <cc1:TextBoxWatermarkExtender ID="txtWMEName" runat="server" TargetControlID="txtName" WatermarkText="Recipe name" WatermarkCssClass="watermark"></cc1:TextBoxWatermarkExtender>
                    </td>
                    <td>
                        <asp:Button ID="btnSearch" runat="server" Text="Search" Width="100px" OnClick="btnSearch_Click" />&nbsp;<asp:Button ID="btnReset" runat="server" Text="Reset" Width="100px" OnClick="btnReset_Click" /></td>
                </tr>
            </table>
        </fieldset>
        <asp:UpdateProgress ID="uProgressRecipe" runat="server" AssociatedUpdatePanelID="upRecipe">
            <ProgressTemplate>
                <div class="progress">
                    <div id="image">
                        <img src="../../Images/PleaseWait.gif" alt="" />
                    </div>
                    <div id="text">
                        Please Wait...
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <fieldset id="fsList" runat="server" style="width: 100%; min-height: 100px; margin-top: 10px;">
            <legend>Recipe List</legend>
            <div style="float: right; padding-bottom: 5px;">
                Results Per Page:<asp:DropDownList ID="ddlPaging" runat="server" Width="75px" AutoPostBack="true" CssClass="chzn-select medium-select single"
                    OnSelectedIndexChanged="ddlPaging_SelectedIndexChanged">
                    <asp:ListItem Text="10" Value="10" />
                    <asp:ListItem Text="30" Value="30" />
                    <asp:ListItem Text="50" Value="50" />
                    <asp:ListItem Text="100" Value="100" />
                </asp:DropDownList>&nbsp;&nbsp;            
                <asp:Button ID="btnAdd" runat="server" Text="Add" Width="130px" OnClick="btnAdd_Click" />
            </div>
            <br />
            <div>
                <asp:UpdatePanel ID="upRecipe" runat="server" UpdateMode="Conditional">
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="ddlPaging" EventName="SelectedIndexChanged" />
                    </Triggers>
                    <ContentTemplate>
                        <script type="text/javascript">
                            Sys.Application.add_load(LoadScript);
                        </script>
                        <asp:GridView ID="gvwRecipe" runat="server" AutoGenerateColumns="false" AllowPaging="true" BorderStyle="None" BorderWidth="0" OnPageIndexChanging="gvwRecipe_PageIndexChanging" OnRowDataBound="gvwRecipe_RowDataBound" OnRowCommand="gvwRecipe_RowCommand" Width="100%">
                            <PagerSettings Mode="NumericFirstLast" Position="Bottom" />
                            <PagerStyle CssClass="gridviewpager" />
                            <EmptyDataRowStyle CssClass="gridviewemptydatarow" />
                            <EmptyDataTemplate>No recipies Found</EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="Sl#">
                                    <HeaderStyle CssClass="gridviewheader" />
                                    <ItemStyle CssClass="gridviewitem" Width="5%" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="gridviewheader" />
                                    <ItemStyle CssClass="gridviewitem" Width="10%" />
                                    <HeaderTemplate>
                                        <asp:LinkButton ID="lnkHType" runat="server" CommandName="Sort" CommandArgument="Type" Text="Type"></asp:LinkButton>
                                    </HeaderTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="gridviewheader" />
                                    <ItemStyle CssClass="gridviewitem" Width="20%" />
                                    <HeaderTemplate>
                                        <asp:LinkButton ID="lnkHName" runat="server" CommandName="Sort" CommandArgument="Name" Text="Name"></asp:LinkButton>
                                    </HeaderTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="gridviewheader_num" />
                                    <ItemStyle CssClass="gridviewitem" Width="10%" HorizontalAlign="Right"/>
                                    <HeaderTemplate>
                                        <asp:LinkButton ID="lnkHQty" runat="server" CommandName="Sort" CommandArgument="Qty" Text="Tot.Qty."></asp:LinkButton>
                                    </HeaderTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="gridviewheader" />
                                    <ItemStyle CssClass="gridviewitem" Width="10%" />
                                    <HeaderTemplate>
                                        <asp:LinkButton ID="lnkHUnit" runat="server" CommandName="Sort" CommandArgument="Unit" Text="Unit"></asp:LinkButton>
                                    </HeaderTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="gridviewheader_num" />
                                    <ItemStyle CssClass="gridviewitem" Width="10%" HorizontalAlign="Right"/>
                                    <HeaderTemplate>
                                        <asp:LinkButton ID="lnkHRate" runat="server" CommandName="Sort" CommandArgument="Plates" Text="Plates"></asp:LinkButton>
                                    </HeaderTemplate>
                                </asp:TemplateField>
                                <%--<asp:TemplateField>
                                    <HeaderStyle CssClass="gridviewheader" />
                                    <ItemStyle CssClass="gridviewitem" Width="10%" />
                                    <HeaderTemplate>
                                        <asp:LinkButton ID="lnkHRPU" runat="server" CommandName="Sort" CommandArgument="RPU" Text="Rate / unit"></asp:LinkButton>
                                    </HeaderTemplate>
                                </asp:TemplateField>--%>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="gridviewheader" />
                                    <ItemStyle CssClass="gridviewitem" Width="15%" />
                                    <HeaderTemplate>
                                        <asp:LinkButton ID="lnkHProcessed" runat="server" CommandName="Sort" CommandArgument="ProcessedAt" Text="Processed at"></asp:LinkButton>
                                    </HeaderTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="gridviewheader" />
                                    <ItemStyle CssClass="gridviewitem" Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnEdit" runat="server" CommandName="Edit" ImageUrl="~/Images/edit.png" Height="16" Width="16" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderStyle CssClass="gridviewheader" />
                                    <ItemStyle CssClass="gridviewitem" Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnRemove" runat="server" CommandName="Remove" ImageUrl="~/Images/remove.png" Height="16" Width="16" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </fieldset>
    </div>
</asp:Content>
