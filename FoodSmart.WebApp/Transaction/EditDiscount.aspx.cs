﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FoodSmart.BLL;
using FoodSmart.Common;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using FoodSmart.Utilities.ResourceManager;
using System.Data;


namespace FoodSmart.WebApp.Transaction
{
    public partial class EditDiscount : System.Web.UI.Page
    {
        #region Private Member Variables

        private int _userId = 0;
        private int _uId = 0;
        private bool _canAdd = false;
        private bool _canEdit = false;
        private bool _canDelete = false;
        private bool _canView = false;

        #endregion

        #region Protected Event Handlers

        protected void Page_Load(object sender, EventArgs e)
        {
            RetriveParameters();
            CheckUserAccess();
            SetAttributes();
            txtStartDt.Attributes.Add("readonly", "readonly");
            txtEndDt.Attributes.Add("readonly", "readonly");
            
            if (!IsPostBack)
            {
                DisableAll();
                LoadDiscType();
                LoadData();
                
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            SaveRecord();
        }

        #endregion

        #region Private Methods

        private void DisableAll()
        {
            txtDiscPer.Enabled = false;
            txtMinAmount.Enabled = false;
            txtDiscAmt.Enabled = false;
            ddlBaseItem.Enabled = false;
            ddlFreeItem.Enabled = false;
            txtMinQty.Enabled = false;

            rfvDiscPer.Enabled = false;
            rfvCalcMethod.Enabled = false;
            rfvDiscAmt.Enabled = false;
            
        }

        private void Initialize()
        {
            txtDiscAmt.Text = "";
            txtMinAmount.Text = "";
            txtDiscPer.Text = "";
            txtMinQty.Text = "";
            ddlBaseItem.SelectedIndex = -1;
            ddlFreeItem.SelectedIndex = -1;
            //rngDisc.MaximumValue = "100";

        }

        private void LoadDiscType()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            //FoodSmart.BLL.itemService.SearchCriteria searchcriteria = new FoodSmart.BLL.itemService.SearchCriteria();

            //SearchCriteria searchcriteria = new SearchCriteria();
            //searchcriteria.QueryStat = "L";
            ddlDiscType.DataSource = null;

            ddlDiscType.Items.Insert(0, "Select Disc Type");
            ds = new DiscBLL().GetAllDiscType();
            dt = ds.Tables[0];

            DataRow dr = dt.NewRow();
            dr["DiscTypeName"] = "--Select--";
            dr["pk_DiscTypeID"] = 0;

            dt.Rows.InsertAt(dr, 0);

            if (dt.Rows.Count > 0)
            {
                ddlDiscType.DataSource = dt;
                ddlDiscType.DataTextField = "DiscTypeName";
                ddlDiscType.DataValueField = "pk_DiscTypeID";
                ddlDiscType.DataBind();
                ddlDiscType.SelectedValue = "0";
            }
        }

        private void LoadDiscCalcMethod()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            //FoodSmart.BLL.itemService.SearchCriteria searchcriteria = new FoodSmart.BLL.itemService.SearchCriteria();

            //SearchCriteria searchcriteria = new SearchCriteria();
            //searchcriteria.QueryStat = "L";
            ddlDiscCalcMethod.DataSource = null;

            ddlDiscCalcMethod.Items.Insert(0, "Select Calculation");
            ds = new DiscBLL().GetAllDiscCalcMethod(ddlDiscType.SelectedValue.ToInt(), 0);
            dt = ds.Tables[0];

            DataRow dr = dt.NewRow();
            dr["CalcDisc"] = "--Select--";
            dr["pk_DiscCalcID"] = 0;

            dt.Rows.InsertAt(dr, 0);

            if (dt.Rows.Count > 0)
            {
                ddlDiscCalcMethod.DataSource = dt;
                ddlDiscCalcMethod.DataTextField = "CalcDisc";
                ddlDiscCalcMethod.DataValueField = "pk_DiscCalcID";
                ddlDiscCalcMethod.DataBind();
                ddlDiscCalcMethod.SelectedValue = "0";
            }
        }

        private void RetriveParameters()
        {
            _userId = UserBLL.GetLoggedInUserId();

            //Get user permission.
            UserBLL.GetUserPermission(out _canAdd, out _canEdit, out _canDelete, out _canView);

            if (!ReferenceEquals(Request.QueryString["id"], null))
            {
                Int32.TryParse(GeneralFunctions.DecryptQueryString(Request.QueryString["id"].ToString()), out _uId);
            }
        }

        private void SetAttributes()
        {
            spnName.Style["display"] = "none";

            if (!IsPostBack)
            {
                if (_uId == -1) //Add mode
                {
                    if (!_canAdd) btnSave.Visible = false;
                }
                else
                {
                    if (!_canEdit) btnSave.Visible = false;
                }

                btnSave.ToolTip = ResourceManager.GetStringWithoutName("R00060");
                btnBack.ToolTip = ResourceManager.GetStringWithoutName("R00061");
                btnBack.OnClientClick = "javascript:return RedirectAfterCancelClick('ListDiscount.aspx','" + ResourceManager.GetStringWithoutName("R00025") + "')";
                spnName.InnerText = ResourceManager.GetStringWithoutName("R00046");
            }
        }

        private void CheckUserAccess()
        {
            if (!ReferenceEquals(Session[Constants.SESSION_USER_INFO], null))
            {
                IUser user = (IUser)Session[Constants.SESSION_USER_INFO];

                if (ReferenceEquals(user, null) || user.Id == 0)
                {
                    Response.Redirect("~/Login.aspx");
                }

                if (user.RoleID != (int)UserRole.Admin)
                {
                    Response.Redirect("~/Unauthorized.aspx");
                }
            }
            else
            {
                Response.Redirect("~/Login.aspx");
            }

            if (_uId == 0)
                Response.Redirect("~/Views/ManageUser.aspx");

            if (!_canView)
            {
                Response.Redirect("~/Unauthorized.aspx");
            }
        }

        private void LoadData()
        {
            DiscBLL oDiscbll = new DiscBLL();

            IDisc oDisc = (DiscEntity)oDiscbll.GetDiscForEdit(_uId);

            if (!ReferenceEquals(oDisc, null))
            {
                ddlDiscType.SelectedValue = oDisc.DiscTypeID.ToString();
                LoadDiscCalcMethod();
                ddlDiscCalcMethod_SelectedIndexChanged(null, null);
                txtDiscName.Text = oDisc.DiscName;
                txtDiscAmt.Text = oDisc.DiscAmount.ToString();
                txtDiscPer.Text = oDisc.DiscPer.ToString();
                txtStartDt.Text = Convert.ToString(oDisc.StartDate).Split(' ')[0];
                txtEndDt.Text = Convert.ToString(oDisc.EndDate).Split(' ')[0];
                txtMinAmount.Text = oDisc.ThresholeAmt.ToString();
                txtMinQty.Text = oDisc.ThresholeQty.ToString();
                ddlBaseItem.SelectedValue = oDisc.BaseItemID.ToString();
                ddlFreeItem.SelectedValue = oDisc.DiscItemID.ToString();
                ddlDiscCalcMethod.SelectedValue = oDisc.DiscCalcID.ToString();
            }
        }

        private bool ValidateControls(IDisc dis)
        {
            bool isValid = true;

            DataSet ds = new DiscBLL().GetAllDiscCalcMethod(ddlDiscType.SelectedValue.ToInt(), ddlDiscCalcMethod.SelectedValue.ToInt());
            string Action = ds.Tables[0].Rows[0]["CalcType"].ToString().Trim();

            if (Action == "P" && (string.IsNullOrEmpty(txtDiscPer.Text) || txtDiscPer.Text.ToDecimal() > 100)) // percentage on basic
            {
                isValid = false;
                spnDiscPer.Style["display"] = "";
            }

            if (Action == "F" && string.IsNullOrEmpty(txtDiscAmt.Text)) // amount 
            {
                isValid = false;
                spnDiscAmt.Style["display"] = "";
            }

            if (Action == "II" && ((ddlBaseItem.SelectedIndex < 1 || ddlFreeItem.SelectedIndex < 1))) // Item on Item free
            {
                isValid = false;
                SpnBaseItem.Style["display"] = "";
                spnDiscItem.Style["display"] = "";
            }

            if (Action == "IV" && ((ddlBaseItem.SelectedIndex < 1) || string.IsNullOrEmpty(txtMinAmount.Text))) // Amount on Item
            {
                isValid = false;
                SpnBaseItem.Style["display"] = "";
                spnMinAmount.Style["display"] = "";
            }

            if (txtStartDt.Text.ToDateTime() > txtEndDt.Text.ToDateTime())
            {
                isValid = false;
                spnEndDt.Style["display"] = "";
                spnStartDt.Style["display"] = "";
            }
            
            return isValid;
        }

        private void SaveRecord()
        {
            IDisc oCP = new DiscEntity();
            BuildDiscEntity(oCP);

            if (ValidateControls(oCP))
            {
                string strMode = "A";
                if (oCP.DiscAppID > 0)
                {
                    strMode = "E";
                }
                DiscBLL cpBLL = new DiscBLL();
                int result = cpBLL.SaveDiscounts(oCP, strMode);

                //if (result. != "Failure")
                ////{
                ////}

                ////if (result.HasMessage)
                //{
                //    msgbox.Text = "";
                if (strMode == "E")
                {
                    //SendEmail(user);
                    Response.Redirect("~/Transaction/ListDiscount.aspx");
                }
                else
                {
                    txtDiscName.Text = string.Empty;
                    txtDiscPer.Text = string.Empty;
                    txtDiscAmt.Text = string.Empty;
                    txtMinAmount.Text = string.Empty;
                    txtMinQty.Text = string.Empty;
                    txtStartDt.Text = string.Empty;
                    txtEndDt.Text = string.Empty;
                    ddlBaseItem.SelectedIndex = -1;
                    ddlDiscCalcMethod.SelectedIndex = 0;
                    ddlDiscType.SelectedIndex = 0;
                    ddlFreeItem.SelectedIndex = -1;
                }
            }
        }

        private void BuildDiscEntity(IDisc oDis)
        {
            oDis.DiscAppID = _uId;
            oDis.DiscName = txtDiscName.Text.ToUpper();
            oDis.DiscPer = txtDiscPer.Text.ToDecimal();
            oDis.DiscAmount = txtDiscAmt.Text.ToDecimal();
            oDis.StartDate = Convert.ToDateTime(txtStartDt.Text);
            oDis.EndDate = Convert.ToDateTime(txtEndDt.Text);
            oDis.BaseItemID = ddlBaseItem.SelectedValue.ToInt();
            oDis.DiscItemID = ddlFreeItem.SelectedValue.ToInt();
            oDis.DiscCalcID = ddlDiscCalcMethod.SelectedValue.ToInt();
            oDis.DiscTypeID = ddlDiscType.SelectedValue.ToInt();

            oDis.ThresholeAmt = txtMinAmount.Text.ToDecimal();
            oDis.ThresholeQty = txtMinQty.Text.ToInt();
            oDis.DiscStatus = true;
            oDis.CreatedBy = _userId;
        }
        #endregion

        protected void ddlDiscType_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadDiscCalcMethod();
        }

        protected void ddlDiscCalcMethod_SelectedIndexChanged(object sender, EventArgs e)
        {
            DisableAll();
            Initialize();
            DataSet ds = new DiscBLL().GetAllDiscCalcMethod(ddlDiscType.SelectedValue.ToInt(), ddlDiscCalcMethod.SelectedValue.ToInt());
            string Action = ds.Tables[0].Rows[0]["CalcType"].ToString().Trim();

            if (Action == "P") // percentage on basic
            {
                txtDiscPer.Enabled = true;
                txtMinAmount.Enabled = true;
                rfvDiscPer.Enabled = true;
            }

            if (Action == "F") // amount 
            {
                txtDiscAmt.Enabled = true;
                txtMinAmount.Enabled = true;
                rfvDiscAmt.Enabled = true;
            }

            if (Action == "II") // Item on Item free
            {
                ddlBaseItem.Enabled = true;
                ddlFreeItem.Enabled = true;
            }

            if (Action == "IV") // Amount on Item
            {
                ddlBaseItem.Enabled = true;
                txtMinAmount.Enabled = true;
                txtDiscPer.Enabled = true;
            }
        }
    }
}