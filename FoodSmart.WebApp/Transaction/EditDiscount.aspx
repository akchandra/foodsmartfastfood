﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditDiscount.aspx.cs" Inherits="FoodSmart.WebApp.Transaction.EditDiscount" MasterPageFile="~/Site.Master" Title=":: POS :: Add / Edit Coupon" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="FoodSmart.WebApp" Namespace="FoodSmart.WebApp.CustomControls" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">    
    <script type="text/javascript">
        LoadScript();
    </script>
    <script language="javascript" type="text/javascript">
        // this function check start date less then current date and also less then end date.
        function CheckForPastDate(sender, args) {
            var selectedDate = new Date();
            selectedDate = sender._selectedDate;
            var todayDate = new Date();
            var txtDate1 = document.getElementById('<%= txtEndDt.ClientID %>').value;
            var tostartDate = new Date(txtDate1);
            if (selectedDate.getDateOnly() < todayDate.getDateOnly()) {
                sender._selectedDate = todayDate;
                sender._textbox.set_Value(sender._selectedDate.format(sender._format));
                alert("Start Date is not less then current Date.");
            }
            else if (tostartDate < selectedDate.getDateOnly()) {
                sender._selectedDate = tostartDate;
                sender._textbox.set_Value(sender._selectedDate.format(sender._format));
                alert("Start Date must less then or Equal End date");

            }
        }
        // this function check end date not less then start date and start date not null.
        function CheckStartDate(sender, args) {
            var txtDate1 = document.getElementById('<%= txtStartDt.ClientID %>').value;
            var tostartDate = new Date(txtDate1);
            var enddateDate = new Date(sender._selectedDate);

            if (txtDate1.length > 0) {
                if (enddateDate < tostartDate) {
                    sender._selectedDate = tostartDate;
                    sender._textbox.set_Value(sender._selectedDate.format(sender._format));
                    alert("End Date must greater then or Equal start date");
                }
            }
            else {
                document.getElementById('<%= txtEndDt.ClientID %>').value = ' ';
                alert("Please Select Start Date.");
            }
        }
    </script>
    <style type="text/css">
        .style1
        {
            width: 200px;
            height: 25px;
        }
        .style2
        {
            height: 25px;
        }
    </style>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="container" runat="Server">
    <div id="headercaption">ADD / EDIT DISCOUNT</div>
    <div style="width:702px; padding-left:300px;">
        <fieldset style="width:100%;">
            <legend>Add / Edit Discount</legend>
            <table style="height: 300px;vertical-align:top; width: 647px;">
                <tr>
                    <td>

                        <table  border="0" cellpadding="3" cellspacing="3" 
                            style="width: 105%; height: 248px">
                            <tr>
                                <td>Discount Name:<span class="errormessage1">*</span></td>
                                <td>
                                    <asp:TextBox ID="txtDiscName" runat="server" CssClass="textboxuppercase" TabIndex="1"
                                        MaxLength="30" Width="323px" height="20px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtDiscName" InitialValue="" ErrorMessage="This field is required*" CssClass="errormessage" ValidationGroup="Save" Display="Dynamic"></asp:RequiredFieldValidator>
                                    <span id="spnName" runat="server" class="errormessage" style="display:none;"></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="style1">Discount Type:<span class="errormessage1">*</span></td>
                                <td class="style2">
                                    <asp:DropDownList ID="ddlDiscType" runat="server" Width="250px" 
                                        CssClass="dropdownlist" AutoPostBack="true"
                                        onselectedindexchanged="ddlDiscType_SelectedIndexChanged">
                                        <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                    </asp:DropDownList>
                                    <br /><asp:RequiredFieldValidator ID="rfvDiscType" runat="server" ControlToValidate="ddlDiscType" InitialValue="0" ErrorMessage="This field is required*" CssClass="errormessage" ValidationGroup="Save" Display="Dynamic"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">Calcuation Method:<span class="errormessage1">*</span></td>
                                <td>
                                    <asp:DropDownList ID="ddlDiscCalcMethod" runat="server" Width="250px" 
                                        CssClass="dropdownlist" AutoPostBack="true"
                                        onselectedindexchanged="ddlDiscCalcMethod_SelectedIndexChanged"><asp:ListItem Value="0" Text="--Select--"></asp:ListItem></asp:DropDownList>
                                    <br /><asp:RequiredFieldValidator ID="rfvCalcMethod" runat="server" ControlToValidate="ddlDiscCalcMethod" InitialValue="0" ErrorMessage="This field is required*" CssClass="errormessage" ValidationGroup="Save" Display="Dynamic"></asp:RequiredFieldValidator>

                                    <asp:RequiredFieldValidator ID="rfvStartNo" runat="server" ControlToValidate="ddlDiscCalcMethod" InitialValue="" ErrorMessage="This field is required*" CssClass="errormessage" ValidationGroup="Save" Display="Dynamic"></asp:RequiredFieldValidator>
                                    <span id="spnStartNo" runat="server" class="errormessage" style="display:none;"></span>
                                </td>
                            </tr>
                            
                            <tr>
                                <td valign="top">Start Date:<span class="errormessage1">*</span></td>
                                <td>
                                    <asp:TextBox ID="txtStartDt" runat="server" TabIndex="5" MaxLength="30" Width="102px" height="20px" Style="text-align: left">
                                    </asp:TextBox>
                                    <cc1:CalendarExtender ID="ceStartDate" TargetControlID="txtStartDt" runat="server" OnClientDateSelectionChanged="CheckForPastDate" Format="dd/MM/yyyy" Enabled="True" />
                                    <asp:RequiredFieldValidator ID="rfvStartDate" runat="server" ControlToValidate="txtStartDt" ErrorMessage="This field is required*" CssClass="errormessage" ValidationGroup="Save" Display="Dynamic"></asp:RequiredFieldValidator>
                                    <span id="spnStartDt" runat="server" class="errormessage" style="display:none;"></span>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">End Date:<span class="errormessage1">*</span></td>
                                <td>
                                    <asp:TextBox ID="txtEndDt" runat="server" TabIndex="6" MaxLength="30" Width="102px" height="20px" Style="text-align: left">
                                    </asp:TextBox>
                                    <cc1:CalendarExtender ID="ceEndDate" TargetControlID="txtEndDt" runat="server" OnClientDateSelectionChanged="CheckStartDate" Format="dd/MM/yyyy" Enabled="True" />
                                    <asp:RequiredFieldValidator ID="rfvEndDate" runat="server" ControlToValidate="txtEndDt" ErrorMessage="This field is required*" CssClass="errormessage" ValidationGroup="Save" Display="Dynamic"></asp:RequiredFieldValidator>
                                    <span id="spnEndDt" runat="server" class="errormessage" style="display:none;"></span>   
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblDiscPer" runat="server" Text="Discount Percent:"></asp:Label>
                                </td>
                                <td>
                                    <cc2:CustomTextBox ID="txtDiscPer" runat="server" CssClass="numerictextbox" TabIndex="4" Width="102px" Height="20px" 
                                        Type="Decimal" MaxLength="6" Precision="7" Scale="2">
                                    </cc2:CustomTextBox>
                                    <asp:RangeValidator runat="server" Display="Dynamic" ID="rngDisc" ValidationGroup="Save" SetFocusOnError="true" 
                                        ControlToValidate="txtDiscPer" Type ="Double" MinimumValue="0" MaximumValue="100" ErrorMessage="Range 1 to 100" CssClass="errormessage">
                                    </asp:RangeValidator>

                                    <asp:RequiredFieldValidator ID="rfvDiscPer" runat="server" ControlToValidate="txtDiscPer" InitialValue="" ErrorMessage="This field is required*" CssClass="errormessage" ValidationGroup="Save"></asp:RequiredFieldValidator>
                                    <span id="spnDiscPer" runat="server" class="errormessage" style="display:none;"></span>
                                </td>
                            </tr>
                            <tr>
                                
                                <td><asp:Label ID="lblDiscAmt" runat="server" Text="Discount Amount:"></asp:Label></td>
                                <td>
                                    <cc2:CustomTextBox ID="txtDiscAmt" runat="server" CssClass="numerictextbox" TabIndex="7" Width="102px" Type="Decimal" MaxLength="13" Precision="10" Scale="2">
                                    </cc2:CustomTextBox>
                                    <asp:RequiredFieldValidator ID="rfvDiscAmt" runat="server" ControlToValidate="txtDiscAmt" InitialValue="" ErrorMessage="This field is required*" CssClass="errormessage" ValidationGroup="Save" Display="Dynamic"></asp:RequiredFieldValidator>
                                    <span id="spnDiscAmt" runat="server" class="errormessage" style="display:none;"></span>
                                </td>
                            </tr>
                            <tr>
                                
                                <td><asp:Label ID="lblBaseItem" runat="server" Text="Base Item:"></asp:Label></td>
                                <td>
                                    <asp:DropDownList ID="ddlBaseItem" runat="server" Width="250px" CssClass="dropdownlist">
                                        <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                    </asp:DropDownList>
<%--                                    <asp:RequiredFieldValidator ID="rfvBaseItem" runat="server" ControlToValidate="ddlBaseItem" InitialValue="" ErrorMessage="This field is required*" CssClass="errormessage" ValidationGroup="Save" Display="Dynamic"></asp:RequiredFieldValidator>
--%>                                    <span id="SpnBaseItem" runat="server" class="errormessage" style="display:none;"></span>
                                </td>
                            </tr>
                            <tr>
                                
                                <td><asp:Label ID="lblDiscItem" runat="server" Text="Free Item:"></asp:Label></td>
                                <td>
                                     <asp:DropDownList ID="ddlFreeItem" runat="server" Width="250px" CssClass="dropdownlist">
                                        <asp:ListItem Value="0" Text="--Select--"></asp:ListItem>
                                    </asp:DropDownList>
<%--                                    <asp:RequiredFieldValidator ID="rfvDiscItem" runat="server" ControlToValidate="ddlFreeItem" InitialValue="" ErrorMessage="This field is required*" CssClass="errormessage" ValidationGroup="Save" Display="Dynamic"></asp:RequiredFieldValidator>
--%>                                    <span id="spnDiscItem" runat="server" class="errormessage" style="display:none;"></span>
                                </td>
                            </tr>
                            <tr>
                                
                                <td><asp:Label ID="lblMinAmount" runat="server" Text="Disc Eligibility (Amt):"></asp:Label></td>
                                <td>
                                    <cc2:CustomTextBox ID="txtMinAmount" runat="server" CssClass="numerictextbox" TabIndex="7" 
                                        Width="102px" Type="Decimal" MaxLength="13" Precision="10" Scale="2">
                                    </cc2:CustomTextBox>
                                    <%--<asp:RequiredFieldValidator ID="rfvMinAmount" runat="server" ControlToValidate="txtMinAmount" InitialValue="" ErrorMessage="This field is required*" CssClass="errormessage" ValidationGroup="Save" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                                    <span id="spnMinAmount" runat="server" class="errormessage" style="display:none;"></span>
                                </td>
                            </tr>
                            <tr>
                                
                                <td><asp:Label ID="lblMinQty" runat="server" Text="Minimum Eligibility (Qty):"></asp:Label></td>
                                <td>
                                    <cc2:CustomTextBox ID="txtMinQty" runat="server" CssClass="numerictextbox" TabIndex="7" Width="102px" Type="Decimal" MaxLength="13" Precision="10" Scale="2">
                                    </cc2:CustomTextBox>
                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtMinQty" InitialValue="" ErrorMessage="This field is required*" CssClass="errormessage" ValidationGroup="Save" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                                    <span id="spnMinQty" runat="server" class="errormessage" style="display:none;"></span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="display:none" class="style1">
                        <br />
                    </td>
                </tr>                
                <tr>
                    <td colspan="2">
                        <asp:Button ID="btnSave" runat="server" Text="Save" ValidationGroup="Save" OnClick="btnSave_Click" />&nbsp;&nbsp;
                        <asp:Button ID="btnBack" runat="server" Text="Back"/>&nbsp;&nbsp;<asp:Label ID="msgbox" runat="server" Text="" ForeColor="Red" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </div>
</asp:Content>