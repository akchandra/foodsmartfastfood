﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using FoodSmart.DAL.DbManager;
using FoodSmart.BLL;
using FoodSmart.DAL;
using FoodSmart.Entity;

namespace FoodSmartWebservice
{
	/// <summary>
	/// Summary description for Service1
	/// </summary>
	[WebService(Namespace = "http://tempuri.org/")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[System.ComponentModel.ToolboxItem(false)]
	// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
	// [System.Web.Script.Services.ScriptService]
    public class DBInteractionService : System.Web.Services.WebService
    {
        #region General
        [WebMethod]
        public bool IsUnique(string tableName, string colName, string val)
        {

            DbQuery dquery = new DbQuery("Select count(*) from " + tableName + " where " + colName + " = '" + val + "'", true);
            bool returnval = false;
            try
            {
                returnval = Convert.ToInt32(dquery.GetScalar()) > 0 ? false : true;
            }
            catch (Exception)
            {


            }

            return returnval;

        }

        [WebMethod]
        public DataSet PopulateDDLDS(string tableName, string textField, string valuefield)
        {

            DbQuery dquery = new DbQuery("Select [" + textField + "] ListItemValue, [" + valuefield + "] ListItemText from dbo." + tableName + " order by ListItemText", true);

            return dquery.GetTables();

        }

        [WebMethod]
        public DataSet PopulateDDLDS_1(string tableName, string textField, string valuefield, bool isDSR)
        {

            DbQuery dquery = new DbQuery("Select [" + textField + "] ListItemValue, [" + valuefield + "] ListItemText from " + tableName + " order by ListItemText", true);

            return dquery.GetTables();

        }

        [WebMethod]
        public DataSet PopulateDDLDS_2(string tableName, string textField, string valuefield, string WhereClause)
        {
            DbQuery dquery = new DbQuery("Select [" + textField + "] ListItemValue, [" + valuefield + "] ListItemText from dbo." + tableName + " " + WhereClause, true);
            return dquery.GetTables();
        }

        [WebMethod]
        public DataSet PopulateDDLDS_3(string tableName, string textField, string valuefield, string WhereClause, bool isDSR)
        {

            DbQuery dquery = new DbQuery("Select [" + textField + "] ListItemValue, [" + valuefield + "] ListItemText from " + tableName + " " + WhereClause + ") order by ListItemText", true);

            return dquery.GetTables();

        }

        [WebMethod]
        public int GetId(string ItemName, string ItemValue)
        {
            DbQuery dquery = new DbQuery("prcGetId");
            dquery.AddVarcharParam("@ItemName", 15, ItemName);
            dquery.AddVarcharParam("@ItemValue", 50, ItemValue);
            return Convert.ToInt32(dquery.GetScalar());
        }

        [WebMethod]
        public void DeleteGeneral(string formName, int pk_tableID)
        {
            string ProcName = "prcDeleteGeneral";
            DbQuery dquery = new DbQuery(ProcName);
            dquery.AddIntegerParam("@pk_tableID", pk_tableID);
            dquery.AddVarcharParam("@formName", 20, formName);
            dquery.RunActionQuery();
        }

        [WebMethod]
        public int InvoiceDateCheck(DateTime dt)
        {
            DbQuery dquery = new DbQuery("prcInvoiceDateCheck");
            dquery.AddDateTimeParam("@StaxDate", dt);
            return Convert.ToInt32(dquery.GetScalar());
        }
        #endregion

        #region Financial Year

        [WebMethod]
        public DataSet GetFinancialYear(int QueryType, int YearID)
        {
            string ProcName = "dbo.SP_SELECTYEAR";
            DbQuery dquery = new DbQuery(ProcName);

            dquery.AddIntegerParam("@YrID", YearID);
            dquery.AddIntegerParam("@Query", QueryType);

            return dquery.GetTables();
        }

        [WebMethod]
        public DataSet GetCompany()
        {
            string ProcName = "[admin].[sp_GetCompanyDetail]";
            DbQuery dquery = new DbQuery(ProcName);
            return dquery.GetTables();
        }

        #endregion

        #region STax

        [WebMethod]
        public DataSet GetSTaxDate(DateTime? Startdt)
        {
            string ProcName = "admin.prcGetSTaxDate";
            DbQuery dquery = new DbQuery(ProcName);

            dquery.AddDateTimeParam("@StartDate", Startdt);


            return dquery.GetTables();
        }

        [WebMethod]
        public int SaveParameters(int SecDep, string WebRptURL, int GraceTime, int NoChargePeriod)
        {
            string ProcName = "[admin].[prcSaveParameters]";
            DbQuery dquery = new DbQuery(ProcName);

            dquery.AddIntegerParam("@SecDep", SecDep);
            ////dquery.AddDecimalParam("@ServiceTaxPer", 6, 2, SerTaxPer);
            ////dquery.AddDecimalParam("@SBTPer", 6, 2, sbtPer);
            ////dquery.AddIntegerParam("@CalculationMethod", CalculationMethod);
            dquery.AddVarcharParam("@WebRptURL",200, WebRptURL);
            dquery.AddIntegerParam("@GraceTime", GraceTime);
            dquery.AddIntegerParam("@NoChargePeriod", NoChargePeriod);

            dquery.RunActionQuery();
            return 0;
        }

        [WebMethod]
        public DataSet GetSTax(int pk_StaxID, DateTime? StartDate)
        {
            string ProcName = "admin.prcGetSTax";
            DbQuery dquery = new DbQuery(ProcName);
            dquery.AddIntegerParam("@pk_StaxID", pk_StaxID);
            dquery.AddDateTimeParam("@StartDate", StartDate);

            return dquery.GetTables();
        }

        [WebMethod]
        public int AddEditSTax(int userID, int pk_StaxID, DateTime? StartDate, decimal TaxAddCess, decimal TaxCess, decimal TaxPer, bool TaxStatus, bool isEdit)
        {
            string ProcName = "admin.prcAddEditSTax";
            DbQuery dquery = new DbQuery(ProcName);
            dquery.AddIntegerParam("@userID", userID);
            dquery.AddIntegerParam("@pk_StaxID", pk_StaxID);
            dquery.AddDateTimeParam("@StartDate", StartDate);
            dquery.AddDecimalParam("@TaxAddCess", 6, 2, TaxAddCess);
            dquery.AddDecimalParam("@TaxCess", 6, 2, TaxCess);
            dquery.AddDecimalParam("@TaxPer", 6, 2, TaxPer);
            dquery.AddBooleanParam("@TaxStatus", TaxStatus);
            dquery.AddBooleanParam("@isEdit", isEdit);

            return dquery.RunActionQuery();
        }

        #endregion


        #region Import Export"

        //AR 07/06/2017
        [WebMethod]
        public DataSet ImportLocationFromWeb()
        {
            SearchCriteria searchCriteria = new SearchCriteria();
            LocationBLL oLoc = new LocationBLL();
            return oLoc.GetAllLoc(searchCriteria);
        }

        [WebMethod]
        public DataSet ImportUsersFromWeb()
        {
            SearchCriteria searchCriteria = new SearchCriteria();
            return UserDAL.ImportUsersFromWeb(searchCriteria);
        }

        [WebMethod]
        public bool ExportFromLocal(DataSet ds)
        {
            try
            {
                bool retVal = CommonBLL.ImportFromLocal(ds);
                return retVal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod]
        public DataSet ImportFromWeb()
        {
            return CommonBLL.ExportMasterFromWeb();
        }

        [WebMethod]
        public DataSet CheckForCoupon(string prefix, int CouponNo, string QueryType)
        {
            return CouponDAL.CheckCoupon(prefix, CouponNo, QueryType);
        }

        #endregion
    }
}