﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using FoodSmart.DAL;
using FoodSmart.Entity;


namespace FoodSmartWebservice
{
	/// <summary>
	/// Summary description for Service1
	/// </summary>
	[WebService(Namespace = "http://tempuri.org/")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[System.ComponentModel.ToolboxItem(false)]
	// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
	// [System.Web.Script.Services.ScriptService]
    public class UserService : System.Web.Services.WebService
    {
        #region User
        [WebMethod]
        public DataSet GetAllUsers(SearchCriteria searchCriteria)
        {
            return UserDAL.GetAllUsers(searchCriteria);
        }

        [WebMethod]
        public DataSet GetAllRoles()
        {
            return UserDAL.GetAllRoles();
        }

        [WebMethod]
        public UserEntity GetUserForEdit(int ID, string CalledFrom, int UserID)
        {
            return UserDAL.GetUserForEdit(ID, CalledFrom);
        }

        [WebMethod]
        public UserEntity ValidateUser(UserEntity user)
        {
            UserDAL.ValidateUser(user);
            return user;
        }

        
        private void SetDefaultSearchCriteriaForUser(SearchCriteria searchCriteria)
        {
            searchCriteria.SortExpression = "UserName";
            searchCriteria.SortDirection = "ASC";
        }

        //[WebMethod]
        //public List<UserEntity> GetAllUserList(SearchCriteria searchCriteria)
        //{
        //    return UserDAL.GetUserList(false, searchCriteria);
        //}

        //[WebMethod]
        //public List<UserEntity> GetActiveUserList()
        //{
        //    SearchCriteria searchCriteria = new SearchCriteria();
        //    SetDefaultSearchCriteriaForUser(searchCriteria);
        //    return UserDAL.GetUserList(true, searchCriteria);
        //}

        //[WebMethod]
        //public UserEntity GetUser(int userId)
        //{
        //    SearchCriteria searchCriteria = new SearchCriteria();
        //    SetDefaultSearchCriteriaForUser(searchCriteria);
        //    return UserDAL.GetUser(userId, false, searchCriteria);
        //}

        //[WebMethod]
        //public int GetUserLoc(int userID)
        //{
        //    return UserDAL.GetUserLoc(userID);
        //}

        [WebMethod]
        public DataSet GetUserById(string LoginId)
        {
            return UserDAL.GetUserById(LoginId);
        }

        [WebMethod]
        public string SaveUser(UserEntity user, int modifiedBy)
        {
            int result = 0;
            string errMessage = string.Empty;
            result = UserDAL.SaveUser(user, modifiedBy);

            switch (result)
            {
                case 1:
                    errMessage = "Save Error";
                    break;
                default:
                    break;
            }

            return errMessage;
        }

        [WebMethod]
        public void DeleteUser(int userId, int modifiedBy)
        {
            UserDAL.DeleteUser(userId, modifiedBy);
        }

        [WebMethod]
        public bool ChangePassword(UserEntity user)
        {
            return UserDAL.ChangePassword(user);
        }

        [WebMethod]
        public bool CheckDuplicateLogin(string LoginID)
        {
            return UserDAL.CheckDuplicateLogin(LoginID);
        }

        [WebMethod]
        public void ResetPassword(UserEntity user, int modifiedBy)
        {
            UserDAL.ResetPassword(user, modifiedBy);
        }

        [WebMethod]
        public DataSet GetAdminUsers(int LoggedUserId, int locationId)
        {
            return UserDAL.GetAdminUsers(LoggedUserId, locationId);
        }

        [WebMethod]
        public int AddEditUser(UserEntity usr, string mode, int CreatedBy)
        {
            return UserDAL.AddEditUser(usr, mode, CreatedBy);
        }

        [WebMethod]
        public void SavePOSUserLogin(UserEntity usr, string UpdateType)
        {
            UserDAL.SavePOSUserLogin(usr, UpdateType);
        }


        #endregion

       

        #region Role

        private void SetDefaultSearchCriteriaForRole(SearchCriteria searchCriteria)
        {
            searchCriteria.SortExpression = "Role";
            searchCriteria.SortDirection = "ASC";
        }

        //[WebMethod]
        //public List<RoleEntity> GetAllRole(SearchCriteria searchCriteria)
        //{
        //    return UserDAL.GetRole(false, searchCriteria);
        //}

        //[WebMethod]
        //public List<RoleEntity> GetActiveRole()
        //{
        //    SearchCriteria searchCriteria = new SearchCriteria();
        //    SetDefaultSearchCriteriaForRole(searchCriteria);
        //    return UserDAL.GetRole(true, searchCriteria);
        //}

        [WebMethod]
        public RoleEntity GetRole(int roleId)
        {
            SearchCriteria searchCriteria = new SearchCriteria();
            SetDefaultSearchCriteriaForRole(searchCriteria);
            return UserDAL.GetRole(roleId, false, searchCriteria);
        }

        [WebMethod]
        public int SaveRole(RoleEntity role, string xmlDoc , int CompID, int modifiedBy)
        {
            
            return UserDAL.SaveRole(role, CompID, xmlDoc, modifiedBy);
            
        }

        [WebMethod]
        public void DeleteRole(int roleId, int modifiedBy)
        {
            UserDAL.DeleteRole(roleId, modifiedBy);
        }

        [WebMethod]
        public void ChangeRoleStatus(int roleId, bool status, int modifiedBy)
        {
            UserDAL.ChangeRoleStatus(roleId, status, modifiedBy);
        }

        //[WebMethod]
        //public List<RoleMenuEntity> GetMenuByRole(int roleId, int mainId)
        //{
        //    return UserDAL.GetMenuByRole(roleId, mainId);
        //}

        [WebMethod]
        public UserPermission GetMenuAccessByUser(int userId, int menuId)
        {
            UserPermission userPermission = new UserPermission();

            RoleMenuEntity roleMenuAccess = UserDAL.GetMenuAccessByUser(userId, menuId);

            if (!ReferenceEquals(roleMenuAccess, null))
            {
                userPermission.CanAdd = roleMenuAccess.CanAdd;
                userPermission.CanEdit = roleMenuAccess.CanEdit;
                userPermission.CanDelete = roleMenuAccess.CanDelete;
                userPermission.CanView = roleMenuAccess.CanView;
            }

            return userPermission;
        }

       [WebMethod]
        public DataTable GetUserSpecificMenuList(int UserID)
        {
            return UserDAL.GetUserSpecificMenuList(UserID);
        }

        #endregion

    }
}