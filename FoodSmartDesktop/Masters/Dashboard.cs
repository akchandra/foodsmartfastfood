﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FoodSmart.BLL;
using FoodSmart.Common;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using FoodSmart.Utilities.Cryptography;
using FoodSmart.Utilities.ResourceManager;

namespace FoodSmartDesktop.Masters
{
    public partial class Dashboard : Form
    {
        public Dashboard()
        {
            InitializeComponent();
        }


        private void Dashboard_Load(object sender, EventArgs e)
        {
            this.Location = new Point(0, 0);
            this.Size = Screen.PrimaryScreen.WorkingArea.Size;

            foreach (Control item in this.Controls)
            {
                item.Anchor = (AnchorStyles.Bottom | AnchorStyles.Right);
            }

            label1.BackColor = System.Drawing.Color.Transparent;
            label2.BackColor = System.Drawing.Color.Transparent;
            lblDate.BackColor = System.Drawing.Color.Transparent;
            lblExit.BackColor = System.Drawing.Color.Transparent;
            lblWelcome.BackColor = System.Drawing.Color.Transparent;
            lblChangePW.BackColor = System.Drawing.Color.Transparent;
            wlbWebApp.Text = "Web Reports Login ";

            lblDate.Text = "Today : " + GeneralFunctions.LoginInfo.CurrDate.ToString("dd-MMM-yy");
            lblWelcome.Text = "Welcome " + GeneralFunctions.LoginInfo.UserFullName;
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void lblChangePW_Click(object sender, EventArgs e)
        {
            Masters.ChangePassword item = new Masters.ChangePassword();
            item.ShowDialog();
        }

        private void btnUser_Click(object sender, EventArgs e)
        {
            POS.ManageCashierTran User = new POS.ManageCashierTran();
            User.ShowDialog();
        }

        private void btnCards_Click(object sender, EventArgs e)
        {
            //Masters.ManageCard card = new Masters.ManageCard();
            //card.ShowDialog();
        }

        private void btnCounters_Click(object sender, EventArgs e)
        {
            Masters.ManageRestaurant rest = new Masters.ManageRestaurant();
            rest.ShowDialog();
        }

        private void btnItemGroup_Click(object sender, EventArgs e)
        {
            Masters.ManageItemGroup ig = new Masters.ManageItemGroup();
            ig.ShowDialog();
        }

        #region Mouser Movement
        private void btnUser_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnUser_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnCards_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnCards_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnCounters_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnCounters_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnItemGroup_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnItemGroup_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnItems_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnItems_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnItemRates_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnItemRates_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnTaxes_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnTaxes_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnReports_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnReports_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnSetting_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnSetting_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void lblExit_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void lblExit_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void lblChangePW_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void lblChangePW_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        #endregion

        private void btnItems_Click(object sender, EventArgs e)
        {
            Masters.ManageItems item = new Masters.ManageItems();
            item.ShowDialog();
        }

        private void btnItemRates_Click(object sender, EventArgs e)
        {
            Masters.ManageItemRate itemrt = new Masters.ManageItemRate();
            itemrt.ShowDialog();
        }

        private void btnTaxes_Click(object sender, EventArgs e)
        {
            //Masters.ManageHolidays holiday = new Masters.ManageHolidays();
            //holiday.ShowDialog();
        }

        private void wlbWebApp_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://103.253.125.136:81/Login.aspx");
        }

        private void btnParkingRate_Click(object sender, EventArgs e)
        {
            //Masters.ManageParkingRate ParkingRate = new Masters.ManageParkingRate();
            //ParkingRate.ShowDialog();
        }

        private void btnSetting_Click(object sender, EventArgs e)
        {
            Masters.manageParameters ParkingRate = new Masters.manageParameters();
            ParkingRate.ShowDialog();
        }
    }
}
