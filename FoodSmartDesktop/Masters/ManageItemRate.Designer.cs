﻿namespace FoodSmartDesktop.Masters
{
    partial class ManageItemRate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ManageItemRate));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblUserAddEdit = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.lblExit = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lbltoday = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ddlDate = new System.Windows.Forms.ComboBox();
            this.ddlGroup = new System.Windows.Forms.ComboBox();
            this.ddlLoc = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.ddlRestName = new System.Windows.Forms.ComboBox();
            this.txtItemName = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblPageNo = new System.Windows.Forms.Label();
            this.picMoveFirst = new System.Windows.Forms.PictureBox();
            this.picMovePrev = new System.Windows.Forms.PictureBox();
            this.picMoveLast = new System.Windows.Forms.PictureBox();
            this.picMoveNext = new System.Windows.Forms.PictureBox();
            this.btnUpdateRate = new System.Windows.Forms.Button();
            this.btnNewRate = new System.Windows.Forms.Button();
            this.dtNewRate = new System.Windows.Forms.DateTimePicker();
            this.dvgItem = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picMoveFirst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMovePrev)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMoveLast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMoveNext)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dvgItem)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(199)))), ((int)(((byte)(233)))));
            this.panel1.Controls.Add(this.lblUserAddEdit);
            this.panel1.Controls.Add(this.lblUserName);
            this.panel1.Controls.Add(this.lblExit);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(3, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(962, 52);
            this.panel1.TabIndex = 3;
            // 
            // lblUserAddEdit
            // 
            this.lblUserAddEdit.BackColor = System.Drawing.Color.Transparent;
            this.lblUserAddEdit.Font = new System.Drawing.Font("Arial", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserAddEdit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.lblUserAddEdit.Location = new System.Drawing.Point(410, 16);
            this.lblUserAddEdit.Name = "lblUserAddEdit";
            this.lblUserAddEdit.Size = new System.Drawing.Size(146, 22);
            this.lblUserAddEdit.TabIndex = 23;
            this.lblUserAddEdit.Text = "User Add/Edit";
            // 
            // lblUserName
            // 
            this.lblUserName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(199)))), ((int)(((byte)(233)))));
            this.lblUserName.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.lblUserName.Location = new System.Drawing.Point(677, 16);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(159, 22);
            this.lblUserName.TabIndex = 21;
            this.lblUserName.Text = "Welcome ";
            // 
            // lblExit
            // 
            this.lblExit.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExit.ForeColor = System.Drawing.Color.DarkRed;
            this.lblExit.Image = ((System.Drawing.Image)(resources.GetObject("lblExit.Image")));
            this.lblExit.Location = new System.Drawing.Point(881, 5);
            this.lblExit.Name = "lblExit";
            this.lblExit.Size = new System.Drawing.Size(63, 47);
            this.lblExit.TabIndex = 1;
            this.lblExit.Click += new System.EventHandler(this.lblExit_Click);
            this.lblExit.MouseLeave += new System.EventHandler(this.lblExit_MouseLeave);
            this.lblExit.MouseHover += new System.EventHandler(this.lblExit_MouseHover);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Monotype Corsiva", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
            this.label1.Location = new System.Drawing.Point(3, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(247, 42);
            this.label1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Black;
            this.panel2.Controls.Add(this.lbltoday);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Location = new System.Drawing.Point(-1, 543);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(962, 33);
            this.panel2.TabIndex = 4;
            // 
            // lbltoday
            // 
            this.lbltoday.BackColor = System.Drawing.Color.Transparent;
            this.lbltoday.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltoday.ForeColor = System.Drawing.Color.White;
            this.lbltoday.Location = new System.Drawing.Point(11, 6);
            this.lbltoday.Name = "lbltoday";
            this.lbltoday.Size = new System.Drawing.Size(159, 22);
            this.lbltoday.TabIndex = 22;
            this.lbltoday.Text = "Today";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Snow;
            this.label3.Location = new System.Drawing.Point(831, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 16);
            this.label3.TabIndex = 1;
            this.label3.Text = "Powered By Europia";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox1.Controls.Add(this.ddlDate);
            this.groupBox1.Controls.Add(this.ddlGroup);
            this.groupBox1.Controls.Add(this.ddlLoc);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.ddlRestName);
            this.groupBox1.Controls.Add(this.txtItemName);
            this.groupBox1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox1.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox1.Location = new System.Drawing.Point(3, 59);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Size = new System.Drawing.Size(961, 55);
            this.groupBox1.TabIndex = 27;
            this.groupBox1.TabStop = false;
            // 
            // ddlDate
            // 
            this.ddlDate.FormattingEnabled = true;
            this.ddlDate.Location = new System.Drawing.Point(775, 14);
            this.ddlDate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ddlDate.Name = "ddlDate";
            this.ddlDate.Size = new System.Drawing.Size(121, 28);
            this.ddlDate.TabIndex = 22;
            this.ddlDate.SelectedIndexChanged += new System.EventHandler(this.ddlDate_SelectedIndexChanged);
            // 
            // ddlGroup
            // 
            this.ddlGroup.FormattingEnabled = true;
            this.ddlGroup.Location = new System.Drawing.Point(596, 14);
            this.ddlGroup.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ddlGroup.Name = "ddlGroup";
            this.ddlGroup.Size = new System.Drawing.Size(173, 28);
            this.ddlGroup.TabIndex = 20;
            this.ddlGroup.SelectedIndexChanged += new System.EventHandler(this.ddlGroup_SelectedIndexChanged);
            // 
            // ddlLoc
            // 
            this.ddlLoc.FormattingEnabled = true;
            this.ddlLoc.Location = new System.Drawing.Point(223, 16);
            this.ddlLoc.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ddlLoc.Name = "ddlLoc";
            this.ddlLoc.Size = new System.Drawing.Size(177, 28);
            this.ddlLoc.TabIndex = 19;
            this.ddlLoc.SelectedIndexChanged += new System.EventHandler(this.ddlLoc_SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button1.BackgroundImage")));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.Control;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(902, 8);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(50, 41);
            this.button1.TabIndex = 18;
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = false;
            // 
            // ddlRestName
            // 
            this.ddlRestName.FormattingEnabled = true;
            this.ddlRestName.Location = new System.Drawing.Point(403, 15);
            this.ddlRestName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ddlRestName.Name = "ddlRestName";
            this.ddlRestName.Size = new System.Drawing.Size(190, 28);
            this.ddlRestName.TabIndex = 1;
            this.ddlRestName.SelectedIndexChanged += new System.EventHandler(this.ddlRestName_SelectedIndexChanged);
            // 
            // txtItemName
            // 
            this.txtItemName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtItemName.Location = new System.Drawing.Point(0, 16);
            this.txtItemName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtItemName.Name = "txtItemName";
            this.txtItemName.Size = new System.Drawing.Size(219, 26);
            this.txtItemName.TabIndex = 0;
            this.txtItemName.TextChanged += new System.EventHandler(this.txtItemName_TextChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblPageNo);
            this.groupBox2.Controls.Add(this.picMoveFirst);
            this.groupBox2.Controls.Add(this.picMovePrev);
            this.groupBox2.Controls.Add(this.picMoveLast);
            this.groupBox2.Controls.Add(this.picMoveNext);
            this.groupBox2.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(9, 119);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox2.Size = new System.Drawing.Size(244, 48);
            this.groupBox2.TabIndex = 28;
            this.groupBox2.TabStop = false;
            // 
            // lblPageNo
            // 
            this.lblPageNo.AutoSize = true;
            this.lblPageNo.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPageNo.Location = new System.Drawing.Point(73, 18);
            this.lblPageNo.Name = "lblPageNo";
            this.lblPageNo.Size = new System.Drawing.Size(102, 20);
            this.lblPageNo.TabIndex = 17;
            this.lblPageNo.Text = "Current Page : ";
            // 
            // picMoveFirst
            // 
            this.picMoveFirst.Image = ((System.Drawing.Image)(resources.GetObject("picMoveFirst.Image")));
            this.picMoveFirst.Location = new System.Drawing.Point(10, 18);
            this.picMoveFirst.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.picMoveFirst.Name = "picMoveFirst";
            this.picMoveFirst.Size = new System.Drawing.Size(23, 21);
            this.picMoveFirst.TabIndex = 13;
            this.picMoveFirst.TabStop = false;
            this.picMoveFirst.Click += new System.EventHandler(this.picMoveFirst_Click);
            // 
            // picMovePrev
            // 
            this.picMovePrev.Image = ((System.Drawing.Image)(resources.GetObject("picMovePrev.Image")));
            this.picMovePrev.Location = new System.Drawing.Point(40, 18);
            this.picMovePrev.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.picMovePrev.Name = "picMovePrev";
            this.picMovePrev.Size = new System.Drawing.Size(23, 21);
            this.picMovePrev.TabIndex = 14;
            this.picMovePrev.TabStop = false;
            this.picMovePrev.Click += new System.EventHandler(this.picMovePrev_Click);
            // 
            // picMoveLast
            // 
            this.picMoveLast.Image = ((System.Drawing.Image)(resources.GetObject("picMoveLast.Image")));
            this.picMoveLast.Location = new System.Drawing.Point(212, 18);
            this.picMoveLast.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.picMoveLast.Name = "picMoveLast";
            this.picMoveLast.Size = new System.Drawing.Size(23, 21);
            this.picMoveLast.TabIndex = 16;
            this.picMoveLast.TabStop = false;
            this.picMoveLast.Click += new System.EventHandler(this.picMoveLast_Click);
            // 
            // picMoveNext
            // 
            this.picMoveNext.Image = ((System.Drawing.Image)(resources.GetObject("picMoveNext.Image")));
            this.picMoveNext.Location = new System.Drawing.Point(181, 18);
            this.picMoveNext.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.picMoveNext.Name = "picMoveNext";
            this.picMoveNext.Size = new System.Drawing.Size(23, 21);
            this.picMoveNext.TabIndex = 15;
            this.picMoveNext.TabStop = false;
            this.picMoveNext.Click += new System.EventHandler(this.picMoveNext_Click);
            // 
            // btnUpdateRate
            // 
            this.btnUpdateRate.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnUpdateRate.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnUpdateRate.BackgroundImage")));
            this.btnUpdateRate.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnUpdateRate.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdateRate.ForeColor = System.Drawing.SystemColors.Control;
            this.btnUpdateRate.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdateRate.Image")));
            this.btnUpdateRate.Location = new System.Drawing.Point(833, 119);
            this.btnUpdateRate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnUpdateRate.Name = "btnUpdateRate";
            this.btnUpdateRate.Size = new System.Drawing.Size(131, 50);
            this.btnUpdateRate.TabIndex = 36;
            this.btnUpdateRate.Text = "Update Rate";
            this.btnUpdateRate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnUpdateRate.UseVisualStyleBackColor = false;
            this.btnUpdateRate.Click += new System.EventHandler(this.btnUpdateRate_Click);
            this.btnUpdateRate.MouseLeave += new System.EventHandler(this.btnUpdateRate_MouseLeave);
            this.btnUpdateRate.MouseHover += new System.EventHandler(this.btnUpdateRate_MouseHover);
            // 
            // btnNewRate
            // 
            this.btnNewRate.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnNewRate.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnNewRate.BackgroundImage")));
            this.btnNewRate.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnNewRate.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNewRate.ForeColor = System.Drawing.SystemColors.Control;
            this.btnNewRate.Image = ((System.Drawing.Image)(resources.GetObject("btnNewRate.Image")));
            this.btnNewRate.Location = new System.Drawing.Point(689, 119);
            this.btnNewRate.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnNewRate.Name = "btnNewRate";
            this.btnNewRate.Size = new System.Drawing.Size(131, 50);
            this.btnNewRate.TabIndex = 35;
            this.btnNewRate.Text = "New Rate";
            this.btnNewRate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnNewRate.UseVisualStyleBackColor = false;
            this.btnNewRate.Click += new System.EventHandler(this.btnNewRate_Click);
            this.btnNewRate.MouseLeave += new System.EventHandler(this.btnNewRate_MouseLeave);
            this.btnNewRate.MouseHover += new System.EventHandler(this.btnNewRate_MouseHover);
            // 
            // dtNewRate
            // 
            this.dtNewRate.CustomFormat = "dd-MM-yyyy";
            this.dtNewRate.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtNewRate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtNewRate.Location = new System.Drawing.Point(556, 132);
            this.dtNewRate.Name = "dtNewRate";
            this.dtNewRate.Size = new System.Drawing.Size(104, 22);
            this.dtNewRate.TabIndex = 43;
            // 
            // dvgItem
            // 
            this.dvgItem.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dvgItem.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dvgItem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dvgItem.DefaultCellStyle = dataGridViewCellStyle2;
            this.dvgItem.Location = new System.Drawing.Point(3, 176);
            this.dvgItem.Name = "dvgItem";
            this.dvgItem.RowHeadersVisible = false;
            this.dvgItem.Size = new System.Drawing.Size(961, 361);
            this.dvgItem.TabIndex = 44;
            this.dvgItem.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.dvgItem_CellValidated);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(436, 132);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 20);
            this.label2.TabIndex = 45;
            this.label2.Text = "New Rate From : ";
            // 
            // ManageItemRate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(973, 576);
            this.ControlBox = false;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dvgItem);
            this.Controls.Add(this.dtNewRate);
            this.Controls.Add(this.btnUpdateRate);
            this.Controls.Add(this.btnNewRate);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ManageItemRate";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.ManageItemRate_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picMoveFirst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMovePrev)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMoveLast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMoveNext)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dvgItem)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblUserAddEdit;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label lblExit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lbltoday;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox ddlGroup;
        private System.Windows.Forms.ComboBox ddlLoc;
        protected internal System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox ddlRestName;
        private System.Windows.Forms.TextBox txtItemName;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblPageNo;
        private System.Windows.Forms.PictureBox picMoveFirst;
        private System.Windows.Forms.PictureBox picMovePrev;
        private System.Windows.Forms.PictureBox picMoveLast;
        private System.Windows.Forms.PictureBox picMoveNext;
        protected internal System.Windows.Forms.Button btnUpdateRate;
        protected internal System.Windows.Forms.Button btnNewRate;
        internal System.Windows.Forms.DateTimePicker dtNewRate;
        private System.Windows.Forms.ComboBox ddlDate;
        private System.Windows.Forms.DataGridView dvgItem;
        private System.Windows.Forms.Label label2;
    }
}