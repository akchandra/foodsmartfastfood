﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using FoodSmart.DAL;
using FoodSmart.BLL;
using FoodSmart.Entity;
using FoodSmart.GeneralUtilities;
using FoodSmart.Utilities;
using FoodSmart.Common;

namespace FoodSmartDesktop.Masters
{
    public partial class ManageItemGroup : Form
    {
        BindingSource source1 = new BindingSource();
        private int PgSize = 12;
        private int CurrentPageIndex = 1;
        private int TotalPage = 0;
        private DataSet dsPage;

        public ManageItemGroup()
        {
            InitializeComponent();
        }
        #region Public Method
        private void ManageItemGroup_Load(object sender, EventArgs e)
        {
            dvgItemGroup.ColumnCount = 4;
            this.ddlRestName.SelectedIndexChanged -= new System.EventHandler(this.ddlRestName_SelectedIndexChanged);
            this.ddlLoc.SelectedIndexChanged -= new System.EventHandler(this.ddlLoc_SelectedIndexChanged);

            SetWatermarkText();
            LoadLoc();
            LoadRest("");

            SearchCriteria searchCriteria = new SearchCriteria();
            BuildDefaultSearch(searchCriteria);

            LoadItemGroup(searchCriteria);

            this.ddlRestName.SelectedIndexChanged += new System.EventHandler(this.ddlRestName_SelectedIndexChanged);
            this.ddlLoc.SelectedIndexChanged += new System.EventHandler(this.ddlLoc_SelectedIndexChanged);
            foreach (DataGridViewRow row in dvgItemGroup.Rows)
            {
                row.Height = 25;
            }

            lbltoday.Text += " : " + GeneralFunctions.LoginInfo.CurrDate.ToString("dd-MMM-yyyy");
            lblUserName.Text += GeneralFunctions.LoginInfo.UserFullName;
            ddlRestName.Focus();
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAddGroup_Click(object sender, EventArgs e)
        {
            this.Hide();
            Masters.AddEditItemGroup User = new Masters.AddEditItemGroup(0);
            if (User.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {

            }
            else
            {
                this.Show();
                //dvgItem.Columns.Remove("Dimg");
                dvgItemGroup.Columns.Remove("Eimg");
                dvgItemGroup.Columns.Remove("Dimg");
                SearchCriteria searchCriteria = new SearchCriteria();
                BuildDefaultSearch(searchCriteria);
                LoadItemGroup(searchCriteria);
                foreach (DataGridViewRow row in dvgItemGroup.Rows)
                {
                    row.Height = 25;
                }
            }
        }

        private void dvgItemGroup_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int Reply;
            int i;
            if (dvgItemGroup.RowCount == 0)
                return;
            else
                i = dvgItemGroup.CurrentRow.Index;

            if (dvgItemGroup.Columns[e.ColumnIndex].Name == "Dimg")
            {
                if (!CheckForDeleted(Convert.ToInt32(dvgItemGroup.CurrentRow.Cells[0].Value)))
                {
                    MessageBox.Show("Record Already Deleted.", "Delete Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }

                if (!CheckDeletePossible((Convert.ToInt32(dvgItemGroup.CurrentRow.Cells[0].Value))))
                {
                    DialogResult dialogResult = MessageBox.Show("Item Group Present on Item Master. Deletion on Group will Delete all Items within the Group", "Delete Record", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                    if (dialogResult != DialogResult.OK)
                        return;
                }

                Reply = Convert.ToInt32(MessageBox.Show("Want to Delete this Record?", "Delete Record", MessageBoxButtons.OKCancel, MessageBoxIcon.Question));
                if (Reply == 1)
                {
                    Reply = Convert.ToInt32(dvgItemGroup.CurrentRow.Cells[0].Value);

                    ItemBLL itembll = new ItemBLL();
                    
                    //RestaurantBLL restBLL = new RestaurantBLL();
                    itembll.DeleteItemgroup(Reply);

                    dvgItemGroup.Columns.Remove("Dimg");
                    dvgItemGroup.Columns.Remove("Eimg");
                    SearchCriteria searchCriteria = new SearchCriteria();
                    BuildDefaultSearch(searchCriteria);
                    LoadItemGroup(searchCriteria);
                    foreach (DataGridViewRow row in dvgItemGroup.Rows)
                    {
                        row.Height = 25;
                    }
                }
            }

            if (dvgItemGroup.Columns[e.ColumnIndex].Name == "Eimg")
            {
                Masters.AddEditItemGroup frm1 = new Masters.AddEditItemGroup(Convert.ToInt32(dvgItemGroup.CurrentRow.Cells[0].Value));
                this.Hide();

                if (frm1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    this.Hide();
                }
                else
                {
                    this.Show();
                    dvgItemGroup.Columns.Remove("Dimg");
                    dvgItemGroup.Columns.Remove("Eimg");
                    SearchCriteria searchCriteria = new SearchCriteria();
                    BuildDefaultSearch(searchCriteria);
                    LoadItemGroup(searchCriteria);
                    foreach (DataGridViewRow row in dvgItemGroup.Rows)
                    {
                        row.Height = 25;
                    }
                }
            }
        }

        private void txtGroupName_TextChanged(object sender, EventArgs e)
        {
            CheckFilterCondition();
        }

        private void ddlRestName_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckFilterCondition();
        }

        private void ddlLoc_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadRest(ddlLoc.Text);
            CheckFilterCondition();
        }


        #endregion

        #region "Private Methods"
        private void LoadItemGroup(SearchCriteria searchCriteria)
        {
            string path = Application.StartupPath + "\\";
            DataView view1;

            DataGridViewCellStyle style = new DataGridViewCellStyle();
            dvgItemGroup.EnableHeadersVisualStyles = false;
            style.ForeColor = Color.Blue;

            dvgItemGroup.AutoGenerateColumns = false;

            dvgItemGroup.Columns[0].Name = "pk_ItemGroupID";
            dvgItemGroup.Columns[1].Name = "Item Group Name";
            dvgItemGroup.Columns[2].Name = "Location";
            dvgItemGroup.Columns[3].Name = "Restaurant Name";

            dvgItemGroup.Columns[1].HeaderText = "Item Group";
            dvgItemGroup.Columns[2].HeaderText = "Location";
            dvgItemGroup.Columns[3].HeaderText = "Restaurant Name";

            dvgItemGroup.Columns[0].DataPropertyName = "pk_ItemGroupID";
            dvgItemGroup.Columns[1].DataPropertyName = "descr";
            dvgItemGroup.Columns[2].DataPropertyName = "LocationName";
            dvgItemGroup.Columns[3].DataPropertyName = "RestName";

            dvgItemGroup.Columns[1].Width = 300;
            dvgItemGroup.Columns[2].Width = 250;
            dvgItemGroup.Columns[3].Width = 300;


            dvgItemGroup.Columns[1].SortMode = DataGridViewColumnSortMode.Automatic;
            dvgItemGroup.Columns[2].SortMode = DataGridViewColumnSortMode.Automatic;
            dvgItemGroup.Columns[3].SortMode = DataGridViewColumnSortMode.Automatic;

            dvgItemGroup.Columns[0].Visible = false;
            //dvgItemGroup.Columns[9].Visible = false;

            DataGridViewImageColumn Editimg = new DataGridViewImageColumn();
            Image Editimage = Image.FromFile(path + ConfigurationManager.AppSettings["imageFolder"] + "edit-icon.png");
            Editimg.Image = Editimage;
            dvgItemGroup.Columns.Add(Editimg);
            Editimg.HeaderText = "Edit";
            Editimg.Name = "Eimg";
            Editimg.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            Editimg.Width = 40;

            DataGridViewImageColumn Delimg = new DataGridViewImageColumn();
            Image Delimage = Image.FromFile(path + ConfigurationManager.AppSettings["imageFolder"] + "delete-icon.png");
            Delimg.Image = Delimage;
            dvgItemGroup.Columns.Add(Delimg);
            Delimg.HeaderText = "Del";
            Delimg.Name = "Dimg";
            Delimg.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            Delimg.Width = 40;

            dvgItemGroup.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dvgItemGroup.MultiSelect = false;

            ItemBLL itembll = new ItemBLL();
            dsPage = itembll.GetAllItemGroup(searchCriteria);
            if (dsPage.Tables[0].Rows.Count == 0)
                view1 = null;
            else
            {
                TotalPage = GeneralFunctions.CalculateTotalPages(dsPage.Tables[0], PgSize);
                view1 = new DataView(GeneralFunctions.GetCurrentRecords(0, dsPage.Tables[0], PgSize, "pk_itemgroupid"));
                CurrentPageIndex = 0;
                lblPageNo.Text = 1.ToString() + " / " + TotalPage.ToString();

            }
            source1.DataSource = view1;

            dvgItemGroup.DataSource = source1;
            dvgItemGroup.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            dvgItemGroup.AllowUserToResizeColumns = false;
            dvgItemGroup.AllowUserToAddRows = false;
            dvgItemGroup.ReadOnly = true;
            dvgItemGroup.Columns[1].HeaderCell.Style = style;
            dvgItemGroup.Columns[2].HeaderCell.Style = style;
            //dvgItemGroup.Columns[6].HeaderCell.Style = style;
            //dvgItemGroup.Columns[7].HeaderCell.Style = style;
        }

        private void BuildDefaultSearch(SearchCriteria criteria)
        {
            criteria.ItemGroupName = "";
            criteria.RestName = "All Restaurants";
            criteria.LocName = "All Locations";
        }

        private void BuildSearchCriteria(SearchCriteria criteria)
        {

            criteria.ItemGroupName = txtGroupName.Text;
            //criteria.CardName = txtName.Text;
            criteria.RestName = ddlRestName.Text.ToString();
            criteria.LocName = ddlLoc.Text.ToString();
        }

        private void CheckFilterCondition()
        {
            SearchCriteria searchCriteria = new SearchCriteria();
            BuildSearchCriteria(searchCriteria);
            LoadItemGroup(searchCriteria);
            dvgItemGroup.Columns.Remove("Dimg");
            dvgItemGroup.Columns.Remove("Eimg");
            //LoadRestaurants(searchCriteria);
            foreach (DataGridViewRow row in dvgItemGroup.Rows)
            {
                row.Height = 25;
            }
        }

        private void SetWatermarkText()
        {

            WatermarkText.SetWatermark(txtGroupName, "Item Group Name");
            //WatermarkText.SetWatermark(txtName, "User / Restaurant Name");
        }

        private void LoadLoc()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            //populate Locations
            SearchCriteria searchcriteria = new SearchCriteria();
            searchcriteria.QueryStat = "L";

            ds = new LocationBLL().GetAllLoc(searchcriteria);
            dt = ds.Tables[0];

            DataRow nullrow = dt.NewRow();
            nullrow["pk_LocID"] = 0;
            nullrow["LocationName"] = "All Locations";
            dt.Rows.Add(nullrow);

            ddlLoc.DataSource = dt;
            ddlLoc.DisplayMember = "LocationName";
            ddlLoc.ValueMember = "pk_LocID";
            ddlLoc.SelectedValue = "0";
        }

        private void LoadRest(string Locname)
        {
            //DataRow row = new DataRow();
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            SearchCriteria searchcriteria = new SearchCriteria();
            searchcriteria.QueryStat = "L";
            if (ddlLoc.Text == "All Locations")
                searchcriteria.LocName = "";
            else
                searchcriteria.LocName = Locname;

            ds = new RestaurantBLL().GetAllRestaurant(searchcriteria);
            dt = ds.Tables[0];

            DataRow nullrow = dt.NewRow();
            nullrow["pk_RestID"] = 0;
            nullrow["RestName"] = "All Restaurants";
            dt.Rows.Add(nullrow);

            ddlRestName.DataSource = dt;
            ddlRestName.DisplayMember = "RestName";
            ddlRestName.ValueMember = "pk_RestID";
            ddlRestName.SelectedValue = "0";

        }

        private bool CheckForDeleted(int itemgrpID)
        {
            ItemBLL itemgrpbll = new ItemBLL();
            FoodSmart.Entity.ItemsEntity oitemGrpEntity = (FoodSmart.Entity.ItemsEntity)itemgrpbll.GetItemGroupForEdit(itemgrpID);
            if (oitemGrpEntity.GroupStatus)
            {
                return true;
            }
            else
                return false;
        }

        private bool CheckDeletePossible(int Cardid)
        {
            ItemBLL itemGroupbll = new ItemBLL();

            int stat = itemGroupbll.ChkItemGroupDelPossible(Cardid);
            if (stat == 1)
            {
                return true;
            }
            else
                return false;
        }

        #endregion

        #region "Cursor Type Change"
        private void btnAddGroup_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnAddGroup_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void lblExit_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void lblExit_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        #endregion

        #region "Pagination Control"

        private void ChangePageIndex()
        {
            DataView view1;
            view1 = new DataView(GeneralFunctions.GetCurrentRecords(CurrentPageIndex, dsPage.Tables[0], PgSize, "pk_itemgroupid"));
            source1.DataSource = view1;
            int pg = CurrentPageIndex + 1;
            lblPageNo.Text = pg.ToString() + " / " + TotalPage.ToString();
            foreach (DataGridViewRow row in dvgItemGroup.Rows)
            {
                row.Height = 25;
            }
        }

        private void picMoveFirst_Click(object sender, EventArgs e)
        {
            if (CurrentPageIndex != 0)
            {
                CurrentPageIndex = 0;
                ChangePageIndex();
            }
        }

        private void picMovePrev_Click(object sender, EventArgs e)
        {
            if (CurrentPageIndex != 0)
            {
                CurrentPageIndex--;
                ChangePageIndex();
            }
        }

        private void picMoveNext_Click(object sender, EventArgs e)
        {
            if (CurrentPageIndex + 1 < TotalPage)
            {
                CurrentPageIndex++;
                ChangePageIndex();
            }
        }

        private void picMoveLast_Click(object sender, EventArgs e)
        {
            if (CurrentPageIndex + 1 < TotalPage)
            {
                CurrentPageIndex = TotalPage - 1;
                ChangePageIndex();
            }
        }
        #endregion

      
      
    }
}
