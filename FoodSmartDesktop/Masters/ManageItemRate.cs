﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FoodSmart.BLL;
using FoodSmart.Common;
using System.Configuration;
using System.IO;
using System.Drawing.Drawing2D;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using FoodSmart.Utilities.Cryptography;
using System.Globalization;
using FoodSmart.GeneralUtilities;

namespace FoodSmartDesktop.Masters
{
    public partial class ManageItemRate : Form
    {
        BindingSource source1 = new BindingSource();
        private int PgSize = 12;
        private int CurrentPageIndex = 1;
        private int TotalPage = 0;
        private DataSet dsPage;
        private int Stat;
        int cval;
        int rowid;

        public ManageItemRate()
        {
            InitializeComponent();
        }

        private void ManageItemRate_Load(object sender, EventArgs e)
        {
            dvgItem.ColumnCount = 12;
            dtNewRate.Visible = false;
            label2.Visible = false;
            btnNewRate.Text = "New Rate";
            btnUpdateRate.Text = "Update Rate";

            //btnSave.Visible = false;
            //btnCancel.Visible = false;

            this.ddlRestName.SelectedIndexChanged -= new System.EventHandler(this.ddlRestName_SelectedIndexChanged);
            this.ddlLoc.SelectedIndexChanged -= new System.EventHandler(this.ddlLoc_SelectedIndexChanged);
            this.ddlGroup.SelectedIndexChanged -= new System.EventHandler(this.ddlGroup_SelectedIndexChanged);
            this.ddlDate.SelectedIndexChanged -= new System.EventHandler(this.ddlDate_SelectedIndexChanged);
            Stat = 1;
            LoadDate();
            SetWatermarkText();
            LoadLoc();
            LoadRest();
            LoadItemGroup();

            SearchCriteria searchCriteria = new SearchCriteria();
            BuildDefaultSearch(searchCriteria);

            LoadItemRate(searchCriteria);

            this.ddlDate.SelectedIndexChanged += new System.EventHandler(this.ddlDate_SelectedIndexChanged);
            this.ddlRestName.SelectedIndexChanged += new System.EventHandler(this.ddlRestName_SelectedIndexChanged);
            this.ddlLoc.SelectedIndexChanged += new System.EventHandler(this.ddlLoc_SelectedIndexChanged);
            this.ddlGroup.SelectedIndexChanged += new System.EventHandler(this.ddlGroup_SelectedIndexChanged);
            foreach (DataGridViewRow row in dvgItem.Rows)
            {
                row.Height = 25;
            }

            lbltoday.Text += " : " + GeneralFunctions.LoginInfo.CurrDate.ToString("dd-MMM-yyyy");
            lblUserName.Text += GeneralFunctions.LoginInfo.UserFullName;
            ddlRestName.Focus();
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnNewRate_Click(object sender, EventArgs e)
        {
            DateTime newdate;
            string Mode="";
            if (Stat == 1)
            {
                dtNewRate.Visible = true;
                dtNewRate.MinDate = GeneralFunctions.LoginInfo.CurrDate;
                Stat = 2;
                label2.Visible = true;
                btnNewRate.Text = "Save [F2]";
                btnUpdateRate.Text = "Cancel";
                //btnSave.Visible = true;
                //btnCancel.Visible = true;

                SearchCriteria searchCriteria = new SearchCriteria();
                BuildSearchCriteria(searchCriteria);
                LoadItemRate(searchCriteria);
            }
            else
            {
                // save rate
                if (Stat == 2 && dtNewRate.Value.Date <= GeneralFunctions.LoginInfo.CurrDate.Date)
                {
                    MessageBox.Show("Error!! Date should be Greater than Today's Date", "Save Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                foreach (DataGridViewRow row in dvgItem.Rows)
                {
                     if (row.Cells[10].Value.ToDecimal() > 0)
                     {
                         if (Stat == 2)
                             newdate = dtNewRate.Value;
                         else
                         {
                             //string strDate = ddlDate.Text;
                             //string[] sa = strDate.Split('-');
                             //string strNew = sa[2] + "-" + sa[1] + "-" + sa[0];
                             //newdate = Convert.ToDateTime(strNew);

                             newdate = (ddlDate.Text.Substring(6, 4) + "-" + ddlDate.Text.Substring(3, 2) + "-" + ddlDate.Text.Substring(0, 2)).ToDateTime();
 
                             //newdate = DateTime.ParseExact(ddlDate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                             //newdate = Convert.ToDateTime(newdate.ToString("MM/dd/yyyy"));
                         }

                         IItems Item = new ItemsEntity();

                         Item.ItemDescription = txtItemName.Text;
                         
                         Item.EffectiveDate = newdate;
                         Item.ItemID = row.Cells[1].Value.ToInt();
                         Item.SellingRate = row.Cells[10].Value.ToDecimal();

                         
                         switch (Stat)
                         {
                             case 2:
                                 Mode = "A";
                                 break;
                             case 3:
                                 Mode = "E";
                                 Item.ItemRateID = row.Cells[0].Value.ToInt();
                                 break;
                         }

                         int Status = new ItemBLL().SaveItemRate(Item, Mode, GeneralFunctions.LoginInfo.UserID);

                         if (Status == 0)
                         {
                             MessageBox.Show("Error!! Record Not Saved", "Save Record", MessageBoxButtons.OK, MessageBoxIcon.Error);
                             return;
                         }
                         
                     }
                }

                Stat = 1;

                btnNewRate.Text = "New Rate";
                btnUpdateRate.Text = "Update Rate";
                SearchCriteria searchCriteria = new SearchCriteria();
                BuildSearchCriteria(searchCriteria);
                LoadItemRate(searchCriteria);
                label2.Visible = false;
                dtNewRate.Visible = false;
            }

        }

        private void btnUpdateRate_Click(object sender, EventArgs e)
        {
            if (Stat == 1)
            {
                Stat = 3;
                btnNewRate.Text = "Save [F2]";
                btnUpdateRate.Text = "Cancel";
                SearchCriteria searchCriteria = new SearchCriteria();
                BuildSearchCriteria(searchCriteria);
                LoadItemRate(searchCriteria);

            }
            else
            {

                // Cancel Rate
                Stat = 1;
                btnNewRate.Text = "New Rate";
                btnUpdateRate.Text = "Update Rate";
                SearchCriteria searchCriteria = new SearchCriteria();
                BuildSearchCriteria(searchCriteria);
                LoadItemRate(searchCriteria);
                label2.Visible = false;
                dtNewRate.Visible = false;

            }
        }

        //private void btnCancel_Click(object sender, EventArgs e)
        //{
        //    btnNewRate.Text = "New Rate";
        //    btnUpdateRate.Text = "Update Rate";
        //    //btnSave.Visible = false;
        //    //btnCancel.Visible = false;
        //    Stat = 1;
        //    SearchCriteria searchCriteria = new SearchCriteria();
        //    BuildSearchCriteria(searchCriteria);
        //    LoadItemRate(searchCriteria);
        //    label2.Visible = false;
        //    dtNewRate.Visible = false;
        //}

        //private void btnSave_Click(object sender, EventArgs e)
        //{
        //    Stat = 1;
        //    btnNewRate.Text = "New Rate";
        //    btnUpdateRate.Text = "Update Rate";
        //}


        private void dvgItem_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            if (dvgItem.Columns[e.ColumnIndex].Name == "NewRate")
            {
                cval = dvgItem.CurrentCell.Value.ToInt();
                if (cval > 0)
                {
                    rowid = dvgItem.CurrentCell.RowIndex;
                    if (dvgItem.Rows[rowid].Cells[9].Value != null)
                    {
                        if (dvgItem.Rows[rowid].Cells[10].Value.ToDateTime() == dtNewRate.Value)
                        {
                            MessageBox.Show("Already Changed Once", "Error");
                            dvgItem.Rows[rowid].Cells[9].Value = 0;
                            return;
                        }
                    }
                    dvgItem.Rows[rowid].DefaultCellStyle.BackColor = Color.Salmon;
                }
            }
        }

        private void ddlLoc_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadRest();
            CheckFilterCondition();
        }

        private void ddlRestName_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadItemGroup();
            CheckFilterCondition();
        }

        private void ddlGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckFilterCondition();
        }

        private void txtItemName_TextChanged(object sender, EventArgs e)
        {
            CheckFilterCondition();
        }

        private void picMoveFirst_Click(object sender, EventArgs e)
        {
            if (CurrentPageIndex != 0)
            {
                CurrentPageIndex--;
                ChangePageIndex();
            }
        }

        private void ddlDate_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckFilterCondition();
        }


        private void picMovePrev_Click(object sender, EventArgs e)
        {
            if (CurrentPageIndex + 1 < TotalPage)
            {
                CurrentPageIndex++;
                ChangePageIndex();
            }
        }

        private void picMoveNext_Click(object sender, EventArgs e)
        {
            if (CurrentPageIndex + 1 < TotalPage)
            {
                CurrentPageIndex++;
                ChangePageIndex();
            }
        }

        private void picMoveLast_Click(object sender, EventArgs e)
        {
            if (CurrentPageIndex + 1 < TotalPage)
            {
                CurrentPageIndex = TotalPage - 1;
                ChangePageIndex();
            }
        }

        private void lblExit_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void lblExit_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnNewRate_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnNewRate_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnUpdateRate_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnUpdateRate_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnCancel_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnCancel_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnSave_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnSave_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        #region "Private Methods"
        private void ChangePageIndex()
        {
            DataView view1;
            view1 = new DataView(GeneralFunctions.GetCurrentRecords(CurrentPageIndex, dsPage.Tables[0], PgSize, "pk_Rateid"));
            source1.DataSource = view1;
            int pg = CurrentPageIndex + 1;
            lblPageNo.Text = pg.ToString() + " / " + TotalPage.ToString();
            foreach (DataGridViewRow row in dvgItem.Rows)
            {
                row.Height = 25;
            }
        }

        private void LoadItemRate(SearchCriteria searchCriteria)
        {
            string path = Application.StartupPath + "\\";
            DataView view1;
            

            DataGridViewCellStyle style = new DataGridViewCellStyle();
            dvgItem.EnableHeadersVisualStyles = false;
            style.ForeColor = Color.Blue;

            dvgItem.AutoGenerateColumns = false;

            dvgItem.Columns[0].Name = "pk_RateID";
            dvgItem.Columns[1].Name = "fk_ItemID";
            dvgItem.Columns[2].Name = "fk_ItemGroupID";
            dvgItem.Columns[3].Name = "fk_LocID";
            dvgItem.Columns[4].Name = "Item Name";
            dvgItem.Columns[5].Name = "Location";
            dvgItem.Columns[6].Name = "Item Group";
            dvgItem.Columns[7].Name = "Restaurant";
            dvgItem.Columns[8].Name = "VAT %";
            dvgItem.Columns[9].Name = "Existing Rate";
            dvgItem.Columns[10].Name = "NewRate";
            dvgItem.Columns[11].Name = "Effective Date";

            dvgItem.Columns[1].HeaderText = "fk_ItemID";
            dvgItem.Columns[2].HeaderText = "fk_ItemGroupID";
            dvgItem.Columns[3].HeaderText = "fk_LocID";
            dvgItem.Columns[4].HeaderText = "Item Name";
            dvgItem.Columns[5].HeaderText = "Location";
            dvgItem.Columns[6].HeaderText = "Item Group";
            dvgItem.Columns[7].HeaderText = "Restaurant";
            dvgItem.Columns[8].HeaderText = "VAT %";
            dvgItem.Columns[9].HeaderText = "Existing Rate";
            if (Stat == 3)
                dvgItem.Columns[10].HeaderText = "Update Rate";
            else if (Stat == 2)
                dvgItem.Columns[10].HeaderText = "New Rate";

            dvgItem.Columns[11].Name = "Effective Date";

            dvgItem.Columns[0].DataPropertyName = "pk_RateID";
            dvgItem.Columns[1].DataPropertyName = "pk_ItemID";
            dvgItem.Columns[2].DataPropertyName = "fk_ItemGroupID";
            dvgItem.Columns[3].DataPropertyName = "fk_LocID";
            dvgItem.Columns[4].DataPropertyName = "ItemDescr";
            dvgItem.Columns[5].DataPropertyName = "LocationName";
            dvgItem.Columns[6].DataPropertyName = "Descr";
            dvgItem.Columns[7].DataPropertyName = "RestName";
            dvgItem.Columns[8].DataPropertyName = "VATPer";
            dvgItem.Columns[9].DataPropertyName = "SELLINGRATE";
            dvgItem.Columns[10].DataPropertyName = "NewRate";
            dvgItem.Columns[11].DataPropertyName = "EFFECTIVEDATE";


            dvgItem.Columns[4].Width = 250;
            dvgItem.Columns[5].Width = 100;
            dvgItem.Columns[6].Width = 200;
            dvgItem.Columns[7].Width = 200;
            dvgItem.Columns[8].Width = 60;
            dvgItem.Columns[9].Width = 70;
            dvgItem.Columns[10].Width = 70;
    
            dvgItem.Columns[1].SortMode = DataGridViewColumnSortMode.Automatic;
            dvgItem.Columns[2].SortMode = DataGridViewColumnSortMode.Automatic;
            dvgItem.Columns[3].SortMode = DataGridViewColumnSortMode.Automatic;
            dvgItem.Columns[4].SortMode = DataGridViewColumnSortMode.Automatic;
            dvgItem.Columns[5].SortMode = DataGridViewColumnSortMode.NotSortable;
            dvgItem.Columns[7].SortMode = DataGridViewColumnSortMode.NotSortable;

            dvgItem.Columns[10].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dvgItem.Columns[10].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;

            dvgItem.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dvgItem.Columns[8].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;


            dvgItem.Columns[9].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dvgItem.Columns[9].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;

            dvgItem.Columns[0].Visible = false;
            dvgItem.Columns[1].Visible = false;
            dvgItem.Columns[2].Visible = false;
            dvgItem.Columns[3].Visible = false;

            if (Stat == 1)
                dvgItem.Columns[10].Visible = false;
            else
                dvgItem.Columns[10].Visible = true;  
            
            dvgItem.Columns[11].Visible = false;

            //dvgItem.Columns[9].Visible = false;

            //DataGridViewImageColumn Editimg = new DataGridViewImageColumn();
            //Image Editimage = Image.FromFile(path + ConfigurationManager.AppSettings["imageFolder"] + "edit-icon.png");
            //Editimg.Image = Editimage;
            //dvgItem.Columns.Add(Editimg);
            //Editimg.HeaderText = "Edit";
            //Editimg.Name = "Eimg";
            //Editimg.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //Editimg.Width = 40;

            //DataGridViewImageColumn Delimg = new DataGridViewImageColumn();
            //Image Delimage = Image.FromFile(path + ConfigurationManager.AppSettings["imageFolder"] + "delete-icon.png");
            //Delimg.Image = Delimage;
            //dvgItem.Columns.Add(Delimg);
            //Delimg.HeaderText = "Del";
            //Delimg.Name = "Dimg";
            //Delimg.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //Delimg.Width = 40;

            dvgItem.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dvgItem.MultiSelect = false;

            ItemBLL itembll = new ItemBLL();
            dsPage = itembll.GetAllItemRates(searchCriteria);
            if (dsPage.Tables[0].Rows.Count == 0)
                view1 = null;
            else
            {
                TotalPage = GeneralFunctions.CalculateTotalPages(dsPage.Tables[0], PgSize);
                view1 = new DataView(GeneralFunctions.GetCurrentRecords(0, dsPage.Tables[0], PgSize, "pk_Rateid"));
                CurrentPageIndex = 0;
                lblPageNo.Text = 1.ToString() + " / " + TotalPage.ToString();

            }
            source1.DataSource = view1;

            dvgItem.DataSource = source1;
            dvgItem.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            dvgItem.AllowUserToResizeColumns = false;
            dvgItem.AllowUserToAddRows = false;
            dvgItem.AllowUserToResizeRows = false;
            //dvgItem.ReadOnly = true;
            dvgItem.Columns[1].HeaderCell.Style = style;
            dvgItem.Columns[2].HeaderCell.Style = style;

            if (dsPage.Tables[0].Rows.Count > 0)
            {
                foreach (DataGridViewColumn c in dvgItem.Columns)
                {
                    if (c.DisplayIndex != 10)
                    {
                        c.ReadOnly = true;
                        c.Resizable = DataGridViewTriState.False;
                    }
                    else
                    {
                        c.ReadOnly = false;
                        rowid = dvgItem.CurrentCell.RowIndex.ToInt();
                        cval = dvgItem.Rows[rowid].Cells[10].Value.ToInt();
                        if (cval > 0)
                            dvgItem.Rows[rowid].DefaultCellStyle.BackColor = Color.Salmon;

                    }
                    
                }
            }
            foreach (DataGridViewRow row in dvgItem.Rows)
            {
                row.Height = 25;
            }

            //dvgItem.Columns[6].HeaderCell.Style = style;
            //dvgItem.Columns[7].HeaderCell.Style = style;
        }


        private void BuildDefaultSearch(SearchCriteria criteria)
        {
            criteria.ItemName = "";
            criteria.RestName = ddlRestName.Text;
            criteria.LocName = ddlLoc.Text;
            criteria.ItemGroupName = ddlGroup.Text;
            criteria.IntegerOption1 = ddlRestName.SelectedValue.ToInt();
            criteria.IntegerOption3 = ddlGroup.SelectedValue.ToInt();
            //string strDate = ddlDate.Text;
            //string[] sa = strDate.Split('-');
            //string strNew = sa[2] + "-" + sa[1] + "-" + sa[0];
            //criteria.Date = string.Format(ddlDate.Text, "0:yyyy-MM-dd").ToDateTime();
            criteria.Date = (ddlDate.Text.Substring(6, 4) + "-" + ddlDate.Text.Substring(3, 2) + "-" + ddlDate.Text.Substring(0, 2)).ToDateTime();
            //criteria.Date = Convert.ToDateTime(strNew);


            //criteria.RestName = "All Restaurants";
            //criteria.LocName = "All Locations";
            //criteria.ItemGroupName = "All Groups";
        }

        private void BuildSearchCriteria(SearchCriteria criteria)
        {
            criteria.ItemName = txtItemName.Text;
            //criteria.CardName = txtName.Text;
            criteria.RestName = ddlRestName.Text;
            criteria.LocName = ddlLoc.Text;
            criteria.ItemGroupName = ddlGroup.Text;
            criteria.IntegerOption1 = ddlRestName.SelectedValue.ToInt();
            criteria.IntegerOption3 = ddlGroup.SelectedValue.ToInt();
            //criteria.Date = string.Format(ddlDate.Text, "0:yyyy-MM-dd").ToDateTime();
            criteria.Date = (ddlDate.Text.Substring(6, 4) + "-" + ddlDate.Text.Substring(3, 2) + "-" + ddlDate.Text.Substring(0, 2)).ToDateTime();
            //string strDate = ddlDate.Text;
            //string[] sa = strDate.Split('-');
            //string strNew = sa[2] + "-" + sa[1] + "-" + sa[0];
            //criteria.Date = Convert.ToDateTime(strNew);
           
        }

        private void CheckFilterCondition()
        {
            SearchCriteria searchCriteria = new SearchCriteria();
            BuildSearchCriteria(searchCriteria);
            LoadItemRate(searchCriteria);
            //dvgItem.Columns.Remove("Dimg");
            //dvgItem.Columns.Remove("Eimg");
            //LoadRestaurants(searchCriteria);
            foreach (DataGridViewRow row in dvgItem.Rows)
            {
                row.Height = 25;
            }
        }

        private void SetWatermarkText()
        {

            WatermarkText.SetWatermark(txtItemName, "Item Name");
            //WatermarkText.SetWatermark(txtName, "User / Restaurant Name");
        }

        private void LoadLoc()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            //populate Locations
            SearchCriteria searchcriteria = new SearchCriteria();
            searchcriteria.QueryStat = "L";

            ds = new LocationBLL().GetAllLoc(searchcriteria);
            dt = ds.Tables[0];

            //DataRow nullrow = dt.NewRow();
            //nullrow["pk_LocID"] = 0;
            //nullrow["LocationName"] = "All Locations";
            //dt.Rows.Add(nullrow);
            if (dt.Rows.Count > 0)
            {
                ddlLoc.DataSource = dt;
                ddlLoc.DisplayMember = "LocationName";
                ddlLoc.ValueMember = "pk_LocID";
                ddlLoc.SelectedIndex = 0;
            }
        }

        private void LoadDate()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            //populate Date
            ds = new ItemBLL().GetAllRateDate("C", 0, DateTime.Today);
            dt = ds.Tables[0];

            //DataRow nullrow = dt.NewRow();
            //nullrow["pk_LocID"] = 0;
            //nullrow["LocationName"] = "All Locations";
            //dt.Rows.Add(nullrow);
            if (dt.Rows.Count > 0)
            {
                ddlDate.DataSource = dt;
                ddlDate.DisplayMember = "effectiveDate";
                ddlDate.ValueMember = "effectiveDate";
                ddlDate.SelectedIndex = 0;
            }
            else
            {
                DataRow nullrow = dt.NewRow();
                nullrow["effectiveDate"] = DateTime.Today;
                nullrow["effectiveDate"] = DateTime.Today;
                dt.Rows.Add(nullrow);
            }

            ddlDate.FormatString = "dd/MM/yyyy";
        }

        private void LoadRest()
        {
            //DataRow row = new DataRow();
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            SearchCriteria searchcriteria = new SearchCriteria();
            searchcriteria.QueryStat = "L";
            searchcriteria.LocName = ddlLoc.Text;
            //if (ddlLoc.Text == "All Locations")
            //    searchcriteria.LocName = "";
            //else
            //    searchcriteria.LocName = Locname;
            ddlRestName.DataSource = null;

            ds = new RestaurantBLL().GetAllRestaurant(searchcriteria);
            dt = ds.Tables[0];
            if (dt.Rows.Count > 0)
            {
                //DataRow nullrow = dt.NewRow();
                //nullrow["pk_RestID"] = 0;
                //nullrow["RestName"] = "All Restaurants";
                //dt.Rows.Add(nullrow);

                ddlRestName.DataSource = dt;
                ddlRestName.DisplayMember = "RestName";
                ddlRestName.ValueMember = "pk_RestID";
                ddlRestName.SelectedIndex = 0;
            }
        }

        private void LoadItemGroup()
        {
            //DataRow row = new DataRow();
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            //FoodSmart.BLL.itemService.SearchCriteria searchcriteria = new FoodSmart.BLL.itemService.SearchCriteria();

            SearchCriteria searchcriteria = new SearchCriteria();
            searchcriteria.QueryStat = "L";
            searchcriteria.LocName = ddlLoc.Text;
            searchcriteria.RestName = ddlRestName.Text;
            //searchcriteria.ItemGroupName = ddlGroup.Text;

            //if (ddlLoc.Text == "All Locations")
            //    searchcriteria.LocName = "";
            //else
            //    searchcriteria.LocName = Locname;

            //if (ddlRestName.Text == "All Restaurants")
            //    searchcriteria.RestName = "";
            //else
            //    searchcriteria.RestName = RestName;
            ddlGroup.DataSource = null;

            ds = new ItemBLL().GetAllItemGroup(searchcriteria);
            dt = ds.Tables[0];

            DataRow nullrow = dt.NewRow();
            nullrow["pk_ItemGroupID"] = 0;
            nullrow["descr"] = "All Groups";
            dt.Rows.Add(nullrow);
            if (dt.Rows.Count > 0)
            {
                ddlGroup.DataSource = dt;
                ddlGroup.DisplayMember = "descr";
                ddlGroup.ValueMember = "pk_ItemGroupID";
                ddlGroup.SelectedValue = "0";
            }
        }
        private bool CheckForDeleted(int itemgrpID)
        {
            ItemBLL itemgrpbll = new ItemBLL();
            FoodSmart.Entity.ItemsEntity oitemGrpEntity = (FoodSmart.Entity.ItemsEntity)itemgrpbll.GetItemGroupForEdit(itemgrpID);
            if (oitemGrpEntity.GroupStatus)
            {
                return true;
            }
            else
                return false;
        }

        private bool CheckDeletePossible(int Cardid)
        {
            ItemBLL itemGroupbll = new ItemBLL();

            int stat = itemGroupbll.ChkItemGroupDelPossible(Cardid);
            if (stat == 1)
            {
                return true;
            }
            else
                return false;
        }

        #endregion

   
     
    }
}
