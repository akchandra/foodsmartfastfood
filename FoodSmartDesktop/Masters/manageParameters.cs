﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FoodSmartDesktop.Masters
{
    public partial class manageParameters : Form
    {
        public manageParameters()
        {
            InitializeComponent();
        }

        private void manageParameters_Load(object sender, EventArgs e)
        {
            //LoadDDL();
            LoadParameters();
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            FoodSmart.BLL.DBInteraction dbinteract = new FoodSmart.BLL.DBInteraction();
            dbinteract.SaveParameters(Convert.ToInt32(txtSecurityDep.Text), txtWebRptURL.Text, Convert.ToInt32(txtGracePeriod.Text), Convert.ToInt32(txtNoCharge.Text));
            this.Close();
        }

        private void LoadParameters()
        {
            FoodSmart.BLL.DBInteraction dbinteract = new FoodSmart.BLL.DBInteraction();
            System.Data.DataSet ds = dbinteract.GetCompany();
            //if (Convert.ToBoolean(ds.Tables[0].Rows[0]["item_inclTax"]))
            //    ddlTaxCalcMethod.SelectedIndex = 0;
            //else
            //    ddlTaxCalcMethod.SelectedIndex = 1;
            txtSecurityDep.Text = ds.Tables[0].Rows[0]["SecurityDep"].ToString();
            //txtServiceTaxPer.Text = ds.Tables[0].Rows[0]["ServiceTaxPer"].ToString();
            //txtSBTPer.Text = ds.Tables[0].Rows[0]["SBT"].ToString();
            txtWebRptURL.Text = ds.Tables[0].Rows[0]["ReportURL"].ToString();
            txtGracePeriod.Text = ds.Tables[1].Rows[0]["Gracemin"].ToString();
            txtNoCharge.Text = ds.Tables[1].Rows[0]["NoChargeMin"].ToString();
        }

        //private void LoadDDL()
        //{
        //    DataSet ds = new DataSet();
        //    DataTable dt = new DataTable();
        //    //DataRow row = new DataRow();

        //    Dictionary<string, string> cStatus = new Dictionary<string, string>();
        //    cStatus.Add("1", "Rate Including Tax");
        //    cStatus.Add("0", "Rate Excluding Tax");
        //    ddlTaxCalcMethod.DataSource = new BindingSource(cStatus, null);
        //    ddlTaxCalcMethod.DisplayMember = "Value";
        //    ddlTaxCalcMethod.ValueMember = "Key";
        //    ddlTaxCalcMethod.SelectedIndex = 0;


        //}

    }
}
