﻿namespace FoodSmartDesktop.Masters
{
    partial class AddEditRestaurant
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddEditRestaurant));
            this.panel2 = new System.Windows.Forms.Panel();
            this.lbltoday = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblUserAddEdit = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.lblExit = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblRight = new System.Windows.Forms.Label();
            this.lblLeft = new System.Windows.Forms.Label();
            this.txtRestName = new System.Windows.Forms.TextBox();
            this.Label4 = new System.Windows.Forms.Label();
            this.txtRestPW = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtRatio = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.ChkPrintBill = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtPrinterName = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtVATNo = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtServTaxNo = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.ddlCalcMethod = new System.Windows.Forms.ComboBox();
            this.txtServTaxPer = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.ddlLoc = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtsbcPer = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtKOTPrinter = new System.Windows.Forms.TextBox();
            this.ddlBillPrinterType = new System.Windows.Forms.ComboBox();
            this.ddlKOTPrinterType = new System.Windows.Forms.ComboBox();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Black;
            this.panel2.Controls.Add(this.lbltoday);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Location = new System.Drawing.Point(-1, 388);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1027, 33);
            this.panel2.TabIndex = 157;
            // 
            // lbltoday
            // 
            this.lbltoday.BackColor = System.Drawing.Color.Transparent;
            this.lbltoday.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltoday.ForeColor = System.Drawing.Color.White;
            this.lbltoday.Location = new System.Drawing.Point(6, 3);
            this.lbltoday.Name = "lbltoday";
            this.lbltoday.Size = new System.Drawing.Size(180, 22);
            this.lbltoday.TabIndex = 22;
            this.lbltoday.Text = "Today";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Snow;
            this.label3.Location = new System.Drawing.Point(903, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 16);
            this.label3.TabIndex = 1;
            this.label3.Text = "Powered By Europia";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(199)))), ((int)(((byte)(233)))));
            this.panel1.Controls.Add(this.lblUserAddEdit);
            this.panel1.Controls.Add(this.lblUserName);
            this.panel1.Controls.Add(this.lblExit);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1026, 47);
            this.panel1.TabIndex = 158;
            // 
            // lblUserAddEdit
            // 
            this.lblUserAddEdit.BackColor = System.Drawing.Color.Transparent;
            this.lblUserAddEdit.Font = new System.Drawing.Font("Arial", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserAddEdit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.lblUserAddEdit.Location = new System.Drawing.Point(424, 14);
            this.lblUserAddEdit.Name = "lblUserAddEdit";
            this.lblUserAddEdit.Size = new System.Drawing.Size(200, 22);
            this.lblUserAddEdit.TabIndex = 161;
            this.lblUserAddEdit.Text = "Restaurant Add/Edit";
            // 
            // lblUserName
            // 
            this.lblUserName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(199)))), ((int)(((byte)(233)))));
            this.lblUserName.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.lblUserName.Location = new System.Drawing.Point(677, 16);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(159, 22);
            this.lblUserName.TabIndex = 21;
            this.lblUserName.Text = "Welcome ";
            // 
            // lblExit
            // 
            this.lblExit.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExit.ForeColor = System.Drawing.Color.DarkRed;
            this.lblExit.Image = ((System.Drawing.Image)(resources.GetObject("lblExit.Image")));
            this.lblExit.Location = new System.Drawing.Point(953, 0);
            this.lblExit.Name = "lblExit";
            this.lblExit.Size = new System.Drawing.Size(63, 47);
            this.lblExit.TabIndex = 1;
            this.lblExit.Click += new System.EventHandler(this.lblExit_Click);
            this.lblExit.MouseLeave += new System.EventHandler(this.lblExit_MouseLeave);
            this.lblExit.MouseHover += new System.EventHandler(this.lblExit_MouseHover);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Monotype Corsiva", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
            this.label1.Location = new System.Drawing.Point(3, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(247, 28);
            this.label1.TabIndex = 0;
            // 
            // lblRight
            // 
            this.lblRight.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblRight.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblRight.Location = new System.Drawing.Point(1023, 48);
            this.lblRight.Name = "lblRight";
            this.lblRight.Size = new System.Drawing.Size(3, 355);
            this.lblRight.TabIndex = 160;
            // 
            // lblLeft
            // 
            this.lblLeft.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblLeft.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblLeft.Location = new System.Drawing.Point(-1, 35);
            this.lblLeft.Name = "lblLeft";
            this.lblLeft.Size = new System.Drawing.Size(3, 355);
            this.lblLeft.TabIndex = 161;
            // 
            // txtRestName
            // 
            this.txtRestName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRestName.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRestName.Location = new System.Drawing.Point(165, 62);
            this.txtRestName.Name = "txtRestName";
            this.txtRestName.Size = new System.Drawing.Size(299, 26);
            this.txtRestName.TabIndex = 1;
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label4.Location = new System.Drawing.Point(30, 64);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(79, 20);
            this.Label4.TabIndex = 163;
            this.Label4.Text = "Restaurant:";
            // 
            // txtRestPW
            // 
            this.txtRestPW.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRestPW.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRestPW.Location = new System.Drawing.Point(165, 282);
            this.txtRestPW.Name = "txtRestPW";
            this.txtRestPW.PasswordChar = '*';
            this.txtRestPW.Size = new System.Drawing.Size(299, 26);
            this.txtRestPW.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(30, 283);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 20);
            this.label2.TabIndex = 165;
            this.label2.Text = "Password:";
            // 
            // txtRatio
            // 
            this.txtRatio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRatio.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRatio.Location = new System.Drawing.Point(165, 106);
            this.txtRatio.Name = "txtRatio";
            this.txtRatio.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtRatio.Size = new System.Drawing.Size(299, 26);
            this.txtRatio.TabIndex = 3;
            this.txtRatio.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRatio_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(30, 108);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(98, 20);
            this.label5.TabIndex = 167;
            this.label5.Text = "Sharing Ratio:";
            // 
            // ChkPrintBill
            // 
            this.ChkPrintBill.AutoSize = true;
            this.ChkPrintBill.Font = new System.Drawing.Font("Arial", 12F);
            this.ChkPrintBill.Location = new System.Drawing.Point(681, 112);
            this.ChkPrintBill.Name = "ChkPrintBill";
            this.ChkPrintBill.Size = new System.Drawing.Size(15, 14);
            this.ChkPrintBill.TabIndex = 6;
            this.ChkPrintBill.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(516, 109);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 20);
            this.label6.TabIndex = 169;
            this.label6.Text = "Print Bill:";
            // 
            // txtPrinterName
            // 
            this.txtPrinterName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPrinterName.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrinterName.Location = new System.Drawing.Point(228, 150);
            this.txtPrinterName.Name = "txtPrinterName";
            this.txtPrinterName.Size = new System.Drawing.Size(236, 26);
            this.txtPrinterName.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(30, 151);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 20);
            this.label7.TabIndex = 171;
            this.label7.Text = "Bill Printer:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(516, 151);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(86, 20);
            this.label8.TabIndex = 172;
            this.label8.Text = "KOT Printer:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(30, 196);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(109, 20);
            this.label9.TabIndex = 175;
            this.label9.Text = "Service Tax No.:";
            // 
            // txtVATNo
            // 
            this.txtVATNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVATNo.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVATNo.Location = new System.Drawing.Point(681, 194);
            this.txtVATNo.Name = "txtVATNo";
            this.txtVATNo.Size = new System.Drawing.Size(263, 26);
            this.txtVATNo.TabIndex = 8;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(520, 198);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 20);
            this.label10.TabIndex = 177;
            this.label10.Text = "VAT No:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(516, 284);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(133, 20);
            this.label11.TabIndex = 179;
            this.label11.Text = "Calculation Method:";
            // 
            // txtServTaxNo
            // 
            this.txtServTaxNo.AcceptsReturn = true;
            this.txtServTaxNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtServTaxNo.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtServTaxNo.Location = new System.Drawing.Point(165, 194);
            this.txtServTaxNo.Name = "txtServTaxNo";
            this.txtServTaxNo.Size = new System.Drawing.Size(299, 26);
            this.txtServTaxNo.TabIndex = 7;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(30, 240);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(100, 20);
            this.label12.TabIndex = 182;
            this.label12.Text = "Service Tax %:";
            // 
            // ddlCalcMethod
            // 
            this.ddlCalcMethod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlCalcMethod.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlCalcMethod.FormattingEnabled = true;
            this.ddlCalcMethod.Location = new System.Drawing.Point(681, 279);
            this.ddlCalcMethod.Name = "ddlCalcMethod";
            this.ddlCalcMethod.Size = new System.Drawing.Size(263, 26);
            this.ddlCalcMethod.TabIndex = 10;
            // 
            // txtServTaxPer
            // 
            this.txtServTaxPer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtServTaxPer.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtServTaxPer.Location = new System.Drawing.Point(165, 238);
            this.txtServTaxPer.Name = "txtServTaxPer";
            this.txtServTaxPer.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtServTaxPer.Size = new System.Drawing.Size(299, 26);
            this.txtServTaxPer.TabIndex = 9;
            this.txtServTaxPer.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtServTaxPer_KeyPress);
            // 
            // btnSave
            // 
            this.btnSave.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSave.BackgroundImage")));
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSave.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.Location = new System.Drawing.Point(803, 333);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(141, 49);
            this.btnSave.TabIndex = 12;
            this.btnSave.Text = "Save [F2]";
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.btnSave.MouseLeave += new System.EventHandler(this.btnSave_MouseLeave);
            this.btnSave.MouseHover += new System.EventHandler(this.btnSave_MouseHover);
            // 
            // ddlLoc
            // 
            this.ddlLoc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlLoc.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlLoc.FormattingEnabled = true;
            this.ddlLoc.Location = new System.Drawing.Point(681, 61);
            this.ddlLoc.Name = "ddlLoc";
            this.ddlLoc.Size = new System.Drawing.Size(263, 26);
            this.ddlLoc.TabIndex = 2;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(516, 64);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(67, 20);
            this.label13.TabIndex = 187;
            this.label13.Text = "Location:";
            // 
            // txtsbcPer
            // 
            this.txtsbcPer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtsbcPer.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsbcPer.Location = new System.Drawing.Point(681, 239);
            this.txtsbcPer.Name = "txtsbcPer";
            this.txtsbcPer.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtsbcPer.Size = new System.Drawing.Size(263, 26);
            this.txtsbcPer.TabIndex = 188;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(520, 240);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(56, 20);
            this.label14.TabIndex = 189;
            this.label14.Text = "SBC %:";
            // 
            // txtKOTPrinter
            // 
            this.txtKOTPrinter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtKOTPrinter.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKOTPrinter.Location = new System.Drawing.Point(739, 149);
            this.txtKOTPrinter.Name = "txtKOTPrinter";
            this.txtKOTPrinter.Size = new System.Drawing.Size(205, 26);
            this.txtKOTPrinter.TabIndex = 190;
            // 
            // ddlBillPrinterType
            // 
            this.ddlBillPrinterType.Font = new System.Drawing.Font("Arial", 12F);
            this.ddlBillPrinterType.FormattingEnabled = true;
            this.ddlBillPrinterType.Location = new System.Drawing.Point(165, 149);
            this.ddlBillPrinterType.Name = "ddlBillPrinterType";
            this.ddlBillPrinterType.Size = new System.Drawing.Size(57, 26);
            this.ddlBillPrinterType.TabIndex = 191;
            // 
            // ddlKOTPrinterType
            // 
            this.ddlKOTPrinterType.Font = new System.Drawing.Font("Arial", 12F);
            this.ddlKOTPrinterType.FormattingEnabled = true;
            this.ddlKOTPrinterType.Location = new System.Drawing.Point(676, 149);
            this.ddlKOTPrinterType.Name = "ddlKOTPrinterType";
            this.ddlKOTPrinterType.Size = new System.Drawing.Size(57, 26);
            this.ddlKOTPrinterType.TabIndex = 192;
            // 
            // AddEditRestaurant
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1033, 424);
            this.ControlBox = false;
            this.Controls.Add(this.ddlKOTPrinterType);
            this.Controls.Add(this.ddlBillPrinterType);
            this.Controls.Add(this.txtKOTPrinter);
            this.Controls.Add(this.txtsbcPer);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.ddlLoc);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.txtServTaxPer);
            this.Controls.Add(this.ddlCalcMethod);
            this.Controls.Add(this.txtServTaxNo);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtVATNo);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtPrinterName);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.ChkPrintBill);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtRatio);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtRestPW);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtRestName);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.lblLeft);
            this.Controls.Add(this.lblRight);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AddEditRestaurant";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.AddEditRestaurant_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lbltoday;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblUserAddEdit;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label lblExit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblRight;
        private System.Windows.Forms.Label lblLeft;
        private System.Windows.Forms.TextBox txtRestName;
        internal System.Windows.Forms.Label Label4;
        private System.Windows.Forms.TextBox txtRestPW;
        internal System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtRatio;
        internal System.Windows.Forms.Label label5;
        internal System.Windows.Forms.CheckBox ChkPrintBill;
        internal System.Windows.Forms.Label label6;
        internal System.Windows.Forms.TextBox txtPrinterName;
        internal System.Windows.Forms.Label label7;
        internal System.Windows.Forms.Label label8;
        internal System.Windows.Forms.Label label9;
        internal System.Windows.Forms.TextBox txtVATNo;
        internal System.Windows.Forms.Label label10;
        internal System.Windows.Forms.Label label11;
        internal System.Windows.Forms.TextBox txtServTaxNo;
        internal System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox ddlCalcMethod;
        internal System.Windows.Forms.TextBox txtServTaxPer;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ComboBox ddlLoc;
        internal System.Windows.Forms.Label label13;
        internal System.Windows.Forms.TextBox txtsbcPer;
        internal System.Windows.Forms.Label label14;
        internal System.Windows.Forms.TextBox txtKOTPrinter;
        private System.Windows.Forms.ComboBox ddlBillPrinterType;
        private System.Windows.Forms.ComboBox ddlKOTPrinterType;
    }
}