﻿namespace FoodSmartDesktop.Masters
{
    partial class MainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainMenu));
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblExit = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblChangePW = new System.Windows.Forms.Label();
            this.lblWelcome = new System.Windows.Forms.Label();
            this.wlbWebApp = new System.Windows.Forms.LinkLabel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.btnInvTran = new System.Windows.Forms.Button();
            this.btnTaxes = new System.Windows.Forms.Button();
            this.btnCounters = new System.Windows.Forms.Button();
            this.btnParkingRate = new System.Windows.Forms.Button();
            this.btnSetting = new System.Windows.Forms.Button();
            this.btnUser = new System.Windows.Forms.Button();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.lblExit);
            this.panel1.Controls.Add(this.label1);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // lblExit
            // 
            resources.ApplyResources(this.lblExit, "lblExit");
            this.lblExit.ForeColor = System.Drawing.Color.DarkRed;
            this.lblExit.Name = "lblExit";
            this.lblExit.Click += new System.EventHandler(this.lblExit_Click);
            this.lblExit.MouseLeave += new System.EventHandler(this.lblExit_MouseLeave);
            this.lblExit.MouseHover += new System.EventHandler(this.lblExit_MouseHover);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Name = "label1";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.tableLayoutPanel1, "tableLayoutPanel1");
            this.tableLayoutPanel1.Controls.Add(this.lblChangePW, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblWelcome, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.wlbWebApp, 2, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            // 
            // lblChangePW
            // 
            this.lblChangePW.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.lblChangePW, "lblChangePW");
            this.lblChangePW.ForeColor = System.Drawing.Color.White;
            this.lblChangePW.Name = "lblChangePW";
            this.lblChangePW.Click += new System.EventHandler(this.lblChangePW_Click);
            this.lblChangePW.MouseHover += new System.EventHandler(this.lblChangePW_MouseHover);
            // 
            // lblWelcome
            // 
            this.lblWelcome.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.lblWelcome, "lblWelcome");
            this.lblWelcome.ForeColor = System.Drawing.Color.White;
            this.lblWelcome.Name = "lblWelcome";
            // 
            // wlbWebApp
            // 
            resources.ApplyResources(this.wlbWebApp, "wlbWebApp");
            this.wlbWebApp.BackColor = System.Drawing.Color.Transparent;
            this.wlbWebApp.LinkColor = System.Drawing.Color.White;
            this.wlbWebApp.Name = "wlbWebApp";
            this.wlbWebApp.TabStop = true;
            this.wlbWebApp.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.wlbWebApp_LinkClicked);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.tableLayoutPanel3, "tableLayoutPanel3");
            this.tableLayoutPanel3.Controls.Add(this.btnCounters, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnUser, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnTaxes, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnParkingRate, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.btnSetting, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.btnInvTran, 1, 1);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            // 
            // btnInvTran
            // 
            resources.ApplyResources(this.btnInvTran, "btnInvTran");
            this.btnInvTran.Name = "btnInvTran";
            this.btnInvTran.UseVisualStyleBackColor = true;
            this.btnInvTran.Click += new System.EventHandler(this.btnInvTran_Click);
            this.btnInvTran.MouseLeave += new System.EventHandler(this.btnInvTran_MouseLeave);
            this.btnInvTran.MouseHover += new System.EventHandler(this.btnInvTran_MouseHover);
            // 
            // btnTaxes
            // 
            resources.ApplyResources(this.btnTaxes, "btnTaxes");
            this.btnTaxes.Name = "btnTaxes";
            this.btnTaxes.UseVisualStyleBackColor = true;
            this.btnTaxes.Click += new System.EventHandler(this.btnTaxes_Click);
            this.btnTaxes.MouseLeave += new System.EventHandler(this.btnTaxes_MouseLeave);
            this.btnTaxes.MouseHover += new System.EventHandler(this.btnTaxes_MouseHover);
            // 
            // btnCounters
            // 
            resources.ApplyResources(this.btnCounters, "btnCounters");
            this.btnCounters.Name = "btnCounters";
            this.btnCounters.UseVisualStyleBackColor = true;
            this.btnCounters.Click += new System.EventHandler(this.btnCounters_Click);
            this.btnCounters.MouseLeave += new System.EventHandler(this.btnCounters_MouseLeave);
            this.btnCounters.MouseHover += new System.EventHandler(this.btnCounters_MouseHover);
            // 
            // btnParkingRate
            // 
            resources.ApplyResources(this.btnParkingRate, "btnParkingRate");
            this.btnParkingRate.Name = "btnParkingRate";
            this.btnParkingRate.UseVisualStyleBackColor = true;
            this.btnParkingRate.Click += new System.EventHandler(this.btnParkingRate_Click);
            this.btnParkingRate.MouseLeave += new System.EventHandler(this.btnParkingRate_MouseLeave);
            this.btnParkingRate.MouseHover += new System.EventHandler(this.btnParkingRate_MouseHover);
            // 
            // btnSetting
            // 
            resources.ApplyResources(this.btnSetting, "btnSetting");
            this.btnSetting.Name = "btnSetting";
            this.btnSetting.UseVisualStyleBackColor = true;
            this.btnSetting.Click += new System.EventHandler(this.btnSetting_Click_1);
            this.btnSetting.MouseLeave += new System.EventHandler(this.btnSetting_MouseLeave);
            this.btnSetting.MouseHover += new System.EventHandler(this.btnSetting_MouseHover);
            // 
            // btnUser
            // 
            resources.ApplyResources(this.btnUser, "btnUser");
            this.btnUser.Name = "btnUser";
            this.btnUser.UseVisualStyleBackColor = true;
            this.btnUser.Click += new System.EventHandler(this.btnUser_Click);
            this.btnUser.MouseLeave += new System.EventHandler(this.btnUser_MouseLeave);
            this.btnUser.MouseHover += new System.EventHandler(this.btnUser_MouseHover);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            resources.ApplyResources(this.tableLayoutPanel4, "tableLayoutPanel4");
            this.tableLayoutPanel4.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.lblDate, 0, 0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.ForeColor = System.Drawing.Color.Snow;
            this.label3.Name = "label3";
            // 
            // lblDate
            // 
            resources.ApplyResources(this.lblDate, "lblDate");
            this.lblDate.ForeColor = System.Drawing.Color.Transparent;
            this.lblDate.Name = "lblDate";
            // 
            // MainMenu
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::FoodSmartDesktop.Properties.Resources.dashboard_bg;
            this.ControlBox = false;
            this.Controls.Add(this.tableLayoutPanel4);
            this.Controls.Add(this.tableLayoutPanel3);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainMenu";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainMenu_Load);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblExit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lblWelcome;
        private System.Windows.Forms.LinkLabel wlbWebApp;
        private System.Windows.Forms.Label lblChangePW;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button btnTaxes;
        private System.Windows.Forms.Button btnUser;
        private System.Windows.Forms.Button btnCounters;
        private System.Windows.Forms.Button btnParkingRate;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnInvTran;
        private System.Windows.Forms.Button btnSetting;
    }
}