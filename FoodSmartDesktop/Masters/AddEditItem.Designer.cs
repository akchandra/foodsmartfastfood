﻿namespace FoodSmartDesktop.Masters
{
    partial class AddEditItem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddEditItem));
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblUserAddEdit = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.lblExit = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lbltoday = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.Label8 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ddlRestName = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.ddlLoc = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.ddlGroup = new System.Windows.Forms.ComboBox();
            this.txtItemName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lblRight = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtSaleUnit = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtCGSTPer = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.ddlItemType = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.ddlDisc = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.dtIntroducedOn = new System.Windows.Forms.DateTimePicker();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtSGSTPer = new System.Windows.Forms.TextBox();
            this.txtHSNCode = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(199)))), ((int)(((byte)(233)))));
            this.panel1.Controls.Add(this.lblUserAddEdit);
            this.panel1.Controls.Add(this.lblUserName);
            this.panel1.Controls.Add(this.lblExit);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(5, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(987, 52);
            this.panel1.TabIndex = 4;
            // 
            // lblUserAddEdit
            // 
            this.lblUserAddEdit.BackColor = System.Drawing.Color.Transparent;
            this.lblUserAddEdit.Font = new System.Drawing.Font("Arial", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserAddEdit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.lblUserAddEdit.Location = new System.Drawing.Point(424, 14);
            this.lblUserAddEdit.Name = "lblUserAddEdit";
            this.lblUserAddEdit.Size = new System.Drawing.Size(159, 22);
            this.lblUserAddEdit.TabIndex = 161;
            this.lblUserAddEdit.Text = "Add / Edit Items";
            // 
            // lblUserName
            // 
            this.lblUserName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(199)))), ((int)(((byte)(233)))));
            this.lblUserName.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.lblUserName.Location = new System.Drawing.Point(677, 16);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(159, 22);
            this.lblUserName.TabIndex = 21;
            this.lblUserName.Text = "Welcome ";
            // 
            // lblExit
            // 
            this.lblExit.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExit.ForeColor = System.Drawing.Color.DarkRed;
            this.lblExit.Image = ((System.Drawing.Image)(resources.GetObject("lblExit.Image")));
            this.lblExit.Location = new System.Drawing.Point(909, 5);
            this.lblExit.Name = "lblExit";
            this.lblExit.Size = new System.Drawing.Size(63, 47);
            this.lblExit.TabIndex = 1;
            this.lblExit.Click += new System.EventHandler(this.lblExit_Click);
            this.lblExit.MouseLeave += new System.EventHandler(this.lblExit_MouseLeave);
            this.lblExit.MouseHover += new System.EventHandler(this.lblExit_MouseHover);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Monotype Corsiva", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
            this.label1.Location = new System.Drawing.Point(3, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(247, 42);
            this.label1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Black;
            this.panel2.Controls.Add(this.lbltoday);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Location = new System.Drawing.Point(3, 397);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(991, 33);
            this.panel2.TabIndex = 157;
            // 
            // lbltoday
            // 
            this.lbltoday.BackColor = System.Drawing.Color.Transparent;
            this.lbltoday.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltoday.ForeColor = System.Drawing.Color.White;
            this.lbltoday.Location = new System.Drawing.Point(6, 3);
            this.lbltoday.Name = "lbltoday";
            this.lbltoday.Size = new System.Drawing.Size(180, 22);
            this.lbltoday.TabIndex = 22;
            this.lbltoday.Text = "Today";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Snow;
            this.label3.Location = new System.Drawing.Point(848, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 16);
            this.label3.TabIndex = 1;
            this.label3.Text = "Powered By Europia";
            // 
            // btnSave
            // 
            this.btnSave.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSave.BackgroundImage")));
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSave.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.Location = new System.Drawing.Point(824, 342);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(141, 49);
            this.btnSave.TabIndex = 158;
            this.btnSave.Text = "Save [F2]";
            this.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.btnSave.MouseLeave += new System.EventHandler(this.btnSave_MouseLeave);
            this.btnSave.MouseHover += new System.EventHandler(this.btnSave_MouseHover);
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label8.Location = new System.Drawing.Point(41, 86);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(115, 20);
            this.Label8.TabIndex = 168;
            this.Label8.Text = "Item Description:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(45, 129);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 20);
            this.label2.TabIndex = 196;
            this.label2.Text = "Restaurant:";
            // 
            // ddlRestName
            // 
            this.ddlRestName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlRestName.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlRestName.FormattingEnabled = true;
            this.ddlRestName.Location = new System.Drawing.Point(181, 127);
            this.ddlRestName.Name = "ddlRestName";
            this.ddlRestName.Size = new System.Drawing.Size(263, 26);
            this.ddlRestName.TabIndex = 195;
            this.ddlRestName.SelectedIndexChanged += new System.EventHandler(this.ddlRestName_SelectedIndexChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(486, 86);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(67, 20);
            this.label13.TabIndex = 194;
            this.label13.Text = "Location:";
            // 
            // ddlLoc
            // 
            this.ddlLoc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlLoc.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlLoc.FormattingEnabled = true;
            this.ddlLoc.Location = new System.Drawing.Point(646, 84);
            this.ddlLoc.Name = "ddlLoc";
            this.ddlLoc.Size = new System.Drawing.Size(263, 26);
            this.ddlLoc.TabIndex = 193;
            this.ddlLoc.SelectedIndexChanged += new System.EventHandler(this.ddlLoc_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(486, 129);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 20);
            this.label4.TabIndex = 198;
            this.label4.Text = "Group:";
            // 
            // ddlGroup
            // 
            this.ddlGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlGroup.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlGroup.FormattingEnabled = true;
            this.ddlGroup.Location = new System.Drawing.Point(646, 127);
            this.ddlGroup.Name = "ddlGroup";
            this.ddlGroup.Size = new System.Drawing.Size(263, 26);
            this.ddlGroup.TabIndex = 197;
            // 
            // txtItemName
            // 
            this.txtItemName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtItemName.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtItemName.Location = new System.Drawing.Point(181, 84);
            this.txtItemName.Name = "txtItemName";
            this.txtItemName.Size = new System.Drawing.Size(263, 26);
            this.txtItemName.TabIndex = 199;
            this.txtItemName.TextChanged += new System.EventHandler(this.txtItemName_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(41, 170);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(102, 20);
            this.label5.TabIndex = 200;
            this.label5.Text = "Introduced On:";
            // 
            // lblRight
            // 
            this.lblRight.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblRight.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblRight.Location = new System.Drawing.Point(989, 52);
            this.lblRight.Name = "lblRight";
            this.lblRight.Size = new System.Drawing.Size(5, 350);
            this.lblRight.TabIndex = 202;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label6.Location = new System.Drawing.Point(3, 51);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(5, 350);
            this.label6.TabIndex = 203;
            // 
            // txtSaleUnit
            // 
            this.txtSaleUnit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSaleUnit.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSaleUnit.Location = new System.Drawing.Point(646, 168);
            this.txtSaleUnit.Name = "txtSaleUnit";
            this.txtSaleUnit.Size = new System.Drawing.Size(263, 26);
            this.txtSaleUnit.TabIndex = 205;
            this.txtSaleUnit.TextChanged += new System.EventHandler(this.txtSaleUnit_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(486, 170);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 20);
            this.label7.TabIndex = 204;
            this.label7.Text = "Sale Unit:";
            // 
            // txtCGSTPer
            // 
            this.txtCGSTPer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCGSTPer.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCGSTPer.Location = new System.Drawing.Point(181, 209);
            this.txtCGSTPer.Name = "txtCGSTPer";
            this.txtCGSTPer.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtCGSTPer.Size = new System.Drawing.Size(263, 26);
            this.txtCGSTPer.TabIndex = 207;
            this.txtCGSTPer.TextChanged += new System.EventHandler(this.txtVATPer_TextChanged);
            this.txtCGSTPer.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVATPer_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(41, 211);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 20);
            this.label9.TabIndex = 206;
            this.label9.Text = "CGST %:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(486, 211);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 20);
            this.label10.TabIndex = 208;
            this.label10.Text = "SGST %:";
            // 
            // ddlItemType
            // 
            this.ddlItemType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlItemType.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlItemType.FormattingEnabled = true;
            this.ddlItemType.Location = new System.Drawing.Point(181, 251);
            this.ddlItemType.Name = "ddlItemType";
            this.ddlItemType.Size = new System.Drawing.Size(263, 26);
            this.ddlItemType.TabIndex = 210;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(41, 253);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(50, 20);
            this.label11.TabIndex = 211;
            this.label11.Text = "Brand:";
            // 
            // ddlDisc
            // 
            this.ddlDisc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlDisc.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlDisc.FormattingEnabled = true;
            this.ddlDisc.Location = new System.Drawing.Point(646, 251);
            this.ddlDisc.Name = "ddlDisc";
            this.ddlDisc.Size = new System.Drawing.Size(263, 26);
            this.ddlDisc.TabIndex = 213;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(486, 253);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(121, 20);
            this.label12.TabIndex = 212;
            this.label12.Text = "Discount Allowed:";
            // 
            // dtIntroducedOn
            // 
            this.dtIntroducedOn.CalendarFont = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtIntroducedOn.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtIntroducedOn.Location = new System.Drawing.Point(181, 170);
            this.dtIntroducedOn.Name = "dtIntroducedOn";
            this.dtIntroducedOn.Size = new System.Drawing.Size(263, 26);
            this.dtIntroducedOn.TabIndex = 214;
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(181, 293);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(263, 26);
            this.textBox1.TabIndex = 216;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(41, 295);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(95, 20);
            this.label14.TabIndex = 215;
            this.label14.Text = "Item Barcode:";
            // 
            // txtSGSTPer
            // 
            this.txtSGSTPer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSGSTPer.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSGSTPer.Location = new System.Drawing.Point(646, 209);
            this.txtSGSTPer.Name = "txtSGSTPer";
            this.txtSGSTPer.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSGSTPer.Size = new System.Drawing.Size(263, 26);
            this.txtSGSTPer.TabIndex = 217;
            // 
            // txtHSNCode
            // 
            this.txtHSNCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtHSNCode.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHSNCode.Location = new System.Drawing.Point(646, 293);
            this.txtHSNCode.Name = "txtHSNCode";
            this.txtHSNCode.Size = new System.Drawing.Size(263, 26);
            this.txtHSNCode.TabIndex = 219;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(486, 294);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(76, 20);
            this.label15.TabIndex = 218;
            this.label15.Text = "HSN Code:";
            // 
            // AddEditItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(992, 431);
            this.ControlBox = false;
            this.Controls.Add(this.txtHSNCode);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.txtSGSTPer);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.dtIntroducedOn);
            this.Controls.Add(this.ddlDisc);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.ddlItemType);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtCGSTPer);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtSaleUnit);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblRight);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtItemName);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.ddlGroup);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ddlRestName);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.ddlLoc);
            this.Controls.Add(this.Label8);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AddEditItem";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.AddEditItem_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblUserAddEdit;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label lblExit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lbltoday;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnSave;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox ddlRestName;
        internal System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox ddlLoc;
        internal System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox ddlGroup;
        private System.Windows.Forms.TextBox txtItemName;
        internal System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblRight;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtSaleUnit;
        internal System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtCGSTPer;
        internal System.Windows.Forms.Label label9;
        internal System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox ddlItemType;
        internal System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox ddlDisc;
        internal System.Windows.Forms.Label label12;
        private System.Windows.Forms.DateTimePicker dtIntroducedOn;
        private System.Windows.Forms.TextBox textBox1;
        internal System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtSGSTPer;
        private System.Windows.Forms.TextBox txtHSNCode;
        internal System.Windows.Forms.Label label15;
    }
}