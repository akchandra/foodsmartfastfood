﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FoodSmart.BLL;
using FoodSmart.Common;
using System.Configuration;
using System.IO;
using System.Drawing.Drawing2D;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using FoodSmart.Utilities.Cryptography;

namespace FoodSmartDesktop.Masters
{
    public partial class AddEditItemGroup : Form
    {
        string stat;
        int LID;
        int GroupID;

        public AddEditItemGroup(int ID)
        {
            InitializeComponent();

            if (ID == 0)
            {
                stat = "A";
                LID = 0;
            }
            else
            {
                stat = "E";

                LID = ID;
            }
        }
        #region "Public Methods"
        private void AddEditItemGroup_Load(object sender, EventArgs e)
        {
            this.ddlLoc.SelectedIndexChanged -= new System.EventHandler(this.ddlLoc_SelectedIndexChanged);
            LoadLoc();
            Initialize();
            if (LID != 0)
                FillItemGroup(LID);
            this.ddlLoc.SelectedIndexChanged += new System.EventHandler(this.ddlLoc_SelectedIndexChanged);
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (ValidateEntry())
            {
                IItems ItemGrp = new ItemsEntity();
                ItemGrp.GroupDescription = txtGroupName.Text;
                ItemGrp.ItemGroupID = LID;
                ItemGrp.RestID = ddlRestName.SelectedValue.ToInt();
                ItemGrp.LocID = ddlLoc.SelectedValue.ToInt();
                ItemGrp.CreatedBy = GeneralFunctions.LoginInfo.UserID;

                int Status = new ItemBLL().SaveItemGroup(ItemGrp, stat);

                if (Status == 0)
                {
                    MessageBox.Show("Error!! Record Not Saved", "Save Record", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    if (stat == "A")
                    {
                        Initialize();
                        txtGroupName.Focus();
                    }
                    else
                    {
                        this.Close();
                    }
                }
            }
        }
        private void ddlLoc_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadRest(ddlLoc.Text);
            ddlRestName.SelectedIndex = 0;
        }

        private void lblExit_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void lblExit_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnSave_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnSave_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }
        #endregion

        #region "Private Methods"
        private void LoadLoc()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            //populate Locations
            SearchCriteria searchcriteria = new SearchCriteria();
            searchcriteria.QueryStat = "L";

            ds = new LocationBLL().GetAllLoc(searchcriteria);
            dt = ds.Tables[0];

            DataRow nullrow = dt.NewRow();
            nullrow["pk_LocID"] = 0;
            nullrow["LocationName"] = "Select Locations";
            dt.Rows.Add(nullrow);

            ddlLoc.DataSource = dt;
            ddlLoc.DisplayMember = "LocationName";
            ddlLoc.ValueMember = "pk_LocID";
            ddlLoc.SelectedValue = "0";
        }

        private void LoadRest(string Locname)
        {
            //DataRow row = new DataRow();
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            SearchCriteria searchcriteria = new SearchCriteria();
            searchcriteria.QueryStat = "L";
            if (ddlLoc.Text == "Select Locations")
            {
                MessageBox.Show("Please Select select a location for Item Group", "Location Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                ddlLoc.Focus();
            }
            else
                searchcriteria.LocName = Locname;

            ds = new RestaurantBLL().GetAllRestaurant(searchcriteria);
            dt = ds.Tables[0];

            //DataRow nullrow = dt.NewRow();
            //nullrow["pk_RestID"] = 0;
            //nullrow["RestName"] = "Select Restaurants";
            //dt.Rows.Add(nullrow);

            ddlRestName.DataSource = dt;
            ddlRestName.DisplayMember = "RestName";
            ddlRestName.ValueMember = "pk_RestID";
            ddlRestName.SelectedValue = "0";

        }

        private void FillItemGroup(int ItemGooupId)
        {
            ItemBLL oItembll = new ItemBLL();

            ItemsEntity oitem = (ItemsEntity)oItembll.GetItemGroupForEdit(ItemGooupId);

            ddlLoc.SelectedValue = oitem.LocID;
            LoadRest(oitem.LocName);
            ddlRestName.SelectedValue = oitem.RestID;
            txtGroupName.Text = oitem.GroupDescription;
        }

        private void Initialize()
        {
            this.ddlLoc.SelectedIndexChanged -= new System.EventHandler(this.ddlLoc_SelectedIndexChanged);
            txtGroupName.Text = string.Empty;
            ddlLoc.SelectedValue = "0";
            ddlRestName.DataSource = null;
            this.ddlLoc.SelectedIndexChanged += new System.EventHandler(this.ddlLoc_SelectedIndexChanged);
            //ddlRestName.SelectedValue = "0";
        }

        private bool ValidateEntry()
        {
            bool RetVal = true;

            if (string.IsNullOrEmpty(txtGroupName.Text))
            {
                txtGroupName.BackColor = Color.Red;
                RetVal = false;
            }

            if (ddlLoc.SelectedIndex == -1 || ddlLoc.SelectedValue == "0")
            {
                ddlLoc.BackColor = Color.Red;
                RetVal = false;
            }

            if (ddlRestName.SelectedIndex == -1 || ddlRestName.SelectedValue == "0")
            {
                ddlRestName.BackColor = Color.Red;
                RetVal = false;
            }

            return RetVal;
        }


        #endregion

        

       
    }
}
