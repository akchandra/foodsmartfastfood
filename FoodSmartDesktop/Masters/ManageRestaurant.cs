﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using FoodSmart.DAL;
using FoodSmart.BLL;
using FoodSmart.Entity;
using FoodSmart.GeneralUtilities;
using FoodSmart.Utilities;
using FoodSmart.Common;
namespace FoodSmartDesktop.Masters
{
    public partial class ManageRestaurant : Form
    {
        BindingSource source1 = new BindingSource();
        private int PgSize = 12;
        private int CurrentPageIndex = 1;
        private int TotalPage = 0;
        private DataSet dsPage;

        public ManageRestaurant()
        {
            InitializeComponent();
        }

        #region Public Method
        private void ManageRestaurant_Load(object sender, EventArgs e)
        {
            dvgRest.ColumnCount = 8;
            this.ddlLoc.SelectedIndexChanged -= new System.EventHandler(this.ddlLoc_SelectedIndexChanged);
            SetWatermarkText();
            LoadDDL();

            SearchCriteria searchCriteria = new SearchCriteria();
            BuildDefaultSearch(searchCriteria);

            LoadRestaurants(searchCriteria);

            this.ddlLoc.SelectedIndexChanged += new System.EventHandler(this.ddlLoc_SelectedIndexChanged);
            foreach (DataGridViewRow row in dvgRest.Rows)
            {
                row.Height = 25;
            }

            lbltoday.Text += " : " + GeneralFunctions.LoginInfo.CurrDate.ToString("dd-MMM-yyyy");
            lblUserName.Text += GeneralFunctions.LoginInfo.UserFullName;
            ddlLoc.Focus();
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAddRest_Click(object sender, EventArgs e)
        {
            this.Hide();
            Masters.AddEditRestaurant User = new Masters.AddEditRestaurant(0);
            if (User.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {

            }
            else
            {
                this.Show();
                //dvgItem.Columns.Remove("Dimg");
                dvgRest.Columns.Remove("Eimg");
                dvgRest.Columns.Remove("Dimg");
                SearchCriteria searchCriteria = new SearchCriteria();
                BuildDefaultSearch(searchCriteria);
                LoadRestaurants(searchCriteria);
                foreach (DataGridViewRow row in dvgRest.Rows)
                {
                    row.Height = 25;
                }
            }
        }

        private void dvgRest_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int Reply;
            int i;
            if (dvgRest.RowCount == 0)
                return;
            else
                i = dvgRest.CurrentRow.Index;

            if (dvgRest.Columns[e.ColumnIndex].Name == "Dimg")
            {
                if (!CheckForDeleted(Convert.ToInt32(dvgRest.CurrentRow.Cells[0].Value)))
                {
                    MessageBox.Show("Record Already Deleted.", "Delete Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }

                Reply = Convert.ToInt32(MessageBox.Show("Want to Delete this Record?", "Delete Record", MessageBoxButtons.OKCancel, MessageBoxIcon.Question));
                if (Reply == 1)
                {
                    Reply = Convert.ToInt32(dvgRest.CurrentRow.Cells[0].Value);

                    RestaurantBLL restBLL = new RestaurantBLL();
                    restBLL.DeleteRest(Reply);

                    dvgRest.Columns.Remove("Dimg");
                    dvgRest.Columns.Remove("Eimg");
                    SearchCriteria searchCriteria = new SearchCriteria();
                    BuildDefaultSearch(searchCriteria);
                    LoadRestaurants(searchCriteria);
                    foreach (DataGridViewRow row in dvgRest.Rows)
                    {
                        row.Height = 25;
                    }
                }
            }

            if (dvgRest.Columns[e.ColumnIndex].Name == "Eimg")
            {
                Masters.AddEditRestaurant frm1 = new Masters.AddEditRestaurant(Convert.ToInt32(dvgRest.CurrentRow.Cells[0].Value));
                this.Hide();

                if (frm1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    this.Hide();
                }
                else
                {
                    this.Show();
                    dvgRest.Columns.Remove("Dimg");
                    dvgRest.Columns.Remove("Eimg");
                    SearchCriteria searchCriteria = new SearchCriteria();
                    BuildDefaultSearch(searchCriteria);
                    LoadRestaurants(searchCriteria);
                    foreach (DataGridViewRow row in dvgRest.Rows)
                    {
                        row.Height = 25;
                    }
                }
            }
        }

        private void txtRestName_TextChanged(object sender, EventArgs e)
        {
            CheckFilterCondition();
        }

        private void ddlLoc_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckFilterCondition();
        }

      
        #endregion

        #region "Private Methods"
        private void LoadRestaurants(SearchCriteria searchCriteria)
        {
            string path = Application.StartupPath + "\\";
            DataView view1;

            DataGridViewCellStyle style = new DataGridViewCellStyle();
            dvgRest.EnableHeadersVisualStyles = false;
            style.ForeColor = Color.Blue;

            dvgRest.AutoGenerateColumns = false;

            dvgRest.Columns[0].Name = "pk_RestID";
            dvgRest.Columns[1].Name = "Location";
            dvgRest.Columns[2].Name = "Restaurant Name";
            //dvgRest.Columns[2].Name = "Sharing Ratio";
            dvgRest.Columns[3].Name = "VAT No";
            dvgRest.Columns[4].Name = "Service Tax No";
            dvgRest.Columns[5].Name = "Service Tax %";
            //dvgRest.Columns[5].Name = "Printer Type";
            dvgRest.Columns[6].Name = "Restaurant Type";
            dvgRest.Columns[7].Name = "Card No";

            dvgRest.Columns[1].HeaderText = "Location";
            dvgRest.Columns[2].HeaderText = "Restaurant Name";
            dvgRest.Columns[3].HeaderText = "VAT No";
            dvgRest.Columns[4].HeaderText = "S.Tax No";
            dvgRest.Columns[5].HeaderText = "S.Tax %";
            dvgRest.Columns[6].HeaderText = "Restaurant Type";
            dvgRest.Columns[7].HeaderText = "Card No";
            //dvgRest.Columns[8].HeaderText = "Parking";
            //dvgRest.Columns[5].HeaderText = "Cashier / Restaurant";

            dvgRest.Columns[0].DataPropertyName = "pk_RestID";
            dvgRest.Columns[1].DataPropertyName = "LocationName";
            dvgRest.Columns[2].DataPropertyName = "RestName";
            dvgRest.Columns[3].DataPropertyName = "VATNo";
            dvgRest.Columns[4].DataPropertyName = "ServiceTaxNo";
            dvgRest.Columns[5].DataPropertyName = "ServiceTaxPer";
            dvgRest.Columns[6].DataPropertyName = "RESTTYPE";
            dvgRest.Columns[7].DataPropertyName = "CardNoOutside";

            dvgRest.Columns[1].Width = 100;
            dvgRest.Columns[2].Width = 240;
            dvgRest.Columns[3].Width = 120;
            dvgRest.Columns[4].Width = 150;
            dvgRest.Columns[5].Width = 55;
            dvgRest.Columns[6].Width = 100;
            dvgRest.Columns[7].Width = 100;

            dvgRest.Columns[1].SortMode = DataGridViewColumnSortMode.Automatic;
            dvgRest.Columns[2].SortMode = DataGridViewColumnSortMode.Automatic;
            dvgRest.Columns[6].SortMode = DataGridViewColumnSortMode.Automatic;
            dvgRest.Columns[7].SortMode = DataGridViewColumnSortMode.Automatic;

            dvgRest.Columns[3].SortMode = DataGridViewColumnSortMode.NotSortable;
            dvgRest.Columns[4].SortMode = DataGridViewColumnSortMode.NotSortable;
            dvgRest.Columns[5].SortMode = DataGridViewColumnSortMode.NotSortable;
            //dvgRest.Columns[7].SortMode = DataGridViewColumnSortMode.NotSortable;

            dvgRest.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            //dvgRest.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            dvgRest.Columns[5].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
            //dvgRest.Columns[4].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;

            dvgRest.Columns[0].Visible = false;
            //dvgRest.Columns[9].Visible = false;

            DataGridViewImageColumn Editimg = new DataGridViewImageColumn();
            Image Editimage = Image.FromFile(path + ConfigurationManager.AppSettings["imageFolder"] + "edit-icon.png");
            Editimg.Image = Editimage;
            dvgRest.Columns.Add(Editimg);
            Editimg.HeaderText = "Edit";
            Editimg.Name = "Eimg";
            Editimg.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            Editimg.Width = 40;

            DataGridViewImageColumn Delimg = new DataGridViewImageColumn();
            Image Delimage = Image.FromFile(path + ConfigurationManager.AppSettings["imageFolder"] + "delete-icon.png");
            Delimg.Image = Delimage;
            dvgRest.Columns.Add(Delimg);
            Delimg.HeaderText = "Del";
            Delimg.Name = "Dimg";
            Delimg.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            Delimg.Width = 40;

            dvgRest.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dvgRest.MultiSelect = false;

            RestaurantBLL restbll = new RestaurantBLL();
            dsPage = restbll.GetAllRestaurant(searchCriteria);
            if (dsPage.Tables[0].Rows.Count == 0)
                view1 = null;
            else
            {
                TotalPage = GeneralFunctions.CalculateTotalPages(dsPage.Tables[0], PgSize);
                view1 = new DataView(GeneralFunctions.GetCurrentRecords(0, dsPage.Tables[0], PgSize, "pk_RestID"));
                CurrentPageIndex = 0;
                lblPageNo.Text = 1.ToString() + " / " + TotalPage.ToString();

            }
            source1.DataSource = view1;

            dvgRest.DataSource = source1;
            dvgRest.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            dvgRest.AllowUserToResizeColumns = false;
            dvgRest.AllowUserToAddRows = false;
            dvgRest.ReadOnly = true;
            dvgRest.Columns[1].HeaderCell.Style = style;
            dvgRest.Columns[2].HeaderCell.Style = style;
            dvgRest.Columns[6].HeaderCell.Style = style;
            dvgRest.Columns[7].HeaderCell.Style = style;
        }

        private void BuildDefaultSearch(SearchCriteria criteria)
        {
            criteria.CardNoExternal = "";
            criteria.CardName = "";
            criteria.CardType = ddlLoc.SelectedValue.ToString();
        }

        private void BuildSearchCriteria(SearchCriteria criteria)
        {
            criteria.RestName = txtRestName.Text;
            //criteria.CardName = txtName.Text;
            criteria.LocName = ddlLoc.Text.ToString();
        }

        private void CheckFilterCondition()
        {
            SearchCriteria searchCriteria = new SearchCriteria();
            BuildSearchCriteria(searchCriteria);
            dvgRest.Columns.Remove("Dimg");
            dvgRest.Columns.Remove("Eimg");
            LoadRestaurants(searchCriteria);

            //LoadRestaurants(searchCriteria);
            foreach (DataGridViewRow row in dvgRest.Rows)
            {
                row.Height = 25;
            }
        }

        private void SetWatermarkText()
        {

            WatermarkText.SetWatermark(txtRestName, "Card No");
            //WatermarkText.SetWatermark(txtName, "User / Restaurant Name");
        }

        private void LoadDDL()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            //populate Locations
            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.QueryStat = "L";

            ds = new LocationBLL().GetAllLoc(searchCriteria);
            dt = ds.Tables[0];
            ddlLoc.DataSource = dt;
            ddlLoc.DisplayMember = "LocationName";
            ddlLoc.ValueMember = "pk_LocID";
            ddlLoc.SelectedIndex = 0;


            ////DataRow row = new DataRow();

            //Dictionary<string, string> cStatus = new Dictionary<string, string>();
            //cStatus.Add("R", "Restaurant");
            //cStatus.Add("P", "Parking");
            //ddlLoc.DataSource = new BindingSource(cStatus, null);
            //ddlLoc.DisplayMember = "Value";
            //ddlLoc.ValueMember = "Key";
            //ddlLoc.SelectedIndex = 0;

        }

        private bool CheckForDeleted(int Restid)
        {
            RestaurantBLL restbll = new RestaurantBLL();
            FoodSmart.Entity.RestaurantEntity orestEntity = (FoodSmart.Entity.RestaurantEntity)restbll.GetRestData(Restid);
            if (orestEntity.RestStatus)
            {
                return true;
            }
            else
                return false;
        }

        #endregion

        #region "Cursor Type Change"


        #endregion

        #region "Pagination Control"

        private void ChangePageIndex()
        {
            DataView view1;
            view1 = new DataView(GeneralFunctions.GetCurrentRecords(CurrentPageIndex, dsPage.Tables[0], PgSize, "pk_RestID"));
            source1.DataSource = view1;
            int pg = CurrentPageIndex + 1;
            lblPageNo.Text = pg.ToString() + " / " + TotalPage.ToString();
            foreach (DataGridViewRow row in dvgRest.Rows)
            {
                row.Height = 25;
            }
        }


        #endregion

        private void lblExit_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void lblExit_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnAddRest_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnAddRest_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void picMoveLast_Click(object sender, EventArgs e)
        {
            if (CurrentPageIndex + 1 < TotalPage)
            {
                CurrentPageIndex = TotalPage - 1;
                ChangePageIndex();
            }
        }

        private void picMoveNext_Click(object sender, EventArgs e)
        {
            if (CurrentPageIndex + 1 < TotalPage)
            {
                CurrentPageIndex++;
                ChangePageIndex();
            }
        }

        private void picMovePrev_Click(object sender, EventArgs e)
        {
            if (CurrentPageIndex != 0)
            {
                CurrentPageIndex--;
                ChangePageIndex();
            }
        }

        private void picMoveFirst_Click(object sender, EventArgs e)
        {
            if (CurrentPageIndex != 0)
            {
                CurrentPageIndex = 0;
                ChangePageIndex();
            }
        }

       
    }
}
