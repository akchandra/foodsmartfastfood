﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using FoodSmart.DAL;
using FoodSmart.BLL;
using FoodSmart.Entity;
using FoodSmart.GeneralUtilities;
using FoodSmart.Utilities;
using FoodSmart.Common;

namespace FoodSmartDesktop.Masters
{
    public partial class ManageItems : Form
    {
        BindingSource source1 = new BindingSource();
        private int PgSize = 12;
        private int CurrentPageIndex = 1;
        private int TotalPage = 0;
        private DataSet dsPage;

        public ManageItems()
        {
            InitializeComponent();
        }

        #region Public Method
        private void ManageItems_Load(object sender, EventArgs e)
        {
            dvgItem.ColumnCount = 8;
            this.ddlRestName.SelectedIndexChanged -= new System.EventHandler(this.ddlRestName_SelectedIndexChanged);
            this.ddlLoc.SelectedIndexChanged -= new System.EventHandler(this.ddlLoc_SelectedIndexChanged);
            this.ddlGroup.SelectedIndexChanged -= new System.EventHandler(this.ddlGroup_SelectedIndexChanged);

            SetWatermarkText();
            LoadLoc();
            LoadRest();
            LoadItemGroup();

            SearchCriteria searchCriteria = new SearchCriteria();
            BuildDefaultSearch(searchCriteria);

            LoadItem(searchCriteria);

            this.ddlRestName.SelectedIndexChanged += new System.EventHandler(this.ddlRestName_SelectedIndexChanged);
            this.ddlLoc.SelectedIndexChanged += new System.EventHandler(this.ddlLoc_SelectedIndexChanged);
            this.ddlGroup.SelectedIndexChanged += new System.EventHandler(this.ddlGroup_SelectedIndexChanged);
            foreach (DataGridViewRow row in dvgItem.Rows)
            {
                row.Height = 25;
            }

            lbltoday.Text += " : " + GeneralFunctions.LoginInfo.CurrDate.ToString("dd-MMM-yyyy");
            lblUserName.Text += GeneralFunctions.LoginInfo.UserFullName;
            ddlRestName.Focus();
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAddItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Masters.AddEditItem User = new Masters.AddEditItem(0);
            if (User.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {

            }
            else
            {
                this.Show();
                //dvgItem.Columns.Remove("Dimg");
                dvgItem.Columns.Remove("Eimg");
                dvgItem.Columns.Remove("Dimg");
                SearchCriteria searchCriteria = new SearchCriteria();
                BuildDefaultSearch(searchCriteria);
                LoadItem(searchCriteria);
                foreach (DataGridViewRow row in dvgItem.Rows)
                {
                    row.Height = 25;
                }
            }
        }

        private void dvgItem_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int Reply;
            int i;
            if (dvgItem.RowCount == 0)
                return;
            else
                i = dvgItem.CurrentRow.Index;

            if (dvgItem.Columns[e.ColumnIndex].Name == "Dimg")
            {
                if (!CheckForDeleted(Convert.ToInt32(dvgItem.CurrentRow.Cells[0].Value)))
                {
                    MessageBox.Show("Record Already Deleted.", "Delete Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }

                //if (!CheckDeletePossible((Convert.ToInt32(dvgItem.CurrentRow.Cells[0].Value))))
                //{
                //    DialogResult dialogResult = MessageBox.Show("Item Present on Tra. Deletion on Group will Delete all Items within the Group", "Delete Record", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                //    if (dialogResult != DialogResult.OK)
                //        return;
                //}

                Reply = Convert.ToInt32(MessageBox.Show("Want to Delete this Record?", "Delete Record", MessageBoxButtons.OKCancel, MessageBoxIcon.Question));
                if (Reply == 1)
                {
                    Reply = Convert.ToInt32(dvgItem.CurrentRow.Cells[0].Value);

                    ItemBLL itembll = new ItemBLL();

                    //RestaurantBLL restBLL = new RestaurantBLL();
                    itembll.DeleteItem(Reply);

                    dvgItem.Columns.Remove("Dimg");
                    dvgItem.Columns.Remove("Eimg");
                    SearchCriteria searchCriteria = new SearchCriteria();
                    BuildDefaultSearch(searchCriteria);
                    LoadItem(searchCriteria);
                    foreach (DataGridViewRow row in dvgItem.Rows)
                    {
                        row.Height = 25;
                    }
                }
            }

            if (dvgItem.Columns[e.ColumnIndex].Name == "Eimg")
            {
                Masters.AddEditItem frm1 = new Masters.AddEditItem(Convert.ToInt32(dvgItem.CurrentRow.Cells[0].Value));
                this.Hide();

                if (frm1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    this.Hide();
                }
                else
                {
                    this.Show();
                    dvgItem.Columns.Remove("Dimg");
                    dvgItem.Columns.Remove("Eimg");
                    SearchCriteria searchCriteria = new SearchCriteria();
                    BuildDefaultSearch(searchCriteria);
                    LoadItem(searchCriteria);
                    foreach (DataGridViewRow row in dvgItem.Rows)
                    {
                        row.Height = 25;
                    }
                }
            }
        }
        private void ddlLoc_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadRest();
            CheckFilterCondition();
        }

        private void ddlRestName_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadItemGroup();
            CheckFilterCondition();
        }

        private void ddlGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckFilterCondition();
        }

        
        private void txtItemName_TextChanged(object sender, EventArgs e)
        {
            CheckFilterCondition();
        }

        private void picMoveFirst_Click(object sender, EventArgs e)
        {
            if (CurrentPageIndex != 0)
            {
                CurrentPageIndex = 0;
                ChangePageIndex();
            }
        }

        private void picMovePrev_Click(object sender, EventArgs e)
        {
            if (CurrentPageIndex != 0)
            {
                CurrentPageIndex--;
                ChangePageIndex();
            }
        }

        private void picMoveNext_Click(object sender, EventArgs e)
        {
            if (CurrentPageIndex + 1 < TotalPage)
            {
                CurrentPageIndex++;
                ChangePageIndex();
            }
        }

        private void picMoveLast_Click(object sender, EventArgs e)
        {
            if (CurrentPageIndex + 1 < TotalPage)
            {
                CurrentPageIndex = TotalPage - 1;
                ChangePageIndex();
            }
        }

        #endregion

        #region "Private Methods"
        private void ChangePageIndex()
        {
            DataView view1;
            view1 = new DataView(GeneralFunctions.GetCurrentRecords(CurrentPageIndex, dsPage.Tables[0], PgSize, "itemID"));
            source1.DataSource = view1;
            int pg = CurrentPageIndex + 1;
            lblPageNo.Text = pg.ToString() + " / " + TotalPage.ToString();
            foreach (DataGridViewRow row in dvgItem.Rows)
            {
                row.Height = 25;
            }
        }

        private void LoadItem(SearchCriteria searchCriteria)
        {
            string path = Application.StartupPath + "\\";
            DataView view1;

            DataGridViewCellStyle style = new DataGridViewCellStyle();
            dvgItem.EnableHeadersVisualStyles = false;
            style.ForeColor = Color.Blue;

            dvgItem.AutoGenerateColumns = false;

            dvgItem.Columns[0].Name = "ItemID";
            dvgItem.Columns[1].Name = "Item Name";
            dvgItem.Columns[2].Name = "Location";
            dvgItem.Columns[3].Name = "Restaurant Name";
            dvgItem.Columns[4].Name = "Item Group";
            dvgItem.Columns[5].Name = "VAT %";
            dvgItem.Columns[6].Name = "S.Tax";
            dvgItem.Columns[7].Name = "Introduced";

            dvgItem.Columns[1].HeaderText = "Item";
            dvgItem.Columns[2].HeaderText = "Location";
            dvgItem.Columns[3].HeaderText = "Restaurant";
            dvgItem.Columns[4].HeaderText = "Item Group";
            dvgItem.Columns[5].HeaderText = "VAT %";
            dvgItem.Columns[6].HeaderText = "S.Tax";
            dvgItem.Columns[7].HeaderText = "Introduced";

            dvgItem.Columns[0].DataPropertyName = "ItemID";
            dvgItem.Columns[1].DataPropertyName = "ItemDescr";
            dvgItem.Columns[2].DataPropertyName = "LocationName";
            dvgItem.Columns[3].DataPropertyName = "RestName";
            dvgItem.Columns[4].DataPropertyName = "GroupName";
            dvgItem.Columns[5].DataPropertyName = "VATPer";
            dvgItem.Columns[6].DataPropertyName = "ServTaxApp";
            dvgItem.Columns[7].DataPropertyName = "EffectiveDate";

            dvgItem.Columns[1].Width = 200;
            dvgItem.Columns[2].Width = 150;
            dvgItem.Columns[3].Width = 150;
            dvgItem.Columns[4].Width = 150;
            dvgItem.Columns[5].Width = 55;
            dvgItem.Columns[6].Width = 50;
            dvgItem.Columns[7].Width = 100;

            dvgItem.Columns[1].SortMode = DataGridViewColumnSortMode.Automatic;
            dvgItem.Columns[2].SortMode = DataGridViewColumnSortMode.Automatic;
            dvgItem.Columns[3].SortMode = DataGridViewColumnSortMode.Automatic;
            dvgItem.Columns[4].SortMode = DataGridViewColumnSortMode.Automatic;
            dvgItem.Columns[5].SortMode = DataGridViewColumnSortMode.NotSortable;
            dvgItem.Columns[7].SortMode = DataGridViewColumnSortMode.NotSortable;

            dvgItem.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dvgItem.Columns[5].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;

            dvgItem.Columns[0].Visible = false;
            //dvgItem.Columns[9].Visible = false;

            DataGridViewImageColumn Editimg = new DataGridViewImageColumn();
            Image Editimage = Image.FromFile(path + ConfigurationManager.AppSettings["imageFolder"] + "edit-icon.png");
            Editimg.Image = Editimage;
            dvgItem.Columns.Add(Editimg);
            Editimg.HeaderText = "Edit";
            Editimg.Name = "Eimg";
            Editimg.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            Editimg.Width = 40;

            DataGridViewImageColumn Delimg = new DataGridViewImageColumn();
            Image Delimage = Image.FromFile(path + ConfigurationManager.AppSettings["imageFolder"] + "delete-icon.png");
            Delimg.Image = Delimage;
            dvgItem.Columns.Add(Delimg);
            Delimg.HeaderText = "Del";
            Delimg.Name = "Dimg";
            Delimg.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            Delimg.Width = 40;

            dvgItem.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dvgItem.MultiSelect = false;

            ItemBLL itembll = new ItemBLL();
            dsPage = itembll.GetAllItems(searchCriteria);
            if (dsPage.Tables[0].Rows.Count == 0)
                view1 = null;
            else
            {
                TotalPage = GeneralFunctions.CalculateTotalPages(dsPage.Tables[0], PgSize);
                view1 = new DataView(GeneralFunctions.GetCurrentRecords(0, dsPage.Tables[0], PgSize, "Itemid"));
                CurrentPageIndex = 0;
                lblPageNo.Text = 1.ToString() + " / " + TotalPage.ToString();

            }
            source1.DataSource = view1;

            dvgItem.DataSource = source1;
            dvgItem.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            dvgItem.AllowUserToResizeColumns = false;
            dvgItem.AllowUserToAddRows = false;
            dvgItem.ReadOnly = true;
            dvgItem.Columns[1].HeaderCell.Style = style;
            dvgItem.Columns[2].HeaderCell.Style = style;
            //dvgItem.Columns[6].HeaderCell.Style = style;
            //dvgItem.Columns[7].HeaderCell.Style = style;
        }


        private void BuildDefaultSearch(SearchCriteria criteria)
        {
            criteria.ItemName = "";
            criteria.RestName = ddlRestName.Text;
            criteria.LocName = ddlLoc.Text;
            criteria.ItemGroupName = ddlGroup.Text;
            criteria.IntegerOption1 = ddlRestName.SelectedValue.ToInt();
            criteria.IntegerOption3 = ddlGroup.SelectedValue.ToInt();


            //criteria.RestName = "All Restaurants";
            //criteria.LocName = "All Locations";
            //criteria.ItemGroupName = "All Groups";
        }

        private void BuildSearchCriteria(SearchCriteria criteria)
        {
            criteria.ItemName = txtItemName.Text;
            //criteria.CardName = txtName.Text;
            criteria.RestName = ddlRestName.Text;
            criteria.LocName = ddlLoc.Text;
            criteria.ItemGroupName = ddlGroup.Text;
            criteria.IntegerOption1 = ddlRestName.SelectedValue.ToInt();
            criteria.IntegerOption3 = ddlGroup.SelectedValue.ToInt();
        }

        private void CheckFilterCondition()
        {
            SearchCriteria searchCriteria = new SearchCriteria();
            BuildSearchCriteria(searchCriteria);
            LoadItem(searchCriteria);
            dvgItem.Columns.Remove("Dimg");
            dvgItem.Columns.Remove("Eimg");
            //LoadRestaurants(searchCriteria);
            foreach (DataGridViewRow row in dvgItem.Rows)
            {
                row.Height = 25;
            }
        }

        private void SetWatermarkText()
        {

            WatermarkText.SetWatermark(txtItemName, "Item Name");
            //WatermarkText.SetWatermark(txtName, "User / Restaurant Name");
        }

        private void LoadLoc()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            //populate Locations
            SearchCriteria searchcriteria = new SearchCriteria();
            searchcriteria.QueryStat = "L";

            ds = new LocationBLL().GetAllLoc(searchcriteria);
            dt = ds.Tables[0];

            //DataRow nullrow = dt.NewRow();
            //nullrow["pk_LocID"] = 0;
            //nullrow["LocationName"] = "All Locations";
            //dt.Rows.Add(nullrow);
            if (dt.Rows.Count > 0)
            {
                ddlLoc.DataSource = dt;
                ddlLoc.DisplayMember = "LocationName";
                ddlLoc.ValueMember = "pk_LocID";
                ddlLoc.SelectedIndex = 0;
            }
        }

        private void LoadRest()
        {
            //DataRow row = new DataRow();
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            SearchCriteria searchcriteria = new SearchCriteria();
            searchcriteria.QueryStat = "L";
            searchcriteria.LocName = ddlLoc.Text;
            //if (ddlLoc.Text == "All Locations")
            //    searchcriteria.LocName = "";
            //else
            //    searchcriteria.LocName = Locname;
            ddlRestName.DataSource = null;

            ds = new RestaurantBLL().GetAllRestaurant(searchcriteria);
            dt = ds.Tables[0];
            if (dt.Rows.Count > 0)
            {
                //DataRow nullrow = dt.NewRow();
                //nullrow["pk_RestID"] = 0;
                //nullrow["RestName"] = "All Restaurants";
                //dt.Rows.Add(nullrow);

                ddlRestName.DataSource = dt;
                ddlRestName.DisplayMember = "RestName";
                ddlRestName.ValueMember = "pk_RestID";
                ddlRestName.SelectedIndex = 0;
            }
        }

        private void LoadItemGroup()
        {
            //DataRow row = new DataRow();
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            //FoodSmart.BLL.itemService.SearchCriteria searchcriteria = new FoodSmart.BLL.itemService.SearchCriteria();

            SearchCriteria searchcriteria = new SearchCriteria();
            searchcriteria.QueryStat = "L";
            searchcriteria.LocName = ddlLoc.Text;
            searchcriteria.RestName = ddlRestName.Text;
            //searchcriteria.ItemGroupName = ddlGroup.Text;

            //if (ddlLoc.Text == "All Locations")
            //    searchcriteria.LocName = "";
            //else
            //    searchcriteria.LocName = Locname;

            //if (ddlRestName.Text == "All Restaurants")
            //    searchcriteria.RestName = "";
            //else
            //    searchcriteria.RestName = RestName;
            ddlGroup.DataSource = null;

            ds = new ItemBLL().GetAllItemGroup(searchcriteria);
            dt = ds.Tables[0];

            DataRow nullrow = dt.NewRow();
            nullrow["pk_ItemGroupID"] = 0;
            nullrow["descr"] = "All Groups";
            dt.Rows.Add(nullrow);
            if (dt.Rows.Count > 0)
            {
                ddlGroup.DataSource = dt;
                ddlGroup.DisplayMember = "descr";
                ddlGroup.ValueMember = "pk_ItemGroupID";
                ddlGroup.SelectedValue = "0";
            }
        }
        private bool CheckForDeleted(int itemgrpID)
        {
            ItemBLL itemgrpbll = new ItemBLL();
            FoodSmart.Entity.ItemsEntity oitemGrpEntity = (FoodSmart.Entity.ItemsEntity)itemgrpbll.GetItemForEdit(itemgrpID);
            if (oitemGrpEntity.itemStatus)
            {
                return true;
            }
            else
                return false;
        }

        private bool CheckDeletePossible(int Cardid)
        {
            ItemBLL itemGroupbll = new ItemBLL();

            int stat = itemGroupbll.ChkItemGroupDelPossible(Cardid);
            if (stat == 1)
            {
                return true;
            }
            else
                return false;
        }

        #endregion


    }
}
