﻿namespace FoodSmartDesktop.Masters
{
    partial class ManageItemGroup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ManageItemGroup));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.lblExit = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lbltoday = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ddlLoc = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.ddlRestName = new System.Windows.Forms.ComboBox();
            this.txtGroupName = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblPageNo = new System.Windows.Forms.Label();
            this.picMoveFirst = new System.Windows.Forms.PictureBox();
            this.picMovePrev = new System.Windows.Forms.PictureBox();
            this.picMoveLast = new System.Windows.Forms.PictureBox();
            this.picMoveNext = new System.Windows.Forms.PictureBox();
            this.btnAddGroup = new System.Windows.Forms.Button();
            this.dvgItemGroup = new System.Windows.Forms.DataGridView();
            this.lblRight = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picMoveFirst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMovePrev)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMoveLast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMoveNext)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dvgItemGroup)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(199)))), ((int)(((byte)(233)))));
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.lblUserName);
            this.panel1.Controls.Add(this.lblExit);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(975, 52);
            this.panel1.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(439, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(193, 23);
            this.label2.TabIndex = 26;
            this.label2.Text = "Manage Item Group";
            // 
            // lblUserName
            // 
            this.lblUserName.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblUserName.Location = new System.Drawing.Point(712, 18);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(159, 22);
            this.lblUserName.TabIndex = 19;
            this.lblUserName.Text = "Welcome ";
            // 
            // lblExit
            // 
            this.lblExit.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExit.ForeColor = System.Drawing.Color.DarkRed;
            this.lblExit.Image = ((System.Drawing.Image)(resources.GetObject("lblExit.Image")));
            this.lblExit.Location = new System.Drawing.Point(902, 5);
            this.lblExit.Name = "lblExit";
            this.lblExit.Size = new System.Drawing.Size(63, 47);
            this.lblExit.TabIndex = 1;
            this.lblExit.Click += new System.EventHandler(this.lblExit_Click);
            this.lblExit.MouseLeave += new System.EventHandler(this.lblExit_MouseLeave);
            this.lblExit.MouseHover += new System.EventHandler(this.lblExit_MouseHover);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Monotype Corsiva", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
            this.label1.Location = new System.Drawing.Point(3, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(247, 28);
            this.label1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Black;
            this.panel2.Controls.Add(this.lbltoday);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Location = new System.Drawing.Point(0, 509);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(979, 33);
            this.panel2.TabIndex = 24;
            // 
            // lbltoday
            // 
            this.lbltoday.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltoday.ForeColor = System.Drawing.Color.Transparent;
            this.lbltoday.Location = new System.Drawing.Point(15, 7);
            this.lbltoday.Name = "lbltoday";
            this.lbltoday.Size = new System.Drawing.Size(159, 22);
            this.lbltoday.TabIndex = 20;
            this.lbltoday.Text = "Today";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Snow;
            this.label3.Location = new System.Drawing.Point(854, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 16);
            this.label3.TabIndex = 1;
            this.label3.Text = "Powered By Europia";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox1.Controls.Add(this.ddlLoc);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.ddlRestName);
            this.groupBox1.Controls.Add(this.txtGroupName);
            this.groupBox1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox1.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox1.Location = new System.Drawing.Point(1, 58);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(967, 49);
            this.groupBox1.TabIndex = 25;
            this.groupBox1.TabStop = false;
            // 
            // ddlLoc
            // 
            this.ddlLoc.FormattingEnabled = true;
            this.ddlLoc.Location = new System.Drawing.Point(299, 13);
            this.ddlLoc.Name = "ddlLoc";
            this.ddlLoc.Size = new System.Drawing.Size(226, 28);
            this.ddlLoc.TabIndex = 19;
            this.ddlLoc.SelectedIndexChanged += new System.EventHandler(this.ddlLoc_SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button1.BackgroundImage")));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.Control;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(906, 10);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(59, 40);
            this.button1.TabIndex = 18;
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = false;
            // 
            // ddlRestName
            // 
            this.ddlRestName.FormattingEnabled = true;
            this.ddlRestName.Location = new System.Drawing.Point(558, 13);
            this.ddlRestName.Name = "ddlRestName";
            this.ddlRestName.Size = new System.Drawing.Size(252, 28);
            this.ddlRestName.TabIndex = 1;
            this.ddlRestName.SelectedIndexChanged += new System.EventHandler(this.ddlRestName_SelectedIndexChanged);
            // 
            // txtGroupName
            // 
            this.txtGroupName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGroupName.Location = new System.Drawing.Point(6, 14);
            this.txtGroupName.Name = "txtGroupName";
            this.txtGroupName.Size = new System.Drawing.Size(262, 26);
            this.txtGroupName.TabIndex = 0;
            this.txtGroupName.TextChanged += new System.EventHandler(this.txtGroupName_TextChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblPageNo);
            this.groupBox2.Controls.Add(this.picMoveFirst);
            this.groupBox2.Controls.Add(this.picMovePrev);
            this.groupBox2.Controls.Add(this.picMoveLast);
            this.groupBox2.Controls.Add(this.picMoveNext);
            this.groupBox2.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(1, 108);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(273, 40);
            this.groupBox2.TabIndex = 26;
            this.groupBox2.TabStop = false;
            // 
            // lblPageNo
            // 
            this.lblPageNo.AutoSize = true;
            this.lblPageNo.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPageNo.Location = new System.Drawing.Point(74, 15);
            this.lblPageNo.Name = "lblPageNo";
            this.lblPageNo.Size = new System.Drawing.Size(102, 20);
            this.lblPageNo.TabIndex = 17;
            this.lblPageNo.Text = "Current Page : ";
            // 
            // picMoveFirst
            // 
            this.picMoveFirst.Image = ((System.Drawing.Image)(resources.GetObject("picMoveFirst.Image")));
            this.picMoveFirst.Location = new System.Drawing.Point(16, 15);
            this.picMoveFirst.Name = "picMoveFirst";
            this.picMoveFirst.Size = new System.Drawing.Size(20, 17);
            this.picMoveFirst.TabIndex = 13;
            this.picMoveFirst.TabStop = false;
            this.picMoveFirst.Click += new System.EventHandler(this.picMoveFirst_Click);
            // 
            // picMovePrev
            // 
            this.picMovePrev.Image = ((System.Drawing.Image)(resources.GetObject("picMovePrev.Image")));
            this.picMovePrev.Location = new System.Drawing.Point(42, 15);
            this.picMovePrev.Name = "picMovePrev";
            this.picMovePrev.Size = new System.Drawing.Size(20, 17);
            this.picMovePrev.TabIndex = 14;
            this.picMovePrev.TabStop = false;
            this.picMovePrev.Click += new System.EventHandler(this.picMovePrev_Click);
            // 
            // picMoveLast
            // 
            this.picMoveLast.Image = ((System.Drawing.Image)(resources.GetObject("picMoveLast.Image")));
            this.picMoveLast.Location = new System.Drawing.Point(214, 15);
            this.picMoveLast.Name = "picMoveLast";
            this.picMoveLast.Size = new System.Drawing.Size(20, 17);
            this.picMoveLast.TabIndex = 16;
            this.picMoveLast.TabStop = false;
            this.picMoveLast.Click += new System.EventHandler(this.picMoveLast_Click);
            // 
            // picMoveNext
            // 
            this.picMoveNext.Image = ((System.Drawing.Image)(resources.GetObject("picMoveNext.Image")));
            this.picMoveNext.Location = new System.Drawing.Point(188, 15);
            this.picMoveNext.Name = "picMoveNext";
            this.picMoveNext.Size = new System.Drawing.Size(20, 17);
            this.picMoveNext.TabIndex = 15;
            this.picMoveNext.TabStop = false;
            this.picMoveNext.Click += new System.EventHandler(this.picMoveNext_Click);
            // 
            // btnAddGroup
            // 
            this.btnAddGroup.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAddGroup.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAddGroup.BackgroundImage")));
            this.btnAddGroup.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAddGroup.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddGroup.ForeColor = System.Drawing.SystemColors.Control;
            this.btnAddGroup.Image = ((System.Drawing.Image)(resources.GetObject("btnAddGroup.Image")));
            this.btnAddGroup.Location = new System.Drawing.Point(810, 109);
            this.btnAddGroup.Name = "btnAddGroup";
            this.btnAddGroup.Size = new System.Drawing.Size(158, 46);
            this.btnAddGroup.TabIndex = 27;
            this.btnAddGroup.Text = "Add New Group";
            this.btnAddGroup.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAddGroup.UseVisualStyleBackColor = false;
            this.btnAddGroup.Click += new System.EventHandler(this.btnAddGroup_Click);
            this.btnAddGroup.MouseLeave += new System.EventHandler(this.btnAddGroup_MouseLeave);
            this.btnAddGroup.MouseHover += new System.EventHandler(this.btnAddGroup_MouseHover);
            // 
            // dvgItemGroup
            // 
            this.dvgItemGroup.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dvgItemGroup.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dvgItemGroup.Location = new System.Drawing.Point(7, 161);
            this.dvgItemGroup.Name = "dvgItemGroup";
            this.dvgItemGroup.RowHeadersVisible = false;
            this.dvgItemGroup.Size = new System.Drawing.Size(952, 342);
            this.dvgItemGroup.TabIndex = 28;
            this.dvgItemGroup.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dvgItemGroup_CellContentClick);
            // 
            // lblRight
            // 
            this.lblRight.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblRight.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblRight.Location = new System.Drawing.Point(970, 52);
            this.lblRight.Name = "lblRight";
            this.lblRight.Size = new System.Drawing.Size(5, 478);
            this.lblRight.TabIndex = 163;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label4.Location = new System.Drawing.Point(-2, 54);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(5, 478);
            this.label4.TabIndex = 164;
            // 
            // ManageItemGroup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(973, 543);
            this.ControlBox = false;
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblRight);
            this.Controls.Add(this.dvgItemGroup);
            this.Controls.Add(this.btnAddGroup);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "ManageItemGroup";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.ManageItemGroup_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picMoveFirst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMovePrev)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMoveLast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMoveNext)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dvgItemGroup)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label lblExit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lbltoday;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        protected internal System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox ddlRestName;
        private System.Windows.Forms.TextBox txtGroupName;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblPageNo;
        private System.Windows.Forms.PictureBox picMoveFirst;
        private System.Windows.Forms.PictureBox picMovePrev;
        private System.Windows.Forms.PictureBox picMoveLast;
        private System.Windows.Forms.PictureBox picMoveNext;
        protected internal System.Windows.Forms.Button btnAddGroup;
        private System.Windows.Forms.DataGridView dvgItemGroup;
        private System.Windows.Forms.Label lblRight;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox ddlLoc;
    }
}