﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FoodSmart.BLL;
using FoodSmart.Common;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using FoodSmart.Utilities.Cryptography;
using FoodSmart.Utilities.ResourceManager;
using System.Configuration;


namespace FoodSmartDesktop.Masters
{
    public partial class MainMenu : Form
    {
        public MainMenu()
        {
            InitializeComponent();
        }

        private void MainMenu_Load(object sender, EventArgs e)
        {
            this.Location = new Point(0, 0);
            this.Size = Screen.PrimaryScreen.WorkingArea.Size;
            string path = Application.StartupPath + "\\";

            if (GeneralFunctions.LoginInfo.UserRole == 2 || GeneralFunctions.LoginInfo.UserRole == 4)
            {
                btnUser.Enabled = false;
                //btnItemRates.Enabled = false;
                //btnItemGroup.Enabled = false;
                //btnItems.Enabled = false;
                btnCounters.Enabled = false;
                //btnCards.Enabled = false;

                btnTaxes.Visible = false;
                btnParkingRate.Visible = false;
                //btnCostCenter.Visible = false;
                btnInvTran.Visible = false;
                //btnVendor.Visible = false;
                btnSetting.Visible = false;

                btnUser.BackgroundImage = Image.FromFile(path + ConfigurationManager.AppSettings["imageFolder"] + "InactiveUser-btn.png");
                //btnCards.BackgroundImage = Image.FromFile(path + ConfigurationManager.AppSettings["imageFolder"] + "InactiveCards-btn.png");
                btnCounters.BackgroundImage = Image.FromFile(path + ConfigurationManager.AppSettings["imageFolder"] + "InactiveCounters-btn.png");
                //btnItems.BackgroundImage = Image.FromFile(path + ConfigurationManager.AppSettings["imageFolder"] + "InactiveItems-btn.png");
                //btnItemGroup.BackgroundImage = Image.FromFile(path + ConfigurationManager.AppSettings["imageFolder"] + "InactiveItemgroups-btn.png");
                //btnItemRates.BackgroundImage = Image.FromFile(path + ConfigurationManager.AppSettings["imageFolder"] + "InactiveItemrate-btn.png");


            }
            wlbWebApp.Text = "Reports Login ";

            lblDate.Text = "Today : " + GeneralFunctions.LoginInfo.CurrDate.ToString("dd-MMM-yy");
            lblWelcome.Text = "Welcome " + GeneralFunctions.LoginInfo.UserFullName;
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void lblChangePW_Click(object sender, EventArgs e)
        {
            Masters.ChangePassword item = new Masters.ChangePassword();
            item.ShowDialog();
        }

        #region "Button Click Events"
        private void btnUser_Click(object sender, EventArgs e)
        {
            //Masters.ManageUsers User = new Masters.ManageUsers();
            //User.ShowDialog();
            POS.ManageCashierTran tran = new POS.ManageCashierTran();
            tran.ShowDialog();
        }

        
        private void btnCounters_Click(object sender, EventArgs e)
        {
            //Inventory.ManageRecipe rest = new Inventory.ManageRecipe();
            //rest.ShowDialog();
        }

        //private void btnItemGroup_Click(object sender, EventArgs e)
        //{
        //    Masters.ManageItemGroup ig = new Masters.ManageItemGroup();
        //    ig.ShowDialog();
        //}

        //private void btnItems_Click(object sender, EventArgs e)
        //{
        //    Masters.ManageItems item = new Masters.ManageItems();
        //    item.ShowDialog();
        //}

        //private void btnItemRates_Click(object sender, EventArgs e)
        //{
        //    Masters.ManageItemRate itemrt = new Masters.ManageItemRate();
        //    itemrt.ShowDialog();
        //}

        private void btnTaxes_Click(object sender, EventArgs e)
        {
            //Inventory.ManageRMGroup rmGroup = new Inventory.ManageRMGroup();
            //rmGroup.ShowDialog();
            //Masters.ManageHolidays holiday = new Masters.ManageHolidays();
            //holiday.ShowDialog();
        }

        private void btnParkingRate_Click(object sender, EventArgs e)
        {
            //Inventory.ManageRM rmGroup = new Inventory.ManageRM();
            //rmGroup.ShowDialog();
        }

        private void btnVendor_Click(object sender, EventArgs e)
        {
            //Inventory.ManageVendor Vendor = new Inventory.ManageVendor();
            //Vendor.ShowDialog();
        }
        private void btnInvTran_Click(object sender, EventArgs e)
        {
            Inventory.ManageInvTran Tran = new Inventory.ManageInvTran();
            Tran.ShowDialog();
        }

        private void btnSetting_Click_1(object sender, EventArgs e)
        {
            //Masters.manageParameters Param = new Masters.manageParameters();
            //Param.ShowDialog();
        }

        private void btnCostCenter_Click(object sender, EventArgs e)
        {
            //Inventory.ManageCostCenter cc = new Inventory.ManageCostCenter();
            //cc.ShowDialog();
        }

        private void wlbWebApp_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://192.168.50.55/Login.aspx");
        }

        
        #endregion

        #region MouseHover
        private void lblChangePW_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnCards_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnUser_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnCounters_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnItemGroup_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnItems_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnItemRates_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnTaxes_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnParkingRate_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnCostCenter_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void lblExit_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnInvTran_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnVendor_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnSetting_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        #endregion

        #region MouseLeave

        private void lblExit_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnCards_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnUser_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnCounters_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnItemGroup_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnItems_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnItemRates_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnTaxes_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnParkingRate_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnCostCenter_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnInvTran_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnVendor_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnSetting_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }
        #endregion

       

       
    }
}
