﻿namespace FoodSmartDesktop.Masters
{
    partial class Dashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Dashboard));
            this.btnItemGroup = new System.Windows.Forms.Button();
            this.btnUser = new System.Windows.Forms.Button();
            this.btnItems = new System.Windows.Forms.Button();
            this.btnItemRates = new System.Windows.Forms.Button();
            this.btnSetting = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblExit = new System.Windows.Forms.Label();
            this.lblChangePW = new System.Windows.Forms.Label();
            this.lblWelcome = new System.Windows.Forms.Label();
            this.wlbWebApp = new System.Windows.Forms.LinkLabel();
            this.btnCounters = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnItemGroup
            // 
            this.btnItemGroup.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnItemGroup.BackgroundImage")));
            this.btnItemGroup.Location = new System.Drawing.Point(82, 237);
            this.btnItemGroup.Name = "btnItemGroup";
            this.btnItemGroup.Size = new System.Drawing.Size(130, 130);
            this.btnItemGroup.TabIndex = 2;
            this.btnItemGroup.UseVisualStyleBackColor = true;
            this.btnItemGroup.Click += new System.EventHandler(this.btnItemGroup_Click);
            this.btnItemGroup.MouseLeave += new System.EventHandler(this.btnItemGroup_MouseLeave);
            this.btnItemGroup.MouseHover += new System.EventHandler(this.btnItemGroup_MouseHover);
            // 
            // btnUser
            // 
            this.btnUser.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnUser.BackgroundImage")));
            this.btnUser.Location = new System.Drawing.Point(82, 86);
            this.btnUser.Name = "btnUser";
            this.btnUser.Size = new System.Drawing.Size(130, 130);
            this.btnUser.TabIndex = 3;
            this.btnUser.UseVisualStyleBackColor = true;
            this.btnUser.Click += new System.EventHandler(this.btnUser_Click);
            this.btnUser.MouseLeave += new System.EventHandler(this.btnUser_MouseLeave);
            this.btnUser.MouseHover += new System.EventHandler(this.btnUser_MouseHover);
            // 
            // btnItems
            // 
            this.btnItems.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnItems.BackgroundImage")));
            this.btnItems.Location = new System.Drawing.Point(433, 237);
            this.btnItems.Name = "btnItems";
            this.btnItems.Size = new System.Drawing.Size(130, 130);
            this.btnItems.TabIndex = 4;
            this.btnItems.UseVisualStyleBackColor = true;
            this.btnItems.Click += new System.EventHandler(this.btnItems_Click);
            this.btnItems.MouseLeave += new System.EventHandler(this.btnItems_MouseLeave);
            this.btnItems.MouseHover += new System.EventHandler(this.btnItems_MouseHover);
            // 
            // btnItemRates
            // 
            this.btnItemRates.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnItemRates.BackgroundImage")));
            this.btnItemRates.Location = new System.Drawing.Point(786, 237);
            this.btnItemRates.Name = "btnItemRates";
            this.btnItemRates.Size = new System.Drawing.Size(130, 130);
            this.btnItemRates.TabIndex = 5;
            this.btnItemRates.UseVisualStyleBackColor = true;
            this.btnItemRates.Click += new System.EventHandler(this.btnItemRates_Click);
            this.btnItemRates.MouseLeave += new System.EventHandler(this.btnItemRates_MouseLeave);
            this.btnItemRates.MouseHover += new System.EventHandler(this.btnItemRates_MouseHover);
            // 
            // btnSetting
            // 
            this.btnSetting.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSetting.BackgroundImage")));
            this.btnSetting.Location = new System.Drawing.Point(786, 86);
            this.btnSetting.Name = "btnSetting";
            this.btnSetting.Size = new System.Drawing.Size(130, 130);
            this.btnSetting.TabIndex = 8;
            this.btnSetting.UseVisualStyleBackColor = true;
            this.btnSetting.Click += new System.EventHandler(this.btnSetting_Click);
            this.btnSetting.MouseLeave += new System.EventHandler(this.btnSetting_MouseLeave);
            this.btnSetting.MouseHover += new System.EventHandler(this.btnSetting_MouseHover);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(0, 527);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(1020, 25);
            this.label1.TabIndex = 9;
            this.label1.Text = "Powered By Europia";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Image = ((System.Drawing.Image)(resources.GetObject("label2.Image")));
            this.label2.Location = new System.Drawing.Point(-3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(243, 53);
            this.label2.TabIndex = 10;
            // 
            // lblDate
            // 
            this.lblDate.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDate.ForeColor = System.Drawing.Color.White;
            this.lblDate.Location = new System.Drawing.Point(766, 57);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(174, 22);
            this.lblDate.TabIndex = 11;
            this.lblDate.Text = "Today";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Image = ((System.Drawing.Image)(resources.GetObject("label3.Image")));
            this.label3.Location = new System.Drawing.Point(1119, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 12;
            // 
            // lblExit
            // 
            this.lblExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblExit.Image = ((System.Drawing.Image)(resources.GetObject("lblExit.Image")));
            this.lblExit.Location = new System.Drawing.Point(940, 1);
            this.lblExit.Name = "lblExit";
            this.lblExit.Size = new System.Drawing.Size(77, 52);
            this.lblExit.TabIndex = 13;
            this.lblExit.Click += new System.EventHandler(this.lblExit_Click);
            this.lblExit.MouseLeave += new System.EventHandler(this.lblExit_MouseLeave);
            this.lblExit.MouseHover += new System.EventHandler(this.lblExit_MouseHover);
            // 
            // lblChangePW
            // 
            this.lblChangePW.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChangePW.ForeColor = System.Drawing.Color.White;
            this.lblChangePW.Location = new System.Drawing.Point(517, 57);
            this.lblChangePW.Name = "lblChangePW";
            this.lblChangePW.Size = new System.Drawing.Size(159, 22);
            this.lblChangePW.TabIndex = 14;
            this.lblChangePW.Text = "Change Password";
            this.lblChangePW.Click += new System.EventHandler(this.lblChangePW_Click);
            this.lblChangePW.MouseLeave += new System.EventHandler(this.lblChangePW_MouseLeave);
            this.lblChangePW.MouseHover += new System.EventHandler(this.lblChangePW_MouseHover);
            // 
            // lblWelcome
            // 
            this.lblWelcome.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWelcome.ForeColor = System.Drawing.Color.White;
            this.lblWelcome.Location = new System.Drawing.Point(53, 57);
            this.lblWelcome.Name = "lblWelcome";
            this.lblWelcome.Size = new System.Drawing.Size(159, 22);
            this.lblWelcome.TabIndex = 15;
            this.lblWelcome.Text = "Welcome ";
            // 
            // wlbWebApp
            // 
            this.wlbWebApp.AutoSize = true;
            this.wlbWebApp.BackColor = System.Drawing.Color.Transparent;
            this.wlbWebApp.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wlbWebApp.LinkColor = System.Drawing.Color.White;
            this.wlbWebApp.Location = new System.Drawing.Point(279, 57);
            this.wlbWebApp.Name = "wlbWebApp";
            this.wlbWebApp.Size = new System.Drawing.Size(125, 24);
            this.wlbWebApp.TabIndex = 16;
            this.wlbWebApp.TabStop = true;
            this.wlbWebApp.Text = "Link to Report";
            this.wlbWebApp.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.wlbWebApp_LinkClicked);
            // 
            // btnCounters
            // 
            this.btnCounters.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCounters.BackgroundImage")));
            this.btnCounters.Location = new System.Drawing.Point(433, 86);
            this.btnCounters.Name = "btnCounters";
            this.btnCounters.Size = new System.Drawing.Size(130, 130);
            this.btnCounters.TabIndex = 1;
            this.btnCounters.UseVisualStyleBackColor = true;
            this.btnCounters.Click += new System.EventHandler(this.btnCounters_Click);
            this.btnCounters.MouseLeave += new System.EventHandler(this.btnCounters_MouseLeave);
            this.btnCounters.MouseHover += new System.EventHandler(this.btnCounters_MouseHover);
            // 
            // Dashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::FoodSmartDesktop.Properties.Resources.dashboard_bg;
            this.ClientSize = new System.Drawing.Size(1020, 552);
            this.ControlBox = false;
            this.Controls.Add(this.wlbWebApp);
            this.Controls.Add(this.lblWelcome);
            this.Controls.Add(this.lblChangePW);
            this.Controls.Add(this.lblExit);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblDate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSetting);
            this.Controls.Add(this.btnItemRates);
            this.Controls.Add(this.btnItems);
            this.Controls.Add(this.btnUser);
            this.Controls.Add(this.btnItemGroup);
            this.Controls.Add(this.btnCounters);
            this.Name = "Dashboard";
            this.Load += new System.EventHandler(this.Dashboard_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnItemGroup;
        private System.Windows.Forms.Button btnUser;
        private System.Windows.Forms.Button btnItems;
        private System.Windows.Forms.Button btnItemRates;
        private System.Windows.Forms.Button btnSetting;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblExit;
        private System.Windows.Forms.Label lblChangePW;
        private System.Windows.Forms.Label lblWelcome;
        private System.Windows.Forms.LinkLabel wlbWebApp;
        private System.Windows.Forms.Button btnCounters;
    }
}