﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FoodSmart.BLL;
using FoodSmart.Common;
using System.Configuration;
using System.IO;
using System.Drawing.Drawing2D;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using FoodSmart.Utilities.Cryptography;

namespace FoodSmartDesktop.Masters
{
    public partial class AddEditUser : Form
    {
        string stat;
        int LID;
        int CardID;


        public AddEditUser(int ID)
        {
            InitializeComponent();

            if (ID == 0)
            {
                stat = "A";
                LID = 0;
            }
            else
            {
                stat = "E";
                LID = ID;
            }
        }
        #region "Public Methods"
        private void AddEditUser_Load(object sender, EventArgs e)
        {
            //txtCardNoInside.PasswordChar = '*';
            //txtCardNoOutside.Enabled = false;
            this.ddlRole.SelectedIndexChanged -= new System.EventHandler(this.ddlRole_SelectedIndexChanged);
            LoadDDL();
            Initialize();
            if (LID != 0)
                FillUser(LID);
            lblErrorLoginID.Visible = false;
            lblErrRest.Visible = false;
            this.ddlRole.SelectedIndexChanged += new System.EventHandler(this.ddlRole_SelectedIndexChanged);
            lbltoday.Text += " : " + GeneralFunctions.LoginInfo.CurrDate.ToString("dd-MM-yyyy");
            lblUserName.Text += GeneralFunctions.LoginInfo.UserFullName;
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            IUser usr = new UserEntity();
            if (ValidateEntry(Convert.ToInt32(ddlRole.SelectedValue)))
            {
                usr.UserFullName = txtUserName.Text;
                usr.LoginID = txtLoginID.Text;
                //usr.Password = Encryption.Encrypt(Constants.DEFAULT_PASSWORD);
                usr.Password = Encryption.Encrypt(txtPw.Text);
                usr.RoleID = Convert.ToInt32(ddlRole.SelectedValue);
                //usr.CardID = CardID;
                //usr.LocID = ddlLoc.SelectedValue.ToInt();
                usr.LocID = Convert.ToInt32(ddlLoc.SelectedValue);
                usr.RestID = Convert.ToInt32(ddlRest.SelectedValue);
                //usr.CardNoOutside = txtCardNoOutside.Text;
                usr.EmailId = txtEmailID.Text;
                usr.UserLocked = chkLock.Checked;

                usr.Id = LID;
                if (LID == 0)
                {
                    chkLock.Enabled = false;
                    chkLock.Checked = false;
                }
                else
                    if (chkLock.Checked)
                        usr.IsActive = false;
                    else
                        usr.IsActive = true;

                UserBLL userbll = new UserBLL();
                int Status = userbll.AddEditUser(usr, stat, GeneralFunctions.LoginInfo.UserID);

                if (Status == 0)
                {
                    MessageBox.Show("Error!! Record Not Saved", "Save Record", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    if (stat == "A")
                    {
                        //UpdateCard();
                        Initialize();
                        txtUserName.Focus();
                    }
                    else
                    {
                        this.Close();
                    }
                }
            }
        }
       
        private void ddlRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtPw.Enabled = false;
            //txtLoginID.Enabled = false;
            txtConfPw.Enabled = false;
            ddlRest.SelectedIndex = 0;
            ddlRest.Enabled = false;
            ddlLoc.SelectedIndex = 0;
            ddlLoc.Enabled = false;
            txtPw.Enabled = true;
            txtConfPw.Enabled = true;
             if (ddlRole.Text == "RESTAURANTS")
             {
                 ddlRest.Enabled = true;
                 ddlLoc.SelectedIndex = 0;
                 //ddlLoc.Enabled = true;
                 ddlRest.Focus();
             }


            //if (ddlRole.Text == "ADMIN" || ddlRole.Text == "MANAGEMENT" || ddlRole.Text == "INVENTORY")
            //{
            //    txtPw.Enabled = true;
            //    txtConfPw.Enabled = true;
            //}
            //else
            //{
            //    if (ddlRole.Text == "RESTAURANTS")
            //    {
            //        lblInternalNoMessage.Text = "Please Swipe Restaurant User Card";
            //        //Cont = MessageBox.Show("Please put Restaurant User Card on Reader", "Card Reader", MessageBoxButtons.OKCancel, MessageBoxIcon.Hand).ToInt();
            //        txtLoginID.Enabled = false;
            //        ddlRest.SelectedIndex = 0;
            //        ddlRest.Enabled = true;
            //        ddlLoc.SelectedIndex = 0;
            //        ddlLoc.Enabled = true;
            //    }
            //    else if (ddlRole.Text == "CASHIER")
            //    {
            //        lblInternalNoMessage.Text = "Please Swipe Cashier Card";
            //        //Cont = MessageBox.Show("Please put Cashier Card on Reader", "Reader Error", MessageBoxButtons.OKCancel, MessageBoxIcon.Hand).ToInt();
            //        ddlLoc.SelectedIndex = 0;
            //        ddlLoc.Enabled = true;
            //        //txtLoginID.Enabled = true;
            //        txtPw.Enabled = true;
            //        txtConfPw.Enabled = true;
            //    }
            //}
        }
        #endregion

        #region "Private Methods"
        private void LoadDDL()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.QueryStat = "L";

            //populate role
            ds = new UserBLL().GetAllRoles();
            dt = ds.Tables[0];
            ddlRole.DataSource = dt;
            ddlRole.DisplayMember = "RoleName";
            ddlRole.ValueMember = "pk_RoleID";

            //populate Restaurants
            ds = new RestaurantBLL().GetAllRestaurant(searchCriteria);
            dt = ds.Tables[0];
            ddlRest.DataSource = dt;
            ddlRest.DisplayMember = "RestName";
            ddlRest.ValueMember = "pk_RestID";

            DataRow dr = dt.NewRow();
            dr["RestName"] = "None";
            dr["pk_RestID"] = "0";

            dt.Rows.InsertAt(dr, 0);

            ds = new LocationBLL().GetAllLoc(searchCriteria);
            dt = ds.Tables[0];
            ddlLoc.DataSource = dt;
            ddlLoc.DisplayMember = "LocationName";
            ddlLoc.ValueMember = "pk_LocID";
        }

        private void FillUser(int UserId)
        {
            UserBLL ouserbll = new UserBLL();
            
            UserEntity oUserEntity = (UserEntity)ouserbll.GetUserForEdit(UserId, "M", 1);
            txtUserName.Text = oUserEntity.UserFullName;
            txtLoginID.Text = oUserEntity.LoginID;
            ddlRole.SelectedValue = oUserEntity.RoleID;
            ddlLoc.SelectedValue = oUserEntity.LocID;
            ddlRest.SelectedValue = oUserEntity.RestID;
            txtEmailID.Text = oUserEntity.EmailId;
            txtPw.Text = oUserEntity.Password;
            txtConfPw.Text = oUserEntity.Password;
            //txtCardNoOutside.Text = oUserEntity.CardNoOutside;
            //txtPw.Text = oUserEntity.Password;
            txtConfPw.Text = oUserEntity.Password;
            //CardID = oUserEntity.CardID;
            ddlLoc.SelectedValue = oUserEntity.LocID;

            txtPw.Enabled = true;
            txtConfPw.Enabled = true;
            txtLoginID.Enabled = false;
            chkLock.Enabled = true;

            if (oUserEntity.RoleID == 3 || oUserEntity.RoleID == 4)
            {
                txtPw.Enabled = false;
                txtConfPw.Enabled = false;
                //txtLoginID.Enabled = false;
            }
        }

        private void Initialize()
        {
            txtUserName.Text = "";
            txtLoginID.Text = "";
            //txtCardNoInside.Text = "";
            ddlRole.SelectedIndex = 0;
            ddlRest.SelectedIndex = 0;
            ddlLoc.SelectedIndex = 0;
            //txtCardNoOutside.Text = "";
            txtPw.Text = "";
            txtEmailID.Text = "";
            txtConfPw.Text = "";
            ddlRest.Enabled = false;
            ddlLoc.Enabled = false;
            chkLock.Enabled = false;
            lblInternalNoMessage.Visible = false;
            //CardID = 0;
        }

        private bool ValidateEntry(int roleid)
        {
            bool RetVal = true;
            lblErrRest.Visible = false;

            if (string.IsNullOrEmpty(txtLoginID.Text))
            {
                txtLoginID.BackColor = Color.Red;
                RetVal = false;
            }

            if (string.IsNullOrEmpty(txtUserName.Text))
            {
                txtUserName.BackColor = Color.Red;
                RetVal = false;
            }

            if (string.IsNullOrEmpty(txtPw.Text))
            {
                txtPw.BackColor = Color.Red;
                RetVal = false;
            }

            if (string.IsNullOrEmpty(txtConfPw.Text))
            {
                txtConfPw.BackColor = Color.Red;
                RetVal = false;
            }

            if (txtPw.Text != txtConfPw.Text)
            {
                txtPw.BackColor = Color.Red;
                txtConfPw.BackColor = Color.Red;
                RetVal = false;
            }
            
            if (roleid == 3 && (ddlRest.SelectedIndex == 0 || ddlRest.SelectedIndex == -1))
            {
                RetVal = false;
                lblErrRest.Text = "Restaurant Selection is compulsory";
                lblErrRest.Visible = true;
            }

            return RetVal;
        }

        #endregion

        # region Error Color change
        private void txtUserName_TextChanged(object sender, EventArgs e)
        {
            txtUserName.BackColor = Color.White;
        }

        private void txtLoginID_TextChanged(object sender, EventArgs e)
        {
            txtLoginID.BackColor = Color.White;
        }

        private void txtPw_TextChanged(object sender, EventArgs e)
        {
            txtPw.BackColor = Color.White;
        }

        private void txtConfPw_TextChanged(object sender, EventArgs e)
        {
            txtConfPw.BackColor = Color.White;
        }

        //private void txtCardNoOutside_TextChanged(object sender, EventArgs e)
        //{
        //    txtEmailID.BackColor = Color.White;
        //}
        #endregion

        #region "Cursor Type Change"
        private void lblExit_MouseHover_1(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void lblExit_MouseLeave_1(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnSave_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnSave_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }
        #endregion

        //private void CheckCard(int roleID)
        //{
        //    string CardNo = GeneralFunctions.GetCardNo();
        //    CardNo = CardNo.Replace(" ", "");
        //    if (GeneralFunctions.ConnectCard() == 0)
        //    {
        //        MessageBox.Show("Card Reader Not Connected", "Reader Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
        //        ddlRole.SelectedIndex = -1;
        //        return;
        //    }
        //    if (string.IsNullOrEmpty(CardNo))
        //    {
        //        MessageBox.Show("Card Not present on the Reader", "Reader Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
        //        ddlRole.SelectedIndex = -1;
        //        return;
        //    }
        //    card = new CardBLL().GetCardData(CardNo, "I");
        //    if (card.CardNoInside != null)
        //    {
        //        if (roleID == 2 && card.CardType != "C")
        //        {
        //            ddlRole.SelectedIndex = -1;
        //            MessageBox.Show("Not a Cashier Card", "Type Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        }

        //        if (roleID == 3 && card.CardType != "U")
        //        {
        //            ddlRole.SelectedIndex = -1;
        //            MessageBox.Show("Not a Restaurant Card", "Type Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        }

        //        if (roleID == 4 && card.CardType != "P")
        //        {
        //            ddlRole.SelectedIndex = -1;
        //            MessageBox.Show("Not a Parking User Card", "Type Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        }

        //        if (card.UserID != null && card.UserID != 0)
        //        {
        //            ddlRole.SelectedIndex = -1;
        //            MessageBox.Show("Card Already Alloted to User " + card.UserName, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        }
        //    }
        //    else
        //    {
        //        ddlRole.SelectedIndex = -1;
        //        MessageBox.Show("Card Not registered.", "Registration Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }
        //    if (card != null)
        //    {
        //        txtCardNoOutside.Text = card.CardNoOutside;
        //        CardID = card.CardID;

        //    }
        //}

        private void txtLoginID_Validating(object sender, CancelEventArgs e)
        {
            bool loginExists = new UserBLL().CheckDuplicateLogin(txtLoginID.Text);
            if (loginExists)
            {
                lblErrorLoginID.Visible = true;
                lblErrorLoginID.Text = "Duplicate Login ID";
                e.Cancel = true;
            }
            else
            {
                lblErrorLoginID.Visible = false;
                e.Cancel = false;
            }
        }

        //private void txtCardNoInside_TextChanged(object sender, EventArgs e)
        //{
        //    if (txtCardNoInside.Text.Trim().Length != 12)
        //    {
        //        lblInternalNoMessage.Text = "Invalid Card No. Please Swipe Card Again";
        //        lblInternalNoMessage.Visible = true;
        //        return;
        //    }

        //    if (txtCardNoInside.Text.Substring(0, 1) != "%" && txtCardNoInside.Text.Substring(11, 1) != "?")
        //    {
        //        lblInternalNoMessage.Text = "Invalid Card No. Please Swipe Card Again";
        //        lblInternalNoMessage.Visible = true;
        //        return;
        //    }
        //    lblInternalNoMessage.Visible = false;

        //    txtCardNoInside.Text = txtCardNoInside.Text.Substring(1, 5);
        //    lblInternalNoMessage.Visible = false;

        //    string CardNo = txtCardNoInside.Text;
        //    int roleID = ddlRole.SelectedValue.ToInt();

        //    card = new CardBLL().GetCardData(CardNo, "I");
        //    if (card.CardNoInside != null)
        //    {
        //        if (roleID == 2 && card.CardType != "C")
        //        {
        //            ddlRole.SelectedIndex = -1;
        //            MessageBox.Show("Not a Cashier Card", "Type Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        }

        //        if (roleID == 3 && card.CardType != "U")
        //        {
        //            ddlRole.SelectedIndex = -1;
        //            MessageBox.Show("Not a Restaurant Card", "Type Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        }

        //        if (roleID == 4 && card.CardType != "P")
        //        {
        //            ddlRole.SelectedIndex = -1;
        //            MessageBox.Show("Not a Parking User Card", "Type Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        }

        //        if (card.UserID != null && card.UserID != 0)
        //        {
        //            ddlRole.SelectedIndex = -1;
        //            MessageBox.Show("Card Already Alloted to User " + card.UserName, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        }
        //    }
        //    else
        //    {
        //        ddlRole.SelectedIndex = -1;
        //        MessageBox.Show("Card Not registered.", "Registration Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    }
        //    if (card != null)
        //    {
        //        txtCardNoOutside.Text = card.CardNoOutside;
        //        CardID = card.CardID;

        //    }
        //}
    }
}
