﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using FoodSmart.DAL;
using FoodSmart.BLL;
using FoodSmart.Entity;
using FoodSmart.GeneralUtilities;
using FoodSmart.Utilities;
using FoodSmart.Common;

namespace FoodSmartDesktop.Masters
{
    public partial class ManageUsers : Form
    {
        BindingSource source1 = new BindingSource();
        private int PgSize = 12;
        private int CurrentPageIndex = 1;
        private int TotalPage = 0;
        private DataSet dsPage;

        public ManageUsers()
        {
            InitializeComponent();
        }

        #region Public Method
        private void ManageUsers_Load(object sender, EventArgs e)
        {
            dvgUser.ColumnCount = 7;
            this.ddlRole.SelectedIndexChanged -= new System.EventHandler(this.ddlRole_SelectedIndexChanged);
            SetWatermarkText();

            SearchCriteria searchCriteria = new SearchCriteria();
            BuildDefaultSearch(searchCriteria);

            LoadUsers(searchCriteria);
            LoadDDL();
            this.ddlRole.SelectedIndexChanged += new System.EventHandler(this.ddlRole_SelectedIndexChanged);
            foreach (DataGridViewRow row in dvgUser.Rows)
            {
                row.Height = 25;
            }
            lbltoday.Text += " : " + GeneralFunctions.LoginInfo.CurrDate.ToString("dd-MM-yyyy");
            lblUserName.Text += GeneralFunctions.LoginInfo.UserFullName;
            ddlRole.Focus();
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAddUser_Click(object sender, EventArgs e)
        {
            this.Hide();
            Masters.AddEditUser User = new Masters.AddEditUser(0);
            if (User.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {

            }
            else
            {
                this.Show();
                //dvgItem.Columns.Remove("Dimg");
                dvgUser.Columns.Remove("Eimg");
                dvgUser.Columns.Remove("Dimg");
                SearchCriteria searchCriteria = new SearchCriteria();
                BuildDefaultSearch(searchCriteria);
                LoadUsers(searchCriteria);
                foreach (DataGridViewRow row in dvgUser.Rows)
                {
                    row.Height = 25;
                }
            }

        }

        private void dvgUser_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int Reply;
            int i;
            if (dvgUser.RowCount == 0)
                return;
            else
                i = dvgUser.CurrentRow.Index;

            if (dvgUser.Columns[e.ColumnIndex].Name == "Dimg")
            {
                //if (!CheckForDeleted(Convert.ToInt32(dvgUser.CurrentRow.Cells[0].Value)))
                //{
                //    MessageBox.Show("Record Already Deleted.", "Delete Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                //    return;
                //}

                Reply = Convert.ToInt32(MessageBox.Show("Want to Delete this Record?", "Delete Record", MessageBoxButtons.OKCancel, MessageBoxIcon.Question));
                if (Reply == 1)
                {
                    Reply = Convert.ToInt32(dvgUser.CurrentRow.Cells[0].Value);
                    UserBLL.DeleteUser(Reply, GeneralFunctions.LoginInfo.UserID);

                    //UserBLL usrBLL = new UserBLL();
                    //usrBLL.DeleteUser(Reply, GeneralFunctions.LoginInfo.UserID);

                    dvgUser.Columns.Remove("Dimg");
                    dvgUser.Columns.Remove("Eimg");
                    SearchCriteria searchCriteria = new SearchCriteria();
                    BuildDefaultSearch(searchCriteria);
                    LoadUsers(searchCriteria);
                    foreach (DataGridViewRow row in dvgUser.Rows)
                    {
                        row.Height = 25;
                    }
                }
            }

            if (dvgUser.Columns[e.ColumnIndex].Name == "Eimg")
            {
                Masters.AddEditUser frm1 = new Masters.AddEditUser(Convert.ToInt32(dvgUser.CurrentRow.Cells[0].Value));
                this.Hide();

                if (frm1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    this.Hide();
                }
                else
                {
                    this.Show();
                    dvgUser.Columns.Remove("Dimg");
                    dvgUser.Columns.Remove("Eimg");
                    SearchCriteria searchCriteria = new SearchCriteria();
                    BuildDefaultSearch(searchCriteria);
                    LoadUsers(searchCriteria);
                    foreach (DataGridViewRow row in dvgUser.Rows)
                    {
                        row.Height = 25;
                    }
                }
            }
        }

        private void txtUser_TextChanged(object sender, EventArgs e)
        {
            CheckFilterCondition();
        }

        private void ddlRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckFilterCondition();

        }

        private void txtLogin_TextChanged(object sender, EventArgs e)
        {
            CheckFilterCondition();
        }

        #endregion

        #region "Private Methods"
        private void LoadUsers(SearchCriteria searchCriteria)
        {
            string path = Application.StartupPath + "\\";
            DataView view1;

            DataGridViewCellStyle style = new DataGridViewCellStyle();
            dvgUser.EnableHeadersVisualStyles = false;
            style.ForeColor = Color.Blue;

            dvgUser.AutoGenerateColumns = false;

            dvgUser.Columns[0].Name = "pk_UserID";
            dvgUser.Columns[1].Name = "User Name";
            dvgUser.Columns[2].Name = "Login ID";
            dvgUser.Columns[3].Name = "User Role";
            dvgUser.Columns[4].Name = "Card No";
            dvgUser.Columns[5].Name = "Restaurants";
            dvgUser.Columns[6].Name = "RoleID";

            dvgUser.Columns[1].HeaderText = "User Name";
            dvgUser.Columns[2].HeaderText = "Login ID";
            dvgUser.Columns[3].HeaderText = "User Role";
            dvgUser.Columns[4].HeaderText = "Card No";
            dvgUser.Columns[5].HeaderText = "Restaurants";

            dvgUser.Columns[0].DataPropertyName = "pk_UserID";
            dvgUser.Columns[1].DataPropertyName = "UserName";
            dvgUser.Columns[2].DataPropertyName = "LoginID";
            dvgUser.Columns[3].DataPropertyName = "RoleName";
            dvgUser.Columns[4].DataPropertyName = "CardNoOutside";
            dvgUser.Columns[5].DataPropertyName = "RestName";
            dvgUser.Columns[6].DataPropertyName = "RoleID";

            dvgUser.Columns[1].Width = 200;
            dvgUser.Columns[2].Width = 100;
            dvgUser.Columns[3].Width = 200;
            dvgUser.Columns[4].Width = 100;
            dvgUser.Columns[5].Width = 250;

            //dvgEmp.Columns[7].Width = 100;

            dvgUser.Columns[1].SortMode = DataGridViewColumnSortMode.Automatic;
            dvgUser.Columns[2].SortMode = DataGridViewColumnSortMode.Automatic;
            dvgUser.Columns[3].SortMode = DataGridViewColumnSortMode.Automatic;


            dvgUser.Columns[0].Visible = false;
            dvgUser.Columns[6].Visible = false;

            DataGridViewImageColumn Editimg = new DataGridViewImageColumn();
            Image Editimage = Image.FromFile(path + ConfigurationManager.AppSettings["imageFolder"] + "edit-icon.png");
            Editimg.Image = Editimage;
            dvgUser.Columns.Add(Editimg);
            Editimg.HeaderText = "Edit";
            Editimg.Name = "Eimg";
            Editimg.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            Editimg.Width = 40;

            DataGridViewImageColumn Delimg = new DataGridViewImageColumn();
            Image Delimage = Image.FromFile(path + ConfigurationManager.AppSettings["imageFolder"] + "delete-icon.png");
            Delimg.Image = Delimage;
            dvgUser.Columns.Add(Delimg);
            Delimg.HeaderText = "Del";
            Delimg.Name = "Dimg";
            Delimg.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            Delimg.Width = 40;

            dvgUser.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dvgUser.MultiSelect = false;
            //dvgUser.Dock = DockStyle.Bottom;

            //BuildDefaultSearch(searchCriteria);

            ////SearchCriteria searchCriteria = (SearchCriteria)null;
            //DataSet dsEmp = new DataSet();
            //dsEmp = UserBLL.GetAllUsers(searchCriteria);
            //int TotalPage = GeneralFunctions.CalculateTotalPages(dsEmp.Tables[0], PgSize);
            //DataView view1 = new DataView(GeneralFunctions.GetCurrentRecords(1, dsEmp.Tables[0], PgSize, "pk_UserID"));
            UserBLL userbll = new UserBLL();
            dsPage = userbll.GetAllUsers(searchCriteria);
            if (dsPage.Tables[0].Rows.Count == 0)
                view1 = null;
            else
            {
                TotalPage = GeneralFunctions.CalculateTotalPages(dsPage.Tables[0], PgSize);
                view1 = new DataView(GeneralFunctions.GetCurrentRecords(0, dsPage.Tables[0], PgSize, "pk_UserID"));
                CurrentPageIndex = 0;
                lblPageNo.Text = 1.ToString() + " / " + TotalPage.ToString();

            }
            source1.DataSource = view1;

            dvgUser.DataSource = source1;
            dvgUser.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            dvgUser.AllowUserToResizeColumns = false;
            dvgUser.AllowUserToAddRows = false;
            dvgUser.ReadOnly = true;
            dvgUser.Columns[1].HeaderCell.Style = style;
            dvgUser.Columns[2].HeaderCell.Style = style;
            dvgUser.Columns[3].HeaderCell.Style = style;
        }

        private void BuildDefaultSearch(SearchCriteria criteria)
        {
            criteria.UserName = "";
            //criteria.CardNo = "";
        }

        private void BuildSearchCriteria(SearchCriteria criteria)
        {
            //SearchCriteria newcriteria = new SearchCriteria();
            criteria.UserName = txtUser.Text;
            criteria.LoginID = txtLogin.Text;
            criteria.RoleName = ddlRole.Text;
        }



        private void CheckFilterCondition()
        {
            SearchCriteria searchCriteria = new SearchCriteria();
            BuildSearchCriteria(searchCriteria);
            dvgUser.Columns.Remove("Dimg");
            dvgUser.Columns.Remove("Eimg");
            LoadUsers(searchCriteria);
            foreach (DataGridViewRow row in dvgUser.Rows)
            {
                row.Height = 25;
            }
        }

        

        private void SetWatermarkText()
        {

            WatermarkText.SetWatermark(txtUser, "User Name");
            WatermarkText.SetWatermark(txtLogin, "Login ID");

        }

        private void LoadDDL()
        {

            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            //DataRow row = new DataRow();

            //populate role
            ds = new UserBLL().GetAllRoles();
            dt = ds.Tables[0];

            DataRow nullrow = dt.NewRow();
            nullrow["pk_RoleID"] = 0;
            nullrow["RoleName"] = "All Roles";
            dt.Rows.Add(nullrow);

            ddlRole.DataSource = dt;
            ddlRole.DisplayMember = "RoleName";
            ddlRole.ValueMember = "pk_RoleID";

            ddlRole.SelectedValue = 0;

        }

        //private bool CheckForDeleted(int Userid)
        //{
        //    UserBLL userbll = new UserBLL();
        //    UserEntity oUserEntity = (UserEntity)userbll.GetUserForEdit(Userid, "M", 1);
        //    if (oUserEntity.IsActive)
        //    {
        //        return true;
        //    }
        //    else
        //        return false;
        //}

        #endregion

        #region "Cursor Type Change"
        private void btnAddUser_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnAddUser_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void lblExit_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }


        private void lblExit_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void button1_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        #endregion

        #region "Pagination Control"

        private void ChangePageIndex()
        {
            DataView view1;
            view1 = new DataView(GeneralFunctions.GetCurrentRecords(CurrentPageIndex, dsPage.Tables[0], PgSize, "pk_UserID"));
            source1.DataSource = view1;
            int pg = CurrentPageIndex + 1;
            lblPageNo.Text = pg.ToString() + " / " + TotalPage.ToString();
            foreach (DataGridViewRow row in dvgUser.Rows)
            {
                row.Height = 25;
            }
        }

        private void picMoveFirst_Click(object sender, EventArgs e)
        {
            if (CurrentPageIndex != 0)
            {
                CurrentPageIndex = 0;
                ChangePageIndex();
            }
        }

        private void picMoveNext_Click(object sender, EventArgs e)
        {
            if (CurrentPageIndex + 1 < TotalPage)
            {
                CurrentPageIndex++;
                ChangePageIndex();
            }
        }

        private void picMoveLast_Click(object sender, EventArgs e)
        {
            if (CurrentPageIndex + 1 < TotalPage)
            {
                CurrentPageIndex = TotalPage - 1;
                ChangePageIndex();
            }
        }

        private void picMovePrev_Click(object sender, EventArgs e)
        {
            if (CurrentPageIndex != 0)
            {
                CurrentPageIndex--;
                ChangePageIndex();
            }
        }

        #endregion
    }
}
