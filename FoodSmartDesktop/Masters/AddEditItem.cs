﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FoodSmart.BLL;
using FoodSmart.Common;
using System.Configuration;
using System.IO;
using System.Drawing.Drawing2D;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using FoodSmart.Utilities.Cryptography;

namespace FoodSmartDesktop.Masters
{
    public partial class AddEditItem : Form
    {
        string stat;
        int LID;
        int GroupID;

        public AddEditItem(int ID)
        {
            InitializeComponent();
            if (ID == 0)
            {
                stat = "A";
                LID = 0;
            }
            else
            {
                stat = "E";

                LID = ID;
            }
        }
        #region "Public Methods"
        private void AddEditItem_Load(object sender, EventArgs e)
        {
            this.ddlRestName.SelectedIndexChanged -= new System.EventHandler(this.ddlRestName_SelectedIndexChanged);
            this.ddlLoc.SelectedIndexChanged -= new System.EventHandler(this.ddlLoc_SelectedIndexChanged);
            //this.ddlGroup.SelectedIndexChanged -= new System.EventHandler(this.ddlGroup_SelectedIndexChanged);
            LoadLoc();
            LoadRest();
            LoadOtherddl();
          
            if (LID != 0)
                fillItem(LID);
            else
            {
                LoadItemGroup();
                //LoadOtherddl();
            }

            this.ddlRestName.SelectedIndexChanged += new System.EventHandler(this.ddlRestName_SelectedIndexChanged);
            this.ddlLoc.SelectedIndexChanged += new System.EventHandler(this.ddlLoc_SelectedIndexChanged);
            //this.ddlGroup.SelectedIndexChanged += new System.EventHandler(this.ddlGroup_SelectedIndexChanged);
            txtItemName.Focus();
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (ValidateEntry())
            {
                if (ddlGroup.SelectedIndex == 0)
                {
                    MessageBox.Show("Error!! Invalid Item Group", "Save Record", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (ddlRestName.SelectedIndex == -1)
                {
                    MessageBox.Show("Error!! Restaurant Selection", "Save Record", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                IItems Item = new ItemsEntity();
                Item.ItemDescription = txtItemName.Text;
                Item.ItemID = LID;
                Item.ItemGroupID = ddlGroup.SelectedValue.ToInt();
                Item.RestID = ddlRestName.SelectedValue.ToInt();
                Item.LocID = ddlLoc.SelectedValue.ToInt();
                Item.UOM = txtSaleUnit.Text;
                Item.CGSTPer = txtCGSTPer.Text.ToDecimal();
                Item.itemType = ddlItemType.SelectedValue.ToInt();
                Item.SGSTPer = txtSGSTPer.Text.ToDecimal();
                Item.DiscountAllowed = Convert.ToBoolean(ddlDisc.SelectedValue.ToInt());
                Item.HSNCode = txtHSNCode.Text;

                Item.EffectiveDate = dtIntroducedOn.Value;
                Item.CreatedBy = GeneralFunctions.LoginInfo.UserID;

                int Status = new ItemBLL().SaveItem(Item, stat);

                if (Status == 0)
                {
                    MessageBox.Show("Error!! Record Not Saved", "Save Record", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    if (stat == "A")
                    {
                        Initialize();
                        txtItemName.Focus();
                    }
                    else
                    {
                        this.Close();
                    }
                }
            }
        }

        private void lblExit_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void lblExit_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnSave_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnSave_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void ddlLoc_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadRest();
            ddlRestName.SelectedIndex = 0;
        }

        private void ddlRestName_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadItemGroup();
            ddlGroup.SelectedIndex = 0;
        }

        private void txtVATPer_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtItemName_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtItemName.Text))
                txtItemName.BackColor = Color.Red;
            else
                txtItemName.BackColor = SystemColors.Window;
        }

        private void txtSaleUnit_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtSaleUnit.Text))
                txtSaleUnit.BackColor = Color.Red;
            else
                txtSaleUnit.BackColor = SystemColors.Window;
        }

        private void txtVATPer_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtCGSTPer.Text))
                txtCGSTPer.BackColor = Color.Red;
            else
                txtCGSTPer.BackColor = SystemColors.Window;
        }

        #endregion
        #region "Private Methods"
        private void LoadLoc()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            SearchCriteria searchcriteria = new SearchCriteria();
            searchcriteria.QueryStat = "L";

            ds = new LocationBLL().GetAllLoc(searchcriteria);
            dt = ds.Tables[0];

            if (dt.Rows.Count > 0)
            {
                ddlLoc.DataSource = dt;
                ddlLoc.DisplayMember = "LocationName";
                ddlLoc.ValueMember = "pk_LocID";
                ddlLoc.SelectedIndex = 0;
            }
        }

        private void LoadRest()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            SearchCriteria searchcriteria = new SearchCriteria();
            searchcriteria.QueryStat = "L";
            searchcriteria.LocName = ddlLoc.Text;

            ddlRestName.DataSource = null;

            ds = new RestaurantBLL().GetAllRestaurant(searchcriteria);
            dt = ds.Tables[0];
            if (dt.Rows.Count > 0)
            {
                ddlRestName.DataSource = dt;
                ddlRestName.DisplayMember = "RestName";
                ddlRestName.ValueMember = "pk_RestID";
                ddlRestName.SelectedIndex = 0;
            }
        }

        private void LoadItemGroup()
        {
            //DataRow row = new DataRow();
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            //FoodSmart.BLL.itemService.SearchCriteria searchcriteria = new FoodSmart.BLL.itemService.SearchCriteria();

            SearchCriteria searchcriteria = new SearchCriteria();
            searchcriteria.QueryStat = "L";
            searchcriteria.LocName = ddlLoc.Text;
            searchcriteria.RestName = ddlRestName.Text;
            ddlGroup.DataSource = null;

            ddlGroup.Items.Insert(0, "Select Category");
            ds = new ItemBLL().GetAllItemGroup(searchcriteria);
            dt = ds.Tables[0];

            DataRow dr = dt.NewRow();
            dr["Descr"] = "--Select--";
            dr["pk_ItemGroupID"] = 0;

            dt.Rows.InsertAt(dr, 0);

            if (dt.Rows.Count > 0)
            {
                ddlGroup.DataSource = dt;
                ddlGroup.DisplayMember = "descr";
                ddlGroup.ValueMember = "pk_ItemGroupID";
            }
            
        }

        private void LoadOtherddl()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            //DataRow row = new DataRow();

            Dictionary<string, string> cStatus = new Dictionary<string, string>();
            //cStatus.Add("1", "Applicable");
            //cStatus.Add("0", "Not Applicable");
            //ddlStax.DataSource = new BindingSource(cStatus, null);
            //ddlStax.DisplayMember = "Value";
            //ddlStax.ValueMember = "Key";
            //ddlStax.SelectedIndex = 0;

            cStatus.Clear();
            cStatus.Add("0", "Samsung");
            cStatus.Add("1", "Lenovo");
            cStatus.Add("2", "Apple");
            ddlItemType.DataSource = new BindingSource(cStatus, null);
            ddlItemType.DisplayMember = "Value";
            ddlItemType.ValueMember = "Key";
            ddlItemType.SelectedIndex = 0;

            cStatus.Clear();
            cStatus.Add("1", "Allowed");
            cStatus.Add("0", "Not Allowed");
            ddlDisc.DataSource = new BindingSource(cStatus, null);
            ddlDisc.DisplayMember = "Value";
            ddlDisc.ValueMember = "Key";
            ddlDisc.SelectedIndex = 1;
        }

        private void Initialize()
        {
            this.ddlLoc.SelectedIndexChanged -= new System.EventHandler(this.ddlLoc_SelectedIndexChanged);
            this.ddlRestName.SelectedIndexChanged -= new System.EventHandler(this.ddlRestName_SelectedIndexChanged);
            this.ddlRestName.SelectedIndexChanged -= new System.EventHandler(this.ddlRestName_SelectedIndexChanged);
            this.txtSaleUnit.TextChanged -= new System.EventHandler(this.txtSaleUnit_TextChanged);
            this.txtCGSTPer.TextChanged -= new System.EventHandler(this.txtVATPer_TextChanged);
            this.txtItemName.TextChanged -= new System.EventHandler(this.txtItemName_TextChanged);
            //this.ddlGroup.SelectedIndexChanged -= new System.EventHandler(this.ddlGroup_SelectedIndexChanged);

            txtItemName.Text = string.Empty;
            //ddlRestName.DataSource = null;
            txtCGSTPer.Text = string.Empty;
            txtSGSTPer.Text = string.Empty;
            txtHSNCode.Text = string.Empty;
            txtSaleUnit.Text = string.Empty;
            ddlItemType.SelectedIndex = 0;
            ddlDisc.SelectedIndex = 0;
            this.ddlLoc.SelectedIndexChanged += new System.EventHandler(this.ddlLoc_SelectedIndexChanged);
            this.ddlRestName.SelectedIndexChanged += new System.EventHandler(this.ddlRestName_SelectedIndexChanged);
            this.ddlRestName.SelectedIndexChanged += new System.EventHandler(this.ddlRestName_SelectedIndexChanged);
            this.txtSaleUnit.TextChanged += new System.EventHandler(this.txtSaleUnit_TextChanged);
            this.txtCGSTPer.TextChanged += new System.EventHandler(this.txtVATPer_TextChanged);
            this.txtItemName.TextChanged += new System.EventHandler(this.txtItemName_TextChanged);
            //ddlLoc.SelectedIndex = 0;
            //this.ddlGroup.SelectedIndexChanged += new System.EventHandler(this.ddlGroup_SelectedIndexChanged);
            //ddlRestName.SelectedValue = "0";
        }


        private bool ValidateEntry()
        {
            bool RetVal = true;

            if (string.IsNullOrEmpty(txtItemName.Text))
            {
                txtItemName.BackColor = Color.Red;
                RetVal = false;
            }

            if (ddlLoc.SelectedIndex == -1 || ddlLoc.SelectedValue.ToInt() == 0)
            {
                ddlLoc.BackColor = Color.Red;
                RetVal = false;
            }

            if (ddlRestName.SelectedIndex == -1 || ddlRestName.SelectedValue.ToInt() == 0)
            {
                ddlRestName.BackColor = Color.Red;
                RetVal = false;
            }

            if (ddlGroup.SelectedValue.ToInt() == 0)
            {
                MessageBox.Show("Error!! Invalid Item Group", "Item Group Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                RetVal = false;
            }

            if (string.IsNullOrEmpty(txtCGSTPer.Text))
            {
                txtCGSTPer.BackColor = Color.Red;
                RetVal = false;
            }

            if (string.IsNullOrEmpty(txtSaleUnit.Text))
            {
                txtSaleUnit.BackColor = Color.Red;
                RetVal = false;
            }

            return RetVal;
        }

        private void fillItem(int ItemID)
        {
            ItemBLL oItembll = new ItemBLL();

            ItemsEntity oitem = (ItemsEntity)oItembll.GetItemForEdit(ItemID);

            ddlLoc.SelectedValue = oitem.LocID;
            LoadRest();
            ddlRestName.SelectedValue = oitem.RestID;
            LoadItemGroup();
            ddlGroup.SelectedValue = oitem.ItemGroupID;
            txtSaleUnit.Text = oitem.UOM;
            txtCGSTPer.Text = oitem.CGSTPer.ToString();
            txtItemName.Text = oitem.ItemDescription;
            txtSGSTPer.Text = oitem.SGSTPer.ToString();
            txtHSNCode.Text = oitem.HSNCode;
        }
        #endregion

        
    }
}
