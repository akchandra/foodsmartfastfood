﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FoodSmart.BLL;
using FoodSmart.Common;
using System.Configuration;
using System.IO;
using System.Drawing.Drawing2D;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using FoodSmart.Utilities.Cryptography;

namespace FoodSmartDesktop.Masters
{
    public partial class ChangePassword : Form
    {
        public ChangePassword()
        {
            InitializeComponent();
        }

        private void ChangePassword_Load(object sender, EventArgs e)
        {
            lblError.Visible = false;
            lbltoday.Text += " : " + GeneralFunctions.LoginInfo.CurrDate.ToString("dd-MMM-yyyy");
            txtOldPw.Focus();
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (txtNewPW.Text != txtConfPW.Text)
            {
                lblError.Text = "New and Confirm Password Mismatched";
                lblError.Visible = true;
                return;
            }

            if (string.IsNullOrEmpty(txtOldPw.Text) || string.IsNullOrEmpty(txtNewPW.Text) || string.IsNullOrEmpty(txtConfPW.Text))
            {
                lblError.Text = "All fields are mandatory";
                lblError.Visible = true;
                return;
            }

            IUser usr = new UserEntity();

            usr.Password = Encryption.Encrypt(txtOldPw.Text);
            usr.Id = GeneralFunctions.LoginInfo.UserID;
            usr.NewPassword = Encryption.Encrypt(txtNewPW.Text);
            //UserBLL userbll = new UserBLL();
            //bool stat = userbll.ChangePassword(usr);

            bool stat = UserBLL.ChangePassword(usr);
            if (stat)
            {
                MessageBox.Show("Password changed Successfully", "Change Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Password change failed", "Change Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            this.Close();

        }

        private void lblExit_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnOK_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void lblExit_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnOK_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

    }
}
