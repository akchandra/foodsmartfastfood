﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FoodSmart.BLL;
using FoodSmart.Common;
using System.Configuration;
using System.IO;
using System.Drawing.Drawing2D;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using FoodSmart.Utilities.Cryptography;

namespace FoodSmartDesktop.Masters
{
    public partial class AddEditRestaurant : Form
    {
        string stat;
        int LID;

        public AddEditRestaurant(int ID)
        {
            InitializeComponent();
            if (ID == 0)
            {
                stat = "A";
                LID = 0;
            }
            else
            {
                stat = "E";

                LID = ID;
            }
        }

        private void AddEditRestaurant_Load(object sender, EventArgs e)
        {
            LoadDDL();
            Initialize();
            if (LID != 0) 
                FillRest(LID);
            ChkPrintBill.Checked = true;
            //this.ddlCardType.SelectedIndexChanged += new System.EventHandler(this.ddlCardType_SelectedIndexChanged);

        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (ValidateEntry())
            {
                IRestaurant rest = new RestaurantEntity();
                rest.RestName = txtRestName.Text;
                rest.RestPass = txtRestPW.Text;
                rest.RestType = "R"; // ddlRestType.SelectedValue.ToString();
                rest.ServTaxPer = txtServTaxPer.Text.ToDecimal();
                rest.VATNo = txtVATNo.Text;
                rest.ServTaxNo = txtServTaxNo.Text;
                rest.PrinterName = txtPrinterName.Text;
                rest.printerType = ddlBillPrinterType.SelectedValue.ToString();
                rest.KOTPrinterName = txtKOTPrinter.Text;
                rest.KOTPrinterType = ddlKOTPrinterType.SelectedValue.ToString();
                rest.PrintBill = ChkPrintBill.Checked;
                rest.Ratio = txtRatio.Text.ToDecimal();
                rest.RestID = LID;
                rest.SBCPer = txtsbcPer.Text.ToDecimal();

                rest.LocID = ddlLoc.SelectedValue.ToInt();
                //rest.CreatedBy = GeneralFunctions.LoginInfo.UserID;

                int Status = new RestaurantBLL().SaveRestautant(rest, stat, GeneralFunctions.LoginInfo.UserID);

                if (Status == 0)
                {
                    MessageBox.Show("Error!! Record Not Saved", "Save Record", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    if (stat == "A")
                    {
                        //UpdateCard();
                        Initialize();
                        txtRestName.Focus();
                    }
                    else
                    {
                        this.Close();
                    }
                }
            }
        }


        #region "Private Methods"
        private void LoadDDL()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            //DataRow row = new DataRow();

            Dictionary<string, string> cStatus = new Dictionary<string, string>();
            cStatus.Add("C", "Com");
            cStatus.Add("U", "USB");
            ddlBillPrinterType.DataSource = new BindingSource(cStatus, null);
            ddlBillPrinterType.DisplayMember = "Value";
            ddlBillPrinterType.ValueMember = "Key";
            ddlBillPrinterType.SelectedIndex = 0;

            ddlKOTPrinterType.DataSource = new BindingSource(cStatus, null);
            ddlKOTPrinterType.DisplayMember = "Value";
            ddlKOTPrinterType.ValueMember = "Key";
            ddlKOTPrinterType.SelectedIndex = 0;

            cStatus.Clear();
            cStatus.Add("F", "Forward");
            cStatus.Add("B", "Backward");
            ddlCalcMethod.DataSource = new BindingSource(cStatus, null);
            ddlCalcMethod.DisplayMember = "Value";
            ddlCalcMethod.ValueMember = "Key";
            ddlCalcMethod.SelectedIndex = 0;

            //populate Locations
            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.QueryStat = "L";

            ds = new LocationBLL().GetAllLoc(searchCriteria);
            dt = ds.Tables[0];
            ddlLoc.DataSource = dt;
            ddlLoc.DisplayMember = "LocationName";
            ddlLoc.ValueMember = "pk_LocID";
            ddlLoc.SelectedIndex = 0;
        }

        private void FillRest(int RestId)
        {
            RestaurantBLL oRestbll = new RestaurantBLL();

            RestaurantEntity oRestEntity = (RestaurantEntity)oRestbll.GetRestData(RestId);

            //ddlRestType.SelectedValue = oRestEntity.RestType;
            txtRestName.Text = oRestEntity.RestName;
            txtRatio.Text = oRestEntity.Ratio.ToString();
            txtRestPW.Text = oRestEntity.RestPass;
            ChkPrintBill.Checked = oRestEntity.PrintBill;
            txtServTaxPer.Text = oRestEntity.ServTaxPer.ToString();
            txtServTaxNo.Text = oRestEntity.ServTaxNo;
            txtVATNo.Text = oRestEntity.VATNo;
            ddlCalcMethod.SelectedValue = oRestEntity.CalculationMethod;
            ddlLoc.SelectedValue = oRestEntity.LocID;
            txtPrinterName.Text = oRestEntity.PrinterName + "";
            ddlBillPrinterType.SelectedValue = oRestEntity.printerType + "";
            txtKOTPrinter.Text = oRestEntity.KOTPrinterName + "";
            ddlKOTPrinterType.SelectedValue = oRestEntity.KOTPrinterType + "";

            txtsbcPer.Text = oRestEntity.SBCPer.ToString();
        }

        private void Initialize()
        {
            //ddlRestType.SelectedIndex = 0;
            txtRestName.Text = string.Empty;
            txtRatio.Text = string.Empty;
            txtRestPW.Text = string.Empty;
            ChkPrintBill.Checked = true;
            txtServTaxPer.Text = string.Empty;
            txtServTaxNo.Text = string.Empty;
            txtVATNo.Text = string.Empty;
            ddlCalcMethod.SelectedIndex = 0;
            txtPrinterName.Text = string.Empty;
            txtsbcPer.Text = string.Empty;
        }

        private bool ValidateEntry()
        {
            bool RetVal = true;

            if (string.IsNullOrEmpty(txtRestName.Text))
            {
                txtRestName.BackColor = Color.Red;
                RetVal = false;
            }

            if (ddlCalcMethod.SelectedIndex == -1)
            {
                ddlCalcMethod.BackColor = Color.Red;
                RetVal = false;
            }

            //if (ddlRestType.SelectedIndex == -1)
            //{
            //    ddlCalcMethod.BackColor = Color.Red;
            //    RetVal = false;
            //}

            return RetVal;
        }

      
        #endregion

        private void txtServTaxPer_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void lblExit_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void lblExit_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnSave_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnSave_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void txtRatio_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }
    }
}
