﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using Outlook = Microsoft.Office.Interop.Outlook;
using System.Runtime.InteropServices;


namespace ReadEmailFromOutlook
{
	public partial class Form1 : Form
	{
		DataTable EmailInfoTable = null;
        Dictionary<string, string> dictionary;
		public Form1()
		{
			InitializeComponent();
			label1.Visible = false;
		}

		public void ReadEmail()
		{
			Outlook.Application app = new Outlook.Application();
			Outlook.NameSpace outlookNs = app.GetNamespace("MAPI");
			Outlook.MAPIFolder rootFolder = outlookNs.GetDefaultFolder(Microsoft.Office.Interop.Outlook.OlDefaultFolders.olFolderContacts);
            //PrintRootFolderMailInfo(rootFolder);
            GetOutlookData(rootFolder);
			
		}

		public DataTable GetTable()
		{
			DataTable table = new DataTable();
            table.TableName = "Sch.mstTransferredEmails";
            string strColMappings = ConfigurationManager.AppSettings["columnMapping"].ToString();
            string[] arrColMappings = strColMappings.Split(';');
            for (int i = 0; i < arrColMappings.Length; i++)
            {
                string[] arrColMapping = arrColMappings[i].Split(',');
                dictionary.Add(arrColMapping[1],arrColMapping[0]);
                table.Columns.Add(arrColMapping[0], typeof(string));
            }
            table.Columns.Add("Error", typeof(Boolean));
			return table;
		}


		public void PrintRootFolderMailInfo(Outlook.MAPIFolder folder)
		{
			try
			{
                pbMailTransfer.Visible = true;
                pbMailTransfer.Minimum = 0;
                pbMailTransfer.Maximum = folder.Items.Count;

				for (int i = 0; i < folder.Items.Count; i++)
				{
                    DataRow dr = EmailInfoTable.NewRow();
					int cnt = 0;
                    Microsoft.Office.Interop.Outlook.ContactItem contact = (Microsoft.Office.Interop.Outlook.ContactItem)folder.Items[i + 1];
                    if (String.IsNullOrEmpty(contact.Email1Address))
                    {
                        continue;
                    }

					for (int j = 0; j < contact.ItemProperties.Count; j++)
					{
						foreach (KeyValuePair<string, string> entry in dictionary)
						{

							if (contact.ItemProperties[j].Name.ToLower() == entry.Key.ToLower())
							{
								dr[entry.Value] = contact.ItemProperties[j].Value;
								cnt++;
							}
						}
						if (cnt + 1 > dictionary.Count)
						{
							break;
						}
					}
                    EmailInfoTable.Rows.Add(dr);
                    pbMailTransfer.Value = i;
				}
                pbMailTransfer.Visible = false;
			}
			catch (Exception ex)
			{ 
                MessageBox.Show(ex.Message); 
            }
		}

        public void GetOutlookData(Outlook.MAPIFolder folder)
        {
            try
            {
                int i = 0;
                pbMailTransfer.Visible = true;
                pbMailTransfer.Minimum = 0;
                pbMailTransfer.Maximum = folder.Items.Count;

                Outlook.Items contactItems = folder.Items;
                // loop the list of contacts returned
                foreach (Outlook.ContactItem foundContact in contactItems)
                {
                    // do something with each contact
                    if (String.IsNullOrEmpty(foundContact.Email1Address))
                    {
                        i++;
                        pbMailTransfer.Value = i;
                        continue;
                    }

                    DataRow dr = EmailInfoTable.NewRow();
                    dr["EmailIDActive"] = foundContact.Email1Address;
                    dr["CompanyName"] = foundContact.CompanyName;
                    dr["Department"] = foundContact.Department;
                    dr["ReceiverName"] = foundContact.FullName;

                    EmailInfoTable.Rows.Add(dr);
                    i++;
                    pbMailTransfer.Value = i;
                }

                //for (int i = 0; i < folder.Items.Count; i++)
                //{
                //    DataRow dr = EmailInfoTable.NewRow();
                //    int cnt = 0;
                //    Microsoft.Office.Interop.Outlook.ContactItem contact = (Microsoft.Office.Interop.Outlook.ContactItem)folder.Items[i + 1];
                //    if (String.IsNullOrEmpty(contact.Email1Address))
                //    {
                //        continue;
                //    }

                    
                //    for (int j = 0; j < contact.ItemProperties.Count; j++)
                //    {
                //        foreach (KeyValuePair<string, string> entry in dictionary)
                //        {

                //            if (contact.ItemProperties[j].Name.ToLower() == entry.Key.ToLower())
                //            {
                //                dr[entry.Value] = contact.ItemProperties[j].Value;
                //                cnt++;
                //            }
                //        }
                //        if (cnt + 1 > dictionary.Count)
                //        {
                //            break;
                //        }
                //    }
                //    EmailInfoTable.Rows.Add(dr);
                //    pbMailTransfer.Value = i;
                //}
                pbMailTransfer.Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            //Outlook.Application app = new Outlook.Application();
            //Outlook.NameSpace ns = app.GetNamespace("MAPI");
            ////Microsoft.Office.Interop.Outlook.NameSpace ns = Globals.ThisAddIn.Application.GetNamespace("MAPI");
            //Microsoft.Office.Interop.Outlook.MAPIFolder contactsFld = ns.GetDefaultFolder(Microsoft.Office.Interop.Outlook.OlDefaultFolders.olFolderContacts);

            //Microsoft.Office.Interop.Outlook.Table tb = ns.GetDefaultFolder(Microsoft.Office.Interop.Outlook.OlDefaultFolders.olFolderContacts).GetTable(null, Microsoft.Office.Interop.Outlook.OlItemType.olContactItem);

            //tb.Columns.RemoveAll();
            //tb.Columns.Add("Email1Address");
            //tb.Columns.Add("FullName");
            //tb.Columns.Add("Department");

            //object[,] otb = tb.GetArray(100000) as object[,];
            //int len = otb.GetUpperBound(0);

            //for (int i = 0; i < len; i++)
            //{
            //    if (otb[i, 0] == null)
            //    {
            //        continue;
            //    }
                
            //    Contacts.Add(new ContactItem() { ContactDisplay = otb[i, 1].ToString(), ContactEmail = otb[i, 0].ToString() });

            //}
        }
		 
		private void button1_Click(object sender, EventArgs e)
		{
            //try
            //{
                if (MessageBox.Show("Are you sure, you want to proceed ?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    return;
                }

                Cursor.Current = Cursors.WaitCursor;

                SaveContact sc = new SaveContact();
                if (!String.IsNullOrEmpty(ConfigurationManager.AppSettings["WsURL"]))
                {
                    sc.Url = ConfigurationManager.AppSettings["WsURL"];
                }

                label1.Visible = true;
                if (!radioButton3.Checked)
                {
                    DataSet ds = new DataSet();
                    label1.Text = "Gathering outlook data!!!";
                    dictionary = new Dictionary<string, string>();
                    EmailInfoTable = GetTable();
                    ReadEmail();
                    ds.Tables.Add(EmailInfoTable);
                    label1.Text = "Saving Records to Table!";
                    DataSet retDS = sc.SaveContacts(ds, radioButton1.Checked);
                    if (retDS != null)
                    {
                        DataView dv = retDS.Tables["sch.mstTransferredEmails"].DefaultView;
                        dv.RowFilter = "Error=1";
                        dvEmailDetails.DataSource = dv; 
                        label1.Text = "Update unsuccessful due to error in cargo group. Check the grid view";
                    }
                    else
                    {
                        dvEmailDetails.DataSource = ds.Tables["sch.mstTransferredEmails"];
                        label1.Text = "Records updated successfully";
                    }
                }
                else
                {
                    label1.Text = sc.RestorePrevContacts();
                }
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}
            Cursor.Current = Cursors.Default;
		}

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            pbMailTransfer.Visible = false;
        }

	}
}
