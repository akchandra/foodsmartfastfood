﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using FoodSmart.BLL;
using FoodSmart.Common;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using FoodSmart.Utilities.Cryptography;
using FoodSmart.Utilities.ResourceManager;
using FoodSmartDesktop.ImportExport;

namespace FoodSmartDesktop
{
    public partial class Login : Form
    {
        ActivityMonitor.ActivityMonitor _am = new ActivityMonitor.ActivityMonitor();
        //bool readerPresent = false;
        //Timer timer = new Timer();
        Label label = new Label();
        int thisLocId = 0;
        string thisLocName = string.Empty;
        string vData = "";
        string vData1 = "";
        Label lblCarddata = new Label();
        private StringBuilder trackInfo = new StringBuilder();
        private StringBuilder IgnoreInfo = new StringBuilder();
        TextBox activeTextBox;
        Thread oThread;
        private bool connStat = true;

        public string POSLogin
        {
            get { return vData; }
            set
            {
                vData = value;

                if (string.IsNullOrEmpty(value))
                {
                    txtUserName.Text = "";
                }
                else
                {
                    txtUserName.Text = value;
                }
            }
        }

        public string POSPassWord
        {
            get { return vData1; }
            set
            {
                vData1 = value;

                if (string.IsNullOrEmpty(value))
                {
                    txtPassword.Text = "";
                }
                else
                {
                    txtPassword.Text = value;
                }
            }
        }
        public Login()
        {
            InitializeComponent();
            _am.WarningMinutes = 0.5;
            _am.MaxMinutesIdle = 10;
            _am.Idle += new EventHandler(am_Idle);
            activityMonitorBindingSource.DataSource = _am;
        }

        void am_Idle(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Login_Load(object sender, EventArgs e)
        {
            LoadDDL();

            label3.Text = DateTime.Now.ToString("dd-MMM-yyyy");
            lblBuildInfo.Text = String.Format("FoodSmart Build: {0}", System.IO.File.GetLastWriteTime(System.Reflection.Assembly.GetExecutingAssembly().Location).ToString("dd-MMM-yyyy"));
            lblValidUserPw.Visible = false;
            this.btn00.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NumericMouseUp);
            this.btn0.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NumericMouseUp);
            this.btn1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NumericMouseUp);
            this.btn2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NumericMouseUp);
            this.btn3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NumericMouseUp);
            this.btn4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NumericMouseUp);
            this.btn5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NumericMouseUp);
            this.btn6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NumericMouseUp);
            this.btn7.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NumericMouseUp);
            this.btn8.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NumericMouseUp);
            this.btn9.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NumericMouseUp);

            btnLogin.Focus();
           
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            ValidateLocation(); //AR 07/06/2017
            ValidateDayEnd(); //AR 07/06/2017

            if (ValidateUserControl())
            {
                IUser user = new UserEntity();
                user.LoginID = txtUserName.Text.Trim();
                user.Password = Encryption.Encrypt(txtPassword.Text.Trim());

                //try
                //{
                bool valid = new UserBLL().ValidateUser(user);
                if (valid)
                {
                    if (user.LocID != thisLocId && user.RoleID != 1)
                    {
                        MessageBox.Show("User Location and Current Location mismatch. Aborting..");
                        Application.Exit();
                    }
                    Encryption.Encrypt(txtPassword.Text.Trim());
                    GeneralFunctions.LoginInfo.UserID = user.Id;
                    GeneralFunctions.LoginInfo.UserFullName = user.UserFullName;
                    GeneralFunctions.LoginInfo.LocID = thisLocId;
                    GeneralFunctions.LoginInfo.LocName = thisLocName;
                    //GeneralFunctions.LoginInfo.RestID = user.RestID;
                    GeneralFunctions.LoginInfo.CurrDate = user.CurrDate;
                    GeneralFunctions.LoginInfo.UserRole = user.RoleID;
                    GeneralFunctions.LoginInfo.RestName = user.RestName;
                    GeneralFunctions.LoginInfo.CalcMethod = user.CalcMethod;
                    GeneralFunctions.LoginInfo.InternetExists = connStat;
                    //GeneralFunctions.LoginInfo.SecDeposit = user.SecDeposit;

                    FoodSmart.BLL.DBInteraction dbinteract = new FoodSmart.BLL.DBInteraction();
                    System.Data.DataSet ds = dbinteract.GetFinancialYear(1, 0);
                    GeneralFunctions.LoginInfo.CurrFinYearID = ds.Tables[0].Rows[0]["pk_YearID"].ToInt();
                    //timer.Enabled = false;
                    this.Hide();

                    if (connStat)
                    {
                        ImportExport.ImpExp oImpExp = new ImportExport.ImpExp();
                        oThread = new Thread(new ThreadStart(oImpExp.StartModule));
                        oThread.Start();
                    }

                    if (user.RoleID == 3)
                    {
                        new UserBLL().SavePOSUserLogin(user, "I");
                        txtUserName.Text = "";
                        POS.POSInvoice posInvoice = new POS.POSInvoice();
                        //POS.POSInvLayout posInvoice = new POS.POSInvLayout();
                        posInvoice.ShowDialog();
                    }
                    else
                    {
                        Masters.MainMenu Dashboard = new Masters.MainMenu();
                        this.Hide();
                        Dashboard.ShowDialog();
                    }
                }
                else
                {
                    lblValidUserPw.Visible = true;
                    txtPassword.Text = "";
                    //POSLogin = string.Empty;
                    POSPassWord = string.Empty;
                    //trackInfo.Clear();
                }
            }
        }

        private void btnLogin_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void lblExit_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private bool ValidateUserControl()
        {
            if (txtUserName.Text == string.Empty || txtPassword.Text == string.Empty)
            {
                lblValidUserPw.Visible = true;
                txtPassword.Text = "";
                return false;
            }
            return true;
        }
        
        private void lblExit_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnLogin_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        //private void txtUserName_Validating(object sender, CancelEventArgs e)
        //{
        //    DataSet usr = new UserBLL().GetUserById(txtUserName.Text.Trim() + vData);
        //    if (usr.Tables[0].Rows.Count != 0)
        //    {
        //        if (usr.Tables[0].Rows[0]["fk_RoleID"].ToInt() == 2)
        //        {
        //            lblLoginAT.Visible = true;
        //            ddlLoginMode.Visible = true;
        //        }
        //    }
        //}
        private void LoadDDL()
        {
            Dictionary<string, string> cStatus = new Dictionary<string, string>();
            cStatus.Add("C", "Cashier");
            //cStatus.Add("C", "Cashier Card");
            //cStatus.Add("U", "Restaurant Card");
            cStatus.Add("A", "Admin");

            //cStatus.Add("P", "Parking Card");
            ddlLoginMode.DataSource = new BindingSource(cStatus, null);
            ddlLoginMode.DisplayMember = "Value";
            ddlLoginMode.ValueMember = "Key";
            ddlLoginMode.SelectedIndex = 0;
        }
        private void NumericMouseUp(System.Object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (activeTextBox.Name == "txtUserName")
            {
                POSLogin += (sender as Button).Tag.ToString();
                DataSet usr = new UserBLL().GetUserById(POSLogin.Trim());
                if (usr.Tables[0].Rows.Count != 0)
                {
                    if (usr.Tables[0].Rows[0]["fk_RoleID"].ToInt() == 2)
                    {
                        lblLoginAT.Visible = true;
                        ddlLoginMode.Visible = true;
                    }
                    else
                    {
                        lblLoginAT.Visible = false;
                        ddlLoginMode.Visible = false;
                    }
                }
                else
                {
                    lblLoginAT.Visible = false;
                    ddlLoginMode.Visible = false;
                }

            }
            else if (activeTextBox.Name == "txtPassword")
            {
                POSPassWord += (sender as Button).Tag.ToString();
            }
            
        }

        private void txtUserName_Enter(object sender, EventArgs e)
        {
            activeTextBox = (TextBox)sender;
        }

        private void txtPassword_Enter(object sender, EventArgs e)
        {
            activeTextBox = (TextBox)sender;
        }

        private void btnback_MouseUp(object sender, MouseEventArgs e)
        {
            if (activeTextBox.Name == "txtUserName")
            {
                lblLoginAT.Visible = false;
                ddlLoginMode.Visible = false;
                if (POSLogin.Length == 0)
                {
                    POSLogin = "";
                }
                else
                {
                    POSLogin = POSLogin.Substring(0, POSLogin.Length - 1);
                }
            }
            else if (activeTextBox.Name == "txtPassword")
            {
                if (POSPassWord.Length == 0)
                {
                    POSPassWord = "";
                }
                else
                {
                    POSPassWord = POSPassWord.Substring(0, POSPassWord.Length - 1);
                }
            }
        }

        private void ValidateLocation()
        {
            DataSet ds = new DataSet();
            try
            {
                LocationBLL oLoc = new LocationBLL();
                ds = oLoc.GetAndUpdateLocFromWeb();

                if (ds.Tables["LocLocal"].Rows.Count == 0)
                {
                    frmLocationSelection frmLocSelection = new frmLocationSelection(ds);
                    frmLocSelection.ShowDialog();
                    thisLocId = frmLocSelection.selectedLocId;
                    oLoc.SaveAllLocation(ds.Tables["LocWeb"], thisLocId, "E");
                }
                else
                {
                    thisLocId = ds.Tables["LocLocal"].Rows[0]["pk_LocID"].ToInt();
                    thisLocName = ds.Tables["LocLocal"].Rows[0]["LocationName"].ToString();
                    oLoc.SaveAllLocation(ds.Tables["LocWeb"], thisLocId, "E");
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.ToInt() == 0)
                {
                    if (thisLocId == 0)
                    {
                        MessageBox.Show("Internet connection not found as a result location not downloaded. Current loaction  not set...Aborting application");
                        Application.Exit();
                    }
                }
                else
                {
                    thisLocId = ex.Message.ToInt();
                    connStat = false;
                }
            }
        }

        private void ValidateDayEnd()
        {
            DataTable dt = CommonBLL.GetPOSDayEnd();
            if (dt.Rows.Count > 0)
            {
                if (DateTime.Now.Date < Convert.ToDateTime(dt.Rows[0]["WorkingDate"]).Date)
                {
                    MessageBox.Show("Invalid system date. Aborting application !!");
                    Application.Exit();
                }
            }
            CommonBLL.UpDatePOSDayEnd();
        }
        //-AR 07/06/2017 
    }
}
