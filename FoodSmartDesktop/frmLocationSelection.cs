﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FoodSmartDesktop
{
    public partial class frmLocationSelection : Form
    {
        public int selectedLocId = 0;
        public frmLocationSelection(DataSet ds)
        {
            InitializeComponent();
            cmbLocation.DataSource = ds.Tables["LocWeb"];
            cmbLocation.DisplayMember = "LocationName";
            cmbLocation.ValueMember = "pk_LocID";
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {

            if (cmbLocation.SelectedIndex < 0)
            {
                MessageBox.Show("You have to choose one location !!");
            }
            else
            {
                selectedLocId = Convert.ToInt32(cmbLocation.SelectedValue);
                this.Close();
            }
        }
    }
}
