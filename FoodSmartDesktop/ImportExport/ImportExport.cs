﻿///new file created by Anirban Roy on 03/06/2017

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Timers;
using FoodSmart.BLL;

namespace FoodSmartDesktop.ImportExport
{
    public class ImpExp
    {
        Timer aTimer;
        System.Windows.Forms.Form loginForm;
        public ImpExp()
        {
            
        }

        public void StartModule()
        {
            aTimer = new Timer();
            aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);

            aTimer.Interval = 20000;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["ImpExpTimerInterval"]))
            {
                aTimer.Interval = Convert.ToInt32(ConfigurationManager.AppSettings["ImpExpTimerInterval"]) * 1000;
            }
             
            aTimer.Enabled = true;
            OnTimedEvent(null,null);
        }

        private void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            aTimer.Stop();
            //System.Windows.Forms.MessageBox.Show("Hi");
            ImportMasters();
            ExportFromLocal();
            aTimer.Start();
        }

        private void ImportMasters()
        {
            bool retVal=false;
            bool needInsert = false;
            frmNotify fNotify = new frmNotify("Please wait....Masters are getting loaded !!!");
            try
            {
                DataTable dt = CommonBLL.MasterUpdateAllowed();
                if (dt.Rows.Count == 0)
                {
                    retVal = true;
                    needInsert = true;
                }
                else
                {
                    if (Convert.ToDateTime(dt.Rows[0]["LastUpdateDate"].ToString()).Date < DateTime.Now.Date || dt.Rows[0]["Status"].ToString() == "F")
                    {
                        retVal = true;
                    }
                }

                if (retVal)
                {
                    fNotify.Show();
                    fNotify.Refresh();
                    CommonBLL.UpdateStatus(needInsert, "P");
                    retVal = CommonBLL.ImportFromWeb();
                    needInsert = false;
                    CommonBLL.UpdateStatus(needInsert, "N");
                }
            }
            catch (Exception ex)
            {
                CommonBLL.UpdateStatus(needInsert, "F");
                System.Windows.Forms.MessageBox.Show(ex.ToString());
            }
            finally
            {
                fNotify.Close();
                fNotify = null;
            }
        }
        private void ExportFromLocal()
        {
            CommonBLL.ExportFromLocal();
        }
    }
}
