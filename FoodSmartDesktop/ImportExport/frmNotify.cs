﻿///new file created by Anirban Roy on 03/06/2017
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FoodSmartDesktop.ImportExport
{
    public partial class frmNotify : Form
    {
        public frmNotify()
        {
            InitializeComponent();
        }

        public frmNotify(string msg)
        {
            InitializeComponent();
            label1.Text = msg;
        }
    }
}
