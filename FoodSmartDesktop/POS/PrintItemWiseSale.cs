﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;
using System.IO;
using System.Drawing.Printing;
using System.Configuration;
using FoodSmart.BLL;
using FoodSmart.Utilities;

namespace FoodSmartDesktop.POS
{
    public partial class PrintItemWiseSale : Form
    {
        private Font printFont;
        private int RestaurantID;

        public PrintItemWiseSale(int RestID, string prnName)
        {
            InitializeComponent();
            try
            {
                //string prnName = ConfigurationManager.AppSettings["PrinterName"];
                PrinterSettings ps = new PrinterSettings();
                PrintDocument pd;
                PrintDocument pd1;

                //streamToPrint = new StreamWriter("D:\\MyFile.txt");
                try
                {
                    pd = new PrintDocument();
                    foreach (string printer in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
                    {
                        if (printer == prnName)
                        {
                            pd.PrinterSettings.PrinterName = printer;
                            break;
                        }
                    }

                    RestaurantID = RestID;

                    pd1 = new PrintDocument();
                    pd1.PrinterSettings.PrinterName = pd.PrinterSettings.PrinterName;
                    pd1.PrintPage += new PrintPageEventHandler(this.pd_PrintItemReg);
                    pd1.Print();

                    //pd.PrintPage += new PrintPageEventHandler(this.pd_PrintItemReg);
                    //pd.Print();
                    //pd = new PrintDocument();
                }
                finally
                {
                    //pd1.Dispose();
                    //streamToPrint.Close();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void PrintItemWiseSale_Load(object sender, EventArgs e)
        {

        }

        private void pd_PrintItemReg(object sender, PrintPageEventArgs e)
        {

            float x = 10;
            float y = 5;

            float width = 270.0F; // max width I found through trial and error
            float height = 0F;
            int TotQty = 0;
            decimal TotAmt = 0;

            Font drawFontArial12Bold = new Font("Arial", 12, FontStyle.Bold);
            Font drawFontArial10Regular = new Font("Arial", 10, FontStyle.Regular);
            Font drawFontArial8Bold = new Font("Arial", 8, FontStyle.Bold);
            Font drawFontArial8Regular = new Font("Arial", 8, FontStyle.Regular);
            Font drawFontArial10Bold = new Font("Arial", 10, FontStyle.Bold);
            Font drawFontArial7Bold = new Font("Arial", 7, FontStyle.Bold);

            Single topMargin = e.MarginBounds.Top;
            string text = "";
            StringFormat sf = new StringFormat();
            // Set format of string.
            StringFormat drawFormatCenter = new StringFormat();
            drawFormatCenter.Alignment = StringAlignment.Center;
            StringFormat drawFormatLeft = new StringFormat();
            drawFormatLeft.Alignment = StringAlignment.Near;
            StringFormat drawFormatRight = new StringFormat();
            drawFormatRight.Alignment = StringAlignment.Far;

            InvoiceBLL invbll = new InvoiceBLL();
            DataSet ds = invbll.ItemWiseSalesRegister(RestaurantID);
            DataTable dt = ds.Tables[0];
            DataTable dt1 = ds.Tables[1];

            e.Graphics.DrawString(ds.Tables[1].Rows[0]["Company_Name"].ToString(), drawFontArial7Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatCenter);
            y += (1 * drawFontArial12Bold.GetHeight(e.Graphics));

            e.Graphics.DrawString(ds.Tables[1].Rows[0]["Address1"].ToString(), drawFontArial7Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatCenter);
            y += (1 * drawFontArial12Bold.GetHeight(e.Graphics));

            e.Graphics.DrawString("ITEM SALE SUMMARY", drawFontArial8Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatCenter);
            y += (1 * drawFontArial8Bold.GetHeight(e.Graphics));

            e.Graphics.DrawLine(new Pen(Brushes.Black, 1), 1, y, 290, y);
            y += (1 * drawFontArial8Bold.GetHeight(e.Graphics));

            //e.Graphics.DrawString("CODE", drawFontArial8Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
            e.Graphics.DrawString("ITEM NAME", drawFontArial8Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
            e.Graphics.DrawString("QTY", drawFontArial8Regular, Brushes.Black, new RectangleF(200, y, width, height), drawFormatLeft);
            e.Graphics.DrawString("AMT(Rs.)", drawFontArial8Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);

            y += (1 * drawFontArial12Bold.GetHeight(e.Graphics));
            e.Graphics.DrawLine(new Pen(Brushes.Black, 1), 1, y, 290, y);
            y += (1 * drawFontArial10Regular.GetHeight(e.Graphics));

            foreach (DataRow dr1 in dt.Rows)
            {
                //e.Graphics.DrawString(dr1["fk_ItemID"].ToString(), drawFontArial8Regular, Brushes.Black, new RectangleF(150, y, width, height), drawFormatLeft);
                e.Graphics.DrawString(dr1["ItemDescr"].ToString(), drawFontArial8Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
                e.Graphics.DrawString(dr1["TQty"].ToString(), drawFontArial8Regular, Brushes.Black, new RectangleF(200, y, width, height), drawFormatLeft);
                e.Graphics.DrawString(dr1["TTotal"].ToString(), drawFontArial8Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
                y += (1 * drawFontArial8Regular.GetHeight(e.Graphics));
                TotQty += dr1["Tqty"].ToInt();
                TotAmt += dr1["BasicAmount"].ToDecimal();
            }

            //y += (1 * drawFontArial12Bold.GetHeight(e.Graphics));
            e.Graphics.DrawLine(new Pen(Brushes.Black, 1), 1, y, 290, y);
            //e.Graphics.DrawString(string.Concat(Enumerable.Repeat("-", 55)), drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
            y += (1 * drawFontArial10Regular.GetHeight(e.Graphics));

            //decimal basicamt = ds.Tables[0].Rows[0]["GTOTAMOUNT"].ToDecimal() - ds.Tables[0].Rows[0]["VAT"].ToDecimal() - ds.Tables[0].Rows[0]["STAX"].ToDecimal() - ds.Tables[0].Rows[0]["SBCTotal"].ToDecimal();
            e.Graphics.DrawString("R.Off : ", drawFontArial8Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
            e.Graphics.DrawString(ds.Tables[2].Rows[0]["Roff"].ToString(), drawFontArial8Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
            y += (1 * drawFontArial8Bold.GetHeight(e.Graphics));

            e.Graphics.DrawString("Total : ", drawFontArial8Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
            e.Graphics.DrawString(TotQty.ToString(), drawFontArial8Bold, Brushes.Black, new RectangleF(200, y, width, height), drawFormatLeft);
            e.Graphics.DrawString(TotAmt.ToString(), drawFontArial8Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);

            y += (1 * drawFontArial8Bold.GetHeight(e.Graphics));
        }
    }
}
