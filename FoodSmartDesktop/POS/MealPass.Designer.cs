﻿namespace FoodSmartDesktop.POS
{
    partial class MealPass
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MealPass));
            this.ddlMealPass = new System.Windows.Forms.ComboBox();
            this.Label8 = new System.Windows.Forms.Label();
            this.btnCont = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblUserAddEdit = new System.Windows.Forms.Label();
            this.lblExit = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ddlMealPass
            // 
            this.ddlMealPass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlMealPass.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlMealPass.FormattingEnabled = true;
            this.ddlMealPass.Location = new System.Drawing.Point(139, 71);
            this.ddlMealPass.Name = "ddlMealPass";
            this.ddlMealPass.Size = new System.Drawing.Size(294, 32);
            this.ddlMealPass.TabIndex = 213;
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label8.Location = new System.Drawing.Point(15, 78);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(92, 19);
            this.Label8.TabIndex = 214;
            this.Label8.Text = "Meal Psss:";
            // 
            // btnCont
            // 
            this.btnCont.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCont.BackgroundImage")));
            this.btnCont.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCont.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCont.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCont.Image = ((System.Drawing.Image)(resources.GetObject("btnCont.Image")));
            this.btnCont.Location = new System.Drawing.Point(272, 125);
            this.btnCont.Name = "btnCont";
            this.btnCont.Size = new System.Drawing.Size(161, 49);
            this.btnCont.TabIndex = 215;
            this.btnCont.Text = "Continue";
            this.btnCont.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCont.UseVisualStyleBackColor = true;
            this.btnCont.Click += new System.EventHandler(this.btnCont_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(199)))), ((int)(((byte)(233)))));
            this.panel1.Controls.Add(this.lblUserAddEdit);
            this.panel1.Controls.Add(this.lblExit);
            this.panel1.Location = new System.Drawing.Point(1, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(432, 52);
            this.panel1.TabIndex = 216;
            // 
            // lblUserAddEdit
            // 
            this.lblUserAddEdit.BackColor = System.Drawing.Color.Transparent;
            this.lblUserAddEdit.Font = new System.Drawing.Font("Arial", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserAddEdit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.lblUserAddEdit.Location = new System.Drawing.Point(14, 16);
            this.lblUserAddEdit.Name = "lblUserAddEdit";
            this.lblUserAddEdit.Size = new System.Drawing.Size(159, 22);
            this.lblUserAddEdit.TabIndex = 161;
            this.lblUserAddEdit.Text = "Meal Pass";
            // 
            // lblExit
            // 
            this.lblExit.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExit.ForeColor = System.Drawing.Color.DarkRed;
            this.lblExit.Image = ((System.Drawing.Image)(resources.GetObject("lblExit.Image")));
            this.lblExit.Location = new System.Drawing.Point(366, 5);
            this.lblExit.Name = "lblExit";
            this.lblExit.Size = new System.Drawing.Size(63, 47);
            this.lblExit.TabIndex = 1;
            this.lblExit.Click += new System.EventHandler(this.lblExit_Click);
            // 
            // MealPass
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(436, 187);
            this.ControlBox = false;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.ddlMealPass);
            this.Controls.Add(this.btnCont);
            this.Controls.Add(this.Label8);
            this.Name = "MealPass";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.MealPass_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox ddlMealPass;
        private System.Windows.Forms.Button btnCont;
        internal System.Windows.Forms.Label Label8;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblUserAddEdit;
        private System.Windows.Forms.Label lblExit;
    }
}