﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;
using Microsoft.Win32.SafeHandles;
using FoodSmart.BLL;
using FoodSmart.Utilities;

namespace FoodSmartDesktop.POS
{
    public partial class PrintItemWiseSaleLPT : Form
    {
        [DllImport("kernel32.dll", SetLastError = true)]
        static extern SafeFileHandle CreateFile(string lpFileName, FileAccess dwDesiredAccess, uint dwShareMode, IntPtr lpSecurityAttributes, FileMode dwCreationDisposition, uint dwFlagsAndAttributes, IntPtr hTemplateFile);

        private int RestaurantID;

        public PrintItemWiseSaleLPT(int RestID, string prnName)
        {
            InitializeComponent();
            //BillID = InvID;
            //BillType = Mode;
            //CardBalance = AvailableBalance;
            RestaurantID = RestID;

            PrintSaleReg();
        }

        private void PrintItemWiseSaleLPT_Load(object sender, EventArgs e)
        {

        }

        private void PrintSaleReg()
        {
            Byte[] buffer;
            //bool IsConnected = false;
            string sampleText;

            string SLine;
            SLine = string.Concat(Enumerable.Repeat("-", 48));
            int Linesize = 48;
            int sp;
            string nl = Convert.ToChar(13).ToString() + Convert.ToChar(10).ToString();
            int TotQty = 0;
            decimal TotBasic = 0;

            SafeFileHandle fh = CreateFile("LPT1:", FileAccess.Write, 0, IntPtr.Zero, FileMode.OpenOrCreate, 0, IntPtr.Zero);
            if (!fh.IsInvalid)
            {
                //IsConnected = true;
                FileStream lpt1 = new FileStream(fh, FileAccess.ReadWrite);

                SLine = string.Concat(Enumerable.Repeat("-", 48));
                InvoiceBLL invbll = new InvoiceBLL();
                DataSet ds = invbll.ItemWiseSalesRegister(RestaurantID);
                DataTable dt = ds.Tables[0];
                DataTable dt1 = ds.Tables[1];

                sp = (Linesize - (ds.Tables[1].Rows[0]["Company_Name"].ToString()).Length) / 2;
                sampleText = new String(' ', sp) + ds.Tables[1].Rows[0]["Company_Name"].ToString() + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sp = (Linesize - (ds.Tables[1].Rows[0]["Address1"].ToString()).Length) / 2;
                sampleText = new String(' ', sp) + ds.Tables[1].Rows[0]["Address1"].ToString() + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sp = (Linesize - (ds.Tables[0].Rows[0]["RestName"].ToString()).Length) / 2;
                sampleText = new String(' ', sp) + ds.Tables[0].Rows[0]["RestName"].ToString() + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sp = (Linesize - ("ITEM SALE SUMMARY".Length)) / 2;
                sampleText = new String(' ', sp) + "ITEM SALE SUMMARY" + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sp = (Linesize - ("FOR " + DateTime.Today).Length) / 2;
                sampleText = new String(' ', sp) + "FOR " + DateTime.Today + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sampleText = SLine + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sampleText = "ITEM NAME                      QTY     AMOUNT" + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sampleText = SLine + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                foreach (DataRow dr1 in dt.Rows)
                {
                    if (dr1["ItemDescr"].ToString().Length > 35)
                        sampleText = dr1["ItemDescr"].ToString().Substring(0, 35);
                    else
                    {
                        sp = 35 - dr1["ItemDescr"].ToString().Length;
                        sampleText = dr1["ItemDescr"].ToString() + new String(' ', sp);
                    }

                    if (Convert.ToDecimal(dr1["TQty"]).ToString("##0").Length < 3)
                        sp = 3 - Convert.ToDecimal(dr1["TQty"]).ToString("##0").Length;
                    else
                        sp = 0;

                    sampleText = sampleText + new String(' ', sp + 1) + Convert.ToDecimal(dr1["TQty"]).ToString("##0");

                    if (Convert.ToDecimal(dr1["BasicAmount"]).ToString("####0.00").Length < 8)
                        sp = 8 - Convert.ToDecimal(dr1["BasicAmount"]).ToString("####0.00").Length;
                    else
                        sp = 0;
                    sampleText = sampleText + new String(' ', sp + 1) + Convert.ToDecimal(dr1["BasicAmount"]).ToString("####0.00") + nl;

                    buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                    lpt1.Write(buffer, 0, buffer.Length);

                    //sampleText = dr1["ItemDescr"].ToString() + string.Concat(Enumerable.Repeat(" ", 38 - (dr1["ItemDescr"].ToString().Trim().Length))) + Convert.ToInt32(dr1["Qty"]).ToString("##0") + nl;
                    //sampleText = string.Concat(Enumerable.Repeat(" ", 30 - (dr1["ItemDescr"].ToString().Trim().Length))) + " " + Convert.ToInt32(dr1["TQty"]).ToString("##0") + Convert.ToDecimal(dr1["BasicAmount"]).ToString("######0.00");
                    //buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                    //lpt1.Write(buffer, 0, buffer.Length);

                    TotQty += dr1["Tqty"].ToInt();
                    TotBasic += dr1["BasicAmount"].ToDecimal();

                }

                sampleText = SLine + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sampleText = "Total :                            " + TotQty.ToString("##0") + " " + TotBasic.ToString("######0.00") + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sampleText = SLine + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sampleText = " " + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sampleText = " " + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sampleText = "\r\n" + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sampleText = string.Format("{0}{1}", Convert.ToChar(27), Convert.ToChar(105));
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);
               

                lpt1.Close();
            }
        }
    }
}
