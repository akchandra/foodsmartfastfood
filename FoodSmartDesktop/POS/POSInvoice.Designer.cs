﻿namespace FoodSmartDesktop.POS
{
    partial class POSInvoice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(POSInvoice));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tmrUserCard = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblExit = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblInvoiceHeading = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.LinkLabel();
            this.gbMenu = new System.Windows.Forms.TableLayoutPanel();
            this.btnCashMemo = new System.Windows.Forms.Button();
            this.btnReprint = new System.Windows.Forms.Button();
            this.btnClearItems = new System.Windows.Forms.Button();
            this.btnRefund = new System.Windows.Forms.Button();
            this.btnItemSummary = new System.Windows.Forms.Button();
            this.btnItemMenu = new System.Windows.Forms.Button();
            this.tplPersonalInfo = new System.Windows.Forms.TableLayoutPanel();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtMobileNo = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.lblToday = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.btn5 = new System.Windows.Forms.Button();
            this.btn0 = new System.Windows.Forms.Button();
            this.btn1 = new System.Windows.Forms.Button();
            this.btn3 = new System.Windows.Forms.Button();
            this.btn4 = new System.Windows.Forms.Button();
            this.btn2 = new System.Windows.Forms.Button();
            this.btn00 = new System.Windows.Forms.Button();
            this.btnback = new System.Windows.Forms.Button();
            this.btn9 = new System.Windows.Forms.Button();
            this.btn8 = new System.Windows.Forms.Button();
            this.btn7 = new System.Windows.Forms.Button();
            this.btn6 = new System.Windows.Forms.Button();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.gbItems = new CodeVendor.Controls.Grouper();
            this.pnlItems = new System.Windows.Forms.Panel();
            this.gbHotItems = new CodeVendor.Controls.Grouper();
            this.grpInvoice = new CodeVendor.Controls.Grouper();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.dvgInvoice = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.lblDisc = new System.Windows.Forms.Label();
            this.txtDiscPer = new System.Windows.Forms.TextBox();
            this.lnkDiscount = new System.Windows.Forms.LinkLabel();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.lblBillAmount = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblRoffValue = new System.Windows.Forms.Label();
            this.lblBreakup = new System.Windows.Forms.Label();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.lblInternalNoMessage = new System.Windows.Forms.Label();
            this.tbLayoutItemGroup = new System.Windows.Forms.TableLayoutPanel();
            this.gbItemGroup = new CodeVendor.Controls.Grouper();
            this.btnLeft = new System.Windows.Forms.Button();
            this.btnRight = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.gbMenu.SuspendLayout();
            this.tplPersonalInfo.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.gbItems.SuspendLayout();
            this.grpInvoice.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvgInvoice)).BeginInit();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tbLayoutItemGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(199)))), ((int)(((byte)(233)))));
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 42.58783F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35.38989F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22.02228F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 312F));
            this.tableLayoutPanel1.Controls.Add(this.lblExit, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblInvoiceHeading, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblUserName, 2, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1270, 45);
            this.tableLayoutPanel1.TabIndex = 29;
            // 
            // lblExit
            // 
            this.lblExit.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblExit.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExit.ForeColor = System.Drawing.Color.DarkRed;
            this.lblExit.Image = ((System.Drawing.Image)(resources.GetObject("lblExit.Image")));
            this.lblExit.Location = new System.Drawing.Point(1204, 0);
            this.lblExit.Name = "lblExit";
            this.lblExit.Size = new System.Drawing.Size(63, 45);
            this.lblExit.TabIndex = 23;
            this.lblExit.Click += new System.EventHandler(this.lblExit_Click);
            this.lblExit.MouseLeave += new System.EventHandler(this.lblExit_MouseLeave);
            this.lblExit.MouseHover += new System.EventHandler(this.lblExit_MouseHover);
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Font = new System.Drawing.Font("Monotype Corsiva", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(407, 45);
            this.label1.TabIndex = 1;
            // 
            // lblInvoiceHeading
            // 
            this.lblInvoiceHeading.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblInvoiceHeading.AutoSize = true;
            this.lblInvoiceHeading.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInvoiceHeading.Location = new System.Drawing.Point(410, 10);
            this.lblInvoiceHeading.Name = "lblInvoiceHeading";
            this.lblInvoiceHeading.Size = new System.Drawing.Size(333, 25);
            this.lblInvoiceHeading.TabIndex = 21;
            this.lblInvoiceHeading.Text = "Restaurant Name";
            // 
            // lblUserName
            // 
            this.lblUserName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUserName.AutoSize = true;
            this.lblUserName.Location = new System.Drawing.Point(749, 16);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(204, 13);
            this.lblUserName.TabIndex = 24;
            this.lblUserName.TabStop = true;
            this.lblUserName.Text = "Welcome ";
            this.lblUserName.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblUserName_LinkClicked);
            // 
            // gbMenu
            // 
            this.gbMenu.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.OutsetDouble;
            this.gbMenu.ColumnCount = 7;
            this.gbMenu.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66544F));
            this.gbMenu.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66691F));
            this.gbMenu.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66691F));
            this.gbMenu.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66691F));
            this.gbMenu.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66691F));
            this.gbMenu.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66691F));
            this.gbMenu.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 277F));
            this.gbMenu.Controls.Add(this.btnCashMemo, 0, 0);
            this.gbMenu.Controls.Add(this.btnReprint, 3, 0);
            this.gbMenu.Controls.Add(this.btnClearItems, 2, 0);
            this.gbMenu.Controls.Add(this.btnRefund, 1, 0);
            this.gbMenu.Controls.Add(this.btnItemSummary, 4, 0);
            this.gbMenu.Controls.Add(this.btnItemMenu, 5, 0);
            this.gbMenu.Controls.Add(this.tplPersonalInfo, 6, 0);
            this.gbMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbMenu.Location = new System.Drawing.Point(0, 45);
            this.gbMenu.Name = "gbMenu";
            this.gbMenu.RowCount = 1;
            this.gbMenu.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.gbMenu.Size = new System.Drawing.Size(1270, 60);
            this.gbMenu.TabIndex = 30;
            // 
            // btnCashMemo
            // 
            this.btnCashMemo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(86)))), ((int)(((byte)(125)))));
            this.btnCashMemo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCashMemo.BackgroundImage")));
            this.btnCashMemo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCashMemo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCashMemo.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCashMemo.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnCashMemo.Image = ((System.Drawing.Image)(resources.GetObject("btnCashMemo.Image")));
            this.btnCashMemo.Location = new System.Drawing.Point(6, 6);
            this.btnCashMemo.Name = "btnCashMemo";
            this.btnCashMemo.Size = new System.Drawing.Size(155, 48);
            this.btnCashMemo.TabIndex = 17;
            this.btnCashMemo.Text = "CASH MEMO";
            this.btnCashMemo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCashMemo.UseVisualStyleBackColor = false;
            this.btnCashMemo.Click += new System.EventHandler(this.btnCashMemo_Click);
            this.btnCashMemo.MouseLeave += new System.EventHandler(this.btnCashMemo_MouseLeave);
            this.btnCashMemo.MouseHover += new System.EventHandler(this.btnCashMemo_MouseHover);
            // 
            // btnReprint
            // 
            this.btnReprint.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(86)))), ((int)(((byte)(125)))));
            this.btnReprint.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnReprint.BackgroundImage")));
            this.btnReprint.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnReprint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReprint.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReprint.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnReprint.Image = ((System.Drawing.Image)(resources.GetObject("btnReprint.Image")));
            this.btnReprint.Location = new System.Drawing.Point(498, 6);
            this.btnReprint.Name = "btnReprint";
            this.btnReprint.Size = new System.Drawing.Size(155, 48);
            this.btnReprint.TabIndex = 20;
            this.btnReprint.Text = "RE-PRINT";
            this.btnReprint.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnReprint.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnReprint.UseVisualStyleBackColor = false;
            this.btnReprint.Click += new System.EventHandler(this.btnReprint_Click);
            this.btnReprint.MouseLeave += new System.EventHandler(this.btnCancelCard_MouseLeave);
            this.btnReprint.MouseHover += new System.EventHandler(this.btnCancelCard_MouseHover);
            // 
            // btnClearItems
            // 
            this.btnClearItems.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(86)))), ((int)(((byte)(125)))));
            this.btnClearItems.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnClearItems.BackgroundImage")));
            this.btnClearItems.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnClearItems.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClearItems.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClearItems.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnClearItems.Image = ((System.Drawing.Image)(resources.GetObject("btnClearItems.Image")));
            this.btnClearItems.Location = new System.Drawing.Point(334, 6);
            this.btnClearItems.Name = "btnClearItems";
            this.btnClearItems.Size = new System.Drawing.Size(155, 48);
            this.btnClearItems.TabIndex = 19;
            this.btnClearItems.Text = "CLEAR BILL";
            this.btnClearItems.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClearItems.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClearItems.UseVisualStyleBackColor = false;
            this.btnClearItems.Click += new System.EventHandler(this.btnClearItems_Click);
            this.btnClearItems.MouseLeave += new System.EventHandler(this.btnClearItems_MouseLeave);
            this.btnClearItems.MouseHover += new System.EventHandler(this.btnClearItems_MouseHover);
            // 
            // btnRefund
            // 
            this.btnRefund.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(86)))), ((int)(((byte)(125)))));
            this.btnRefund.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRefund.BackgroundImage")));
            this.btnRefund.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnRefund.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRefund.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefund.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnRefund.Image = ((System.Drawing.Image)(resources.GetObject("btnRefund.Image")));
            this.btnRefund.Location = new System.Drawing.Point(170, 6);
            this.btnRefund.Name = "btnRefund";
            this.btnRefund.Size = new System.Drawing.Size(155, 48);
            this.btnRefund.TabIndex = 18;
            this.btnRefund.Text = "REFUND";
            this.btnRefund.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnRefund.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnRefund.UseVisualStyleBackColor = false;
            this.btnRefund.Click += new System.EventHandler(this.btnRefund_Click);
            this.btnRefund.MouseLeave += new System.EventHandler(this.btnRefund_MouseLeave);
            this.btnRefund.MouseHover += new System.EventHandler(this.btnRefund_MouseHover);
            // 
            // btnItemSummary
            // 
            this.btnItemSummary.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(86)))), ((int)(((byte)(125)))));
            this.btnItemSummary.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnItemSummary.BackgroundImage")));
            this.btnItemSummary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnItemSummary.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.btnItemSummary.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnItemSummary.Image = ((System.Drawing.Image)(resources.GetObject("btnItemSummary.Image")));
            this.btnItemSummary.Location = new System.Drawing.Point(662, 6);
            this.btnItemSummary.Name = "btnItemSummary";
            this.btnItemSummary.Size = new System.Drawing.Size(155, 48);
            this.btnItemSummary.TabIndex = 30;
            this.btnItemSummary.Text = "Summary";
            this.btnItemSummary.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnItemSummary.UseVisualStyleBackColor = false;
            this.btnItemSummary.Click += new System.EventHandler(this.btnItemSummary_Click);
            // 
            // btnItemMenu
            // 
            this.btnItemMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(86)))), ((int)(((byte)(125)))));
            this.btnItemMenu.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnItemMenu.BackgroundImage")));
            this.btnItemMenu.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.btnItemMenu.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnItemMenu.Image = ((System.Drawing.Image)(resources.GetObject("btnItemMenu.Image")));
            this.btnItemMenu.Location = new System.Drawing.Point(826, 6);
            this.btnItemMenu.Name = "btnItemMenu";
            this.btnItemMenu.Size = new System.Drawing.Size(155, 48);
            this.btnItemMenu.TabIndex = 29;
            this.btnItemMenu.Text = "Hold Item";
            this.btnItemMenu.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnItemMenu.UseVisualStyleBackColor = false;
            this.btnItemMenu.Visible = false;
            this.btnItemMenu.Click += new System.EventHandler(this.btnItemMenu_Click);
            // 
            // tplPersonalInfo
            // 
            this.tplPersonalInfo.ColumnCount = 1;
            this.tplPersonalInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tplPersonalInfo.Controls.Add(this.txtName, 0, 1);
            this.tplPersonalInfo.Controls.Add(this.txtMobileNo, 0, 0);
            this.tplPersonalInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tplPersonalInfo.Location = new System.Drawing.Point(990, 6);
            this.tplPersonalInfo.Name = "tplPersonalInfo";
            this.tplPersonalInfo.RowCount = 2;
            this.tplPersonalInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tplPersonalInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tplPersonalInfo.Size = new System.Drawing.Size(274, 48);
            this.tplPersonalInfo.TabIndex = 31;
            // 
            // txtName
            // 
            this.txtName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtName.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName.Location = new System.Drawing.Point(3, 28);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(268, 22);
            this.txtName.TabIndex = 1;
            // 
            // txtMobileNo
            // 
            this.txtMobileNo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMobileNo.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMobileNo.Location = new System.Drawing.Point(3, 3);
            this.txtMobileNo.Name = "txtMobileNo";
            this.txtMobileNo.Size = new System.Drawing.Size(268, 22);
            this.txtMobileNo.TabIndex = 0;
            this.txtMobileNo.TextChanged += new System.EventHandler(this.txtMobileNo_TextChanged);
            this.txtMobileNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMobileNo_KeyPress);
            this.txtMobileNo.Validated += new System.EventHandler(this.txtMobileNo_Validated);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Dock = System.Windows.Forms.DockStyle.Right;
            this.label17.Font = new System.Drawing.Font("Arial", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Snow;
            this.label17.Location = new System.Drawing.Point(1136, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(134, 16);
            this.label17.TabIndex = 1;
            this.label17.Text = "Powered By Europia";
            // 
            // lblToday
            // 
            this.lblToday.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblToday.Font = new System.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblToday.ForeColor = System.Drawing.Color.White;
            this.lblToday.Location = new System.Drawing.Point(0, 0);
            this.lblToday.Name = "lblToday";
            this.lblToday.Size = new System.Drawing.Size(227, 36);
            this.lblToday.TabIndex = 21;
            this.lblToday.Text = "Today";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.panel2.Controls.Add(this.lblToday);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 672);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1270, 36);
            this.panel2.TabIndex = 22;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 6;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel5.Controls.Add(this.btn5, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.btn0, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.btn1, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.btn3, 3, 0);
            this.tableLayoutPanel5.Controls.Add(this.btn4, 4, 0);
            this.tableLayoutPanel5.Controls.Add(this.btn2, 2, 0);
            this.tableLayoutPanel5.Controls.Add(this.btn00, 5, 0);
            this.tableLayoutPanel5.Controls.Add(this.btnback, 5, 1);
            this.tableLayoutPanel5.Controls.Add(this.btn9, 4, 1);
            this.tableLayoutPanel5.Controls.Add(this.btn8, 3, 1);
            this.tableLayoutPanel5.Controls.Add(this.btn7, 2, 1);
            this.tableLayoutPanel5.Controls.Add(this.btn6, 1, 1);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(729, 361);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel3.SetRowSpan(this.tableLayoutPanel5, 2);
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(538, 133);
            this.tableLayoutPanel5.TabIndex = 0;
            // 
            // btn5
            // 
            this.btn5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn5.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn5.ForeColor = System.Drawing.Color.White;
            this.btn5.Location = new System.Drawing.Point(3, 69);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(83, 61);
            this.btn5.TabIndex = 29;
            this.btn5.Tag = "5";
            this.btn5.Text = "5";
            this.btn5.UseVisualStyleBackColor = true;
            this.btn5.Click += new System.EventHandler(this.btnAmt_Click);
            // 
            // btn0
            // 
            this.btn0.BackColor = System.Drawing.SystemColors.Info;
            this.btn0.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn0.BackgroundImage")));
            this.btn0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn0.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn0.ForeColor = System.Drawing.Color.White;
            this.btn0.Location = new System.Drawing.Point(3, 3);
            this.btn0.Name = "btn0";
            this.btn0.Size = new System.Drawing.Size(83, 60);
            this.btn0.TabIndex = 13;
            this.btn0.Tag = "0";
            this.btn0.Text = "0";
            this.btn0.UseVisualStyleBackColor = false;
            this.btn0.Click += new System.EventHandler(this.btnAmt_Click);
            // 
            // btn1
            // 
            this.btn1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn1.BackgroundImage")));
            this.btn1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn1.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn1.ForeColor = System.Drawing.Color.White;
            this.btn1.Location = new System.Drawing.Point(92, 3);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(83, 60);
            this.btn1.TabIndex = 14;
            this.btn1.Tag = "1";
            this.btn1.Text = "1";
            this.btn1.UseVisualStyleBackColor = true;
            this.btn1.Click += new System.EventHandler(this.btnAmt_Click);
            // 
            // btn3
            // 
            this.btn3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn3.BackgroundImage")));
            this.btn3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn3.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn3.ForeColor = System.Drawing.Color.White;
            this.btn3.Location = new System.Drawing.Point(270, 3);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(83, 60);
            this.btn3.TabIndex = 16;
            this.btn3.Tag = "3";
            this.btn3.Text = "3";
            this.btn3.UseVisualStyleBackColor = true;
            this.btn3.Click += new System.EventHandler(this.btnAmt_Click);
            // 
            // btn4
            // 
            this.btn4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn4.BackgroundImage")));
            this.btn4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn4.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn4.ForeColor = System.Drawing.Color.White;
            this.btn4.Location = new System.Drawing.Point(359, 3);
            this.btn4.Name = "btn4";
            this.btn4.Size = new System.Drawing.Size(83, 60);
            this.btn4.TabIndex = 17;
            this.btn4.Tag = "4";
            this.btn4.Text = "4";
            this.btn4.UseVisualStyleBackColor = true;
            this.btn4.Click += new System.EventHandler(this.btnAmt_Click);
            // 
            // btn2
            // 
            this.btn2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn2.BackgroundImage")));
            this.btn2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn2.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn2.ForeColor = System.Drawing.Color.White;
            this.btn2.Location = new System.Drawing.Point(181, 3);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(83, 60);
            this.btn2.TabIndex = 15;
            this.btn2.Tag = "2";
            this.btn2.Text = "2";
            this.btn2.UseVisualStyleBackColor = true;
            this.btn2.Click += new System.EventHandler(this.btnAmt_Click);
            // 
            // btn00
            // 
            this.btn00.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn00.BackgroundImage")));
            this.btn00.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn00.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn00.ForeColor = System.Drawing.Color.White;
            this.btn00.Location = new System.Drawing.Point(448, 3);
            this.btn00.Name = "btn00";
            this.btn00.Size = new System.Drawing.Size(87, 60);
            this.btn00.TabIndex = 23;
            this.btn00.Tag = "00";
            this.btn00.Text = "00";
            this.btn00.UseVisualStyleBackColor = true;
            this.btn00.Click += new System.EventHandler(this.btnAmt_Click);
            // 
            // btnback
            // 
            this.btnback.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnback.BackgroundImage")));
            this.btnback.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnback.Image = ((System.Drawing.Image)(resources.GetObject("btnback.Image")));
            this.btnback.Location = new System.Drawing.Point(448, 69);
            this.btnback.Name = "btnback";
            this.btnback.Size = new System.Drawing.Size(87, 61);
            this.btnback.TabIndex = 24;
            this.btnback.Tag = "0";
            this.btnback.UseVisualStyleBackColor = true;
            this.btnback.Click += new System.EventHandler(this.btnAmt_Click);
            // 
            // btn9
            // 
            this.btn9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn9.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn9.ForeColor = System.Drawing.Color.White;
            this.btn9.Location = new System.Drawing.Point(359, 69);
            this.btn9.Name = "btn9";
            this.btn9.Size = new System.Drawing.Size(83, 61);
            this.btn9.TabIndex = 25;
            this.btn9.Tag = "9";
            this.btn9.Text = "9";
            this.btn9.UseVisualStyleBackColor = true;
            this.btn9.Click += new System.EventHandler(this.btnAmt_Click);
            // 
            // btn8
            // 
            this.btn8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn8.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn8.ForeColor = System.Drawing.Color.White;
            this.btn8.Location = new System.Drawing.Point(270, 69);
            this.btn8.Name = "btn8";
            this.btn8.Size = new System.Drawing.Size(83, 61);
            this.btn8.TabIndex = 26;
            this.btn8.Tag = "8";
            this.btn8.Text = "8";
            this.btn8.UseVisualStyleBackColor = true;
            this.btn8.Click += new System.EventHandler(this.btnAmt_Click);
            // 
            // btn7
            // 
            this.btn7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn7.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn7.ForeColor = System.Drawing.Color.White;
            this.btn7.Location = new System.Drawing.Point(181, 69);
            this.btn7.Name = "btn7";
            this.btn7.Size = new System.Drawing.Size(83, 61);
            this.btn7.TabIndex = 27;
            this.btn7.Tag = "7";
            this.btn7.Text = "7";
            this.btn7.UseVisualStyleBackColor = true;
            this.btn7.Click += new System.EventHandler(this.btnAmt_Click);
            // 
            // btn6
            // 
            this.btn6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn6.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn6.ForeColor = System.Drawing.Color.White;
            this.btn6.Location = new System.Drawing.Point(92, 69);
            this.btn6.Name = "btn6";
            this.btn6.Size = new System.Drawing.Size(83, 61);
            this.btn6.TabIndex = 28;
            this.btn6.Tag = "6";
            this.btn6.Text = "6";
            this.btn6.UseVisualStyleBackColor = true;
            this.btn6.Click += new System.EventHandler(this.btnAmt_Click);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 57.16536F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 42.83464F));
            this.tableLayoutPanel3.Controls.Add(this.gbItems, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.gbHotItems, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.grpInvoice, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel6, 1, 4);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel5, 1, 2);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 154);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 4;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 24.38272F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 75.61729F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 87F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1270, 518);
            this.tableLayoutPanel3.TabIndex = 32;
            // 
            // gbItems
            // 
            this.gbItems.BackgroundColor = System.Drawing.Color.White;
            this.gbItems.BackgroundGradientColor = System.Drawing.Color.White;
            this.gbItems.BackgroundGradientMode = CodeVendor.Controls.Grouper.GroupBoxGradientMode.None;
            this.gbItems.BorderColor = System.Drawing.Color.Black;
            this.gbItems.BorderThickness = 1F;
            this.gbItems.Controls.Add(this.pnlItems);
            this.gbItems.CustomGroupBoxColor = System.Drawing.Color.White;
            this.gbItems.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbItems.GroupImage = null;
            this.gbItems.GroupTitle = "";
            this.gbItems.Location = new System.Drawing.Point(3, 90);
            this.gbItems.Name = "gbItems";
            this.gbItems.Padding = new System.Windows.Forms.Padding(20);
            this.gbItems.PaintGroupBox = false;
            this.gbItems.RoundCorners = 10;
            this.tableLayoutPanel3.SetRowSpan(this.gbItems, 3);
            this.gbItems.ShadowColor = System.Drawing.Color.DarkGray;
            this.gbItems.ShadowControl = true;
            this.gbItems.ShadowThickness = 3;
            this.gbItems.Size = new System.Drawing.Size(720, 404);
            this.gbItems.TabIndex = 29;
            // 
            // pnlItems
            // 
            this.pnlItems.AutoScroll = true;
            this.pnlItems.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlItems.Location = new System.Drawing.Point(20, 20);
            this.pnlItems.Name = "pnlItems";
            this.pnlItems.Size = new System.Drawing.Size(680, 364);
            this.pnlItems.TabIndex = 0;
            // 
            // gbHotItems
            // 
            this.gbHotItems.BackgroundColor = System.Drawing.Color.White;
            this.gbHotItems.BackgroundGradientColor = System.Drawing.Color.White;
            this.gbHotItems.BackgroundGradientMode = CodeVendor.Controls.Grouper.GroupBoxGradientMode.None;
            this.gbHotItems.BorderColor = System.Drawing.Color.Maroon;
            this.gbHotItems.BorderThickness = 1F;
            this.gbHotItems.CustomGroupBoxColor = System.Drawing.Color.White;
            this.gbHotItems.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbHotItems.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbHotItems.GroupImage = null;
            this.gbHotItems.GroupTitle = "Hot Items";
            this.gbHotItems.Location = new System.Drawing.Point(3, 3);
            this.gbHotItems.Name = "gbHotItems";
            this.gbHotItems.Padding = new System.Windows.Forms.Padding(20);
            this.gbHotItems.PaintGroupBox = false;
            this.gbHotItems.RoundCorners = 10;
            this.gbHotItems.ShadowColor = System.Drawing.Color.DarkGray;
            this.gbHotItems.ShadowControl = false;
            this.gbHotItems.ShadowThickness = 3;
            this.gbHotItems.Size = new System.Drawing.Size(720, 81);
            this.gbHotItems.TabIndex = 19;
            // 
            // grpInvoice
            // 
            this.grpInvoice.BackgroundColor = System.Drawing.Color.White;
            this.grpInvoice.BackgroundGradientColor = System.Drawing.Color.White;
            this.grpInvoice.BackgroundGradientMode = CodeVendor.Controls.Grouper.GroupBoxGradientMode.None;
            this.grpInvoice.BorderColor = System.Drawing.Color.Navy;
            this.grpInvoice.BorderThickness = 1F;
            this.grpInvoice.Controls.Add(this.tableLayoutPanel2);
            this.grpInvoice.Controls.Add(this.lblBreakup);
            this.grpInvoice.CustomGroupBoxColor = System.Drawing.Color.White;
            this.grpInvoice.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpInvoice.Font = new System.Drawing.Font("Arial Black", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpInvoice.GroupImage = null;
            this.grpInvoice.GroupTitle = "INVOICE";
            this.grpInvoice.Location = new System.Drawing.Point(729, 3);
            this.grpInvoice.Name = "grpInvoice";
            this.grpInvoice.Padding = new System.Windows.Forms.Padding(20);
            this.grpInvoice.PaintGroupBox = false;
            this.grpInvoice.RoundCorners = 10;
            this.tableLayoutPanel3.SetRowSpan(this.grpInvoice, 2);
            this.grpInvoice.ShadowColor = System.Drawing.Color.DarkGray;
            this.grpInvoice.ShadowControl = false;
            this.grpInvoice.ShadowThickness = 3;
            this.grpInvoice.Size = new System.Drawing.Size(538, 352);
            this.grpInvoice.TabIndex = 30;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.84283F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80.15717F));
            this.tableLayoutPanel2.Controls.Add(this.dvgInvoice, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel7, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.lnkDiscount, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel8, 1, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(20, 20);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 89.28571F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.71429F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(498, 312);
            this.tableLayoutPanel2.TabIndex = 4;
            // 
            // dvgInvoice
            // 
            this.dvgInvoice.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dvgInvoice.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableLayoutPanel2.SetColumnSpan(this.dvgInvoice, 2);
            this.dvgInvoice.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dvgInvoice.Location = new System.Drawing.Point(3, 3);
            this.dvgInvoice.Name = "dvgInvoice";
            this.dvgInvoice.RowHeadersVisible = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dvgInvoice.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dvgInvoice.Size = new System.Drawing.Size(492, 247);
            this.dvgInvoice.TabIndex = 3;
            this.dvgInvoice.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dvgInvoice_CellContentClick);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 283);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 18);
            this.label3.TabIndex = 1;
            this.label3.Text = "TOTALS";
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 2;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Controls.Add(this.lblDisc, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.txtDiscPer, 0, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(101, 256);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(394, 24);
            this.tableLayoutPanel7.TabIndex = 5;
            // 
            // lblDisc
            // 
            this.lblDisc.AutoSize = true;
            this.lblDisc.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblDisc.Location = new System.Drawing.Point(352, 0);
            this.lblDisc.Name = "lblDisc";
            this.lblDisc.Size = new System.Drawing.Size(39, 24);
            this.lblDisc.TabIndex = 1;
            this.lblDisc.Text = "0.00";
            // 
            // txtDiscPer
            // 
            this.txtDiscPer.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtDiscPer.Dock = System.Windows.Forms.DockStyle.Left;
            this.txtDiscPer.Location = new System.Drawing.Point(3, 3);
            this.txtDiscPer.Name = "txtDiscPer";
            this.txtDiscPer.ReadOnly = true;
            this.txtDiscPer.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtDiscPer.Size = new System.Drawing.Size(114, 26);
            this.txtDiscPer.TabIndex = 2;
            this.txtDiscPer.Text = "0.00";
            this.txtDiscPer.TextChanged += new System.EventHandler(this.txtDiscPer_TextChanged);
            // 
            // lnkDiscount
            // 
            this.lnkDiscount.AutoSize = true;
            this.lnkDiscount.Location = new System.Drawing.Point(3, 253);
            this.lnkDiscount.Name = "lnkDiscount";
            this.lnkDiscount.Size = new System.Drawing.Size(89, 18);
            this.lnkDiscount.TabIndex = 6;
            this.lnkDiscount.TabStop = true;
            this.lnkDiscount.Text = "Discount %";
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 3;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.63291F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.36709F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 296F));
            this.tableLayoutPanel8.Controls.Add(this.lblBillAmount, 2, 0);
            this.tableLayoutPanel8.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.lblRoffValue, 1, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(101, 286);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(394, 23);
            this.tableLayoutPanel8.TabIndex = 7;
            // 
            // lblBillAmount
            // 
            this.lblBillAmount.AutoSize = true;
            this.lblBillAmount.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblBillAmount.Location = new System.Drawing.Point(282, 0);
            this.lblBillAmount.Name = "lblBillAmount";
            this.lblBillAmount.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblBillAmount.Size = new System.Drawing.Size(109, 23);
            this.lblBillAmount.TabIndex = 3;
            this.lblBillAmount.Text = "BILL AMOUNT";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 23);
            this.label4.TabIndex = 4;
            this.label4.Text = "Round Off";
            // 
            // lblRoffValue
            // 
            this.lblRoffValue.AutoSize = true;
            this.lblRoffValue.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblRoffValue.Location = new System.Drawing.Point(55, 0);
            this.lblRoffValue.Name = "lblRoffValue";
            this.lblRoffValue.Size = new System.Drawing.Size(39, 23);
            this.lblRoffValue.TabIndex = 5;
            this.lblRoffValue.Text = "0.00";
            // 
            // lblBreakup
            // 
            this.lblBreakup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.lblBreakup.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBreakup.Location = new System.Drawing.Point(110, 320);
            this.lblBreakup.Name = "lblBreakup";
            this.lblBreakup.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblBreakup.Size = new System.Drawing.Size(321, 0);
            this.lblBreakup.TabIndex = 3;
            this.lblBreakup.Visible = false;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 36.24535F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 63.75465F));
            this.tableLayoutPanel6.Controls.Add(this.lblInternalNoMessage, 1, 0);
            this.tableLayoutPanel6.ForeColor = System.Drawing.Color.Red;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(729, 500);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(538, 15);
            this.tableLayoutPanel6.TabIndex = 32;
            // 
            // lblInternalNoMessage
            // 
            this.lblInternalNoMessage.AutoSize = true;
            this.lblInternalNoMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInternalNoMessage.Location = new System.Drawing.Point(197, 0);
            this.lblInternalNoMessage.Name = "lblInternalNoMessage";
            this.lblInternalNoMessage.Size = new System.Drawing.Size(41, 13);
            this.lblInternalNoMessage.TabIndex = 2;
            this.lblInternalNoMessage.Text = "label4";
            // 
            // tbLayoutItemGroup
            // 
            this.tbLayoutItemGroup.ColumnCount = 3;
            this.tbLayoutItemGroup.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.576891F));
            this.tbLayoutItemGroup.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 97.42311F));
            this.tbLayoutItemGroup.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tbLayoutItemGroup.Controls.Add(this.gbItemGroup, 0, 0);
            this.tbLayoutItemGroup.Controls.Add(this.btnLeft, 0, 0);
            this.tbLayoutItemGroup.Controls.Add(this.btnRight, 2, 0);
            this.tbLayoutItemGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.tbLayoutItemGroup.Location = new System.Drawing.Point(0, 105);
            this.tbLayoutItemGroup.Name = "tbLayoutItemGroup";
            this.tbLayoutItemGroup.RowCount = 1;
            this.tbLayoutItemGroup.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tbLayoutItemGroup.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tbLayoutItemGroup.Size = new System.Drawing.Size(1270, 49);
            this.tbLayoutItemGroup.TabIndex = 31;
            // 
            // gbItemGroup
            // 
            this.gbItemGroup.BackgroundColor = System.Drawing.Color.White;
            this.gbItemGroup.BackgroundGradientColor = System.Drawing.Color.White;
            this.gbItemGroup.BackgroundGradientMode = CodeVendor.Controls.Grouper.GroupBoxGradientMode.None;
            this.gbItemGroup.BorderColor = System.Drawing.Color.Transparent;
            this.gbItemGroup.BorderThickness = 1F;
            this.gbItemGroup.CustomGroupBoxColor = System.Drawing.Color.White;
            this.gbItemGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbItemGroup.GroupImage = null;
            this.gbItemGroup.GroupTitle = "";
            this.gbItemGroup.Location = new System.Drawing.Point(34, 3);
            this.gbItemGroup.Name = "gbItemGroup";
            this.gbItemGroup.Padding = new System.Windows.Forms.Padding(20);
            this.gbItemGroup.PaintGroupBox = false;
            this.gbItemGroup.RoundCorners = 10;
            this.gbItemGroup.ShadowColor = System.Drawing.Color.DarkGray;
            this.gbItemGroup.ShadowControl = false;
            this.gbItemGroup.ShadowThickness = 3;
            this.gbItemGroup.Size = new System.Drawing.Size(1196, 43);
            this.gbItemGroup.TabIndex = 17;
            // 
            // btnLeft
            // 
            this.btnLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnLeft.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLeft.Image = ((System.Drawing.Image)(resources.GetObject("btnLeft.Image")));
            this.btnLeft.Location = new System.Drawing.Point(3, 3);
            this.btnLeft.Name = "btnLeft";
            this.btnLeft.Size = new System.Drawing.Size(25, 43);
            this.btnLeft.TabIndex = 1;
            this.btnLeft.UseVisualStyleBackColor = true;
            this.btnLeft.Click += new System.EventHandler(this.btnLeft_Click);
            // 
            // btnRight
            // 
            this.btnRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnRight.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRight.Image = ((System.Drawing.Image)(resources.GetObject("btnRight.Image")));
            this.btnRight.Location = new System.Drawing.Point(1240, 3);
            this.btnRight.Name = "btnRight";
            this.btnRight.Size = new System.Drawing.Size(27, 43);
            this.btnRight.TabIndex = 2;
            this.btnRight.UseVisualStyleBackColor = true;
            this.btnRight.Click += new System.EventHandler(this.btnRight_Click);
            // 
            // POSInvoice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1270, 708);
            this.ControlBox = false;
            this.Controls.Add(this.tableLayoutPanel3);
            this.Controls.Add(this.tbLayoutItemGroup);
            this.Controls.Add(this.gbMenu);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "POSInvoice";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.POSInvoice_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.gbMenu.ResumeLayout(false);
            this.tplPersonalInfo.ResumeLayout(false);
            this.tplPersonalInfo.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.gbItems.ResumeLayout(false);
            this.grpInvoice.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvgInvoice)).EndInit();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tbLayoutItemGroup.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer tmrUserCard;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lblExit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblInvoiceHeading;
        private System.Windows.Forms.TableLayoutPanel gbMenu;
        private System.Windows.Forms.Button btnCashMemo;
        private System.Windows.Forms.Button btnClearItems;
        private System.Windows.Forms.Button btnRefund;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label lblToday;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Button btn5;
        private System.Windows.Forms.Button btn0;
        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.Button btn3;
        private System.Windows.Forms.Button btn4;
        private System.Windows.Forms.Button btn2;
        private System.Windows.Forms.Button btn00;
        private System.Windows.Forms.Button btnback;
        private System.Windows.Forms.Button btn9;
        private System.Windows.Forms.Button btn8;
        private System.Windows.Forms.Button btn7;
        private System.Windows.Forms.Button btn6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private CodeVendor.Controls.Grouper gbItems;
        private System.Windows.Forms.Panel pnlItems;
        private CodeVendor.Controls.Grouper grpInvoice;
        private System.Windows.Forms.Label lblBreakup;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnLeft;
        private System.Windows.Forms.TableLayoutPanel tbLayoutItemGroup;
        private CodeVendor.Controls.Grouper gbItemGroup;
        private System.Windows.Forms.Button btnRight;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.DataGridView dvgInvoice;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Label lblInternalNoMessage;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Label lblDisc;
        private System.Windows.Forms.TextBox txtDiscPer;
        private System.Windows.Forms.LinkLabel lnkDiscount;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.Label lblBillAmount;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblRoffValue;
        private System.Windows.Forms.Button btnItemSummary;
        private System.Windows.Forms.Button btnReprint;
        private System.Windows.Forms.Button btnItemMenu;
        private CodeVendor.Controls.Grouper gbHotItems;
        private System.Windows.Forms.TableLayoutPanel tplPersonalInfo;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtMobileNo;
        private System.Windows.Forms.LinkLabel lblUserName;
    }
}