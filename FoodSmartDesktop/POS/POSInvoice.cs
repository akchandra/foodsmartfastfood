﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.Windows.Forms;
using System.Linq;
using System.Xml.Linq;
using Microsoft.CSharp;
using System.Drawing.Text;
using System.Data.OleDb;
using System.Configuration;
using System.Drawing.Drawing2D;
using System.Text;
using FoodSmart.Entity;
using FoodSmart.BLL;
using FoodSmart.Common;
using FoodSmart.Utilities;
using FoodSmart.Utilities.Cryptography;
using FoodSmart.Utilities.ResourceManager;
using FoodSmart.Utilities.FlexiMessage.Forms;
using FoodSmart.Utilities.InputBox.Forms;

namespace FoodSmartDesktop.POS
{
    public partial class POSInvoice : Form
    {
        # region Public Variable
        string ServiceTaxNo;
        string GSTNo;
        string CalculationMethod;
        //decimal ServiceTaxPer;
        string BtmLine1;
        string BtmLine2;
        string BtmLine3;
        string BtmLine4;
        bool PrintBill;
        string PrinterName;
        string PrinterType;
        //string KOTPrinterName;
        //string KOTPrinterType;
        //int RestID;
        
        bool gridInitialized;
        string path = Application.StartupPath + "\\";
        string bType = "B";
        bool GotCardDetails = false;

        private DataSet ds;
        private DataTable dtItemGrp;
        private DataTable dtHotItems;
        bool ftime = true;
        decimal cAmount;

        public string CreditCardNo { get; set; }
        public int MealPassID { get; set; }
        public decimal PaymentAmount;
        public decimal PaymentCash;
        public decimal PaymentCC;
        public decimal PaymentWallet;
        public decimal PaymentMP;
        public decimal PaymentCoupon;
        public string PMode;
        public string Prefix;
        public int CouponNo;
        public int CouponID;

        bool OpenMenu = false;
        //int CurrGroup = 0;

        //ICard card;
        IRestaurant rest;
        ILocation Locn;

        private StringBuilder trackInfo = new StringBuilder();
        private StringBuilder IgnoreInfo = new StringBuilder();
        //private bool track1Complete = false;

        //int g_retCode;
        int CardAction;
        //string vData = "";
        //string vData1 = "";
        //string vData2 = "";
        //TextBox activeTextBox;
        int fk_Cardid;
        BindingSource source1 = new BindingSource();
        string keyAmt;

        int i = 0;
        public int x = 0;
        int k = 0;
        public int RefReasonID { get; set; }
        public int DisReasonID { get; set; }
        public decimal DiscPer { get; set; }
        public int InvoiceID;
        #endregion

        Button btnLastSelectedGrp;

        #region public modules
        public POSInvoice()
        {
            InitializeComponent();
        }

        private void POSInvoice_Load(object sender, EventArgs e)
        {
            dvgInvoice.ColumnCount = 14;
            tbLayoutItemGroup.Enabled = false;
            gbHotItems.Enabled = false;
            txtMobileNo.MaxLength = 10;
            lblInvoiceHeading.Text = GeneralFunctions.LoginInfo.LocName;
            txtName.CharacterCasing = CharacterCasing.Upper;
            //lblToday.Text += " : " + GeneralFunctions.LoginInfo.CurrDate;
            lblUserName.Text += GeneralFunctions.LoginInfo.UserFullName;
            lblToday.Text += " : " + GeneralFunctions.LoginInfo.CurrDate.ToString("dd-MMM-yyyy");
            lblInternalNoMessage.Text = "";
            lnkDiscount.Enabled = false;

            FoodSmart.BLL.DBInteraction dbinteract = new FoodSmart.BLL.DBInteraction();
            System.Data.DataSet ds = dbinteract.GetCompany();

            if (ds.Tables[0].Rows[0]["Company_Name"].ToString() != null)
            {
                CalculationMethod = ds.Tables[0].Rows[0]["CalculationMethod"].ToString();
            }
            Locn = new LocationBLL().GetLocData(GeneralFunctions.LoginInfo.LocID);
            //rest = new RestaurantBLL().GetRestData(GeneralFunctions.LoginInfo.RestID);
            if (Locn != null)
            {
                if (Locn.LocName != null)
                {
                    //ServiceTaxNo = Locn.ServTaxNo;
                    GSTNo = Locn.GSTNo;
                    //CalculationMethod = Locn.CalculationMethod;
                    //ServiceTaxPer = Locn.ServTaxPer;
                    PrintBill = Locn.PrintBill;
                    PrinterName = Locn.PrinterName;
                    PrinterType = Locn.printerType;

                    BtmLine1 = Locn.btmLine1;
                    BtmLine2 = Locn.btmLine2;
                    BtmLine3 = Locn.btmLine3;
                    BtmLine4 = Locn.btmLine4;
                    //BtmLine5 = ds.Tables[0].Rows[0]["btmLine5"].ToString();
                    //KOTPrinterName = rest.KOTPrinterName;
                    //KOTPrinterType = rest.KOTPrinterType;
                }
            }

            tableLayoutPanel5.Enabled = true;
            EnableDisableNum(1);

            SearchCriteria searchCriteria = new SearchCriteria();
            //searchCriteria.IntegerOption1 = GeneralFunctions.LoginInfo.RestID;
            searchCriteria.IntegerOption2 = GeneralFunctions.LoginInfo.LocID;
            searchCriteria.RorF = "F";
            //searchCriteria.RestName = GeneralFunctions.LoginInfo.RestName;
            //searchCriteria.LocName = GeneralFunctions.LoginInfo.LocName;
            searchCriteria.BillType = "B";
            bType = "B";
            //RestID = GeneralFunctions.LoginInfo.RestID;
            dvgInvoice.DataSource = null;

            ItemBLL itembll = new ItemBLL();
            dtItemGrp = itembll.GetAllItemGroup(searchCriteria).Tables[0];
            createItemGrpButton(dtItemGrp, 0);

            searchCriteria.Date = GeneralFunctions.LoginInfo.CurrDate;

            dtHotItems = itembll.GetHotItems(searchCriteria).Tables[0];

            CreateHotItemButton(dtHotItems);
            InitializeBills();
            EnableDisableControl(0);
            txtMobileNo.Focus();
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            FlexibleMessageBox.FONT = new Font("Arial Black", 12, FontStyle.Bold);
            //FlexibleMessageBox.Show("Record Saved. Please Remove Card ", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information);
            if (FlexibleMessageBox.Show("Want to Leave Invoice Module", "Exit Invoice", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                //FoodSmart.BLL.CardDetails.CardEntity card = new FoodSmart.BLL.CardDetails.CardEntity();
                //if (!string.IsNullOrEmpty(lblCardNo.Text))
                //{
                //    card.CardNoInside = lblCardNo.Text.Substring(0,5);
                //        ////card.UserID = GeneralFunctions.LoginInfo.UserID;
                //        ////card.LocID = GeneralFunctions.LoginInfo.LocID;
                //        ////new CardBLL().SavePOSUserLogin(card, "O");
                //    new InvoiceBLL().UpdateTempCardStatus(card.CardNoInside, false);
                //}
                new InvoiceBLL().SavePOSUserLogin(GeneralFunctions.LoginInfo.UserID);
                System.Environment.Exit(0);
            }
        }

        private void btnItemMenu_Click(object sender, EventArgs e)
        {
            if (!OpenMenu)
            {
                //gbNumbers.Enabled = true;
                gbHotItems.Enabled = true;
                gbItemGroup.Enabled = true;
                gbItems.Enabled = true;
                //txtAcceptCardNo.Enabled = false;
                gbMenu.Enabled = true;
                EnableDisableNumber("E");
                btnItemMenu.Text = "Close Menu";
                btnCashMemo.Enabled = false;
                OpenMenu = true;
            }
            else
            {
                OpenMenu = false;
                Initialize();
                InitializeBills();
                clearItemButton();
                EnableDisableControl(0);
                grpInvoice.GroupTitle = "INVOICE";
                GotCardDetails = true;
                //txtAcceptCardNo.Enabled = true;
                //txtAcceptCardNo.Text = "";
                btnCashMemo.Enabled = true;
                btnItemMenu.Text = "Open Menu";
                //txtAcceptCardNo.Focus();
            }
        }

        private void btnItemGrp_Click(object sender, EventArgs e)
        {
            // (sender as Button).Tag.ToString()

            foreach (object ctl in gbItemGroup.Controls)
            {
                if ((ctl as Button).Name != "btnLeft" && (ctl as Button).Name != "btnRight" && (sender as Button).Name != (ctl as Button).Name)
                {
                    (ctl as Button).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(86)))), ((int)(((byte)(125)))));
                }
            }
            (sender as Button).BackColor = System.Drawing.Color.Maroon;
            btnLastSelectedGrp = (Button)sender;
            clearItemButton();
            ItemBLL itembll = new ItemBLL();
            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.RorF = "F";
            //searchCriteria.RestName = 
            searchCriteria.IntegerOption3 = int.Parse((sender as Button).Tag.ToString());
            //searchCriteria.IntegerOption1 = GeneralFunctions.LoginInfo.RestID;
            //searchCriteria.IntegerOption2 = GeneralFunctions.LoginInfo.LocID;
            DataTable dtItem = itembll.GetAllItems(searchCriteria).Tables[0];
            createItemButton(dtItem);
        }
        #endregion

        #region Mouse move

        private void lblExit_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void lblExit_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnCashMemo_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnCashMemo_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnCancelCard_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnCancelCard_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnClearItems_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnClearItems_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnRefund_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnRefund_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        #endregion

        private void InitializeBills()
        {
            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.BillID = 0;
            searchCriteria.BillType = "B";
            btnCashMemo.Enabled = true;

            bType = "B";
            if (ftime)
                ftime = false;
            else
            {
                dvgInvoice.Columns.Remove("Dimg");
                //dvgInvoice.Columns.Remove("Eimg");
            }
            LoadBills(searchCriteria);
        }

        private void EnableDisableNum(int type)
        {
            if (type == 1)
                EnableDisableNumber("E");
            else
                EnableDisableNumber("D");
        }

        private void EnableDisableControl(int Type)
        {
            if (Type == 1)
            {

                //gbNumbers.Enabled = true;
                gbHotItems.Enabled = true;
                gbItemGroup.Enabled = true;
                gbItems.Enabled = true;

                btnCashMemo.Enabled = true;
                btnRefund.Enabled = true;
                btnReprint.Enabled = true;
                btnClearItems.Enabled = false;
                btnItemMenu.Enabled = false;
                btnItemSummary.Enabled = false;
                lnkDiscount.Enabled = true;
                tbLayoutItemGroup.Enabled = true;
                //EnableDisableNumber("E");
            }
            else
            {
                //gbNumbers.Enabled = false;
                //gbHotItems.Enabled = false;
                //gbItemGroup.Enabled = false;
                //gbItems.Enabled = false;
                tbLayoutItemGroup.Enabled = false;
                gbMenu.Enabled = true;
                btnCashMemo.Enabled = true;
                btnReprint.Enabled = true;

                btnClearItems.Enabled = false;
                btnRefund.Enabled = true;
                btnItemMenu.Enabled = true;
                btnItemSummary.Enabled = true;
                lnkDiscount.Enabled = false;
                //EnableDisableNumber("D");
                CardAction = 0;
            }
        }

        private void Initialize()
        {
            this.txtDiscPer.TextChanged -= new System.EventHandler(this.txtDiscPer_TextChanged);
            //lblCardNo.Text = "";
            /*
            lblOpening.Text = "";
            lblCurrentBalance.Text = "";
            lblClosing.Text = "";
            */
            lblBillAmount.Text = "BILL AMOUNT";
            txtName.Text = string.Empty;
            txtMobileNo.Text = string.Empty;
            txtName.Enabled = true;
            lblDisc.Text = "";
            txtDiscPer.Text = "0.00";
            DisReasonID = 0;
            DiscPer = 0;
            RefReasonID = 0;
            lblRoffValue.Text = "";
            grpInvoice.GroupTitle = "INVOICE";
            dvgInvoice.DataSource = null;
            gridInitialized = false;
            lblBreakup.Text = "";
            tbLayoutItemGroup.Enabled = false;
            this.txtDiscPer.TextChanged += new System.EventHandler(this.txtDiscPer_TextChanged);
        }

        private void EnableDisableNumber(string stat)
        {
            if (stat == "D")
            {
                btn0.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-inactive.png");
                btn1.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-inactive.png");
                btn2.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-inactive.png");
                btn3.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-inactive.png");
                btn4.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-inactive.png");
                btn5.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-inactive.png");
                btn6.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-inactive.png");
                btn7.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-inactive.png");
                btn8.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-inactive.png");
                btn9.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-inactive.png");
                btn00.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-inactive.png");
                btnback.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-inactive.png");
            }
            else
            {
                btn0.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-active.png");
                btn1.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-active.png");
                btn2.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-active.png");
                btn3.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-active.png");
                btn4.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-active.png");
                btn5.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-active.png");
                btn6.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-active.png");
                btn7.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-active.png");
                btn8.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-active.png");
                btn9.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-active.png");
                btn00.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-active.png");
                btnback.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-active.png");
            }
        }
     
        private void createItemGrpButton(DataTable dt, int nextNum)
        {

            int prvheight;
            int prvWidth;
            float Fact;
            //int btnwidth;

            Size desktopSize = System.Windows.Forms.SystemInformation.PrimaryMonitorSize;
            prvheight = desktopSize.Height;
            prvWidth = desktopSize.Width;
            Fact = prvWidth / 1366f;

            //This block dynamically creates a Button and adds it to the form
            i = 0;
            k = nextNum;
            int cnt=0;
            for (; k < dt.Rows.Count; k++)
            {
                if (!string.IsNullOrEmpty(dt.Rows[k]["descr"].ToString()))
                {
                    Button btn = new Button();
                    btn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(86)))), ((int)(((byte)(125)))));
                    btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
                    btn.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    btn.ForeColor = System.Drawing.SystemColors.HighlightText;
                    btn.Location = new Point(i, 0);
                    btn.Name = "btnItemGrp" + k;
                    btn.Size = new System.Drawing.Size((160 * Fact).ToInt(), 44);
                    //btn.TabIndex = 6;
                    btn.Text = dt.Rows[k]["descr"].ToString();
                    btn.Tag = dt.Rows[k]["pk_itemgroupid"].ToString();
                    btn.UseVisualStyleBackColor = false;
                    //btn.BackgroundImage = Image.FromFile(path + ConfigurationManager.AppSettings["imageFolder"] + "itemgroup-btn-inactive.png");

                    //Hook our button up to our generic button handler
                    btn.Click += new EventHandler(btnItemGrp_Click);
                    gbItemGroup.Controls.Add(btn);
                    //i += 164; 
                    i += (162 * Fact).ToInt();
                    cnt++;
                }
                if (cnt == 8)
                {
                    break;
                }
            }
        }


        private void clearItemButton()
        {
            for (int totCnt = 0; totCnt < pnlItems.Controls.Count; totCnt++)
            {
                pnlItems.Controls.Clear();
            }
        }

        private void createItemButton(DataTable dt)
        {
            //This block dynamically creates a Button and adds it to the form
            int prvheight;
            int prvWidth;
            float Fact;
            int btnwidth;
            int iX = 3;
            int iY = 3;

            Size desktopSize = System.Windows.Forms.SystemInformation.PrimaryMonitorSize;
            prvheight = desktopSize.Height;
            prvWidth = desktopSize.Width;
            Fact = prvWidth / 1366f;
            //perY = prvheight / 768;
            if (Fact != 1)
            {
                lblBreakup.Visible = false;
                btnwidth = 119;
            }
            else
            {
                btnwidth = 110;
                iX = 5;
                iY = 3;
            }

           
            int cnt = 0;
            for (int totCnt = 0; totCnt < dt.Rows.Count; totCnt++)
            {
                if (Convert.ToDecimal(dt.Rows[totCnt]["SellingRate"]) > 0)
                {
                    Button btn = new Button();
                    btn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(86)))), ((int)(((byte)(125)))));
                    btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
                    btn.Font = new System.Drawing.Font("Calibri", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    btn.ForeColor = System.Drawing.SystemColors.HighlightText;
                    btn.Location = new Point(iX, iY);
                    btn.Name = "btnItem" + cnt;
                    btn.Size = new System.Drawing.Size((btnwidth * Fact).ToInt(), 58);
                    //btn.TabIndex = 6;
                    if (dt.Rows[totCnt]["TString"].ToString().Length > 0)
                        btn.Text = dt.Rows[totCnt]["ItemDescr"].ToString() + " Rs." + dt.Rows[totCnt]["TString"].ToString().Split('^')[1];
                    btn.Tag = dt.Rows[totCnt]["TString"].ToString();
                    btn.UseVisualStyleBackColor = false;

                    //Hook our button up to our generic button handler
                    btn.Click += new EventHandler(btnItem_Click);
                    pnlItems.Controls.Add(btn);
                    if (Fact != 1)
                    {
                        iX += (121 * Fact).ToInt();
                    }
                    else
                    {
                        iX += (112 * Fact).ToInt();
                    }
                    //totCnt++;
                    cnt++;
                }
                if (cnt == 6)
                {
                    iX = 4;
                    iY += 60;
                    cnt = 0;
                }
            }
        }

        private void CreateHotItemButton(DataTable dt)
        {
            int prvheight;
            int prvWidth;
            float Fact;
            //int btnwidth;

            Size desktopSize = System.Windows.Forms.SystemInformation.PrimaryMonitorSize;
            prvheight = desktopSize.Height;
            prvWidth = desktopSize.Width;
            Fact = prvWidth / 1366f;

            //This block dynamically creates a Button and adds it to the form
            for (int j = 0; j <= 5; j++)
            {
                gbItemGroup.Controls.RemoveByKey("btnHItem" + j);
            }
            int iX = 10;
            int iY = 27;
            int cnt = 0;
            for (int totCnt = 0; totCnt < dt.Rows.Count; totCnt++)
            {
                Button btn = new Button();
                btn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(86)))), ((int)(((byte)(125)))));
                btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
                btn.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                btn.ForeColor = System.Drawing.SystemColors.HighlightText;
                btn.Location = new Point(iX, iY);
                btn.Name = "btnItem" + cnt;
                btn.Size = new System.Drawing.Size((135 * Fact).ToInt(), 60);
                btn.BackgroundImage = Image.FromFile(path + ConfigurationManager.AppSettings["imageFolder"] + "hotitems-btn-active.png");
                //btn.TabIndex = 6;
                btn.Text = dt.Rows[totCnt]["ItemDescr"].ToString() + " Rs." + dt.Rows[totCnt]["TString"].ToString().Split('^')[1];
                btn.Tag = dt.Rows[totCnt]["TString"].ToString();
                btn.UseVisualStyleBackColor = false;

                //Hook our button up to our generic button handler
                btn.Click += new EventHandler(btnItem_Click);
                gbHotItems.Controls.Add(btn);
                iX += (137 * Fact).ToInt();
                
            }
        }
        
        private void btnItem_Click(object sender, EventArgs e)
        {
            string[] result;
            string[] stringSeparators = new string[] { " Rs." };
            //decimal itemNetAmt;
            DataSet dsRefund;
            int cQty;
            //decimal cAmount;
            decimal tempAmt=0;
            decimal Taxes;
            string STApplicable;
            DataTable dtTempItem;
            decimal tempRoff;
            bool ItemFound = false;
            //DataRow dtTempRow;

            foreach (object ctl in pnlItems.Controls)
            {
                    (ctl as Button).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(86)))), ((int)(((byte)(125)))));
            }

            string[] arrItemDtl = (sender as Button).Tag.ToString().Split('^');

            (sender as Button).BackColor = System.Drawing.Color.Maroon;
            DataRow[] drCOLL = ds.Tables[0].Select("fk_ItemID = " + int.Parse(arrItemDtl[0].ToString()));
            DataRow dr;
            
            if (drCOLL.Length == 0)
            {
                dr = ds.Tables[0].NewRow();
                dr["fk_ItemID"] = int.Parse(arrItemDtl[0].ToString());
                result = (sender as Button).Text.Split(stringSeparators, StringSplitOptions.None);
                dr["itemdescr"] = result[0]; // (sender as Button).Text;
                dr["Qty"] = 0;
                ItemFound = false;
            }
            else
            {
                dr = drCOLL[0];
                ItemFound = true;
            }
            if (!String.IsNullOrEmpty(keyAmt))
            {
                dr["Qty"] = int.Parse(dr["Qty"].ToString()) + (int.Parse(keyAmt) == 0 ? 1:int.Parse(keyAmt));
                cQty = int.Parse(keyAmt);
            }
            else
            {
                dr["Qty"] = int.Parse(dr["Qty"].ToString()) + 1;
                cQty = 1;
            }

            dr["GSTPer"] = decimal.Parse(arrItemDtl[3].ToString()) + decimal.Parse(arrItemDtl[4].ToString());
            dr["salerate"] = decimal.Parse(arrItemDtl[1].ToString());
            dr["SGSTPer"] = decimal.Parse(arrItemDtl[4].ToString());
            dr["CGSTPer"] = decimal.Parse(arrItemDtl[3].ToString());
            dr["fk_RateID"] = int.Parse(arrItemDtl[7].ToString());
            STApplicable = arrItemDtl[2].ToString();


            CalculateAmount(GeneralFunctions.LoginInfo.CalcMethod, dr, dr["SaleRate"].ToDecimal(), dr["Qty"].ToDecimal(), dr["CGSTPer"].ToDecimal(), dr["SGSTPer"].ToDecimal());

            //cAmount = lblBillAmount.ToDecimal();
            
            keyAmt = "";

            //if (bType == "R")
            //{
            //    InvoiceBLL invBill = new InvoiceBLL();
            //    dsRefund = invBill.CheckItemRefundPossible(card.CardID, card.CurrentSerial, dr["fk_ItemID"].ToInt(), RestID, GeneralFunctions.LoginInfo.CurrDate);
            //    if (dsRefund.Tables[0].Rows[0]["Qty"].ToInt() != 0)
            //    {
            //        if (dsRefund.Tables[0].Rows[0]["Qty"].ToInt() < dr["Qty"].ToInt())
            //        {
            //            FlexibleMessageBox.Show("Items Sold should be Greater or Equal to Refund", "Invalid Quantity", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //            ds.Tables[0].RejectChanges();
            //            return;
            //        }
            //    }
            //    else
            //    {
            //        FlexibleMessageBox.Show("No Sale. Refund not possible", "Invalid Quantity", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //        ds.Tables[0].RejectChanges();
            //        return;
            //    }
            //}
            if (!OpenMenu)
            {

                dtTempItem = ds.Tables[0];

                for (int i = 0; i < dtTempItem.Rows.Count; i++)
                {
                    DataRow dtTempRow = dtTempItem.Rows[i];
                    tempAmt += dtTempRow["TotalAmount"].ToDecimal();
                }
                if (!ItemFound)
                    tempAmt = tempAmt + cAmount;

                lblRoffValue.Text = "0.00";
                tempAmt = Math.Round(tempAmt, 2);
                //if (amt - Math.Round(amt, 0) != 0)
                if (tempAmt.ToString().Split('.').Length == 2)
                {
                    //roff = Math.Round(amt.ToString().Split('.')[1].Substring(0, amt.ToString().Split('.')[1].Length).ToDecimal()/100, 2);
                    tempRoff = Math.Round(tempAmt - tempAmt.ToInt(), 2);

                    if (tempRoff >= (.50).ToDecimal())
                    {
                        tempRoff = Math.Round(1 - tempRoff, 2);
                        tempAmt += tempRoff;
                    }
                    else
                    {
                        tempAmt -= tempRoff;
                        tempRoff = tempRoff * (-1);
                    }
                    lblRoffValue.Text = tempRoff.ToString("##0.00");
                }
                if (drCOLL.Length == 0)
                {
                    ds.Tables[0].Rows.Add(dr);
                }
                ds.Tables[0].AcceptChanges();
                CalcPayableAmt(ds.Tables[0]);

                /*
                if ((decimal.Parse(lblOpening.Text) - tempAmt >= 0) || bType == "R")
                //if ((decimal.Parse(lblOpening.Text) - Math.Round((decimal.Parse(lblCurrentBalance.Text) + cAmount), 0) >= 0) || bType == "R")
                {
                    if (drCOLL.Length == 0)
                    {
                        ds.Tables[0].Rows.Add(dr);
                    }
                    ds.Tables[0].AcceptChanges();
                    CalcPayableAmt(ds.Tables[0]);
                }
                else
                {
                    if (drCOLL.Length > 0)
                    {
                        ds.Tables[0].RejectChanges();
                    }
                    MessageBox.Show("No Sufficient Balanace Available!!!");
                }
                */
                if (bType == "B" && ds.Tables[0].Rows.Count > 0)
                {
                    btnRefund.Enabled = false;
                    btnReprint.Enabled = false;
                    btnClearItems.Enabled = true;
                }
                else
                    btnClearItems.Enabled = false;
            }
            else
            {
                if (drCOLL.Length == 0)
                {
                    ds.Tables[0].Rows.Add(dr);
                }
                ds.Tables[0].AcceptChanges();
                CalcPayableAmt(ds.Tables[0]);
            }
        }

        private void btnLeft_Click(object sender, EventArgs e)
        {
            if (k > 7)
            {
                for (int j = k - 7; j <= k; j++)
                {
                    gbItemGroup.Controls.RemoveByKey("btnItemGrp" + j);
                }
                k = k - 8;
                createItemGrpButton(dtItemGrp, k);
            }
        }

        private void btnRight_Click(object sender, EventArgs e)
        {
            if (dtItemGrp.Rows.Count > k + 1)
            {
                for (int j = k - 7; j <= k; j++)
                {
                    gbItemGroup.Controls.RemoveByKey("btnItemGrp" + j);
                }
                k = k - 6;
                createItemGrpButton(dtItemGrp, k);
            }
        }
      
        private void btnAmt_Click(object sender, EventArgs e)
        {
            keyAmt += (sender as Button).Tag.ToString();
        }

        private void CalcPayableAmt(DataTable dtItem)
        {
            decimal amt = 0.00m;
            decimal CGST = 0;
            decimal SGST = 0;
            decimal bas = 0;
            decimal roff = 0;
            decimal Disc = 0;

            for (int i = 0; i < dtItem.Rows.Count; i++)
            {
                DataRow dr = dtItem.Rows[i];
                bas += dr["TotalAmount"].ToDecimal() - dr["CGSTAmt"].ToDecimal() - dr["SGSTAmt"].ToDecimal();
                amt += Math.Round((Math.Round(dr["TotalAmount"].ToDecimal(), 2)),2); // + dr["VATAmount"].ToDecimal() + dr["ServiceTaxAmount"].ToDecimal() + dr["SBCAmount"].ToDecimal();

                amt = Convert.ToDecimal(string.Format("{0:0.##}", amt));
                CGST += dr["CGSTAmt"].ToDecimal();
                SGST += dr["SGSTAmt"].ToDecimal();
                Disc += dr["DiscountAmount"].ToDecimal();
            }
            lblRoffValue.Text = "0.00";
            amt = Math.Round(amt, 2);
            //if (amt - Math.Round(amt, 0) != 0)
            if (amt.ToString().Split('.').Length == 2)
            {
                //roff = Math.Round(amt.ToString().Split('.')[1].Substring(0, amt.ToString().Split('.')[1].Length).ToDecimal()/100, 2);
                roff = Math.Round(amt - amt.ToInt(), 2);

                if (roff >= (.50).ToDecimal())
                {
                    roff = Math.Round(1 - roff, 2);
                    amt += roff;
                }
                else
                {
                    amt -= roff;
                    roff = roff * (-1);
                }
                lblRoffValue.Text = roff.ToString("##0.00");
            }

            lblBillAmount.Text = amt.ToString("####0.00");
            lblBreakup.Text = "(Basic: " + bas.ToString("#####0.00") + " CGST: " + CGST.ToString("#####0.00") + " SGST: " + SGST.ToString("#####0.00") + " R.Off: "+ roff.ToString("0.00");
            /*
            lblBillAmount.Text = lblCurrentBalance.Text;
            if (bType == "B")
            {
                lblClosing.Text = (lblOpening.Text.ToDecimal() - lblBillAmount.Text.ToDecimal()).ToString();
            }
            else
            {
                lblClosing.Text = (lblOpening.Text.ToDecimal() + lblBillAmount.Text.ToDecimal()).ToString();
            }
            */ 
        }

        #region "Grid Methods"

        private void btnCashMemo_Click(object sender, EventArgs e)
        {
            //DialogResult yn;
            FlexibleMessageBox.FONT = new Font("Arial Black", 12, FontStyle.Italic);
            //string PrinterType = ConfigurationManager.AppSettings["PrinterType"];
            if (ds.Tables[0].Rows.Count > 0)
            {
                PaymentAmount = lblBillAmount.Text.ToDecimal();
                Form lightbox = new POS.Payment(this);
                lightbox.Owner = this;
                this.Opacity = 1;
                lightbox.ShowDialog();
                //if (string.IsNullOrEmpty(PaymentAmount.ToString()) || PaymentAmount == 0)
                //    return;
                if (x == -1)
                    return;
                IInvoice inv = new FoodSmart.Entity.InvoiceEntity();
                //inv.CardID = card.CardID;
                //inv.CardNoInside = card.CardNoInside;
                //inv.BillAmount = decimal.Parse(lblCurrentBalance.Text);

                inv.BillDate =   DateTime.Now;
                inv.BillType = bType;
                inv.LocID = GeneralFunctions.LoginInfo.LocID;
                inv.PaymentMode = PMode;
                inv.RefundReasonID = RefReasonID;
                
                inv.DiscountPer = DiscPer;
                inv.DiscReasonID = DisReasonID;
                inv.MobileNo = txtMobileNo.Text;
                inv.CustName = txtName.Text;
                inv.CashAmount = PaymentCash;
                inv.CCAmount = PaymentCC;
                inv.MPAmount = PaymentMP;
                inv.eWalletAmount = PaymentWallet;
                inv.CouponAmt = PaymentCoupon;

                inv.Prefix = Prefix;
                inv.CouponID = CouponID;
                inv.CouponNo = CouponNo;

                inv.RoundOff = lblRoffValue.Text.ToDecimal();
              
                inv.YearID = GeneralFunctions.LoginInfo.CurrFinYearID;
                inv.UserID = GeneralFunctions.LoginInfo.UserID;
                InvoiceBLL invBill = new InvoiceBLL();
                int rateval = invBill.SaveInvoiceTran(inv, "A", ds);
                if (rateval > 0)
                {
                    //new InvoiceBLL().UpdateTempCardStatus(card.CardNoInside, false);
                    lblBillAmount.Text = "0.00";
                    
                    FlexibleMessageBox.Show("Record Saved.", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    if (bType == "B")
                    {
                        POS.DirectPrintInvoice printUSBkot1 = new POS.DirectPrintInvoice(bType, rateval, PrinterName, GeneralFunctions.LoginInfo.RestName, "K", "");
                        POS.DirectPrintInvoice print1 = new POS.DirectPrintInvoice(bType, rateval, PrinterName, GeneralFunctions.LoginInfo.RestName, "B", GeneralFunctions.LoginInfo.CalcMethod);


                        //if (KOTPrinterType == "U")
                        //{
                        //    POS.DirectPrintInvoice printUSBkot1 = new POS.DirectPrintInvoice(bType, rateval, KOTPrinterName, GeneralFunctions.LoginInfo.RestName, "K");
                        //    //POS.DirectPrintInvoice printUSBkot2 = new POS.DirectPrintInvoice(bType, rateval, KOTPrinterName, GeneralFunctions.LoginInfo.RestName, "K");
                        //}
                        //else
                        //{
                        //    POS.DirectPrintInvLPT printLPTkot1 = new POS.DirectPrintInvLPT(bType, rateval, KOTPrinterName, GeneralFunctions.LoginInfo.RestName, "K");
                        //    //POS.DirectPrintInvLPT printLPTkot2 = new POS.DirectPrintInvLPT(bType, rateval, KOTPrinterName, GeneralFunctions.LoginInfo.RestName, "K");
                        //}

                        //if (PrinterType == "U")
                        //{
                        //    POS.DirectPrintInvoice print1 = new POS.DirectPrintInvoice(bType, rateval, PrinterName, GeneralFunctions.LoginInfo.RestName, "B");
                        //}
                        //else
                        //{
                        //    POS.DirectPrintInvLPT print1 = new POS.DirectPrintInvLPT(bType, rateval, PrinterName, GeneralFunctions.LoginInfo.RestName, "B");
                        //}
                    }
                    else
                    {
                        POS.DirectPrintInvLPT print1 = new POS.DirectPrintInvLPT(bType, rateval, PrinterName, GeneralFunctions.LoginInfo.RestName, "B", GeneralFunctions.LoginInfo.CalcMethod);
                        //if (PrinterType == "U")
                        //{
                        //    POS.DirectPrintInvoice print1 = new POS.DirectPrintInvoice(bType, rateval, PrinterName, GeneralFunctions.LoginInfo.RestName, "B");
                        //}
                        //else
                        //{
                        //    POS.DirectPrintInvLPT print1 = new POS.DirectPrintInvLPT(bType, rateval, PrinterName, GeneralFunctions.LoginInfo.RestName, "B");
                        //}
                    }

                    //if (PrinterType == "USB")
                    //{
                    //    POS.DirectPrintInvoice print = new POS.DirectPrintInvoice(bType, rateval, lblClosing.Text.ToDecimal(), GeneralFunctions.LoginInfo.RestName, "O");
                    //}
                    //else
                    //{
                    //    POS.DirectPrintInvLPT print = new POS.DirectPrintInvLPT(bType, rateval, lblClosing.Text.ToDecimal(), GeneralFunctions.LoginInfo.RestName, "O");
                    //}
                }
                else
                {
                    FlexibleMessageBox.Show("Save Unsuccessful !!!", "Save", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                if (btnLastSelectedGrp != null)
                    btnLastSelectedGrp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(86)))), ((int)(((byte)(125)))));
                Initialize();
                InitializeBills();
                clearItemButton();
                EnableDisableControl(0);

                SearchCriteria searchCriteria = new SearchCriteria();
                searchCriteria.IntegerOption1 = GeneralFunctions.LoginInfo.RestID;
                searchCriteria.IntegerOption2 = GeneralFunctions.LoginInfo.LocID;
                searchCriteria.RestName = GeneralFunctions.LoginInfo.RestName;
                searchCriteria.LocName = GeneralFunctions.LoginInfo.LocName;
                searchCriteria.Date = GeneralFunctions.LoginInfo.CurrDate;

                ItemBLL itembll = new ItemBLL();

                dtHotItems = itembll.GetHotItems(searchCriteria).Tables[0];

                CreateHotItemButton(dtHotItems);
                grpInvoice.GroupTitle = "INVOICE";
                GotCardDetails = true;
                //txtAcceptCardNo.Enabled = true;
                //txtAcceptCardNo.Text = "";
                //txtAcceptCardNo.Focus();
            }
            else
            {
                FlexibleMessageBox.Show("No Item Is Selected To Save!!!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnRefund_Click(object sender, EventArgs e)
        {
            SearchCriteria searchCriteria = new SearchCriteria();
            grpInvoice.GroupTitle = "REFUND";
            lblBillAmount.Text = "REFUND AMOUNT";

            btnCashMemo.Enabled = false;
            if (bType == "B")
            {
                bType = "R";
                searchCriteria.BillType = "R";
                Form lightbox = new RefundMemo(this);
                lightbox.Owner = this;
                this.Opacity = 1;
                lightbox.ShowDialog();
                if (RefReasonID == 0)
                {
                    btnCashMemo.Enabled = true;
                    grpInvoice.GroupTitle = "INVOICE";
                    lblBillAmount.Text = "BILL AMOUNT";
                    bType = "B";
                }
            }
            else
            {
                btnCashMemo_Click(null, null);
                GotCardDetails = false;
            }
        }
        private void btnClearItems_Click(object sender, EventArgs e)
        {

            FlexibleMessageBox.FONT = new Font("Arial Black", 12, FontStyle.Bold);
            if (FlexibleMessageBox.Show("Want to Clear All Items", "Clear Items", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                SearchCriteria searchCriteria = new SearchCriteria();
                searchCriteria.BillID = 0;
                searchCriteria.BillType = "B";
                bType = "B";
                dvgInvoice.Columns.Remove("Dimg");
                LoadBills(searchCriteria);
                lblBillAmount.Text = "0.00";
                lblBreakup.Text = "";
                
            }
        }
        
        private void LoadBills(SearchCriteria searchCriteria)
        {
            DataGridViewCellStyle style = new DataGridViewCellStyle();
            dvgInvoice.EnableHeadersVisualStyles = false;
            style.ForeColor = Color.Blue;

            dvgInvoice.AutoGenerateColumns = false;

            dvgInvoice.Columns[0].Name = "pk_BillSrl";
            dvgInvoice.Columns[1].Name = "fk_BillID";
            dvgInvoice.Columns[2].Name = "fk_ItemID";
            dvgInvoice.Columns[3].Name = "Item Description";
            dvgInvoice.Columns[4].Name = "Qty";
            dvgInvoice.Columns[5].Name = "Sale Rate";
            dvgInvoice.Columns[6].Name = "GST%";
            dvgInvoice.Columns[7].Name = "CGST Amount";
            dvgInvoice.Columns[8].Name = "SGST Amount";
            dvgInvoice.Columns[9].Name = "Discount %";
            dvgInvoice.Columns[10].Name = "Disc Amount";
            dvgInvoice.Columns[11].Name = "Total Amount";
            dvgInvoice.Columns[12].Name = "Basic Amount";
            dvgInvoice.Columns[13].Name = "fk_RateID";

            dvgInvoice.Columns[1].HeaderText = "BillID";
            dvgInvoice.Columns[2].HeaderText = "ItemID";
            dvgInvoice.Columns[3].HeaderText = "Item Description";
            dvgInvoice.Columns[4].HeaderText = "Qty";
            dvgInvoice.Columns[5].HeaderText = "Rate";
            dvgInvoice.Columns[6].HeaderText = "GST%";
            dvgInvoice.Columns[7].HeaderText = "CGST Amount";
            dvgInvoice.Columns[8].HeaderText = "SGST Amount";
            dvgInvoice.Columns[9].HeaderText = "Discount %";
            dvgInvoice.Columns[10].HeaderText = "Disc Amount";
            dvgInvoice.Columns[11].HeaderText = "Total Amount";
            dvgInvoice.Columns[12].HeaderText = "Basic Amount";
            dvgInvoice.Columns[13].HeaderText = "fk_RateID";
            //dvgInvoice.Columns[5].HeaderText = "Cashier / Restaurant";

            dvgInvoice.Columns[0].DataPropertyName = "pk_BillSrl";
            dvgInvoice.Columns[1].DataPropertyName = "fk_billID";
            dvgInvoice.Columns[2].DataPropertyName = "fk_ItemID";
            dvgInvoice.Columns[3].DataPropertyName = "itemdescr";
            dvgInvoice.Columns[4].DataPropertyName = "Qty";
            dvgInvoice.Columns[5].DataPropertyName = "salerate";
            dvgInvoice.Columns[6].DataPropertyName = "GSTPer";
            dvgInvoice.Columns[7].DataPropertyName = "CGSTAmount";
            dvgInvoice.Columns[8].DataPropertyName = "SGSTAmount";
            dvgInvoice.Columns[9].DataPropertyName = "DiscountPer";
            dvgInvoice.Columns[10].DataPropertyName = "DiscountAmount";
            dvgInvoice.Columns[11].DataPropertyName = "TotalAmount";
            dvgInvoice.Columns[12].DataPropertyName = "BasicAmount";
            dvgInvoice.Columns[13].DataPropertyName = "fk_RateID";

            dvgInvoice.Columns[3].Width = 250;
            dvgInvoice.Columns[4].Width = 50;
            dvgInvoice.Columns[6].Width = 65;
            dvgInvoice.Columns[11].Width = 90;

            dvgInvoice.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dvgInvoice.Columns[3].FillWeight = 54;
            dvgInvoice.Columns[4].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dvgInvoice.Columns[4].FillWeight = 10;
            dvgInvoice.Columns[6].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dvgInvoice.Columns[6].FillWeight = 12;
            dvgInvoice.Columns[11].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dvgInvoice.Columns[11].FillWeight = 20;

            //dvgInvoice.Columns[1].SortMode = DataGridViewColumnSortMode.Automatic;
            dvgInvoice.Columns[3].SortMode = DataGridViewColumnSortMode.NotSortable;
            dvgInvoice.Columns[4].SortMode = DataGridViewColumnSortMode.NotSortable;
            dvgInvoice.Columns[6].SortMode = DataGridViewColumnSortMode.NotSortable;
            dvgInvoice.Columns[11].SortMode = DataGridViewColumnSortMode.NotSortable;

            dvgInvoice.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dvgInvoice.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dvgInvoice.Columns[11].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            dvgInvoice.Columns[4].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
            dvgInvoice.Columns[6].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
            dvgInvoice.Columns[11].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;

            dvgInvoice.Columns[11].DefaultCellStyle.Format = "N2";
            //dvgInvoice.Columns(2).DefaultCellStyle.Format = "N3";

            dvgInvoice.Columns[0].Visible = false;
            dvgInvoice.Columns[1].Visible = false;
            dvgInvoice.Columns[2].Visible = false;
            dvgInvoice.Columns[5].Visible = false;
            dvgInvoice.Columns[7].Visible = false;
            dvgInvoice.Columns[8].Visible = false;
            dvgInvoice.Columns[9].Visible = false;
            dvgInvoice.Columns[10].Visible = false;
            dvgInvoice.Columns[12].Visible = false;
            dvgInvoice.Columns[13].Visible = false;

            DataGridViewImageColumn Delimg = new DataGridViewImageColumn();
            Image Delimage = Image.FromFile(path + ConfigurationManager.AppSettings["imageFolder"] + "delete-icon.png");
            Delimg.Image = Delimage;
            dvgInvoice.Columns.Add(Delimg);
            Delimg.HeaderText = "Del";
            Delimg.Name = "Dimg";
            Delimg.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            Delimg.Width = 40;

            dvgInvoice.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dvgInvoice.MultiSelect = false;

            InvoiceBLL invbll = new InvoiceBLL();
            ds = invbll.DisplayDetailItems(searchCriteria);
           
            DataView view1 = new DataView(ds.Tables[0]);

            source1.DataSource = view1;

            dvgInvoice.DataSource = source1;
            dvgInvoice.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            dvgInvoice.AllowUserToResizeColumns = false;
            dvgInvoice.AllowUserToAddRows = false;
            dvgInvoice.ReadOnly = true;
            dvgInvoice.Columns[1].HeaderCell.Style = style;
            dvgInvoice.Columns[2].HeaderCell.Style = style;
            //dvgInvoice.Columns[3].HeaderCell.Style = style;

            gridInitialized = true;
        }

        private void dvgInvoice_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dvgInvoice.Columns[e.ColumnIndex].Name == "Dimg")
            {
                DialogResult dialogResult = MessageBox.Show("Want to Delete the Item", "Delete Item", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dialogResult == DialogResult.No)
                    return;
                else
                {
                    ds.Tables[0].Rows.RemoveAt(e.RowIndex);
                    ds.Tables[0].AcceptChanges();
                    CalcPayableAmt(ds.Tables[0]);
                }
            }
            else
            //if (dvgInvoice.Columns[e.ColumnIndex].Name == "Eimg")
            {
                DataSet dsRefund;
                //tmrUserCard.Enabled = false;
                int retVal;
                int OldQty;
                decimal OldAmount;
                string STApplicable = "";
                //decimal cAmount = 0;

                DataRow dr = ds.Tables[0].Rows[e.RowIndex];
                InputBox.SetLanguage(InputBox.Language.English);
                DialogResult res = InputBox.ShowDialog(" " + dr["itemdescr"], "Edit Quantity", InputBox.Icon.Question, InputBox.Buttons.OkCancel,
                    InputBox.Type.ComboBox, //Set type (default nothing)
                    new string[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19" }, //String field as ComboBox items (default null)
                    true, //Set visible in taskbar (default false)
                    new System.Drawing.Font("Arial Black", 12F, FontStyle.Bold)); //Set font (default by system)

                //tmrUserCard.Enabled = true;
                //DialogResult res = InputBox.ShowDialog(" " + dr["itemdescr"], "Edit Quantity", InputBox.Icon.Question, InputBox.Buttons.OkCancel, InputBox.Type.TextBox, null, true, new System.Drawing.Font("Arial Black", 10F, FontStyle.Bold));
                if (res == System.Windows.Forms.DialogResult.OK)
                {
                    retVal = InputBox.ResultValue.ToInt();

                    if (bType == "R")
                    {
                        InvoiceBLL invBill = new InvoiceBLL();
                        //dsRefund = invBill.CheckItemRefundPossible(card.CardID, card.CurrentSerial, dr["fk_ItemID"].ToInt(), RestID, GeneralFunctions.LoginInfo.CurrDate);
                        //if (dsRefund.Tables[0].Rows[0]["Qty"].ToInt() != 0)
                        //{
                        //    if (dsRefund.Tables[0].Rows[0]["Qty"].ToInt() < retVal)
                        //    {
                        //        FlexibleMessageBox.Show("Items Sold should be Greater or Equal to Refund", "Invalid Quantity", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        //        ds.Tables[0].RejectChanges();
                        //        return;
                        //    }
                        //}
                        //else
                        //{
                        //    FlexibleMessageBox.Show("No Sale. Refund not possible", "Invalid Quantity", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        //    ds.Tables[0].RejectChanges();
                        //    return;
                        //}
                    }

                    if (retVal > 0)
                    {
                        DataRow[] drCOLL = ds.Tables[0].Select("fk_ItemID = " + int.Parse(dr["fk_ItemID"].ToString()));

                        dr = drCOLL[0];

                        OldQty = dr["Qty"].ToInt();
                        OldAmount = dr["TotalAmount"].ToDecimal();

                        dr["Qty"] = retVal;
                        //if (dr["ServiceTaxPer"].ToDecimal() != 0)
                        //    STApplicable = "1";

                        CalculateAmount(GeneralFunctions.LoginInfo.CalcMethod, dr, dr["salerate"].ToDecimal(), dr["Qty"].ToDecimal(), dr["CGSTPer"].ToDecimal(), dr["SGSTPer"].ToDecimal());
                        //cAmount = lblBillAmount.ToDecimal();

                        if (drCOLL.Length == 0)
                        {
                            ds.Tables[0].Rows.Add(dr);
                        }
                        ds.Tables[0].AcceptChanges();
                        CalcPayableAmt(ds.Tables[0]);

                        /*
                        if (decimal.Parse(lblOpening.Text) - (decimal.Parse(lblCurrentBalance.Text) + cAmount) + OldAmount >= 0)
                        {
                            if (drCOLL.Length == 0)
                            {
                                ds.Tables[0].Rows.Add(dr);
                            }
                            ds.Tables[0].AcceptChanges();
                            CalcPayableAmt(ds.Tables[0]);
                        }
                        else
                        {
                            if (drCOLL.Length > 0)
                            {
                                ds.Tables[0].RejectChanges();
                            }
                            MessageBox.Show("Insufficient Card Balanace!!!");
                            return;
                        }
                        */
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(InputBox.ResultValue))
                        {
                            MessageBox.Show("Enter a valid quantity. Quantity 0 Invalid !!!");
                        }
                    }
                }
            }
        }
        #endregion
        /*
        private void CheckforRefundPossible()
        {
            InvoiceBLL invBill = new InvoiceBLL();

            if (ds.Tables[0].Rows.Count == 0)
            {
                if (invBill.CheckRefundPossible(card.CardNoInside, card.CurrentSerial))
                    btnRefund.Enabled = true;
                else
                    btnRefund.Enabled = false;
            }
        }
        */
        /*
        private void txtAcceptCardNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                trackInfo.Append(e.KeyChar);
                trackInfo.AppendLine();
                //txtAcceptCardNo.Text = trackInfo.ToString();
                e.Handled = true;
                trackInfo.Clear();
                IgnoreInfo.Clear();
                //sendTrackInfo(txtAcceptCardNo.Text);
                OpenMenu = false;
                //txtAcceptCardNo.Text = string.Empty;
                //txtAcceptCardNo.Enabled = false;
            }
            else if (e.KeyChar != '?' && !track1Complete)
            {
                trackInfo.Append(e.KeyChar);
            }
            else if (e.KeyChar == '?' || track1Complete)
            {
                track1Complete = true;
                IgnoreInfo.Append(e.KeyChar);
            }
        }
        */
        /*
        private void sendTrackInfo(string CardNoSwiped)
        {
            FlexibleMessageBox.FONT = new Font("Arial Black", 12, FontStyle.Bold);
            FoodSmart.BLL.itemService.SearchCriteria searchCriteria = new FoodSmart.BLL.itemService.SearchCriteria();
            if (CardNoSwiped.Length > 5)
            {
                string CardNo = CardNoSwiped.Substring(1, 5);
                GotCardDetails = true;
                if (!string.IsNullOrEmpty(CardNo))
                {
                    card = new CardBLL().GetCardData(CardNo, "I");
                    GeneralFunctions.LoginInfo.CurrDate = card.CurrDate;
                    if (card != null)
                    {
                        if (card.CardType == "R")
                        {
                            lblCardNo.Text = card.CardNoOutside + "(" + card.CurrentSerial.ToString() + ")";

                            lblOpening.Text = card.CardBalance.ToString();
                            if (String.IsNullOrEmpty(lblCurrentBalance.Text) || lblCurrentBalance.Text.ToUpper() == "CURRENT")
                            {
                                lblCurrentBalance.Text = "0.00";
                            }
                            if (bType == "B")
                                lblClosing.Text = (decimal.Parse(card.CardBalance.ToString()) - decimal.Parse(lblCurrentBalance.Text)).ToString("####0.00");
                            else
                                lblClosing.Text = (decimal.Parse(card.CardBalance.ToString()) + decimal.Parse(lblCurrentBalance.Text)).ToString("####0.00");
                            fk_Cardid = card.CardID;
                            if (card.CardActive == 0 || card.CardActive == 2)
                            {
                                //lblInternalNoMessage.Text = "Card Not activated at FLAVOURS";
                                //lblInternalNoMessage.Visible = true;
                                FlexibleMessageBox.Show("Card Not activated at FLAVOURS", "Card Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                EnableDisableControl(0);
                                Initialize();
                                lblInternalNoMessage.Visible = true;
                                txtAcceptCardNo.Text = "";
                            }
                            else if (card.TempLock)
                            {
                                //lblInternalNoMessage.Text = "Card Locked from Other terminal";
                                //lblInternalNoMessage.Visible = true;
                                txtAcceptCardNo.Text = "";
                                FlexibleMessageBox.Show("Card Locked from Other terminal.", "Card Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                            }
                            else if (card.CardLocked)
                            {
                                //lblInternalNoMessage.Text = "Card Locked. No Transaction Possible";
                                //lblInternalNoMessage.Visible = true;
                                txtAcceptCardNo.Text = "";
                                FlexibleMessageBox.Show("Card Locked. No Transaction Possible", "Card Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                            }
                            else
                            {
                                EnableDisableControl(1);
                                CheckforRefundPossible();
                                new InvoiceBLL().UpdateTempCardStatus(card.CardNoInside, true);
                                txtAcceptCardNo.Enabled = false;
                                lblInternalNoMessage.Visible = false;
                                if (!gridInitialized)
                                {
                                    InitializeBills();
                                }
                            }
                        }
                        else
                        {
                            Initialize();
                            EnableDisableControl(0);
                            InitializeBills();
                            FlexibleMessageBox.Show("Not a Customer Card", "Card Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                            //lblInternalNoMessage.Text = "Not Customer Card";
                            //lblInternalNoMessage.Visible = true;
                            clearItemButton();
                            txtAcceptCardNo.Enabled = true;
                            txtAcceptCardNo.Text = "";
                            txtAcceptCardNo.Focus();
                        }
                    }
                }
                else
                {
                    Initialize();
                    InitializeBills();
                    clearItemButton();
                    EnableDisableControl(0);
                    FlexibleMessageBox.Show("Invalid Card No", "Card Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    //lblInternalNoMessage.Text = "Invalid Card No";
                    //lblInternalNoMessage.Visible = true;
                    txtAcceptCardNo.Enabled = true;
                    txtAcceptCardNo.Text = "";
                    txtAcceptCardNo.Focus();
                    GotCardDetails = false;
                }
            }
            trackInfo.Clear();
            txtAcceptCardNo.Enabled = true;
            txtAcceptCardNo.Text = "";
            track1Complete = false;
        }
        */
        private void lnkDiscount_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //if (bType == "B")
            //{
            //    Form lightbox = new AcceptDiscount(this);
            //    lightbox.Owner = this;
            //    this.Opacity = 1;
            //    lightbox.ShowDialog();
            //    if (DiscPer != 0)
            //    {
            //        txtDiscPer.Text = DiscPer.ToString("##0.00");
            //    }
            //}
        }

        private void txtDiscPer_TextChanged(object sender, EventArgs e)
        {
            DataRow dr;
            string STApplicable = "";
           
            foreach (DataGridViewRow row in dvgInvoice.Rows)
            {
                DataRow[] drCOLL = ds.Tables[0].Select("fk_ItemID = " + row.Cells["fk_ItemID"].Value);
                dr = drCOLL[0];

                //if (dr["ServiceTaxPer"].ToDecimal() != 0)
                //    STApplicable = "1";

                CalculateAmount(GeneralFunctions.LoginInfo.CalcMethod, dr, dr["salerate"].ToDecimal(), dr["Qty"].ToDecimal(), dr["CGSTPer"].ToDecimal(), dr["SGSTPer"].ToDecimal());
            }
            ds.Tables[0].AcceptChanges();
            CalcPayableAmt(ds.Tables[0]);
        }

        private void CalculateAmount(string CalcType, DataRow dr, decimal salerate, decimal Qty, decimal CGSTPer, decimal SGSTPer)
        {
            decimal itemNetAmt = 0;
            //decimal cAmount = 0;
            decimal Taxes = 0;
            decimal tempAmt = 0;

            if (GeneralFunctions.LoginInfo.CalcMethod == "F")
            {
                if (string.IsNullOrEmpty(txtDiscPer.Text) || txtDiscPer.Text.ToDecimal() == 0)
                    dr["BasicAmount"] = salerate * Qty;
                else if (Convert.ToDecimal(txtDiscPer.Text) != 0)
                    dr["BasicAmount"] = Math.Round(salerate * Qty * (100 - Convert.ToDecimal(txtDiscPer.Text)) / 100, 2);
                else if (Convert.ToDecimal(txtDiscPer.Text) == 100)
                    dr["BasicAmount"] = 0;

                dr["CGSTAmt"] = Math.Round(dr["BasicAmount"].ToDecimal() * CGSTPer / 100, 2);
                dr["SGSTAmt"] = Math.Round(dr["BasicAmount"].ToDecimal() * SGSTPer / 100, 2);

                dr["TotalAmount"] = dr["BasicAmount"].ToDouble() + dr["CGSTAmt"].ToDouble() + dr["SGSTAmt"].ToDouble();

                itemNetAmt = dr["BasicAmount"].ToDecimal() + dr["CGSTAmt"].ToDecimal() + dr["SGSTAmt"].ToDecimal();
                cAmount = itemNetAmt;
            }
            else
            {
                itemNetAmt = decimal.Parse(dr["salerate"].ToString()) * int.Parse(dr["Qty"].ToString());
                dr["SGSTAmt"] = 0;
                dr["CGSTAmt"] = 0;
                dr["DiscountPer"] = txtDiscPer.Text.ToDecimal();
                dr["DiscountAmount"] = 0;

                cAmount = Qty * decimal.Parse(dr["salerate"].ToString());
                Taxes = CGSTPer + SGSTPer;
                
                tempAmt = Math.Round(itemNetAmt * 100 / (100 + Taxes), 2);

                if (!string.IsNullOrEmpty(txtDiscPer.Text) && txtDiscPer.Text.ToDecimal() != 0)
                {
                    if (Convert.ToDecimal(txtDiscPer.Text) == 100)
                        tempAmt = 0;
                    else
                    {
                        dr["DiscountAmount"] = Math.Round(tempAmt * Convert.ToDecimal(txtDiscPer.Text) / 100, 2);
                        lblDisc.Text = (lblDisc.Text.ToDecimal() + dr["DiscountAmount"].ToDecimal()).ToString();
                        tempAmt = tempAmt - dr["DiscountAmount"].ToDecimal();
                    }
                }

                dr["CGSTAmt"] = Math.Round((tempAmt * CGSTPer) / 100, 2);
                dr["SGSTAmt"] = Math.Round((tempAmt * SGSTPer) / 100, 2);

                dr["BasicAmount"] = cAmount - dr["CGSTAmt"].ToDecimal() - dr["SGSTAmt"].ToDecimal() - dr["DiscountAmount"].ToDecimal();

                dr["TotalAmount"] = cAmount - dr["DiscountAmount"].ToDecimal();
                
                cAmount = dr["TotalAmount"].ToDecimal();
            }
        }

        private void btnItemSummary_Click(object sender, EventArgs e)
        {
            Form lightbox = new ViewItemWiseSale(this);
            lightbox.Owner = this;
            this.Opacity = 1;
            lightbox.ShowDialog();
            if (RefReasonID != 0)
            {
                POS.PrintItemWiseSale print = new POS.PrintItemWiseSale(GeneralFunctions.LoginInfo.LocID, PrinterName);
            }
        }

        private void btnReprint_Click(object sender, EventArgs e)
        {
            Form lightbox = new ReprintBill(this);
            lightbox.Owner = this;
            this.Opacity = 1;
            lightbox.ShowDialog();
            if (InvoiceID != 0)
            {
                if (PrinterType == "U")
                {
                    POS.DirectPrintInvoice print = new POS.DirectPrintInvoice(bType, InvoiceID, PrinterName, GeneralFunctions.LoginInfo.RestName, "B", GeneralFunctions.LoginInfo.CalcMethod);
                }
                else
                {
                    POS.DirectPrintInvLPT print = new POS.DirectPrintInvLPT(bType, InvoiceID, PrinterName, GeneralFunctions.LoginInfo.RestName, "B", GeneralFunctions.LoginInfo.CalcMethod);
                }
            }
        }

        private void txtMobileNo_TextChanged(object sender, EventArgs e)
        {
            if (txtMobileNo.Text.Trim().Length == 10)
            {
                tbLayoutItemGroup.Enabled = true;
                gbHotItems.Enabled = true;
            }
            else
            {
                tbLayoutItemGroup.Enabled = false;
                gbHotItems.Enabled = false;
                txtName.Text = string.Empty;
            }
        }

        private void txtMobileNo_Validated(object sender, EventArgs e)
        {
            IInvoice inv = new FoodSmart.Entity.InvoiceEntity();
            InvoiceBLL cardTranBll = new InvoiceBLL();
            DataSet Ds = cardTranBll.GetMobileNo(txtMobileNo.Text);
            if (Ds.Tables[0].Rows.Count != 0)
            {
                txtName.Text = Ds.Tables[0].Rows[0]["Custname"].ToString();
                txtName.Enabled = false;
            }
            else
            {
                txtName.Enabled = true;
                inv.CustID = 0;
            }

        }

        private void txtMobileNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }

        }

        private void lblUserName_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Masters.ChangePassword item = new Masters.ChangePassword();
            item.ShowDialog();
            System.Environment.Exit(0);
        }
    }
}
