﻿namespace FoodSmartDesktop.POS
{
    partial class Payment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Payment));
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblUserAddEdit = new System.Windows.Forms.Label();
            this.lblExit = new System.Windows.Forms.Label();
            this.lblBillAmtDisplay = new System.Windows.Forms.Label();
            this.btnCard = new System.Windows.Forms.Button();
            this.btnCash = new System.Windows.Forms.Button();
            this.gbNumbers = new CodeVendor.Controls.Grouper();
            this.btnback = new System.Windows.Forms.Button();
            this.btn00 = new System.Windows.Forms.Button();
            this.btn9 = new System.Windows.Forms.Button();
            this.btn8 = new System.Windows.Forms.Button();
            this.btn7 = new System.Windows.Forms.Button();
            this.btn6 = new System.Windows.Forms.Button();
            this.btn5 = new System.Windows.Forms.Button();
            this.btn4 = new System.Windows.Forms.Button();
            this.btn3 = new System.Windows.Forms.Button();
            this.btn2 = new System.Windows.Forms.Button();
            this.btn1 = new System.Windows.Forms.Button();
            this.btn0 = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btneWallet = new System.Windows.Forms.Button();
            this.btnMealPass = new System.Windows.Forms.Button();
            this.gbRupees = new System.Windows.Forms.GroupBox();
            this.btnR1000 = new System.Windows.Forms.Button();
            this.btnR500 = new System.Windows.Forms.Button();
            this.btnR100 = new System.Windows.Forms.Button();
            this.btnR50 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtAmountPaid = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tplCash = new System.Windows.Forms.TableLayoutPanel();
            this.txtRefund = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnPay = new System.Windows.Forms.Button();
            this.tlpCC = new System.Windows.Forms.TableLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.ddlCCType = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.label8 = new System.Windows.Forms.Label();
            this.txtAcceptCardNo = new System.Windows.Forms.TextBox();
            this.txtCCCash = new System.Windows.Forms.TextBox();
            this.tlpEwallet = new System.Windows.Forms.TableLayoutPanel();
            this.txtEWalletRef = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.ddlleWallet = new System.Windows.Forms.ComboBox();
            this.tlpMealPass = new System.Windows.Forms.TableLayoutPanel();
            this.ddlMPBalance = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.ddlMealPass = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtMealPassRef = new System.Windows.Forms.TextBox();
            this.txtMPCCNo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtMPAmt = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtMPBalAmt = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.txtCouponNo = new System.Windows.Forms.TextBox();
            this.txtCouponPrefix = new System.Windows.Forms.TextBox();
            this.txtCouponAmt = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.lblError = new System.Windows.Forms.Label();
            this.lblPayable = new System.Windows.Forms.Label();
            this.txtPayable = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.lblPaidCash = new System.Windows.Forms.Label();
            this.lblPaidCC = new System.Windows.Forms.Label();
            this.lblPaidMealPass = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.lblPaidWallet = new System.Windows.Forms.Label();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1.SuspendLayout();
            this.gbNumbers.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.gbRupees.SuspendLayout();
            this.tplCash.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tlpCC.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tlpEwallet.SuspendLayout();
            this.tlpMealPass.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(199)))), ((int)(((byte)(233)))));
            this.panel1.Controls.Add(this.lblUserAddEdit);
            this.panel1.Controls.Add(this.lblExit);
            this.panel1.Location = new System.Drawing.Point(1, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(509, 45);
            this.panel1.TabIndex = 226;
            // 
            // lblUserAddEdit
            // 
            this.lblUserAddEdit.BackColor = System.Drawing.Color.Transparent;
            this.lblUserAddEdit.Font = new System.Drawing.Font("Arial", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserAddEdit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.lblUserAddEdit.Location = new System.Drawing.Point(14, 16);
            this.lblUserAddEdit.Name = "lblUserAddEdit";
            this.lblUserAddEdit.Size = new System.Drawing.Size(213, 22);
            this.lblUserAddEdit.TabIndex = 161;
            this.lblUserAddEdit.Text = "Payment";
            // 
            // lblExit
            // 
            this.lblExit.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExit.ForeColor = System.Drawing.Color.DarkRed;
            this.lblExit.Image = ((System.Drawing.Image)(resources.GetObject("lblExit.Image")));
            this.lblExit.Location = new System.Drawing.Point(441, 0);
            this.lblExit.Name = "lblExit";
            this.lblExit.Size = new System.Drawing.Size(63, 47);
            this.lblExit.TabIndex = 1;
            this.lblExit.Click += new System.EventHandler(this.lblExit_Click);
            // 
            // lblBillAmtDisplay
            // 
            this.lblBillAmtDisplay.AutoSize = true;
            this.lblBillAmtDisplay.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.lblBillAmtDisplay.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblBillAmtDisplay.Location = new System.Drawing.Point(7, 498);
            this.lblBillAmtDisplay.Name = "lblBillAmtDisplay";
            this.lblBillAmtDisplay.Size = new System.Drawing.Size(68, 19);
            this.lblBillAmtDisplay.TabIndex = 227;
            this.lblBillAmtDisplay.Text = "Total Bill";
            // 
            // btnCard
            // 
            this.btnCard.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCard.BackgroundImage")));
            this.btnCard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCard.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCard.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCard.Image = ((System.Drawing.Image)(resources.GetObject("btnCard.Image")));
            this.btnCard.Location = new System.Drawing.Point(253, 3);
            this.btnCard.Name = "btnCard";
            this.btnCard.Size = new System.Drawing.Size(244, 42);
            this.btnCard.TabIndex = 230;
            this.btnCard.Text = "CARD";
            this.btnCard.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCard.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCard.UseVisualStyleBackColor = false;
            this.btnCard.Click += new System.EventHandler(this.btnCard_Click);
            // 
            // btnCash
            // 
            this.btnCash.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCash.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCash.BackgroundImage")));
            this.btnCash.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCash.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCash.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCash.Image = ((System.Drawing.Image)(resources.GetObject("btnCash.Image")));
            this.btnCash.Location = new System.Drawing.Point(3, 3);
            this.btnCash.Name = "btnCash";
            this.btnCash.Size = new System.Drawing.Size(244, 42);
            this.btnCash.TabIndex = 228;
            this.btnCash.Text = "CASH";
            this.btnCash.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCash.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCash.UseVisualStyleBackColor = false;
            this.btnCash.Click += new System.EventHandler(this.btnCash_Click);
            // 
            // gbNumbers
            // 
            this.gbNumbers.BackgroundColor = System.Drawing.Color.White;
            this.gbNumbers.BackgroundGradientColor = System.Drawing.Color.White;
            this.gbNumbers.BackgroundGradientMode = CodeVendor.Controls.Grouper.GroupBoxGradientMode.None;
            this.gbNumbers.BorderColor = System.Drawing.Color.Navy;
            this.gbNumbers.BorderThickness = 1F;
            this.gbNumbers.Controls.Add(this.btnback);
            this.gbNumbers.Controls.Add(this.btn00);
            this.gbNumbers.Controls.Add(this.btn9);
            this.gbNumbers.Controls.Add(this.btn8);
            this.gbNumbers.Controls.Add(this.btn7);
            this.gbNumbers.Controls.Add(this.btn6);
            this.gbNumbers.Controls.Add(this.btn5);
            this.gbNumbers.Controls.Add(this.btn4);
            this.gbNumbers.Controls.Add(this.btn3);
            this.gbNumbers.Controls.Add(this.btn2);
            this.gbNumbers.Controls.Add(this.btn1);
            this.gbNumbers.Controls.Add(this.btn0);
            this.gbNumbers.CustomGroupBoxColor = System.Drawing.Color.White;
            this.gbNumbers.GroupImage = null;
            this.gbNumbers.GroupTitle = "";
            this.gbNumbers.Location = new System.Drawing.Point(8, 362);
            this.gbNumbers.Name = "gbNumbers";
            this.gbNumbers.Padding = new System.Windows.Forms.Padding(20);
            this.gbNumbers.PaintGroupBox = false;
            this.gbNumbers.RoundCorners = 10;
            this.gbNumbers.ShadowColor = System.Drawing.Color.DarkGray;
            this.gbNumbers.ShadowControl = false;
            this.gbNumbers.ShadowThickness = 3;
            this.gbNumbers.Size = new System.Drawing.Size(500, 133);
            this.gbNumbers.TabIndex = 231;
            // 
            // btnback
            // 
            this.btnback.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnback.BackgroundImage")));
            this.btnback.Image = ((System.Drawing.Image)(resources.GetObject("btnback.Image")));
            this.btnback.Location = new System.Drawing.Point(420, 71);
            this.btnback.Name = "btnback";
            this.btnback.Size = new System.Drawing.Size(68, 54);
            this.btnback.TabIndex = 23;
            this.btnback.UseVisualStyleBackColor = true;
            this.btnback.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnback_MouseUp1);
            // 
            // btn00
            // 
            this.btn00.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn00.BackgroundImage")));
            this.btn00.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn00.ForeColor = System.Drawing.Color.White;
            this.btn00.Location = new System.Drawing.Point(420, 14);
            this.btn00.Name = "btn00";
            this.btn00.Size = new System.Drawing.Size(68, 54);
            this.btn00.TabIndex = 22;
            this.btn00.Tag = "00";
            this.btn00.Text = "00";
            this.btn00.UseVisualStyleBackColor = true;
            this.btn00.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NumericMouseUp);
            // 
            // btn9
            // 
            this.btn9.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn9.ForeColor = System.Drawing.Color.White;
            this.btn9.Location = new System.Drawing.Point(339, 71);
            this.btn9.Name = "btn9";
            this.btn9.Size = new System.Drawing.Size(68, 54);
            this.btn9.TabIndex = 21;
            this.btn9.Tag = "9";
            this.btn9.Text = "9";
            this.btn9.UseVisualStyleBackColor = true;
            this.btn9.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NumericMouseUp);
            // 
            // btn8
            // 
            this.btn8.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn8.ForeColor = System.Drawing.Color.White;
            this.btn8.Location = new System.Drawing.Point(258, 71);
            this.btn8.Name = "btn8";
            this.btn8.Size = new System.Drawing.Size(68, 54);
            this.btn8.TabIndex = 20;
            this.btn8.Tag = "8";
            this.btn8.Text = "8";
            this.btn8.UseVisualStyleBackColor = true;
            this.btn8.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NumericMouseUp);
            // 
            // btn7
            // 
            this.btn7.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn7.ForeColor = System.Drawing.Color.White;
            this.btn7.Location = new System.Drawing.Point(177, 71);
            this.btn7.Name = "btn7";
            this.btn7.Size = new System.Drawing.Size(68, 54);
            this.btn7.TabIndex = 19;
            this.btn7.Tag = "7";
            this.btn7.Text = "7";
            this.btn7.UseVisualStyleBackColor = true;
            this.btn7.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NumericMouseUp);
            // 
            // btn6
            // 
            this.btn6.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn6.ForeColor = System.Drawing.Color.White;
            this.btn6.Location = new System.Drawing.Point(96, 71);
            this.btn6.Name = "btn6";
            this.btn6.Size = new System.Drawing.Size(68, 54);
            this.btn6.TabIndex = 18;
            this.btn6.Tag = "6";
            this.btn6.Text = "6";
            this.btn6.UseVisualStyleBackColor = true;
            this.btn6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NumericMouseUp);
            // 
            // btn5
            // 
            this.btn5.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn5.ForeColor = System.Drawing.Color.White;
            this.btn5.Location = new System.Drawing.Point(15, 71);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(68, 54);
            this.btn5.TabIndex = 17;
            this.btn5.Tag = "5";
            this.btn5.Text = "5";
            this.btn5.UseVisualStyleBackColor = true;
            this.btn5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NumericMouseUp);
            // 
            // btn4
            // 
            this.btn4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn4.BackgroundImage")));
            this.btn4.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn4.ForeColor = System.Drawing.Color.White;
            this.btn4.Location = new System.Drawing.Point(339, 14);
            this.btn4.Name = "btn4";
            this.btn4.Size = new System.Drawing.Size(68, 54);
            this.btn4.TabIndex = 16;
            this.btn4.Tag = "4";
            this.btn4.Text = "4";
            this.btn4.UseVisualStyleBackColor = true;
            this.btn4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NumericMouseUp);
            // 
            // btn3
            // 
            this.btn3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn3.BackgroundImage")));
            this.btn3.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn3.ForeColor = System.Drawing.Color.White;
            this.btn3.Location = new System.Drawing.Point(258, 14);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(68, 54);
            this.btn3.TabIndex = 15;
            this.btn3.Tag = "3";
            this.btn3.Text = "3";
            this.btn3.UseVisualStyleBackColor = true;
            this.btn3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NumericMouseUp);
            // 
            // btn2
            // 
            this.btn2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn2.BackgroundImage")));
            this.btn2.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn2.ForeColor = System.Drawing.Color.White;
            this.btn2.Location = new System.Drawing.Point(177, 14);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(68, 54);
            this.btn2.TabIndex = 14;
            this.btn2.Tag = "2";
            this.btn2.Text = "2";
            this.btn2.UseVisualStyleBackColor = true;
            this.btn2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NumericMouseUp);
            // 
            // btn1
            // 
            this.btn1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn1.BackgroundImage")));
            this.btn1.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn1.ForeColor = System.Drawing.Color.White;
            this.btn1.Location = new System.Drawing.Point(96, 14);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(68, 54);
            this.btn1.TabIndex = 13;
            this.btn1.Tag = "1";
            this.btn1.Text = "1";
            this.btn1.UseVisualStyleBackColor = true;
            this.btn1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NumericMouseUp);
            // 
            // btn0
            // 
            this.btn0.BackColor = System.Drawing.SystemColors.Info;
            this.btn0.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn0.BackgroundImage")));
            this.btn0.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn0.ForeColor = System.Drawing.Color.White;
            this.btn0.Location = new System.Drawing.Point(15, 14);
            this.btn0.Name = "btn0";
            this.btn0.Size = new System.Drawing.Size(68, 54);
            this.btn0.TabIndex = 12;
            this.btn0.Tag = "0";
            this.btn0.Text = "0";
            this.btn0.UseVisualStyleBackColor = false;
            this.btn0.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NumericMouseUp);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.btneWallet, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnMealPass, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.btnCash, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnCard, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(8, 49);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(500, 96);
            this.tableLayoutPanel1.TabIndex = 232;
            // 
            // btneWallet
            // 
            this.btneWallet.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btneWallet.BackgroundImage")));
            this.btneWallet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btneWallet.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btneWallet.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btneWallet.Image = ((System.Drawing.Image)(resources.GetObject("btneWallet.Image")));
            this.btneWallet.Location = new System.Drawing.Point(3, 51);
            this.btneWallet.Name = "btneWallet";
            this.btneWallet.Size = new System.Drawing.Size(244, 42);
            this.btneWallet.TabIndex = 232;
            this.btneWallet.Text = "eWALLET";
            this.btneWallet.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btneWallet.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btneWallet.UseVisualStyleBackColor = false;
            this.btneWallet.Click += new System.EventHandler(this.btneWallet_Click);
            // 
            // btnMealPass
            // 
            this.btnMealPass.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnMealPass.BackgroundImage")));
            this.btnMealPass.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnMealPass.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMealPass.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnMealPass.Image = ((System.Drawing.Image)(resources.GetObject("btnMealPass.Image")));
            this.btnMealPass.Location = new System.Drawing.Point(253, 51);
            this.btnMealPass.Name = "btnMealPass";
            this.btnMealPass.Size = new System.Drawing.Size(244, 42);
            this.btnMealPass.TabIndex = 231;
            this.btnMealPass.Text = "MEAL PASS";
            this.btnMealPass.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnMealPass.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnMealPass.UseVisualStyleBackColor = false;
            this.btnMealPass.Click += new System.EventHandler(this.btnMealPass_Click);
            // 
            // gbRupees
            // 
            this.gbRupees.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.gbRupees.Controls.Add(this.btnR1000);
            this.gbRupees.Controls.Add(this.btnR500);
            this.gbRupees.Controls.Add(this.btnR100);
            this.gbRupees.Controls.Add(this.btnR50);
            this.gbRupees.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.gbRupees.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbRupees.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.gbRupees.Location = new System.Drawing.Point(11, 303);
            this.gbRupees.Name = "gbRupees";
            this.gbRupees.Size = new System.Drawing.Size(500, 67);
            this.gbRupees.TabIndex = 234;
            this.gbRupees.TabStop = false;
            // 
            // btnR1000
            // 
            this.btnR1000.Image = ((System.Drawing.Image)(resources.GetObject("btnR1000.Image")));
            this.btnR1000.Location = new System.Drawing.Point(390, 15);
            this.btnR1000.Name = "btnR1000";
            this.btnR1000.Size = new System.Drawing.Size(96, 40);
            this.btnR1000.TabIndex = 4;
            this.btnR1000.Tag = "1000";
            this.btnR1000.UseVisualStyleBackColor = true;
            this.btnR1000.MouseUp += new System.Windows.Forms.MouseEventHandler(this.RupeeMouseUp);
            // 
            // btnR500
            // 
            this.btnR500.Image = ((System.Drawing.Image)(resources.GetObject("btnR500.Image")));
            this.btnR500.Location = new System.Drawing.Point(262, 15);
            this.btnR500.Name = "btnR500";
            this.btnR500.Size = new System.Drawing.Size(96, 40);
            this.btnR500.TabIndex = 3;
            this.btnR500.Tag = "500";
            this.btnR500.UseVisualStyleBackColor = true;
            this.btnR500.MouseUp += new System.Windows.Forms.MouseEventHandler(this.RupeeMouseUp);
            // 
            // btnR100
            // 
            this.btnR100.Image = ((System.Drawing.Image)(resources.GetObject("btnR100.Image")));
            this.btnR100.Location = new System.Drawing.Point(134, 15);
            this.btnR100.Name = "btnR100";
            this.btnR100.Size = new System.Drawing.Size(96, 40);
            this.btnR100.TabIndex = 2;
            this.btnR100.Tag = "100";
            this.btnR100.UseVisualStyleBackColor = true;
            this.btnR100.MouseUp += new System.Windows.Forms.MouseEventHandler(this.RupeeMouseUp);
            // 
            // btnR50
            // 
            this.btnR50.Image = ((System.Drawing.Image)(resources.GetObject("btnR50.Image")));
            this.btnR50.Location = new System.Drawing.Point(6, 15);
            this.btnR50.Name = "btnR50";
            this.btnR50.Size = new System.Drawing.Size(96, 40);
            this.btnR50.TabIndex = 1;
            this.btnR50.Tag = "50";
            this.btnR50.UseVisualStyleBackColor = true;
            this.btnR50.MouseUp += new System.Windows.Forms.MouseEventHandler(this.RupeeMouseUp);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 32);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(247, 36);
            this.label4.TabIndex = 229;
            this.label4.Text = "Balance Refund";
            // 
            // txtAmountPaid
            // 
            this.txtAmountPaid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAmountPaid.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAmountPaid.Location = new System.Drawing.Point(256, 3);
            this.txtAmountPaid.Name = "txtAmountPaid";
            this.txtAmountPaid.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtAmountPaid.Size = new System.Drawing.Size(241, 27);
            this.txtAmountPaid.TabIndex = 1;
            this.txtAmountPaid.TextChanged += new System.EventHandler(this.txtAmountPaid_TextChanged);
            this.txtAmountPaid.Enter += new System.EventHandler(this.TextBox_Enter);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(247, 32);
            this.label1.TabIndex = 0;
            this.label1.Text = "Amount Paid";
            // 
            // tplCash
            // 
            this.tplCash.ColumnCount = 2;
            this.tplCash.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.8F));
            this.tplCash.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.2F));
            this.tplCash.Controls.Add(this.label1, 0, 0);
            this.tplCash.Controls.Add(this.txtAmountPaid, 1, 0);
            this.tplCash.Controls.Add(this.label4, 0, 1);
            this.tplCash.Controls.Add(this.txtRefund, 1, 1);
            this.tplCash.Location = new System.Drawing.Point(8, 183);
            this.tplCash.Name = "tplCash";
            this.tplCash.RowCount = 2;
            this.tplCash.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 47.76119F));
            this.tplCash.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 52.23881F));
            this.tplCash.Size = new System.Drawing.Size(500, 68);
            this.tplCash.TabIndex = 233;
            // 
            // txtRefund
            // 
            this.txtRefund.AutoSize = true;
            this.txtRefund.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRefund.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.txtRefund.ForeColor = System.Drawing.Color.Red;
            this.txtRefund.Location = new System.Drawing.Point(256, 32);
            this.txtRefund.Name = "txtRefund";
            this.txtRefund.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtRefund.Size = new System.Drawing.Size(241, 36);
            this.txtRefund.TabIndex = 230;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.31129F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.68871F));
            this.tableLayoutPanel3.Controls.Add(this.btnClear, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnPay, 1, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(5, 254);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(503, 58);
            this.tableLayoutPanel3.TabIndex = 235;
            // 
            // btnClear
            // 
            this.btnClear.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnClear.BackgroundImage")));
            this.btnClear.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnClear.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnClear.Image = ((System.Drawing.Image)(resources.GetObject("btnClear.Image")));
            this.btnClear.Location = new System.Drawing.Point(3, 3);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(242, 52);
            this.btnClear.TabIndex = 229;
            this.btnClear.Text = "CLEAR PAYMENT";
            this.btnClear.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClear.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnPay
            // 
            this.btnPay.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnPay.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPay.BackgroundImage")));
            this.btnPay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPay.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPay.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnPay.Image = ((System.Drawing.Image)(resources.GetObject("btnPay.Image")));
            this.btnPay.Location = new System.Drawing.Point(251, 3);
            this.btnPay.Name = "btnPay";
            this.btnPay.Size = new System.Drawing.Size(249, 52);
            this.btnPay.TabIndex = 228;
            this.btnPay.Text = "SAVE MEMO";
            this.btnPay.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPay.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnPay.UseVisualStyleBackColor = false;
            this.btnPay.Click += new System.EventHandler(this.btnPay_Click);
            // 
            // tlpCC
            // 
            this.tlpCC.ColumnCount = 2;
            this.tlpCC.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 44.2F));
            this.tlpCC.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 55.8F));
            this.tlpCC.Controls.Add(this.label3, 0, 0);
            this.tlpCC.Controls.Add(this.label5, 0, 1);
            this.tlpCC.Controls.Add(this.ddlCCType, 1, 0);
            this.tlpCC.Controls.Add(this.tableLayoutPanel4, 1, 1);
            this.tlpCC.Location = new System.Drawing.Point(5, 179);
            this.tlpCC.Name = "tlpCC";
            this.tlpCC.RowCount = 2;
            this.tlpCC.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 46.75325F));
            this.tlpCC.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 53.24675F));
            this.tlpCC.Size = new System.Drawing.Size(500, 72);
            this.tlpCC.TabIndex = 236;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 19);
            this.label3.TabIndex = 0;
            this.label3.Text = "Bank Name";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 33);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(212, 19);
            this.label5.TabIndex = 225;
            this.label5.Text = "Credit Card No (Last 4 Digits) :";
            // 
            // ddlCCType
            // 
            this.ddlCCType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlCCType.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.ddlCCType.FormattingEnabled = true;
            this.ddlCCType.Location = new System.Drawing.Point(224, 3);
            this.ddlCCType.Name = "ddlCCType";
            this.ddlCCType.Size = new System.Drawing.Size(273, 27);
            this.ddlCCType.TabIndex = 227;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 3;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 43.16547F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 56.83453F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 105F));
            this.tableLayoutPanel4.Controls.Add(this.label8, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.txtAcceptCardNo, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.txtCCCash, 2, 0);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(224, 36);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(273, 27);
            this.tableLayoutPanel4.TabIndex = 228;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(75, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(74, 19);
            this.label8.TabIndex = 227;
            this.label8.Text = "Cash Paid";
            this.label8.Visible = false;
            // 
            // txtAcceptCardNo
            // 
            this.txtAcceptCardNo.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAcceptCardNo.Location = new System.Drawing.Point(3, 3);
            this.txtAcceptCardNo.MaxLength = 16;
            this.txtAcceptCardNo.Name = "txtAcceptCardNo";
            this.txtAcceptCardNo.Size = new System.Drawing.Size(66, 27);
            this.txtAcceptCardNo.TabIndex = 226;
            // 
            // txtCCCash
            // 
            this.txtCCCash.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCCCash.Location = new System.Drawing.Point(170, 3);
            this.txtCCCash.MaxLength = 16;
            this.txtCCCash.Name = "txtCCCash";
            this.txtCCCash.Size = new System.Drawing.Size(92, 27);
            this.txtCCCash.TabIndex = 228;
            this.txtCCCash.Visible = false;
            // 
            // tlpEwallet
            // 
            this.tlpEwallet.ColumnCount = 2;
            this.tlpEwallet.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpEwallet.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpEwallet.Controls.Add(this.txtEWalletRef, 1, 1);
            this.tlpEwallet.Controls.Add(this.label9, 0, 0);
            this.tlpEwallet.Controls.Add(this.label10, 0, 1);
            this.tlpEwallet.Controls.Add(this.ddlleWallet, 1, 0);
            this.tlpEwallet.Location = new System.Drawing.Point(5, 183);
            this.tlpEwallet.Name = "tlpEwallet";
            this.tlpEwallet.RowCount = 2;
            this.tlpEwallet.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 46.75325F));
            this.tlpEwallet.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 53.24675F));
            this.tlpEwallet.Size = new System.Drawing.Size(500, 65);
            this.tlpEwallet.TabIndex = 237;
            // 
            // txtEWalletRef
            // 
            this.txtEWalletRef.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEWalletRef.Location = new System.Drawing.Point(253, 33);
            this.txtEWalletRef.MaxLength = 16;
            this.txtEWalletRef.Name = "txtEWalletRef";
            this.txtEWalletRef.Size = new System.Drawing.Size(244, 27);
            this.txtEWalletRef.TabIndex = 226;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(3, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(66, 19);
            this.label9.TabIndex = 0;
            this.label9.Text = "E-Wallet";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(3, 30);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 19);
            this.label10.TabIndex = 225;
            this.label10.Text = "Ref No :";
            // 
            // ddlleWallet
            // 
            this.ddlleWallet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlleWallet.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.ddlleWallet.FormattingEnabled = true;
            this.ddlleWallet.Location = new System.Drawing.Point(253, 3);
            this.ddlleWallet.Name = "ddlleWallet";
            this.ddlleWallet.Size = new System.Drawing.Size(244, 27);
            this.ddlleWallet.TabIndex = 227;
            // 
            // tlpMealPass
            // 
            this.tlpMealPass.ColumnCount = 6;
            this.tlpMealPass.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 46F));
            this.tlpMealPass.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 54F));
            this.tlpMealPass.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 69F));
            this.tlpMealPass.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 77F));
            this.tlpMealPass.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 71F));
            this.tlpMealPass.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 83F));
            this.tlpMealPass.Controls.Add(this.ddlMPBalance, 0, 1);
            this.tlpMealPass.Controls.Add(this.label13, 0, 0);
            this.tlpMealPass.Controls.Add(this.ddlMealPass, 1, 0);
            this.tlpMealPass.Controls.Add(this.label14, 2, 0);
            this.tlpMealPass.Controls.Add(this.txtMealPassRef, 3, 0);
            this.tlpMealPass.Controls.Add(this.txtMPCCNo, 3, 1);
            this.tlpMealPass.Controls.Add(this.label2, 4, 0);
            this.tlpMealPass.Controls.Add(this.txtMPAmt, 5, 0);
            this.tlpMealPass.Controls.Add(this.label6, 0, 1);
            this.tlpMealPass.Controls.Add(this.label7, 2, 1);
            this.tlpMealPass.Controls.Add(this.txtMPBalAmt, 5, 1);
            this.tlpMealPass.Controls.Add(this.label11, 4, 1);
            this.tlpMealPass.Location = new System.Drawing.Point(5, 181);
            this.tlpMealPass.Name = "tlpMealPass";
            this.tlpMealPass.RowCount = 2;
            this.tlpMealPass.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 46.75325F));
            this.tlpMealPass.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 53.24675F));
            this.tlpMealPass.Size = new System.Drawing.Size(500, 67);
            this.tlpMealPass.TabIndex = 238;
            // 
            // ddlMPBalance
            // 
            this.ddlMPBalance.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlMPBalance.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.ddlMPBalance.FormattingEnabled = true;
            this.ddlMPBalance.Location = new System.Drawing.Point(95, 34);
            this.ddlMPBalance.Name = "ddlMPBalance";
            this.ddlMPBalance.Size = new System.Drawing.Size(102, 27);
            this.ddlMPBalance.TabIndex = 235;
            this.ddlMPBalance.SelectedIndexChanged += new System.EventHandler(this.ddlMPBalance_SelectedIndexChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(3, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(76, 19);
            this.label13.TabIndex = 0;
            this.label13.Text = "Meal Pass";
            // 
            // ddlMealPass
            // 
            this.ddlMealPass.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ddlMealPass.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.ddlMealPass.FormattingEnabled = true;
            this.ddlMealPass.Location = new System.Drawing.Point(95, 3);
            this.ddlMealPass.Name = "ddlMealPass";
            this.ddlMealPass.Size = new System.Drawing.Size(102, 27);
            this.ddlMealPass.TabIndex = 227;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(203, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(63, 19);
            this.label14.TabIndex = 225;
            this.label14.Text = "Ref No :";
            // 
            // txtMealPassRef
            // 
            this.txtMealPassRef.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMealPassRef.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMealPassRef.Location = new System.Drawing.Point(272, 3);
            this.txtMealPassRef.MaxLength = 16;
            this.txtMealPassRef.Name = "txtMealPassRef";
            this.txtMealPassRef.Size = new System.Drawing.Size(71, 27);
            this.txtMealPassRef.TabIndex = 226;
            this.txtMealPassRef.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMealPassRef_KeyPress);
            // 
            // txtMPCCNo
            // 
            this.txtMPCCNo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMPCCNo.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMPCCNo.Location = new System.Drawing.Point(272, 34);
            this.txtMPCCNo.MaxLength = 16;
            this.txtMPCCNo.Name = "txtMPCCNo";
            this.txtMPCCNo.Size = new System.Drawing.Size(71, 27);
            this.txtMPCCNo.TabIndex = 230;
            this.txtMPCCNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMPCCNo_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(349, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 19);
            this.label2.TabIndex = 228;
            this.label2.Text = "Amount";
            // 
            // txtMPAmt
            // 
            this.txtMPAmt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMPAmt.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMPAmt.Location = new System.Drawing.Point(420, 3);
            this.txtMPAmt.MaxLength = 16;
            this.txtMPAmt.Name = "txtMPAmt";
            this.txtMPAmt.Size = new System.Drawing.Size(77, 27);
            this.txtMPAmt.TabIndex = 232;
            this.txtMPAmt.Validating += new System.ComponentModel.CancelEventHandler(this.txtMPAmt_Validating);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(3, 31);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(86, 19);
            this.label6.TabIndex = 229;
            this.label6.Text = "Balance On";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(203, 31);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 19);
            this.label7.TabIndex = 233;
            this.label7.Text = "No";
            // 
            // txtMPBalAmt
            // 
            this.txtMPBalAmt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMPBalAmt.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMPBalAmt.Location = new System.Drawing.Point(420, 34);
            this.txtMPBalAmt.MaxLength = 16;
            this.txtMPBalAmt.Name = "txtMPBalAmt";
            this.txtMPBalAmt.Size = new System.Drawing.Size(77, 27);
            this.txtMPBalAmt.TabIndex = 231;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(349, 31);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 19);
            this.label11.TabIndex = 236;
            this.label11.Text = "Amount";
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(3, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(61, 30);
            this.label12.TabIndex = 229;
            this.label12.Text = "Coupon :";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 5;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60.43166F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 39.56834F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 131F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 147F));
            this.tableLayoutPanel2.Controls.Add(this.txtCouponNo, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.label12, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtCouponPrefix, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtCouponAmt, 4, 0);
            this.tableLayoutPanel2.Controls.Add(this.label15, 3, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(8, 145);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(500, 30);
            this.tableLayoutPanel2.TabIndex = 239;
            // 
            // txtCouponNo
            // 
            this.txtCouponNo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCouponNo.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.txtCouponNo.Location = new System.Drawing.Point(114, 3);
            this.txtCouponNo.Name = "txtCouponNo";
            this.txtCouponNo.Size = new System.Drawing.Size(104, 27);
            this.txtCouponNo.TabIndex = 232;
            this.txtCouponNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCouponNo_KeyPress);
            this.txtCouponNo.Validating += new System.ComponentModel.CancelEventHandler(this.txtCouponNo_Validating);
            // 
            // txtCouponPrefix
            // 
            this.txtCouponPrefix.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCouponPrefix.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.txtCouponPrefix.Location = new System.Drawing.Point(70, 3);
            this.txtCouponPrefix.Name = "txtCouponPrefix";
            this.txtCouponPrefix.Size = new System.Drawing.Size(38, 27);
            this.txtCouponPrefix.TabIndex = 231;
            this.txtCouponPrefix.Validating += new System.ComponentModel.CancelEventHandler(this.txtCouponPrefix_Validating);
            // 
            // txtCouponAmt
            // 
            this.txtCouponAmt.AutoSize = true;
            this.txtCouponAmt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCouponAmt.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.txtCouponAmt.Location = new System.Drawing.Point(355, 0);
            this.txtCouponAmt.Name = "txtCouponAmt";
            this.txtCouponAmt.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtCouponAmt.Size = new System.Drawing.Size(142, 30);
            this.txtCouponAmt.TabIndex = 230;
            this.txtCouponAmt.Text = "label2";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label15.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(224, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(125, 30);
            this.label15.TabIndex = 233;
            this.label15.Text = "Coupon Amount";
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.lblError.ForeColor = System.Drawing.Color.Red;
            this.lblError.Location = new System.Drawing.Point(8, 576);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(50, 19);
            this.lblError.TabIndex = 240;
            this.lblError.Text = "Errors";
            // 
            // lblPayable
            // 
            this.lblPayable.AutoSize = true;
            this.lblPayable.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.lblPayable.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblPayable.Location = new System.Drawing.Point(252, 498);
            this.lblPayable.Name = "lblPayable";
            this.lblPayable.Size = new System.Drawing.Size(109, 19);
            this.lblPayable.TabIndex = 241;
            this.lblPayable.Text = "Total Payable :";
            // 
            // txtPayable
            // 
            this.txtPayable.Enabled = false;
            this.txtPayable.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.txtPayable.ForeColor = System.Drawing.Color.Blue;
            this.txtPayable.Location = new System.Drawing.Point(401, 495);
            this.txtPayable.Name = "txtPayable";
            this.txtPayable.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtPayable.Size = new System.Drawing.Size(104, 27);
            this.txtPayable.TabIndex = 242;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label16.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.label16.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label16.Location = new System.Drawing.Point(3, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(110, 22);
            this.label16.TabIndex = 243;
            this.label16.Text = "CASH:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label17.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.label17.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label17.Location = new System.Drawing.Point(258, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(90, 22);
            this.label17.TabIndex = 244;
            this.label17.Text = "CARD:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label18.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.label18.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label18.Location = new System.Drawing.Point(258, 22);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(90, 23);
            this.label18.TabIndex = 245;
            this.label18.Text = "MEAL PASS:";
            // 
            // lblPaidCash
            // 
            this.lblPaidCash.AutoSize = true;
            this.lblPaidCash.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPaidCash.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.lblPaidCash.ForeColor = System.Drawing.Color.Blue;
            this.lblPaidCash.Location = new System.Drawing.Point(119, 0);
            this.lblPaidCash.Name = "lblPaidCash";
            this.lblPaidCash.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblPaidCash.Size = new System.Drawing.Size(133, 22);
            this.lblPaidCash.TabIndex = 246;
            this.lblPaidCash.Text = "CASH AMT";
            // 
            // lblPaidCC
            // 
            this.lblPaidCC.AutoSize = true;
            this.lblPaidCC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPaidCC.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.lblPaidCC.ForeColor = System.Drawing.Color.Blue;
            this.lblPaidCC.Location = new System.Drawing.Point(354, 0);
            this.lblPaidCC.Name = "lblPaidCC";
            this.lblPaidCC.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblPaidCC.Size = new System.Drawing.Size(140, 22);
            this.lblPaidCC.TabIndex = 247;
            this.lblPaidCC.Text = "CARD AMT";
            // 
            // lblPaidMealPass
            // 
            this.lblPaidMealPass.AutoSize = true;
            this.lblPaidMealPass.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPaidMealPass.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.lblPaidMealPass.ForeColor = System.Drawing.Color.Blue;
            this.lblPaidMealPass.Location = new System.Drawing.Point(354, 22);
            this.lblPaidMealPass.Name = "lblPaidMealPass";
            this.lblPaidMealPass.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblPaidMealPass.Size = new System.Drawing.Size(140, 23);
            this.lblPaidMealPass.TabIndex = 248;
            this.lblPaidMealPass.Text = "MP AMT";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label19.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.label19.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label19.Location = new System.Drawing.Point(3, 22);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(110, 23);
            this.label19.TabIndex = 249;
            this.label19.Text = "e-Wallet:";
            // 
            // lblPaidWallet
            // 
            this.lblPaidWallet.AutoSize = true;
            this.lblPaidWallet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPaidWallet.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.lblPaidWallet.ForeColor = System.Drawing.Color.Blue;
            this.lblPaidWallet.Location = new System.Drawing.Point(119, 22);
            this.lblPaidWallet.Name = "lblPaidWallet";
            this.lblPaidWallet.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblPaidWallet.Size = new System.Drawing.Size(133, 23);
            this.lblPaidWallet.TabIndex = 250;
            this.lblPaidWallet.Text = "Wallet AMT";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 4;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45.61404F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 54.38596F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 96F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 145F));
            this.tableLayoutPanel5.Controls.Add(this.label16, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.lblPaidMealPass, 3, 1);
            this.tableLayoutPanel5.Controls.Add(this.lblPaidWallet, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.label18, 2, 1);
            this.tableLayoutPanel5.Controls.Add(this.lblPaidCash, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.label19, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.label17, 2, 0);
            this.tableLayoutPanel5.Controls.Add(this.lblPaidCC, 3, 0);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(11, 528);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(497, 45);
            this.tableLayoutPanel5.TabIndex = 251;
            // 
            // Payment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(514, 597);
            this.ControlBox = false;
            this.Controls.Add(this.tableLayoutPanel5);
            this.Controls.Add(this.txtPayable);
            this.Controls.Add(this.lblPayable);
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.tlpMealPass);
            this.Controls.Add(this.tlpEwallet);
            this.Controls.Add(this.tlpCC);
            this.Controls.Add(this.tableLayoutPanel3);
            this.Controls.Add(this.gbRupees);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.gbNumbers);
            this.Controls.Add(this.lblBillAmtDisplay);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tplCash);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Payment";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.Payment_Load);
            this.panel1.ResumeLayout(false);
            this.gbNumbers.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.gbRupees.ResumeLayout(false);
            this.tplCash.ResumeLayout(false);
            this.tplCash.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tlpCC.ResumeLayout(false);
            this.tlpCC.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tlpEwallet.ResumeLayout(false);
            this.tlpEwallet.PerformLayout();
            this.tlpMealPass.ResumeLayout(false);
            this.tlpMealPass.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblUserAddEdit;
        private System.Windows.Forms.Label lblExit;
        private System.Windows.Forms.Label lblBillAmtDisplay;
        private System.Windows.Forms.Button btnCard;
        private System.Windows.Forms.Button btnCash;
        private CodeVendor.Controls.Grouper gbNumbers;
        private System.Windows.Forms.Button btnback;
        private System.Windows.Forms.Button btn00;
        private System.Windows.Forms.Button btn9;
        private System.Windows.Forms.Button btn8;
        private System.Windows.Forms.Button btn7;
        private System.Windows.Forms.Button btn6;
        private System.Windows.Forms.Button btn5;
        private System.Windows.Forms.Button btn4;
        private System.Windows.Forms.Button btn3;
        private System.Windows.Forms.Button btn2;
        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.Button btn0;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox gbRupees;
        private System.Windows.Forms.Button btnR1000;
        private System.Windows.Forms.Button btnR500;
        private System.Windows.Forms.Button btnR100;
        private System.Windows.Forms.Button btnR50;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtAmountPaid;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tplCash;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnPay;
        private System.Windows.Forms.Label txtRefund;
        private System.Windows.Forms.Button btneWallet;
        private System.Windows.Forms.Button btnMealPass;
        private System.Windows.Forms.TableLayoutPanel tlpCC;
        private System.Windows.Forms.TextBox txtAcceptCardNo;
        private System.Windows.Forms.Label label3;
        internal System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox ddlCCType;
        private System.Windows.Forms.TableLayoutPanel tlpEwallet;
        private System.Windows.Forms.TextBox txtEWalletRef;
        private System.Windows.Forms.Label label9;
        internal System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox ddlleWallet;
        private System.Windows.Forms.TableLayoutPanel tlpMealPass;
        internal System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtMealPassRef;
        private System.Windows.Forms.Label label13;
        internal System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox ddlMealPass;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TextBox txtCouponNo;
        private System.Windows.Forms.Label txtCouponAmt;
        private System.Windows.Forms.TextBox txtCouponPrefix;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtCCCash;
        private System.Windows.Forms.ComboBox ddlMPBalance;
        private System.Windows.Forms.TextBox txtMPCCNo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtMPAmt;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtMPBalAmt;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblError;
        internal System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lblPayable;
        private System.Windows.Forms.TextBox txtPayable;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label lblPaidCash;
        private System.Windows.Forms.Label lblPaidCC;
        private System.Windows.Forms.Label lblPaidMealPass;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label lblPaidWallet;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
    }
}