﻿namespace FoodSmartDesktop.POS
{
    partial class ReprintBill
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReprintBill));
            this.lblInternalNoMessage = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ddlBillNo = new System.Windows.Forms.ComboBox();
            this.btnCont = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblUserAddEdit = new System.Windows.Forms.Label();
            this.lblExit = new System.Windows.Forms.Label();
            this.Label8 = new System.Windows.Forms.Label();
            this.txtPw = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblInternalNoMessage
            // 
            this.lblInternalNoMessage.AutoSize = true;
            this.lblInternalNoMessage.ForeColor = System.Drawing.Color.Red;
            this.lblInternalNoMessage.Location = new System.Drawing.Point(144, 92);
            this.lblInternalNoMessage.Name = "lblInternalNoMessage";
            this.lblInternalNoMessage.Size = new System.Drawing.Size(35, 13);
            this.lblInternalNoMessage.TabIndex = 222;
            this.lblInternalNoMessage.Text = "label2";
            // 
            // txtUserName
            // 
            this.txtUserName.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUserName.Location = new System.Drawing.Point(175, 67);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.PasswordChar = '*';
            this.txtUserName.Size = new System.Drawing.Size(243, 20);
            this.txtUserName.TabIndex = 216;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(7, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 19);
            this.label1.TabIndex = 221;
            this.label1.Text = "User Name:";
            // 
            // ddlBillNo
            // 
            this.ddlBillNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlBillNo.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddlBillNo.FormattingEnabled = true;
            this.ddlBillNo.Location = new System.Drawing.Point(175, 143);
            this.ddlBillNo.Name = "ddlBillNo";
            this.ddlBillNo.Size = new System.Drawing.Size(243, 32);
            this.ddlBillNo.TabIndex = 217;
            // 
            // btnCont
            // 
            this.btnCont.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCont.BackgroundImage")));
            this.btnCont.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCont.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCont.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCont.Image = ((System.Drawing.Image)(resources.GetObject("btnCont.Image")));
            this.btnCont.Location = new System.Drawing.Point(257, 199);
            this.btnCont.Name = "btnCont";
            this.btnCont.Size = new System.Drawing.Size(161, 49);
            this.btnCont.TabIndex = 220;
            this.btnCont.Text = "Continue";
            this.btnCont.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCont.UseVisualStyleBackColor = true;
            this.btnCont.Click += new System.EventHandler(this.btnCont_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(199)))), ((int)(((byte)(233)))));
            this.panel1.Controls.Add(this.lblUserAddEdit);
            this.panel1.Controls.Add(this.lblExit);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(432, 52);
            this.panel1.TabIndex = 219;
            // 
            // lblUserAddEdit
            // 
            this.lblUserAddEdit.BackColor = System.Drawing.Color.Transparent;
            this.lblUserAddEdit.Font = new System.Drawing.Font("Arial", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserAddEdit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.lblUserAddEdit.Location = new System.Drawing.Point(14, 16);
            this.lblUserAddEdit.Name = "lblUserAddEdit";
            this.lblUserAddEdit.Size = new System.Drawing.Size(159, 22);
            this.lblUserAddEdit.TabIndex = 161;
            this.lblUserAddEdit.Text = "Re-Print Bill";
            // 
            // lblExit
            // 
            this.lblExit.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExit.ForeColor = System.Drawing.Color.DarkRed;
            this.lblExit.Image = ((System.Drawing.Image)(resources.GetObject("lblExit.Image")));
            this.lblExit.Location = new System.Drawing.Point(366, 5);
            this.lblExit.Name = "lblExit";
            this.lblExit.Size = new System.Drawing.Size(63, 47);
            this.lblExit.TabIndex = 1;
            this.lblExit.Click += new System.EventHandler(this.lblExit_Click);
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label8.Location = new System.Drawing.Point(7, 150);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(65, 19);
            this.Label8.TabIndex = 218;
            this.Label8.Text = "Bill No:";
            // 
            // txtPw
            // 
            this.txtPw.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPw.Location = new System.Drawing.Point(175, 106);
            this.txtPw.Name = "txtPw";
            this.txtPw.PasswordChar = '*';
            this.txtPw.Size = new System.Drawing.Size(243, 20);
            this.txtPw.TabIndex = 223;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(7, 107);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 19);
            this.label2.TabIndex = 224;
            this.label2.Text = "Password:";
            // 
            // ReprintBill
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(433, 260);
            this.ControlBox = false;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtPw);
            this.Controls.Add(this.lblInternalNoMessage);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ddlBillNo);
            this.Controls.Add(this.btnCont);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.Label8);
            this.Name = "ReprintBill";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.ReprintBill_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblInternalNoMessage;
        private System.Windows.Forms.TextBox txtUserName;
        internal System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox ddlBillNo;
        private System.Windows.Forms.Button btnCont;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblUserAddEdit;
        private System.Windows.Forms.Label lblExit;
        internal System.Windows.Forms.Label Label8;
        private System.Windows.Forms.TextBox txtPw;
        internal System.Windows.Forms.Label label2;
    }
}