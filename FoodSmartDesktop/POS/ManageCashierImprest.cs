﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using FoodSmart.DAL;
using FoodSmart.BLL;
using FoodSmart.Entity;
using FoodSmart.GeneralUtilities;
using FoodSmart.Utilities;
using FoodSmart.Common;

namespace FoodSmartDesktop.POS
{
    public partial class ManageCashierTran : Form
    {
        BindingSource source1 = new BindingSource();
        private int PgSize = 12;
        private int CurrentPageIndex = 1;
        private int TotalPage = 0;
        private DataSet dsPage;

        public ManageCashierTran()
        {
            InitializeComponent();
        }

        #region Public Method
        private void ManageCashierTran_Load(object sender, EventArgs e)
        {
            dvgRest.ColumnCount = 7;
            this.ddlLoc.SelectedIndexChanged -= new System.EventHandler(this.ddlLoc_SelectedIndexChanged);
            this.ddlTranType.SelectedIndexChanged -= new System.EventHandler(this.ddlTranType_SelectedIndexChanged);
            SetWatermarkText();
            LoadDDL();
            LoadTranType();

            SearchCriteria searchCriteria = new SearchCriteria();
            BuildDefaultSearch(searchCriteria);

            LoadCashierImprest(searchCriteria);

            this.ddlLoc.SelectedIndexChanged += new System.EventHandler(this.ddlLoc_SelectedIndexChanged);
            this.ddlTranType.SelectedIndexChanged += new System.EventHandler(this.ddlTranType_SelectedIndexChanged);
            foreach (DataGridViewRow row in dvgRest.Rows)
            {
                row.Height = 25;
            }

            lbltoday.Text += " : " + GeneralFunctions.LoginInfo.CurrDate.ToString("dd-MMM-yyyy");
            lblUserName.Text += GeneralFunctions.LoginInfo.UserFullName;
            ddlLoc.Focus();
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAddTran_Click(object sender, EventArgs e)
        {
            this.Hide();
            POS.AddEditCashierImprest User = new POS.AddEditCashierImprest(0);
            if (User.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {

            }
            else
            {
                this.Show();
                //dvgItem.Columns.Remove("Dimg");
                dvgRest.Columns.Remove("Eimg");
                dvgRest.Columns.Remove("Dimg");
                SearchCriteria searchCriteria = new SearchCriteria();
                BuildDefaultSearch(searchCriteria);
                LoadCashierImprest(searchCriteria);
                foreach (DataGridViewRow row in dvgRest.Rows)
                {
                    row.Height = 25;
                }
            }
        }

        private void dvgRest_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int Reply;
            int i;
            if (dvgRest.RowCount == 0)
                return;
            else
                i = dvgRest.CurrentRow.Index;

            if (dvgRest.Columns[e.ColumnIndex].Name == "Dimg")
            {
                //if (!CheckForDeleted(Convert.ToInt32(dvgRest.CurrentRow.Cells[0].Value)))
                //{
                //    MessageBox.Show("Record Already Deleted.", "Delete Record", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                //    return;
                //}

                Reply = Convert.ToInt32(MessageBox.Show("Want to Delete this Record?", "Delete Record", MessageBoxButtons.OKCancel, MessageBoxIcon.Question));
                if (Reply == 1)
                {
                    Reply = Convert.ToInt32(dvgRest.CurrentRow.Cells[0].Value);

                    RestaurantBLL restBLL = new RestaurantBLL();
                    restBLL.DeleteRest(Reply);

                    dvgRest.Columns.Remove("Dimg");
                    dvgRest.Columns.Remove("Eimg");
                    SearchCriteria searchCriteria = new SearchCriteria();
                    BuildDefaultSearch(searchCriteria);
                    LoadCashierImprest(searchCriteria);
                    foreach (DataGridViewRow row in dvgRest.Rows)
                    {
                        row.Height = 25;
                    }
                }
            }

            if (dvgRest.Columns[e.ColumnIndex].Name == "Eimg")
            {
               POS.AddEditCashierImprest frm1 = new POS.AddEditCashierImprest(Convert.ToInt32(dvgRest.CurrentRow.Cells[0].Value));
                this.Hide();

                if (frm1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    this.Hide();
                }
                else
                {
                    this.Show();
                    dvgRest.Columns.Remove("Dimg");
                    dvgRest.Columns.Remove("Eimg");
                    SearchCriteria searchCriteria = new SearchCriteria();
                    BuildDefaultSearch(searchCriteria);
                    LoadCashierImprest(searchCriteria);
                    foreach (DataGridViewRow row in dvgRest.Rows)
                    {
                        row.Height = 25;
                    }
                }
            }
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            CheckFilterCondition();
        }

        private void ddlLoc_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckFilterCondition();
        }

        private void ddlTranType_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckFilterCondition();
        }


        private void lblExit_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnAddTran_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void lblExit_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnAddTran_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        #endregion

        #region "Private Methods"
        private void LoadTranType()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            Dictionary<string, string> cStatus = new Dictionary<string, string>();
            cStatus.Add("R", "Receive");
            cStatus.Add("P", "Paid");

            ddlTranType.DataSource = new BindingSource(cStatus, null);
            ddlTranType.DisplayMember = "Value";
            ddlTranType.ValueMember = "Key";
            ddlTranType.SelectedIndex = -1;
        }

        private void LoadCashierImprest(SearchCriteria searchCriteria)
        {
            string path = Application.StartupPath + "\\";
            DataView view1;

            DataGridViewCellStyle style = new DataGridViewCellStyle();
            dvgRest.EnableHeadersVisualStyles = false;
            style.ForeColor = Color.Blue;

            dvgRest.AutoGenerateColumns = false;

            dvgRest.Columns[0].Name = "pk_TranID";
            dvgRest.Columns[1].Name = "Location Name";
            dvgRest.Columns[2].Name = "Cashier Name";
            dvgRest.Columns[3].Name = "Tran No";    
            dvgRest.Columns[4].Name = "Tran Date";
            dvgRest.Columns[5].Name = "Tran Type";
            dvgRest.Columns[6].Name = "Amount";
            

            dvgRest.Columns[0].HeaderText = "Tran ID";
            dvgRest.Columns[1].HeaderText = "Location Name";
            dvgRest.Columns[2].HeaderText = "Cashier Name";
            dvgRest.Columns[3].HeaderText = "Tran No";
            dvgRest.Columns[4].HeaderText = "Tran Date";
            dvgRest.Columns[5].HeaderText = "Tran Type";
            dvgRest.Columns[6].HeaderText = "Amount";


            dvgRest.Columns[0].DataPropertyName = "pk_TranID";
            dvgRest.Columns[1].DataPropertyName = "LocationName";
            dvgRest.Columns[2].DataPropertyName = "UserName";
            dvgRest.Columns[3].DataPropertyName = "CashTranNo";
            dvgRest.Columns[4].DataPropertyName = "CashTranDate";
            dvgRest.Columns[5].DataPropertyName = "TranType";
            dvgRest.Columns[6].DataPropertyName = "TranAmount";

            dvgRest.Columns[1].Width = 200;
            dvgRest.Columns[2].Width = 200;
            dvgRest.Columns[3].Width = 100;
            dvgRest.Columns[4].Width = 120;
            dvgRest.Columns[5].Width = 100;
            dvgRest.Columns[6].Width = 120;

            dvgRest.Columns[1].SortMode = DataGridViewColumnSortMode.Automatic;
            dvgRest.Columns[2].SortMode = DataGridViewColumnSortMode.Automatic;
            dvgRest.Columns[3].SortMode = DataGridViewColumnSortMode.Automatic;
            dvgRest.Columns[4].SortMode = DataGridViewColumnSortMode.Automatic;
            dvgRest.Columns[5].SortMode = DataGridViewColumnSortMode.Automatic;

            dvgRest.Columns[6].SortMode = DataGridViewColumnSortMode.NotSortable;

            dvgRest.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            //dvgRest.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            dvgRest.Columns[6].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
            //dvgRest.Columns[4].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;

            dvgRest.Columns[0].Visible = false;
            //dvgRest.Columns[9].Visible = false;

            dvgRest.Columns[4].DefaultCellStyle.Format = "dd-MM-yyyy";
            dvgRest.Columns[6].DefaultCellStyle.Format = "#######0.00";

            DataGridViewImageColumn Editimg = new DataGridViewImageColumn();
            Image Editimage = Image.FromFile(path + ConfigurationManager.AppSettings["imageFolder"] + "edit-icon.png");
            Editimg.Image = Editimage;
            dvgRest.Columns.Add(Editimg);
            Editimg.HeaderText = "Edit";
            Editimg.Name = "Eimg";
            Editimg.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            Editimg.Width = 40;

            DataGridViewImageColumn Delimg = new DataGridViewImageColumn();
            Image Delimage = Image.FromFile(path + ConfigurationManager.AppSettings["imageFolder"] + "delete-icon.png");
            Delimg.Image = Delimage;
            dvgRest.Columns.Add(Delimg);
            Delimg.HeaderText = "Del";
            Delimg.Name = "Dimg";
            Delimg.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            Delimg.Width = 40;

            dvgRest.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dvgRest.MultiSelect = false;

            ImprestTranBLL restbll = new ImprestTranBLL();
            dsPage = restbll.GetAllTransactions(searchCriteria);
            if (dsPage.Tables[0].Rows.Count == 0)
                view1 = null;
            else
            {
                TotalPage = GeneralFunctions.CalculateTotalPages(dsPage.Tables[0], PgSize);
                view1 = new DataView(GeneralFunctions.GetCurrentRecords(0, dsPage.Tables[0], PgSize, "pk_TranID"));
                CurrentPageIndex = 0;
                lblPageNo.Text = 1.ToString() + " / " + TotalPage.ToString();

            }
            source1.DataSource = view1;

            dvgRest.DataSource = source1;
            dvgRest.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            dvgRest.AllowUserToResizeColumns = false;
            dvgRest.AllowUserToAddRows = false;
            dvgRest.ReadOnly = true;
            dvgRest.Columns[1].HeaderCell.Style = style;
            dvgRest.Columns[2].HeaderCell.Style = style;
            dvgRest.Columns[3].HeaderCell.Style = style;
            dvgRest.Columns[4].HeaderCell.Style = style;
        }

        private void BuildDefaultSearch(SearchCriteria criteria)
        {
            criteria.BillType = "";
            criteria.LocName = "";
            criteria.Date = null;
            //criteria.CardType = ddlLoc.SelectedValue.ToString();
        }

        private void BuildSearchCriteria(SearchCriteria criteria)
        {
            criteria.Date = dtTranDate.Value;
            //criteria.CardName = txtName.Text;
            criteria.LocName = ddlLoc.Text.ToString();
        }

        private void CheckFilterCondition()
        {
            SearchCriteria searchCriteria = new SearchCriteria();
            BuildSearchCriteria(searchCriteria);
            dvgRest.Columns.Remove("Dimg");
            dvgRest.Columns.Remove("Eimg");
            LoadCashierImprest(searchCriteria);

            //LoadRestaurants(searchCriteria);
            foreach (DataGridViewRow row in dvgRest.Rows)
            {
                row.Height = 25;
            }
        }

        private void SetWatermarkText()
        {

            //WatermarkText.SetWatermark(txtRestName, "Card No");
            //WatermarkText.SetWatermark(txtName, "User / Restaurant Name");
        }

        private void LoadDDL()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            //populate Locations
            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.QueryStat = "L";

            ds = new LocationBLL().GetAllLoc(searchCriteria);
            dt = ds.Tables[0];
            ddlLoc.DataSource = dt;
            ddlLoc.DisplayMember = "LocationName";
            ddlLoc.ValueMember = "pk_LocID";
            ddlLoc.SelectedIndex = 0;


            ////DataRow row = new DataRow();

            //Dictionary<string, string> cStatus = new Dictionary<string, string>();
            //cStatus.Add("R", "Restaurant");
            //cStatus.Add("P", "Parking");
            //ddlLoc.DataSource = new BindingSource(cStatus, null);
            //ddlLoc.DisplayMember = "Value";
            //ddlLoc.ValueMember = "Key";
            //ddlLoc.SelectedIndex = 0;

        }

       
        #endregion


       
    }
}
