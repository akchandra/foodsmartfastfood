﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Printing;
using System.Configuration;
using FoodSmart.BLL;
using FoodSmart.Utilities;

namespace FoodSmartDesktop.POS
{
    public partial class DirectPrintCashierRpt : Form
    {
        private Font printFont;
        private int Cashier;

        public DirectPrintCashierRpt(int CashierID)
        {
            InitializeComponent();
            Cashier = CashierID;
            try
            {
                string prnName = ConfigurationManager.AppSettings["PrinterName"];
                PrinterSettings ps = new PrinterSettings();
                PrintDocument pd = new PrintDocument();
                //streamToPrint = new StreamWriter("D:\\MyFile.txt");
                try
                {
                    foreach (string printer in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
                    {
                        if (printer == prnName)
                        {
                            pd.PrinterSettings.PrinterName = printer;
                            break;
                        }
                    }

                    Cashier = CashierID;
                    pd.PrintPage += new PrintPageEventHandler(this.pd_PrintPage);
                    pd.Print();
                }
                finally
                {
                    pd.Dispose();
                    //streamToPrint.Close();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void DirectPrintCashierRpt_Load(object sender, EventArgs e)
        {

        }

        private void pd_PrintPage(object sender, PrintPageEventArgs e)
        {
            Single yPos = 0;

            string TempStr = "";
            float x = 10;
            float y = 5;

            float width = 270.0F; // max width I found through trial and error
            float height = 0F;


            Font drawFontArial12Bold = new Font("Arial", 12, FontStyle.Bold);
            Font drawFontArial10Regular = new Font("Arial", 10, FontStyle.Regular);
            Font drawFontArial8Bold = new Font("Arial", 8, FontStyle.Bold);

            Single topMargin = e.MarginBounds.Top;
            string text = "";
            StringFormat sf = new StringFormat();
            // Set format of string.
            StringFormat drawFormatCenter = new StringFormat();
            drawFormatCenter.Alignment = StringAlignment.Center;
            StringFormat drawFormatLeft = new StringFormat();
            drawFormatLeft.Alignment = StringAlignment.Near;
            StringFormat drawFormatRight = new StringFormat();
            drawFormatRight.Alignment = StringAlignment.Far;

            InvoiceBLL invbll = new InvoiceBLL();
            DataSet ds = invbll.GetCashierSummaryForPrinting(Cashier);

            //Image img = Image.FromFile("logo.bmp");
            //Rectangle logo = new Rectangle(40, 40, 50, 50);



            //e.Graphics.DrawImage(img, logo);
            //text = "Testing!";
            //leftMargin = Convert.ToInt32((40 - text.Length) / 2);
            //e.Graphics.DrawString("Testing!", printFont, Brushes.Black, leftMargin, yPos, drawFormatCenter);

            //e.Graphics.DrawString("testing Font", printFont, Brushes.Black, new RectangleF(x, y, width, height), drawFormatCenter);
            e.Graphics.DrawString(ds.Tables[1].Rows[0]["Company_Name"].ToString(), drawFontArial12Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatCenter);
            y += (1 * drawFontArial12Bold.GetHeight(e.Graphics));
            e.Graphics.DrawString(ds.Tables[1].Rows[0]["Address1"].ToString(), drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatCenter);
            y += (1 * drawFontArial10Regular.GetHeight(e.Graphics));
            e.Graphics.DrawString("As On  :" + Convert.ToDateTime(DateTime.Now).ToString("dd-MMM-yyyy"), drawFontArial8Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatCenter);
            y += (1 * drawFontArial8Bold.GetHeight(e.Graphics));
            e.Graphics.DrawString("Cashier :" + GeneralFunctions.LoginInfo.UserFullName, drawFontArial8Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatCenter);
            y += (1 * drawFontArial8Bold.GetHeight(e.Graphics));
            e.Graphics.DrawLine(new Pen(Brushes.Black, 1), 1, y, 290, y);
            //e.Graphics.DrawString(string.Concat(Enumerable.Repeat("-", 55)), drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
            y += (1 * drawFontArial8Bold.GetHeight(e.Graphics));
            //e.Graphics.DrawString("Dated  : " + Convert.ToDateTime(ds.Tables[0].Rows[0]["TranDate"]).ToString("dd-MMM-yyyy") + string.Concat(Enumerable.Repeat(" ", 20)) + Convert.ToDateTime(ds.Tables[0].Rows[0]["TranDate"]).ToString("hh:mm"), drawFontArial8Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
            ////e.Graphics.DrawString("New list for testing", printFont, Brushes.Black, leftMargin, yPos, drawFormatLeft);
            //y += (1 * printFont.GetHeight(e.Graphics));
            e.Graphics.DrawString("Activity", drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
            e.Graphics.DrawString("Nos", drawFontArial10Regular, Brushes.Black, new RectangleF(160, y, width, height), drawFormatLeft);
            e.Graphics.DrawString("Amount (Rs.)", drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
            y += (1 * drawFontArial8Bold.GetHeight(e.Graphics));
            e.Graphics.DrawLine(new Pen(Brushes.Black, 1), 1, y, 290, y);
            y += (1 * drawFontArial8Bold.GetHeight(e.Graphics));

            e.Graphics.DrawString("Activated:", drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
            e.Graphics.DrawString(Convert.ToDecimal(ds.Tables[0].Rows[0]["ActNos"]).ToString("#####0"), drawFontArial10Regular, Brushes.Black, new RectangleF(160, y, width, height), drawFormatLeft);
            y += (1 * drawFontArial10Regular.GetHeight(e.Graphics));

            e.Graphics.DrawString("Cash    :", drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
            e.Graphics.DrawString(Convert.ToDecimal(ds.Tables[0].Rows[0]["ActCashAmount"]).ToString("######0.00"), drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
            y += (1 * drawFontArial10Regular.GetHeight(e.Graphics));

            e.Graphics.DrawString("Card    :", drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
            e.Graphics.DrawString(Convert.ToDecimal(ds.Tables[0].Rows[0]["ActCCAmount"]).ToString("######0.00"), drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
            y += (1 * drawFontArial10Regular.GetHeight(e.Graphics));

            e.Graphics.DrawString("Meal    :", drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
            e.Graphics.DrawString(Convert.ToDecimal(ds.Tables[0].Rows[0]["ActMealAmount"]).ToString("######0.00"), drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
            y += (1 * drawFontArial10Regular.GetHeight(e.Graphics));

            e.Graphics.DrawString("Refilled:", drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
            e.Graphics.DrawString(Convert.ToDecimal(ds.Tables[0].Rows[0]["RefillNos"]).ToString("#####0"), drawFontArial10Regular, Brushes.Black, new RectangleF(160, y, width, height), drawFormatLeft);
            y += (1 * drawFontArial10Regular.GetHeight(e.Graphics));

            e.Graphics.DrawString("Cash   :", drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
            e.Graphics.DrawString(Convert.ToDecimal(ds.Tables[0].Rows[0]["RefillCashAmount"]).ToString("######0.00"), drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
            y += (1 * drawFontArial10Regular.GetHeight(e.Graphics));

            e.Graphics.DrawString("Card   :", drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
            e.Graphics.DrawString(Convert.ToDecimal(ds.Tables[0].Rows[0]["RefillCCAmount"]).ToString("######0.00"), drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
            y += (1 * drawFontArial10Regular.GetHeight(e.Graphics));

            e.Graphics.DrawString("Meal   :", drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
            e.Graphics.DrawString(Convert.ToDecimal(ds.Tables[0].Rows[0]["RefillMealAmount"]).ToString("######0.00"), drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
            y += (1 * drawFontArial10Regular.GetHeight(e.Graphics));

            e.Graphics.DrawString("Refund  : ", drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
            e.Graphics.DrawString(Convert.ToDecimal(ds.Tables[0].Rows[0]["RefundNos"]).ToString("#####0"), drawFontArial10Regular, Brushes.Black, new RectangleF(160, y, width, height), drawFormatLeft);
            e.Graphics.DrawString(Convert.ToDecimal(ds.Tables[0].Rows[0]["RefndAmount"]).ToString("######0.00"), drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
            y += (1 * drawFontArial10Regular.GetHeight(e.Graphics));

            e.Graphics.DrawString("Cash Balance     : ", drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
            e.Graphics.DrawString((Convert.ToDecimal(ds.Tables[0].Rows[0]["ActCashAmount"]) + Convert.ToDecimal(ds.Tables[0].Rows[0]["RefillCashAmount"]) - Convert.ToDecimal(ds.Tables[0].Rows[0]["RefndAmount"])).ToString("######0.00"), drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
            y += (1 * drawFontArial10Regular.GetHeight(e.Graphics));
            e.Graphics.DrawLine(new Pen(Brushes.Black, 1), 1, y, 290, y);

            e.Graphics.DrawString("Card Balance     : ", drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
            e.Graphics.DrawString((Convert.ToDecimal(ds.Tables[0].Rows[0]["ActCCAmount"]) + Convert.ToDecimal(ds.Tables[0].Rows[0]["RefillCCAmount"])).ToString("######0.00"), drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
            y += (1 * drawFontArial10Regular.GetHeight(e.Graphics));
            e.Graphics.DrawLine(new Pen(Brushes.Black, 1), 1, y, 290, y);

            e.Graphics.DrawString("Meal Pass Balance: ", drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
            e.Graphics.DrawString((Convert.ToDecimal(ds.Tables[0].Rows[0]["ActMealAmount"]) + Convert.ToDecimal(ds.Tables[0].Rows[0]["RefillMealAmount"])).ToString("######0.00"), drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
            y += (1 * drawFontArial10Regular.GetHeight(e.Graphics));
            e.Graphics.DrawLine(new Pen(Brushes.Black, 1), 1, y, 290, y);

            //e.Graphics.DrawString(string.Concat(Enumerable.Repeat("-", 55)), drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
            y += (2 * drawFontArial10Regular.GetHeight(e.Graphics));
        }
    }
}
