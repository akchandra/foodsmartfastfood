﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FoodSmart.Utilities;

using FoodSmart.BLL;

namespace FoodSmartDesktop.POS
{
    public partial class ReprintBill : Form
    {
        private StringBuilder trackInfo = new StringBuilder();
        private StringBuilder IgnoreInfo = new StringBuilder();

        private bool track1Complete = false;
        bool verified = false;

        Form form1;
        bool GotCardDetails = false;

        public ReprintBill(POSInvoice form1)
        {
            InitializeComponent();
        }

        private void ReprintBill_Load(object sender, EventArgs e)
        {
            LoadBills();
            txtUserName.Text = "";
            ddlBillNo.Enabled = false;
            txtUserName.Focus();
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            POSInvoice Parent = (POSInvoice)this.Owner;
            Parent.InvoiceID = 0;
            this.Close();
        }

        private void btnCont_Click(object sender, EventArgs e)
        {
            if (verified)
            {
                POSInvoice parent = (POSInvoice)this.Owner;
                parent.InvoiceID = Convert.ToInt32(ddlBillNo.SelectedValue);
                this.Close();
            }
        }

        

        private void LoadBills()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();


            ds = new InvoiceBLL().GetAllBills(GeneralFunctions.LoginInfo.LocID);
            dt = ds.Tables[0];

            if (dt.Rows.Count > 0)
            {
                ddlBillNo.DataSource = dt;
                ddlBillNo.DisplayMember = "BillNo";
                ddlBillNo.ValueMember = "pk_BillID";
                ddlBillNo.SelectedIndex = 0;
            }
        }


    }
}
