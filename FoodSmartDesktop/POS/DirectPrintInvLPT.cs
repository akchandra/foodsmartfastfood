﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;
using Microsoft.Win32.SafeHandles;
using System.Windows.Forms;
using FoodSmart.BLL;
using FoodSmart.Utilities;

namespace FoodSmartDesktop.POS
{
    public partial class DirectPrintInvLPT : Form
    {
        [DllImport("kernel32.dll", SetLastError = true)]
        static extern SafeFileHandle CreateFile(string lpFileName, FileAccess dwDesiredAccess, uint dwShareMode, IntPtr lpSecurityAttributes, FileMode dwCreationDisposition, uint dwFlagsAndAttributes, IntPtr hTemplateFile);

        private int BillID;
        private string BillType;
        private decimal CardBalance;
        private string RestaurantName;

        public DirectPrintInvLPT(string Mode, int InvID, string prnName, string RestName, string cpy, string CalcMethod)
        {
            InitializeComponent();
            BillID = InvID;
            BillType = Mode;
            //CardBalance = AvailableBalance;
            RestaurantName = RestName;
            if (cpy == "K" && BillType == "B")
                PrintKOT(prnName);
            if (cpy == "B")
                PrintInvoice(prnName);
        }

        private void DirectPrintInvLPT_Load(object sender, EventArgs e)
        {

        }

        public void PrintKOT(string PrnName)
        {
            Byte[] buffer;
            //bool IsConnected = false;
            string sampleText;

            string SLine;
            SLine = string.Concat(Enumerable.Repeat("-", 48));
            int Linesize = 48;
            int sp;
            string nl = Convert.ToChar(13).ToString() + Convert.ToChar(10).ToString();

            //SafeFileHandle fh = CreateFile("LPT1:", FileAccess.Write, 0, IntPtr.Zero, FileMode.OpenOrCreate, 0, IntPtr.Zero);
            SafeFileHandle fh = CreateFile(PrnName, FileAccess.Write, 0, IntPtr.Zero, FileMode.OpenOrCreate, 0, IntPtr.Zero);
            if (!fh.IsInvalid)
            {
                //IsConnected = true;
                FileStream lpt1 = new FileStream(fh, FileAccess.ReadWrite);

                //buffer = new byte[sampleText.Length];
                //buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);

                SLine = string.Concat(Enumerable.Repeat("-", 48));
                InvoiceBLL invbll = new InvoiceBLL();
                DataSet ds = invbll.GetInvoiceForPrinting(BillID, BillType);
                DataTable dt = ds.Tables[0];
                DataTable dt1 = ds.Tables[1];

                if (BillType == "B")
                {
                    sampleText = " " + nl;
                    buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                    lpt1.Write(buffer, 0, buffer.Length);

                    sampleText = " " + nl;
                    buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                    lpt1.Write(buffer, 0, buffer.Length);

                    sp = (Linesize - ("KITCHEN ORDER TICKET").Length) / 2;
                    sampleText = new String(' ', sp) + "KITCHEN ORDER TICKET" + nl;
                    buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                    lpt1.Write(buffer, 0, buffer.Length);

                    sp = (Linesize - ("CARD NO : " + ds.Tables[0].Rows[0]["CardNoOutside"].ToString() + "(" + ds.Tables[0].Rows[0]["CurrentSerial"].ToString() + ")").Length) / 2;
                    sampleText = new String(' ', sp) + "CARD NO : " + ds.Tables[0].Rows[0]["CardNoOutside"].ToString() + "(" + ds.Tables[0].Rows[0]["CurrentSerial"].ToString() + ")" + nl;
                    buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                    lpt1.Write(buffer, 0, buffer.Length);

                    //sp = (Linesize - ("CASH MEMO NO :" + ds.Tables[0].Rows[0]["CardNoOutside"].ToString() + "(" + ds.Tables[0].Rows[0]["CurrentSerial"].ToString() + ")").Length) / 2;
                    sampleText = "CASH MEMO NO :" + ds.Tables[0].Rows[0]["BillNo"].ToString() + string.Concat(Enumerable.Repeat(" ", 5)) + " Date : " + Convert.ToDateTime(ds.Tables[0].Rows[0]["BillDate"]).ToString("dd-MM-yy hh:mm") + nl;
                    buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                    lpt1.Write(buffer, 0, buffer.Length);

                    sampleText = SLine + nl;
                    buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                    lpt1.Write(buffer, 0, buffer.Length);

                    sampleText = "ITEM                                   QTY" + nl;
                    buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                    lpt1.Write(buffer, 0, buffer.Length);

                    sampleText = SLine + nl;
                    buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                    lpt1.Write(buffer, 0, buffer.Length);

                    foreach (DataRow dr1 in dt1.Rows)
                    {
                        sampleText = dr1["ItemDescr"].ToString() + string.Concat(Enumerable.Repeat(" ", 38 - (dr1["ItemDescr"].ToString().Trim().Length))) + Convert.ToInt32(dr1["Qty"]).ToString("##0") + nl;
                        buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                        lpt1.Write(buffer, 0, buffer.Length);
                    }


                    sampleText = SLine + nl;
                    buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                    lpt1.Write(buffer, 0, buffer.Length);

                    sampleText = "User:" + GeneralFunctions.LoginInfo.UserFullName + nl;
                    buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                    lpt1.Write(buffer, 0, buffer.Length);

                    sampleText = SLine + nl;
                    buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                    lpt1.Write(buffer, 0, buffer.Length);

                    sampleText = " " + nl;
                    buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                    lpt1.Write(buffer, 0, buffer.Length);

                    sampleText = " " + nl;
                    buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                    lpt1.Write(buffer, 0, buffer.Length);


                    sampleText = "\r\n" + nl;
                    buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                    lpt1.Write(buffer, 0, buffer.Length);

                    sampleText = string.Format("{0}{1}", Convert.ToChar(27), Convert.ToChar(105));
                    //sampleText = string.Format("{0},{1},{2}", (char)(27), (char)(105), (char)(1));
                    buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                    lpt1.Write(buffer, 0, buffer.Length);
                }
                //sampleText = nl;
                //buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                //lpt1.Write(buffer, 0, buffer.Length);

                lpt1.Close();
            }
        }

        public void PrintInvoice(string PrnName)
        {
            Byte[] buffer;
            //bool IsConnected = false;
            string sampleText;

            string SLine;
            SLine = string.Concat(Enumerable.Repeat("-", 48));
            int Linesize = 48;
            int sp;
            string nl = Convert.ToChar(13).ToString() + Convert.ToChar(10).ToString(); 

            //SafeFileHandle fh = CreateFile("LPT1:", FileAccess.Write, 0, IntPtr.Zero, FileMode.OpenOrCreate, 0, IntPtr.Zero);
            SafeFileHandle fh = CreateFile(PrnName, FileAccess.Write, 0, IntPtr.Zero, FileMode.OpenOrCreate, 0, IntPtr.Zero);
            if (!fh.IsInvalid)
            {
                ////IsConnected = true;
                FileStream lpt1 = new FileStream(fh, FileAccess.ReadWrite);

                //buffer = new byte[sampleText.Length];
                //buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);

                //SLine = string.Concat(Enumerable.Repeat("-", 42));
                InvoiceBLL invbll = new InvoiceBLL();
                DataSet ds = invbll.GetInvoiceForPrinting(BillID, BillType);
                DataTable dt = ds.Tables[0];
                DataTable dt1 = ds.Tables[1];

                if (BillType == "B")
                {
                    sp = (Linesize - ("TAX INVOICE".Length)) / 2;
                    sampleText = new String(' ', sp) + "TAX INVOICE" + nl;
                    buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                    lpt1.Write(buffer, 0, buffer.Length);

                    sp = (Linesize - (ds.Tables[2].Rows[0]["Company_Name"].ToString()).Length) / 2;
                    sampleText = new String(' ', sp) + ds.Tables[2].Rows[0]["Company_Name"].ToString() + nl;
                    buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                    lpt1.Write(buffer, 0, buffer.Length);

                    sp = (Linesize - (ds.Tables[2].Rows[0]["Address1"].ToString()).Length) / 2;
                    sampleText = new String(' ', sp) + ds.Tables[2].Rows[0]["Address1"].ToString() + nl;
                    buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                    lpt1.Write(buffer, 0, buffer.Length);

                    sp = (Linesize - ("CASH MEMO No:" + ds.Tables[0].Rows[0]["BillNo"].ToString() + string.Concat(Enumerable.Repeat(" ", 5)) + " Date:" + Convert.ToDateTime(ds.Tables[0].Rows[0]["BillDate"]).ToString("dd-MM-yy hh:mm")).Length) / 2;
                    sampleText = "CASH MEMO No:" + ds.Tables[0].Rows[0]["BillNo"].ToString() + string.Concat(Enumerable.Repeat(" ", 5)) + " Date:" + Convert.ToDateTime(ds.Tables[0].Rows[0]["BillDate"]).ToString("dd-MM-yy hh:mm") + nl;
                    buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                    lpt1.Write(buffer, 0, buffer.Length);
                }
                else
                {
                    sp = (Linesize - ("REFUND INVOICE".Length)) / 2;
                    sampleText = new String(' ', sp) + "REFUND INVOICE" + nl;
                    buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                    lpt1.Write(buffer, 0, buffer.Length);

                    sp = (Linesize - (ds.Tables[2].Rows[0]["Company_Name"].ToString()).Length) / 2;
                    sampleText = new String(' ', sp) + ds.Tables[2].Rows[0]["Company_Name"].ToString() + nl;
                    buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                    lpt1.Write(buffer, 0, buffer.Length);

                    sp = (Linesize - (ds.Tables[2].Rows[0]["Address1"].ToString()).Length) / 2;
                    sampleText = new String(' ', sp) + ds.Tables[2].Rows[0]["Address1"].ToString() + nl;
                    buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                    lpt1.Write(buffer, 0, buffer.Length);

                    sp = (Linesize - ("REFUND MEMO NO.:" + ds.Tables[0].Rows[0]["BillNo"].ToString() + string.Concat(Enumerable.Repeat(" ", 5)) + " Date:" + Convert.ToDateTime(ds.Tables[0].Rows[0]["BillDate"]).ToString("dd-MM-yy hh:mm")).Length) / 2;
                    sampleText = new String(' ', sp) + "REFUND MEMO NO.:" + ds.Tables[0].Rows[0]["BillNo"].ToString() + string.Concat(Enumerable.Repeat(" ", 5)) + " Date:" + Convert.ToDateTime(ds.Tables[0].Rows[0]["BillDate"]).ToString("dd-MM-yy hh:mm") + nl;
                    buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                    lpt1.Write(buffer, 0, buffer.Length);
                }

                sp = (Linesize - (RestaurantName.Length)) / 2;
                sampleText = new String(' ', sp) + RestaurantName + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sp = (Linesize - ("VAT NO.:" + ds.Tables[0].Rows[0]["VATNo"].ToString()).Length) / 2;
                sampleText = new String(' ', sp) + "VAT NO.:" + ds.Tables[0].Rows[0]["VATNo"].ToString() + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sp = (Linesize - ("SERVICE TAX NO.:" + ds.Tables[0].Rows[0]["ServiceTaxNo"].ToString()).Length) / 2;
                sampleText = new String(' ', sp) + "SERVICE TAX NO.:" + ds.Tables[0].Rows[0]["ServiceTaxNo"].ToString() + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sp = (Linesize - ("CARD NO : " + ds.Tables[0].Rows[0]["CardNoOutside"].ToString() + "(" + ds.Tables[0].Rows[0]["CurrentSerial"].ToString() + ")").Length) / 2;
                sampleText = new String(' ', sp) + "CARD NO : " + ds.Tables[0].Rows[0]["CardNoOutside"].ToString() + "(" + ds.Tables[0].Rows[0]["CurrentSerial"].ToString() + ")" + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sampleText = SLine + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sampleText = "ITEM                     VAT% QTY AMT(Rs.)" + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sampleText = SLine + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sampleText = SLine + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                foreach (DataRow dr1 in dt1.Rows)
                {
                    //sampleText = dr1["ItemDescr"].ToString().Substring(0, 23) + string.Concat(Enumerable.Repeat(" ", 23 - (dr1["ItemDescr"].ToString().Trim().Length)));
                    //sampleText = sampleText + Convert.ToDecimal(dr1["VATPer"]).ToString("#0.00") + string.Concat(Enumerable.Repeat(" ", 5 - (dr1["VATPer"].ToString().Trim().Length)));
                    //sampleText = sampleText + Convert.ToDecimal(dr1["Qty"]).ToString("##0") + string.Concat(Enumerable.Repeat(" ", 3 - (Convert.ToDecimal(dr1["Qty"]).ToString("##0").Trim()).Length));
                    //sampleText = sampleText + Convert.ToDecimal(dr1["TotalAmount"]).ToString("####0.00") + string.Concat(Enumerable.Repeat(" ", 8 - (Convert.ToDecimal(dr1["TotalAmount"]).ToString("####0.00").Trim().Length))) + nl;
                    if (dr1["ItemDescr"].ToString().Length > 23)
                        sampleText = dr1["ItemDescr"].ToString().Substring(0, 23);
                    else
                    {
                        sp = 23 - dr1["ItemDescr"].ToString().Length ;
                        sampleText = dr1["ItemDescr"].ToString() + new String(' ', sp);
                    }
                    if (Convert.ToDecimal(dr1["VATPer"]).ToString("#0.00").Length < 5)
                        sp = 5 - Convert.ToDecimal(dr1["VATPer"]).ToString("#0.00").Length;
                    else
                        sp = 0;
                    sampleText = sampleText + new String(' ', sp + 1) + Convert.ToDecimal(dr1["VATPer"]).ToString("#0.00");

                    if (Convert.ToDecimal(dr1["Qty"]).ToString("##0").Length < 3)
                        sp = 3 - Convert.ToDecimal(dr1["Qty"]).ToString("##0").Length;
                    else
                        sp = 0;
                    
                    sampleText = sampleText + new String(' ', sp + 1) + Convert.ToDecimal(dr1["Qty"]).ToString("##0");

                    if (Convert.ToDecimal(dr1["TotalAmount"]).ToString("####0.00").Length < 8)
                        sp = 8 - Convert.ToDecimal(dr1["TotalAmount"]).ToString("####0.00").Length;
                    else
                        sp = 0;
                    sampleText = sampleText + new String(' ', sp + 1) + Convert.ToDecimal(dr1["TotalAmount"]).ToString("####0.00") + nl;

                    buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                    lpt1.Write(buffer, 0, buffer.Length);
                }
                sampleText = SLine + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                decimal basicamt = ds.Tables[0].Rows[0]["GTOTAMOUNT"].ToDecimal() - ds.Tables[0].Rows[0]["VAT"].ToDecimal() - ds.Tables[0].Rows[0]["STAX"].ToDecimal() - ds.Tables[0].Rows[0]["SBCTotal"].ToDecimal();

                sampleText = "Basic     : " + string.Concat(Enumerable.Repeat(" ", 22)) + Convert.ToInt32(ds.Tables[0].Rows[0]["TQty"]).ToString("##0") + "  " + basicamt.ToString("####0.00") + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sampleText = "Total VAT   : " + string.Concat(Enumerable.Repeat(" ", 23)) + Convert.ToDecimal(ds.Tables[0].Rows[0]["VAT"]).ToString("#####0.00") + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sampleText = "Total S.Tax : " + string.Concat(Enumerable.Repeat(" ", 23)) + Convert.ToDecimal(ds.Tables[0].Rows[0]["STAX"]).ToString("#####0.00") + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sampleText = "Total SBC   : " + string.Concat(Enumerable.Repeat(" ", 23)) + Convert.ToDecimal(ds.Tables[0].Rows[0]["SBCTotal"]).ToString("#####0.00") + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sampleText = "Round Off   : " + string.Concat(Enumerable.Repeat(" ", 23)) + Convert.ToDecimal(ds.Tables[0].Rows[0]["RoundOff"]).ToString("####0.00") + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sampleText = SLine + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                if (BillType == "B")
                {
                    sampleText = "Net Amount  : " + string.Concat(Enumerable.Repeat(" ", 22)) + Convert.ToDecimal(ds.Tables[0].Rows[0]["GTOTAMOUNT"]).ToString("####0.00") + nl;
                    buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                    lpt1.Write(buffer, 0, buffer.Length);

                }
                else
                {
                    sampleText = "Total Refund : " + string.Concat(Enumerable.Repeat(" ", 22)) + Convert.ToDecimal(ds.Tables[0].Rows[0]["GTOTAMOUNT"]).ToString("####0.00") + nl;
                    buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                    lpt1.Write(buffer, 0, buffer.Length);

                }

                sampleText = SLine + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sampleText = "Printed By:" + GeneralFunctions.LoginInfo.UserFullName + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sampleText = " " + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sampleText = "Card Balance:" + Convert.ToDecimal(ds.Tables[0].Rows[0]["CardBalance"]).ToString("####0.00") + " + ( " + Convert.ToDecimal(ds.Tables[0].Rows[0]["SecurityDeposit"]).ToString("####0.00") + ")" + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sampleText = SLine + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sampleText = " " + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sampleText = " " + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);


                sampleText = "\r\n" + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sampleText = string.Format("{0}{1}", Convert.ToChar(27), Convert.ToChar(105));
                //sampleText = string.Format("{0},{1},{2}", (char)(27), (char)(105), (char)(1));
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sampleText = string.Format("{0}{1}", Convert.ToChar(27), "@", Convert.ToChar(27) + "p" + Convert.ToChar(0) + ".}");
                //sampleText = string.Format("{0},{1},{2}", (char)(27), (char)(105), (char)(1));
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                lpt1.Close();

            }
        }
    }
}
