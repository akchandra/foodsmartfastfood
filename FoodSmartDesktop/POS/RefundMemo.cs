﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using FoodSmart.BLL;

namespace FoodSmartDesktop.POS
{

    public partial class RefundMemo : Form
    {
        private StringBuilder trackInfo = new StringBuilder();
        private StringBuilder IgnoreInfo = new StringBuilder();

        private bool track1Complete = false;
        bool verified = false;
        Form form1;
        bool GotCardDetails = false;

        public RefundMemo(POSInvoice form1)
        {
            InitializeComponent();
        }

        private void RefundMemo_Load(object sender, EventArgs e)
        {
            LoadReason();
            txtUserName.Text = "";
            ddlRefundReason.Enabled = false;
            txtUserName.Focus();
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            POSInvoice Parent = (POSInvoice)this.Owner;
            Parent.RefReasonID = 0;
            this.Close();
        }

        private void btnCont_Click(object sender, EventArgs e)
        {
            verified = CheckUserNamePw();
            if (verified)
            {
                POSInvoice parent = (POSInvoice)this.Owner;
                parent.RefReasonID = Convert.ToInt32(ddlRefundReason.SelectedValue);
                this.Close();
            }
        }

        private bool CheckUserNamePw()
        {

            return false;
        }
        private void LoadReason()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();


            ds = new InvoiceBLL().GetAllReason();
            dt = ds.Tables[0];

            if (dt.Rows.Count > 0)
            {
                ddlRefundReason.DataSource = dt;
                ddlRefundReason.DisplayMember = "Reason";
                ddlRefundReason.ValueMember = "pk_ReasonID";
                ddlRefundReason.SelectedIndex = -1;
            }
        }


       
    }
}
