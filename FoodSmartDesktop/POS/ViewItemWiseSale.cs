﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;
using System.IO;
using System.Drawing.Printing;
using System.Configuration;
using FoodSmart.BLL;
using FoodSmart.Utilities;

namespace FoodSmartDesktop.POS
{
    public partial class ViewItemWiseSale : Form
    {
        BindingSource source1 = new BindingSource();
        public ViewItemWiseSale(POSInvoice form1)
        {
            InitializeComponent();
        }

        private void ViewItemWiseSale_Load(object sender, EventArgs e)
        {
            dvgItemWiseSale.ColumnCount = 3;
            LoadItem();
        }


        private void lblExit_Click(object sender, EventArgs e)
        {
            POSInvoice Parent = (POSInvoice)this.Owner;
            Parent.RefReasonID = 0;
            this.Close();
        }

        private void LoadItem()
        {
            string path = Application.StartupPath + "\\";
            DataView view1;
            decimal TotAmt = 0.00.ToDecimal();
            int TotQty = 0;

            DataGridViewCellStyle style = new DataGridViewCellStyle();

            dvgItemWiseSale.EnableHeadersVisualStyles = false;
            style.ForeColor = Color.Blue;

            dvgItemWiseSale.AutoGenerateColumns = false;

            dvgItemWiseSale.Columns[0].Name = "Item Description";
            dvgItemWiseSale.Columns[1].Name = "Quantity";
            dvgItemWiseSale.Columns[2].Name = "Amount";

            dvgItemWiseSale.Columns[0].HeaderText = "Item Description";
            dvgItemWiseSale.Columns[1].HeaderText = "Quantity";
            dvgItemWiseSale.Columns[2].HeaderText = "Amount";

            dvgItemWiseSale.Columns[0].DataPropertyName = "ItemDescr";
            dvgItemWiseSale.Columns[1].DataPropertyName = "tQty";
            dvgItemWiseSale.Columns[2].DataPropertyName = "TTotal";

            dvgItemWiseSale.Columns[0].Width = 320;
            dvgItemWiseSale.Columns[1].Width = 60;
            dvgItemWiseSale.Columns[2].Width = 100;

            dvgItemWiseSale.Columns[0].SortMode = DataGridViewColumnSortMode.Automatic;
            dvgItemWiseSale.Columns[1].SortMode = DataGridViewColumnSortMode.NotSortable;
            dvgItemWiseSale.Columns[2].SortMode = DataGridViewColumnSortMode.NotSortable;

            dvgItemWiseSale.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dvgItemWiseSale.Columns[1].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;

            dvgItemWiseSale.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dvgItemWiseSale.Columns[2].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;

            InvoiceBLL invbll = new InvoiceBLL();
            DataSet ds = invbll.ItemWiseSalesRegister(GeneralFunctions.LoginInfo.LocID);
            if (ds.Tables[0].Rows.Count == 0)
                view1 = null;
            else
                view1 = new DataView(ds.Tables[0]);

            source1.DataSource = view1;
            dvgItemWiseSale.DataSource = source1;

            dvgItemWiseSale.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            dvgItemWiseSale.AllowUserToResizeColumns = false;
            dvgItemWiseSale.AllowUserToResizeRows = false;

            dvgItemWiseSale.AllowUserToAddRows = false;
            dvgItemWiseSale.ReadOnly = true;
            dvgItemWiseSale.Columns[0].HeaderCell.Style = style;

            DataTable dt = ds.Tables[0];
            foreach (DataRow dr1 in dt.Rows)
            {
               
                TotQty += dr1["Tqty"].ToInt();
                TotAmt += dr1["TTotal"].ToDecimal();
            }

            lblQty.Text = TotQty.ToString("###0");
            lblAmount.Text = (TotAmt + ds.Tables[2].Rows[0]["Roff"].ToDecimal()).ToString("########0.00");

            lblRoff.Text = ds.Tables[2].Rows[0]["Roff"].ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            POSInvoice Parent = (POSInvoice)this.Owner;
            Parent.RefReasonID = GeneralFunctions.LoginInfo.LocID;
            this.Close();
        }

    }
}
