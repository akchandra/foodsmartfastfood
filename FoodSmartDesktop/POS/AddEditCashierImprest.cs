﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FoodSmart.BLL;
using FoodSmart.Common;
using System.Configuration;
using System.IO;
using System.Drawing.Drawing2D;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using FoodSmart.Utilities.Cryptography;

namespace FoodSmartDesktop.POS
{
    public partial class AddEditCashierImprest : Form
    {
        string stat;
        int LID;

        public AddEditCashierImprest(int ID)
        {
            InitializeComponent();
            if (ID == 0)
            {
                stat = "A";
                LID = 0;
            }
            else
            {
                stat = "E";

                LID = ID;
            }
        }

        #region Public Method
        private void AddEditCashierImprest_Load(object sender, EventArgs e)
        {
            LoadTranType();
            LoadLoc();
            Initialize();
            if (LID != 0)
                FillImprest(LID);
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lblExit_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnSave_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void lblExit_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnSave_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void txtAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (ValidateEntry())
            {
                ICashierImprest imprest = new CashierImprestEntity();
                imprest.CashierID = ddlCashier.SelectedValue.ToInt();
                imprest.CashTranDate = dtTran.Value;
                imprest.CashTranNo = txtTranNo.Text;
                imprest.Mode = stat;
                imprest.Narration = txtNarration.Text;
                imprest.TranAmount = txtAmount.Text.ToDecimal();
                imprest.LocID = ddlLoc.SelectedValue.ToInt();
                imprest.TranType = ddlTranType.SelectedValue.ToString();
                imprest.TranID = LID;

                int Status = new ImprestTranBLL().SaveImprest(imprest, GeneralFunctions.LoginInfo.UserID);

                if (Status == 0)
                {
                    MessageBox.Show("Error!! Record Not Saved", "Save Record", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    if (stat == "A")
                    {
                        //UpdateCard();
                        Initialize();
                        dtTran.Focus();
                    }
                    else
                    {
                        this.Close();
                    }
                }
            }
        }
       
        private void ddlLoc_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadUsers();
        }

        #endregion

        #region "Private Methods"
        private void LoadTranType()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            Dictionary<string, string> cStatus = new Dictionary<string, string>();
            cStatus.Add("R", "Receive");
            cStatus.Add("P", "Paid");
            cStatus.Add("C", "Cash Purchase");

            ddlTranType.DataSource = new BindingSource(cStatus, null);
            ddlTranType.DisplayMember = "Value";
            ddlTranType.ValueMember = "Key";
            ddlTranType.SelectedIndex = -1;
        }

        private void LoadUsers()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            SearchCriteria searchcriteria = new SearchCriteria();
            searchcriteria.LocID = ddlLoc.SelectedValue.ToInt();
            searchcriteria.QueryStat = "L";

            ddlCashier.DataSource = null;

            ds = new UserBLL().GetUserByLoc(ddlLoc.SelectedValue.ToInt());
            dt = ds.Tables[0];

            DataRow nullrow = dt.NewRow();
            nullrow["PK_USERID"] = 0;
            nullrow["USERNAME"] = "Select";
            dt.Rows.Add(nullrow);
            if (dt.Rows.Count > 0)
            {
                ddlCashier.DataSource = dt;
                ddlCashier.DisplayMember = "UserName";
                ddlCashier.ValueMember = "pk_UserID";
                ddlCashier.SelectedValue = "0";
            }
        }

        private void LoadLoc()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            SearchCriteria searchcriteria = new SearchCriteria();
            searchcriteria.QueryStat = "L";

            ddlCashier.DataSource = null;

            ds = new LocationBLL().GetAllLoc(searchcriteria);
            dt = ds.Tables[0];

            DataRow nullrow = dt.NewRow();
            nullrow["pk_LocID"] = 0;
            nullrow["LocationName"] = "Select";
            dt.Rows.Add(nullrow);
            if (dt.Rows.Count > 0)
            {
                ddlLoc.DataSource = dt;
                ddlLoc.DisplayMember = "LocationName";
                ddlLoc.ValueMember = "pk_LocID";
                ddlLoc.SelectedValue = "0";
            }
        }

        private void FillImprest(int ImprestID)
        {
            ImprestTranBLL oImp = new ImprestTranBLL();
            

            ImprestTranBLL oRestbll = new ImprestTranBLL();

            CashierImprestEntity oImpEntity = (CashierImprestEntity)oRestbll.GetImprestData(ImprestID);

            txtAmount.Text = oImpEntity.TranAmount.ToString("########0.00");
            ddlLoc.SelectedValue = oImpEntity.LocID;
            txtNarration.Text = oImpEntity.Narration;
            ddlTranType.SelectedValue = oImpEntity.TranType;
            ddlCashier.SelectedValue = oImpEntity.CashierID;
            dtTran.Value = oImpEntity.CashTranDate;
            txtTranNo.Text = oImpEntity.CashTranNo;

        }

        private void Initialize()
        {
            //ddlRestType.SelectedIndex = 0;
            txtAmount.Text = string.Empty;
            ddlLoc.SelectedIndex = -1;
            txtTranNo.Text = string.Empty;

            ddlTranType.SelectedIndex = 0;
            ddlCashier.SelectedIndex = -1;
            dtTran.Value = DateTime.Today;
            txtNarration.Text = string.Empty;
        }

        private bool ValidateEntry()
        {
            bool RetVal = true;

            if (string.IsNullOrEmpty(txtAmount.Text))
            {
                txtAmount.BackColor = Color.Red;
                RetVal = false;
            }

            if (ddlCashier.SelectedIndex == -1)
            {
                ddlCashier.BackColor = Color.Red;
                RetVal = false;
            }

            return RetVal;
        }

      

        #endregion

     
    }
}
