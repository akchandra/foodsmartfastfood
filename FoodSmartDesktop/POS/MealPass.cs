﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FoodSmart.BLL.FSmartService;

namespace FoodSmartDesktop.POS
{
    public partial class MealPass : Form
    {
        public MealPass(posCashier Form1)
        {
            InitializeComponent();
        }

        private void MealPass_Load(object sender, EventArgs e)
        {
            LoadMeal();
            ddlMealPass.Focus();
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            posCashier Parent = (posCashier)this.Owner;
            Parent.MealPassID = 0;
            this.Close();
        }

        private void btnCont_Click(object sender, EventArgs e)
        {
            posCashier parent = (posCashier)this.Owner;
            parent.MealPassID = Convert.ToInt32(ddlMealPass.SelectedValue);
            this.Close();
        }
        
        private void LoadMeal()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            
            ds = new FoodSmart.BLL.CardTranBLL().GetAllMealPass();
            dt = ds.Tables[0];

            if (dt.Rows.Count > 0)
            {
                ddlMealPass.DataSource = dt;
                ddlMealPass.DisplayMember = "Description";
                ddlMealPass.ValueMember = "pk_MealPassID";
                ddlMealPass.SelectedIndex = -1;
            }
        }
    }
}
