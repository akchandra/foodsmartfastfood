﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Printing;
using System.Configuration;
using FoodSmart.BLL;
using FoodSmart.Utilities;

namespace FoodSmartDesktop.POS
{
    public partial class DirectPrintInvoice : Form
    {
        //private Font printFont;
        private int BillID;
        private string BillType;
        private decimal CardBalance;
        private string RestaurantName;
        private string CalMethod;

        public DirectPrintInvoice(string Mode, int InvID, string prnName, string RestName, string cpy, string Calcmethod)
        {
            InitializeComponent();
            try
            {
                //string prnName = ConfigurationManager.AppSettings["PrinterName"];
                PrinterSettings ps = new PrinterSettings();
                PrintDocument pd;
                PrintDocument pd1;

                //streamToPrint = new StreamWriter("D:\\MyFile.txt");
                try
                {
                    pd = new PrintDocument();
                    foreach (string printer in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
                    {
                        if (printer == prnName)
                        {
                            pd.PrinterSettings.PrinterName = printer;
                            break;
                        }
                    }

                    BillID = InvID;
                    BillType = Mode;
                    //CardBalance = AvailableBalance;
                    RestaurantName = RestName;
                    CalMethod = Calcmethod;
                    if (cpy == "K")
                    {
                        pd.PrintPage += new PrintPageEventHandler(this.pd_PrintKOT);
                        pd.Print();
                    }
                    if (cpy == "B")
                    {
                        pd1 = new PrintDocument();
                        pd1.PrinterSettings.PrinterName = pd.PrinterSettings.PrinterName;
                        pd1.PrintPage += new PrintPageEventHandler(this.pd_PrintInvoice);
                        pd1.Print();
                    }
                }
                finally
                {
                    //pd1.Dispose();
                    //streamToPrint.Close();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void DirectPrintInvoice_Load(object sender, EventArgs e)
        {
            
        }

        private void pd_PrintKOT(object sender, PrintPageEventArgs e)
        {
            //Single yPos = 0;

            //string TempStr = "";
            float x = 10;
            float y = 5;

            float width = 270.0F; // max width I found through trial and error
            float height = 0F;


            Font drawFontArial12Bold = new Font("Arial", 12, FontStyle.Bold);
            Font drawFontArial10Regular = new Font("Arial", 10, FontStyle.Regular);
            Font drawFontArial8Bold = new Font("Arial", 8, FontStyle.Bold);
            Font drawFontArial8Regular = new Font("Arial", 8, FontStyle.Regular);
            Font drawFontArial10Bold = new Font("Arial", 10, FontStyle.Bold);
            Font drawFontArial7Bold = new Font("Arial", 7, FontStyle.Bold);

            Single topMargin = e.MarginBounds.Top;
            string text = "";
            StringFormat sf = new StringFormat();
            // Set format of string.
            StringFormat drawFormatCenter = new StringFormat();
            drawFormatCenter.Alignment = StringAlignment.Center;
            StringFormat drawFormatLeft = new StringFormat();
            drawFormatLeft.Alignment = StringAlignment.Near;
            StringFormat drawFormatRight = new StringFormat();
            drawFormatRight.Alignment = StringAlignment.Far;

            InvoiceBLL invbll = new InvoiceBLL();
            DataSet ds = invbll.GetInvoiceForPrinting(BillID, BillType);
            DataTable dt = ds.Tables[0];
            DataTable dt1 = ds.Tables[1];

            if (BillType == "B")
            {
                e.Graphics.DrawString("KITCHEN ORDER TICKET", drawFontArial12Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatCenter);
                y += (1 * drawFontArial12Bold.GetHeight(e.Graphics));
                e.Graphics.DrawString(RestaurantName, drawFontArial10Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatCenter);
                y += (1 * drawFontArial10Bold.GetHeight(e.Graphics));
                //e.Graphics.DrawString("CARD NO : " + ds.Tables[0].Rows[0]["CardNoOutside"].ToString() + "(" + ds.Tables[0].Rows[0]["CurrentSerial"].ToString() + ")", drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatCenter);
                //y += (1 * drawFontArial10Bold.GetHeight(e.Graphics));
                e.Graphics.DrawString("CASH MEMO NO :" + ds.Tables[0].Rows[0]["BillNo"].ToString() + string.Concat(Enumerable.Repeat(" ", 5)) + " Date : " + Convert.ToDateTime(ds.Tables[0].Rows[0]["BillDate"]).ToString("dd-MM-yy hh:mm"), drawFontArial8Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
                y += (1 * drawFontArial8Bold.GetHeight(e.Graphics));
                e.Graphics.DrawLine(new Pen(Brushes.Black, 1), 1, y, 290, y);
                //e.Graphics.DrawString(string.Concat(Enumerable.Repeat("-", 55)), drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
                y += (1 * drawFontArial8Bold.GetHeight(e.Graphics));
                e.Graphics.DrawString("ITEM", drawFontArial8Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
                e.Graphics.DrawString("Quantity", drawFontArial8Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
                y += (1 * drawFontArial8Bold.GetHeight(e.Graphics));
                e.Graphics.DrawLine(new Pen(Brushes.Black, 1), 1, y, 290, y);

                //e.Graphics.DrawString(string.Concat(Enumerable.Repeat("-", 55)), drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
                y += (1 * drawFontArial8Bold.GetHeight(e.Graphics));

                foreach (DataRow dr1 in dt1.Rows)
                {
                    e.Graphics.DrawString(dr1["ItemDescr"].ToString(), drawFontArial8Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
                    e.Graphics.DrawString(dr1["Qty"].ToString(), drawFontArial8Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
                    y += (1 * drawFontArial8Regular.GetHeight(e.Graphics));
                }
                e.Graphics.DrawLine(new Pen(Brushes.Black, 1), 1, y, 290, y);
                //e.Graphics.DrawString(string.Concat(Enumerable.Repeat("-", 55)), drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
                y += (2 * drawFontArial8Bold.GetHeight(e.Graphics));
            }
        }

        private void pd_PrintInvoice(object sender, PrintPageEventArgs e)
        {

            float x = 10;
            float y = 5;

            float width = 270.0F; // max width I found through trial and error
            float height = 0F;

            //List<Point> p = new List<Point>();
            //for (int i = 0; i < 20; i++)
            //{
            //    p.Add(new Point((i % 2) * e.MarginBounds.Width, i * 20));
            //    p.Add(new Point(p[p.Count - 1].X, p[p.Count - 1].Y + 20));
            //}

            Font drawFontArial12Bold = new Font("Arial", 12, FontStyle.Bold);
            Font drawFontArial10Regular = new Font("Arial", 10, FontStyle.Regular);
            Font drawFontArial8Bold = new Font("Arial", 8, FontStyle.Bold);
            Font drawFontArial8Regular = new Font("Arial", 8, FontStyle.Regular);
            Font drawFontArial10Bold = new Font("Arial", 10, FontStyle.Bold);
            Font drawFontArial7Bold = new Font("Arial", 7, FontStyle.Bold);

            Single topMargin = e.MarginBounds.Top;
            string text = "";
            StringFormat sf = new StringFormat();
            // Set format of string.
            StringFormat drawFormatCenter = new StringFormat();
            drawFormatCenter.Alignment = StringAlignment.Center;
            StringFormat drawFormatLeft = new StringFormat();
            drawFormatLeft.Alignment = StringAlignment.Near;
            StringFormat drawFormatRight = new StringFormat();
            drawFormatRight.Alignment = StringAlignment.Far;

            InvoiceBLL invbll = new InvoiceBLL();
            DataSet ds = invbll.GetInvoiceForPrinting(BillID, BillType);
            DataTable dt = ds.Tables[0];
            DataTable dt1 = ds.Tables[1];

            e.Graphics.DrawString(RestaurantName, drawFontArial10Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatCenter);
            y += (1 * drawFontArial10Bold.GetHeight(e.Graphics));
            //e.Graphics.DrawString("Card No:" + ds.Tables[0].Rows[0]["CardNoOutside"].ToString() + "(" + ds.Tables[0].Rows[0]["CurrentSerial"].ToString() + ")", drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatCenter);
            //y += (1 * drawFontArial10Bold.GetHeight(e.Graphics));

            if (BillType == "B")
            {
                e.Graphics.DrawString("TAX INVOICE", drawFontArial10Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatCenter);
                y += (1 * drawFontArial10Bold.GetHeight(e.Graphics));

                e.Graphics.DrawString(ds.Tables[2].Rows[0]["Company_Name"].ToString(), drawFontArial12Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatCenter);
                y += (1 * drawFontArial12Bold.GetHeight(e.Graphics));

                e.Graphics.DrawString(ds.Tables[0].Rows[0]["locationAddress1"].ToString(), drawFontArial8Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatCenter);
                y += (1 * drawFontArial8Bold.GetHeight(e.Graphics));

                e.Graphics.DrawString("CASH MEMO No :" + ds.Tables[0].Rows[0]["BillNo"].ToString() + string.Concat(Enumerable.Repeat(" ", 5)) + " Date : " + Convert.ToDateTime(ds.Tables[0].Rows[0]["BillDate"]).ToString("dd-MM-yy hh:mm"), drawFontArial8Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
                y += (1 * drawFontArial8Bold.GetHeight(e.Graphics));

            }
            else
            {
                e.Graphics.DrawString("REFUND INVOICE", drawFontArial12Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatCenter);
                y += (1 * drawFontArial12Bold.GetHeight(e.Graphics));

                e.Graphics.DrawString(ds.Tables[2].Rows[0]["Company_Name"].ToString(), drawFontArial8Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatCenter);
                y += (1 * drawFontArial8Bold.GetHeight(e.Graphics));

                e.Graphics.DrawString(ds.Tables[0].Rows[0]["locationAddress1"].ToString(), drawFontArial8Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatCenter);
                y += (1 * drawFontArial8Bold.GetHeight(e.Graphics));

                e.Graphics.DrawString("REFUND MEMO NO.:" + ds.Tables[0].Rows[0]["BillNo"].ToString(), drawFontArial10Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatCenter);
                y += (1 * drawFontArial12Bold.GetHeight(e.Graphics));
            }

            e.Graphics.DrawString("GST NO.:" + ds.Tables[0].Rows[0]["GSTNo"].ToString(), drawFontArial8Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatCenter);
            y += (1 * drawFontArial8Bold.GetHeight(e.Graphics));

            e.Graphics.DrawString("SERVICE : Restaurant Service (SAC - 9963)", drawFontArial8Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatCenter);
            y += (1 * drawFontArial8Bold.GetHeight(e.Graphics));

            //e.Graphics.DrawString("SERVICE TAX NO.:" + ds.Tables[0].Rows[0]["ServiceTaxNo"].ToString(), drawFontArial8Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatCenter);
            //y += (1 * drawFontArial8Bold.GetHeight(e.Graphics));

            e.Graphics.DrawLine(new Pen(Brushes.Black, 1), 1, y, 290, y);
            //e.Graphics.DrawString(string.Concat(Enumerable.Repeat("-", 55)), drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
            y += (1 * drawFontArial8Bold.GetHeight(e.Graphics));

            e.Graphics.DrawString("ITEM", drawFontArial8Regular, Brushes.Black, new RectangleF(1, y, width, height), drawFormatLeft);
            e.Graphics.DrawString("QTY", drawFontArial8Regular, Brushes.Black, new RectangleF(130, y, width, height), drawFormatLeft);
            e.Graphics.DrawString("BASIC", drawFontArial8Regular, Brushes.Black, new RectangleF(160, y, width, height), drawFormatLeft);
            e.Graphics.DrawString("GST", drawFontArial8Regular, Brushes.Black, new RectangleF(205, y, width, height), drawFormatLeft);
            e.Graphics.DrawString("TOTAL", drawFontArial8Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);

            //e.Graphics.DrawString("ITEM", drawFontArial8Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
            //e.Graphics.DrawString("GST%", drawFontArial8Regular, Brushes.Black, new RectangleF(150, y, width, height), drawFormatLeft);
            //e.Graphics.DrawString("QTY", drawFontArial8Regular, Brushes.Black, new RectangleF(200, y, width, height), drawFormatLeft);
            //e.Graphics.DrawString("AMT(Rs.)", drawFontArial8Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);

            y += (1 * drawFontArial12Bold.GetHeight(e.Graphics));
            e.Graphics.DrawLine(new Pen(Brushes.Black, 1), 1, y, 290, y);
            //e.Graphics.DrawString(string.Concat(Enumerable.Repeat("-", 55)), drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
            //e.Graphics.DrawLines(Pens.Black, p.ToArray());
            y += (1 * drawFontArial10Regular.GetHeight(e.Graphics));

            foreach (DataRow dr1 in dt1.Rows)
            {
                e.Graphics.DrawString(dr1["itDescr1"].ToString(), drawFontArial8Regular, Brushes.Black, new RectangleF(1, y, width, height), drawFormatLeft);
                e.Graphics.DrawString(dr1["Qty"].ToString(), drawFontArial8Regular, Brushes.Black, new RectangleF(130, y, width, height), drawFormatLeft);
                e.Graphics.DrawString((dr1["TotalAmount"].ToDecimal() - dr1["CGSTAmt"].ToDecimal() - dr1["SGSTAmt"].ToDecimal()).ToString(), drawFontArial8Regular, Brushes.Black, new RectangleF(-75, y, width, height), drawFormatRight);
                e.Graphics.DrawString((dr1["CGSTAmt"].ToDecimal() + dr1["SGSTAmt"].ToDecimal()).ToString(), drawFontArial8Regular, Brushes.Black, new RectangleF(-40, y, width, height), drawFormatRight);
                e.Graphics.DrawString(dr1["TotalAmount"].ToString(), drawFontArial8Regular, Brushes.Black, new RectangleF(1, y, width, height), drawFormatRight);

                y += (1 * drawFontArial8Regular.GetHeight(e.Graphics));
                e.Graphics.DrawString("SGST%:" + dr1["SGSTPer"].ToString(), drawFontArial8Regular, Brushes.Black, new RectangleF(1, y, width, height), drawFormatLeft);
                e.Graphics.DrawString(dr1["SGSTAmt"].ToString(), drawFontArial8Regular, Brushes.Black, new RectangleF(-40, y, width, height), drawFormatRight);
                y += (1 * drawFontArial8Regular.GetHeight(e.Graphics));
                e.Graphics.DrawString("CGST%:" + dr1["CGSTPer"].ToString() + "%", drawFontArial8Regular, Brushes.Black, new RectangleF(1, y, width, height), drawFormatLeft);
                e.Graphics.DrawString(dr1["CGSTAmt"].ToString(), drawFontArial8Regular, Brushes.Black, new RectangleF(-40, y, width, height), drawFormatRight);
                y += (1 * drawFontArial8Regular.GetHeight(e.Graphics));

                //e.Graphics.DrawString(dr1["ItemDescr"].ToString(), drawFontArial8Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
                //e.Graphics.DrawString(dr1["GSTPer"].ToString(), drawFontArial8Regular, Brushes.Black, new RectangleF(150, y, width, height), drawFormatLeft);
                //e.Graphics.DrawString(dr1["Qty"].ToString(), drawFontArial8Regular, Brushes.Black, new RectangleF(200, y, width, height), drawFormatLeft);
                //e.Graphics.DrawString(dr1["TotalAmount"].ToString(), drawFontArial8Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
                //y += (1 * drawFontArial8Regular.GetHeight(e.Graphics));
            }

            e.Graphics.DrawLine(new Pen(Brushes.Black, 1), 1, y, 290, y);
            y += (1 * drawFontArial10Regular.GetHeight(e.Graphics));

            if (CalMethod == "B")
            {
                decimal basicamt = ds.Tables[0].Rows[0]["BasicAmount"].ToDecimal() - ds.Tables[0].Rows[0]["CGSTAmount"].ToDecimal() - ds.Tables[0].Rows[0]["SGSTAmount"].ToDecimal();

                e.Graphics.DrawString("Basic : ", drawFontArial8Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
                e.Graphics.DrawString(ds.Tables[0].Rows[0]["TQty"].ToString(), drawFontArial8Bold, Brushes.Black, new RectangleF(130, y, width, height), drawFormatLeft);
                e.Graphics.DrawString(basicamt.ToString(), drawFontArial8Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
                y += (1 * drawFontArial8Bold.GetHeight(e.Graphics));

                e.Graphics.DrawString("Total CGST  : ", drawFontArial8Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
                e.Graphics.DrawString(ds.Tables[0].Rows[0]["CGSTAmount"].ToString(), drawFontArial8Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
                y += (1 * drawFontArial8Bold.GetHeight(e.Graphics));

                e.Graphics.DrawString("Total SGST  : ", drawFontArial8Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
                e.Graphics.DrawString(ds.Tables[0].Rows[0]["SGSTAmount"].ToString(), drawFontArial8Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
                y += (1 * drawFontArial8Bold.GetHeight(e.Graphics));

                e.Graphics.DrawString("Round Off   : ", drawFontArial8Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
                e.Graphics.DrawString(ds.Tables[0].Rows[0]["RoundOff"].ToString(), drawFontArial8Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
                y += (1 * drawFontArial8Bold.GetHeight(e.Graphics));

                decimal BilTot = ds.Tables[0].Rows[0]["GTOTAMOUNT"].ToDecimal() + ds.Tables[0].Rows[0]["RoundOff"].ToDecimal();

                if (BillType == "B")
                {
                    e.Graphics.DrawString("Net Amount  : ", drawFontArial8Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
                    e.Graphics.DrawString(BilTot.ToString(), drawFontArial10Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
                    y += (1 * drawFontArial10Bold.GetHeight(e.Graphics));
                }
                else
                {
                    e.Graphics.DrawString("Total Refund : ", drawFontArial8Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
                    e.Graphics.DrawString(BilTot.ToString(), drawFontArial10Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
                    y += (1 * drawFontArial10Bold.GetHeight(e.Graphics));
                }
            }
            else
            {
                decimal basicamt = ds.Tables[0].Rows[0]["BasicAmount"].ToDecimal();

                e.Graphics.DrawString("Basic : ", drawFontArial8Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
                e.Graphics.DrawString(ds.Tables[0].Rows[0]["TQty"].ToString(), drawFontArial8Bold, Brushes.Black, new RectangleF(130, y, width, height), drawFormatLeft);
                e.Graphics.DrawString(basicamt.ToString(), drawFontArial8Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
                y += (1 * drawFontArial8Bold.GetHeight(e.Graphics));

                e.Graphics.DrawString("Total CGST  : ", drawFontArial8Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
                e.Graphics.DrawString(ds.Tables[0].Rows[0]["CGSTAmount"].ToString(), drawFontArial8Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
                y += (1 * drawFontArial8Bold.GetHeight(e.Graphics));

                e.Graphics.DrawString("Total SGST  : ", drawFontArial8Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
                e.Graphics.DrawString(ds.Tables[0].Rows[0]["SGSTAmount"].ToString(), drawFontArial8Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
                y += (1 * drawFontArial8Bold.GetHeight(e.Graphics));

                e.Graphics.DrawString("Round Off   : ", drawFontArial8Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
                e.Graphics.DrawString(ds.Tables[0].Rows[0]["RoundOff"].ToString(), drawFontArial8Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
                y += (1 * drawFontArial8Bold.GetHeight(e.Graphics));

                decimal BilTot = ds.Tables[0].Rows[0]["GTOTAMOUNT"].ToDecimal();

                if (BillType == "B")
                {
                    e.Graphics.DrawString("Net Amount  : ", drawFontArial8Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
                    e.Graphics.DrawString(BilTot.ToString(), drawFontArial10Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
                    y += (1 * drawFontArial10Bold.GetHeight(e.Graphics));
                }
                else
                {
                    e.Graphics.DrawString("Total Refund : ", drawFontArial8Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
                    e.Graphics.DrawString(BilTot.ToString(), drawFontArial10Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
                    y += (1 * drawFontArial10Bold.GetHeight(e.Graphics));
                }
            }
            y += (1 * drawFontArial10Regular.GetHeight(e.Graphics));

            if (Convert.ToString(ds.Tables[0].Rows[0]["PaymentMode"]) == "C")
                e.Graphics.DrawString("Paid By : Cash", drawFontArial12Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
            //e.Graphics.DrawString("Card Balance : Rs." + CardBalance.ToString("#####0.00"), drawFontArial12Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
            else if (Convert.ToString(ds.Tables[0].Rows[0]["PaymentMode"]) == "R" || (Convert.ToString(ds.Tables[0].Rows[0]["PaymentMode"]) == "W"))
                e.Graphics.DrawString("Paid By : " + ds.Tables[0].Rows[0]["eWalletGateway"] + " No: ****" + ds.Tables[0].Rows[0]["CCNo"], drawFontArial8Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);

            //y += (1 * drawFontArial10Bold.GetHeight(e.Graphics));

            y += (1 * drawFontArial10Regular.GetHeight(e.Graphics));
            e.Graphics.DrawString("Printed By:" + GeneralFunctions.LoginInfo.UserFullName, drawFontArial8Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatCenter);
            y += (1 * drawFontArial10Regular.GetHeight(e.Graphics));
            e.Graphics.DrawString(ds.Tables[0].Rows[0]["btmLine1"].ToString(), drawFontArial8Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
            y += (1 * drawFontArial10Regular.GetHeight(e.Graphics));
            e.Graphics.DrawString(ds.Tables[0].Rows[0]["btmLine2"].ToString(), drawFontArial8Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
            y += (1 * drawFontArial10Regular.GetHeight(e.Graphics));

            e.Graphics.DrawString(ds.Tables[0].Rows[0]["btmLine3"].ToString(), drawFontArial8Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
            y += (1 * drawFontArial10Regular.GetHeight(e.Graphics));

            e.Graphics.DrawString(ds.Tables[0].Rows[0]["btmLine4"].ToString(), drawFontArial8Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
            //y += (1 * drawFontArial10Regular.GetHeight(e.Graphics));


            //decimal basicamt = ds.Tables[0].Rows[0]["GTOTAMOUNT"].ToDecimal() - ds.Tables[0].Rows[0]["VAT"].ToDecimal() - ds.Tables[0].Rows[0]["STAX"].ToDecimal() - ds.Tables[0].Rows[0]["SBCTotal"].ToDecimal();

            //e.Graphics.DrawString("Basic : ", drawFontArial8Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
            //e.Graphics.DrawString(ds.Tables[0].Rows[0]["TQty"].ToString(), drawFontArial8Bold, Brushes.Black, new RectangleF(200, y, width, height), drawFormatLeft);
            //e.Graphics.DrawString(basicamt.ToString(), drawFontArial8Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
            //y += (1 * drawFontArial8Bold.GetHeight(e.Graphics));

            //e.Graphics.DrawString("Total VAT   : ", drawFontArial8Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
            //e.Graphics.DrawString(ds.Tables[0].Rows[0]["VAT"].ToString(), drawFontArial8Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
            //y += (1 * drawFontArial8Bold.GetHeight(e.Graphics));

            //e.Graphics.DrawString("Total S.Tax : ", drawFontArial8Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
            //e.Graphics.DrawString(ds.Tables[0].Rows[0]["STAX"].ToString(), drawFontArial8Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
            //y += (1 * drawFontArial8Bold.GetHeight(e.Graphics));

            //e.Graphics.DrawString("Total SBC   : ", drawFontArial8Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
            //e.Graphics.DrawString(ds.Tables[0].Rows[0]["SBCTotal"].ToString(), drawFontArial8Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
            //y += (1 * drawFontArial8Bold.GetHeight(e.Graphics));

            //e.Graphics.DrawString("Rounding Off: ", drawFontArial8Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
            //e.Graphics.DrawString(ds.Tables[0].Rows[0]["RoundOff"].ToString(), drawFontArial8Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
            //y += (1 * drawFontArial8Bold.GetHeight(e.Graphics));

            //if (BillType == "B")
            //{
            //    e.Graphics.DrawString("Net Amount  : ", drawFontArial8Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
            //    e.Graphics.DrawString(ds.Tables[0].Rows[0]["GTOTAMOUNT"].ToString(), drawFontArial10Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
            //    y += (1 * drawFontArial10Bold.GetHeight(e.Graphics));
            //}
            //else
            //{
            //    e.Graphics.DrawString("Total Refund : ", drawFontArial8Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
            //    e.Graphics.DrawString(ds.Tables[0].Rows[0]["GTOTAMOUNT"].ToString(), drawFontArial10Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
            //    y += (1 * drawFontArial10Bold.GetHeight(e.Graphics));
            //}
            //y += (1 * drawFontArial10Regular.GetHeight(e.Graphics));
            //e.Graphics.DrawString("Printed By:" + GeneralFunctions.LoginInfo.UserFullName, drawFontArial8Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatCenter);
            //y += (1 * drawFontArial10Regular.GetHeight(e.Graphics));

            y += (1 * drawFontArial8Regular.GetHeight(e.Graphics));

        }
    }
}
