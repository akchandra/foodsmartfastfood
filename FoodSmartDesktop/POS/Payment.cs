﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FoodSmart.BLL;
using System.Configuration;
using System.Text.RegularExpressions;
using FoodSmart.Utilities.FlexiMessage.Forms;
using FoodSmart.Entity;
using FoodSmart.Utilities;

namespace FoodSmartDesktop.POS
{
    public partial class Payment : Form
    {
        private StringBuilder trackInfo = new StringBuilder();
        private StringBuilder IgnoreInfo = new StringBuilder();
        private bool inputToLabel = true;
        private decimal BlAmount;
        private bool InternetExist;
        //private StringBuilder cardData = new StringBuilder();
        string vData = "";
        string vData1 = "";
        string vData2 = "";
        string PaymentMode = "C";
        TextBox activeTextBox;
        int CouponID = 0;
        public string AmountPaid
        {
            get { return vData; }
            set
            {
                vData = value;

                if (string.IsNullOrEmpty(value))
                {
                    txtAmountPaid.Text = "0";
                }
                else
                {
                    txtAmountPaid.Text = value;
                }
            }
        }

        public string CCardNo
        {
            get { return vData1; }
            set
            {
                vData1 = value;

                if (string.IsNullOrEmpty(value))
                {
                    txtAcceptCardNo.Text = "";
                }
                else
                {
                    txtAcceptCardNo.Text = value;
                }
            }
        }

       
        string CCNo;


        public Payment(POSInvoice Form1)
        {
            InitializeComponent();
            BlAmount = Form1.PaymentAmount;

        }

        private void Payment_Load(object sender, EventArgs e)
        {
            txtAmountPaid.Text = string.Empty;
            txtAcceptCardNo.Text = string.Empty;
            lblPaidCash.Text = string.Empty;
            lblPaidCC.Text = string.Empty;
            lblPaidMealPass.Text = string.Empty;

            lblError.Visible = false;

            lblPayable.Text = "PAYABLE AMOUNT : " + Convert.ToString(BlAmount);
            txtAcceptCardNo.MaxLength = 4;
            lblBillAmtDisplay.Text = "BILL AMOUNT : " + Convert.ToString(BlAmount);
            txtCouponAmt.Text = string.Empty;
            btnCash_Click(null, null);
            TextBox_Enter(null, null);
            txtPayable.Text = BlAmount.ToString("########0.00");
            if (!GeneralFunctions.LoginInfo.InternetExists)
            {
                txtCouponNo.Enabled = false;
                txtCouponPrefix.Enabled = false;
            }
        }

        private void txtAcceptCardNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!inputToLabel)
                return;

            //if (e.KeyChar == '\r')
            //{
            //    string[] ccString = cardData.ToString().Split('^');
            //    //MessageBox.Show(cardData.ToString()); // Call your method here.
            //    if (ccString[0].ToString().Length < 18)
            //    {
            //        DialogResult est = MessageBox.Show("Invalid Credit Card. Please Retry", "CC Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            //        //lblInternalNoMessage.Visible = true;
            //        txtAmountPaid.Text = "";
            //        cardData.Clear();
            //        txtAmountPaid.Focus();
            //        return;
            //    }
            //    CCNo = ccString[0].ToString().Substring(2, 16);

            //    posCashier Parent = (posCashier)this.Owner;
            //    txtAmountPaid.Text = "";
            //    Parent.CreditCardNo = CCNo;
            //    cardData.Clear();
            //    this.Close();

            //}
            //else
            //{
            //    cardData.Append(e.KeyChar);
            //    //label13.Text = label13.Text + e.KeyChar;
            //}
            e.Handled = true;
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            //posCashier Parent = (posCashier)this.Owner;
            POSInvoice Parent = (POSInvoice)this.Owner;
            Parent.CreditCardNo = "";
            Parent.x = -1;
            //cardData.Clear();
            this.Close();
        }

        private void TextBox_Enter(System.Object sender, System.EventArgs e)
        {
            activeTextBox = (TextBox)sender;
        }

        private void btnCash_Click(object sender, EventArgs e)
        {

            btnCash.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "hotitems-btn-active.png");
            btnCard.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "cashier-rt-btn-bg.png");
            btneWallet.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "cashier-rt-btn-bg.png");
            btnMealPass.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "cashier-rt-btn-bg.png");

            tplCash.Visible = true;
            tlpCC.Visible = false;
            tlpEwallet.Visible = false;
            tlpMealPass.Visible = false;

            txtAmountPaid.Enabled = true;
            
            tplCash.Enabled = true;
            tableLayoutPanel3.Enabled = true;
            gbNumbers.Enabled = true;
            gbRupees.Enabled = true;
            EnableDisableNumber("E");
            DisplayPaymentBreakup();
            
            //txtAcceptCardNo.Enabled = false;
            PaymentMode = "C";
            txtAmountPaid.Focus();
            
        }

        private void RupeeMouseUp(System.Object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (activeTextBox != null)
            {
                if (activeTextBox.Name == "txtAmountPaid")
                {
                    AmountPaid = (sender as Button).Tag.ToString();
                }
            }
            //else if (activeTextBox.Name == "txtRefillAmount")
            //{
            //    // RefillData = sender.Tag;
            //    RefillData = (sender as Button).Tag.ToString();
            //}
        }

        private void NumericMouseUp(System.Object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (activeTextBox != null)
            {
                if (activeTextBox.Name == "txtAmountPaid")
                {
                    AmountPaid += (sender as Button).Tag.ToString();
                }
                else if (activeTextBox.Name == "txtAcceptCardNo")
                {
                    if (CCardNo.Length < 16)
                        CCardNo += (sender as Button).Tag.ToString();
                }
            }
           
        }

        private void btnback_MouseUp1(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (activeTextBox != null)
            {
                if (activeTextBox.Name == "txtAmountPaid")
                {
                    if (AmountPaid.Length == 0)
                    {
                        AmountPaid = "";
                    }
                    else
                    {
                        AmountPaid = AmountPaid.Substring(0, AmountPaid.Length - 1);
                    }
                }
                else if (activeTextBox.Name == "txtAcceptCardNo")
                {
                    if (CCardNo.Length == 0)
                    {
                        CCardNo = "";
                    }
                    else
                    {
                        CCardNo = CCardNo.Substring(0, CCardNo.Length - 1);
                    }
                }
            }
        }

        private void btnCard_Click(object sender, EventArgs e)
        {
            btnCash.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "cashier-rt-btn-bg.png");
            btnCard.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "hotitems-btn-active.png");
            btneWallet.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "cashier-rt-btn-bg.png");
            btnMealPass.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "cashier-rt-btn-bg.png");


            tplCash.Visible = false;
            tlpCC.Visible = true;
            tlpEwallet.Visible = false;
            tlpMealPass.Visible = false;

            tableLayoutPanel3.Enabled = true;
            gbNumbers.Enabled = false;
            gbRupees.Enabled = true;
            txtAmountPaid.Enabled = false;
            txtAcceptCardNo.Enabled = true;
            PaymentMode = "R";
            Loadddl(PaymentMode);
            EnableDisableNumber("D");
            txtAmountPaid.Text = "0.00";   // BlAmount.ToString("########0.00");
            txtAcceptCardNo.Focus();
            DisplayPaymentBreakup();
        }

        private void LoadMealAddlPayment()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            Dictionary<string, string> cStatus = new Dictionary<string, string>();

            cStatus.Clear();
            //cStatus.Add("I", "Invoice");
            //cStatus.Add("R", "Refund");
            //ddlInvType.DataSource = new BindingSource(cStatus, null);
            //ddlInvType.DisplayMember = "Value";
            //ddlInvType.ValueMember = "Key";
            //ddlInvType.SelectedIndex = 0;

            cStatus.Clear();
            cStatus.Add("C", "Cash");
            cStatus.Add("R", "Credit Card");
            ddlMPBalance.DataSource = new BindingSource(cStatus, null);
            ddlMPBalance.DisplayMember = "Value";
            ddlMPBalance.ValueMember = "Key";
            ddlMPBalance.SelectedIndex = 1;

            //FoodSmart.BLL.DBInteraction dbinteract = new FoodSmart.BLL.DBInteraction();
            //ds = dbinteract.GetStates();
            //dt = ds.Tables[0];
            //ddlState.DataSource = dt;
            //ddlState.DisplayMember = "Name";
            //ddlState.ValueMember = "Id";
            //ddlState.SelectedIndex = -1;

            //RetailSmart.BLL.DBInteraction dbinteract1 = new RetailSmart.BLL.DBInteraction();
            //ds = dbinteract.GetGSTRates();
            //dt = ds.Tables[0];
            //ddlGSTRate.DataSource = dt;
            //ddlGSTRate.DisplayMember = "Name";
            //ddlGSTRate.ValueMember = "Id";
            //ddlGSTRate.SelectedIndex = -1;
        }

        private void Loadddl(string PmtMode)
        {
            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.GatewayType = PmtMode;

            ddlCCType.DataSource = null;

            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            ds = new FoodSmart.BLL.eWalletBLL().GetAlleWallet(searchCriteria);
            dt = ds.Tables[0];

            if (dt.Rows.Count > 0)
            {
                if (PmtMode == "R")
                {
                    ddlCCType.DataSource = dt;
                    ddlCCType.DisplayMember = "eWalletGateway";
                    ddlCCType.ValueMember = "pk_eWalletID";
                    ddlCCType.SelectedIndex = 0;
                }
                else if (PmtMode == "M")
                {
                    ddlMealPass.DataSource = dt;
                    ddlMealPass.DisplayMember = "eWalletGateway";
                    ddlMealPass.ValueMember = "pk_eWalletID";
                    ddlMealPass.SelectedIndex = 0;
                }
                else if (PmtMode == "E")
                {
                    ddlleWallet.DataSource = dt;
                    ddlleWallet.DisplayMember = "eWalletGateway";
                    ddlleWallet.ValueMember = "pk_eWalletID";
                    ddlleWallet.SelectedIndex = 0;
                }
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            this.txtAmountPaid.TextChanged -= new System.EventHandler(this.txtAmountPaid_TextChanged);
            FlexibleMessageBox.FONT = new Font("Arial Black", 12, FontStyle.Bold);
            DialogResult yn = FlexibleMessageBox.Show("Are You sure to Clear Payment", "Remove", MessageBoxButtons.YesNo, MessageBoxIcon.Hand);
            if (yn == DialogResult.Yes)
            {
                txtAmountPaid.Text = "0";
                txtAcceptCardNo.Text = "";
                btnCash.Enabled = true;
                btnCard.Enabled = true;
                btnMealPass.Enabled = true;
                btneWallet.Enabled = true;

                btnCash.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "cashier-rt-btn-bg.png");
                btnCard.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "cashier-rt-btn-bg.png");
                btneWallet.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "cashier-rt-btn-bg.png");
                btnMealPass.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "cashier-rt-btn-bg.png");

                //cardData.Clear();
                EnableDisableNumber("D");
                txtAcceptCardNo.Enabled = false;
                txtAcceptCardNo.Text = "";
                tplCash.Visible = true;
                tableLayoutPanel3.Enabled = false;
                gbNumbers.Enabled = false;
                gbRupees.Enabled = false;
                btnCash_Click(null, null);
                //txtAcceptCardNo.Focus();
                AmountPaid = "";
                CCardNo = "";
                txtCouponAmt.Text = "";

                txtCouponNo.Text = "";
                txtCouponAmt.Text = "";

                this.txtAmountPaid.TextChanged += new System.EventHandler(this.txtAmountPaid_TextChanged);
            }
        }

        private void txtAmountPaid_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtAmountPaid.Text))
            {
                if (!string.IsNullOrEmpty(txtAmountPaid.Text) && !string.IsNullOrEmpty(txtPayable.Text))
                {
                //btnCard.Enabled = false;
                //btnCard.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "hotitems-btn-inactive.png");
                    if ((Convert.ToDecimal(txtAmountPaid.Text) - Convert.ToDecimal(txtPayable.Text)) >= 0)
                    {
                        txtRefund.Text = (Convert.ToDecimal(txtAmountPaid.Text) - Convert.ToDecimal(txtPayable.Text)).ToString("#########0.00");
                    }
                    else
                        txtRefund.Text = "";
                }
            }
            //else
            //{
            //    btnCard.Enabled = true;
            //    btnCard.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "item-btn.png");
            //}

        }

        private void btnPay_Click(object sender, EventArgs e)
        {
            FlexibleMessageBox.FONT = new Font("Arial Black", 12, FontStyle.Bold);
            if (PaymentMode == "C")
            {
                if (string.IsNullOrEmpty(txtAmountPaid.Text))
                {
                    FlexibleMessageBox.Show("Payment Amount Null. Can't Save Record", "Remove", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if ((Convert.ToDecimal(txtAmountPaid.Text) - BlAmount) < 0)
                {
                    FlexibleMessageBox.Show("Payment Amount < Bill Amount. Can't Save Record", "Remove", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }

            if (PaymentMode == "R" && string.IsNullOrEmpty(txtAcceptCardNo.Text))
            {
                FlexibleMessageBox.Show("Credit Card No is Compulsory. Can't Save Record", "Remove", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else
                CCNo = txtAcceptCardNo.Text;

            if (PaymentMode == "E" && string.IsNullOrEmpty(txtEWalletRef.Text))
            {
                FlexibleMessageBox.Show("eWallet reference is Compulsory. Can't Save Record", "Remove", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else
                CCNo = txtEWalletRef.Text;

            if (PaymentMode == "M")
            {
                if (string.IsNullOrEmpty(txtMealPassRef.Text))
                {
                    FlexibleMessageBox.Show("Meal Pass reference is Compulsory. Can't Save Record", "Remove", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (string.IsNullOrEmpty(txtMPAmt.Text))
                {
                    FlexibleMessageBox.Show("Meal Pass Amount is Compulsory. Can't Save Record", "Remove", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            //if (PaymentMode == "R" && txtAcceptCardNo.Text.Length == 16)
            

            POSInvoice Parent = (POSInvoice)this.Owner;
            if (PaymentMode == "R")
                Parent.CreditCardNo = CCNo;
            else if (PaymentMode == "M")
                Parent.CreditCardNo = txtMealPassRef.Text;
            else if (PaymentMode == "W")
                Parent.CreditCardNo = txtEWalletRef.Text;
               
            Parent.PaymentAmount = Convert.ToDecimal(txtAmountPaid.Text);
            Parent.PaymentCash = Convert.ToDecimal(lblPaidCash.Text);
            Parent.PaymentCC = Convert.ToDecimal(lblPaidCC.Text);
            Parent.PaymentWallet = Convert.ToDecimal(lblPaidWallet.Text);
            Parent.PaymentMP = Convert.ToDecimal(lblPaidMealPass.Text);
            Parent.PMode = PaymentMode;
            Parent.CouponNo = txtCouponNo.Text.ToInt();
            Parent.Prefix = txtCouponPrefix.Text;
            Parent.PaymentCoupon = txtCouponAmt.Text.ToDecimal();
            Parent.x = 1;
            this.Close();

        }

        private void EnableDisableNumber(string stat)
        {
            if (stat == "D")
            {
                btn0.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-inactive.png");
                btn1.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-inactive.png");
                btn2.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-inactive.png");
                btn3.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-inactive.png");
                btn4.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-inactive.png");
                btn5.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-inactive.png");
                btn6.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-inactive.png");
                btn7.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-inactive.png");
                btn8.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-inactive.png");
                btn9.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-inactive.png");
                btn00.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-inactive.png");
                btnback.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-inactive.png");
            }
            else
            {
                btn0.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-active.png");
                btn1.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-active.png");
                btn2.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-active.png");
                btn3.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-active.png");
                btn4.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-active.png");
                btn5.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-active.png");
                btn6.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-active.png");
                btn7.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-active.png");
                btn8.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-active.png");
                btn9.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-active.png");
                btn00.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-active.png");
                btnback.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "num-btn-active.png");
            }
        }

        private void btneWallet_Click(object sender, EventArgs e)
        {
            btnCash.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "cashier-rt-btn-bg.png");
            btnCard.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "cashier-rt-btn-bg.png");
            btneWallet.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "hotitems-btn-active.png");
            btnMealPass.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "cashier-rt-btn-bg.png");

            tplCash.Visible = false;
            tlpCC.Visible = false;
            tlpEwallet.Visible = true;
            tlpMealPass.Visible = false;

            tableLayoutPanel3.Enabled = true;
            gbNumbers.Enabled = false;
            gbRupees.Enabled = true;
            txtAmountPaid.Enabled = false;
            txtAcceptCardNo.Enabled = true;
            PaymentMode = "W";
            Loadddl(PaymentMode);
            EnableDisableNumber("D");
            txtAmountPaid.Text = "0.00"; // BlAmount.ToString("########0.00");
            //txtPayable.Text = BlAmount.ToString("########0.00");
            txtAcceptCardNo.Focus();
            DisplayPaymentBreakup();
        }

        private void btnMealPass_Click(object sender, EventArgs e)
        {
            btnCash.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "cashier-rt-btn-bg.png");
            btnCard.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "cashier-rt-btn-bg.png");
            btneWallet.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "cashier-rt-btn-bg.png");
            btnMealPass.BackgroundImage = Image.FromFile(ConfigurationManager.AppSettings["imageFolder"] + "hotitems-btn-active.png");

            tplCash.Visible = false;
            tlpCC.Visible = false;
            tlpEwallet.Visible = false;
            tlpMealPass.Visible = true;

            tableLayoutPanel3.Enabled = true;
            gbNumbers.Enabled = false;
            gbRupees.Enabled = true;
            txtAmountPaid.Enabled = false;
            txtAcceptCardNo.Enabled = true;
            txtAmountPaid.Text = "0.00";
            PaymentMode = "M";
            LoadMealAddlPayment();
            Loadddl(PaymentMode);
            DisplayPaymentBreakup();
            EnableDisableNumber("D");
        }

        private void txtCouponPrefix_Validating(object sender, CancelEventArgs e)
        {
            CouponBLL ocou = new CouponBLL();
            DataSet ds = ocou.CheckForCoupon(txtCouponPrefix.Text, 0, "P");
            if (ds != null)
            {
                if (ds.Tables[0].Rows[0]["PrefixRec"].ToInt() == 0)
                {
                    lblError.Text = "Invalid Prefix";
                    lblError.Visible = true;
                }
                else
                    lblError.Visible = false;
            }
            else
            {
                lblError.Text = "No Valid Coupon Exists";
                lblError.Visible = true;
            }
        }

        private void txtCouponNo_Validating(object sender, CancelEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtCouponNo.Text))
            {
                CouponBLL ocou = new CouponBLL();
                DataSet ds = ocou.CheckForCoupon(txtCouponPrefix.Text, txtCouponNo.Text.ToInt(), "C");

                if (ds.Tables[0].Rows[0]["CouponAmount"].ToInt() > 0)
                {
                    txtCouponAmt.Text = ds.Tables[0].Rows[0]["CouponAmount"].ToString();
                    CouponID = ds.Tables[0].Rows[0]["CouponID"].ToInt();
                    lblError.Visible = false;
                    if (BlAmount > txtCouponAmt.Text.ToDecimal())
                        txtPayable.Text = Convert.ToString(BlAmount - txtCouponAmt.Text.ToDecimal());
                    else
                        lblPayable.Text = "0.00";
                    //lblPayableAfterCoupon.Visible = true;
                }
                else
                {
                    lblError.Text = "Invalid Prefix & Coupon No Combination";
                    lblError.Visible = true;
                    //lblPayableAfterCoupon.Visible = false;
                }
                DisplayPaymentBreakup();
            }
        }

        private void DisplayPaymentBreakup()
        {
            if (PaymentMode == "C")
            {
                lblPaidCash.Text = Convert.ToString(BlAmount - txtCouponAmt.Text.ToDecimal());
                lblPaidCC.Text = "0.00";
                lblPaidMealPass.Text = "0.00";
                lblPaidWallet.Text = "0.00";
            }

            if (PaymentMode == "R")
            {
                if (string.IsNullOrEmpty(txtCCCash.Text))
                {
                    lblPaidCash.Text = "0.00";
                    lblPaidCC.Text = Convert.ToString(BlAmount - txtCouponAmt.Text.ToDecimal());
                    lblPaidMealPass.Text = "0.00";
                    lblPaidWallet.Text = "0.00";
                }
                else
                {
                    lblPaidCash.Text = txtCCCash.Text;
                    lblPaidCC.Text = Convert.ToString(BlAmount - txtCouponAmt.Text.ToDecimal() - txtCCCash.Text.ToDecimal());
                    lblPaidMealPass.Text = "0.00";
                    lblPaidWallet.Text = "0.00";
                }
            }

            if (PaymentMode == "W")
            {
                lblPaidWallet.Text = Convert.ToString(BlAmount - txtCouponAmt.Text.ToDecimal());
                lblPaidCC.Text = "0.00";
                lblPaidMealPass.Text = "0.00";
                lblPaidCash.Text = "0.00";

                //lblPaidWallet.Text = "0.00";

                //if (string.IsNullOrEmpty(txtCCCash.Text))
                //{
                //    lblPaidCash.Text = "0.00";
                //    lblPaidCC.Text = Convert.ToString(BlAmount - txtCouponAmt.Text.ToDecimal());
                //    lblPaidMealPass.Text = "0.00";
                //    lblPaidWallet.Text = "0.00";
                //}
                //else
                //{
                //    lblPaidCash.Text = txtCCCash.Text;
                //    lblPaidCC.Text = Convert.ToString(BlAmount - txtCouponAmt.Text.ToDecimal() - txtCCCash.Text.ToDecimal());
                //    lblPaidMealPass.Text = "0.00";
                //    lblPaidWallet.Text = "0.00";
                //}
            }

            if (PaymentMode == "M")
            {
                lblPaidMealPass.Text = txtMPAmt.Text;
                lblPaidCC.Text = "0.00";
                lblPaidCash.Text = "0.00";
                if (ddlMPBalance.SelectedValue.ToString() == "C")
                {
                    lblPaidCash.Text = txtMPBalAmt.Text;
                    lblPaidCC.Text = "0.00";
                    lblPaidWallet.Text = "0.00";
                }
                else
                {
                    lblPaidCC.Text = txtMPBalAmt.Text;
                    lblPaidCash.Text = "0.00";
                    lblPaidWallet.Text = "0.00";
                }
            }
        }

        private void txtMPAmt_Validating(object sender, CancelEventArgs e)
        {
            if (txtMPAmt.Text.ToDecimal() < txtPayable.Text.ToDecimal())
            {
                txtMPBalAmt.Text = (txtPayable.Text.ToDecimal() - txtMPAmt.Text.ToDecimal()).ToString("########0.00");
            }
            DisplayPaymentBreakup();
        }

        private void ddlMPBalance_SelectedIndexChanged(object sender, EventArgs e)
        {
            DisplayPaymentBreakup();
        }

        private void txtCouponNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void txtMealPassRef_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void txtMPCCNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void txtAcceptCardNo_KeyPress_1(object sender, KeyPressEventArgs e)
        {

        }
    }
}
