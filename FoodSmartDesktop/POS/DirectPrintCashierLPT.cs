﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;
using Microsoft.Win32.SafeHandles;
using System.Windows.Forms;
using FoodSmart.BLL;
using FoodSmart.Utilities;

namespace FoodSmartDesktop.POS
{
    public partial class DirectPrintCashierLPT : Form
    {
        [DllImport("kernel32.dll", SetLastError = true)]
        static extern SafeFileHandle CreateFile(string lpFileName, FileAccess dwDesiredAccess, uint dwShareMode, IntPtr lpSecurityAttributes, FileMode dwCreationDisposition, uint dwFlagsAndAttributes, IntPtr hTemplateFile);

        private int Cashier;

        public DirectPrintCashierLPT(int CashierID)
        {
            Cashier = CashierID;
            InitializeComponent();
            CashierReportLPT();
        }

        private void DirectPrintCashierLPT_Load(object sender, EventArgs e)
        {

        }

        public void CashierReportLPT()
        {
            Byte[] buffer;
            bool IsConnected = false;
            string sampleText;

            string SLine;
            SLine = string.Concat(Enumerable.Repeat("-", 48));
            int Linesize = 48;
            int sp;
            string nl = Convert.ToChar(13).ToString() + Convert.ToChar(10).ToString();
            
            InvoiceBLL invbll = new InvoiceBLL();
            DataSet ds = invbll.GetCashierSummaryForPrinting(Cashier);

            SafeFileHandle fh = CreateFile("LPT1:", FileAccess.Write, 0, IntPtr.Zero, FileMode.OpenOrCreate, 0, IntPtr.Zero);
            if (!fh.IsInvalid)
            {
                IsConnected = true;
                
                FileStream lpt1 = new FileStream(fh, FileAccess.ReadWrite);
                SLine = string.Concat(Enumerable.Repeat("-", 48));

                sp = (Linesize - (ds.Tables[1].Rows[0]["Company_Name"].ToString()).Length) / 2;
                sampleText = new String(' ', sp) + ds.Tables[1].Rows[0]["Company_Name"].ToString() + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sp = (Linesize - (ds.Tables[1].Rows[0]["Address1"].ToString()).Length) / 2;
                sampleText = new String(' ', sp) + ds.Tables[1].Rows[0]["Address1"].ToString() + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sp = (Linesize - ("As On:" + Convert.ToDateTime(DateTime.Now).ToString("dd-MMM-yyyy")).Length) / 2;
                sampleText = "As On:" + Convert.ToDateTime(DateTime.Now).ToString("dd-MMM-yyyy") + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sp = (Linesize - ("Cashier:" + GeneralFunctions.LoginInfo.UserFullName).Length) / 2;
                sampleText = "Cashier:" + GeneralFunctions.LoginInfo.UserFullName + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sampleText = SLine + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sampleText = "Activated (Nos.): " + Convert.ToDecimal(ds.Tables[0].Rows[0]["ActNos"]).ToString("#####0") + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sampleText = "Activated On Cash (Rs.)   : " + Convert.ToDecimal(ds.Tables[0].Rows[0]["ActCashAmount"]).ToString("######0.00") + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sampleText = "Activated On Card (Rs.)   : " + Convert.ToDecimal(ds.Tables[0].Rows[0]["ActCCAmount"]).ToString("######0.00") + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sampleText = "Activated On Pass (Rs.)   : " + Convert.ToDecimal(ds.Tables[0].Rows[0]["ActMealAmount"]).ToString("######0.00") + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sampleText = "Refilled  (Nos.): " + Convert.ToDecimal(ds.Tables[0].Rows[0]["RefillNos"]).ToString("#####0") + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sampleText = "Refilled on Cash (Rs.)  : " + Convert.ToDecimal(ds.Tables[0].Rows[0]["RefillCashAmount"]).ToString("######0.00") + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sampleText = "Refilled on Card (Rs.)  : " + Convert.ToDecimal(ds.Tables[0].Rows[0]["RefillCCAmount"]).ToString("######0.00") + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sampleText = "Refilled on Meal (Rs.)  : " + Convert.ToDecimal(ds.Tables[0].Rows[0]["RefillMealAmount"]).ToString("######0.00") + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sampleText = "Refund    (Nos.): " + Convert.ToDecimal(ds.Tables[0].Rows[0]["RefundNos"]).ToString("#####0") + " (Rs.)   : " + Convert.ToDecimal(ds.Tables[0].Rows[0]["RefndAmount"]).ToString("######0.00") + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                decimal CashBalAmt = Convert.ToDecimal(ds.Tables[0].Rows[0]["ActCashAmount"]) + Convert.ToDecimal(ds.Tables[0].Rows[0]["RefillCashAmount"]) - Convert.ToDecimal(ds.Tables[0].Rows[0]["RefndAmount"]);
                sampleText = "Cash Balance     (Rs.) : " + CashBalAmt.ToString("######0.00") + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                decimal CCBalAmt = Convert.ToDecimal(ds.Tables[0].Rows[0]["ActCCAmount"]) + Convert.ToDecimal(ds.Tables[0].Rows[0]["RefillCCAmount"]);
                sampleText = "Card Balance     (Rs.) : " + CCBalAmt.ToString("######0.00") + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                decimal MealBalAmt = Convert.ToDecimal(ds.Tables[0].Rows[0]["ActMealAmount"]) + Convert.ToDecimal(ds.Tables[0].Rows[0]["RefillMealAmount"]);
                sampleText = "Meal Pass Balance (Rs.) : " + MealBalAmt.ToString("######0.00") + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);
                
                //sampleText = SLine + nl;
                //buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                //lpt1.Write(buffer, 0, buffer.Length);

                //sampleText = " " + nl;
                //buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                //lpt1.Write(buffer, 0, buffer.Length);

                sampleText = " " + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sampleText = "\r\n" + nl;
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                sampleText = string.Format("{0}{1}", Convert.ToChar(27), Convert.ToChar(105));
                //sampleText = string.Format("{0},{1},{2}", (char)(27), (char)(105), (char)(1));
                buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                lpt1.Write(buffer, 0, buffer.Length);

                //sampleText = nl;
                //buffer = System.Text.Encoding.ASCII.GetBytes(sampleText);
                //lpt1.Write(buffer, 0, buffer.Length);

                lpt1.Close();
            }
        }
    }
}
