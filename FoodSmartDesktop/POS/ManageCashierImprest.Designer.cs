﻿namespace FoodSmartDesktop.POS
{
    partial class ManageCashierTran
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ManageCashierTran));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.lblExit = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ddlTranType = new System.Windows.Forms.ComboBox();
            this.dtTranDate = new System.Windows.Forms.DateTimePicker();
            this.button1 = new System.Windows.Forms.Button();
            this.ddlLoc = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblPageNo = new System.Windows.Forms.Label();
            this.picMoveFirst = new System.Windows.Forms.PictureBox();
            this.picMovePrev = new System.Windows.Forms.PictureBox();
            this.picMoveLast = new System.Windows.Forms.PictureBox();
            this.picMoveNext = new System.Windows.Forms.PictureBox();
            this.dvgRest = new System.Windows.Forms.DataGridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lbltoday = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnAddTran = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picMoveFirst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMovePrev)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMoveLast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMoveNext)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dvgRest)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(199)))), ((int)(((byte)(233)))));
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.lblUserName);
            this.panel1.Controls.Add(this.lblExit);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(6, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(960, 54);
            this.panel1.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(357, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(238, 23);
            this.label4.TabIndex = 23;
            this.label4.Text = "Manage Cashier Imprest";
            // 
            // lblUserName
            // 
            this.lblUserName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(199)))), ((int)(((byte)(233)))));
            this.lblUserName.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.lblUserName.Location = new System.Drawing.Point(677, 16);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(159, 22);
            this.lblUserName.TabIndex = 21;
            this.lblUserName.Text = "Welcome ";
            // 
            // lblExit
            // 
            this.lblExit.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExit.ForeColor = System.Drawing.Color.DarkRed;
            this.lblExit.Image = ((System.Drawing.Image)(resources.GetObject("lblExit.Image")));
            this.lblExit.Location = new System.Drawing.Point(889, 7);
            this.lblExit.Name = "lblExit";
            this.lblExit.Size = new System.Drawing.Size(63, 47);
            this.lblExit.TabIndex = 1;
            this.lblExit.Click += new System.EventHandler(this.lblExit_Click);
            this.lblExit.MouseLeave += new System.EventHandler(this.lblExit_MouseLeave);
            this.lblExit.MouseHover += new System.EventHandler(this.lblExit_MouseHover);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Monotype Corsiva", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
            this.label1.Location = new System.Drawing.Point(3, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(247, 38);
            this.label1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox1.Controls.Add(this.ddlTranType);
            this.groupBox1.Controls.Add(this.dtTranDate);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.ddlLoc);
            this.groupBox1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox1.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox1.Location = new System.Drawing.Point(5, 61);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(952, 50);
            this.groupBox1.TabIndex = 159;
            this.groupBox1.TabStop = false;
            // 
            // ddlTranType
            // 
            this.ddlTranType.FormattingEnabled = true;
            this.ddlTranType.Location = new System.Drawing.Point(419, 14);
            this.ddlTranType.Name = "ddlTranType";
            this.ddlTranType.Size = new System.Drawing.Size(217, 28);
            this.ddlTranType.TabIndex = 20;
            this.ddlTranType.SelectedIndexChanged += new System.EventHandler(this.ddlTranType_SelectedIndexChanged);
            // 
            // dtTranDate
            // 
            this.dtTranDate.CustomFormat = "dd-MMM-yyyy";
            this.dtTranDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtTranDate.Location = new System.Drawing.Point(7, 14);
            this.dtTranDate.Name = "dtTranDate";
            this.dtTranDate.ShowCheckBox = true;
            this.dtTranDate.Size = new System.Drawing.Size(143, 26);
            this.dtTranDate.TabIndex = 19;
            this.dtTranDate.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button1.BackgroundImage")));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.Control;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(896, 9);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(48, 40);
            this.button1.TabIndex = 18;
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = false;
            // 
            // ddlLoc
            // 
            this.ddlLoc.FormattingEnabled = true;
            this.ddlLoc.Location = new System.Drawing.Point(173, 14);
            this.ddlLoc.Name = "ddlLoc";
            this.ddlLoc.Size = new System.Drawing.Size(217, 28);
            this.ddlLoc.TabIndex = 1;
            this.ddlLoc.SelectedIndexChanged += new System.EventHandler(this.ddlLoc_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblPageNo);
            this.groupBox2.Controls.Add(this.picMoveFirst);
            this.groupBox2.Controls.Add(this.picMovePrev);
            this.groupBox2.Controls.Add(this.picMoveLast);
            this.groupBox2.Controls.Add(this.picMoveNext);
            this.groupBox2.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(5, 114);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(273, 32);
            this.groupBox2.TabIndex = 161;
            this.groupBox2.TabStop = false;
            // 
            // lblPageNo
            // 
            this.lblPageNo.AutoSize = true;
            this.lblPageNo.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPageNo.Location = new System.Drawing.Point(74, 11);
            this.lblPageNo.Name = "lblPageNo";
            this.lblPageNo.Size = new System.Drawing.Size(102, 20);
            this.lblPageNo.TabIndex = 17;
            this.lblPageNo.Text = "Current Page : ";
            // 
            // picMoveFirst
            // 
            this.picMoveFirst.Image = ((System.Drawing.Image)(resources.GetObject("picMoveFirst.Image")));
            this.picMoveFirst.Location = new System.Drawing.Point(16, 11);
            this.picMoveFirst.Name = "picMoveFirst";
            this.picMoveFirst.Size = new System.Drawing.Size(20, 17);
            this.picMoveFirst.TabIndex = 13;
            this.picMoveFirst.TabStop = false;
            // 
            // picMovePrev
            // 
            this.picMovePrev.Image = ((System.Drawing.Image)(resources.GetObject("picMovePrev.Image")));
            this.picMovePrev.Location = new System.Drawing.Point(42, 11);
            this.picMovePrev.Name = "picMovePrev";
            this.picMovePrev.Size = new System.Drawing.Size(20, 17);
            this.picMovePrev.TabIndex = 14;
            this.picMovePrev.TabStop = false;
            // 
            // picMoveLast
            // 
            this.picMoveLast.Image = ((System.Drawing.Image)(resources.GetObject("picMoveLast.Image")));
            this.picMoveLast.Location = new System.Drawing.Point(214, 11);
            this.picMoveLast.Name = "picMoveLast";
            this.picMoveLast.Size = new System.Drawing.Size(20, 17);
            this.picMoveLast.TabIndex = 16;
            this.picMoveLast.TabStop = false;
            // 
            // picMoveNext
            // 
            this.picMoveNext.Image = ((System.Drawing.Image)(resources.GetObject("picMoveNext.Image")));
            this.picMoveNext.Location = new System.Drawing.Point(188, 11);
            this.picMoveNext.Name = "picMoveNext";
            this.picMoveNext.Size = new System.Drawing.Size(20, 17);
            this.picMoveNext.TabIndex = 15;
            this.picMoveNext.TabStop = false;
            // 
            // dvgRest
            // 
            this.dvgRest.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dvgRest.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dvgRest.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dvgRest.DefaultCellStyle = dataGridViewCellStyle4;
            this.dvgRest.Location = new System.Drawing.Point(5, 166);
            this.dvgRest.Name = "dvgRest";
            this.dvgRest.RowHeadersVisible = false;
            this.dvgRest.Size = new System.Drawing.Size(952, 327);
            this.dvgRest.TabIndex = 163;
            this.dvgRest.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dvgRest_CellContentClick);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Black;
            this.panel2.Controls.Add(this.lbltoday);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Location = new System.Drawing.Point(12, 499);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(954, 33);
            this.panel2.TabIndex = 164;
            // 
            // lbltoday
            // 
            this.lbltoday.BackColor = System.Drawing.Color.Transparent;
            this.lbltoday.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltoday.ForeColor = System.Drawing.Color.White;
            this.lbltoday.Location = new System.Drawing.Point(12, 6);
            this.lbltoday.Name = "lbltoday";
            this.lbltoday.Size = new System.Drawing.Size(251, 22);
            this.lbltoday.TabIndex = 22;
            this.lbltoday.Text = "Today";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Snow;
            this.label3.Location = new System.Drawing.Point(823, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 16);
            this.label3.TabIndex = 1;
            this.label3.Text = "Powered By Europia";
            // 
            // btnAddTran
            // 
            this.btnAddTran.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAddTran.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAddTran.BackgroundImage")));
            this.btnAddTran.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAddTran.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddTran.ForeColor = System.Drawing.SystemColors.Control;
            this.btnAddTran.Image = ((System.Drawing.Image)(resources.GetObject("btnAddTran.Image")));
            this.btnAddTran.Location = new System.Drawing.Point(777, 114);
            this.btnAddTran.Name = "btnAddTran";
            this.btnAddTran.Size = new System.Drawing.Size(180, 46);
            this.btnAddTran.TabIndex = 162;
            this.btnAddTran.Text = "Add New Transaction";
            this.btnAddTran.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAddTran.UseVisualStyleBackColor = false;
            this.btnAddTran.Click += new System.EventHandler(this.btnAddTran_Click);
            this.btnAddTran.MouseLeave += new System.EventHandler(this.btnAddTran_MouseLeave);
            this.btnAddTran.MouseHover += new System.EventHandler(this.btnAddTran_MouseHover);
            // 
            // ManageCashierTran
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(967, 537);
            this.ControlBox = false;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.dvgRest);
            this.Controls.Add(this.btnAddTran);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ManageCashierTran";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.ManageCashierTran_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picMoveFirst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMovePrev)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMoveLast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMoveNext)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dvgRest)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label lblExit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        protected internal System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox ddlLoc;
        protected internal System.Windows.Forms.Button btnAddTran;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblPageNo;
        private System.Windows.Forms.PictureBox picMoveFirst;
        private System.Windows.Forms.PictureBox picMovePrev;
        private System.Windows.Forms.PictureBox picMoveLast;
        private System.Windows.Forms.PictureBox picMoveNext;
        private System.Windows.Forms.DataGridView dvgRest;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lbltoday;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtTranDate;
        private System.Windows.Forms.ComboBox ddlTranType;
    }
}