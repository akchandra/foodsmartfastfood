﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Printing;
using System.Configuration;
using FoodSmart.BLL;
using FoodSmart.Utilities;

namespace FoodSmartDesktop.POS
{
    public partial class DirectPrint : Form
    {
        //private System.Windows.Forms.Button printButton;
        private Font printFont;
        private int CashTypeID;
        private int TranType;
        private decimal CardBalance;

        //private StreamReader streamToPrint;
        //private StreamWriter streamToPrint;

        public DirectPrint(int Mode, int CashID, decimal AvailableBalance)
        {
            InitializeComponent();
            try
            {
                string prnName = ConfigurationManager.AppSettings["PrinterName"];
                PrinterSettings ps = new PrinterSettings();
                PrintDocument pd = new PrintDocument();
                //streamToPrint = new StreamWriter("D:\\MyFile.txt");
                try
                {
                    foreach (string printer in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
                    {
                        if (printer == prnName)
                        {
                            pd.PrinterSettings.PrinterName = printer;
                            break;
                        }
                    }

                    CashTypeID = CashID;
                    TranType = Mode;
                    CardBalance = AvailableBalance;
                    pd.PrintPage += new PrintPageEventHandler(this.pd_PrintPage);
                    pd.Print();
                }
                finally
                {
                    pd.Dispose();
                    //streamToPrint.Close();
                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void DirectPrint_Load(object sender, EventArgs e)
        {
            //try
            //{
                
            //    try
            //    {
            //        PrinterSettings ps = new PrinterSettings();
            //        PrintDocument pd = new PrintDocument();
            //        pd.PrinterSettings.PrinterName = ps.PrinterName;

            //        pd.PrintPage += new PrintPageEventHandler(this.pd_PrintPage);
            //        pd.Print();
            //    }
            //    finally
            //    {
            //        streamToPrint.Close();
            //    }
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message);
            //}

        }

        private void pd_PrintPage(object sender, PrintPageEventArgs e)
        {
            Single yPos = 0;

            string TempStr = "";
            float x = 10;
            float y = 5;

            float width = 270.0F; // max width I found through trial and error
            float height = 0F;
            string PaidBy;

            Font drawFontArial12Bold = new Font("Arial", 12, FontStyle.Bold);
            Font drawFontArial10Regular = new Font("Arial", 10, FontStyle.Regular);
            Font drawFontArial8Bold = new Font("Arial", 8, FontStyle.Bold);

            Single topMargin = e.MarginBounds.Top;
            string text = "";
            StringFormat sf = new StringFormat();
            // Set format of string.
            StringFormat drawFormatCenter = new StringFormat();
            drawFormatCenter.Alignment = StringAlignment.Center;
            StringFormat drawFormatLeft = new StringFormat();
            drawFormatLeft.Alignment = StringAlignment.Near;
            StringFormat drawFormatRight = new StringFormat();
            drawFormatRight.Alignment = StringAlignment.Far;

            InvoiceBLL invbll = new InvoiceBLL();
            DataSet ds = invbll.GetCashTranForPrinting(CashTypeID);

            if (ds.Tables[0].Rows[0]["PaymentMode"].ToString() == "C")
                PaidBy = "CASH";
            else if (ds.Tables[0].Rows[0]["PaymentMode"].ToString() == "R")
                PaidBy = "CREDIT CARD NO ****" + ds.Tables[0].Rows[0]["CardNo"].ToString();
            else
                PaidBy = "Meal Pass " + ds.Tables[0].Rows[0]["MealPassDesc"].ToString();

            //Image img = Image.FromFile("logo.bmp");
            //Rectangle logo = new Rectangle(40, 40, 50, 50);


         
            //e.Graphics.DrawImage(img, logo);
            //text = "Testing!";
            //leftMargin = Convert.ToInt32((40 - text.Length) / 2);
            //e.Graphics.DrawString("Testing!", printFont, Brushes.Black, leftMargin, yPos, drawFormatCenter);

            //e.Graphics.DrawString("testing Font", printFont, Brushes.Black, new RectangleF(x, y, width, height), drawFormatCenter);
            e.Graphics.DrawString(ds.Tables[1].Rows[0]["Company_Name"].ToString(), drawFontArial12Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatCenter);
            y += (1 * drawFontArial12Bold.GetHeight(e.Graphics));
            e.Graphics.DrawString(ds.Tables[1].Rows[0]["Address1"].ToString(), drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatCenter);
            y += (1 * drawFontArial10Regular.GetHeight(e.Graphics));
            e.Graphics.DrawString("Card No:" + ds.Tables[0].Rows[0]["CardNoOutside"].ToString() + "(" + ds.Tables[0].Rows[0]["Serial"].ToString() + ")", drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatCenter);
            //y += (1 * drawFontArial10Regular.GetHeight(e.Graphics));
            //e.Graphics.DrawString("Cashier:" + GeneralFunctions.LoginInfo.UserFullName, drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatCenter);
            y += (1 * drawFontArial10Regular.GetHeight(e.Graphics));
            e.Graphics.DrawLine(new Pen(Brushes.Black, 1), 1, y, 290, y);
            //e.Graphics.DrawString(string.Concat(Enumerable.Repeat("-", 55)), drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
            y += (1 * drawFontArial10Regular.GetHeight(e.Graphics));
            e.Graphics.DrawString("Dated  :" + Convert.ToDateTime(ds.Tables[0].Rows[0]["TranDate"]).ToString("dd-MMM-yyyy") + string.Concat(Enumerable.Repeat(" ", 40)) + Convert.ToDateTime(ds.Tables[0].Rows[0]["TranDate"]).ToString("hh:mm"), drawFontArial8Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
            //e.Graphics.DrawString("New list for testing", printFont, Brushes.Black, leftMargin, yPos, drawFormatLeft);
            y += (1 * drawFontArial8Bold.GetHeight(e.Graphics));
            e.Graphics.DrawLine(new Pen(Brushes.Black, 1), 1, y, 290, y);
            //e.Graphics.DrawString(string.Concat(Enumerable.Repeat("-", 55)), drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
            y += (1 * drawFontArial8Bold.GetHeight(e.Graphics));
            //e.Graphics.DrawString("Dated  : " + Convert.ToDateTime(ds.Tables[0].Rows[0]["TranDate"]).ToString("dd-MMM-yyyy") + string.Concat(Enumerable.Repeat(" ", 20)) + Convert.ToDateTime(ds.Tables[0].Rows[0]["TranDate"]).ToString("hh:mm"), drawFontArial8Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
            ////e.Graphics.DrawString("New list for testing", printFont, Brushes.Black, leftMargin, yPos, drawFormatLeft);
            //y += (1 * printFont.GetHeight(e.Graphics));
            if (TranType == 1)
            {
                e.Graphics.DrawString("Security Deposit (Rs.)   : " , drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
                e.Graphics.DrawString(Convert.ToDecimal(ds.Tables[0].Rows[0]["SecurityDeposit"]).ToString("######0.00"), drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
                y += (1 * drawFontArial10Regular.GetHeight(e.Graphics));
                e.Graphics.DrawString("Amount Activated (Rs.)   : ", drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
                e.Graphics.DrawString(Convert.ToDecimal(ds.Tables[0].Rows[0]["UsableAmount"]).ToString("######0.00"), drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
                y += (1 * drawFontArial10Regular.GetHeight(e.Graphics));
                e.Graphics.DrawString("Total Amount     (Rs.)   : ", drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
                e.Graphics.DrawString(Convert.ToDecimal(ds.Tables[0].Rows[0]["TranAmount"]).ToString("######0.00"), drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
                y += (1 * drawFontArial10Regular.GetHeight(e.Graphics));
                e.Graphics.DrawString("Paid By                  : ", drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
                e.Graphics.DrawString(PaidBy, drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);

            }
            else if (TranType == 2)
            {
                e.Graphics.DrawString("Existing Balance (Rs.)   : ", drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
                e.Graphics.DrawString(CardBalance.ToString("######0.00"), drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
                y += (1 * drawFontArial10Regular.GetHeight(e.Graphics));
                e.Graphics.DrawString("Amount Refilled  (Rs.)   : ", drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
                e.Graphics.DrawString(Convert.ToDecimal(ds.Tables[0].Rows[0]["UsableAmount"]).ToString("######0.00"), drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
                y += (1 * drawFontArial10Regular.GetHeight(e.Graphics));
                e.Graphics.DrawString("Usable Balance   (Rs.)   : ", drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
                e.Graphics.DrawString((CardBalance + Convert.ToDecimal(ds.Tables[0].Rows[0]["UsableAmount"])).ToString("######0.00"), drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
                y += (1 * drawFontArial10Regular.GetHeight(e.Graphics));
                e.Graphics.DrawString("Paid By                  : ", drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
                e.Graphics.DrawString(PaidBy, drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
            }
            else if (TranType == 3)
            {
                e.Graphics.DrawString("Security Deposit (Rs.)   : ", drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
                e.Graphics.DrawString(Convert.ToDecimal(ds.Tables[0].Rows[0]["SecurityDeposit"]).ToString("######0.00"), drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
                y += (1 * drawFontArial10Regular.GetHeight(e.Graphics));
                e.Graphics.DrawString("Amount Refilled  (Rs.)   : ", drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
                e.Graphics.DrawString(Convert.ToDecimal(ds.Tables[0].Rows[0]["UsableAmount"]).ToString("######0.00"), drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
                y += (1 * drawFontArial10Regular.GetHeight(e.Graphics));
                e.Graphics.DrawString("Total Refund     (Rs.)   : ", drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
                e.Graphics.DrawString((Convert.ToDecimal(ds.Tables[0].Rows[0]["UsableAmount"]) + Convert.ToDecimal(ds.Tables[0].Rows[0]["SecurityDeposit"])).ToString("######0.00"), drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
                y += (1 * drawFontArial10Regular.GetHeight(e.Graphics));
                e.Graphics.DrawString("Paid By                  : ", drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
                e.Graphics.DrawString(PaidBy, drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatRight);
            }
            y += (1 * drawFontArial10Regular.GetHeight(e.Graphics));
            e.Graphics.DrawLine(new Pen(Brushes.Black, 1), 1, y, 290, y);
            //e.Graphics.DrawString(string.Concat(Enumerable.Repeat("-", 55)), drawFontArial10Regular, Brushes.Black, new RectangleF(x, y, width, height), drawFormatLeft);
            y += (1 * drawFontArial10Regular.GetHeight(e.Graphics));
            e.Graphics.DrawString("Cashier:" + GeneralFunctions.LoginInfo.UserFullName, drawFontArial8Bold, Brushes.Black, new RectangleF(x, y, width, height), drawFormatCenter);
        }
    }
}
