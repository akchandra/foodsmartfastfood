﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using FoodSmart.DAL;
using FoodSmart.BLL;
using FoodSmart.Entity;
using FoodSmart.GeneralUtilities;
using FoodSmart.Utilities;
using FoodSmart.Common;

namespace FoodSmartDesktop.Inventory
{
    public partial class ManageCostCenter : Form
    {
        BindingSource source1 = new BindingSource();
        private int PgSize = 12;
        private int CurrentPageIndex = 1;
        private int TotalPage = 0;
        private DataSet dsPage;

        public ManageCostCenter()
        {
            InitializeComponent();
        }

        private void ManageCostCenter_Load(object sender, EventArgs e)
        {
            dvgRMGroup.ColumnCount = 2;

            SetWatermarkText();

            SearchCriteria searchCriteria = new SearchCriteria();
            BuildDefaultSearch(searchCriteria);

            LoadItem(searchCriteria);

            foreach (DataGridViewRow row in dvgRMGroup.Rows)
            {
                row.Height = 25;
            }

            lbltoday.Text += " : " + GeneralFunctions.LoginInfo.CurrDate.ToString("dd-MMM-yyyy");
            lblUserName.Text += GeneralFunctions.LoginInfo.UserFullName;
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAddCC_Click(object sender, EventArgs e)
        {
            this.Hide();
            Inventory.AddEditCostCenter rmg = new Inventory.AddEditCostCenter(0);

            if (rmg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {

            }
            else
            {
                this.Show();
                //dvgRMGroup.Columns.Remove("Dimg");
                dvgRMGroup.Columns.Remove("Eimg");
                dvgRMGroup.Columns.Remove("Dimg");
                SearchCriteria searchCriteria = new SearchCriteria();
                BuildDefaultSearch(searchCriteria);
                LoadItem(searchCriteria);
                foreach (DataGridViewRow row in dvgRMGroup.Rows)
                {
                    row.Height = 25;
                }
            }
        }
        private void dvgRMGroup_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int Reply;
            int i;
            if (dvgRMGroup.RowCount == 0)
                return;
            else
                i = dvgRMGroup.CurrentRow.Index;

            if (dvgRMGroup.Columns[e.ColumnIndex].Name == "Dimg")
            {
                Reply = Convert.ToInt32(MessageBox.Show("Want to Delete this Record?", "Delete Record", MessageBoxButtons.OKCancel, MessageBoxIcon.Question));
                if (Reply == 1)
                {
                    Reply = Convert.ToInt32(dvgRMGroup.CurrentRow.Cells[0].Value);

                    InventoryBLL itembll = new InventoryBLL();

                    //RestaurantBLL restBLL = new RestaurantBLL();
                    itembll.DeleteCC(Reply);

                    dvgRMGroup.Columns.Remove("Dimg");
                    dvgRMGroup.Columns.Remove("Eimg");
                    SearchCriteria searchCriteria = new SearchCriteria();
                    BuildDefaultSearch(searchCriteria);
                    LoadItem(searchCriteria);
                    foreach (DataGridViewRow row in dvgRMGroup.Rows)
                    {
                        row.Height = 25;
                    }
                }

            }

            if (dvgRMGroup.Columns[e.ColumnIndex].Name == "Eimg")
            {
                Inventory.AddEditCostCenter rmg = new Inventory.AddEditCostCenter(Convert.ToInt32(dvgRMGroup.CurrentRow.Cells[0].Value));
                this.Hide();

                if (rmg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    this.Hide();
                }
                else
                {
                    this.Show();
                    dvgRMGroup.Columns.Remove("Dimg");
                    dvgRMGroup.Columns.Remove("Eimg");
                    SearchCriteria searchCriteria = new SearchCriteria();
                    BuildDefaultSearch(searchCriteria);
                    LoadItem(searchCriteria);
                    foreach (DataGridViewRow row in dvgRMGroup.Rows)
                    {
                        row.Height = 25;
                    }
                }
            }
        }

        private void txtGroupName_TextChanged(object sender, EventArgs e)
        {
            CheckFilterCondition();
        }

        private void picMoveFirst_Click(object sender, EventArgs e)
        {
            if (CurrentPageIndex != 0)
            {
                CurrentPageIndex = 0;
                ChangePageIndex();
            }
        }

        private void picMovePrev_Click(object sender, EventArgs e)
        {
            if (CurrentPageIndex != 0)
            {
                CurrentPageIndex--;
                ChangePageIndex();
            }
        }

        private void picMoveNext_Click(object sender, EventArgs e)
        {
            if (CurrentPageIndex + 1 < TotalPage)
            {
                CurrentPageIndex++;
                ChangePageIndex();
            }
        }

        private void picMoveLast_Click(object sender, EventArgs e)
        {
            if (CurrentPageIndex + 1 < TotalPage)
            {
                CurrentPageIndex = TotalPage - 1;
                ChangePageIndex();
            }
        }

        #region Private method
        private void ChangePageIndex()
        {
            DataView view1;
            view1 = new DataView(GeneralFunctions.GetCurrentRecords(CurrentPageIndex, dsPage.Tables[0], PgSize, "CCID"));
            source1.DataSource = view1;
            int pg = CurrentPageIndex + 1;
            lblPageNo.Text = pg.ToString() + " / " + TotalPage.ToString();
            foreach (DataGridViewRow row in dvgRMGroup.Rows)
            {
                row.Height = 25;
            }
        }

        private void LoadItem(SearchCriteria searchCriteria)
        {
            string path = Application.StartupPath + "\\";
            DataView view1;

            DataGridViewCellStyle style = new DataGridViewCellStyle();
            dvgRMGroup.EnableHeadersVisualStyles = false;
            style.ForeColor = Color.Blue;

            dvgRMGroup.AutoGenerateColumns = false;

            dvgRMGroup.Columns[0].Name = "CCID";
            dvgRMGroup.Columns[1].Name = "CC Name";
            dvgRMGroup.Columns[1].HeaderText = "Cost Center Name";
            dvgRMGroup.Columns[0].DataPropertyName = "CCID";
            dvgRMGroup.Columns[1].DataPropertyName = "CostCenterName";

            dvgRMGroup.Columns[1].Width = 600;

            dvgRMGroup.Columns[1].SortMode = DataGridViewColumnSortMode.Automatic;

            dvgRMGroup.Columns[0].Visible = false;

            DataGridViewImageColumn Editimg = new DataGridViewImageColumn();
            Image Editimage = Image.FromFile(path + ConfigurationManager.AppSettings["imageFolder"] + "edit-icon.png");
            Editimg.Image = Editimage;
            dvgRMGroup.Columns.Add(Editimg);
            Editimg.HeaderText = "Edit";
            Editimg.Name = "Eimg";
            Editimg.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            Editimg.Width = 40;

            DataGridViewImageColumn Delimg = new DataGridViewImageColumn();
            Image Delimage = Image.FromFile(path + ConfigurationManager.AppSettings["imageFolder"] + "delete-icon.png");
            Delimg.Image = Delimage;
            dvgRMGroup.Columns.Add(Delimg);
            Delimg.HeaderText = "Del";
            Delimg.Name = "Dimg";
            Delimg.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            Delimg.Width = 40;

            dvgRMGroup.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dvgRMGroup.MultiSelect = false;

            InventoryBLL grpbll = new InventoryBLL();
            dsPage = grpbll.GetAllCC(searchCriteria);
            if (dsPage.Tables[0].Rows.Count == 0)
                view1 = null;
            else
            {
                TotalPage = GeneralFunctions.CalculateTotalPages(dsPage.Tables[0], PgSize);
                view1 = new DataView(GeneralFunctions.GetCurrentRecords(0, dsPage.Tables[0], PgSize, "CCID"));
                CurrentPageIndex = 0;
                lblPageNo.Text = 1.ToString() + " / " + TotalPage.ToString();

            }
            source1.DataSource = view1;

            dvgRMGroup.DataSource = source1;
            dvgRMGroup.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            dvgRMGroup.AllowUserToResizeColumns = false;
            dvgRMGroup.AllowUserToAddRows = false;
            dvgRMGroup.ReadOnly = true;
            dvgRMGroup.Columns[1].HeaderCell.Style = style;
        }


        private void BuildDefaultSearch(SearchCriteria criteria)
        {
            criteria.GroupName = "";
        }

        private void BuildSearchCriteria(SearchCriteria criteria)
        {
            criteria.GroupName = txtCCName.Text;
        }

        private void CheckFilterCondition()
        {
            SearchCriteria searchCriteria = new SearchCriteria();
            BuildSearchCriteria(searchCriteria);
            LoadItem(searchCriteria);
            dvgRMGroup.Columns.Remove("Dimg");
            dvgRMGroup.Columns.Remove("Eimg");
            //LoadRestaurants(searchCriteria);
            foreach (DataGridViewRow row in dvgRMGroup.Rows)
            {
                row.Height = 25;
            }
        }

        private void SetWatermarkText()
        {
            WatermarkText.SetWatermark(txtCCName, "Cost Center");
        }
        #endregion

        
    }
}
