﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FoodSmart.BLL;
using FoodSmart.Common;
using System.Configuration;
using System.IO;
using System.Drawing.Drawing2D;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using FoodSmart.Utilities.Cryptography;

namespace FoodSmartDesktop.Inventory
{
    public partial class AddEditRM : Form
    {
        string stat;
        int LID;
        int GroupID;

        public AddEditRM(int ID)
        {
            InitializeComponent();
            if (ID == 0)
            {
                stat = "A";
                LID = 0;
            }
            else
            {
                stat = "E";

                LID = ID;
            }
        }

        #region public method
        private void AddEditRM_Load(object sender, EventArgs e)
        {
            txtRatePerUnit.MaxLength = 14;
            txtRMCode.MaxLength = 10;
            txtRMName.MaxLength = 30;
            txtRMCode.CharacterCasing = CharacterCasing.Upper;
            txtRMName.CharacterCasing = CharacterCasing.Upper;

            LoadItemGroup();

            if (LID != 0)
                fillItem(LID);

            txtRMName.Focus();
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (ValidateEntry())
            {
                IInventory rmItem = new InventoryEntity();
                rmItem.RMID = LID;
                rmItem.GroupID = ddlGroup.SelectedValue.ToInt();
                rmItem.RMName = txtRMName.Text;
                rmItem.RMUOM = txtUnit.Text;
                rmItem.RMRate = txtRatePerUnit.Text.ToDecimal();
                rmItem.RMCode = txtRMCode.Text;
                //rmItem.CreatedBy = GeneralFunctions.LoginInfo.UserID;

                int Status = new InventoryBLL().SaveRM(rmItem, stat, GeneralFunctions.LoginInfo.UserID);

                if (Status == 0)
                {
                    MessageBox.Show("Error!! Record Not Saved", "Save Record", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    if (stat == "A")
                    {
                        Initialize();
                        txtRMName.Focus();
                    }
                    else
                    {
                        this.Close();
                    }
                }
            }
        }

        private void lblExit_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void lblExit_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnSave_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnSave_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void txtRatePerUnit_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        #endregion

        #region "Private Methods"

        private void LoadItemGroup()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            SearchCriteria searchcriteria = new SearchCriteria();
            searchcriteria.QueryStat = "L";
            //searchcriteria.LocName = ddlLoc.Text;
            //searchcriteria.RestName = ddlRestName.Text;

            ddlGroup.DataSource = null;

            ds = new InventoryBLL().GetAllRMGroup(searchcriteria);
            dt = ds.Tables[0];

            //DataRow nullrow = dt.NewRow();
            //nullrow["GroupID"] = 0;
            //nullrow["RMGroupdescr"] = "All Groups";
            //dt.Rows.Add(nullrow);
            if (dt.Rows.Count > 0)
            {
                ddlGroup.DataSource = dt;
                ddlGroup.DisplayMember = "RMGroupdescr";
                ddlGroup.ValueMember = "GroupID";
            }
            ddlGroup.SelectedIndex = 0;
        }
        
        private void Initialize()
        {
            ddlGroup.SelectedIndex = 0;
            txtRMName.Text = string.Empty;
            txtRMCode.Text = string.Empty;
            txtRatePerUnit.Text = string.Empty;
            txtUnit.Text = string.Empty;
        }


        private bool ValidateEntry()
        {
            bool RetVal = true;

            if (string.IsNullOrEmpty(txtUnit.Text))
            {
                txtUnit.BackColor = Color.Red;
                RetVal = false;
            }

            if (string.IsNullOrEmpty(txtRMName.Text))
            {
                txtRMName.BackColor = Color.Red;
                RetVal = false;
            }
            return RetVal;
        }

        private void fillItem(int GroupID)
        {
            InventoryBLL oinvbll = new InventoryBLL();
            InventoryEntity oinv = (InventoryEntity)oinvbll.GetRMForEdit(GroupID, GeneralFunctions.LoginInfo.UserID);

            txtRMName.Text = oinv.RMName;
            txtRMCode.Text = oinv.RMCode;
            ddlGroup.SelectedValue = oinv.GroupID;
            txtUnit.Text = oinv.RMUOM;
            txtRatePerUnit.Text = oinv.RMRate.ToString();
            if (GeneralFunctions.LoginInfo.UserRole == 2 || (GeneralFunctions.LoginInfo.UserRole == 4))
                btnSave.Visible = false;

        }
        #endregion
    }
}
