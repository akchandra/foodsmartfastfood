﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FoodSmart.BLL;
using FoodSmart.Common;
using System.Configuration;
using System.IO;
using System.Drawing.Drawing2D;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using FoodSmart.Utilities.Cryptography;

namespace FoodSmartDesktop.Inventory
{
    public partial class AddEditVendor : Form
    {
        string stat;
        int LID;
        int GroupID;

        public AddEditVendor(int ID)
        {
            InitializeComponent();
            if (ID == 0)
            {
                stat = "A";
                LID = 0;
            }
            else
            {
                stat = "E";

                LID = ID;
            }
        }

        #region public method
        private void AddEditVendor_Load(object sender, EventArgs e)
        {
            
            txtVendorName.MaxLength = 100;
            txtAddress.MaxLength = 300;
            txtContactPerson.MaxLength = 50;
            txtContMob.MaxLength = 20;
            txtEmailID.MaxLength = 50;
            txtVATNo.MaxLength = 50;
            txtPANNo.MaxLength = 10;
            txtSTNo.MaxLength = 50;
            txtPhone.MaxLength = 20;

            txtVendorName.CharacterCasing = CharacterCasing.Upper;
            txtContactPerson.CharacterCasing = CharacterCasing.Upper;
            txtVATNo.CharacterCasing = CharacterCasing.Upper;
            txtPANNo.CharacterCasing = CharacterCasing.Upper;
            txtSTNo.CharacterCasing = CharacterCasing.Upper;
            txtContMob.CharacterCasing = CharacterCasing.Upper;

            if (LID != 0)
                fillItem(LID);

            txtVendorName.Focus();
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (ValidateEntry())
            {
                IVendor rmItem = new VendorEntity();
                rmItem.VendorID = LID;
                rmItem.VendorName = txtVendorName.Text;
                rmItem.VendorAddress = txtAddress.Text;
                rmItem.ContactPerson = txtContactPerson.Text;
                rmItem.ContactMob = txtContMob.Text;
                rmItem.VATNo = txtVATNo.Text;
                rmItem.ServiceTaxNo = txtSTNo.Text;
                rmItem.PANNo = txtPANNo.Text;
                rmItem.LandPhone = txtPhone.Text;
                rmItem.emailID = txtEmailID.Text;

                rmItem.CreatedBy = GeneralFunctions.LoginInfo.UserID;

                int Status = new VendorBLL().SaveVendor(rmItem, stat);

                if (Status == 0)
                {
                    MessageBox.Show("Error!! Record Not Saved", "Save Record", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    if (stat == "A")
                    {
                        Initialize();
                        txtVendorName.Focus();
                    }
                    else
                    {
                        this.Close();
                    }
                }
            }
        }

        private void lblExit_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void lblExit_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnSave_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnSave_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        #endregion

        #region "Private Methods"

        private void Initialize()
        {
            txtVendorName.Text = string.Empty;
            txtAddress.Text = string.Empty;
            txtContactPerson.Text = string.Empty;
            txtContMob.Text = string.Empty;
            txtEmailID.Text = string.Empty;
            txtPANNo.Text = string.Empty;
            txtPhone.Text = string.Empty;
            txtSTNo.Text = string.Empty;
            txtVATNo.Text = string.Empty;
        }

        private bool ValidateEntry()
        {
            bool RetVal = true;

            if (string.IsNullOrEmpty(txtVendorName.Text))
            {
                txtVendorName.BackColor = Color.Red;
                RetVal = false;
            }

            if (string.IsNullOrEmpty(txtAddress.Text))
            {
                txtAddress.BackColor = Color.Red;
                RetVal = false;
            }
            return RetVal;
        }

        private void fillItem(int GroupID)
        {
            VendorBLL ven = new VendorBLL();
            VendorEntity oven = (VendorEntity)ven.GetVendorForEdit(GroupID, GeneralFunctions.LoginInfo.UserID);

            txtVendorName.Text = oven.VendorName.ToString();
            txtAddress.Text = oven.VendorAddress.ToString();
            txtContactPerson.Text = oven.ContactPerson.ToString();
            txtContMob.Text = oven.ContactMob.ToString();
            txtEmailID.Text = oven.emailID.ToString();
            txtPANNo.Text = oven.PANNo.ToString();
            txtPhone.Text = oven.LandPhone.ToString();
            txtSTNo.Text = oven.ServiceTaxNo.ToString();
            txtVATNo.Text = oven.VATNo.ToString();

            if (GeneralFunctions.LoginInfo.UserRole == 2 || (GeneralFunctions.LoginInfo.UserRole == 4))
                btnSave.Visible = false;

        }

        #endregion
    }
}
