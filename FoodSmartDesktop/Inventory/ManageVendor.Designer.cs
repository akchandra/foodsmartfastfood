﻿namespace FoodSmartDesktop.Inventory
{
    partial class ManageVendor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ManageVendor));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtVendorPAN = new System.Windows.Forms.TextBox();
            this.txtVendorName = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblPageNo = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lbltoday = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dvgVendor = new System.Windows.Forms.DataGridView();
            this.btnAddVendor = new System.Windows.Forms.Button();
            this.picMoveFirst = new System.Windows.Forms.PictureBox();
            this.picMovePrev = new System.Windows.Forms.PictureBox();
            this.picMoveLast = new System.Windows.Forms.PictureBox();
            this.picMoveNext = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.lblExit = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvgVendor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMoveFirst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMovePrev)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMoveLast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMoveNext)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(199)))), ((int)(((byte)(233)))));
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.lblUserName);
            this.panel1.Controls.Add(this.lblExit);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(1, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1062, 60);
            this.panel1.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(425, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(157, 23);
            this.label2.TabIndex = 26;
            this.label2.Text = "Manage Vendor";
            // 
            // lblUserName
            // 
            this.lblUserName.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblUserName.Location = new System.Drawing.Point(756, 20);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(185, 27);
            this.lblUserName.TabIndex = 19;
            this.lblUserName.Text = "Welcome ";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox1.Controls.Add(this.txtVendorPAN);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.txtVendorName);
            this.groupBox1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox1.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox1.Location = new System.Drawing.Point(1, 64);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Size = new System.Drawing.Size(1062, 55);
            this.groupBox1.TabIndex = 28;
            this.groupBox1.TabStop = false;
            // 
            // txtVendorPAN
            // 
            this.txtVendorPAN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVendorPAN.Location = new System.Drawing.Point(418, 16);
            this.txtVendorPAN.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtVendorPAN.Name = "txtVendorPAN";
            this.txtVendorPAN.Size = new System.Drawing.Size(190, 26);
            this.txtVendorPAN.TabIndex = 19;
            this.txtVendorPAN.TextChanged += new System.EventHandler(this.txtVendorPAN_TextChanged);
            // 
            // txtVendorName
            // 
            this.txtVendorName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVendorName.Location = new System.Drawing.Point(9, 16);
            this.txtVendorName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtVendorName.Name = "txtVendorName";
            this.txtVendorName.Size = new System.Drawing.Size(403, 26);
            this.txtVendorName.TabIndex = 0;
            this.txtVendorName.TextChanged += new System.EventHandler(this.txtVendorName_TextChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblPageNo);
            this.groupBox2.Controls.Add(this.picMoveFirst);
            this.groupBox2.Controls.Add(this.picMovePrev);
            this.groupBox2.Controls.Add(this.picMoveLast);
            this.groupBox2.Controls.Add(this.picMoveNext);
            this.groupBox2.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(1, 121);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox2.Size = new System.Drawing.Size(282, 51);
            this.groupBox2.TabIndex = 29;
            this.groupBox2.TabStop = false;
            // 
            // lblPageNo
            // 
            this.lblPageNo.AutoSize = true;
            this.lblPageNo.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPageNo.Location = new System.Drawing.Point(86, 18);
            this.lblPageNo.Name = "lblPageNo";
            this.lblPageNo.Size = new System.Drawing.Size(102, 20);
            this.lblPageNo.TabIndex = 17;
            this.lblPageNo.Text = "Current Page : ";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Black;
            this.panel2.Controls.Add(this.lbltoday);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Location = new System.Drawing.Point(1, 512);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1062, 41);
            this.panel2.TabIndex = 34;
            // 
            // lbltoday
            // 
            this.lbltoday.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltoday.ForeColor = System.Drawing.Color.Transparent;
            this.lbltoday.Location = new System.Drawing.Point(17, 9);
            this.lbltoday.Name = "lbltoday";
            this.lbltoday.Size = new System.Drawing.Size(185, 27);
            this.lbltoday.TabIndex = 20;
            this.lbltoday.Text = "Today";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Snow;
            this.label3.Location = new System.Drawing.Point(942, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 16);
            this.label3.TabIndex = 1;
            this.label3.Text = "Powered By Europia";
            // 
            // dvgVendor
            // 
            this.dvgVendor.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dvgVendor.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dvgVendor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dvgVendor.DefaultCellStyle = dataGridViewCellStyle2;
            this.dvgVendor.Location = new System.Drawing.Point(1, 173);
            this.dvgVendor.Name = "dvgVendor";
            this.dvgVendor.RowHeadersVisible = false;
            this.dvgVendor.Size = new System.Drawing.Size(1062, 332);
            this.dvgVendor.TabIndex = 33;
            this.dvgVendor.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dvgVendor_CellContentClick);
            // 
            // btnAddVendor
            // 
            this.btnAddVendor.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAddVendor.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAddVendor.BackgroundImage")));
            this.btnAddVendor.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAddVendor.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddVendor.ForeColor = System.Drawing.SystemColors.Control;
            this.btnAddVendor.Image = ((System.Drawing.Image)(resources.GetObject("btnAddVendor.Image")));
            this.btnAddVendor.Location = new System.Drawing.Point(909, 121);
            this.btnAddVendor.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnAddVendor.Name = "btnAddVendor";
            this.btnAddVendor.Size = new System.Drawing.Size(154, 50);
            this.btnAddVendor.TabIndex = 30;
            this.btnAddVendor.Text = "Add New Vendor";
            this.btnAddVendor.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAddVendor.UseVisualStyleBackColor = false;
            this.btnAddVendor.Click += new System.EventHandler(this.btnAddVendor_Click);
            this.btnAddVendor.MouseLeave += new System.EventHandler(this.btnAddVendor_MouseLeave);
            this.btnAddVendor.MouseHover += new System.EventHandler(this.btnAddVendor_MouseHover);
            // 
            // picMoveFirst
            // 
            this.picMoveFirst.Image = ((System.Drawing.Image)(resources.GetObject("picMoveFirst.Image")));
            this.picMoveFirst.Location = new System.Drawing.Point(19, 18);
            this.picMoveFirst.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.picMoveFirst.Name = "picMoveFirst";
            this.picMoveFirst.Size = new System.Drawing.Size(23, 21);
            this.picMoveFirst.TabIndex = 13;
            this.picMoveFirst.TabStop = false;
            this.picMoveFirst.Click += new System.EventHandler(this.picMoveFirst_Click);
            // 
            // picMovePrev
            // 
            this.picMovePrev.Image = ((System.Drawing.Image)(resources.GetObject("picMovePrev.Image")));
            this.picMovePrev.Location = new System.Drawing.Point(49, 18);
            this.picMovePrev.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.picMovePrev.Name = "picMovePrev";
            this.picMovePrev.Size = new System.Drawing.Size(23, 21);
            this.picMovePrev.TabIndex = 14;
            this.picMovePrev.TabStop = false;
            this.picMovePrev.Click += new System.EventHandler(this.picMovePrev_Click);
            // 
            // picMoveLast
            // 
            this.picMoveLast.Image = ((System.Drawing.Image)(resources.GetObject("picMoveLast.Image")));
            this.picMoveLast.Location = new System.Drawing.Point(250, 18);
            this.picMoveLast.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.picMoveLast.Name = "picMoveLast";
            this.picMoveLast.Size = new System.Drawing.Size(23, 21);
            this.picMoveLast.TabIndex = 16;
            this.picMoveLast.TabStop = false;
            this.picMoveLast.Click += new System.EventHandler(this.picMoveLast_Click);
            // 
            // picMoveNext
            // 
            this.picMoveNext.Image = ((System.Drawing.Image)(resources.GetObject("picMoveNext.Image")));
            this.picMoveNext.Location = new System.Drawing.Point(219, 18);
            this.picMoveNext.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.picMoveNext.Name = "picMoveNext";
            this.picMoveNext.Size = new System.Drawing.Size(23, 21);
            this.picMoveNext.TabIndex = 15;
            this.picMoveNext.TabStop = false;
            this.picMoveNext.Click += new System.EventHandler(this.picMoveNext_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button1.BackgroundImage")));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.Control;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(1006, 14);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(50, 41);
            this.button1.TabIndex = 18;
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = false;
            // 
            // lblExit
            // 
            this.lblExit.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExit.ForeColor = System.Drawing.Color.DarkRed;
            this.lblExit.Image = ((System.Drawing.Image)(resources.GetObject("lblExit.Image")));
            this.lblExit.Location = new System.Drawing.Point(989, 2);
            this.lblExit.Name = "lblExit";
            this.lblExit.Size = new System.Drawing.Size(73, 58);
            this.lblExit.TabIndex = 1;
            this.lblExit.Click += new System.EventHandler(this.lblExit_Click);
            this.lblExit.MouseLeave += new System.EventHandler(this.lblExit_MouseLeave);
            this.lblExit.MouseHover += new System.EventHandler(this.lblExit_MouseHover);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Monotype Corsiva", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
            this.label1.Location = new System.Drawing.Point(3, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(288, 34);
            this.label1.TabIndex = 0;
            // 
            // ManageVendor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1070, 555);
            this.ControlBox = false;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.dvgVendor);
            this.Controls.Add(this.btnAddVendor);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ManageVendor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.ManageVendor_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dvgVendor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMoveFirst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMovePrev)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMoveLast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMoveNext)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label lblExit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtVendorPAN;
        protected internal System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtVendorName;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblPageNo;
        private System.Windows.Forms.PictureBox picMoveFirst;
        private System.Windows.Forms.PictureBox picMovePrev;
        private System.Windows.Forms.PictureBox picMoveLast;
        private System.Windows.Forms.PictureBox picMoveNext;
        protected internal System.Windows.Forms.Button btnAddVendor;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lbltoday;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dvgVendor;
    }
}