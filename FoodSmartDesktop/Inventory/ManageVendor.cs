﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using FoodSmart.DAL;
using FoodSmart.BLL;
using FoodSmart.Entity;
using FoodSmart.GeneralUtilities;
using FoodSmart.Utilities;
using FoodSmart.Common;


namespace FoodSmartDesktop.Inventory
{
    public partial class ManageVendor : Form
    {
        BindingSource source1 = new BindingSource();
        private int PgSize = 12;
        private int CurrentPageIndex = 1;
        private int TotalPage = 0;
        private DataSet dsPage;

        public ManageVendor()
        {
            InitializeComponent();
        }
        #region Public Method
        private void ManageVendor_Load(object sender, EventArgs e)
        {
            dvgVendor.ColumnCount = 8;

            SetWatermarkText();
            SearchCriteria searchCriteria = new SearchCriteria();
            BuildDefaultSearch(searchCriteria);

            LoadVendor(searchCriteria);

            foreach (DataGridViewRow row in dvgVendor.Rows)
            {
                row.Height = 25;
            }

            lbltoday.Text += " : " + GeneralFunctions.LoginInfo.CurrDate.ToString("dd-MMM-yyyy");
            lblUserName.Text += GeneralFunctions.LoginInfo.UserFullName;
            txtVendorName.Focus();
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void picMoveFirst_Click(object sender, EventArgs e)
        {
            if (CurrentPageIndex != 0)
            {
                CurrentPageIndex = 0;
                ChangePageIndex();
            }
        }

        private void picMovePrev_Click(object sender, EventArgs e)
        {
            if (CurrentPageIndex != 0)
            {
                CurrentPageIndex--;
                ChangePageIndex();
            }
        }

        private void picMoveNext_Click(object sender, EventArgs e)
        {
            if (CurrentPageIndex + 1 < TotalPage)
            {
                CurrentPageIndex++;
                ChangePageIndex();
            }
        }

        private void picMoveLast_Click(object sender, EventArgs e)
        {
            if (CurrentPageIndex + 1 < TotalPage)
            {
                CurrentPageIndex = TotalPage - 1;
                ChangePageIndex();
            }
        }

        private void txtVendorName_TextChanged(object sender, EventArgs e)
        {
            CheckFilterCondition();
        }

        private void txtVendorPAN_TextChanged(object sender, EventArgs e)
        {
            CheckFilterCondition();
        }

        private void btnAddVendor_Click(object sender, EventArgs e)
        {
            this.Hide();
            Inventory.AddEditVendor rmg = new Inventory.AddEditVendor(0);

            if (rmg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {

            }
            else
            {
                this.Show();
                //dvgRMGroup.Columns.Remove("Dimg");
                dvgVendor.Columns.Remove("Eimg");
                dvgVendor.Columns.Remove("Dimg");
                SearchCriteria searchCriteria = new SearchCriteria();
                BuildDefaultSearch(searchCriteria);
                LoadVendor(searchCriteria);
                foreach (DataGridViewRow row in dvgVendor.Rows)
                {
                    row.Height = 25;
                }
            }
        }

        private void lblExit_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void lblExit_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnAddVendor_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnAddVendor_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void dvgVendor_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int Reply;
            int i;
            if (dvgVendor.RowCount == 0)
                return;
            else
                i = dvgVendor.CurrentRow.Index;

            if (dvgVendor.Columns[e.ColumnIndex].Name == "Dimg")
            {
                Reply = Convert.ToInt32(MessageBox.Show("Want to Delete this Record?", "Delete Record", MessageBoxButtons.OKCancel, MessageBoxIcon.Question));
                if (Reply == 1)
                {
                    Reply = Convert.ToInt32(dvgVendor.CurrentRow.Cells[0].Value);

                    VendorBLL itembll = new VendorBLL();

                    //RestaurantBLL restBLL = new RestaurantBLL();
                    itembll.DeleteVendor(Reply);

                    dvgVendor.Columns.Remove("Dimg");
                    dvgVendor.Columns.Remove("Eimg");
                    SearchCriteria searchCriteria = new SearchCriteria();
                    BuildDefaultSearch(searchCriteria);
                    LoadVendor(searchCriteria);
                    foreach (DataGridViewRow row in dvgVendor.Rows)
                    {
                        row.Height = 25;
                    }
                }

            }

            if (dvgVendor.Columns[e.ColumnIndex].Name == "Eimg")
            {
                Inventory.AddEditVendor rmg = new Inventory.AddEditVendor(Convert.ToInt32(dvgVendor.CurrentRow.Cells[0].Value));
                this.Hide();

                if (rmg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    this.Hide();
                }
                else
                {
                    this.Show();
                    dvgVendor.Columns.Remove("Dimg");
                    dvgVendor.Columns.Remove("Eimg");
                    SearchCriteria searchCriteria = new SearchCriteria();
                    BuildDefaultSearch(searchCriteria);
                    LoadVendor(searchCriteria);
                    foreach (DataGridViewRow row in dvgVendor.Rows)
                    {
                        row.Height = 25;
                    }
                }
            }
        }

        #endregion

        #region Private method
        private void ChangePageIndex()
        {
            DataView view1;
            view1 = new DataView(GeneralFunctions.GetCurrentRecords(CurrentPageIndex, dsPage.Tables[0], PgSize, "VendorID"));
            source1.DataSource = view1;
            int pg = CurrentPageIndex + 1;
            lblPageNo.Text = pg.ToString() + " / " + TotalPage.ToString();
            foreach (DataGridViewRow row in dvgVendor.Rows)
            {
                row.Height = 25;
            }
        }

        private void LoadVendor(SearchCriteria searchCriteria)
        {
            string path = Application.StartupPath + "\\";
            DataView view1;

            DataGridViewCellStyle style = new DataGridViewCellStyle();
            dvgVendor.EnableHeadersVisualStyles = false;
            style.ForeColor = Color.Blue;

            dvgVendor.AutoGenerateColumns = false;

            dvgVendor.Columns[0].Name = "VendorID";
            dvgVendor.Columns[1].Name = "Vendor Name";
            //dvgVendor.Columns[2].Name = "Address";
            dvgVendor.Columns[2].Name = "LandPhone";
            dvgVendor.Columns[3].Name = "ContactPerson";
            dvgVendor.Columns[4].Name = "Contact Mob";
            dvgVendor.Columns[5].Name = "VATNo";
            dvgVendor.Columns[6].Name = "ServiceTaxNo";
            dvgVendor.Columns[7].Name = "PANNo";

            dvgVendor.Columns[0].HeaderText = "VendorID";
            dvgVendor.Columns[1].HeaderText = "Vendor Name";
            //dvgVendor.Columns[2].HeaderText = "Vendor Address";
            dvgVendor.Columns[2].HeaderText = "Land Phone";
            dvgVendor.Columns[3].HeaderText = "Contact Person";
            dvgVendor.Columns[4].HeaderText = "Contact Mob";
            dvgVendor.Columns[5].HeaderText = "VAT No";
            dvgVendor.Columns[6].HeaderText = "S.Tax No";
            dvgVendor.Columns[7].HeaderText = "PAN No";

            dvgVendor.Columns[0].DataPropertyName = "pk_VendorID";
            dvgVendor.Columns[1].DataPropertyName = "VendorName";
            //dvgVendor.Columns[2].HeaderText = "Vendor Address";
            dvgVendor.Columns[2].DataPropertyName = "LandPhone";
            dvgVendor.Columns[3].DataPropertyName = "ContactPerson";
            dvgVendor.Columns[4].DataPropertyName = "ContactMob";
            dvgVendor.Columns[5].DataPropertyName = "VATNo";
            dvgVendor.Columns[6].DataPropertyName = "ServiceTaxNo";
            dvgVendor.Columns[7].DataPropertyName = "PANNo";

            dvgVendor.Columns[1].Width = 310;
            dvgVendor.Columns[2].Width = 100;
            dvgVendor.Columns[3].Width = 200;
            dvgVendor.Columns[4].Width = 150;
            dvgVendor.Columns[5].Width = 120;
            dvgVendor.Columns[6].Width = 120;
            dvgVendor.Columns[7].Width = 120;

            dvgVendor.Columns[1].SortMode = DataGridViewColumnSortMode.Automatic;
            dvgVendor.Columns[2].SortMode = DataGridViewColumnSortMode.Automatic;
            dvgVendor.Columns[3].SortMode = DataGridViewColumnSortMode.Automatic;

            dvgVendor.Columns[0].Visible = false;
            dvgVendor.Columns[4].Visible = false;
            //dvgRMGroup.Columns[9].Visible = false;

            DataGridViewImageColumn Editimg = new DataGridViewImageColumn();
            Image Editimage = Image.FromFile(path + ConfigurationManager.AppSettings["imageFolder"] + "edit-icon.png");
            Editimg.Image = Editimage;
            dvgVendor.Columns.Add(Editimg);
            Editimg.HeaderText = "Edit";
            Editimg.Name = "Eimg";
            Editimg.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            Editimg.Width = 40;

            DataGridViewImageColumn Delimg = new DataGridViewImageColumn();
            Image Delimage = Image.FromFile(path + ConfigurationManager.AppSettings["imageFolder"] + "delete-icon.png");
            Delimg.Image = Delimage;
            dvgVendor.Columns.Add(Delimg);
            Delimg.HeaderText = "Del";
            Delimg.Name = "Dimg";
            Delimg.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            Delimg.Width = 40;

            dvgVendor.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dvgVendor.MultiSelect = false;

            VendorBLL grpbll = new VendorBLL();
            dsPage = grpbll.GetAllVendor(searchCriteria);
            if (dsPage.Tables[0].Rows.Count == 0)
                view1 = null;
            else
            {
                TotalPage = GeneralFunctions.CalculateTotalPages(dsPage.Tables[0], PgSize);
                view1 = new DataView(GeneralFunctions.GetCurrentRecords(0, dsPage.Tables[0], PgSize, "pk_VendorID"));
                CurrentPageIndex = 0;
                lblPageNo.Text = 1.ToString() + " / " + TotalPage.ToString();

            }
            source1.DataSource = view1;

            dvgVendor.DataSource = source1;
            dvgVendor.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            dvgVendor.AllowUserToResizeColumns = false;
            dvgVendor.AllowUserToAddRows = false;
            dvgVendor.ReadOnly = true;
            dvgVendor.Columns[1].HeaderCell.Style = style;
            dvgVendor.Columns[2].HeaderCell.Style = style;
            dvgVendor.Columns[3].HeaderCell.Style = style;

            //foreach (DataGridViewRow row in dvgVendor.Rows)
            //{
            //    string RowType = row.Cells["RM"].Value.ToString();

            //    if (RowType == "1")
            //    {
            //        row.Cells["Dimg"].Value = new Bitmap(1, 1);
            //    }
            //}
        }


        private void BuildDefaultSearch(SearchCriteria criteria)
        {
            criteria.StringOption1 = "";
            criteria.StringOption2 = "";

        }

        private void BuildSearchCriteria(SearchCriteria criteria)
        {
            criteria.StringOption1 = txtVendorName.Text;
            criteria.StringOption2 = txtVendorPAN.Text;
        }

        private void CheckFilterCondition()
        {
            SearchCriteria searchCriteria = new SearchCriteria();
            BuildSearchCriteria(searchCriteria);
            LoadVendor(searchCriteria);
            dvgVendor.Columns.Remove("Dimg");
            dvgVendor.Columns.Remove("Eimg");
            //LoadRestaurants(searchCriteria);
            foreach (DataGridViewRow row in dvgVendor.Rows)
            {
                row.Height = 25;
            }
        }

        private void SetWatermarkText()
        {

            WatermarkText.SetWatermark(txtVendorName, "Vendor Name");
            WatermarkText.SetWatermark(txtVendorPAN, "PAN No");
        }

        #endregion

    }
}
