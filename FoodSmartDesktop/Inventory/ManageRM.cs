﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using FoodSmart.DAL;
using FoodSmart.BLL;
using FoodSmart.Entity;
using FoodSmart.GeneralUtilities;
using FoodSmart.Utilities;
using FoodSmart.Common;


namespace FoodSmartDesktop.Inventory
{
    public partial class ManageRM : Form
    {
        BindingSource source1 = new BindingSource();
        private int PgSize = 12;
        private int CurrentPageIndex = 1;
        private int TotalPage = 0;
        private DataSet dsPage;

        public ManageRM()
        {
            InitializeComponent();
        }

        #region Public Method
        private void ManageRM_Load(object sender, EventArgs e)
        {
            dvgRM.ColumnCount = 7;
            this.ddlGroup.SelectedIndexChanged -= new System.EventHandler(this.ddlGroup_SelectedIndexChanged);

            SetWatermarkText();
            LoadLoc();
            LoadItemGroup();
            ddlGroup.SelectedValue = "0";

            SearchCriteria searchCriteria = new SearchCriteria();
            BuildDefaultSearch(searchCriteria);

            LoadItem(searchCriteria);

            this.ddlGroup.SelectedIndexChanged += new System.EventHandler(this.ddlGroup_SelectedIndexChanged);
            foreach (DataGridViewRow row in dvgRM.Rows)
            {
                row.Height = 25;
            }

            lbltoday.Text += " : " + GeneralFunctions.LoginInfo.CurrDate.ToString("dd-MMM-yyyy");
            lblUserName.Text += GeneralFunctions.LoginInfo.UserFullName;
            ddlGroup.Focus();
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAddItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Inventory.AddEditRM rmg = new Inventory.AddEditRM(0);

            if (rmg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {

            }
            else
            {
                this.Show();
                //dvgRM.Columns.Remove("Dimg");
                dvgRM.Columns.Remove("Eimg");
                dvgRM.Columns.Remove("Dimg");
                SearchCriteria searchCriteria = new SearchCriteria();
                BuildDefaultSearch(searchCriteria);
                LoadItem(searchCriteria);
                foreach (DataGridViewRow row in dvgRM.Rows)
                {
                    row.Height = 25;
                }
            }
        }

        private void dvgRM_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int Reply;
            int i;
            if (dvgRM.RowCount == 0)
                return;
            else
                i = dvgRM.CurrentRow.Index;

            if (dvgRM.Columns[e.ColumnIndex].Name == "Dimg")
            {
                Reply = Convert.ToInt32(MessageBox.Show("Want to Delete this Record?", "Delete Record", MessageBoxButtons.OKCancel, MessageBoxIcon.Question));
                if (Reply == 1)
                {
                    Reply = Convert.ToInt32(dvgRM.CurrentRow.Cells[0].Value);

                    InventoryBLL itembll = new InventoryBLL();

                    //RestaurantBLL restBLL = new RestaurantBLL();
                    itembll.DeleteRM(Reply);

                    dvgRM.Columns.Remove("Dimg");
                    dvgRM.Columns.Remove("Eimg");
                    SearchCriteria searchCriteria = new SearchCriteria();
                    BuildDefaultSearch(searchCriteria);
                    LoadItem(searchCriteria);
                    foreach (DataGridViewRow row in dvgRM.Rows)
                    {
                        row.Height = 25;
                    }
                }
            }

            if (dvgRM.Columns[e.ColumnIndex].Name == "Eimg")
            {
                Inventory.AddEditRM rmg = new Inventory.AddEditRM(Convert.ToInt32(dvgRM.CurrentRow.Cells[0].Value));
                this.Hide();

                if (rmg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    this.Hide();
                }
                else
                {
                    this.Show();
                    dvgRM.Columns.Remove("Dimg");
                    dvgRM.Columns.Remove("Eimg");
                    SearchCriteria searchCriteria = new SearchCriteria();
                    BuildDefaultSearch(searchCriteria);
                    LoadItem(searchCriteria);
                    foreach (DataGridViewRow row in dvgRM.Rows)
                    {
                        row.Height = 25;
                    }
                }
            }
        }


        private void ddlGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckFilterCondition();
        }

        private void txtItemCode_TextChanged(object sender, EventArgs e)
        {
            CheckFilterCondition();
        }

        private void txtItemName_TextChanged(object sender, EventArgs e)
        {
            CheckFilterCondition();
        }

        private void picMoveFirst_Click(object sender, EventArgs e)
        {
            if (CurrentPageIndex != 0)
            {
                CurrentPageIndex = 0;
                ChangePageIndex();
            }
        }

        private void picMovePrev_Click(object sender, EventArgs e)
        {
            if (CurrentPageIndex != 0)
            {
                CurrentPageIndex--;
                ChangePageIndex();
            }
        }

        private void picMoveNext_Click(object sender, EventArgs e)
        {
            if (CurrentPageIndex + 1 < TotalPage)
            {
                CurrentPageIndex++;
                ChangePageIndex();
            }
        }

        private void picMoveLast_Click(object sender, EventArgs e)
        {
            if (CurrentPageIndex + 1 < TotalPage)
            {
                CurrentPageIndex = TotalPage - 1;
                ChangePageIndex();
            }
        }

        private void lblExit_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void lblExit_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnAddItem_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnAddItem_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }
        #endregion

        #region Private method
        private void ChangePageIndex()
        {
            DataView view1;
            view1 = new DataView(GeneralFunctions.GetCurrentRecords(CurrentPageIndex, dsPage.Tables[0], PgSize, "RMID"));
            source1.DataSource = view1;
            int pg = CurrentPageIndex + 1;
            lblPageNo.Text = pg.ToString() + " / " + TotalPage.ToString();
            foreach (DataGridViewRow row in dvgRM.Rows)
            {
                row.Height = 25;
            }
        }

        private void LoadItem(SearchCriteria searchCriteria)
        {
            string path = Application.StartupPath + "\\";
            DataView view1;

            DataGridViewCellStyle style = new DataGridViewCellStyle();
            dvgRM.EnableHeadersVisualStyles = false;
            style.ForeColor = Color.Blue;

            dvgRM.AutoGenerateColumns = false;

            dvgRM.Columns[0].Name = "RMID";
            //dvgRM.Columns[1].Name = "RM Code";
            dvgRM.Columns[1].Name = "RM Description";
            dvgRM.Columns[2].Name = "RM Group";
            dvgRM.Columns[3].Name = "Unit";
            dvgRM.Columns[4].Name = "Rate/Unit";
            dvgRM.Columns[5].Name = "Balance";
            dvgRM.Columns[6].Name = "Stock Value";

            //dvgRM.Columns[1].HeaderText = "RM Code";
            dvgRM.Columns[1].HeaderText = "RM Description";
            dvgRM.Columns[2].HeaderText = "RM Group";
            dvgRM.Columns[3].HeaderText = "Unit";
            dvgRM.Columns[4].HeaderText = "Rate/Unit";
            dvgRM.Columns[5].HeaderText = "Balance";
            dvgRM.Columns[6].HeaderText = "Stock Value";

            dvgRM.Columns[0].DataPropertyName = "RMID";
            //dvgRM.Columns[1].DataPropertyName = "ItemCode";
            dvgRM.Columns[1].DataPropertyName = "ItemName";
            dvgRM.Columns[2].DataPropertyName = "GroupName";
            dvgRM.Columns[3].DataPropertyName = "UOM";
            dvgRM.Columns[4].DataPropertyName = "CurrentRate";
            dvgRM.Columns[5].DataPropertyName = "BalanceQty";
            dvgRM.Columns[6].DataPropertyName = "StockValue";

            ////dvgRM.Columns[1].Width = 100;
            dvgRM.Columns[1].Width = 250;
            dvgRM.Columns[2].Width = 200;
            dvgRM.Columns[3].Width = 50;
            dvgRM.Columns[4].Width = 80;
            dvgRM.Columns[5].Width = 80;
            dvgRM.Columns[6].Width = 80;
            //dvgRM.Columns[7].Width = 80;

            dvgRM.Columns[1].SortMode = DataGridViewColumnSortMode.Automatic;
            dvgRM.Columns[2].SortMode = DataGridViewColumnSortMode.Automatic;
            dvgRM.Columns[3].SortMode = DataGridViewColumnSortMode.Automatic;
            //dvgRM.Columns[5].SortMode = DataGridViewColumnSortMode.NotSortable;
            dvgRM.Columns[6].SortMode = DataGridViewColumnSortMode.NotSortable;

            dvgRM.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dvgRM.Columns[5].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;

            dvgRM.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dvgRM.Columns[6].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;

            dvgRM.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dvgRM.Columns[4].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;

            dvgRM.Columns[4].DefaultCellStyle.Format = "N2";
            dvgRM.Columns[5].DefaultCellStyle.Format = "N3";
            dvgRM.Columns[6].DefaultCellStyle.Format = "N2";

            dvgRM.Columns[0].Visible = false;
            //dvgRM.Columns[9].Visible = false;

            DataGridViewImageColumn Editimg = new DataGridViewImageColumn();
            Image Editimage = Image.FromFile(path + ConfigurationManager.AppSettings["imageFolder"] + "edit-icon.png");
            Editimg.Image = Editimage;
            dvgRM.Columns.Add(Editimg);
            Editimg.HeaderText = "Edit";
            Editimg.Name = "Eimg";
            Editimg.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            Editimg.Width = 40;

            DataGridViewImageColumn Delimg = new DataGridViewImageColumn();
            Image Delimage = Image.FromFile(path + ConfigurationManager.AppSettings["imageFolder"] + "delete-icon.png");
            Delimg.Image = Delimage;
            dvgRM.Columns.Add(Delimg);
            Delimg.HeaderText = "Del";
            Delimg.Name = "Dimg";
            Delimg.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            Delimg.Width = 40;

            dvgRM.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dvgRM.MultiSelect = false;

            InventoryBLL invtbll = new InventoryBLL();
            dsPage = invtbll.GetAllRM(searchCriteria);
            if (dsPage.Tables[0].Rows.Count == 0)
                view1 = null;
            else
            {
                TotalPage = GeneralFunctions.CalculateTotalPages(dsPage.Tables[0], PgSize);
                view1 = new DataView(GeneralFunctions.GetCurrentRecords(0, dsPage.Tables[0], PgSize, "RMID"));
                CurrentPageIndex = 0;
                lblPageNo.Text = 1.ToString() + " / " + TotalPage.ToString();

            }
            source1.DataSource = view1;

            dvgRM.DataSource = source1;
            dvgRM.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            dvgRM.AllowUserToResizeColumns = false;
            dvgRM.AllowUserToAddRows = false;
            dvgRM.ReadOnly = true;
            dvgRM.Columns[1].HeaderCell.Style = style;
            dvgRM.Columns[2].HeaderCell.Style = style;
            dvgRM.Columns[3].HeaderCell.Style = style;

            //foreach (DataGridViewRow row in dvgRM.Rows)
            //{
            //    string RowType = row.Cells["RM"].Value.ToString();

            //    if (RowType == "1")
            //    {
            //        row.Cells["Dimg"].Value = new Bitmap(1, 1);
            //    }
            //}
        }

        private void BuildDefaultSearch(SearchCriteria criteria)
        {
            criteria.GroupName = "";
            criteria.LocName = ddlLoc.Text;
            criteria.BillType = ddlGroup.SelectedValue.ToString();
            criteria.StringOption1 = txtItemCode.Text;
        }

        private void BuildSearchCriteria(SearchCriteria criteria)
        {
            criteria.ItemName = txtItemName.Text;
            criteria.LocName = ddlLoc.Text;
            criteria.BillType = ddlGroup.SelectedValue.ToString();
            criteria.GroupName = ddlGroup.Text;
            criteria.StringOption1 = txtItemCode.Text;
        }

        private void LoadItemGroup()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            SearchCriteria searchcriteria = new SearchCriteria();
            searchcriteria.QueryStat = "L";
            searchcriteria.LocName = ddlLoc.Text;
            //searchcriteria.RestName = ddlRestName.Text;
            
            ddlGroup.DataSource = null;

            ds = new InventoryBLL().GetAllRMGroup(searchcriteria);
            dt = ds.Tables[0];

            DataRow nullrow = dt.NewRow();
            nullrow["GroupID"] = 0;
            nullrow["RMGroupdescr"] = "Select";
            dt.Rows.Add(nullrow);
            if (dt.Rows.Count > 0)
            {
                ddlGroup.DataSource = dt;
                ddlGroup.DisplayMember = "RMGroupdescr";
                ddlGroup.ValueMember = "GroupID";
                //ddlGroup.SelectedValue = "0";
            }
            ddlGroup.SelectedValue = "0";
            
        }

        private void CheckFilterCondition()
        {
            SearchCriteria searchCriteria = new SearchCriteria();
            BuildSearchCriteria(searchCriteria);
            LoadItem(searchCriteria);
            dvgRM.Columns.Remove("Dimg");
            dvgRM.Columns.Remove("Eimg");
            foreach (DataGridViewRow row in dvgRM.Rows)
            {
                row.Height = 25;
            }
        }

        private void SetWatermarkText()
        {

            WatermarkText.SetWatermark(txtItemName, "RM Item");
            WatermarkText.SetWatermark(txtItemCode, "RM Code");
        }

        private void LoadLoc()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            //populate Locations
            SearchCriteria searchcriteria = new SearchCriteria();
            searchcriteria.QueryStat = "L";

            ds = new LocationBLL().GetAllLoc(searchcriteria);
            dt = ds.Tables[0];

            //DataRow nullrow = dt.NewRow();
            //nullrow["pk_LocID"] = 0;
            //nullrow["LocationName"] = "All Locations";
            //dt.Rows.Add(nullrow);
            if (dt.Rows.Count > 0)
            {
                ddlLoc.DataSource = dt;
                ddlLoc.DisplayMember = "LocationName";
                ddlLoc.ValueMember = "pk_LocID";
                ddlLoc.SelectedIndex = 0;
            }
        }


        #endregion

       

    }
}
