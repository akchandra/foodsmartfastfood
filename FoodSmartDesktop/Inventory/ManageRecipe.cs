﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using FoodSmart.DAL;
using FoodSmart.BLL;
using FoodSmart.Entity;
using FoodSmart.GeneralUtilities;
using FoodSmart.Utilities;
using FoodSmart.Common;


namespace FoodSmartDesktop.Inventory
{
    public partial class ManageRecipe : Form
    {
        BindingSource source1 = new BindingSource();
        private int PgSize = 12;
        private int CurrentPageIndex = 1;
        private int TotalPage = 0;
        private DataSet dsPage;
        DataTable dt;

        public ManageRecipe()
        {
            InitializeComponent();
        }

        #region Public Method

        private void ManageRecipe_Load(object sender, EventArgs e)
        {
            dvgInvTran.ColumnCount = 7;

            SetWatermarkText();

            SearchCriteria searchCriteria = new SearchCriteria();
            BuildDefaultSearch(searchCriteria);

            LoadTran(searchCriteria);


            foreach (DataGridViewRow row in dvgInvTran.Rows)
            {
                row.Height = 25;
            }

            lbltoday.Text += " : " + GeneralFunctions.LoginInfo.CurrDate.ToString("dd-MMM-yyyy");
            lblUserName.Text += GeneralFunctions.LoginInfo.UserFullName;
            txtItem.Focus();
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAddItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Inventory.AddEditRecipe rmg = new Inventory.AddEditRecipe(0);

            if (rmg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {

            }
            else
            {
                this.Show();
                //dvgInvTran.Columns.Remove("Dimg");
                dvgInvTran.Columns.Remove("Eimg");
                dvgInvTran.Columns.Remove("Dimg");
                SearchCriteria searchCriteria = new SearchCriteria();
                BuildDefaultSearch(searchCriteria);
                LoadTran(searchCriteria);
                foreach (DataGridViewRow row in dvgInvTran.Rows)
                {
                    row.Height = 25;
                }
            }
        }

        private void dvgInvTran_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int Reply;
            int i;
            if (dvgInvTran.RowCount == 0)
                return;
            else
                i = dvgInvTran.CurrentRow.Index;

            if (dvgInvTran.Columns[e.ColumnIndex].Name == "Dimg")
            {
                Reply = Convert.ToInt32(MessageBox.Show("Want to Delete this Record?", "Delete Record", MessageBoxButtons.OKCancel, MessageBoxIcon.Question));
                if (Reply == 1)
                {
                    Reply = Convert.ToInt32(dvgInvTran.CurrentRow.Cells[0].Value);

                    InventoryBLL invbll = new InventoryBLL();
                    invbll.DeleteRecipe(Reply);

                    dvgInvTran.Columns.Remove("Dimg");
                    dvgInvTran.Columns.Remove("Eimg");
                    SearchCriteria searchCriteria = new SearchCriteria();
                    BuildDefaultSearch(searchCriteria);
                    LoadTran(searchCriteria);
                    foreach (DataGridViewRow row in dvgInvTran.Rows)
                    {
                        row.Height = 25;
                    }
                }
            }

            if (dvgInvTran.Columns[e.ColumnIndex].Name == "Eimg")
            {
                Inventory.AddEditRecipe rmg = new Inventory.AddEditRecipe(Convert.ToInt32(dvgInvTran.CurrentRow.Cells[0].Value));
                this.Hide();

                if (rmg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    this.Hide();
                }
                else
                {
                    this.Show();
                    dvgInvTran.Columns.Remove("Dimg");
                    dvgInvTran.Columns.Remove("Eimg");
                    SearchCriteria searchCriteria = new SearchCriteria();
                    BuildDefaultSearch(searchCriteria);
                    LoadTran(searchCriteria);
                    foreach (DataGridViewRow row in dvgInvTran.Rows)
                    {
                        row.Height = 25;
                    }
                }
            }
        }

        private void txtTranNo_TextChanged(object sender, EventArgs e)
        {
            CheckFilterCondition();
        }

        private void picMoveFirst_Click(object sender, EventArgs e)
        {
            if (CurrentPageIndex != 0)
            {
                CurrentPageIndex = 0;
                ChangePageIndex();
            }
        }

        private void picMovePrev_Click(object sender, EventArgs e)
        {
            if (CurrentPageIndex != 0)
            {
                CurrentPageIndex--;
                ChangePageIndex();
            }
        }

        private void picMoveNext_Click(object sender, EventArgs e)
        {
            if (CurrentPageIndex + 1 < TotalPage)
            {
                CurrentPageIndex++;
                ChangePageIndex();
            }
        }

        private void picMoveLast_Click(object sender, EventArgs e)
        {
            if (CurrentPageIndex + 1 < TotalPage)
            {
                CurrentPageIndex = TotalPage - 1;
                ChangePageIndex();
            }
        }

        private void lblExit_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void lblExit_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnAddItem_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnAddItem_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        #endregion

        #region Private method
        private void ChangePageIndex()
        {
            DataView view1;
            view1 = new DataView(GeneralFunctions.GetCurrentRecords(CurrentPageIndex, dsPage.Tables[0], PgSize, "pk_RecipeID"));
            source1.DataSource = view1;
            int pg = CurrentPageIndex + 1;
            lblPageNo.Text = pg.ToString() + " / " + TotalPage.ToString();
            foreach (DataGridViewRow row in dvgInvTran.Rows)
            {
                row.Height = 25;
            }
        }

        private void LoadTran(SearchCriteria searchCriteria)
        {
            string path = Application.StartupPath + "\\";
            DataView view1;

            DataGridViewCellStyle style = new DataGridViewCellStyle();
            dvgInvTran.EnableHeadersVisualStyles = false;
            style.ForeColor = Color.Blue;

            dvgInvTran.AutoGenerateColumns = false;

            dvgInvTran.Columns[0].Name = "pk_RecipeID";
            dvgInvTran.Columns[1].Name = "ItemType";
            dvgInvTran.Columns[2].Name = "RecipeName";
            dvgInvTran.Columns[3].Name = "UOM";
            dvgInvTran.Columns[4].Name = "DateIntroduced";
            dvgInvTran.Columns[5].Name = "noofPlates";
            dvgInvTran.Columns[6].Name = "TotalQty";

            dvgInvTran.Columns[0].HeaderText = "pk_RecipeID";
            dvgInvTran.Columns[1].HeaderText = "Type";
            dvgInvTran.Columns[2].HeaderText = "Receipe Name";
            dvgInvTran.Columns[3].HeaderText = "Unit";
            dvgInvTran.Columns[4].HeaderText = "Introduced On";
            dvgInvTran.Columns[5].HeaderText = "No of Plates";
            dvgInvTran.Columns[6].HeaderText = "Quantity";

            dvgInvTran.Columns[0].DataPropertyName = "pk_RecipeID";
            dvgInvTran.Columns[1].DataPropertyName = "ItemType";
            dvgInvTran.Columns[2].DataPropertyName = "RecipeName";
            dvgInvTran.Columns[3].DataPropertyName = "UOM";
            dvgInvTran.Columns[4].DataPropertyName = "DateIntroduced";
            dvgInvTran.Columns[5].DataPropertyName = "noofPlates";
            dvgInvTran.Columns[6].DataPropertyName = "TotalQty";

            dvgInvTran.Columns[1].Width = 100;
            dvgInvTran.Columns[2].Width = 350;
            dvgInvTran.Columns[3].Width = 90;
            dvgInvTran.Columns[4].Width = 100;
            dvgInvTran.Columns[5].Width = 100;
            dvgInvTran.Columns[6].Width = 100;

            dvgInvTran.Columns[1].SortMode = DataGridViewColumnSortMode.Automatic;
            dvgInvTran.Columns[2].SortMode = DataGridViewColumnSortMode.Automatic;
            dvgInvTran.Columns[3].SortMode = DataGridViewColumnSortMode.Automatic;
            dvgInvTran.Columns[4].SortMode = DataGridViewColumnSortMode.Automatic;
            //dvgInvTran.Columns[5].SortMode = DataGridViewColumnSortMode.Automatic;

            dvgInvTran.Columns[0].Visible = false;

            DataGridViewImageColumn Editimg = new DataGridViewImageColumn();
            Image Editimage = Image.FromFile(path + ConfigurationManager.AppSettings["imageFolder"] + "edit-icon.png");
            Editimg.Image = Editimage;
            dvgInvTran.Columns.Add(Editimg);
            Editimg.HeaderText = "Edit";
            Editimg.Name = "Eimg";
            Editimg.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            Editimg.Width = 40;

            DataGridViewImageColumn Delimg = new DataGridViewImageColumn();
            Image Delimage = Image.FromFile(path + ConfigurationManager.AppSettings["imageFolder"] + "delete-icon.png");
            Delimg.Image = Delimage;
            dvgInvTran.Columns.Add(Delimg);
            Delimg.HeaderText = "Del";
            Delimg.Name = "Dimg";
            Delimg.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            Delimg.Width = 40;

            dvgInvTran.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dvgInvTran.MultiSelect = false;

            InventoryBLL invtbll = new InventoryBLL();
            dsPage = invtbll.GetAllRecipe(searchCriteria);
            if (dsPage.Tables[0].Rows.Count == 0)
                view1 = null;
            else
            {
                TotalPage = GeneralFunctions.CalculateTotalPages(dsPage.Tables[0], PgSize);
                view1 = new DataView(GeneralFunctions.GetCurrentRecords(0, dsPage.Tables[0], PgSize, "pk_RecipeID"));
                CurrentPageIndex = 0;
                lblPageNo.Text = 1.ToString() + " / " + TotalPage.ToString();
            }
            source1.DataSource = view1;

            dvgInvTran.DataSource = source1;
            dvgInvTran.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            dvgInvTran.AllowUserToResizeColumns = false;
            dvgInvTran.AllowUserToAddRows = false;
            dvgInvTran.ReadOnly = true;
            dvgInvTran.Columns[1].HeaderCell.Style = style;
            dvgInvTran.Columns[2].HeaderCell.Style = style;
            dvgInvTran.Columns[3].HeaderCell.Style = style;
            dvgInvTran.Columns[4].HeaderCell.Style = style;


            dvgInvTran.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dvgInvTran.Columns[5].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;

            dvgInvTran.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dvgInvTran.Columns[6].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;

        }

        private void BuildDefaultSearch(SearchCriteria criteria)
        {
            criteria.ItemName = txtItem.Text;
        }

        private void BuildSearchCriteria(SearchCriteria criteria)
        {
            criteria.ItemName = txtItem.Text;
        }

        private void CheckFilterCondition()
        {
            SearchCriteria searchCriteria = new SearchCriteria();
            BuildSearchCriteria(searchCriteria);
            LoadTran(searchCriteria);
            dvgInvTran.Columns.Remove("Dimg");
            dvgInvTran.Columns.Remove("Eimg");
            foreach (DataGridViewRow row in dvgInvTran.Rows)
            {
                row.Height = 25;
            }
        }

        private void SetWatermarkText()
        {

            WatermarkText.SetWatermark(txtItem, "Recipe Item");
            //WatermarkText.SetWatermark(txtItemCode, "RM Code");
        }

        #endregion
    }
}
