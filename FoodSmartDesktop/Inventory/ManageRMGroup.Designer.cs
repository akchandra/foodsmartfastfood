﻿namespace FoodSmartDesktop.Inventory
{
    partial class ManageRMGroup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ManageRMGroup));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.lblUserName = new System.Windows.Forms.Label();
            this.lblExit = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ddlLoc = new System.Windows.Forms.ComboBox();
            this.ddlGroupType = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.txtGroupName = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblPageNo = new System.Windows.Forms.Label();
            this.picMoveFirst = new System.Windows.Forms.PictureBox();
            this.picMovePrev = new System.Windows.Forms.PictureBox();
            this.picMoveLast = new System.Windows.Forms.PictureBox();
            this.picMoveNext = new System.Windows.Forms.PictureBox();
            this.dvgRMGroup = new System.Windows.Forms.DataGridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lbltoday = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnAddRMGroup = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picMoveFirst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMovePrev)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMoveLast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMoveNext)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dvgRMGroup)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(199)))), ((int)(((byte)(233)))));
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.lblUserName);
            this.panel1.Controls.Add(this.lblExit);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(10, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(846, 60);
            this.panel1.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(288, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(275, 23);
            this.label2.TabIndex = 26;
            this.label2.Text = "Manage Raw Material Group";
            // 
            // lblUserName
            // 
            this.lblUserName.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblUserName.Location = new System.Drawing.Point(578, 20);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(185, 27);
            this.lblUserName.TabIndex = 19;
            this.lblUserName.Text = "Welcome ";
            // 
            // lblExit
            // 
            this.lblExit.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExit.ForeColor = System.Drawing.Color.DarkRed;
            this.lblExit.Image = ((System.Drawing.Image)(resources.GetObject("lblExit.Image")));
            this.lblExit.Location = new System.Drawing.Point(773, 2);
            this.lblExit.Name = "lblExit";
            this.lblExit.Size = new System.Drawing.Size(73, 58);
            this.lblExit.TabIndex = 1;
            this.lblExit.Click += new System.EventHandler(this.lblExit_Click);
            this.lblExit.MouseLeave += new System.EventHandler(this.lblExit_MouseLeave);
            this.lblExit.MouseHover += new System.EventHandler(this.lblExit_MouseHover);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Monotype Corsiva", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Image = ((System.Drawing.Image)(resources.GetObject("label1.Image")));
            this.label1.Location = new System.Drawing.Point(3, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(288, 34);
            this.label1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox1.Controls.Add(this.ddlLoc);
            this.groupBox1.Controls.Add(this.ddlGroupType);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.txtGroupName);
            this.groupBox1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox1.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBox1.Location = new System.Drawing.Point(1, 68);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Size = new System.Drawing.Size(855, 49);
            this.groupBox1.TabIndex = 27;
            this.groupBox1.TabStop = false;
            // 
            // ddlLoc
            // 
            this.ddlLoc.FormattingEnabled = true;
            this.ddlLoc.Location = new System.Drawing.Point(578, 15);
            this.ddlLoc.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ddlLoc.Name = "ddlLoc";
            this.ddlLoc.Size = new System.Drawing.Size(192, 28);
            this.ddlLoc.TabIndex = 21;
            this.ddlLoc.Visible = false;
            // 
            // ddlGroupType
            // 
            this.ddlGroupType.FormattingEnabled = true;
            this.ddlGroupType.Location = new System.Drawing.Point(380, 14);
            this.ddlGroupType.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ddlGroupType.Name = "ddlGroupType";
            this.ddlGroupType.Size = new System.Drawing.Size(192, 28);
            this.ddlGroupType.TabIndex = 20;
            this.ddlGroupType.SelectedIndexChanged += new System.EventHandler(this.ddlGroupType_SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button1.BackgroundImage")));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.Control;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(805, 7);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(50, 41);
            this.button1.TabIndex = 18;
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = false;
            // 
            // txtGroupName
            // 
            this.txtGroupName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGroupName.Location = new System.Drawing.Point(9, 16);
            this.txtGroupName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtGroupName.Name = "txtGroupName";
            this.txtGroupName.Size = new System.Drawing.Size(365, 26);
            this.txtGroupName.TabIndex = 0;
            this.txtGroupName.TextChanged += new System.EventHandler(this.txtItemName_TextChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblPageNo);
            this.groupBox2.Controls.Add(this.picMoveFirst);
            this.groupBox2.Controls.Add(this.picMovePrev);
            this.groupBox2.Controls.Add(this.picMoveLast);
            this.groupBox2.Controls.Add(this.picMoveNext);
            this.groupBox2.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(8, 121);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox2.Size = new System.Drawing.Size(282, 51);
            this.groupBox2.TabIndex = 28;
            this.groupBox2.TabStop = false;
            // 
            // lblPageNo
            // 
            this.lblPageNo.AutoSize = true;
            this.lblPageNo.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPageNo.Location = new System.Drawing.Point(86, 18);
            this.lblPageNo.Name = "lblPageNo";
            this.lblPageNo.Size = new System.Drawing.Size(102, 20);
            this.lblPageNo.TabIndex = 17;
            this.lblPageNo.Text = "Current Page : ";
            // 
            // picMoveFirst
            // 
            this.picMoveFirst.Image = ((System.Drawing.Image)(resources.GetObject("picMoveFirst.Image")));
            this.picMoveFirst.Location = new System.Drawing.Point(19, 18);
            this.picMoveFirst.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.picMoveFirst.Name = "picMoveFirst";
            this.picMoveFirst.Size = new System.Drawing.Size(23, 21);
            this.picMoveFirst.TabIndex = 13;
            this.picMoveFirst.TabStop = false;
            this.picMoveFirst.Click += new System.EventHandler(this.picMoveFirst_Click);
            // 
            // picMovePrev
            // 
            this.picMovePrev.Image = ((System.Drawing.Image)(resources.GetObject("picMovePrev.Image")));
            this.picMovePrev.Location = new System.Drawing.Point(49, 18);
            this.picMovePrev.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.picMovePrev.Name = "picMovePrev";
            this.picMovePrev.Size = new System.Drawing.Size(23, 21);
            this.picMovePrev.TabIndex = 14;
            this.picMovePrev.TabStop = false;
            this.picMovePrev.Click += new System.EventHandler(this.picMovePrev_Click);
            // 
            // picMoveLast
            // 
            this.picMoveLast.Image = ((System.Drawing.Image)(resources.GetObject("picMoveLast.Image")));
            this.picMoveLast.Location = new System.Drawing.Point(250, 18);
            this.picMoveLast.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.picMoveLast.Name = "picMoveLast";
            this.picMoveLast.Size = new System.Drawing.Size(23, 21);
            this.picMoveLast.TabIndex = 16;
            this.picMoveLast.TabStop = false;
            this.picMoveLast.Click += new System.EventHandler(this.picMoveLast_Click);
            // 
            // picMoveNext
            // 
            this.picMoveNext.Image = ((System.Drawing.Image)(resources.GetObject("picMoveNext.Image")));
            this.picMoveNext.Location = new System.Drawing.Point(219, 18);
            this.picMoveNext.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.picMoveNext.Name = "picMoveNext";
            this.picMoveNext.Size = new System.Drawing.Size(23, 21);
            this.picMoveNext.TabIndex = 15;
            this.picMoveNext.TabStop = false;
            this.picMoveNext.Click += new System.EventHandler(this.picMoveNext_Click);
            // 
            // dvgRMGroup
            // 
            this.dvgRMGroup.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dvgRMGroup.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dvgRMGroup.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dvgRMGroup.DefaultCellStyle = dataGridViewCellStyle4;
            this.dvgRMGroup.Location = new System.Drawing.Point(8, 176);
            this.dvgRMGroup.Name = "dvgRMGroup";
            this.dvgRMGroup.RowHeadersVisible = false;
            this.dvgRMGroup.Size = new System.Drawing.Size(848, 329);
            this.dvgRMGroup.TabIndex = 32;
            this.dvgRMGroup.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dvgRMGroup_CellContentClick);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Black;
            this.panel2.Controls.Add(this.lbltoday);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Location = new System.Drawing.Point(1, 512);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(855, 41);
            this.panel2.TabIndex = 33;
            // 
            // lbltoday
            // 
            this.lbltoday.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltoday.ForeColor = System.Drawing.Color.Transparent;
            this.lbltoday.Location = new System.Drawing.Point(17, 9);
            this.lbltoday.Name = "lbltoday";
            this.lbltoday.Size = new System.Drawing.Size(185, 27);
            this.lbltoday.TabIndex = 20;
            this.lbltoday.Text = "Today";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Snow;
            this.label3.Location = new System.Drawing.Point(736, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 16);
            this.label3.TabIndex = 1;
            this.label3.Text = "Powered By Europia";
            // 
            // btnAddRMGroup
            // 
            this.btnAddRMGroup.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAddRMGroup.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAddRMGroup.BackgroundImage")));
            this.btnAddRMGroup.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAddRMGroup.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddRMGroup.ForeColor = System.Drawing.SystemColors.Control;
            this.btnAddRMGroup.Image = ((System.Drawing.Image)(resources.GetObject("btnAddRMGroup.Image")));
            this.btnAddRMGroup.Location = new System.Drawing.Point(702, 125);
            this.btnAddRMGroup.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnAddRMGroup.Name = "btnAddRMGroup";
            this.btnAddRMGroup.Size = new System.Drawing.Size(154, 50);
            this.btnAddRMGroup.TabIndex = 29;
            this.btnAddRMGroup.Text = "Add New Group";
            this.btnAddRMGroup.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAddRMGroup.UseVisualStyleBackColor = false;
            this.btnAddRMGroup.Click += new System.EventHandler(this.btnAddRMGroup_Click);
            this.btnAddRMGroup.MouseLeave += new System.EventHandler(this.btnAddRMGroup_MouseLeave);
            this.btnAddRMGroup.MouseHover += new System.EventHandler(this.btnAddRMGroup_MouseHover);
            // 
            // ManageRMGroup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(863, 554);
            this.ControlBox = false;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.dvgRMGroup);
            this.Controls.Add(this.btnAddRMGroup);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ManageRMGroup";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.ManageRMGroup_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picMoveFirst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMovePrev)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMoveLast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMoveNext)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dvgRMGroup)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Label lblExit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        protected internal System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtGroupName;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblPageNo;
        private System.Windows.Forms.PictureBox picMoveFirst;
        private System.Windows.Forms.PictureBox picMovePrev;
        private System.Windows.Forms.PictureBox picMoveLast;
        private System.Windows.Forms.PictureBox picMoveNext;
        protected internal System.Windows.Forms.Button btnAddRMGroup;
        private System.Windows.Forms.DataGridView dvgRMGroup;
        private System.Windows.Forms.ComboBox ddlGroupType;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lbltoday;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox ddlLoc;
    }
}