﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FoodSmart.BLL;
using FoodSmart.Common;
using System.Configuration;
using System.IO;
using System.Drawing.Drawing2D;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using FoodSmart.Utilities.Cryptography;

namespace FoodSmartDesktop.Inventory
{
    public partial class AddEditInvTran : Form
    {
        string stat;
        int LID;
        int ItemTranID;
        BindingSource source1 = new BindingSource();
        private DataSet dsrm;
        string keyAmt;

        public AddEditInvTran(int ID)
        {
            InitializeComponent();
            if (ID == 0)
            {
                stat = "A";
                LID = 0;
            }
            else
            {
                stat = "E";

                LID = ID;
            }
        }

        #region public method
        private void AddEditInvTran_Load(object sender, EventArgs e)
        {
            dvgItemTran.ColumnCount = 9;
            txtTranNo.MaxLength = 50;
            txtNarration.MaxLength = 300;
            txtVendorRef.MaxLength = 50;
            tableLayoutPanel3.Enabled = false;

            this.ddlType.SelectedIndexChanged -= new System.EventHandler(this.ddlType_SelectedIndexChanged);
            //this.ddlVendor.SelectedIndexChanged -= new System.EventHandler(this.ddlVendor_SelectedIndexChanged);
            //this.ddlCC.SelectedIndexChanged -= new System.EventHandler(this.ddlCC_SelectedIndexChanged);

            txtTranNo.CharacterCasing = CharacterCasing.Upper;
            txtNarration.CharacterCasing = CharacterCasing.Upper;
            txtVendorRef.CharacterCasing = CharacterCasing.Upper;
            LoadDDL();
            //LoadVendor();
            //LoadCC();
            LoadTran();
            LoadRM();

            if (LID != 0)
                fillItem(LID);
           
            txtTranNo.Focus();
            this.ddlType.SelectedIndexChanged += new System.EventHandler(this.ddlType_SelectedIndexChanged);
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (ValidateEntry())
            {
                IInvTran rmItem = new InvTranEntity();
                rmItem.TranID = LID;
                rmItem.VendorId = ddlVendor.SelectedValue.ToInt();
                rmItem.TranDate = dtTranDate.Value;
                rmItem.TranNo = txtTranNo.Text;
                rmItem.TranType = ddlType.SelectedValue.ToString();
                rmItem.Narration = txtNarration.Text;
                rmItem.RefDate = dtRefDate.Value;
                rmItem.RefDoc = txtVendorRef.Text;

                rmItem.CreatedBy = GeneralFunctions.LoginInfo.UserID;

                int Status = new InventoryBLL().SaveInvTran(rmItem, stat, dsrm);

                if (Status == 0)
                {
                    MessageBox.Show("Error!! Record Not Saved", "Save Record", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    if (stat == "A")
                    {
                        Initialize();
                        txtTranNo.Focus();
                    }
                    else
                    {
                        this.Close();
                    }
                }
            }
        }

        private void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlType.SelectedIndex == -1)
            {
                tableLayoutPanel3.Enabled = false;
            }
            else
            {
                tableLayoutPanel3.Enabled = true;
                if (ddlType.SelectedValue.ToString() == "R" || ddlType.SelectedValue.ToString() == "X")
                {
                    lbltrnFrom.Text = "From Location";
                    LoadFromLocation(GeneralFunctions.LoginInfo.LocID);
                    LoadToLocation(0);
                    ddlVendor.SelectedValue = "0";
                    ddlVendor.Enabled = false;
                    ddlVendor.Enabled = true;
                    txtRate.ReadOnly = false;


                }
                else if (ddlType.SelectedValue.ToString() == "I" || ddlType.SelectedValue.ToString() == "Y")
                {
                    LoadFromLocation(0);
                    LoadToLocation(GeneralFunctions.LoginInfo.LocID);
                    ddlVendor.SelectedValue = "0";
                    ddlVendor.Enabled = false;
                    ddlToLoc.Enabled = true;
                    txtRate.ReadOnly = true;
                }
                else
                {
                    ddlVendor.SelectedValue = "0";
                    ddlVendor.Enabled = false;
                    ddlToLoc.SelectedValue = "0";
                    ddlToLoc.Enabled = false;
                }
            }
        }

        private void btnAddRow_Click(object sender, EventArgs e)
        {
            ddlType.Enabled = false;
            if (ddlItem.SelectedValue.ToInt() == 0)
            {
                ddlItem.BackColor = Color.Red;
                return;
            }

            if ((string.IsNullOrEmpty(txtRate.Text) || txtRate.Text.ToDecimal() == 0) && (ddlType.SelectedValue.ToString() == "R" || ddlType.SelectedValue.ToString() == "X"))
            {
                txtRate.BackColor = Color.Red;
                return;
            }

            if (string.IsNullOrEmpty(txtQty.Text) || txtQty.Text.ToDecimal() == 0)
            {
                txtQty.BackColor = Color.Red;
                return;
            }

            DataRow[] drCOLL = dsrm.Tables[0].Select("fk_RMID = " + ddlItem.SelectedValue);
            DataRow dr;

            if (drCOLL.Length == 0)
            {
                dr = dsrm.Tables[0].NewRow();
                dr["fk_RMID"] = ddlItem.SelectedValue.ToInt();
                dr["RMName"] = ddlItem.Text;
                dr["Qty"] = txtQty.Text;
                dr["Rate"] = txtRate.Text;
                dr["Taxes"] = txtTax.Text;
                dr["TotalAmt"] = (dr["Qty"].ToDecimal() * dr["Rate"].ToDecimal()) + dr["Taxes"].ToDecimal();
            }
            else
            {
                dr = drCOLL[0];
                dr["fk_RMID"] = ddlItem.SelectedValue.ToInt();
                dr["RMName"] = ddlItem.Text;
                dr["Qty"] = txtQty.Text;
                dr["Rate"] = txtRate.Text;
                dr["Taxes"] = txtTax.Text;
                //dr["TotalAmt"] = (dr["Qty"].ToDecimal() * dr["Rate"].ToDecimal());
                dr["TotalAmt"] = (dr["Qty"].ToDecimal() * dr["Rate"].ToDecimal()) + dr["Taxes"].ToDecimal();
            }

            if (drCOLL.Length == 0)
            {
                dsrm.Tables[0].Rows.Add(dr);
            }

            dvgItemTran.BackgroundColor = Color.White;
           
            dsrm.Tables[0].AcceptChanges();
            InitializeItems();
            CalculteGrandTotal(dsrm.Tables[0]);
            ddlItem.Focus();
        }

        private void lblExit_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void lblExit_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnSave_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnSave_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void txtRate_TextChanged(object sender, EventArgs e)
        {
            if (txtRate.Text.ToDecimal() > 0)
                txtRate.BackColor = SystemColors.Window;
            CalculateTotal();
        }
        private void txtQty_TextChanged(object sender, EventArgs e)
        {
            if (txtQty.Text.ToDecimal() > 0)
                txtQty.BackColor = SystemColors.Window;
            CalculateTotal();
        }
        private void txtTax_TextChanged(object sender, EventArgs e)
        {
            CalculateTotal();
        }


        private void dvgItemTran_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int Reply;
            int i;
            if (dvgItemTran.RowCount == 0)
                return;
            else
                i = dvgItemTran.CurrentRow.Index;

            if (dvgItemTran.Columns[e.ColumnIndex].Name == "Dimg")
            {
                Reply = Convert.ToInt32(MessageBox.Show("Want to Delete this Record?", "Delete Record", MessageBoxButtons.OKCancel, MessageBoxIcon.Question));
                if (Reply == 1)
                {
                    Reply = Convert.ToInt32(dvgItemTran.CurrentRow.Cells[3].Value);

                    DataRow[] drCOLL = dsrm.Tables[0].Select("fk_RMID = " + Reply);
                    DataRow dr;
                    dr = drCOLL[0];

                    if (drCOLL.Length != 0)
                    {
                        dsrm.Tables[0].Rows.Remove(dr);
                    }
                    dsrm.Tables[0].AcceptChanges();
                    dvgItemTran.Columns.Remove("Dimg");
                    dvgItemTran.Columns.Remove("Eimg");

                    foreach (DataGridViewRow row in dvgItemTran.Rows)
                    {
                        row.Height = 25;
                    }
                    return;
                }
            }

            if (dvgItemTran.Columns[e.ColumnIndex].Name == "Eimg")
            {
                ItemTranID = dvgItemTran.CurrentRow.Cells[0].Value.ToInt();
                txtQty.Text = dvgItemTran.CurrentRow.Cells[5].Value.ToString();
                txtRate.Text = dvgItemTran.CurrentRow.Cells[6].Value.ToString();
                ddlItem.SelectedValue = dvgItemTran.CurrentRow.Cells[3].Value.ToInt();
                txtTotal.Text = dvgItemTran.CurrentRow.Cells[7].Value.ToString();
                ddlItem.Enabled = false;
            }
        }

        private void ddlItem_KeyUp(object sender, KeyEventArgs e)
        {
            ddlItem.DroppedDown = false;
        }

        private void ddlItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlItem.SelectedValue.ToInt() != 0)
            {
                // need to be replace
                
                //ddlItem.BackColor = Color.White;
                //InventoryBLL oinvbll = new InventoryBLL();
                //InventoryEntity oinv = (InventoryEntity)oinvbll.GetEdit(ddlItem.SelectedValue.ToInt(), GeneralFunctions.LoginInfo.UserID);
                //txtRate.Text = oinv.RMRate.ToString();
                //txtQty.Text = "0";
                //txtTax.Text = "0";
                //txtQty.Focus();
            }
        }

        private void txtQty_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtRate_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }


        private void txtTax_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        #endregion

        #region "Private Methods"

        private void Initialize()
        {
            txtTranNo.Text = string.Empty;
            txtNarration.Text = string.Empty;
            txtVendorRef.Text = string.Empty;
            dtRefDate.Checked = false;
            dtTranDate.Value = DateTime.Now;
            ddlType.SelectedIndex = 0;
            ddlToLoc.SelectedValue = "0";
            ddlVendor.SelectedValue = "0";
            ddlType.Enabled = true;

            ddlItem.SelectedValue = "0";
            txtRate.Text = "";
            txtQty.Text = "";
            txtTotal.Text = "";
            LoadTran();
        }

        private bool ValidateEntry()
        {
            bool RetVal = true;

            if (dtTranDate.Value.Date > DateTime.Today)
            {
                RetVal = false;
            }
            if ((ddlType.SelectedValue.ToString() == "R" || ddlType.SelectedValue.ToString() == "X") && ddlVendor.SelectedValue.ToInt() == 0)
            {
                ddlVendor.BackColor = Color.Red;
                RetVal = false;
            }
            else
                ddlVendor.BackColor = Color.White;

            if ((ddlType.SelectedValue.ToString() == "I" || ddlType.SelectedValue.ToString() == "Y") && ddlToLoc.SelectedValue.ToInt() == 0)
            {
                ddlToLoc.BackColor = Color.Red;
                RetVal = false;
            }
            else
                ddlToLoc.BackColor = Color.White;

            if (ddlType.SelectedValue.ToString() == "0")
                ddlType.BackColor = Color.Red;
            else
                ddlType.BackColor = Color.White;

            if (dvgItemTran.Rows.Count == 0)
            {
                RetVal = false;
                dvgItemTran.BackgroundColor = Color.Red;
            }
            else
                dvgItemTran.BackgroundColor =  Color.White;
            return RetVal;
        }

        private void fillItem(int GroupID)
        {
            InventoryBLL ven = new InventoryBLL();
            InventoryEntity oven = (InventoryEntity)ven.GetInvTranForEdit(GroupID, GeneralFunctions.LoginInfo.UserID);

            ddlVendor.Enabled = true;
            ddlToLoc.Enabled = true;
            dtRefDate.Enabled = true;
            txtVendorRef.Enabled = true;

            txtNarration.Text = oven.Narration;
            txtTranNo.Text = oven.TranNo;
            txtVendorRef.Text = oven.RefNo;
            dtRefDate.Checked = false;
            if (oven.RefDate != null)
            {
                dtRefDate.Checked = true;
                dtRefDate.Value = oven.RefDate.Value;
            }
            dtTranDate.Value = oven.TranDate;
            ddlToLoc.SelectedValue = oven.CCID;
            ddlVendor.SelectedValue = oven.VendorID;
            ddlType.SelectedValue = oven.TranType;
            if (oven.CCID == 0)
                ddlToLoc.Enabled = false;
            if (oven.VendorID == 0)
            {
                ddlVendor.Enabled = false;
                txtVendorRef.Enabled = false;
                dtRefDate.Enabled = false;
            }
            if (GeneralFunctions.LoginInfo.UserRole == 2 || (GeneralFunctions.LoginInfo.UserRole == 4))
                btnSave.Visible = false;
        }

        private void LoadDDL()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            Dictionary<string, string> cStatus = new Dictionary<string, string>();
            cStatus.Add("R", "Receive");
            cStatus.Add("I", "Issue");
            cStatus.Add("L", "Local Cash Purchase");
            cStatus.Add("X", "Receive Return");
            cStatus.Add("Y", "Issue Return");
            cStatus.Add("D", "Wastage");

            ddlType.DataSource = new BindingSource(cStatus, null);
            ddlType.DisplayMember = "Value";
            ddlType.ValueMember = "Key";
            ddlType.SelectedIndex = -1;
        }

        private void LoadFromLocation(int locID)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            //populate Locations
            //cStatus.Add("R", "Receive");
            //cStatus.Add("I", "Issue");
            //cStatus.Add("L", "Local Cash Purchase");
            //cStatus.Add("X", "Receive Return");
            //cStatus.Add("Y", "Issue Return");
            //cStatus.Add("D", "Wastage");

            SearchCriteria searchcriteria = new SearchCriteria();
            searchcriteria.QueryStat = "L";
            if (ddlType.SelectedValue.ToString() == "R" || ddlType.SelectedValue.ToString() == "X")
            {
                searchcriteria.LocID = GeneralFunctions.LoginInfo.LocID;
            }
            else if (ddlType.SelectedValue.ToString() == "I" || ddlType.SelectedValue.ToString() == "Y")
            {
                searchcriteria.LocID = 0;
            }
            else if (ddlType.SelectedValue.ToString() == "W")
            {
                searchcriteria.LocID = 0;
            }
           
            if (ddlType.SelectedValue != "L")
            {
                ds = new LocationBLL().GetAllLoc(searchcriteria);
                dt = ds.Tables[0];

                DataRow nullrow = dt.NewRow();
                nullrow["pk_LocID"] = 0;
                nullrow["LocationName"] = "All Locations";
                dt.Rows.Add(nullrow);
                if (dt.Rows.Count > 0)
                {
                    ddlVendor.DataSource = dt;
                    ddlVendor.DisplayMember = "LocationName";
                    ddlVendor.ValueMember = "pk_LocID";
                    ddlVendor.SelectedValue = "0";
                }
                if (ddlType.SelectedValue == "I" || ddlType.SelectedValue == "Y" || ddlType.SelectedValue == "W")
                {
                    ddlVendor.SelectedValue = GeneralFunctions.LoginInfo.LocID;
                    ddlVendor.Enabled = false;
                }
            }
            //else
            //{
            //    LoadVendor();
            //}
        }

        private void LoadToLocation(int locID)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            //populate Locations
            SearchCriteria searchcriteria = new SearchCriteria();
            searchcriteria.QueryStat = "L";

            if (ddlType.SelectedValue == "R" || ddlType.SelectedValue == "X")
            {
                searchcriteria.LocID = 0;
            }
            else if (ddlType.SelectedValue == "I" || ddlType.SelectedValue == "Y")
            {
                searchcriteria.LocID = searchcriteria.LocID = GeneralFunctions.LoginInfo.LocID; ;
            }
            else if (ddlType.SelectedValue == "W")
            {
                searchcriteria.LocID = 0;
            }

            ds = new LocationBLL().GetAllLoc(searchcriteria);
            dt = ds.Tables[0];

            DataRow nullrow = dt.NewRow();
            nullrow["pk_LocID"] = 0;
            nullrow["LocationName"] = "All Locations";
            dt.Rows.Add(nullrow);
            if (dt.Rows.Count > 0)
            {
                ddlToLoc.DataSource = dt;
                ddlToLoc.DisplayMember = "LocationName";
                ddlToLoc.ValueMember = "pk_LocID";
                ddlToLoc.SelectedValue = "0";
            }

            if (ddlType.SelectedValue == "R" || ddlType.SelectedValue == "X")
            {
                ddlToLoc.SelectedValue = GeneralFunctions.LoginInfo.LocID;
                ddlToLoc.Enabled = false;
            }
            else if (ddlType.SelectedValue == "W")
            {
                ddlToLoc.SelectedIndex = -1;
                ddlToLoc.Enabled = false;
            }
        }

        //private void LoadVendor()
        //{
        //    DataSet ds = new DataSet();
        //    DataTable dt = new DataTable();

        //    SearchCriteria searchcriteria = new SearchCriteria();
        //    searchcriteria.QueryStat = "L";

        //    ddlVendor.DataSource = null;

        //    ds = new VendorBLL().GetAllVendor(searchcriteria);
        //    dt = ds.Tables[0];

        //    DataRow nullrow = dt.NewRow();
        //    nullrow["pk_VendorID"] = 0;
        //    nullrow["VendorName"] = "Select";
        //    dt.Rows.Add(nullrow);
        //    if (dt.Rows.Count > 0)
        //    {
        //        ddlVendor.DataSource = dt;
        //        ddlVendor.DisplayMember = "VendorName";
        //        ddlVendor.ValueMember = "pk_VendorID";
        //        ddlVendor.SelectedValue = "0";
        //    }
            
        //}

        private void CalculateTotal()
        {
            //txtRate.Text = txtRate.Text.ToDecimal().ToString("n2");
            //txtQty.Text = txtQty.Text.ToDecimal().ToString("n3");
            //txtTax.Text = txtTax.Text.ToDecimal().ToString("n2");
            if (txtRate.Text != string.Empty && txtQty.Text != string.Empty)
            {
                txtTotal.Text = (txtRate.Text.ToDecimal() * txtQty.Text.ToDecimal() + txtTax.Text.ToDecimal()).ToString("n2");
            }
        }

        private void InitializeItems()
        {
            ddlItem.SelectedValue = "0";
            txtQty.Text = "";
            txtRate.Text = "";
            txtTotal.Text = "";
            ddlItem.Enabled = true;
        }

        private void CalculteGrandTotal(DataTable dtItem)
        {
            decimal amt = 0;
            for (int i = 0; i < dtItem.Rows.Count; i++)
            {
                DataRow dr = dtItem.Rows[i];
                amt += (dr["Qty"].ToDecimal() * dr["Rate"].ToDecimal()) + dr["Taxes"].ToDecimal();
            }
            txtGtotal.Text = amt.ToString("########0.00");
        }

        #endregion

        #region Item Entry
        private void LoadTran()
        {
            string path = Application.StartupPath + "\\";

            DataGridViewCellStyle style = new DataGridViewCellStyle();
            dvgItemTran.EnableHeadersVisualStyles = false;
            style.ForeColor = Color.Blue;

            dvgItemTran.AutoGenerateColumns = false;

            dvgItemTran.Columns[0].Name = "pk_RMTranItemID";
            dvgItemTran.Columns[1].Name = "fk_RMTranID";
            dvgItemTran.Columns[2].Name = "TranType";
            dvgItemTran.Columns[3].Name = "fk_ItemID";
            dvgItemTran.Columns[4].Name = "Item Description";
            dvgItemTran.Columns[5].Name = "Qty";
            dvgItemTran.Columns[6].Name = "Rate";
            dvgItemTran.Columns[7].Name = "Taxes";
            dvgItemTran.Columns[8].Name = "TotalAmt";

            dvgItemTran.Columns[0].HeaderText = "pk_RMTranItemID";
            dvgItemTran.Columns[1].HeaderText = "fk_RMTranID";
            dvgItemTran.Columns[2].HeaderText = "TranType";
            dvgItemTran.Columns[3].HeaderText = "fk_ItemID";
            dvgItemTran.Columns[4].HeaderText = "Item Description";
            dvgItemTran.Columns[5].HeaderText = "Qty";
            dvgItemTran.Columns[6].HeaderText = "Rate";
            dvgItemTran.Columns[7].HeaderText = "Taxes";
            dvgItemTran.Columns[8].HeaderText = "Total";

            dvgItemTran.Columns[0].DataPropertyName = "pk_RMTranItemID";
            dvgItemTran.Columns[1].DataPropertyName = "fk_RMTranID";
            dvgItemTran.Columns[2].DataPropertyName = "TranType";
            dvgItemTran.Columns[3].DataPropertyName = "fk_RMID";
            dvgItemTran.Columns[4].DataPropertyName = "RMName";
            dvgItemTran.Columns[5].DataPropertyName = "Qty";
            dvgItemTran.Columns[6].DataPropertyName = "Rate";
            dvgItemTran.Columns[7].DataPropertyName = "Taxes";
            dvgItemTran.Columns[8].DataPropertyName = "TotalAmt";

            dvgItemTran.Columns[4].Width = 360;
            dvgItemTran.Columns[5].Width = 75;
            dvgItemTran.Columns[6].Width = 100;
            dvgItemTran.Columns[7].Width = 93;
            dvgItemTran.Columns[8].Width = 115;

            dvgItemTran.Columns[0].Visible = false;
            dvgItemTran.Columns[1].Visible = false;
            dvgItemTran.Columns[2].Visible = false;
            dvgItemTran.Columns[3].Visible = false;

            dvgItemTran.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dvgItemTran.Columns[5].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
            dvgItemTran.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dvgItemTran.Columns[6].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
            dvgItemTran.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dvgItemTran.Columns[7].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
            dvgItemTran.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dvgItemTran.Columns[8].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;

            DataGridViewImageColumn Editimg = new DataGridViewImageColumn();
            Image Editimage = Image.FromFile(path + ConfigurationManager.AppSettings["imageFolder"] + "edit-icon.png");
            Editimg.Image = Editimage;
            dvgItemTran.Columns.Add(Editimg);
            Editimg.HeaderText = "Edit";
            Editimg.Name = "Eimg";
            Editimg.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            Editimg.Width = 40;

            DataGridViewImageColumn Delimg = new DataGridViewImageColumn();
            Image Delimage = Image.FromFile(path + ConfigurationManager.AppSettings["imageFolder"] + "delete-icon.png");
            Delimg.Image = Delimage;
            dvgItemTran.Columns.Add(Delimg);
            Delimg.HeaderText = "Del";
            Delimg.Name = "Dimg";
            Delimg.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            Delimg.Width = 40;

            dvgItemTran.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dvgItemTran.MultiSelect = false;
            dvgItemTran.Columns["Qty"].DefaultCellStyle.Format = "#######0.000";
            dvgItemTran.Columns["Rate"].DefaultCellStyle.Format = "#######0.00";
            dvgItemTran.Columns["TotalAmt"].DefaultCellStyle.Format = "#######0.00";

            InventoryBLL invtbll = new InventoryBLL();
            dsrm = invtbll.GetAllInvItem(LID);
            DataView view1 = new DataView(dsrm.Tables[0]);

            source1.DataSource = view1;

            dvgItemTran.DataSource = source1;
            dvgItemTran.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            dvgItemTran.AllowUserToResizeColumns = false;
            dvgItemTran.AllowUserToAddRows = false;
            dvgItemTran.ReadOnly = true;
            CalculteGrandTotal(dsrm.Tables[0]);
            
        }

        private void LoadRM()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            SearchCriteria searchcriteria = new SearchCriteria();
            searchcriteria.QueryStat = "L";

            ddlItem.DataSource = null;
            ddlItem.Items.Clear();

            //InventoryBLL invtbll = new InventoryBLL();
            //ds = invtbll.GetAllRM(searchcriteria);
            //dt = ds.Tables[0];

            //DataRow nullrow = dt.NewRow();
            //nullrow["RMID"] = 0;
            //nullrow["ItemName"] = "Select";
            //dt.Rows.Add(nullrow);
            //if (dt.Rows.Count > 0)
            //{
            //    ddlItem.DataSource = dt;
            //    ddlItem.DisplayMember = "ItemName";
            //    ddlItem.ValueMember = "RMID";
            //    ddlItem.SelectedValue = "0";
            //}


            ddlItem.AutoCompleteMode = AutoCompleteMode.Suggest;
            ddlItem.AutoCompleteSource = AutoCompleteSource.ListItems;
            // Load data in comboBox => cboComboBox1.DataSource = .....
            // Other things
        }


        #endregion

    }
}
