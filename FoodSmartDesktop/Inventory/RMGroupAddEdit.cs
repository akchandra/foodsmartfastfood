﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FoodSmart.BLL;
using FoodSmart.Common;
using System.Configuration;
using System.IO;
using System.Drawing.Drawing2D;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using FoodSmart.Utilities.Cryptography;

namespace FoodSmartDesktop.Inventory
{
    public partial class RMGroupAddEdit : Form
    {
        string stat;
        int LID;
        int GroupID;

        public RMGroupAddEdit(int ID)
        {
            InitializeComponent();
            if (ID == 0)
            {
                stat = "A";
                LID = 0;
            }
            else
            {
                stat = "E";

                LID = ID;
            }
           
        }
        #region Public Methods
        private void RMGroupAddEdit_Load(object sender, EventArgs e)
        {
            txtGroupInitial.MaxLength = 2;
            txtGroupName.MaxLength = 30;
            txtGroupInitial.CharacterCasing = CharacterCasing.Upper;
            txtGroupName.CharacterCasing = CharacterCasing.Upper;

            LoadOtherddl();

            if (LID != 0)
                fillItem(LID);

            txtGroupName.Focus();
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (ValidateEntry())
            {
                IInventory grp = new InventoryEntity();
                grp.GroupID = LID;
                grp.GroupInitial = txtGroupInitial.Text;
                grp.RMGroupName = txtGroupName.Text;
                grp.TranType = ddlGroupType.SelectedValue.ToString();
                grp.CreatedBy = GeneralFunctions.LoginInfo.UserID;

                int Status = new InventoryBLL().SaveRMGroup(grp, stat);

                if (Status == 0)
                {
                    MessageBox.Show("Error!! Record Not Saved", "Save Record", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    if (stat == "A")
                    {
                        Initialize();
                        txtGroupName.Focus();
                    }
                    else
                    {
                        this.Close();
                    }
                }
            }
        }

        private void lblExit_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void lblExit_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnSave_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnSave_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        #endregion
        #region "Private Methods"

        private void LoadOtherddl()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            //DataRow row = new DataRow();

            Dictionary<string, string> cStatus = new Dictionary<string, string>();
            cStatus.Add("S", "Stock Item");
            cStatus.Add("C", "Consumables");
            ddlGroupType.DataSource = new BindingSource(cStatus, null);
            ddlGroupType.DisplayMember = "Value";
            ddlGroupType.ValueMember = "Key";
            ddlGroupType.SelectedIndex = 0;
        }

        private void Initialize()
        {
            ddlGroupType.SelectedIndex = 0;
            txtGroupInitial.Text = "";
            txtGroupName.Text = "";
        }


        private bool ValidateEntry()
        {
            bool RetVal = true;

            if (string.IsNullOrEmpty(txtGroupName.Text))
            {
                txtGroupName.BackColor = Color.Red;
                RetVal = false;
            }

            return RetVal;
        }

        private void fillItem(int GroupID)
        {
            InventoryBLL oinvbll = new InventoryBLL();
            InventoryEntity oinv = (InventoryEntity)oinvbll.GetRMGroupForEdit(GroupID, GeneralFunctions.LoginInfo.UserID);

            txtGroupInitial.Text = oinv.GroupInitial;
            txtGroupName.Text = oinv.RMGroupName;
            ddlGroupType.SelectedValue = oinv.TranType;
            if (GeneralFunctions.LoginInfo.UserRole == 2 || (GeneralFunctions.LoginInfo.UserRole == 4))
                btnSave.Visible = false;
        }

        #endregion

    }
}
