﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using FoodSmart.DAL;
using FoodSmart.BLL;
using FoodSmart.Entity;
using FoodSmart.GeneralUtilities;
using FoodSmart.Utilities;
using FoodSmart.Common;

namespace FoodSmartDesktop.Inventory
{
    public partial class ManageRMGroup : Form
    {
        BindingSource source1 = new BindingSource();
        private int PgSize = 12;
        private int CurrentPageIndex = 1;
        private int TotalPage = 0;
        private DataSet dsPage;

        public ManageRMGroup()
        {
            InitializeComponent();
        }

        #region Public Method
        private void ManageRMGroup_Load(object sender, EventArgs e)
        {
            dvgRMGroup.ColumnCount = 4;
            this.ddlGroupType.SelectedIndexChanged -= new System.EventHandler(this.ddlGroupType_SelectedIndexChanged);

            SetWatermarkText();
            LoadLoc();
            LoadDDL();
            ddlGroupType.SelectedIndex = 0;
            //LoadRest();
            //LoadItemGroup();

            SearchCriteria searchCriteria = new SearchCriteria();
            BuildDefaultSearch(searchCriteria);

            LoadItem(searchCriteria);

            this.ddlGroupType.SelectedIndexChanged += new System.EventHandler(this.ddlGroupType_SelectedIndexChanged);
            foreach (DataGridViewRow row in dvgRMGroup.Rows)
            {
                row.Height = 25;
            }

            lbltoday.Text += " : " + GeneralFunctions.LoginInfo.CurrDate.ToString("dd-MMM-yyyy");
            lblUserName.Text += GeneralFunctions.LoginInfo.UserFullName;
            ddlGroupType.Focus();
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAddRMGroup_Click(object sender, EventArgs e)
        {
            this.Hide();
            Inventory.RMGroupAddEdit rmg = new Inventory.RMGroupAddEdit(0);

            if (rmg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {

            }
            else
            {
                this.Show();
                //dvgRMGroup.Columns.Remove("Dimg");
                dvgRMGroup.Columns.Remove("Eimg");
                dvgRMGroup.Columns.Remove("Dimg");
                SearchCriteria searchCriteria = new SearchCriteria();
                BuildDefaultSearch(searchCriteria);
                LoadItem(searchCriteria);
                foreach (DataGridViewRow row in dvgRMGroup.Rows)
                {
                    row.Height = 25;
                }
            }
        }

        private void dvgRMGroup_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int Reply;
            int i;
            if (dvgRMGroup.RowCount == 0)
                return;
            else
                i = dvgRMGroup.CurrentRow.Index;

            if (dvgRMGroup.Columns[e.ColumnIndex].Name == "Dimg")
            {
                Reply = Convert.ToInt32(MessageBox.Show("Want to Delete this Record?", "Delete Record", MessageBoxButtons.OKCancel, MessageBoxIcon.Question));
                if (Reply == 1)
                {
                    Reply = Convert.ToInt32(dvgRMGroup.CurrentRow.Cells[0].Value);

                    InventoryBLL itembll = new InventoryBLL();

                    //RestaurantBLL restBLL = new RestaurantBLL();
                    itembll.DeleteRMGroup(Reply);

                    dvgRMGroup.Columns.Remove("Dimg");
                    dvgRMGroup.Columns.Remove("Eimg");
                    SearchCriteria searchCriteria = new SearchCriteria();
                    BuildDefaultSearch(searchCriteria);
                    LoadItem(searchCriteria);
                    foreach (DataGridViewRow row in dvgRMGroup.Rows)
                    {
                        row.Height = 25;
                    }
                }
                
            }

            if (dvgRMGroup.Columns[e.ColumnIndex].Name == "Eimg")
            {
                Inventory.RMGroupAddEdit rmg = new Inventory.RMGroupAddEdit(Convert.ToInt32(dvgRMGroup.CurrentRow.Cells[0].Value));
                this.Hide();

                if (rmg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    this.Hide();
                }
                else
                {
                    this.Show();
                    dvgRMGroup.Columns.Remove("Dimg");
                    dvgRMGroup.Columns.Remove("Eimg");
                    SearchCriteria searchCriteria = new SearchCriteria();
                    BuildDefaultSearch(searchCriteria);
                    LoadItem(searchCriteria);
                    foreach (DataGridViewRow row in dvgRMGroup.Rows)
                    {
                        row.Height = 25;
                    }
                }
            }
        }

        private void ddlGroupType_SelectedIndexChanged(object sender, EventArgs e)
        {
             CheckFilterCondition();
        }

        private void txtItemName_TextChanged(object sender, EventArgs e)
        {
            CheckFilterCondition();
        }

        private void picMoveFirst_Click(object sender, EventArgs e)
        {
            if (CurrentPageIndex != 0)
            {
                CurrentPageIndex = 0;
                ChangePageIndex();
            }
        }

        private void picMovePrev_Click(object sender, EventArgs e)
        {
            if (CurrentPageIndex != 0)
            {
                CurrentPageIndex--;
                ChangePageIndex();
            }
        }

        private void picMoveNext_Click(object sender, EventArgs e)
        {
            if (CurrentPageIndex + 1 < TotalPage)
            {
                CurrentPageIndex++;
                ChangePageIndex();
            }
        }

        private void picMoveLast_Click(object sender, EventArgs e)
        {
            if (CurrentPageIndex + 1 < TotalPage)
            {
                CurrentPageIndex = TotalPage - 1;
                ChangePageIndex();
            }
        }

        private void lblExit_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void lblExit_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnAddRMGroup_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnAddRMGroup_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        #endregion

        #region Private method
        private void ChangePageIndex()
        {
            DataView view1;
            view1 = new DataView(GeneralFunctions.GetCurrentRecords(CurrentPageIndex, dsPage.Tables[0], PgSize, "GroupID"));
            source1.DataSource = view1;
            int pg = CurrentPageIndex + 1;
            lblPageNo.Text = pg.ToString() + " / " + TotalPage.ToString();
            foreach (DataGridViewRow row in dvgRMGroup.Rows)
            {
                row.Height = 25;
            }
        }

        private void LoadItem(SearchCriteria searchCriteria)
        {
            string path = Application.StartupPath + "\\";
            DataView view1;

            DataGridViewCellStyle style = new DataGridViewCellStyle();
            dvgRMGroup.EnableHeadersVisualStyles = false;
            style.ForeColor = Color.Blue;

            dvgRMGroup.AutoGenerateColumns = false;

            dvgRMGroup.Columns[0].Name = "GroupID";
            dvgRMGroup.Columns[1].Name = "Group Name";
            dvgRMGroup.Columns[2].Name = "Group Type";
            //dvgRMGroup.Columns[3].Name = "Group Initial";
            dvgRMGroup.Columns[3].Name = "RM";

            dvgRMGroup.Columns[1].HeaderText = "Group Name";
            dvgRMGroup.Columns[2].HeaderText = "Group Type";
            //dvgRMGroup.Columns[3].HeaderText = "Group Initial";
            dvgRMGroup.Columns[3].HeaderText = "RM";

            dvgRMGroup.Columns[0].DataPropertyName = "GroupID";
            dvgRMGroup.Columns[1].DataPropertyName = "RMGroupDescr";
            dvgRMGroup.Columns[2].DataPropertyName = "GroupType";
            //dvgRMGroup.Columns[3].DataPropertyName = "GroupInitials";
            dvgRMGroup.Columns[3].DataPropertyName = "RM";

            dvgRMGroup.Columns[1].Width = 500;
            dvgRMGroup.Columns[2].Width = 200;
            //dvgRMGroup.Columns[3].Width = 150;

            dvgRMGroup.Columns[1].SortMode = DataGridViewColumnSortMode.Automatic;
            dvgRMGroup.Columns[2].SortMode = DataGridViewColumnSortMode.Automatic;
            dvgRMGroup.Columns[3].SortMode = DataGridViewColumnSortMode.Automatic;

            dvgRMGroup.Columns[0].Visible = false;
            dvgRMGroup.Columns[3].Visible = false;
            //dvgRMGroup.Columns[9].Visible = false;

            DataGridViewImageColumn Editimg = new DataGridViewImageColumn();
            Image Editimage = Image.FromFile(path + ConfigurationManager.AppSettings["imageFolder"] + "edit-icon.png");
            Editimg.Image = Editimage;
            dvgRMGroup.Columns.Add(Editimg);
            Editimg.HeaderText = "Edit";
            Editimg.Name = "Eimg";
            Editimg.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            Editimg.Width = 40;

            DataGridViewImageColumn Delimg = new DataGridViewImageColumn();
            Image Delimage = Image.FromFile(path + ConfigurationManager.AppSettings["imageFolder"] + "delete-icon.png");
            Delimg.Image = Delimage;
            dvgRMGroup.Columns.Add(Delimg);
            Delimg.HeaderText = "Del";
            Delimg.Name = "Dimg";
            Delimg.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            Delimg.Width = 40;

            dvgRMGroup.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dvgRMGroup.MultiSelect = false;

            InventoryBLL grpbll = new InventoryBLL();
            dsPage = grpbll.GetAllRMGroup(searchCriteria);
            if (dsPage.Tables[0].Rows.Count == 0)
                view1 = null;
            else
            {
                TotalPage = GeneralFunctions.CalculateTotalPages(dsPage.Tables[0], PgSize);
                view1 = new DataView(GeneralFunctions.GetCurrentRecords(0, dsPage.Tables[0], PgSize, "GroupID"));
                CurrentPageIndex = 0;
                lblPageNo.Text = 1.ToString() + " / " + TotalPage.ToString();

            }
            source1.DataSource = view1;

            dvgRMGroup.DataSource = source1;
            dvgRMGroup.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            dvgRMGroup.AllowUserToResizeColumns = false;
            dvgRMGroup.AllowUserToAddRows = false;
            dvgRMGroup.ReadOnly = true;
            dvgRMGroup.Columns[1].HeaderCell.Style = style;
            dvgRMGroup.Columns[2].HeaderCell.Style = style;
            dvgRMGroup.Columns[3].HeaderCell.Style = style;

            //foreach (DataGridViewRow row in dvgRMGroup.Rows)
            //{
            //    string RowType = row.Cells["RM"].Value.ToString();

            //    if (RowType == "1")
            //    {
            //        row.Cells["Dimg"].Value = new Bitmap(1, 1);
            //    }
            //}
        }


        private void BuildDefaultSearch(SearchCriteria criteria)
        {
            criteria.GroupName = "";
            criteria.LocName = ddlLoc.Text;
            //criteria.RestName = ddlGroupType.SelectedValue.ToString();
            criteria.BillType = ddlGroupType.SelectedValue.ToString();

        }

        private void BuildSearchCriteria(SearchCriteria criteria)
        {
            criteria.GroupName = txtGroupName.Text;
            //criteria.CardName = txtName.Text;
            criteria.RestName = ddlGroupType.SelectedValue.ToString();
            criteria.LocName = ddlLoc.Text;
            criteria.BillType = ddlGroupType.SelectedValue.ToString();
        }

        private void LoadDDL()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            //DataRow row = new DataRow();

            Dictionary<string, string> cStatus = new Dictionary<string, string>();
            cStatus.Add("S", "Stock Item");
            cStatus.Add("C", "Consumables");
            ddlGroupType.DataSource = new BindingSource(cStatus, null);
            ddlGroupType.DisplayMember = "Value";
            ddlGroupType.ValueMember = "Key";
            ddlGroupType.SelectedIndex = -1;
        }

        private void CheckFilterCondition()
        {
            SearchCriteria searchCriteria = new SearchCriteria();
            BuildSearchCriteria(searchCriteria);
            LoadItem(searchCriteria);
            dvgRMGroup.Columns.Remove("Dimg");
            dvgRMGroup.Columns.Remove("Eimg");
            //LoadRestaurants(searchCriteria);
            foreach (DataGridViewRow row in dvgRMGroup.Rows)
            {
                row.Height = 25;
            }
        }

        private void SetWatermarkText()
        {

            WatermarkText.SetWatermark(txtGroupName, "RM Group");
            //WatermarkText.SetWatermark(txtName, "User / Restaurant Name");
        }

        private void LoadLoc()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            //populate Locations
            
            SearchCriteria searchcriteria = new SearchCriteria();
            searchcriteria.QueryStat = "L";

            ds = new LocationBLL().GetAllLoc(searchcriteria);
            dt = ds.Tables[0];

            //DataRow nullrow = dt.NewRow();
            //nullrow["pk_LocID"] = 0;
            //nullrow["LocationName"] = "All Locations";
            //dt.Rows.Add(nullrow);
            if (dt.Rows.Count > 0)
            {
                ddlLoc.DataSource = dt;
                ddlLoc.DisplayMember = "LocationName";
                ddlLoc.ValueMember = "pk_LocID";
                ddlLoc.SelectedIndex = 0;
            }
        }


        #endregion
        
       
    }
}
