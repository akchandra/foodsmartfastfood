﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FoodSmart.BLL;
using FoodSmart.Common;
using System.Configuration;
using System.IO;
using System.Drawing.Drawing2D;
using FoodSmart.Entity;
//using FoodSmart.BLL.itemService;
using FoodSmart.Utilities;
using FoodSmart.Utilities.Cryptography;
using SearchCriteria = FoodSmart.BLL.InventoryService.SearchCriteria;


namespace FoodSmartDesktop.Inventory
{
    public partial class AddEditCostCenter : Form
    {
        string stat;
        int LID;
        int GroupID;

        public AddEditCostCenter(int ID)
        {
            InitializeComponent();

            if (ID == 0)
            {
                stat = "A";
                LID = 0;
            }
            else
            {
                stat = "E";

                LID = ID;
            }
        }

        #region Public Methods
        private void AddEditCostCenter_Load(object sender, EventArgs e)
        {
            txtCCName.MaxLength = 30;
            txtCCName.CharacterCasing = CharacterCasing.Upper;

            if (LID != 0)
                fillCC(LID);

            txtCCName.Focus();
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (ValidateEntry())
            {

                IInventory grp = new InventoryEntity();
                grp.CCID = LID;
                grp.CostCenterName = txtCCName.Text;
                grp.CreatedBy = GeneralFunctions.LoginInfo.UserID;

                int Status = new InventoryBLL().SaveCC(grp, stat);

                if (Status == 0)
                {
                    MessageBox.Show("Error!! Record Not Saved", "Save Record", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    if (stat == "A")
                    {
                        Initialize();
                        txtCCName.Focus();
                    }
                    else
                    {
                        this.Close();
                    }
                }
            }
        }

        private void lblExit_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void lblExit_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnSave_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnSave_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        #endregion

        #region "Private Methods"

        private void Initialize()
        {
            txtCCName.Text = "";
        }


        private bool ValidateEntry()
        {
            bool RetVal = true;

            if (string.IsNullOrEmpty(txtCCName.Text))
            {
                txtCCName.BackColor = Color.Red;
                RetVal = false;
            }

            return RetVal;
        }

        private void fillCC(int CCID)
        {
            InventoryBLL oinvbll = new InventoryBLL();
            InventoryEntity oinv = (InventoryEntity)oinvbll.GetCCForEdit(CCID, GeneralFunctions.LoginInfo.UserID);

            txtCCName.Text = oinv.CostCenterName;
            if (GeneralFunctions.LoginInfo.UserRole == 2 || (GeneralFunctions.LoginInfo.UserRole == 4))
                btnSave.Visible = false;

        }
        #endregion


    }
}
