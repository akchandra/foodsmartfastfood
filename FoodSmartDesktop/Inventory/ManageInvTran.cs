﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using FoodSmart.DAL;
using FoodSmart.BLL;
using FoodSmart.Entity;
using FoodSmart.GeneralUtilities;
using FoodSmart.Utilities;
using FoodSmart.Common;

namespace FoodSmartDesktop.Inventory
{
    public partial class ManageInvTran : Form
    {
        BindingSource source1 = new BindingSource();
        private int PgSize = 12;
        private int CurrentPageIndex = 1;
        private int TotalPage = 0;
        private DataSet dsPage;

        public ManageInvTran()
        {
            InitializeComponent();
        }

        #region Public Method

        private void ManageInvTran_Load(object sender, EventArgs e)
        {
            dvgInvTran.ColumnCount = 6;
            this.ddlVendor.SelectedIndexChanged -= new System.EventHandler(this.ddlVendor_SelectedIndexChanged);
            this.ddlCC.SelectedIndexChanged -= new System.EventHandler(this.ddlCC_SelectedIndexChanged);
            this.ddlType.SelectedIndexChanged -= new System.EventHandler(this.ddlType_SelectedIndexChanged);

            SetWatermarkText();
            LoadCC();
            
            LoadDDL();
            //LoadVendor();
            ddlCC.SelectedValue = "0";
            ddlVendor.SelectedValue = "0";
            ddlType.SelectedIndex = 0;

            SearchCriteria searchCriteria = new SearchCriteria();
            BuildDefaultSearch(searchCriteria);

            LoadTran(searchCriteria);

            this.ddlVendor.SelectedIndexChanged += new System.EventHandler(this.ddlVendor_SelectedIndexChanged);
            this.ddlCC.SelectedIndexChanged += new System.EventHandler(this.ddlCC_SelectedIndexChanged);
            this.ddlType.SelectedIndexChanged += new System.EventHandler(this.ddlType_SelectedIndexChanged);

            foreach (DataGridViewRow row in dvgInvTran.Rows)
            {
                row.Height = 25;
            }

            lbltoday.Text += " : " + GeneralFunctions.LoginInfo.CurrDate.ToString("dd-MMM-yyyy");
            lblUserName.Text += GeneralFunctions.LoginInfo.UserFullName;
            ddlVendor.Focus();
        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAddItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            Inventory.AddEditInvTran rmg = new Inventory.AddEditInvTran(0);

            if (rmg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {

            }
            else
            {
                this.Show();
                //dvgInvTran.Columns.Remove("Dimg");
                dvgInvTran.Columns.Remove("Eimg");
                dvgInvTran.Columns.Remove("Dimg");
                SearchCriteria searchCriteria = new SearchCriteria();
                BuildDefaultSearch(searchCriteria);
                LoadTran(searchCriteria);
                foreach (DataGridViewRow row in dvgInvTran.Rows)
                {
                    row.Height = 25;
                }
            }
        }

        private void dvgInvTran_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int Reply;
            int i;
            if (dvgInvTran.RowCount == 0)
                return;
            else
                i = dvgInvTran.CurrentRow.Index;

            if (dvgInvTran.Columns[e.ColumnIndex].Name == "Dimg")
            {
                Reply = Convert.ToInt32(MessageBox.Show("Want to Delete this Record?", "Delete Record", MessageBoxButtons.OKCancel, MessageBoxIcon.Question));
                if (Reply == 1)
                {
                    Reply = Convert.ToInt32(dvgInvTran.CurrentRow.Cells[0].Value);

                    InventoryBLL itembll = new InventoryBLL();

                    //RestaurantBLL restBLL = new RestaurantBLL();
                    itembll.DeleteInvTran(Reply, GeneralFunctions.LoginInfo.UserID);

                    dvgInvTran.Columns.Remove("Dimg");
                    dvgInvTran.Columns.Remove("Eimg");
                    SearchCriteria searchCriteria = new SearchCriteria();
                    BuildDefaultSearch(searchCriteria);
                    LoadTran(searchCriteria);
                    foreach (DataGridViewRow row in dvgInvTran.Rows)
                    {
                        row.Height = 25;
                    }
                }
            }

            if (dvgInvTran.Columns[e.ColumnIndex].Name == "Eimg")
            {
                Inventory.AddEditInvTran rmg = new Inventory.AddEditInvTran(Convert.ToInt32(dvgInvTran.CurrentRow.Cells[0].Value));
                this.Hide();

                if (rmg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    this.Hide();
                }
                else
                {
                    this.Show();
                    dvgInvTran.Columns.Remove("Dimg");
                    dvgInvTran.Columns.Remove("Eimg");
                    SearchCriteria searchCriteria = new SearchCriteria();
                    BuildDefaultSearch(searchCriteria);
                    LoadTran(searchCriteria);
                    foreach (DataGridViewRow row in dvgInvTran.Rows)
                    {
                        row.Height = 25;
                    }
                }
            }
        }

        private void txtTranNo_TextChanged(object sender, EventArgs e)
        {
            CheckFilterCondition();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            CheckFilterCondition();
        }

        private void ddlVendor_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckFilterCondition();
        }

        private void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckFilterCondition();
        }

        private void ddlCC_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckFilterCondition();
        }

        private void picMoveFirst_Click(object sender, EventArgs e)
        {
            if (CurrentPageIndex != 0)
            {
                CurrentPageIndex = 0;
                ChangePageIndex();
            }
        }

        private void picMovePrev_Click(object sender, EventArgs e)
        {
            if (CurrentPageIndex != 0)
            {
                CurrentPageIndex--;
                ChangePageIndex();
            }
        }

        private void picMoveNext_Click(object sender, EventArgs e)
        {
            if (CurrentPageIndex + 1 < TotalPage)
            {
                CurrentPageIndex++;
                ChangePageIndex();
            }
        }

        private void picMoveLast_Click(object sender, EventArgs e)
        {
            if (CurrentPageIndex + 1 < TotalPage)
            {
                CurrentPageIndex = TotalPage - 1;
                ChangePageIndex();
            }
        }

        private void lblExit_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void lblExit_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnAddItem_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnAddItem_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        #endregion


        #region Private method
        private void ChangePageIndex()
        {
            DataView view1;
            view1 = new DataView(GeneralFunctions.GetCurrentRecords(CurrentPageIndex, dsPage.Tables[0], PgSize, "itemID"));
            source1.DataSource = view1;
            int pg = CurrentPageIndex + 1;
            lblPageNo.Text = pg.ToString() + " / " + TotalPage.ToString();
            foreach (DataGridViewRow row in dvgInvTran.Rows)
            {
                row.Height = 25;
            }
        }

        private void LoadDDL()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            //DataRow row = new DataRow();

            Dictionary<string, string> cStatus = new Dictionary<string, string>();
            cStatus.Add("A", "All");
            cStatus.Add("R", "Receive");
            cStatus.Add("I", "Issue");
            cStatus.Add("X", "Receive Return");
            cStatus.Add("Y", "Issue Return");
            cStatus.Add("D", "Destroy");

            ddlType.DataSource = new BindingSource(cStatus, null);
            ddlType.DisplayMember = "Value";
            ddlType.ValueMember = "Key";
            ddlType.SelectedIndex = 0;
        }

        private void LoadTran(SearchCriteria searchCriteria)
        {
            string path = Application.StartupPath + "\\";
            DataView view1;

            DataGridViewCellStyle style = new DataGridViewCellStyle();
            dvgInvTran.EnableHeadersVisualStyles = false;
            style.ForeColor = Color.Blue;

            dvgInvTran.AutoGenerateColumns = false;

            dvgInvTran.Columns[0].Name = "pk_TranID";
            dvgInvTran.Columns[1].Name = "Tran type";
            dvgInvTran.Columns[2].Name = "Tran No";
            dvgInvTran.Columns[3].Name = "Tran Date";
            dvgInvTran.Columns[4].Name = "VendorName";
            dvgInvTran.Columns[5].Name = "CC Name";

            dvgInvTran.Columns[0].HeaderText = "pk_TranID";
            dvgInvTran.Columns[1].HeaderText = "Type";
            dvgInvTran.Columns[2].HeaderText = "Transaction No";
            dvgInvTran.Columns[3].HeaderText = "Tran Date";

            dvgInvTran.Columns[0].DataPropertyName = "pk_TranID";
            dvgInvTran.Columns[1].DataPropertyName = "TranType";
            dvgInvTran.Columns[2].DataPropertyName = "TranNo";
            dvgInvTran.Columns[3].DataPropertyName = "TranDate";
            dvgInvTran.Columns[4].DataPropertyName = "VendorName";
            dvgInvTran.Columns[5].DataPropertyName = "RefDoc";

            dvgInvTran.Columns[1].Width = 100;
            dvgInvTran.Columns[2].Width = 100;
            dvgInvTran.Columns[3].Width = 150;
            dvgInvTran.Columns[4].Width = 280;
            dvgInvTran.Columns[5].Width = 220;

            dvgInvTran.Columns[1].SortMode = DataGridViewColumnSortMode.Automatic;
            dvgInvTran.Columns[2].SortMode = DataGridViewColumnSortMode.Automatic;
            dvgInvTran.Columns[3].SortMode = DataGridViewColumnSortMode.Automatic;
            dvgInvTran.Columns[4].SortMode = DataGridViewColumnSortMode.Automatic;
            dvgInvTran.Columns[5].SortMode = DataGridViewColumnSortMode.Automatic;

            dvgInvTran.Columns[0].Visible = false;

            DataGridViewImageColumn Editimg = new DataGridViewImageColumn();
            Image Editimage = Image.FromFile(path + ConfigurationManager.AppSettings["imageFolder"] + "edit-icon.png");
            Editimg.Image = Editimage;
            dvgInvTran.Columns.Add(Editimg);
            Editimg.HeaderText = "Edit";
            Editimg.Name = "Eimg";
            Editimg.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            Editimg.Width = 40;

            DataGridViewImageColumn Delimg = new DataGridViewImageColumn();
            Image Delimage = Image.FromFile(path + ConfigurationManager.AppSettings["imageFolder"] + "delete-icon.png");
            Delimg.Image = Delimage;
            dvgInvTran.Columns.Add(Delimg);
            Delimg.HeaderText = "Del";
            Delimg.Name = "Dimg";
            Delimg.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            Delimg.Width = 40;

            dvgInvTran.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dvgInvTran.MultiSelect = false;

            InventoryBLL invtbll = new InventoryBLL();
            dsPage = invtbll.GetAllTransaction(searchCriteria, GeneralFunctions.LoginInfo.UserID);
            if (dsPage.Tables[0].Rows.Count == 0)
                view1 = null;
            else
            {
                TotalPage = GeneralFunctions.CalculateTotalPages(dsPage.Tables[0], PgSize);
                view1 = new DataView(GeneralFunctions.GetCurrentRecords(0, dsPage.Tables[0], PgSize, "pk_TranID"));
                CurrentPageIndex = 0;
                lblPageNo.Text = 1.ToString() + " / " + TotalPage.ToString();
            }
            source1.DataSource = view1;

            dvgInvTran.DataSource = source1;
            dvgInvTran.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            dvgInvTran.AllowUserToResizeColumns = false;
            dvgInvTran.AllowUserToAddRows = false;
            dvgInvTran.ReadOnly = true;
            dvgInvTran.Columns[1].HeaderCell.Style = style;
            dvgInvTran.Columns[2].HeaderCell.Style = style;
            dvgInvTran.Columns[3].HeaderCell.Style = style;

        }

        private void BuildDefaultSearch(SearchCriteria criteria)
        {
            criteria.LocName = ddlLoc.Text;
            
            if (ddlVendor.Text == "--Select--")
                criteria.StringOption2 = null;
            else
                criteria.StringOption2 = ddlVendor.Text;
            criteria.BillType = ddlType.SelectedValue.ToString(); ;
            criteria.StringOption1 = txtTranNo.Text;
            criteria.IntegerOption3 = GeneralFunctions.LoginInfo.CurrFinYearID;
        }

        private void BuildSearchCriteria(SearchCriteria criteria)
        {
            criteria.StringOption1 = txtTranNo.Text;
            //if (ddlCC.Text == "--Select--")
            //    criteria.CCName = null;
            //else
            //    criteria.CCName = ddlCC.Text;

            if (ddlVendor.Text == "--Select--")
                criteria.StringOption2 = null;
            else
                criteria.StringOption2 = ddlVendor.Text;

            //criteria.CCName = ddlCC.Text;
            criteria.BillType = ddlType.SelectedValue.ToString();
            criteria.IntegerOption3 = GeneralFunctions.LoginInfo.CurrFinYearID;
            //criteria.StringOption2 = ddlVendor.Text;
        }

        //private void LoadVendor()
        //{
        //    DataSet ds = new DataSet();
        //    DataTable dt = new DataTable();

        //    SearchCriteria searchcriteria = new SearchCriteria();
        //    //SearchCriteria searchcriteria = new SearchCriteria();
        //    searchcriteria.QueryStat = "L";
        //    //searchcriteria.LocName = ddlLoc.Text;
        //    //searchcriteria.RestName = ddlRestName.Text;

        //    ddlVendor.DataSource = null;

        //    ds = new VendorBLL().GetAllVendor(searchcriteria);
        //    dt = ds.Tables[0];

        //    DataRow nullrow = dt.NewRow();
        //    nullrow["pk_VendorID"] = 0;
        //    nullrow["VendorName"] = "--Select--";
        //    dt.Rows.Add(nullrow);
        //    if (dt.Rows.Count > 0)
        //    {
        //        ddlVendor.DataSource = dt;
        //        ddlVendor.DisplayMember = "VendorName";
        //        ddlVendor.ValueMember = "pk_VendorID";
        //        ddlVendor.SelectedValue = "0";
        //    }
        //    //ddlVendor.SelectedValue = "0";

        //}

        private void CheckFilterCondition()
        {
            SearchCriteria searchCriteria = new SearchCriteria();
            BuildSearchCriteria(searchCriteria);
            LoadTran(searchCriteria);
            dvgInvTran.Columns.Remove("Dimg");
            dvgInvTran.Columns.Remove("Eimg");
            foreach (DataGridViewRow row in dvgInvTran.Rows)
            {
                row.Height = 25;
            }
        }

        private void SetWatermarkText()
        {

            WatermarkText.SetWatermark(txtTranNo, "RM Item");
            //WatermarkText.SetWatermark(txtItemCode, "RM Code");
        }

        private void LoadCC()
        {
            //DataSet ds = new DataSet();
            //DataTable dt = new DataTable();

            ////populate Locations
            //SearchCriteria searchcriteria = new SearchCriteria();
            //searchcriteria.QueryStat = "L";

            //ds = new InventoryBLL().GetAllCC(searchcriteria);
            //dt = ds.Tables[0];

            //DataRow nullrow = dt.NewRow();
            //nullrow["CCID"] = 0;
            //nullrow["CostCenterName"] = "--Select--";
            //dt.Rows.Add(nullrow);
            //if (dt.Rows.Count > 0)
            //{
            //    ddlCC.DataSource = dt;
            //    ddlCC.DisplayMember = "CostCenterName";
            //    ddlCC.ValueMember = "CCID";
            //    ddlCC.SelectedValue = "0";
            //}
        }


        #endregion

       
    }
}
