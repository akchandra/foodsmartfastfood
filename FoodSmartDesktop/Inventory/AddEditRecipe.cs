﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FoodSmart.BLL;
using FoodSmart.Common;
using System.Configuration;
using System.IO;
using System.Drawing.Drawing2D;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using FoodSmart.Utilities.Cryptography;

namespace FoodSmartDesktop.Inventory
{
    public partial class AddEditRecipe : Form
    {
        string stat;
        int LID;
        int ItemTranID;
        BindingSource source1 = new BindingSource();
        private DataSet dsrm;
        string keyAmt;

        public AddEditRecipe(int ID)
        {
            InitializeComponent();
            if (ID == 0)
            {
                stat = "A";
                LID = 0;
            }
            else
            {
                stat = "E";

                LID = ID;
            }
        }

        private void AddEditRecipe_Load(object sender, EventArgs e)
        {
            dvgItemTran.ColumnCount = 9;
            txtRecipeUnit.MaxLength = 50;
            txtNarration.MaxLength = 300;
            //tableLayoutPanel3.Enabled = false;

            this.ddlType.SelectedIndexChanged -= new System.EventHandler(this.ddlType_SelectedIndexChanged);
            //this.ddlVendor.SelectedIndexChanged -= new System.EventHandler(this.ddlVendor_SelectedIndexChanged);
            //this.ddlCC.SelectedIndexChanged -= new System.EventHandler(this.ddlCC_SelectedIndexChanged);

            txtRecipeUnit.CharacterCasing = CharacterCasing.Upper;
            txtNarration.CharacterCasing = CharacterCasing.Upper;
            
            txtRecipeName.Enabled = false;
            ddlFGItem.Enabled = false;
            txtRecipeUnit.Enabled = false;
            dtTranDate.Text = DateTime.Now.ToString();
            

            LoadDDL();
            LoadTran();
            LoadFG();
            //LoadRM();
            tableLayoutPanel3.Enabled = false;

            if (LID != 0)
            {
                fillItem(LID);
                tableLayoutPanel3.Enabled = true;
            }

            //txtRecipeUnit.Focus();
            ddlItem.Enabled = false;
           
            this.ddlType.SelectedIndexChanged += new System.EventHandler(this.ddlType_SelectedIndexChanged);
        }

        private void ddlItem_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void lblExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (ValidateEntry())
            {

                for (int i = 0; i < dsrm.Tables[0].Rows.Count; i++)
                {
                    DataRow dr = dsrm.Tables[0].Rows[i];
                    if (dr["ItemType"] == "Raw Material")
                    {
                        dr["ItemType"] = "R";
                    }
                    else
                    {
                        dr["ItemType"] = "S";
                    }
                    dr["fk_RecipeID"] = LID;
                }

                IInventory rmItem = new InventoryEntity();
                rmItem.RecipeID = LID;
                rmItem.TranDate = dtTranDate.Value;
                rmItem.RecipeUnit = txtRecipeUnit.Text;
                rmItem.TranType = ddlType.SelectedValue.ToString();
                rmItem.RecipeDetail = txtNarration.Text;
                if (ddlType.SelectedValue.ToString() == "F")
                    rmItem.CCID = ddlFGItem.SelectedValue.ToInt();
                else
                    rmItem.RecipeName = txtRecipeName.Text;
                rmItem.BalanceQty = txtQty.Text.ToDecimal();
                
                rmItem.NoofPlates = txtPlates.Text.ToDecimal();
 

                rmItem.CreatedBy = GeneralFunctions.LoginInfo.UserID;

                int Status = new InventoryBLL().SaveRecipe(rmItem, stat, dsrm);

                if (Status == 0)
                {
                    MessageBox.Show("Error!! Record Not Saved", "Save Record", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    if (stat == "A")
                    {
                        Initialize();
                        //txtRecipeUnit.Focus();
                    }
                    else
                    {
                        this.Close();
                    }
                }
            }
        }

        private void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlType.SelectedIndex == 0)
            {
                txtRecipeName.Enabled = false;
                ddlFGItem.Enabled = true;
                txtRecipeUnit.Enabled = false;
            }
            else
            {
                txtRecipeName.Enabled = true;
                ddlFGItem.Enabled = false;
                txtRecipeUnit.Enabled = true;
            }

            if (ddlType.SelectedIndex != -1)
            {
                tableLayoutPanel3.Enabled = true;
            }
        }

        private void btnAddRow_Click(object sender, EventArgs e)
        {
            if (ddlType.SelectedIndex == -1)
            {
               MessageBox.Show("Recipe Type Selection is compulsory.", "Selection Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
               return;
            }
            
            ddlType.Enabled = false;
            if (ddlItem.SelectedValue.ToInt() == 0)
            {
                ddlItem.BackColor = Color.Red;
                return;
            }

            //if ((string.IsNullOrEmpty(txtUnit.Text) || txtUnit.Text.ToDecimal() == 0) && (ddlType.SelectedValue.ToString() == "R" || ddlType.SelectedValue.ToString() == "X"))
            //{
            //    txtUnit.BackColor = Color.Red;
            //    return;
            //}

            if (string.IsNullOrEmpty(txtQty.Text) || txtQty.Text.ToDecimal() == 0)
            {
                txtQty.BackColor = Color.Red;
                return;
            }

            DataRow[] drCOLL = dsrm.Tables[0].Select("fk_RMID = " + ddlItem.SelectedValue);
            DataRow dr;

            if (drCOLL.Length == 0)
            {
                dr = dsrm.Tables[0].NewRow();
                dr["fk_RMID"] = ddlItem.SelectedValue.ToInt();
                dr["ItemType"] = ddlItemType.Text;
                dr["ItemName"] = ddlItem.Text;
                dr["QtyReq"] = txtQty.Text;
                dr["ItemRate"] = txtRate.Text;
                dr["UOM"] = txtUnit.Text;
                dr["TotalAmt"] = (dr["QtyReq"].ToDecimal() * dr["ItemRate"].ToDecimal());
            }
            else
            {
                dr = drCOLL[0];
                dr["fk_RMID"] = ddlItem.SelectedValue.ToInt();
                dr["ItemName"] = ddlItem.Text;
                dr["QtyReq"] = txtQty.Text;
                dr["Rate"] = txtRate.Text;
                dr["UOM"] = txtUnit.Text;
                //dr["TotalAmt"] = (dr["Qty"].ToDecimal() * dr["Rate"].ToDecimal());
                dr["TotalAmt"] = (dr["QtyReq"].ToDecimal() * dr["Rate"].ToDecimal());
            }

            if (drCOLL.Length == 0)
            {
                dsrm.Tables[0].Rows.Add(dr);
            }

            dvgItemTran.BackgroundColor = Color.White;
            //CalcPayableAmt(ds.Tables[0]);
            //if (!String.IsNullOrEmpty(keyAmt))
            //{
            //    dr["Qty"] = int.Parse(dr["Qty"].ToString()) + (int.Parse(keyAmt) == 0 ? 1 : int.Parse(keyAmt));
            //    cQty = int.Parse(keyAmt);
            //}
            //else
            //{
            //    dr["Qty"] = int.Parse(dr["Qty"].ToString()) + 1;
            //    cQty = 1;
            //}
            dsrm.Tables[0].AcceptChanges();
            InitializeItems();
            CalculteGrandTotal(dsrm.Tables[0]);
            ddlItem.Focus();
        }

        private void lblExit_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void lblExit_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnAddRow_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnAddRow_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnSave_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnSave_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void dvgItemTran_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int Reply;
            int i;
            if (dvgItemTran.RowCount == 0)
                return;
            else
                i = dvgItemTran.CurrentRow.Index;

            if (dvgItemTran.Columns[e.ColumnIndex].Name == "Dimg")
            {
                Reply = Convert.ToInt32(MessageBox.Show("Want to Delete this Record?", "Delete Record", MessageBoxButtons.OKCancel, MessageBoxIcon.Question));
                if (Reply == 1)
                {
                    Reply = Convert.ToInt32(dvgItemTran.CurrentRow.Cells[3].Value);

                    DataRow[] drCOLL = dsrm.Tables[0].Select("fk_RMID = " + Reply);
                    DataRow dr;
                    dr = drCOLL[0];

                    if (drCOLL.Length != 0)
                    {
                        dsrm.Tables[0].Rows.Remove(dr);
                    }
                    dsrm.Tables[0].AcceptChanges();
                    dvgItemTran.Columns.Remove("Dimg");
                    dvgItemTran.Columns.Remove("Eimg");

                    foreach (DataGridViewRow row in dvgItemTran.Rows)
                    {
                        row.Height = 25;
                    }
                    return;
                }
            }

            if (dvgItemTran.Columns[e.ColumnIndex].Name == "Eimg")
            {
                ItemTranID = dvgItemTran.CurrentRow.Cells[0].Value.ToInt();
                txtQty.Text = dvgItemTran.CurrentRow.Cells[5].Value.ToString();
                txtUnit.Text = dvgItemTran.CurrentRow.Cells[6].Value.ToString();
                ddlItem.SelectedValue = dvgItemTran.CurrentRow.Cells[3].Value.ToInt();
                txtRate.Text = dvgItemTran.CurrentRow.Cells[7].Value.ToString();
                ddlItem.Enabled = false;
                //Inventory.AddEditInvTran rmg = new Inventory.AddEditInvTran(Convert.ToInt32(dvgItemTran.CurrentRow.Cells[0].Value));
                //this.Hide();

                //if (rmg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                //{
                //    this.Hide();
                //}
                //else
                //{
                //    this.Show();
                //    dvgItemTran.Columns.Remove("Dimg");
                //    dvgItemTran.Columns.Remove("Eimg");

                //    foreach (DataGridViewRow row in dvgItemTran.Rows)
                //    {
                //        row.Height = 25;
                //    }
                //}
            }
        }

        private void ddlItem_KeyUp(object sender, KeyEventArgs e)
        {
            ddlItem.DroppedDown = false;
        }

        private void ddlItem_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            if (ddlItem.SelectedValue.ToInt() != 0)
            {
                if (ddlItemType.SelectedIndex == 0)
                {
                    ddlItem.BackColor = Color.White;
                    InventoryBLL oinvbll = new InventoryBLL();
                    InventoryEntity oinv = (InventoryEntity)oinvbll.GetRMForEdit(ddlItem.SelectedValue.ToInt(), GeneralFunctions.LoginInfo.UserID);
                    txtUnit.Text = oinv.RMUOM.ToString();
                    txtRate.Text = oinv.RMRate.ToString();
                    txtQty.Text = "0";
                    txtQty.Focus();
                }
                else
                {
                    ddlItem.BackColor = Color.White;
                    InventoryBLL oinvbll = new InventoryBLL();
                    InventoryEntity oinv = (InventoryEntity)oinvbll.GetSFForEdit(ddlItem.SelectedValue.ToInt(), GeneralFunctions.LoginInfo.UserID);
                    txtUnit.Text = oinv.RecipeUnit.ToString();
                    txtRate.Text = oinv.RMRate.ToString();
                    txtQty.Text = "0";
                    txtQty.Focus();
                }
            }
        }

        private void txtQty_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtRate_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtPlates_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        #region "Private Methods"

        private void Initialize()
        {
            txtRecipeUnit.Text = string.Empty;
            txtNarration.Text = string.Empty;
            dtTranDate.Value = DateTime.Now;
            ddlType.SelectedIndex = 0;
            ddlType.Enabled = true;
            ddlFGItem.SelectedValue = "0";

            ddlItem.SelectedValue = "0";
            txtRate.Text = "";
            txtQty.Text = "";
            txtUnit.Text = "";
            txtTotal.Text = "";
            LoadTran();
        }

        private bool ValidateEntry()
        {
            bool RetVal = true;

            if (dtTranDate.Value.Date > DateTime.Today)
            {
                RetVal = false;
            }
            

            if (ddlType.SelectedValue.ToString() == "0")
                ddlType.BackColor = Color.Red;
            else
                ddlType.BackColor = Color.White;

            if (dvgItemTran.Rows.Count == 0)
            {
                RetVal = false;
                dvgItemTran.BackgroundColor = Color.Red;
            }
            else
                dvgItemTran.BackgroundColor = Color.White;
            return RetVal;
        }

        private void fillItem(int GroupID)
        {
            InventoryBLL ven = new InventoryBLL();
            InventoryEntity oven = (InventoryEntity)ven.GetRecipeForEdit(GroupID, GeneralFunctions.LoginInfo.UserID);

            txtNarration.Text = oven.RecipeDetail;
            txtRecipeUnit.Text = oven.RecipeUnit;
            txtRecipeName.Text = oven.RecipeName;
            ddlType.SelectedValue = oven.TranType;
            ddlFGItem.SelectedValue = oven.fk_FGItemID.ToString();
            dtTranDate.Value = oven.TranDate;
            txtPlates.Text = oven.NoofPlates.ToString();
            ddlType.Enabled = false;
            ddlFGItem.Enabled = false;
            
            if (GeneralFunctions.LoginInfo.UserRole == 2 || (GeneralFunctions.LoginInfo.UserRole == 4))
                btnSave.Visible = false;
        }

        private void LoadDDL()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            Dictionary<string, string> cStatus = new Dictionary<string, string>();
            cStatus.Add("F", "Finished");
            cStatus.Add("S", "Semi Finished");

            ddlType.DataSource = new BindingSource(cStatus, null);
            ddlType.DisplayMember = "Value";
            ddlType.ValueMember = "Key";
            ddlType.SelectedIndex = -1;


            Dictionary<string, string> cStatus1 = new Dictionary<string, string>();
            cStatus1.Add("R", "Raw Material");
            cStatus1.Add("S", "Semi Finished");
            ddlItemType.DataSource = new BindingSource(cStatus1, null);
            ddlItemType.DisplayMember = "Value";
            ddlItemType.ValueMember = "Key";
            ddlItemType.SelectedIndex = -1;
        }

        private void CalculateTotal()
        {
            //txtRate.Text = txtRate.Text.ToDecimal().ToString("n2");
            //txtQty.Text = txtQty.Text.ToDecimal().ToString("n3");
            //txtTax.Text = txtTax.Text.ToDecimal().ToString("n2");
            if (txtRate.Text != string.Empty && txtQty.Text != string.Empty)
            {
                txtTotal.Text = (txtRate.Text.ToDecimal() * txtQty.Text.ToDecimal()).ToString("n2");
            }
        }

        private void InitializeItems()
        {
            ddlItem.SelectedValue = "0";
            txtQty.Text = "";
            txtRate.Text = "";
            txtTotal.Text = "";
            //ddlItem.Enabled = true;
        }

        private void CalculteGrandTotal(DataTable dtItem)
        {
            decimal amt = 0;
            for (int i = 0; i < dtItem.Rows.Count; i++)
            {
                DataRow dr = dtItem.Rows[i];
                amt += (dr["QtyReq"].ToDecimal() * dr["ItemRate"].ToDecimal());
            }
            txtRateValue.Text = amt.ToString("########0.00");
        }

        #endregion

        #region Item Entry
        private void LoadTran()
        {
            string path = Application.StartupPath + "\\";

            DataGridViewCellStyle style = new DataGridViewCellStyle();
            dvgItemTran.EnableHeadersVisualStyles = false;
            style.ForeColor = Color.Blue;

            dvgItemTran.AutoGenerateColumns = false;

            dvgItemTran.Columns[0].Name = "pk_RecipeDtlID";
            dvgItemTran.Columns[1].Name = "fk_RecipeID";
            dvgItemTran.Columns[2].Name = "ItemType";
            dvgItemTran.Columns[3].Name = "fk_RMID";
            dvgItemTran.Columns[4].Name = "Item Used";
            dvgItemTran.Columns[5].Name = "UOM";
            dvgItemTran.Columns[6].Name = "QtyReq";
            dvgItemTran.Columns[7].Name = "ItemRate";
            dvgItemTran.Columns[8].Name = "TotalAmt";

            dvgItemTran.Columns[0].HeaderText = "pk_RecipeDtlID";
            dvgItemTran.Columns[1].HeaderText = "fk_RecipeID";
            dvgItemTran.Columns[2].HeaderText = "Type";
            dvgItemTran.Columns[3].HeaderText = "fk_RMID";
            dvgItemTran.Columns[4].HeaderText = "Item Used";
            dvgItemTran.Columns[5].HeaderText = "UOM";
            dvgItemTran.Columns[6].HeaderText = "Qty Req";
            dvgItemTran.Columns[7].HeaderText = "Rate";
            dvgItemTran.Columns[8].HeaderText = "Total";

            dvgItemTran.Columns[0].DataPropertyName = "pk_RecipeDtlID";
            dvgItemTran.Columns[1].DataPropertyName = "fk_RecipeID";
            dvgItemTran.Columns[2].DataPropertyName = "ItemType";
            dvgItemTran.Columns[3].DataPropertyName = "fk_RMID";
            dvgItemTran.Columns[4].DataPropertyName = "ItemName";
            dvgItemTran.Columns[5].DataPropertyName = "UOM";
            dvgItemTran.Columns[6].DataPropertyName = "QtyReq";
            dvgItemTran.Columns[7].DataPropertyName = "ItemRate";
            dvgItemTran.Columns[8].DataPropertyName = "TotalAmt";

            dvgItemTran.Columns[2].Width = 90;
            dvgItemTran.Columns[4].Width = 286;
            dvgItemTran.Columns[5].Width = 58;
            dvgItemTran.Columns[6].Width = 75;
            dvgItemTran.Columns[7].Width = 59;
            dvgItemTran.Columns[8].Width = 86;
      
            dvgItemTran.Columns[0].Visible = false;
            dvgItemTran.Columns[1].Visible = false;
            //dvgItemTran.Columns[2].Visible = false;
            dvgItemTran.Columns[3].Visible = false;

            dvgItemTran.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dvgItemTran.Columns[7].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
            dvgItemTran.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dvgItemTran.Columns[6].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
            dvgItemTran.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dvgItemTran.Columns[8].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
            //dvgItemTran.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            //dvgItemTran.Columns[8].HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;

            DataGridViewImageColumn Editimg = new DataGridViewImageColumn();
            Image Editimage = Image.FromFile(path + ConfigurationManager.AppSettings["imageFolder"] + "edit-icon.png");
            Editimg.Image = Editimage;
            dvgItemTran.Columns.Add(Editimg);
            Editimg.HeaderText = "Edit";
            Editimg.Name = "Eimg";
            Editimg.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            Editimg.Width = 40;

            DataGridViewImageColumn Delimg = new DataGridViewImageColumn();
            Image Delimage = Image.FromFile(path + ConfigurationManager.AppSettings["imageFolder"] + "delete-icon.png");
            Delimg.Image = Delimage;
            dvgItemTran.Columns.Add(Delimg);
            Delimg.HeaderText = "Del";
            Delimg.Name = "Dimg";
            Delimg.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
            Delimg.Width = 40;

            dvgItemTran.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dvgItemTran.MultiSelect = false;
            dvgItemTran.Columns["QtyReq"].DefaultCellStyle.Format = "#######0.000";
            dvgItemTran.Columns["ItemRate"].DefaultCellStyle.Format = "#######0.00";
            dvgItemTran.Columns["TotalAmt"].DefaultCellStyle.Format = "#######0.00";

            InventoryBLL invtbll = new InventoryBLL();
            dsrm = invtbll.GetAllRecipeItem(LID);
            DataView view1 = new DataView(dsrm.Tables[0]);

            source1.DataSource = view1;

            dvgItemTran.DataSource = source1;
            dvgItemTran.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None;
            dvgItemTran.AllowUserToResizeColumns = false;
            dvgItemTran.AllowUserToAddRows = false;
            dvgItemTran.ReadOnly = true;
            CalculteGrandTotal(dsrm.Tables[0]);

        }

        private void LoadRM()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            SearchCriteria searchcriteria = new SearchCriteria();
            searchcriteria.QueryStat = "L";

            ddlItem.DataSource = null;
            ddlItem.Items.Clear();

            InventoryBLL invtbll = new InventoryBLL();
            ds = invtbll.GetAllRM(searchcriteria);
            dt = ds.Tables[0];

            DataRow nullrow = dt.NewRow();
            nullrow["RMID"] = 0;
            nullrow["ItemName"] = "Select";
            dt.Rows.Add(nullrow);
            if (dt.Rows.Count > 0)
            {
                ddlItem.DataSource = dt;
                ddlItem.DisplayMember = "ItemName";
                ddlItem.ValueMember = "RMID";
                ddlItem.SelectedValue = "0";
            }


            ddlItem.AutoCompleteMode = AutoCompleteMode.Suggest;
            ddlItem.AutoCompleteSource = AutoCompleteSource.ListItems;
            // Load data in comboBox => cboComboBox1.DataSource = .....
            // Other things
        }

        private void LoadFG()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            //SearchCriteria searchcriteria = new SearchCriteria();
            //searchcriteria.QueryStat = "L";

            ddlFGItem.DataSource = null;
            ddlFGItem.Items.Clear();

            InventoryBLL invtbll = new InventoryBLL();
            ItemBLL itm = new ItemBLL();
            SearchCriteria scr = new SearchCriteria();
            ds = itm.GetAllItems(scr);
  
            //ds = FoodSmart.BLL.itemService.
            //ds = invtbll.GetAllRM(searchcriteria);

            dt = ds.Tables[0];

            DataRow nullrow = dt.NewRow();
            nullrow["ItemID"] = 0;
            nullrow["ItemDescr"] = "Select";
            dt.Rows.Add(nullrow);
            if (dt.Rows.Count > 0)
            {
                ddlFGItem.DataSource = dt;
                ddlFGItem.DisplayMember = "ItemDescr";
                ddlFGItem.ValueMember = "ItemID";
                ddlFGItem.SelectedValue = "0";
            }
            ddlFGItem.AutoCompleteMode = AutoCompleteMode.Suggest;
            ddlFGItem.AutoCompleteSource = AutoCompleteSource.ListItems;
            // Load data in comboBox => cboComboBox1.DataSource = .....
            // Other things
        }

        private void LoadSF()
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            ddlItem.DataSource = null;
            ddlItem.Items.Clear();

            InventoryBLL invtbll = new InventoryBLL();
            ds = invtbll.GetAllSF();
            dt = ds.Tables[0];

            DataRow nullrow = dt.NewRow();
            nullrow["RMID"] = 0;
            nullrow["ItemName"] = "Select";
            dt.Rows.Add(nullrow);
            if (dt.Rows.Count > 0)
            {
                ddlItem.DataSource = dt;
                ddlItem.DisplayMember = "ItemName";
                ddlItem.ValueMember = "RMID";
                ddlItem.SelectedValue = "0";
            }


            ddlItem.AutoCompleteMode = AutoCompleteMode.Suggest;
            ddlItem.AutoCompleteSource = AutoCompleteSource.ListItems;
            // Load data in comboBox => cboComboBox1.DataSource = .....
            // Other things
        }
        #endregion

        private void ddlItemType_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.ddlItem.SelectedIndexChanged -= new System.EventHandler(this.ddlItem_SelectedIndexChanged_1);
            if (ddlItemType.SelectedIndex == 0)
            {
                LoadRM();
                ddlItem.Enabled = true;
            }
            else if (ddlItemType.SelectedIndex == 1)
            {
                LoadSF();
                ddlItem.Enabled = true;
            }
            this.ddlItem.SelectedIndexChanged += new System.EventHandler(this.ddlItem_SelectedIndexChanged_1);
        }

        private void ddlFGItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            ItemBLL oItembll = new ItemBLL();

            ItemsEntity oitem = (ItemsEntity)oItembll.GetItemForEdit(ddlFGItem.SelectedValue.ToInt());

            txtRecipeUnit.Text = oitem.UOM;
        }

        private void txtQty_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtQty.Text) && !string.IsNullOrEmpty(txtRate.Text))
            {
                txtTotal.Text = (txtQty.Text.ToDecimal() * txtRate.Text.ToDecimal()).ToString("########0.00");
            }
        }


    }
}
