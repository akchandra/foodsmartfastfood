﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FoodSmart.Common;
using FoodSmart.DAL.DbManager;
using FoodSmart.Entity;
using FoodSmart.Utilities;

namespace FoodSmart.DAL
{
     public sealed class OrderDAL
    {
        public static List<OrderEntity> GetAllOrders(SearchCriteria criteria)
        {
            string strExecution = "[dbo].[sp_ManageOrder]";
            List<OrderEntity> transactions = new List<OrderEntity>();
            SearchCriteria searchCriteria = new SearchCriteria();

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@Mode", 1, "L");
                //oDq.AddIntegerParam("@fk_FinYearId", criteria.FinancialYearId);
                oDq.AddVarcharParam("@OrderNo", 100, criteria.OrderNo);
                oDq.AddIntegerParam("@fk_LocID", criteria.LocID);
                oDq.AddVarcharParam("@OrderType", 1, criteria.Type);
                oDq.AddIntegerParam("@fk_UserID", GeneralFunctions.LoginInfo.UserID);
                oDq.AddIntegerParam("@Result", 0);
                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    var transaction = new OrderEntity(reader);
                    transactions.Add(transaction);
                }
                reader.Close();
            }
            return transactions;
        }

        public static OrderEntity GetOrder(int id)
        {
            string strExecution = "[dbo].[sp_ManageOrder]";
            OrderEntity transaction = null;

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@Mode", 1, "R");
                oDq.AddIntegerParam("@pk_OrderID", 0);
                oDq.AddIntegerParam("@fk_LocID", id);
                oDq.AddIntegerParam("@fk_UserID", GeneralFunctions.LoginInfo.UserID);
                oDq.AddIntegerParam("@Result", 0);
                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    transaction = new OrderEntity(reader);
                }

                reader.Close();
            }
            return transaction;
        }

        public static List<IOrderItem> GetAllOrderItems(int tranId)
        {
            string strExecution = "[dbo].[sp_GetOrderItems]";
            List<IOrderItem> items = new List<IOrderItem>();

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddCharParam("@Mode", 1, 'I');
                oDq.AddBigIntegerParam("@fk_OrderID", tranId);
                oDq.AddIntegerParam("@Result", 0);
                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    var item = new OrderItemEntity(reader);
                    items.Add(item);
                }
                reader.Close();
            }
            return items;
        }

        public static int SaveOrders(IOrder ord, string mode, DataSet ds)
        {
            string strExecution = "[dbo].[sp_ManageOrder]";
            int result = 0;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                //ds.Tables[0].Columns.Remove("pk_OrderItemID");
                ds.Tables[0].Columns.Remove("ItemTypeDesc");
                ds.Tables[0].Columns.Remove("Item");
                ds.Tables[0].Columns.Remove("Total");

                oDq.AddVarcharParam("@Mode", 1, mode);
                //passing  table as parameter
                oDq.AddTableValueParam("@MParam", ds.Tables[0], QueryParameterDirection.Input);
                oDq.AddBigIntegerParam("@pk_OrderID", ord.Id);
                oDq.AddVarcharParam("@OrderType", 1, ord.OrderType);
                oDq.AddVarcharParam("@OrderNo", 20, ord.OrderRef);
                oDq.AddDateTimeParam("@OrderDate", ord.OrderDate);
                oDq.AddVarcharParam("@Narration", 20, ord.Narration);
                oDq.AddIntegerParam("@fk_LocID", ord.LocID);
                oDq.AddBooleanParam("@Rstatus", ord.Status);
                oDq.AddIntegerParam("@fk_UserID", GeneralFunctions.LoginInfo.UserID);
                oDq.AddIntegerParam("@Result", result, QueryParameterDirection.Output);
                oDq.RunActionQuery();
                result = Convert.ToInt32(oDq.GetParaValue("@Result"));
            }
            return result;
        }

        public static int DeleteOrder(int id)
        {
            string strExecution = "[dbo].[sp_ManageOrder]";
            int ret = 0;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@Mode", 1, "D");
                oDq.AddIntegerParam("@pk_OrderID", id);
                oDq.AddBigIntegerParam("@fk_UserID", GeneralFunctions.LoginInfo.UserID);
                oDq.AddIntegerParam("@Result", 0);
                ret = Convert.ToInt32(oDq.GetScalar());
            }
            return ret;
        }

        public static string GetStandardOrderForLoc(int id)
        {
            string strExecution = "[dbo].[sp_ManageOrder]";
            string ret = string.Empty;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@Mode", 1, "C");
                oDq.AddIntegerParam("@pk_OrderID", id);
                oDq.AddBigIntegerParam("@fk_UserID", GeneralFunctions.LoginInfo.UserID);
                oDq.AddIntegerParam("@Result", 0);
                if (Convert.ToInt32(oDq.GetScalar()) == 0)
                    ret = "A";
                else
                    ret = "E";
            }
            return ret;
        }
    }
}
