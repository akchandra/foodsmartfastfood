﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FoodSmart.Common;
using FoodSmart.DAL.DbManager;
using FoodSmart.Entity;
using FoodSmart.Utilities;

namespace FoodSmart.DAL
{
    public sealed class TransactionDAL
    {
        public static List<TransactionEntity> GetAllTransactions(SearchCriteria criteria, int UserID)
        {
            string strExecution = "[inv].[sp_ManageInvTran]";
            List<TransactionEntity> transactions = new List<TransactionEntity>();
            SearchCriteria searchCriteria = new SearchCriteria();

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@Mode", 1, "L");
                oDq.AddIntegerParam("@fk_FinYearId", criteria.FinancialYearId);
                oDq.AddVarcharParam("@RefDocNo", 100, criteria.VendorName);
                oDq.AddIntegerParam("@fk_FromStoreID", 0);
                oDq.AddIntegerParam("@fk_ToStoreID", 0);
                oDq.AddDateTimeParam("@TranDate", criteria.Date);
                oDq.AddVarcharParam("@TranType", 1, criteria.Type);
                oDq.AddIntegerParam("@fk_UserID", UserID);
                oDq.AddIntegerParam("@Result", 0);
                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    var transaction = new TransactionEntity(reader);
                    transactions.Add(transaction);
                }
                reader.Close();
            }
            return transactions;
        }

        public static TransactionEntity GetTransaction(Int64 id, int UserID)
        {
            string strExecution = "[inv].[sp_ManageInvTran]";
            TransactionEntity transaction = null;

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@Mode", 1, "M");
                oDq.AddBigIntegerParam("@pk_TranID", id);
                oDq.AddIntegerParam("@fk_FromStoreID", 0);
                oDq.AddIntegerParam("@fk_ToStoreID", 0);
                oDq.AddIntegerParam("@fk_UserID", UserID);
                oDq.AddIntegerParam("@Result", 0);
                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    transaction = new TransactionEntity(reader);
                }

                reader.Close();
            }
            return transaction;
        }

        public static int DeleteTransaction(Int64 id)
        {
            string strExecution = "[inv].[sp_ManageInvTran]";
            int ret = 0;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@Mode", 1, "D");
                oDq.AddBigIntegerParam("@pk_TranID", id);
                oDq.AddBigIntegerParam("@fk_UserID", GeneralFunctions.LoginInfo.UserID);
                oDq.AddIntegerParam("@Result", 0);
                ret = Convert.ToInt32(oDq.GetScalar());
            }
            return ret;
        }

        public static int SaveTransaction(ITransaction transaction, string mode, DataSet ds, int UserID)
        {
            string strExecution = "[inv].[sp_ManageInvTran]";
            int result = 0;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@Mode", 1, mode);
                //passing  table as parameter
                oDq.AddTableValueParam("@MParam", ds.Tables[0], QueryParameterDirection.Input);
                oDq.AddBigIntegerParam("@pk_TranID", transaction.Id);
                oDq.AddIntegerParam("@fk_VendorID", transaction.VendorId);
                oDq.AddVarcharParam("@TranType", 1, transaction.TranType);
                oDq.AddVarcharParam("@TranNo", 20, transaction.TranNo);
                oDq.AddDateTimeParam("@TranDate", transaction.TranDate);
                oDq.AddVarcharParam("@RefDocNo", 20, transaction.RefDoc);
                oDq.AddDateTimeParam("@RefDocDate", transaction.RefDate);
                oDq.AddDecimalParam("@TotQty", 10, 3, transaction.TranQty);
                oDq.AddIntegerParam("@fk_FromStoreID", transaction.FromLocID);
                oDq.AddIntegerParam("@fk_ToStoreID", transaction.ToLocID);
                oDq.AddVarcharParam("@Narration", 200, transaction.Narration);
                oDq.AddIntegerParam("@fk_FinYearId", transaction.FinYearID);
                oDq.AddVarcharParam("@VendorName", 100, transaction.VendorName);
                oDq.AddBooleanParam("@Rstatus", transaction.Status);
                oDq.AddIntegerParam("@fk_UserID", UserID);
                oDq.AddIntegerParam("@Result", result, QueryParameterDirection.Output);
                oDq.RunActionQuery();
                result = Convert.ToInt32(oDq.GetParaValue("@Result"));

                //oDq.AddVarcharParam("@Mode", 1, mode);
                ////passing  table as parameter
                //oDq.AddTableValueParam("@MParam", ds.Tables[0], QueryParameterDirection.Input);
                //oDq.AddBigIntegerParam("@pk_TranID", transaction.Id);
                //oDq.AddIntegerParam("@fk_VendorID", transaction.VendorId);
                //oDq.AddVarcharParam("@TranType", 1, transaction.TranType);
                //oDq.AddVarcharParam("@TranNo", 20, transaction.TranNo);
                //oDq.AddDateTimeParam("@TranDate", transaction.TranDate);
                //oDq.AddVarcharParam("@RefDocNo", 20, transaction.RefDoc);
                //oDq.AddDateTimeParam("@RefDocDate", transaction.RefDate);
                //oDq.AddDecimalParam("@TotQty", 10, 3, transaction.TranQty);
                //oDq.AddIntegerParam("@fk_FromLocID", transaction.FromLocID);
                //oDq.AddIntegerParam("@fk_ToLocID", transaction.ToLocID);
                //oDq.AddVarcharParam("@Narration", 200, transaction.Narration);
                //oDq.AddIntegerParam("@fk_FinYearId", transaction.FinYearID);
                //oDq.AddVarcharParam("@VendorName", 100, transaction.VendorName);
                //oDq.AddBooleanParam("@Rstatus", transaction.Status);
                //oDq.AddIntegerParam("@fk_UserID", UserID);
                //oDq.AddIntegerParam("@Result", result, QueryParameterDirection.Output);
                //oDq.RunActionQuery();
                //result = Convert.ToInt32(oDq.GetParaValue("@Result"));
            }
            return result;
        }

        public static List<ITransactionItem> GetAllTransactionItems(Int64 tranId)
        {
            string strExecution = "[inv].[sp_GetTranItems]";
            List<ITransactionItem> items = new List<ITransactionItem>();

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                //oDq.AddCharParam("@Mode", 1, 'I');
                oDq.AddBigIntegerParam("@fk_TranID", tranId);
                oDq.AddIntegerParam("@Result", 0);
                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    var item = new TransactionItemEntity(reader);
                    items.Add(item);
                }
                reader.Close();
            }
            return items;
        }

        public static List<ITransactionItem> GetAllTransactionItems(string tranNo)
        {
            string strExecution = "[inv].[sp_GetTranItems]";
            List<ITransactionItem> items = new List<ITransactionItem>();

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddCharParam("@Mode", 1, 'N');
                oDq.AddVarcharParam("@TranNo", 20, tranNo);
                oDq.AddIntegerParam("@Result", 0);
                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    var item = new TransactionItemEntity(reader);
                    items.Add(item);
                }
                reader.Close();
            }
            return items;
        }

        public static List<ILocation> GetLocations()
        {
            string strExecution = "[dbo].[GetLocations]";
            List<ILocation> lstLocation = new List<ILocation>();

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@RESULT", 0, QueryParameterDirection.Output);
                DataTableReader reader = oDq.GetTableReader();

                while (reader.Read())
                {
                    ILocation location = new LocationEntity();
                    location.LocID = Convert.ToInt32(reader["pk_LocID"]);
                    location.LocName = Convert.ToString(reader["LocationName"]);
                    lstLocation.Add(location);
                }
                reader.Close();
            }
            return lstLocation;
        }

        public static List<IVendor> GetVendors()
        {
            string strExecution = "[inv].[spGetVendor]";
            List<IVendor> lstVendor = new List<IVendor>();

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@VendorID", 0);
                //oDq.AddIntegerParam("@RESULT", 0, QueryParameterDirection.Output);
                DataTableReader reader = oDq.GetTableReader();

                while (reader.Read())
                {
                    IVendor vendor = new VendorEntity();
                    vendor.VendorID = Convert.ToInt32(reader["pk_VendorID"]);
                    vendor.VendorName = Convert.ToString(reader["VendorName"]);
                    lstVendor.Add(vendor);
                }

                reader.Close();
            }
            return lstVendor;
        }

        public static List<IStore> GetAllStore(string CalledFrom)
        {
            string strExecution = "[inv].[GetAllStores]";
            List<IStore> lstLocation = new List<IStore>();

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@CalledFrom", 1, CalledFrom);
                //oDq.AddIntegerParam("@RESULT", 0, QueryParameterDirection.Output);
                DataTableReader reader = oDq.GetTableReader();

                while (reader.Read())
                {
                    IStore Store = new StoreEntity();
                    Store.StoreID = Convert.ToInt32(reader["pk_StoreID"]);
                    Store.StoreName = Convert.ToString(reader["StoreName"]);
                    lstLocation.Add(Store);
                }

                reader.Close();
            }

            return lstLocation;
        }

        public static int DeleteTransaction(Int64 id, int UserID)
        {
            string strExecution = "[inv].[sp_ManageInvTran]";
            int ret = 0;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@Mode", 1, "D");
                oDq.AddBigIntegerParam("@pk_TranID", id);
                oDq.AddBigIntegerParam("@fk_UserID", UserID);
                oDq.AddIntegerParam("@Result", 0);
                ret = Convert.ToInt32(oDq.GetScalar());
            }
            return ret;
        }
    }
}
