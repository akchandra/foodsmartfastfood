﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FoodSmart.Common;
using FoodSmart.DAL.DbManager;
using FoodSmart.Entity;
using FoodSmart.Utilities;

namespace FoodSmart.DAL
{
    public sealed class InventoryDAL
    {
        #region Raw Material
        //public static DataSet GetAllRM(SearchCriteria searchCriteria)
        //{
        //    DataSet ds = new DataSet();
        //    using (DbQuery dq = new DbQuery("[rm].[sp_ManageRM]"))
        //    {
        //        dq.AddVarcharParam("@ItemName", 100, searchCriteria.ItemName);
        //        dq.AddVarcharParam("@GroupDescr", 100, searchCriteria.GroupName);
        //        dq.AddVarcharParam("@Mode", 1, "L");
        //        dq.AddIntegerParam("@RESULT", 0, QueryParameterDirection.Output);
        //        ds = dq.GetTables();
        //    }
        //    return ds;
        //}

        //public static int SaveRM(IInventory inv, string mode, int userid)
        //{
        //    string strExecution = "[rm].[sp_ManageRM]";
        //    int result = 0;
        //    using (DbQuery oDq = new DbQuery(strExecution))
        //    {
        //        oDq.AddIntegerParam("@pk_RMID", inv.RMID);
        //        oDq.AddIntegerParam("@fk_RMGroupID", inv.GroupID);
        //        oDq.AddVarcharParam("@Mode", 1, mode);
        //        oDq.AddVarcharParam("@ItemCode", 50, inv.RMCode);
        //        oDq.AddVarcharParam("@ItemName", 50, inv.RMName);
        //        oDq.AddVarcharParam("@UOM", 10, inv.RMUOM);
        //        oDq.AddDecimalParam("@BalanceQty", 12, 2, inv.BalanceQty);
        //        oDq.AddDecimalParam("@CurrentRate", 12, 2, inv.RMRate);
        //        oDq.AddIntegerParam("@UserID", userid);
        //        oDq.AddIntegerParam("@Result", result, QueryParameterDirection.Output);
        //        oDq.RunActionQuery();
        //        result = Convert.ToInt32(oDq.GetParaValue("@Result"));
        //    }
        //    return result;
        //}

        //public static IInventory GetRMForEdit(int ID, string CalledFrom)
        //{
        //    string strExecution = "[rm].[sp_ManageRM]";
        //    IInventory oLoc = null;
        //    using (DbQuery oDq = new DbQuery(strExecution))
        //    {
        //        oDq.AddIntegerParam("@pk_RMID", ID);
        //        oDq.AddVarcharParam("@Mode", 1, CalledFrom);
        //        //oDq.AddIntegerParam("@UserID", UserID);
        //        oDq.AddIntegerParam("@Result", 0);
        //        DataTableReader reader = oDq.GetTableReader();

        //        while (reader.Read())
        //        {
        //            oLoc = new InventoryEntity(reader);
        //        }
        //        reader.Close();
        //    }
        //    return oLoc;
        //}

        //public static int DeleteRM(int RMID)
        //{
        //    string strExecution = "[rm].[sp_ManageRM]";
        //    int ret = 0;
        //    using (DbQuery oDq = new DbQuery(strExecution))
        //    {
        //        oDq.AddIntegerParam("@pk_RMID", RMID);
        //        oDq.AddVarcharParam("@Mode", 1, "D");
        //        oDq.AddBigIntegerParam("@UserID", GeneralFunctions.LoginInfo.UserID);
        //        oDq.AddIntegerParam("@Result", 0);
        //        ret = Convert.ToInt32(oDq.GetScalar());
        //    }
        //    return ret;
        //}
        #endregion

        #region Raw Material Group
        //public static DataSet GetAllRMGroup(SearchCriteria searchCriteria)
        //{
        //    DataSet ds = new DataSet();
        //    using (DbQuery dq = new DbQuery("[rm].[sp_ManageRMGroup]"))
        //    {
        //        dq.AddVarcharParam("@GroupDescr", 100, searchCriteria.GroupName);
        //        dq.AddVarcharParam("@Mode", 1, "L");
        //        dq.AddIntegerParam("@RESULT", 0, QueryParameterDirection.Output);
        //        ds = dq.GetTables();
        //    }
        //    return ds;
        //}

        //public static int SaveRMGroup(IInventory inv, string mode)
        //{
        //    string strExecution = "[rm].[sp_ManageRMGroup]";
        //    int result = 0;
        //    using (DbQuery oDq = new DbQuery(strExecution))
        //    {
        //        oDq.AddIntegerParam("@pk_RMGroupID", inv.GroupID);
        //        oDq.AddVarcharParam("@Mode", 1, mode);
        //        oDq.AddVarcharParam("@GroupDescr", 50, inv.RMGroupName);
        //        oDq.AddVarcharParam("@GroupType", 10, inv.TranType);
        //        oDq.AddVarcharParam("@GroupInitials", 2, inv.GroupInitial);
        //        oDq.AddIntegerParam("@UserID", GeneralFunctions.LoginInfo.UserID);
        //        oDq.AddIntegerParam("@Result", result, QueryParameterDirection.Output);
        //        oDq.RunActionQuery();
        //        result = Convert.ToInt32(oDq.GetParaValue("@Result"));
        //    }
        //    return result;
        //}

        //public static IInventory GetRMGroupForEdit(int ID, string CalledFrom)
        //{
        //    string strExecution = "[rm].[sp_ManageRMGroup]";
        //    IInventory oLoc = null;
        //    using (DbQuery oDq = new DbQuery(strExecution))
        //    {
        //        oDq.AddIntegerParam("@pk_RMGroupID", ID);
        //        oDq.AddVarcharParam("@Mode", 1, CalledFrom);
        //        //oDq.AddIntegerParam("@UserID", UserID);
        //        oDq.AddIntegerParam("@Result", 0);
        //        DataTableReader reader = oDq.GetTableReader();

        //        while (reader.Read())
        //        {
        //            oLoc = new InventoryEntity(reader);
        //        }
        //        reader.Close();
        //    }
        //    return oLoc;
        //}

        //public static int DeleteRMGroup(int GroupID)
        //{
        //    string strExecution = "[rm].[sp_ManageRMGroup]";
        //    int ret = 0;
        //    using (DbQuery oDq = new DbQuery(strExecution))
        //    {
        //        oDq.AddIntegerParam("@pk_RMGroupID", GroupID);
        //        oDq.AddVarcharParam("@Mode", 1, "D");
        //        oDq.AddBigIntegerParam("@UserID", GeneralFunctions.LoginInfo.UserID);
        //        oDq.AddIntegerParam("@Result", 0);
        //        ret = Convert.ToInt32(oDq.GetScalar());
        //    }
        //    return ret;
        //}
        #endregion

        #region Cost Center
        //public static DataSet GetAllCC(SearchCriteria searchCriteria)
        //{
        //    DataSet ds = new DataSet();
        //    using (DbQuery dq = new DbQuery("[rm].[sp_ManageCostCenter]"))
        //    {
        //        //dq.AddVarcharParam("@CCDescr", 100, searchCriteria.CCName);
        //        dq.AddVarcharParam("@Mode", 1, "L");
        //        dq.AddIntegerParam("@RESULT", 0, QueryParameterDirection.Output);
        //        ds = dq.GetTables();
        //    }
        //    return ds;
        //}

        //public static int SaveCC(IInventory inv, string mode)
        //{
        //    string strExecution = "[rm].[sp_ManageCostCenter]";
        //    int result = 0;
        //    using (DbQuery oDq = new DbQuery(strExecution))
        //    {
        //        oDq.AddIntegerParam("@pk_CCID", inv.CCID);
        //        oDq.AddVarcharParam("@Mode", 1, mode);
        //        oDq.AddVarcharParam("@CCDescr", 50, inv.CostCenterName);
        //        oDq.AddIntegerParam("@UserID", GeneralFunctions.LoginInfo.UserID);
        //        oDq.AddIntegerParam("@Result", result, QueryParameterDirection.Output);
        //        oDq.RunActionQuery();
        //        result = Convert.ToInt32(oDq.GetParaValue("@Result"));
        //    }
        //    return result;
        //}

        //public static InventoryEntity GetCCForEdit(int ID, string CalledFrom)
        //{
        //    string strExecution = "[rm].[sp_ManageCostCenter]";
        //    InventoryEntity oLoc = null;
        //    using (DbQuery oDq = new DbQuery(strExecution))
        //    {
        //        oDq.AddIntegerParam("@pk_CCID", ID);
        //        oDq.AddVarcharParam("@Mode", 1, CalledFrom);
        //        //oDq.AddIntegerParam("@UserID", UserID);
        //        oDq.AddIntegerParam("@Result", 0);
        //        DataTableReader reader = oDq.GetTableReader();

        //        while (reader.Read())
        //        {
        //            oLoc = new InventoryEntity(reader);
        //        }
        //        reader.Close();
        //    }
        //    return oLoc;
        //}

        //public static int DeleteCC(int CCID)
        //{
        //    string strExecution = "[rm].[sp_ManageCostCenter]";
        //    int ret = 0;
        //    using (DbQuery oDq = new DbQuery(strExecution))
        //    {
        //        oDq.AddIntegerParam("@pk_CCID", CCID);
        //        oDq.AddVarcharParam("@Mode", 1, "D");
        //        oDq.AddBigIntegerParam("@UserID", GeneralFunctions.LoginInfo.UserID);
        //        oDq.AddIntegerParam("@Result", 0);
        //        ret = Convert.ToInt32(oDq.GetScalar());
        //    }
        //    return ret;
        //}
        #endregion

        #region Inventory Transaction
        public static DataSet GetAllInvTran(SearchCriteria searchCriteria, int UserID)
        {
            DataSet ds = new DataSet();
            using (DbQuery dq = new DbQuery("[sp_ManageInvTran]"))
            {
                dq.AddVarcharParam("@TranNo", 100, searchCriteria.StringOption1);
                dq.AddDateTimeParam("@TranDate", searchCriteria.Date);
                dq.AddVarcharParam("@TranType", 100, searchCriteria.BillType);
                //dq.AddVarcharParam("@CCName", 100, searchCriteria.CCName);
                //dq.AddVarcharParam("@VendorName", 100, searchCriteria.StringOption2);
                dq.AddVarcharParam("@Mode", 1, "L");
                dq.AddIntegerParam("@fk_UserID", UserID);
                dq.AddIntegerParam("@fk_FinYearId", searchCriteria.IntegerOption3);
                dq.AddIntegerParam("@RESULT", 0, QueryParameterDirection.Output);
                ds = dq.GetTables();
            }
            return ds;
        }

        public static int SaveInvTran(IInvTran inv, string mode, DataSet ds)
        {
            string strExecution = "[sp_ManageInvTran]";
            int result = 0;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                ds.Tables[0].Columns.Remove("RMName");
                ds.Tables[0].Columns.Remove("TotalAmt");

                oDq.AddIntegerParam("@pk_RMTranID", inv.TranID);
                //oDq.AddIntegerParam("@fk_CCID", inv.CCID);
                oDq.AddIntegerParam("@fk_VendorID", inv.VendorId);
                oDq.AddIntegerParam("@fk_FinYearId", GeneralFunctions.LoginInfo.CurrFinYearID);
                oDq.AddVarcharParam("@Mode", 1, mode);
                oDq.AddVarcharParam("@TranType", 1, inv.TranType);
                oDq.AddVarcharParam("@TranNo", 50, inv.TranNo);
                oDq.AddDateTimeParam("@TranDate", inv.TranDate);
                oDq.AddVarcharParam("@RefDocNo", 50, inv.RefDoc);
                //oDq.AddDateTimeParam("@RefDocDate", inv.RefDate);
                oDq.AddDecimalParam("@TotQty", 12, 2, inv.TranQty);
                oDq.AddVarcharParam("@Narration", 300, inv.Narration);
                //oDq.AddDecimalParam("@CurrentRate", 12, 2, inv.RMRate);
                oDq.AddIntegerParam("@fk_UserID", inv.UserID);
                oDq.AddIntegerParam("@fk_FromLocID", inv.FromLocID);
                oDq.AddIntegerParam("@fk_ToLocID", inv.ToLocID);
                oDq.AddIntegerParam("@Result", result, QueryParameterDirection.Output);
                oDq.AddTableValueParam("@MParam", ds.Tables[0], QueryParameterDirection.Input);
                oDq.RunActionQuery();
                result = Convert.ToInt32(oDq.GetParaValue("@Result"));
            }
            return result;
        }

        public static InventoryEntity GetInvTranForEdit(int ID, string CalledFrom)
        {
            string strExecution = "[sp_ManageInvTran]";
            InventoryEntity oLoc = null;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@pk_TranID", ID);
                oDq.AddVarcharParam("@Mode", 1, CalledFrom);
                //oDq.AddIntegerParam("@fk_UserID", GeneralFunctions.LoginInfo.UserID);
                oDq.AddIntegerParam("@Result", 0);
                DataTableReader reader = oDq.GetTableReader();

                while (reader.Read())
                {
                    oLoc = new InventoryEntity(reader);
                }
                reader.Close();
            }
            return oLoc;
        }

        public static int DeleteInvTran(int RMTranID, int UserID)
        {
            string strExecution = "[sp_ManageInvTran]";
            int ret = 0;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@pk_RMTranID", RMTranID);
                oDq.AddVarcharParam("@Mode", 1, "D");
                oDq.AddBigIntegerParam("@fk_UserID", UserID);
                oDq.AddIntegerParam("@Result", 0);
                ret = Convert.ToInt32(oDq.GetScalar());
            }
            return ret;
        }

        public static DataSet GetAllInvItem(int TranID)
        {
            DataSet ds = new DataSet();
            using (DbQuery dq = new DbQuery("[sp_GetTranItems]"))
            {
                dq.AddIntegerParam("@fk_TranID", TranID);
                dq.AddIntegerParam("@RESULT", 0, QueryParameterDirection.Output);
                ds = dq.GetTables();
            }
            return ds;
        }
        #endregion

        #region Recipe

        public static DataSet GetAllRecipe(SearchCriteria searchCriteria)
        {
            DataSet ds = new DataSet();
            using (DbQuery dq = new DbQuery("[rm].[sp_ManageRecipe]"))
            {
                dq.AddIntegerParam("@pk_RecipeID", 0);
                dq.AddDateTimeParam("@DateIntroduced", searchCriteria.Date);
                dq.AddVarcharParam("@RecipeName", 100, searchCriteria.ItemName);
                dq.AddVarcharParam("@Mode", 1, "L");
                dq.AddIntegerParam("@fk_UserID", GeneralFunctions.LoginInfo.UserID);
                //dq.AddIntegerParam("@fk_FinYearId", searchCriteria.IntegerOption3);
                dq.AddIntegerParam("@RESULT", 0, QueryParameterDirection.Output);
                ds = dq.GetTables();
            }
            return ds;
        }

        public static int SaveRecipe(IInventory inv, string mode, DataSet ds)
        {
            string strExecution = "[rm].[sp_ManageRecipe]";
            int result = 0;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                ds.Tables[0].Columns.Remove("ItemName");
                ds.Tables[0].Columns.Remove("TotalAmt");
                //ds.Tables[0].Columns.Remove("TotalAmt");

                oDq.AddIntegerParam("@pk_RecipeID", inv.RecipeID);
                oDq.AddIntegerParam("@fk_ItemID", inv.CCID);
                //oDq.AddIntegerParam("@fk_VendorID", inv.VendorID);
                //oDq.AddIntegerParam("@fk_FinYearId", GeneralFunctions.LoginInfo.CurrFinYearID);
                oDq.AddVarcharParam("@Mode", 1, mode);
                oDq.AddVarcharParam("@RecipeType", 1, inv.TranType);
                oDq.AddDateTimeParam("@DateIntroduced", inv.TranDate);
                oDq.AddDecimalParam("@NoofPlates", 12, 2, inv.NoofPlates);
                oDq.AddDecimalParam("@TotalQty", 12, 3, inv.BalanceQty);
                oDq.AddVarcharParam("@RecipeName", 50, inv.RecipeName);
                oDq.AddVarcharParam("@RecipeDetail", 300, inv.RecipeDetail);
                oDq.AddVarcharParam("@UOM", 10, inv.RecipeUnit);
                //oDq.AddDecimalParam("@CurrentRate", 12, 2, inv.RMRate);
                oDq.AddIntegerParam("@fk_UserID", GeneralFunctions.LoginInfo.UserID);
                oDq.AddIntegerParam("@Result", result, QueryParameterDirection.Output);
                oDq.AddTableValueParam("@MParam", ds.Tables[0], QueryParameterDirection.Input);
                oDq.RunActionQuery();
                result = Convert.ToInt32(oDq.GetParaValue("@Result"));
            }
            return result;
        }

        public static DataSet GetAllRecipeItem(int TranID)
        {
            DataSet ds = new DataSet();
            using (DbQuery dq = new DbQuery("[rm].[sp_GetRecipeItems]"))
            {
                dq.AddIntegerParam("@fk_RecipeID", TranID);
                dq.AddIntegerParam("@RESULT", 0, QueryParameterDirection.Output);
                ds = dq.GetTables();
            }
            return ds;
        }

        public static DataSet GetAllSF()
        {
            DataSet ds = new DataSet();
            using (DbQuery dq = new DbQuery("[rm].[sp_GetOnlySF]"))
            {
                dq.AddIntegerParam("@RESULT", 0, QueryParameterDirection.Output);
                ds = dq.GetTables();
            }
            return ds;
        }

        public static DataSet GetAllFG()
        {
            DataSet ds = new DataSet();
            using (DbQuery dq = new DbQuery("[rm].[sp_ManageRM]"))
            {

                dq.AddVarcharParam("@Mode", 1, "L");
                dq.AddIntegerParam("@RESULT", 0, QueryParameterDirection.Output);
                ds = dq.GetTables();
            }
            return ds;
        }
        public static int DeleteRecipe(int RMTranID)
        {
            string strExecution = "[rm].[sp_ManageRecipe]";
            int ret = 0;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@pk_RecipeID", RMTranID);
                oDq.AddVarcharParam("@Mode", 1, "D");
                oDq.AddBigIntegerParam("@fk_UserID", GeneralFunctions.LoginInfo.UserID);
                oDq.AddIntegerParam("@Result", 0);
                ret = Convert.ToInt32(oDq.GetScalar());
            }
            return ret;
        }

        public static InventoryEntity GetSFForEdit(int ID, string CalledFrom)
        {
            string strExecution = "[rm].[sp_GetOnlySF]";
            InventoryEntity oLoc = null;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@pk_RecipeID", ID);
                oDq.AddIntegerParam("@Result", 0);
                DataTableReader reader = oDq.GetTableReader();

                while (reader.Read())
                {
                    oLoc = new InventoryEntity(reader);
                }
                reader.Close();
            }
            return oLoc;
        }

        public static InventoryEntity GetRecipeForEdit(int ID, string CalledFrom)
        {
            string strExecution = "[rm].[sp_ManageRecipe]";
            InventoryEntity oLoc = null;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@pk_RecipeID", ID);
                oDq.AddVarcharParam("@Mode", 1, CalledFrom);
                oDq.AddIntegerParam("@fk_UserID", GeneralFunctions.LoginInfo.UserID);
                oDq.AddIntegerParam("@Result", 0);
                DataTableReader reader = oDq.GetTableReader();

                while (reader.Read())
                {
                    oLoc = new InventoryEntity(reader);
                }
                reader.Close();
            }
            return oLoc;
        }
        #endregion

    }
}
