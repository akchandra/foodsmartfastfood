﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FoodSmart.Common;
using FoodSmart.DAL.DbManager;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using System.Data.SqlClient;

namespace FoodSmart.DAL
{
    public sealed class InvTranDAL
    {

        public static int SaveInvoiceTran(IInvoice inv, string mode, DataSet ds)
        {
            string strExecution = "[pos].[sp_ManageBILLS]";
            int result = 0;
            SqlCommand SqlCmd = new SqlCommand();
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                ds.Tables[0].Columns.Remove("pk_BillSrl");
                ds.Tables[0].Columns.Remove("itemdescr");
                ds.Tables[0].Columns.Remove("servicetaxper");
                ds.Tables[0].Columns.Remove("sbcper");

                oDq.AddIntegerParam("@pk_BillID", inv.InvoiceID);
                oDq.AddVarcharParam("@Mode", 1, mode);
                oDq.AddIntegerParam("@fk_CardID", inv.CustID);
                oDq.AddIntegerParam("@fk_RestID", inv.RestID);
                oDq.AddIntegerParam("@fk_RefundReason", inv.RefundReasonID);
                oDq.AddIntegerParam("@UserID", inv.UserID);
                oDq.AddIntegerParam("@LocationID", inv.LocID);
                oDq.AddIntegerParam("@fk_FisYearID", inv.YearID);
                oDq.AddIntegerParam("@billNo", inv.InvoiceID);
                oDq.AddVarcharParam("@BillType", 1, inv.BillType);
                oDq.AddDateTimeParam("@BillDate", inv.BillDate);
                oDq.AddIntegerParam("@Serial", inv.SerialNo);
                oDq.AddDecimalParam("@BillAmount", 12, 2, inv.BillAmount);
                oDq.AddDecimalParam("@RoundOff", 12, 2, inv.RoundOff);
                oDq.AddVarcharParam("@PMode", 1, inv.PaymentMode);
                oDq.AddIntegerParam("@Result", result, QueryParameterDirection.Output);
                oDq.AddVarcharParam("@MachineName", 1, inv.MachineName);

                //passing  table as parameter
                oDq.AddTableValueParam("@MParam", ds.Tables[0], QueryParameterDirection.Input);

                oDq.RunActionQuery();
                result = Convert.ToInt32(oDq.GetParaValue("@Result"));
            }
            return result;
        }
    }
}
