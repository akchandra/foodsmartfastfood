﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FoodSmart.Common;
using FoodSmart.DAL.DbManager;
using FoodSmart.Entity;
using FoodSmart.Utilities;

namespace FoodSmart.DAL
{
    public sealed class ImprestTranDAL
    {
        public static DataSet GetAllTransactions(SearchCriteria searchCriteria)
        {
            DataSet ds = new DataSet();
            using (DbQuery dq = new DbQuery("[dbo].[sp_ManageImprestCash]"))
            {
                dq.AddDateTimeParam("@TranDate", searchCriteria.Date);
                dq.AddVarcharParam("@TranType", 1, searchCriteria.CardType);
                dq.AddVarcharParam("@Mode", 1, "L");
                dq.AddIntegerParam("@fk_LocID", searchCriteria.LocID);
                dq.AddIntegerParam("@RESULT", 0, QueryParameterDirection.Output);
                ds = dq.GetTables();
            }
            return ds;
        }

        public static CashierImprestEntity GetImprestData(int ImprestID)
        {
            string strExecution = "[dbo].[sp_ManageImprestCash]";
            CashierImprestEntity rest = null;

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@Mode", 1, "M");
                oDq.AddIntegerParam("@pk_TranID", ImprestID);
                oDq.AddIntegerParam("@Result", 0);
                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    rest = new CashierImprestEntity(reader);
                }
                reader.Close();
            }
            return rest;
        }

        public static int SaveImprest(ICashierImprest Imprest, int Userid)
        {
            string strExecution = "[dbo].[sp_ManageImprestCash]";
            int result = 0;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@pk_TranID", Imprest.TranID);
                oDq.AddVarcharParam("@Mode", 1, Imprest.Mode);
                oDq.AddIntegerParam("@fk_LocID", Imprest.LocID);
                oDq.AddVarcharParam("@Narration", 300, Imprest.Narration);
                oDq.AddDecimalParam("@TranAmount", 12, 2, Imprest.TranAmount);
                oDq.AddDateTimeParam("@TranDate", Imprest.CashTranDate);
                oDq.AddVarcharParam("@TranType", 1, Imprest.TranType);
                oDq.AddIntegerParam("@fk_CashierID", Imprest.CashierID);
                oDq.AddIntegerParam("@UserID", Userid);
                oDq.AddIntegerParam("@Result", result, QueryParameterDirection.Output);
                oDq.RunActionQuery();
                result = Convert.ToInt32(oDq.GetParaValue("@Result"));
            }
            return result;
        }
    }
}
