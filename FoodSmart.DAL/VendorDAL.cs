﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FoodSmart.Common;
using FoodSmart.DAL.DbManager;
using FoodSmart.Entity;
using FoodSmart.Utilities;
namespace FoodSmart.DAL
{
    public sealed class VendorDAL
    {
        #region Vendor
        public static DataSet GetAllVendor(SearchCriteria searchCriteria)
        {
            DataSet ds = new DataSet();
            using (DbQuery dq = new DbQuery("[inv].[sp_ManageVendor]"))
            {
                dq.AddVarcharParam("@VendorName", 100, searchCriteria.StringOption1);
                dq.AddVarcharParam("@GSTNo", 100, searchCriteria.StringOption2);
                dq.AddVarcharParam("@Mode", 1, "L");
                dq.AddIntegerParam("@RESULT", 0, QueryParameterDirection.Output);
                ds = dq.GetTables();
            }
            return ds;
        }

        public static int SaveVendor(IVendor ven, string mode)
        {
            string strExecution = "[inv].[sp_ManageVendor]";
            int result = 0;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@pk_VendorID", ven.VendorID);
                oDq.AddVarcharParam("@Mode", 1, mode);
                oDq.AddVarcharParam("@VendorName", 100, ven.VendorName);
                oDq.AddVarcharParam("@VendorAddress", 300, ven.VendorAddress);
                oDq.AddVarcharParam("@LandPhone", 10, ven.LandPhone);
                oDq.AddVarcharParam("@ContactPerson", 50, ven.ContactPerson);
                oDq.AddVarcharParam("@ContactMob", 20, ven.ContactMob);
                oDq.AddVarcharParam("@EmailID", 20, ven.emailID);
                oDq.AddVarcharParam("@GSTNo", 50, ven.GSTNo);
                oDq.AddIntegerParam("@StateID", ven.StateID);
                oDq.AddVarcharParam("@PANNo", 50, ven.PANNo);
                oDq.AddIntegerParam("@UserID", GeneralFunctions.LoginInfo.UserID);
                oDq.AddIntegerParam("@Result", result, QueryParameterDirection.Output);
                oDq.RunActionQuery();
                result = Convert.ToInt32(oDq.GetParaValue("@Result"));
            }
            return result;
        }

        public static VendorEntity GetVendorEdit(int ID, string CalledFrom)
        {
            string strExecution = "[inv].[sp_ManageVendor]";
            VendorEntity oLoc = null;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@pk_VendorID", ID);
                oDq.AddVarcharParam("@Mode", 1, CalledFrom);
                //oDq.AddIntegerParam("@UserID", UserID);
                oDq.AddIntegerParam("@Result", 0);
                DataTableReader reader = oDq.GetTableReader();

                while (reader.Read())
                {
                    oLoc = new VendorEntity(reader);
                }
                reader.Close();
            }
            return oLoc;
        }

        public static int DeleteVendor(int RMID)
        {
            string strExecution = "[inv].[sp_ManageVendor]";
            int ret = 0;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@pk_VendorID", RMID);
                oDq.AddVarcharParam("@Mode", 1, "D");
                oDq.AddBigIntegerParam("@UserID", GeneralFunctions.LoginInfo.UserID);
                oDq.AddIntegerParam("@Result", 0);
                ret = Convert.ToInt32(oDq.GetScalar());
            }
            return ret;
        }
        #endregion
    }
}
