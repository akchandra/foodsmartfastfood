﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FoodSmart.Common;
using FoodSmart.DAL.DbManager;
using FoodSmart.Entity;
using FoodSmart.Utilities;

namespace FoodSmart.DAL
{
    public sealed class ItemDAL
    {
        #region "Item Group"
        public static DataSet GetAllItemGroup(SearchCriteria searchCriteria)
        {
            DataSet ds = new DataSet();
            using (DbQuery dq = new DbQuery("[admin].[sp_ManageITEMGROUP]"))
            {
                dq.AddVarcharParam("@Descr", 100, searchCriteria.ItemGroupName);
                //dq.AddVarcharParam("@RestName", 100, searchCriteria.RestName);
                //dq.AddVarcharParam("@LocName", 100, searchCriteria.LocName);
                dq.AddVarcharParam("@Mode", 1, "L");
                //dq.AddIntegerParam("@LocID", searchCriteria.IntegerOption2);
                dq.AddVarcharParam("@RorF", 1, searchCriteria.RorF);
                dq.AddIntegerParam("@RESULT", 0, QueryParameterDirection.Output);
                ds = dq.GetTables();
            }
            return ds;
        }

        public static ItemsEntity GetItemGroupForEdit(int ItemGroupID)
        {
            string strExecution = "[admin].[sp_ManageITEMGROUP]";
            ItemsEntity itemgrp = null;

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@Mode", 1, "M");
                oDq.AddIntegerParam("@pk_GROUPID", ItemGroupID);
                oDq.AddIntegerParam("@Result", 0);
                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    itemgrp = new ItemsEntity(reader);
                }
                reader.Close();
            }
            return itemgrp;
        }

        public static int SaveItemGroup(IItems Item, string mode)
        {
            string strExecution = "[admin].[sp_ManageITEMGROUP]";
            int result = 0;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@pk_Groupid", Item.ItemGroupID);
                oDq.AddVarcharParam("@Mode", 1, mode);
                oDq.AddVarcharParam("@descr", 50, Item.GroupDescription);
                oDq.AddIntegerParam("@Restid", Item.RestID);
                oDq.AddIntegerParam("@Locid", Item.LocID);
                oDq.AddIntegerParam("@UserID", Item.CreatedBy);
                oDq.AddVarcharParam("@RorF", 1, Item.RorF);
                oDq.AddIntegerParam("@Result", result, QueryParameterDirection.Output);
                oDq.RunActionQuery();
                result = Convert.ToInt32(oDq.GetParaValue("@Result"));
            }
            return result;
        }

        public static int DeleteItemGroup(int GroupID)
        {
            string strExecution = "[admin].[sp_ManageITEMGROUP]";
            int ret = 0;
            int result = 0;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@pk_GROUPID", GroupID);
                oDq.AddVarcharParam("@Mode", 1, "D");
                oDq.AddBigIntegerParam("@UserID", GeneralFunctions.LoginInfo.UserID);
                oDq.AddIntegerParam("@Result", result, QueryParameterDirection.Output);
                ret = Convert.ToInt32(oDq.GetScalar());
            }
            return ret;
        }

        public static int ChkItemGroupDelPossible(int ItemGroupID)
        {
            string strExecution = "[admin].[sp_CheckItemGroupDeletePossible]";
            int Result = 0;

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddBigIntegerParam("@pk_GroupID", ItemGroupID);
                oDq.AddIntegerParam("@RESULT", Result, QueryParameterDirection.Output);
                oDq.RunActionQuery();
                return Convert.ToInt32(oDq.GetParaValue("@RESULT"));
            }

            //string strExecution = "[admin].[sp_ManageITEMGROUP]";
            //int Result = 0;

            //using (DbQuery oDq = new DbQuery(strExecution))
            //{
            //    oDq.AddBigIntegerParam("@pk_itemgroupid", ItemGroupID);
            //    oDq.AddVarcharParam("@Mode", 1, "C");
            //    oDq.AddIntegerParam("@RESULT", Result, QueryParameterDirection.Output);
            //    oDq.RunActionQuery();
            //    return Convert.ToInt32(oDq.GetParaValue("@RESULT"));
            //}
        }
        #endregion

        #region "Item Master"
        public static DataSet GetAllItems(SearchCriteria searchCriteria)
        {
            DataSet ds = new DataSet();
            using (DbQuery dq = new DbQuery("[admin].[sp_ManageITEM]"))
            {
                dq.AddVarcharParam("@ItemDescr", 100, searchCriteria.ItemName);
                dq.AddVarcharParam("@GroupDescr", 100, searchCriteria.ItemGroupName);
                dq.AddVarcharParam("@RorF", 1, searchCriteria.RorF);
                //dq.AddVarcharParam("@RestName", 100, searchCriteria.RestName);
                //dq.AddVarcharParam("@LocName", 100, searchCriteria.LocName);
                //dq.AddIntegerParam("@fk_RestID", searchCriteria.IntegerOption1);
                //dq.AddIntegerParam("@fk_LocID", searchCriteria.IntegerOption2);
                dq.AddIntegerParam("@ItemRefID", searchCriteria.IntegerOption3);
                dq.AddVarcharParam("@Mode", 1, "L");
                dq.AddIntegerParam("@RESULT", 0, QueryParameterDirection.Output);
                ds = dq.GetTables();
            }
            return ds;
        }

        public static DataSet GetHotItems(SearchCriteria searchCriteria)
        {
            DataSet ds = new DataSet();
            using (DbQuery dq = new DbQuery("[pos].[sp_GetHotItems]"))
            {
                dq.AddDateTimeParam("@EffectiveDate", searchCriteria.Date);
                //dq.AddIntegerParam("@fk_RestID", searchCriteria.IntegerOption1);
                dq.AddIntegerParam("@fk_LocID", searchCriteria.IntegerOption2);
                ds = dq.GetTables();
            }
            return ds;
        }

        public static int SaveItem(IItems Item, string mode)
        {
            string strExecution = "[admin].[sp_ManageITEM]";
            int result = 0;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@pk_ITEMID", Item.ItemID);
                oDq.AddVarcharParam("@Mode", 1, mode);
                oDq.AddVarcharParam("@ItemDescr", 50, Item.ItemDescription);
                //oDq.AddIntegerParam("@fk_RestID", Item.RestID);
                oDq.AddIntegerParam("@ItemRefID", Item.ItemGroupID);
                oDq.AddVarcharParam("@UOM", 50, Item.UOM);
                oDq.AddDecimalParam("@CGSTPer", 6, 2, Item.CGSTPer);
                oDq.AddDecimalParam("@SGSTPer", 6, 2, Item.SGSTPer);
                oDq.AddVarcharParam("@HSNCode", 8, Item.HSNCode);
                oDq.AddIntegerParam("@ItemType", Item.itemType);
                oDq.AddBooleanParam("@DiscAllowed", Item.DiscountAllowed);
                oDq.AddDecimalParam("@SellingRate", 12, 2, Item.SellingRate);
                oDq.AddIntegerParam("@UserID", Item.CreatedBy);
                oDq.AddIntegerParam("@Result", result, QueryParameterDirection.Output);
                oDq.AddVarcharParam("@ProcessedAt", 1, Item.ProcessedAt);
                oDq.RunActionQuery();
                result = Convert.ToInt32(oDq.GetParaValue("@Result"));
            }
            return result;
        }

        public static int DeleteItem(int ItemID)
        {
            string strExecution = "[admin].[sp_ManageITEM]";
            int ret = 0;
            int result = 0;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@pk_ITEMID", ItemID);
                oDq.AddVarcharParam("@Mode", 1, "D");
                oDq.AddBigIntegerParam("@UserID", GeneralFunctions.LoginInfo.UserID);
                oDq.AddIntegerParam("@Result", result, QueryParameterDirection.Output);
                ret = Convert.ToInt32(oDq.GetScalar());
            }
            return ret;
        }

        public static ItemsEntity GetItemForEdit(int ItemID)
        {
            string strExecution = "[admin].[sp_ManageITEM]";
            ItemsEntity itemgrp = null;

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@Mode", 1, "M");
                oDq.AddIntegerParam("@pk_ItemID", ItemID);
                oDq.AddIntegerParam("@Result", 0);
                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    itemgrp = new ItemsEntity(reader);
                }
                reader.Close();
            }
            return itemgrp;
        }

        public static int ChkItemDelPossible(int ItemID)
        {
            string strExecution = "[admin].[sp_ManageITEM]";
            int Result = 0;

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddBigIntegerParam("@pk_ITEMID", ItemID);
                oDq.AddVarcharParam("@Mode", 1, "C");
                oDq.AddIntegerParam("@RESULT", Result, QueryParameterDirection.Output);
                oDq.RunActionQuery();
                return Convert.ToInt32(oDq.GetParaValue("@RESULT"));
            }
        }

        #endregion

        #region "Item Rate"
        public static DataSet GetAllItemRates(SearchCriteria searchCriteria)
        {
            DataSet ds = new DataSet();
            using (DbQuery dq = new DbQuery("[admin].[sp_ManageItemRate]"))
            {
                dq.AddVarcharParam("@ItemDescr", 100, searchCriteria.ItemName);
                dq.AddVarcharParam("@GroupDescr", 100, searchCriteria.ItemGroupName);
                dq.AddVarcharParam("@RestName", 100, searchCriteria.RestName);
                dq.AddVarcharParam("@LocName", 100, searchCriteria.LocName);
                dq.AddDateTimeParam("@EffectiveDate", searchCriteria.Date);
                dq.AddVarcharParam("@Mode", 1, "N");
                dq.AddIntegerParam("@RESULT", 0, QueryParameterDirection.Output);
                ds = dq.GetTables();
            }
            return ds;
        }

        public static int SaveItemRate(IItems Item, string mode, int UserID)
        {
            string strExecution = "[admin].[sp_ManageItemRate]";
            int result = 0;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@pk_Rateid", Item.ItemRateID);
                oDq.AddVarcharParam("@Mode", 1, mode);
                oDq.AddDecimalParam("@SellingRate", 12, 2, Item.SellingRate);
                oDq.AddIntegerParam("@fk_Itemid", Item.ItemID);
                oDq.AddDateTimeParam("@EffectiveDate", Item.EffectiveDate);
                oDq.AddIntegerParam("@UserID", UserID);
                oDq.AddIntegerParam("@Result", result, QueryParameterDirection.Output);
                oDq.RunActionQuery();
                result = Convert.ToInt32(oDq.GetParaValue("@Result"));
            }
            return result;
        }

        public static int DeleteItemRate(int GroupID)
        {
            string strExecution = "[admin].[sp_ManageItemRate]";
            int ret = 0;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@pk_itemRateid", GroupID);
                oDq.AddVarcharParam("@Mode", 1, "D");
                oDq.AddBigIntegerParam("@UserID", GeneralFunctions.LoginInfo.UserID);
                ret = Convert.ToInt32(oDq.GetScalar());
            }
            return ret;
        }

        public static int ChkItemRateDelPossible(int ItemRateID)
        {
            string strExecution = "[admin].[sp_ManageItemRate]";
            int Result = 0;

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddBigIntegerParam("@pk_RATEID", ItemRateID);
                oDq.AddVarcharParam("@Mode", 1, "C");
                oDq.AddIntegerParam("@RESULT", Result, QueryParameterDirection.Output);
                oDq.RunActionQuery();
                return Convert.ToInt32(oDq.GetParaValue("@RESULT"));
            }
        }

        public static DataSet GetAllRateDate(string Action, int ItemID, DateTime EffDate)
        {
            DataSet ds = new DataSet();
            using (DbQuery dq = new DbQuery("[admin].[spGetAllRateDate]"))
            {
                dq.AddIntegerParam("@ItemID", ItemID);
                dq.AddVarcharParam("@Action", 1, Action);
                dq.AddDateTimeParam("@EffDate", EffDate);
                ds = dq.GetTables();
            }
            return ds;
        }
        #endregion

        #region "Item Opening Balance"
        public static DataSet GetAllItemOpbal(SearchCriteria searchCriteria, int yrid)
        {
            DataSet ds = new DataSet();
            using (DbQuery dq = new DbQuery("[admin].[sp_ManageItemOpbal]"))
            {
                dq.AddVarcharParam("@ItemName", 100, searchCriteria.ItemName);
                dq.AddIntegerParam("@fk_FinYrID", yrid);
                dq.AddVarcharParam("@Mode", 1, "L");
                dq.AddIntegerParam("@RESULT", 0, QueryParameterDirection.Output);
                ds = dq.GetTables();
            }
            return ds;
        }

        public static ItemsEntity GetItemOpbalForEdit(int StockID)
        {
            string strExecution = "[admin].[sp_ManageItemOpbal]";
            ItemsEntity itemgrp = null;

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@Mode", 1, "M");
                oDq.AddIntegerParam("@pk_StockID", StockID);
                oDq.AddIntegerParam("@Result", 0);
                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    itemgrp = new ItemsEntity(reader);
                }
                reader.Close();
            }
            return itemgrp;
        }

        public static int SaveItemOpbal(IItems Item, string mode)
        {
            string strExecution = "[admin].[sp_ManageItemOpbal]";
            int result = 0;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@pk_StockID", Item.ItemStockID);
                oDq.AddVarcharParam("@Mode", 1, mode);
                oDq.AddDecimalParam("@ItemStock", 12, 3, Item.Opqty);
                oDq.AddIntegerParam("@fk_ItemID", Item.ItemID);
                oDq.AddIntegerParam("@fk_FinYrID", Item.finYrID);
                oDq.AddIntegerParam("@fk_ItemGroupId", Item.ItemGroupID);
                oDq.AddIntegerParam("@UserID", Item.CreatedBy);
                oDq.AddIntegerParam("@Result", result, QueryParameterDirection.Output);
                oDq.RunActionQuery();
                result = Convert.ToInt32(oDq.GetParaValue("@Result"));
            }
            return result;
        }

        public static int DeleteItemOpbal(int StockID)
        {
            string strExecution = "[admin].[sp_ManageItemOpbal]";
            int ret = 0;
            int result = 0;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@pk_StockID", StockID);
                oDq.AddVarcharParam("@Mode", 1, "D");
                oDq.AddBigIntegerParam("@UserID", GeneralFunctions.LoginInfo.UserID);
                oDq.AddIntegerParam("@Result", result, QueryParameterDirection.Output);
                ret = Convert.ToInt32(oDq.GetScalar());
            }
            return ret;
        }

        
        #endregion

        #region "RM Master"
        public static DataSet GetAllRMItems(SearchCriteria searchCriteria)
        {
            DataSet ds = new DataSet();
            using (DbQuery dq = new DbQuery("[inv].[sp_ManageRM]"))
            {
                dq.AddVarcharParam("@RMName", 100, searchCriteria.rmItem);
                dq.AddVarcharParam("@GroupType", 1, searchCriteria.rmItemType);
                dq.AddIntegerParam("@RMGroupID", searchCriteria.rmGroupID);
                dq.AddVarcharParam("@Mode", 1, "L");
                dq.AddIntegerParam("@RESULT", 0, QueryParameterDirection.Output);
                ds = dq.GetTables();
            }
            return ds;
        }


        public static int SaveRM(IItems Item, string mode)
        {
            string strExecution = "[inv].[sp_ManageRM]";
            int result = 0;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@pk_RMID", Item.ItemID);
                oDq.AddVarcharParam("@Mode", 1, mode);
                oDq.AddVarcharParam("@RMName", 50, Item.ItemDescription);
                //oDq.AddIntegerParam("@fk_RestID", Item.RestID);
                oDq.AddIntegerParam("@RMGroupID", Item.ItemGroupID);
                oDq.AddVarcharParam("@UOM", 50, Item.UOM);
                oDq.AddDecimalParam("@MinOrderLevel", 12, 3, Item.MinStockLevel);
                oDq.AddDecimalParam("@MaxOrderLevel", 12, 3, Item.MaxStockLevel);
                oDq.AddDecimalParam("@ReorderLevel", 12, 3, Item.ReOrdStockLevel);
                oDq.AddIntegerParam("@UserID", Item.CreatedBy);
                oDq.AddIntegerParam("@Result", result, QueryParameterDirection.Output);
                oDq.RunActionQuery();
                result = Convert.ToInt32(oDq.GetParaValue("@Result"));
            }
            return result;
        }

        public static int DeleteRM(int ItemID, int UserID)
        {
            string strExecution = "[inv].[sp_ManageRM]";
            int ret = 0;
            int result = 0;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@pk_RMID", ItemID);
                oDq.AddVarcharParam("@Mode", 1, "D");
                oDq.AddBigIntegerParam("@UserID", UserID);
                oDq.AddIntegerParam("@Result", result, QueryParameterDirection.Output);
                ret = Convert.ToInt32(oDq.GetScalar());
            }
            return ret;
        }

        public static ItemsEntity GetRMForEdit(int ItemID)
        {
            string strExecution = "[inv].[sp_ManageRM]";
            ItemsEntity itemgrp = null;

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@Mode", 1, "M");
                oDq.AddIntegerParam("@pk_RMID", ItemID);
                oDq.AddIntegerParam("@Result", 0);
                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    itemgrp = new ItemsEntity(reader);
                }
                reader.Close();
            }
            return itemgrp;
        }

        public static DataSet GetRMForStockTake(SearchCriteria searchCriteria)
        {
            DataSet ds = new DataSet();
            using (DbQuery dq = new DbQuery("[inv].[sp_ManageRM]"))
            {
                dq.AddVarcharParam("@RMName", 100, searchCriteria.rmItem);
                dq.AddVarcharParam("@GroupType", 1, searchCriteria.rmItemType);
                dq.AddIntegerParam("@RMGroupID", searchCriteria.rmGroupID);
                dq.AddVarcharParam("@Mode", 1, "S");
                dq.AddIntegerParam("@RESULT", 0, QueryParameterDirection.Output);
                ds = dq.GetTables();
            }
            return ds;
        }

        #endregion

        #region "RM Group"
        public static DataSet GetAllRMGroup(SearchCriteria searchCriteria)
        {
            DataSet ds = new DataSet();
            using (DbQuery dq = new DbQuery("[inv].[sp_ManageRMGroup]"))
            {
                dq.AddVarcharParam("@RMGroupName", 100, searchCriteria.RMGroupName);
                dq.AddVarcharParam("@Mode", 1, "L");
                dq.AddVarcharParam("@GroupType", 1, searchCriteria.rmItemType);
                dq.AddIntegerParam("@RESULT", 0, QueryParameterDirection.Output);
                ds = dq.GetTables();
            }
            return ds;
        }

        public static ItemsEntity GetRMGroupForEdit(int ItemGroupID)
        {
            string strExecution = "[inv].[sp_ManageRMGroup]";
            ItemsEntity itemgrp = null;

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@Mode", 1, "M");
                oDq.AddIntegerParam("@pk_RMGROUPID", ItemGroupID);
                oDq.AddIntegerParam("@Result", 0);
                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    itemgrp = new ItemsEntity(reader);
                }
                reader.Close();
            }
            return itemgrp;
        }

        public static int SaveRMGroup(IItems Item, string mode)
        {
            string strExecution = "[inv].[sp_ManageRMGroup]";
            int result = 0;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@pk_RMGroupid", Item.ItemGroupID);
                oDq.AddVarcharParam("@Mode", 1, mode);
                oDq.AddVarcharParam("@RMGroupName", 50, Item.GroupDescription);
                oDq.AddIntegerParam("@UserID", Item.CreatedBy);
                oDq.AddVarcharParam("@GroupType", 1, Item.GroupType);
                oDq.AddIntegerParam("@Result", result, QueryParameterDirection.Output);
                oDq.RunActionQuery();
                result = Convert.ToInt32(oDq.GetParaValue("@Result"));
            }
            return result;
        }

        public static int DeleteRMGroup(int GroupID, int UserID)
        {
            string strExecution = "[inv].[sp_ManageRMGroup]";
            int ret = 0;
            int result = 0;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@pk_RMGroupID", GroupID);
                oDq.AddVarcharParam("@Mode", 1, "D");
                oDq.AddBigIntegerParam("@UserID", UserID);
                oDq.AddIntegerParam("@Result", result, QueryParameterDirection.Output);
                ret = Convert.ToInt32(oDq.GetScalar());
            }
            return ret;
        }

        #endregion
    }
}
