﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FoodSmart.Common;
using FoodSmart.DAL.DbManager;
using FoodSmart.Entity;
using FoodSmart.Utilities;

namespace FoodSmart.DAL
{
    public sealed class CouponDAL
    {
        #region "Coupon Master"
        public static DataSet GetAllCoupons(SearchCriteria searchCriteria)
        {
            DataSet ds = new DataSet();
            using (DbQuery dq = new DbQuery("[dbo].[sp_ManageCoupon]"))
            {
                dq.AddVarcharParam("@Prefix", 3, searchCriteria.CouponPrefix);
                dq.AddVarcharParam("@CouponName", 100, searchCriteria.CouponName);
                dq.AddVarcharParam("@Mode", 1, "L");
                dq.AddIntegerParam("@RESULT", 0, QueryParameterDirection.Output);
                ds = dq.GetTables();
            }
            return ds;
        }

        public static CouponEntity GetCouponForEdit(int CPID)
        {
            string strExecution = "[dbo].[sp_ManageCoupon]";
            CouponEntity oCP = null;

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@Mode", 1, "M");
                oDq.AddIntegerParam("@pk_CouponID", CPID);
                oDq.AddIntegerParam("@Result", 0);
                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    oCP = new CouponEntity(reader);
                }
                reader.Close();
            }
            return oCP;
        }

        public static int SaveCoupon(ICoupon cp, string mode)
        {
            string strExecution = "[dbo].[sp_ManageCoupon]";
            int result = 0;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@pk_CouponID", cp.CPID);
                oDq.AddVarcharParam("@Mode", 1, mode);
                oDq.AddVarcharParam("@prefix", 3, cp.CouponPrefix);
                oDq.AddVarcharParam("@CouponName", 50, cp.CouponName);
                oDq.AddIntegerParam("@StartNo", cp.CouponStartSerial);
                oDq.AddIntegerParam("@EndNo", cp.CouponEndSerial);
                oDq.AddDateTimeParam("@ActivatedOn", cp.ActivateDate);
                oDq.AddDateTimeParam("@ExpiryOn", cp.ExpiryDate);
                oDq.AddIntegerParam("@fk_UserID", cp.CreatedBy);
                oDq.AddDecimalParam("@CouponAmount", 12, 2, cp.CouponAmount);
                oDq.AddIntegerParam("@Result", result, QueryParameterDirection.Output);
                oDq.RunActionQuery();
                result = Convert.ToInt32(oDq.GetParaValue("@Result"));
            }
            return result;
        }

        public static int DeleteCoupon(int CPID)
        {
            string strExecution = "[dbo].[sp_ManageCoupon]";
            int ret = 0;
            int result = 0;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@pk_CouponID", CPID);
                oDq.AddVarcharParam("@Mode", 1, "D");
                oDq.AddBigIntegerParam("@fk_UserID", GeneralFunctions.LoginInfo.UserID);
                oDq.AddIntegerParam("@Result", result, QueryParameterDirection.Output);
                ret = Convert.ToInt32(oDq.GetScalar());
            }
            return ret;
        }

        //public static int ChkItemGroupDelPossible(int ItemGroupID)
        //{
        //    string strExecution = "[admin].[sp_CheckItemGroupDeletePossible]";
        //    int Result = 0;

        //    using (DbQuery oDq = new DbQuery(strExecution))
        //    {
        //        oDq.AddBigIntegerParam("@pk_GroupID", ItemGroupID);
        //        oDq.AddIntegerParam("@RESULT", Result, QueryParameterDirection.Output);
        //        oDq.RunActionQuery();
        //        return Convert.ToInt32(oDq.GetParaValue("@RESULT"));
        //    }

        
        //}

        public static DataSet CheckCoupon(string prefix, int CouponNo, string QueryType)
        {
            string strExecution = "[pos].[sp_GetCouponWithPrefix]";
            DataSet myDataSet;

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@Prefix", 3, prefix);
                oDq.AddIntegerParam("@CouponNo", CouponNo);
                oDq.AddVarcharParam("@Query", 1, QueryType);
                myDataSet = oDq.GetTables();
            }

            return myDataSet;
        }
        #endregion


     
    }
}
