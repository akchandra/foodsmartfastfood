﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FoodSmart.Common;
using FoodSmart.DAL.DbManager;
using FoodSmart.Entity;
using FoodSmart.Utilities;

namespace FoodSmart.DAL
{
    public sealed class RestaurantDAL
    {
        public static DataSet GetAllRestaurant(SearchCriteria searchCriteria)
        {
            DataSet ds = new DataSet();
            using (DbQuery dq = new DbQuery("[admin].[sp_ManageRestaurant]"))
            {
                dq.AddVarcharParam("@RestName", 100, searchCriteria.UserName);
                dq.AddVarcharParam("@LocName", 100, searchCriteria.LocName);
                dq.AddVarcharParam("@Mode", 1, "L");
                dq.AddIntegerParam("@RESULT", 0, QueryParameterDirection.Output);
                ds = dq.GetTables();
            }
            return ds;
        }

        public static int SaveRestaurant(IRestaurant Rest, string mode, int Userid)
        {
            string strExecution = "[admin].[sp_ManageRestaurant]";
            int result = 0;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@pk_RestID", Rest.RestID);
                oDq.AddVarcharParam("@Mode", 1, mode);
                oDq.AddVarcharParam("@RestName", 50, Rest.RestName);
                oDq.AddVarcharParam("@RestPw", 300, Rest.RestPass);
                oDq.AddDecimalParam("@Ratio", 12, 2, Rest.Ratio);
                oDq.AddBooleanParam("@PrintBill", Rest.PrintBill);
                oDq.AddIntegerParam("@fk_LocID", Rest.LocID);
                oDq.AddVarcharParam("@RestType", 1, Rest.RestType);
                oDq.AddVarcharParam("@PrinterType", 1, Rest.printerType);
                oDq.AddVarcharParam("@PrinterName", 100, Rest.PrinterName);
                oDq.AddVarcharParam("@KOTPrinterType", 1, Rest.KOTPrinterType);
                oDq.AddVarcharParam("@KOTPrinterName", 100, Rest.KOTPrinterName);
                oDq.AddVarcharParam("@VATNo", 300, Rest.VATNo);
                oDq.AddVarcharParam("@ServiceTaxNo", 300, Rest.ServTaxNo);
                oDq.AddDecimalParam("@ServiceTaxPer", 6, 2, Rest.ServTaxPer);
                oDq.AddDecimalParam("@SBCPer", 6, 2, Rest.SBCPer);
                oDq.AddIntegerParam("@UserID", Userid);
                oDq.AddIntegerParam("@Result", result, QueryParameterDirection.Output);
                oDq.RunActionQuery();
                result = Convert.ToInt32(oDq.GetParaValue("@Result"));
            }
            return result;
        }

        public static RestaurantEntity GetRestData(int RestID)
        {
            string strExecution = "[admin].[sp_ManageRESTAURANT]";
            RestaurantEntity rest = null;

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@Mode", 1, "M");
                oDq.AddIntegerParam("@pk_RESTID", RestID);
                oDq.AddIntegerParam("@Result", 0);
                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    rest = new RestaurantEntity(reader);
                }
                reader.Close();
            }
            return rest;
        }

        public static int DeleteRest(int RestID)
        {
            string strExecution = "[admin].[sp_ManageRestaurant]";
            int ret = 0;
            int Result = 0;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@pk_RestID", RestID);
                oDq.AddVarcharParam("@Mode", 1, "D");
                oDq.AddBigIntegerParam("@UserID", GeneralFunctions.LoginInfo.UserID);
                oDq.AddIntegerParam("@RESULT", Result, QueryParameterDirection.Output);
                ret = Convert.ToInt32(oDq.GetScalar());
            }
            return ret;
        }

        public static int ChkRestDelPossible(int RestID)
        {
            string strExecution = "[admin].[sp_ManageRestaurant]";
            int Result = 0;

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddBigIntegerParam("@pk_RestID", RestID);
                oDq.AddVarcharParam("@Mode", 1, "C");
                oDq.AddIntegerParam("@RESULT", Result, QueryParameterDirection.Output);
                oDq.RunActionQuery();
                return Convert.ToInt32(oDq.GetParaValue("@RESULT"));
            }
        }
    }
}
