﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FoodSmart.Common;
using FoodSmart.DAL.DbManager;
using FoodSmart.Entity;
using FoodSmart.Utilities;
namespace FoodSmart.DAL
{
    public sealed class CommonDAL
    {
        private CommonDAL()
        {
        }

        #region Common

        
        /// <summary>
        /// Saves the error log.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="message">The message.</param>
        /// <param name="stackTrace">The stack trace.</param>
        /// <createdby>Amit Kumar Chandra</createdby>
        /// <createddate>02/12/2012</createddate>
        public static void SaveErrorLog(int userId, string message, string stackTrace)
        {
            string strExecution = "[admin].[uspSaveError]";

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@UserId", userId);
                oDq.AddVarcharParam("@ErrorMessage", 255, message);
                oDq.AddVarcharParam("@StackTrace", -1, stackTrace);
                oDq.RunActionQuery();
            }
        }


        /// <summary>
        /// Common method to populate all the dropdownlist throughout the application
        /// </summary>
        /// <param name="Number">Unique Number</param>
        /// <returns>DataTable</returns>
        /// <createdby>Rajen Saha</createdby>
        /// <createddate>01/12/2012</createddate>
        public static DataTable PopulateDropdown(int Number, int? Filter1, int? Filter2)
        {
            string strExecution = "[common].[spPopulateDropDownList]";

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@Number", Number);
                oDq.AddIntegerParam("@Filter", Filter1.Value);
                oDq.AddIntegerParam("@Type", Filter2.Value);


                return oDq.GetTable();
            }
        }

        public static int SaveCompanyDetail(ICompany comp, int modifiedBy)
        {
            string strExecution = "[admin].[sp_UpdateCompany]";
            DataTable myDataTable;

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@CalculationMethod", 1, comp.CalculationMethod);
                oDq.AddVarcharParam("@btmLine1", 40, comp.btmLine1);
                oDq.AddVarcharParam("@btmLine2", 40, comp.btmLine2);
                oDq.AddVarcharParam("@btmLine3", 40, comp.btmLine3);
                oDq.AddVarcharParam("@btmLine4", 40, comp.btmLine4);
                oDq.AddDecimalParam("@ServTaxPer", 12, 2, comp.ServiceTaxPer);
                oDq.RunActionQuery();
            }

            return 1;
        }

        #endregion


        #region Import Export From Local

        //+AR 07/06/2017
        public static DataTable GetPOSDayEnd()
        {
            string strExecution = "[pos].[sp_ManagePOSClosingDate]";

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                return oDq.GetTable();
            }
        }

        public static DataTable UpDatePOSDayEnd()
        {
            string strExecution = "[pos].[sp_ManagePOSClosingDate]";

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@Mode", 1, "A");
                return oDq.GetTable();
            }
        }
        //-AR 07/06/2017

        public static DataSet ExportFromLocal()
        {
            string strExecution = "[pos].[sp_ExportFromLocal]";
            DataSet myDataSet;

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                myDataSet = oDq.GetTables();
            }

            return myDataSet;
        }

        public static void UpdateLocalAfterTransfer(int BillId)
        {
            string strExecution = "[pos].[sp_UpdateLocalAfterTransfer]";

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@LocalBillID", BillId);
                oDq.RunActionQuery();
            }

        }

        public static DataTable MasterUpdateAllowed(string tableName)
        {
            string strExecution = "[pos].[sp_ImportExport]";

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@MTableName", 100, tableName);
                oDq.AddVarcharParam("@Mode", 1, "L");
                return oDq.GetTable();
            }
        }

        public static void UpdateStatus(string tableName, string status, string insertUpdate)
        {
            string strExecution = "[pos].[sp_ImportExport]";

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@MTableName", 100, tableName);
                oDq.AddDateTimeParam("@MDate", DateTime.Now);
                oDq.AddVarcharParam("@MStatus", 1, status);
                oDq.AddVarcharParam("@Mode", 1, insertUpdate);
                oDq.RunActionQuery();
            }

        }

        public static void ImportFromWeb(DataSet ds)
        {
            string strExecution = "[pos].[sp_ImportExport]";

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddTableValueParam("@MItem", ds.Tables["mstITEMS"]);
                oDq.AddTableValueParam("@MItemGroup", ds.Tables["mstITEMGROUPS"]);
                oDq.AddTableValueParam("@MItemRates", ds.Tables["mstITEMRATES"]);
                oDq.AddVarcharParam("@MTableName", 100, ds.Tables["mstITEMS"].TableName.ToUpper());
                oDq.RunActionQuery();
            }
        }
        #endregion

        #region Import Export From Web

        public static DataSet ExportMasterFromWeb()
        {
            string strExecution = "[pos].[sp_ImportMasters]";
            DataSet myDataSet;

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                myDataSet = oDq.GetTables();
            }

            return myDataSet;
        }

        //public static DataSet CheckCoupon()
        //{
        //    string strExecution = "[pos].[sp_GetCouponWithPrefix]";
        //    DataSet myDataSet;

        //    using (DbQuery oDq = new DbQuery(strExecution))
        //    {
        //        myDataSet = oDq.GetTables();
        //    }

        //    return myDataSet;
        //}

        #endregion

    }
}
