﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FoodSmart.Common;
using FoodSmart.DAL.DbManager;
using FoodSmart.Entity;
using FoodSmart.Utilities;

namespace FoodSmart.DAL
{
    public sealed class PricelistDAL
    {
        public static List<IItemRate> GetPriceList(SearchCriteria criteria)
        {
            string strExecution = "[admin].[sp_ManageItemRate]";
            List<IItemRate> rates = new List<IItemRate>();
            SearchCriteria searchCriteria = new SearchCriteria();

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@Mode", 1, "L");
                oDq.AddDateTimeParam("@EffectiveDate", criteria.Date);
                oDq.AddIntegerParam("@UserID", GeneralFunctions.LoginInfo.UserID);
                oDq.AddIntegerParam("@GroupID", criteria.IntegerOption3);
                oDq.AddIntegerParam("@Result", 0);
                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    var rate = new ItemRateEntity(reader);
                    rates.Add(rate);
                }
                reader.Close();
            }
            return rates;
        }

        public static int SavePriceList(string mode, Int64 itemId, Int64 rateId, decimal sellingRate, DateTime effectiveDate)
        {
            string strExecution = "[admin].[sp_ManageItemRate]";
            int result = 0;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@Mode", 1, mode);
                oDq.AddBigIntegerParam("@pk_RATEID", rateId);
                oDq.AddBigIntegerParam("@fk_ITEMID", itemId);
                oDq.AddDateTimeParam("@EffectiveDate", effectiveDate);
                oDq.AddDecimalParam("@SellingRate", 10, 2, sellingRate);
                oDq.AddIntegerParam("@UserID", GeneralFunctions.LoginInfo.UserID);
                oDq.AddIntegerParam("@RateTypeID", 1);
                oDq.AddIntegerParam("@Result", result, QueryParameterDirection.Output);
                oDq.RunActionQuery();
                result = Convert.ToInt32(oDq.GetParaValue("@Result"));
            }
            return result;
        }
    }
}
