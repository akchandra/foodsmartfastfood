﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FoodSmart.Common;
//using POSWebRpt.DAL.DBManager;
using FoodSmart.DAL.DbManager;
using FoodSmart.Entity;
//using Utilities.DbManager;

namespace FoodSmart.DAL
{
    public class InvReportDAL
    {
        public static List<RecipeListEntity> GetRecipeList(ReportCriteria criteria)
        {

            DataSet ds = new DataSet();
            string strExecution = "[inv].[sp_PrintRecipe]";
            List<RecipeListEntity> lstItems = new List<RecipeListEntity>();
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@RecipeType", 1, criteria.TransactionType);
                oDq.AddVarcharParam("@RecipeName", 30, criteria.RecipeName);
                oDq.AddIntegerParam("@Result", 0);
                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    var transaction = new RecipeListEntity(reader);
                    lstItems.Add(transaction);
                }
                reader.Close();
            }
            return lstItems;

        }

        public static List<RecipeDetailEntity> GetRecipeItem(ReportCriteria criteria)
        {

            //DataSet ds = new DataSet();
            string strExecution = "[inv].[sp_PrintRecipeItems]";
            List<RecipeDetailEntity> lstItems = new List<RecipeDetailEntity>();
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@RecipeType", 1, criteria.TransactionType);
                oDq.AddVarcharParam("@RecipeName", 30, criteria.RecipeName);
                oDq.AddIntegerParam("@Result", 0);
                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    var transaction = new RecipeDetailEntity(reader);
                    lstItems.Add(transaction);
                }
                reader.Close();
            }
            return lstItems;

        }

        public static List<ReceiptRegEntity> GetInvReceiptRegister(ReportCriteria criteria)
        {
            List<ReceiptRegEntity> lstData = new List<ReceiptRegEntity>();
            string strExecution = "[inv].[sp_PrintReceiptRegister]";
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddDateTimeParam("@StartDate", criteria.FromDate);
                oDq.AddDateTimeParam("@EndDate", criteria.ToDate);
                oDq.AddIntegerParam("@VendorID", criteria.VendorID);
                oDq.AddIntegerParam("@StoreID", criteria.restID);
                //oDq.AddVarcharParam("@CalledFrom", criteria.cal);
                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    var transaction = new ReceiptRegEntity(reader);
                    lstData.Add(transaction);
                }
                reader.Close();
            }
            return lstData;
        }

        public static List<RcptIssueEntity> GetAllInvItem(ReportCriteria criteria, string CalledFrom)
        {
            //List<RcptIssueEntity> lstData = new List<RcptIssueEntity>();

            string strExecution = "[inv].[sp_PrintRMItems]";
            List<RcptIssueEntity> lstItems = new List<RcptIssueEntity>();
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddDateTimeParam("@StartDate", criteria.FromDate);
                oDq.AddDateTimeParam("@EndDate", criteria.ToDate);
                oDq.AddIntegerParam("@VendorID", criteria.VendorID);
                oDq.AddVarcharParam("@CalledFrom", 1, CalledFrom);
                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    var transaction = new RcptIssueEntity(reader);
                    lstItems.Add(transaction);
                }
                reader.Close();
            }
            return lstItems;

        }

        public DataSet GetVendors(int Id = 0)
        {
            DataSet ds = new DataSet();
            string strExecution = "[inv].[proc_GetVendor]";
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@VendorID", Id);
                ds = oDq.GetTables();

            }
            return ds;

        }

        public DataSet GetRMItems(int Id = 0, int GrpID = 0)
        {
            DataSet ds = new DataSet();
            string strExecution = "[inv].[proc_GetRMItems]";
            using (DbQuery oDq = new DbQuery(strExecution))
            {

                oDq.AddIntegerParam("@ItemGrpID", GrpID);
                oDq.AddIntegerParam("@ItemID", Id);
                ds = oDq.GetTables();
            }
            return ds;

        }

        public DataSet GetRMGroups(int Id = 0)
        {
            DataSet ds = new DataSet();
            string strExecution = "[inv].[proc_GetRMGroups]";
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@RMGroupID", Id);
                ds = oDq.GetTables();
            }
            return ds;
        }

        public DataSet GetStores(int Id, string CalledFrom)
        {
            DataSet ds = new DataSet();
            string strExecution = "[inv].[proc_GetStore]";
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@StoreID", Id);
                oDq.AddVarcharParam("@CalledFrom", 1, CalledFrom);
                ds = oDq.GetTables();
            }
            return ds;
        }

        public static List<StockSummaryEntity> GetInvStockSummary(ReportCriteria criteria, string CalledFrom)
        {
            List<StockSummaryEntity> lstData = new List<StockSummaryEntity>();
            string strExecution = "[inv].[sp_PrintRMStock]";
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddDateTimeParam("@StartDate", criteria.FromDate);
                oDq.AddDateTimeParam("@EndDate", criteria.ToDate);
                oDq.AddIntegerParam("@StoreID", criteria.restID);
                oDq.AddIntegerParam("@RMGroupID", criteria.RMGroupID);
                oDq.AddIntegerParam("@RMID", criteria.RMID);
                oDq.AddVarcharParam("@CalledFrom", 1, CalledFrom);
                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    var transaction = new StockSummaryEntity(reader);
                    lstData.Add(transaction);
                }
                reader.Close();
            }
            return lstData;
        }

        public static List<StockSummaryEntity> GetInvStockRegister(ReportCriteria criteria)
        {
            List<StockSummaryEntity> lstData = new List<StockSummaryEntity>();
            string strExecution = "[inv].[sp_PrintRMStockLedgerOB]";
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddDateTimeParam("@StartDate", criteria.FromDate);
                oDq.AddDateTimeParam("@EndDate", criteria.ToDate);
                oDq.AddIntegerParam("@StoreID", criteria.restID);
                oDq.AddIntegerParam("@RMGroupID", criteria.RMGroupID);
                oDq.AddIntegerParam("@RMID", criteria.RMID);
                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    var transaction = new StockSummaryEntity(reader);
                    lstData.Add(transaction);
                }
                reader.Close();
            }
            return lstData;
        }

        public static List<StockTranItemEntity> GetAllStockTransaction(ReportCriteria criteria)
        {
            List<StockTranItemEntity> lstData = new List<StockTranItemEntity>();
            string strExecution = "[inv].[sp_PrintRMStockLedgerDetails]";
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddDateTimeParam("@StartDate", criteria.FromDate);
                oDq.AddDateTimeParam("@EndDate", criteria.ToDate);
                oDq.AddIntegerParam("@StoreID", criteria.restID);
                oDq.AddIntegerParam("@RMGroupID", criteria.RMGroupID);
                oDq.AddIntegerParam("@RMID", criteria.RMID);
                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    var transaction = new StockTranItemEntity(reader);
                    lstData.Add(transaction);
                }
                reader.Close();
            }
            return lstData;
        }

        public static List<StockSummaryEntity> GetInvStockValue(ReportCriteria criteria)
        {
            List<StockSummaryEntity> lstData = new List<StockSummaryEntity>();

            string strExecution = "[inv].[sp_PrintRMStockValueSummary]";
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddDateTimeParam("@EndDate", criteria.ToDate);
                oDq.AddIntegerParam("@RMGroupID", criteria.RMGroupID);
                oDq.AddIntegerParam("@RMID", criteria.RMID);
                oDq.AddIntegerParam("@StoreID", criteria.restID);
                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    var transaction = new StockSummaryEntity(reader);
                    lstData.Add(transaction);
                }
                reader.Close();
            }
            return lstData;
        }
        public static List<StockSummaryEntity> GetInvStockAlert(ReportCriteria criteria, string AlertType)
        {
            List<StockSummaryEntity> lstData = new List<StockSummaryEntity>();

            string strExecution = "[inv].[sp_PrintRMStockAlert]";
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddDateTimeParam("@EndDate", criteria.ToDate);
                oDq.AddIntegerParam("@RMGroupID", criteria.RMGroupID);
                oDq.AddVarcharParam("@AlertType", 1, AlertType);
                oDq.AddIntegerParam("@StoreID", criteria.restID);
                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    var transaction = new StockSummaryEntity(reader);
                    lstData.Add(transaction);
                }
                reader.Close();
            }
            return lstData;

        }

        public static List<ItemwiseReceiptRegEntity> GetInvItemRcptIssue(ReportCriteria criteria, string CalledFrom)
        {
            string strExecution = "[inv].[sp_PrintItemwiseRM]";
            List<ItemwiseReceiptRegEntity> lstItems = new List<ItemwiseReceiptRegEntity>();
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddDateTimeParam("@StartDate", criteria.FromDate);
                oDq.AddDateTimeParam("@EndDate", criteria.ToDate);
                oDq.AddIntegerParam("@VendorID", criteria.VendorID);
                oDq.AddIntegerParam("@RMGroupID", criteria.RMGroupID);
                oDq.AddIntegerParam("@RMID", criteria.RMID);
                oDq.AddVarcharParam("@CalledFrom", 1, CalledFrom);
                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    var transaction = new ItemwiseReceiptRegEntity(reader);
                    lstItems.Add(transaction);
                }
                reader.Close();
            }
            return lstItems;

        }

        public static List<IssueRegEntity> GetInvIssueRegister(ReportCriteria criteria)
        {
            List<IssueRegEntity> lstData = new List<IssueRegEntity>();
            string strExecution = "[inv].[sp_PrintIssueRegister]";
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddDateTimeParam("@StartDate", criteria.FromDate);
                oDq.AddDateTimeParam("@EndDate", criteria.ToDate);
                oDq.AddIntegerParam("@StoreID", criteria.restID);
                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    var transaction = new IssueRegEntity(reader);
                    lstData.Add(transaction);
                }
                reader.Close();
            }
            return lstData;
        }

    }
}
