﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FoodSmart.Common;
using FoodSmart.DAL.DbManager;
using FoodSmart.Entity;
using FoodSmart.Utilities;

namespace FoodSmart.DAL
{
    public sealed class DiscDAL
    {
        public static DataSet GetAllDiscounts(SearchCriteria searchCriteria)
        {
            DataSet ds = new DataSet();
            using (DbQuery dq = new DbQuery("[sp_ManageDiscount]"))
            {
                dq.AddVarcharParam("@DiscName", 100, searchCriteria.ItemGroupName);
                dq.AddIntegerParam("@DiscCalcID", searchCriteria.IntegerOption1);
                dq.AddVarcharParam("@Mode", 1, "L");
                dq.AddIntegerParam("@RESULT", 0, QueryParameterDirection.Output);
                ds = dq.GetTables();
            }
            return ds;
        }

        public static DiscEntity GetDiscForEdit(int DiscID)
        {
            string strExecution = "[sp_ManageDiscount]";
            DiscEntity Disc = null;

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@Mode", 1, "M");
                oDq.AddIntegerParam("@pk_DiscAppID", DiscID);
                oDq.AddIntegerParam("@Result", 0);
                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    Disc = new DiscEntity(reader);
                }
                reader.Close();
            }
            return Disc;
        }

        public static int SaveDiscount(IDisc Disc, string mode)
        {
            string strExecution = "[sp_ManageDiscount]";
            int result = 0;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@pk_DiscAppID", Disc.DiscAppID);
                oDq.AddVarcharParam("@Mode", 1, mode);
                oDq.AddVarcharParam("@DiscName", 50, Disc.DiscName);
                oDq.AddIntegerParam("@DiscCalcID", Disc.DiscCalcID);
                oDq.AddIntegerParam("@DiscTypeID", Disc.DiscTypeID);
                oDq.AddDateTimeParam("@StartDate", Disc.StartDate);
                oDq.AddDateTimeParam("@EndDate", Disc.EndDate);
                oDq.AddDecimalParam("@DiscPer", 6, 2, Disc.DiscPer);
                oDq.AddIntegerParam("@fk_BaseItemID", Disc.BaseItemID);
                oDq.AddIntegerParam("@fk_FreeItemID", Disc.DiscItemID);
                oDq.AddDecimalParam("@ThresholdAmount", 12, 2, Disc.ThresholeAmt);
                oDq.AddIntegerParam("@UserID", Disc.CreatedBy);
                oDq.AddIntegerParam("@Result", result, QueryParameterDirection.Output);
                oDq.RunActionQuery();
                result = Convert.ToInt32(oDq.GetParaValue("@Result"));
            }
            return result;
        }

        public static int DeleteDiscount(int DiscID)
        {
            string strExecution = "[sp_ManageDiscount]";
            int ret = 0;
            int result = 0;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@pk_DiscAppID", DiscID);
                oDq.AddVarcharParam("@Mode", 1, "D");
                oDq.AddBigIntegerParam("@UserID", GeneralFunctions.LoginInfo.UserID);
                oDq.AddIntegerParam("@Result", result, QueryParameterDirection.Output);
                ret = Convert.ToInt32(oDq.GetScalar());
            }
            return ret;
        }

        public static DataSet GetAllDiscType()
        {
            DataSet ds = new DataSet();
            using (DbQuery dq = new DbQuery("[sp_GetAllDiscType]"))
            {
                //dq.AddVarcharParam("@Mode", 1, "L");
                //dq.AddIntegerParam("@RESULT", 0, QueryParameterDirection.Output);
                ds = dq.GetTables();
            }
            return ds;
        }

        public static DataSet GetAllDiscCalcMethod(int DiscTypeID, int CalcMethodID)
        {
            DataSet ds = new DataSet();
            using (DbQuery dq = new DbQuery("[sp_GetAllDiscCalcMethod]"))
            {
                dq.AddIntegerParam("@DiscTypeID", DiscTypeID);
                dq.AddIntegerParam("@DiscCalcID", CalcMethodID);
                //dq.AddIntegerParam("@RESULT", 0, QueryParameterDirection.Output);
                ds = dq.GetTables();
            }
            return ds;
        }
    }
}
