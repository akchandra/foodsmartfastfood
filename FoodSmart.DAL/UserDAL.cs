﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FoodSmart.Common;
using FoodSmart.DAL.DbManager;
using FoodSmart.Entity;

namespace FoodSmart.DAL
{
    public sealed class UserDAL
    {
        private UserDAL()
        {
        }

        #region User

        public static DataSet ImportUsersFromWeb(SearchCriteria searchCriteria)
        {
            DataSet ds = new DataSet();

            string strExecution = "[webadmin].[uspGetUserForImport]";
            List<IUser> lstUser = new List<IUser>();

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                ds = oDq.GetTables();
            }

            return ds;
        }

        public static DataSet GetAllUsers(SearchCriteria searchCriteria)
        {
            DataSet ds = new DataSet();
            using (DbQuery dq = new DbQuery("[admin].[sp_ManageUSER]"))
            {
                dq.AddVarcharParam("@UserName", 100, searchCriteria.UserName);
                dq.AddVarcharParam("@RoleName", 100, searchCriteria.RoleName);
                dq.AddVarcharParam("@LoginID", 100, searchCriteria.LoginID);
                dq.AddVarcharParam("@Mode", 1, "L");
                dq.AddIntegerParam("@RESULT", 0, QueryParameterDirection.Output);
                ds = dq.GetTables();
            }
            return ds;
        }

        public static DataSet GetAllRoles()
        {
            DataSet ds = new DataSet();
            using (DbQuery dq = new DbQuery("[admin].[sp_ManageROLE]"))
            {
                ds = dq.GetTables();
            }
            return ds;
        }

        public static UserEntity GetUserForEdit(int ID, string CalledFrom)
        {
            string strExecution = "[admin].[sp_ManageUSER]";
            UserEntity oEmp = null;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@pk_UserID", ID);
                oDq.AddVarcharParam("@Mode", 1, CalledFrom);
                //oDq.AddIntegerParam("@UserID", UserID);
                oDq.AddIntegerParam("@Result", 0);
                DataTableReader reader = oDq.GetTableReader();

                while (reader.Read())
                {
                    oEmp = new UserEntity(reader);
                }
                reader.Close();
            }
            return oEmp;
        }

        public static bool ChangePassword(IUser user)
        {
            string strExecution = "[admin].[uspChangePassword]";
            bool result = false;

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@UserId", user.Id);
                oDq.AddVarcharParam("@OldPwd", 50, user.Password);
                oDq.AddVarcharParam("@NewPwd", 50, user.NewPassword);
                oDq.AddBooleanParam("@Result", result, QueryParameterDirection.Output);
                oDq.RunActionQuery();
                result = Convert.ToBoolean(oDq.GetParaValue("@Result"));
            }

            return result;
        }

        public static bool CheckDuplicateLogin(string LoginID)
        {
            string strExecution = "[admin].[uspCheckDuplicateLogin]";
            bool result = false;

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@LoginID", 50, LoginID);
                oDq.AddBooleanParam("@Result", result, QueryParameterDirection.Output);
                oDq.RunActionQuery();
                result = Convert.ToBoolean(oDq.GetParaValue("@Result"));
            }

            return result;
        }

        public static string GetUserLoc(int user)
        {
            string strExecution = "[admin].[sp_GetUserLoc]";
            string result = "";

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@UserId", user);
                oDq.AddVarcharParam("@Result", 100, result, QueryParameterDirection.Output);
                oDq.RunActionQuery();
                result = Convert.ToString(oDq.GetParaValue("@Result"));
            }

            return result;
        }

        public static void ValidateUser(IUser user)
        {
            string strExecution = "[admin].[uspValidateUser]";

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@UserName", 10, user.LoginID);
                oDq.AddVarcharParam("@Password", 50, user.Password);
                DataTableReader reader = oDq.GetTableReader();

                while (reader.Read())
                {
                    user.Id = Convert.ToInt32(reader["UserId"]);
                    user.UserFullName = Convert.ToString(reader["UserName"]);
                    user.LoginID = Convert.ToString(reader["LoginID"]);
                    user.RoleID = Convert.ToInt32(reader["RoleId"]);
                    //user.UserLocation.Id = Convert.ToInt32(reader["LocId"]);
                    user.EmailId = Convert.ToString(reader["emailID"]);
                    user.CurrDate = Convert.ToDateTime(reader["CurrDate"]);
                    //user.RoleID = Convert.ToInt32(reader["RoleId"]);
                    //user.RestID = Convert.ToInt32(reader["fk_RestID"]);
                    //user.SecDeposit = Convert.ToDecimal(reader["SecurityDeposit"]);
                    user.CalcMethod = Convert.ToString(reader["CalcMethod"])=="N"?"F":"B";
                    //user.RestName = Convert.ToString(reader["RestName"]);
                    user.LocName = Convert.ToString(reader["LocName"]);
                    if (reader["fk_LocID"] != DBNull.Value)
                        user.LocID = Convert.ToInt32(reader["fk_LocID"]);
                    else
                        user.LocID = 0;
                }
                reader.Close();
            }
        }

        public static void ValidateWebUser(IUser user)
        {
            string strExecution = "[webadmin].[uspValidateUser]";

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@UserName", 10, user.LoginID);
                oDq.AddVarcharParam("@Password", 50, user.Password);
                DataTableReader reader = oDq.GetTableReader();

                while (reader.Read())
                {
                    user.Id = Convert.ToInt32(reader["UserId"]);
                    user.UserFullName = Convert.ToString(reader["UserName"]);
                    user.LoginID = Convert.ToString(reader["LoginID"]);
                    user.RoleID = Convert.ToInt32(reader["RoleId"]);
                    //user.UserLocation.Id = Convert.ToInt32(reader["LocId"]);
                    user.EmailId = Convert.ToString(reader["emailID"]);
                    user.CurrDate = Convert.ToDateTime(reader["CurrDate"]);
                    //user.RoleID = Convert.ToInt32(reader["RoleId"]);
                    //user.RestID = Convert.ToInt32(reader["fk_RestID"]);
                    //user.SecDeposit = Convert.ToDecimal(reader["SecurityDeposit"]);
                    //user.CalcMethod = Convert.ToString(reader["CalcMethod"]);
                    //user.RestName = Convert.ToString(reader["RestName"]);
                    user.LocName = Convert.ToString(reader["LocName"]);
                    if (reader["fk_LocID"] != DBNull.Value)
                        user.LocID = Convert.ToInt32(reader["fk_LocID"]);
                    else
                        user.LocID = 0;
                }
                reader.Close();
            }
        }


        public static List<IUser> GetUserList(bool isActiveOnly, SearchCriteria searchCriteria)
        {
            string strExecution = "[webadmin].[uspGetUser]";
            List<IUser> lstUser = new List<IUser>();

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddBooleanParam("@IsActiveOnly", isActiveOnly);
                oDq.AddVarcharParam("@SchUserName", 10, searchCriteria.UserName);
                oDq.AddVarcharParam("@SchFirstName", 30, searchCriteria.FirstName);
                oDq.AddVarcharParam("@SortExpression", 50, searchCriteria.SortExpression);
                oDq.AddVarcharParam("@SortDirection", 4, searchCriteria.SortDirection);
                DataTableReader reader = oDq.GetTableReader();

                while (reader.Read())
                {
                    IUser user = new UserEntity(reader);
                    lstUser.Add(user);
                }

                reader.Close();
            }

            return lstUser;
        }

        public static DataSet GetUserByLoc(int LocID)
        {

            DataSet ds = new DataSet();
            using (DbQuery dq = new DbQuery("[admin].[uspGetUserByLoc]"))
            {
                dq.AddIntegerParam("@LocId", LocID);
                ds = dq.GetTables();
            }
            return ds;
        }

        public static int SaveUser(IUser user, int modifiedBy)
        {
            string strExecution = "[webadmin].[uspSaveUser]";
            int result = 0;

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                //oDq.AddVarcharParam("@mode", 1, user.Mode);
                oDq.AddIntegerParam("@UserID", user.Id);
                oDq.AddVarcharParam("@Pwd", 50, user.Password);
                oDq.AddVarcharParam("@FirstName", 30, user.UserFullName);
                //oDq.AddBooleanParam("@Authorizer", user.Authorizer);
                oDq.AddVarcharParam("@UserName", 30, user.LoginID);
                oDq.AddIntegerParam("@RoleID", user.RoleID);
                oDq.AddIntegerParam("@LocID", user.LocID);
                oDq.AddVarcharParam("@EmailId", 50, user.EmailId);
                oDq.AddBooleanParam("@IsActive", user.IsActive);
                //oDq.AddIntegerParam("@CompanyId", companyId);
                oDq.AddIntegerParam("@ModifiedBy", modifiedBy);
                oDq.AddIntegerParam("@Result", result, QueryParameterDirection.Output);
                oDq.RunActionQuery();
                result = Convert.ToInt32(oDq.GetParaValue("@Result"));
            }

            return result;
        }

        //public static int SaveUser(IUser user, int modifiedBy)
        //{
        //    string strExecution = "[admin].[uspSaveUser]";
        //    int result = 0;

        //    using (DbQuery oDq = new DbQuery(strExecution))
        //    {
        //        oDq.AddVarcharParam("@mode", 1, user.Mode);
        //        oDq.AddIntegerParam("@UserID", user.Id);
        //        oDq.AddVarcharParam("@Password", 50, user.Password);
        //        oDq.AddVarcharParam("@UserName", 30, user.UserFullName);
        //        oDq.AddBooleanParam("@Authorizer", user.Authorizer);
        //        oDq.AddVarcharParam("@LoginID", 30, user.LoginID);
        //        oDq.AddIntegerParam("@UserRole", user.RoleID);
        //        oDq.AddIntegerParam("@LocID", user.LocID);
        //        oDq.AddIntegerParam("@RestID", user.RestID);
        //        oDq.AddIntegerParam("@CardID", user.CardID);
        //        oDq.AddVarcharParam("@EmailId", 50, user.EmailId);
        //        oDq.AddBooleanParam("@RStatus", user.IsActive);
        //        //oDq.AddIntegerParam("@CompanyId", companyId);
        //        oDq.AddIntegerParam("@UserAdded", modifiedBy);
        //        oDq.AddIntegerParam("@Result", result, QueryParameterDirection.Output);
        //        oDq.RunActionQuery();
        //        result = Convert.ToInt32(oDq.GetParaValue("@Result"));
        //    }

        //    return result;
        //}

        public static void DeleteUser(int userId, int modifiedBy)
        {
            string strExecution = "[admin].[uspDeleteUser]";

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@UserId", userId);
                oDq.AddIntegerParam("@ModifiedBy", modifiedBy);
                oDq.RunActionQuery();
            }
        }

        public static void ResetPassword(IUser user, int modifiedBy)
        {
            string strExecution = "[admin].[uspResetPassword]";

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@UserId", user.Id);
                oDq.AddVarcharParam("@Pwd", 50, user.Password);
                oDq.AddIntegerParam("@ModifiedBy", modifiedBy);
                oDq.RunActionQuery();

            }
        }

        public static DataSet GetAdminUsers(int LoggedUserId, int locationId)
        {
            DataSet ds = new DataSet();
            using (DbQuery dq = new DbQuery("prcGetAdminUsers"))
            {
                dq.AddIntegerParam("@LoggedUserId", LoggedUserId);
                dq.AddIntegerParam("@locationId", locationId);
                ds = dq.GetTables();
            }
            return ds;
        }

        public static IUser GetUser(int userId, bool isActiveOnly, SearchCriteria searchCriteria)
        {

            string strExecution = "[webadmin].[uspGetUser]";
            IUser oEmp = null;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@UserID", userId);
                oDq.AddBooleanParam("@IsActiveOnly", isActiveOnly);
                oDq.AddVarcharParam("@SortExpression", 50, searchCriteria.SortExpression);
                oDq.AddVarcharParam("@SortDirection", 4, searchCriteria.SortDirection);
                //oDq.AddIntegerParam("@UserID", UserID);
                //oDq.AddIntegerParam("@Result", 0);
                DataTableReader reader = oDq.GetTableReader();

                while (reader.Read())
                {
                    oEmp = new UserEntity(reader);
                }
                reader.Close();
            }
            return oEmp;

            //IUser user = null;

            //using (SqlDataHelper helper = new SqlDataHelper(ConnectionString))
            //{
            //    helper.CommandText = "[webadmin].[uspGetUser]";
            //    helper.CommandType = CommandType.StoredProcedure;
            //    helper.Parameters.Add("@UserId", userId);
            //    helper.Parameters.Add("@IsActiveOnly", isActiveOnly);
            //    helper.Parameters.Add("@SortExpression", searchCriteria.SortExpression);
            //    helper.Parameters.Add("@SortDirection", searchCriteria.SortDirection);
            //    helper.Open();
            //    helper.ExecuteReader(CommandBehavior.CloseConnection);

            //    while (helper.DataReader.Read())
            //    {
            //        user = new UserEntity(helper.DataReader);
            //    }

            //    helper.Close();
            //}

            //return user;
        }
        public static DataSet GetUserById(string LoginId)
        {
            string strExecution = "prcgetUserById";
            DataSet reader = null;

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@LoginId", 20, LoginId);
                reader = oDq.GetTables();
            }

            return reader;
        }

        #endregion

        #region Role

        public static List<IRole> GetRole(bool isActiveOnly, SearchCriteria searchCriteria)
        {
            string strExecution = "[webadmin].[uspGetRole]";
            List<IRole> lstRole = new List<IRole>();

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddBooleanParam("@IsActiveOnly", isActiveOnly);
                oDq.AddVarcharParam("@SchRole", 50, searchCriteria.RoleName);
                oDq.AddVarcharParam("@SortExpression", 50, searchCriteria.SortExpression);
                oDq.AddVarcharParam("@SortDirection", 4, searchCriteria.SortDirection);
                DataTableReader reader = oDq.GetTableReader();

                while (reader.Read())
                {
                    IRole role = new RoleEntity(reader);
                    lstRole.Add(role);
                }

                reader.Close();
            }

            return lstRole;
        }

        public static RoleEntity GetRole(int roleId, bool isActiveOnly, SearchCriteria searchCriteria)
        {
            string strExecution = "[webadmin].[uspGetRole]";
            RoleEntity role = null;

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@RoleId", roleId);
                oDq.AddBooleanParam("@IsActiveOnly", isActiveOnly);
                oDq.AddVarcharParam("@SortExpression", 50, searchCriteria.SortExpression);
                oDq.AddVarcharParam("@SortDirection", 4, searchCriteria.SortDirection);
                DataTableReader reader = oDq.GetTableReader();

                while (reader.Read())
                {
                    role = new RoleEntity(reader);
                }

                reader.Close();
            }

            return role;
        }

        public static int SaveRole(IRole role, int companyID, string xmlDoc, int modifiedBy)
        {
            string strExecution = "[admin].[uspSaveRole]";
            int result = 0;

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@RoleID", role.Id);
                oDq.AddVarcharParam("@RoleName", 50, role.Name);
                oDq.AddIntegerParam("@CompanyId", companyID);
                oDq.AddNVarcharParam("@XMLDoc", xmlDoc);
                oDq.AddIntegerParam("@ModifiedBy", modifiedBy);
                oDq.AddIntegerParam("@Result", result, QueryParameterDirection.Output);
                oDq.RunActionQuery();
                result = Convert.ToInt32(oDq.GetParaValue("@Result"));
            }

            return result;
        }

        public static void DeleteRole(int roleId, int modifiedBy)
        {
            string strExecution = "[admin].[uspDeleteRole]";

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@RoleId", roleId);
                oDq.AddIntegerParam("@ModifiedBy", modifiedBy);
                oDq.RunActionQuery();
            }
        }

        public static void ChangeRoleStatus(int roleId, bool status, int modifiedBy)
        {
            string strExecution = "[admin].[uspChangeRoleStatus]";

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@RoleId", roleId);
                oDq.AddBooleanParam("@RoleStatus", status);
                oDq.AddIntegerParam("@ModifiedBy", modifiedBy);
                oDq.RunActionQuery();
            }
        }

        public static List<IRoleMenu> GetMenuByRole(int roleId, int mainId)
        {
            string strExecution = "[admin].[uspGetMenuByRole]";
            List<IRoleMenu> lstMenu = new List<IRoleMenu>();

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@RoleId", roleId);
                oDq.AddIntegerParam("@MainId", mainId);
                DataTableReader reader = oDq.GetTableReader();

                while (reader.Read())
                {
                    IRoleMenu menu = new RoleMenuEntity(reader);
                    lstMenu.Add(menu);
                }

                reader.Close();
            }

            return lstMenu;
        }

        public static RoleMenuEntity GetMenuAccessByUser(int userId, int menuId)
        {
            string strExecution = "[webadmin].[uspGetMenuAccessByUser]";
            RoleMenuEntity roleMenu = null;

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@UserId", userId);
                oDq.AddIntegerParam("@MenuId", menuId);
                DataTableReader reader = oDq.GetTableReader();

                while (reader.Read())
                {
                    roleMenu = new RoleMenuEntity(reader);
                }

                reader.Close();
            }

            return roleMenu;
        }

        public static int AddEditUser(IUser usr, string Mode, int CreatedBy)
        {
            string strExecution = "[admin].[sp_ManageUser]";
            int Result = 0;

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@UserAdded", CreatedBy);
                oDq.AddIntegerParam("@pk_UserID", usr.Id);
                oDq.AddVarcharParam("@Mode", 1, Mode);
                oDq.AddVarcharParam("@UserName", 30, usr.UserFullName);
                oDq.AddVarcharParam("@LoginID", 30, usr.LoginID);
                oDq.AddIntegerParam("@LocID", usr.LocID);
                oDq.AddIntegerParam("@UserRole", usr.RoleID);
                oDq.AddVarcharParam("@Password", 100, usr.Password);
                oDq.AddIntegerParam("@RestID", usr.RestID);
                //oDq.AddIntegerParam("@CardID", usr.CardID);
                oDq.AddVarcharParam("@EmailID", 100, usr.EmailId);
                oDq.AddVarcharParam("@CardNo", 100, usr.CardNoOutside);

                oDq.AddIntegerParam("@RESULT", Result, QueryParameterDirection.Output);
                oDq.RunActionQuery();

                Result = Convert.ToInt32(oDq.GetParaValue("@RESULT"));

            }
            return Result;
        }

        public static DataTable GetUserSpecificMenuList(int UserID)
        {
            string strExecution = "[webadmin].[uspGetUserSpecificMenuList]";
            DataTable dt = new DataTable();

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@UserId", UserID);
                dt = oDq.GetTable();
            }
            return dt;
        }


        public static void SavePOSUserLogin(IUser usr, string UpdateType)
        {
            string strExecution = "[dbo].[SP_insertUserDetails]";
            //int result = 0;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                //oDq.AddIntegerParam("@fk_CardID", card.CardID);
                oDq.AddVarcharParam("@UpdateType", 1, UpdateType);
                oDq.AddVarcharParam("@TerminalID", 100, Environment.MachineName);
                oDq.AddIntegerParam("@UserID", usr.Id);
                oDq.AddIntegerParam("@LocID", usr.LocID);
                //oDq.AddIntegerParam("@Result", result, QueryParameterDirection.Output);
                oDq.RunActionQuery();
                //TranID = Convert.ToInt32(oDq.GetScalar());
                //result = Convert.ToInt32(oDq.GetParaValue("@Result"));

            }
        }

        public static DataSet GetRoleWithUserID(int LoggedUserId)
        {
            DataSet ds = new DataSet();
            string strExecution = "[webadmin].[uspGetRoleWithUserID]";
            //int result = 0;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@UserID", LoggedUserId);
                ds = oDq.GetTables();
            }
            return ds;
        }

        #endregion
    }
}
