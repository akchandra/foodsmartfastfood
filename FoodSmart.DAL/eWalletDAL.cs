﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FoodSmart.Common;
using FoodSmart.DAL.DbManager;
using FoodSmart.Entity;
using FoodSmart.Utilities;

namespace FoodSmart.DAL
{
    public sealed class eWalletDAL
    {
        #region e-Wallets
        public static DataSet GetAlleWallets(SearchCriteria searchCriteria)
        {
            DataSet ds = new DataSet();
            using (DbQuery dq = new DbQuery("[admin].[sp_ManageEWallet]"))
            {
                dq.AddVarcharParam("@eWalletGateway", 100, searchCriteria.eWalletGateway);
                dq.AddVarcharParam("@RegdMobileNo", 100, searchCriteria.RegMobNo);
                dq.AddVarcharParam("@GateWayType", 1, searchCriteria.GatewayType);
                dq.AddVarcharParam("@RegdNo", 100, searchCriteria.RegdNo);
                dq.AddVarcharParam("@Mode", 1, "L");
                dq.AddIntegerParam("@LocID", searchCriteria.LocID);
                dq.AddIntegerParam("@RESULT", 0, QueryParameterDirection.Output);
                ds = dq.GetTables();
            }
            return ds;
        }
        public static int DeleteEWallet(int WalletID, int UserID)
        {
            string strExecution = "[admin].[sp_ManageEWallet]";
            int ret = 0;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@pk_eWalletID", WalletID);
                oDq.AddVarcharParam("@Mode", 1, "D");
                oDq.AddBigIntegerParam("@UserID", UserID);
                oDq.AddIntegerParam("@Result", 0);
                ret = Convert.ToInt32(oDq.GetScalar());
            }
            return ret;
        }

        public static int SaveEWallet(IeWallet inv, string mode, int UserID)
        {
            string strExecution = "[admin].[sp_ManageEWallet]";
            int result = 0;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@pk_eWalletID", inv.eWalletID);
                //oDq.AddIntegerParam("@LocID", inv.LocID);
                oDq.AddVarcharParam("@Mode", 1, mode);
                oDq.AddVarcharParam("@GateWayType", 1, inv.GatewayType);
                oDq.AddVarcharParam("@BankName", 50, inv.BankName);
                oDq.AddVarcharParam("@RegdMobileNo", 50, inv.RegdMobileNo);
                oDq.AddVarcharParam("@eWalletGateway", 50, inv.eWalletGateway);
                oDq.AddVarcharParam("@RegdNo", 50, inv.RegdNo);
                oDq.AddIntegerParam("@UserID", UserID);
                oDq.AddIntegerParam("@Result", result, QueryParameterDirection.Output);
                oDq.RunActionQuery();
                result = Convert.ToInt32(oDq.GetParaValue("@Result"));
            }
            return result;
        }

        public static eWalletEntity GeteWalletForEdit(int WalletID)
        {
            string strExecution = "[admin].[sp_ManageEWallet]";
            eWalletEntity itemgrp = null;

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@Mode", 1, "M");
                oDq.AddIntegerParam("@pk_eWalletID", WalletID);
                oDq.AddIntegerParam("@Result", 0);
                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    itemgrp = new eWalletEntity(reader);
                }
                reader.Close();
            }
            return itemgrp;
        }

        #endregion
    }
}
