﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FoodSmart.Common;
using FoodSmart.DAL.DbManager;
using FoodSmart.Entity;
using FoodSmart.Utilities;

namespace FoodSmart.DAL
{
    public sealed class RecipeDAL
    {
        public static List<RecipeEntity> GetAllRecipies(SearchCriteria criteria)
        {
            string strExecution = "[inv].[sp_ManageRecipe]";
            List<RecipeEntity> recipies = new List<RecipeEntity>();
            SearchCriteria searchCriteria = new SearchCriteria();

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@Mode", 1, "L");
                oDq.AddVarcharParam("@RecipeType", 1, criteria.Type);
                oDq.AddVarcharParam("@RecipeName", 50, criteria.RecipeName);
                oDq.AddIntegerParam("@fk_UserID", GeneralFunctions.LoginInfo.UserID);
                oDq.AddIntegerParam("@Result", 0);
                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    var recipe = new RecipeEntity(reader);
                    recipies.Add(recipe);
                }
                reader.Close();
            }
            return recipies;
        }

        public static RecipeEntity GetRecipe(int id)
        {
            string strExecution = "[inv].[sp_ManageRecipe]";
            RecipeEntity recipe = null;

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@Mode", 1, "M");
                oDq.AddIntegerParam("@pk_RecipeID", id);
                oDq.AddIntegerParam("@fk_UserID", GeneralFunctions.LoginInfo.UserID);
                oDq.AddIntegerParam("@Result", 0);
                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    recipe = new RecipeEntity(reader);
                }

                reader.Close();
            }
            return recipe;
        }


        public static List<IRecipeItem> GetAllRecipieItems(Int64 id)
        {
            string strExecution = "[inv].[sp_ManageRecipe]";
            List<IRecipeItem> items = new List<IRecipeItem>();

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@Mode", 1, "I");
                oDq.AddBigIntegerParam("@pk_RecipeID", id);
                oDq.AddIntegerParam("@fk_UserID", GeneralFunctions.LoginInfo.UserID);
                oDq.AddIntegerParam("@Result", 0);
                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    var recipe = new RecipeItemEntity(reader);
                    items.Add(recipe);
                }
                reader.Close();
            }
            return items;
        }

        public static int DeleteRecipe(int id)
        {
            string strExecution = "[inv].[sp_ManageRecipe]";
            int ret = 0;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@Mode", 1, "D");
                oDq.AddIntegerParam("@pk_RecipeID", id);
                oDq.AddBigIntegerParam("@fk_UserID", GeneralFunctions.LoginInfo.UserID);
                oDq.AddIntegerParam("@Result", 0);
                ret = Convert.ToInt32(oDq.GetScalar());
            }
            return ret;
        }

        public static int SaveRecipe(IRecipe recipe, string mode, DataSet ds)
        {
            string strExecution = "[inv].[sp_ManageRecipe]";
            int result = 0;
            ds.Tables[0].Columns.Remove("ItemRate");
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@Mode", 1, mode);
                //passing  table as parameter
                oDq.AddTableValueParam("@MParam", ds.Tables[0], QueryParameterDirection.Input);
                oDq.AddIntegerParam("@pk_RecipeID", recipe.Id);
                oDq.AddVarcharParam("@ProcessedAt", 1, recipe.ProcessedAt);
                oDq.AddIntegerParam("@fk_ItemID", recipe.ItemId);
                oDq.AddVarcharParam("@RecipeType", 1, recipe.Type);
                oDq.AddVarcharParam("@RecipeName", 50, recipe.Name);
                oDq.AddDecimalParam("@noofplates", 12, 2, recipe.NoofPlates);
                oDq.AddDateTimeParam("@DateIntroduced", recipe.DateIntroduced);
                oDq.AddVarcharParam("@UOM", 10, recipe.UOM);
                oDq.AddDecimalParam("@TotalQty", 12, 2, recipe.TotalQuantity);
                oDq.AddVarcharParam("@RecipeDetail", 200, recipe.RecipeDetail);
                oDq.AddBooleanParam("@Rstatus", recipe.Status);
                oDq.AddIntegerParam("@fk_UserID", GeneralFunctions.LoginInfo.UserID);
                oDq.AddIntegerParam("@Result", result, QueryParameterDirection.Output);
                oDq.RunActionQuery();
                result = Convert.ToInt32(oDq.GetParaValue("@Result"));
            }
            return result;
        }

        public static List<IRecipe> GetItems(string itemType)
        {
            string strExecution = "[inv].[sp_GetRMItems]";
            List<IRecipe> recipies = new List<IRecipe>();
            SearchCriteria searchCriteria = new SearchCriteria();

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@ItemType", 1, itemType);
                oDq.AddIntegerParam("@ItemID", 0);
                oDq.AddIntegerParam("@Result", 0);
                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    var recipe = new RecipeEntity();
                    recipe.Id = Convert.ToInt32(reader["ItemId"]);
                    recipe.Name = Convert.ToString(reader["ItemName"]);
                    recipe.UOM = Convert.ToString(reader["UOM"]);
                    recipe.Quantity = Convert.ToDecimal(reader["Qty"]);
                    recipe.Rate = Convert.ToDecimal(reader["Rate"]);
                    recipe.Amount = Convert.ToDecimal(reader["Amount"]);
                    recipies.Add(recipe);
                }
                reader.Close();
            }
            return recipies;
        }

        public static IRecipe GetItem(string itemType, int itemId)
        {
            string strExecution = "[inv].[sp_GetRMItems]";
            IRecipe recipe = null;

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@ItemType", 1, itemType);
                oDq.AddIntegerParam("@ItemID", itemId);
                oDq.AddIntegerParam("@Result", 0);
                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    recipe = new RecipeEntity();
                    recipe.Id = Convert.ToInt32(reader["ItemId"]);
                    recipe.Name = Convert.ToString(reader["ItemName"]);
                    recipe.UOM = Convert.ToString(reader["UOM"]);
                    recipe.Quantity = Convert.ToDecimal(reader["Qty"]);
                    recipe.Rate = Convert.ToDecimal(reader["Rate"]);
                    recipe.Amount = Convert.ToDecimal(reader["Amount"]);
                }
                reader.Close();
            }
            return recipe;
        }

        public static List<IItemGroup> GetItemGroups()
        {
            string strExecution = "[dbo].[sp_GetItemGroups]";
            List<IItemGroup> groups = new List<IItemGroup>();

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    var group = new ItemGroupEntity(reader);
                    groups.Add(group);
                }
                reader.Close();
            }
            return groups;
        }

        public static List<IItemGroup> GetItemGroupByType(string type)
        {
            string strExecution = "[admin].[sp_ManageITEMGROUP]";
            List<IItemGroup> groups = new List<IItemGroup>();

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@Mode", 1, "L");
                oDq.AddVarcharParam("@RorF", 1, type);
                oDq.AddIntegerParam("@Result", 0);
                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    var group = new ItemGroupEntity(reader);
                    groups.Add(group);
                }
                reader.Close();
            }
            return groups;
        }

        public static List<IRecipe> GetItemsByGroup(string itemType, int itemGroupId)
        {
            string strExecution = "[dbo].[sp_GetItemsByGroup]";
            List<IRecipe> recipies = new List<IRecipe>();
            SearchCriteria searchCriteria = new SearchCriteria();

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@ItemType", 1, itemType);
                oDq.AddIntegerParam("@ItemGroupID", itemGroupId);
                oDq.AddIntegerParam("@Result", 0);
                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    var recipe = new RecipeEntity();
                    recipe.Id = Convert.ToInt32(reader["ItemId"]);
                    recipe.Name = Convert.ToString(reader["ItemName"]);
                    recipe.UOM = Convert.ToString(reader["UOM"]);
                    recipe.Quantity = Convert.ToDecimal(reader["Qty"]);
                    recipe.Rate = Convert.ToDecimal(reader["Rate"]);
                    recipe.Amount = Convert.ToDecimal(reader["Amount"]);
                    recipies.Add(recipe);
                }
                reader.Close();
            }
            return recipies;
        }
    }
}
