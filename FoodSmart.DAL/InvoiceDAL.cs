﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FoodSmart.Common;
using FoodSmart.DAL.DbManager;
using FoodSmart.Entity;
using FoodSmart.Utilities;
using System.Data.SqlClient;

namespace FoodSmart.DAL
{
    public sealed class InvoiceDAL
    {
        public static int SaveInvoiceTran(IInvoice inv, string mode, DataSet ds)
        {
            string strExecution = "[pos].[sp_ManageBILLS]";
            int result = 0;
            SqlCommand SqlCmd = new SqlCommand();
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                ds.Tables[0].Columns.Remove("pk_BillSrl");
                ds.Tables[0].Columns.Remove("itemdescr");
                ds.Tables[0].Columns.Remove("GSTPer");

                oDq.AddIntegerParam("@pk_BillID", inv.InvoiceID);
                oDq.AddVarcharParam("@Mode", 1, mode);
                oDq.AddIntegerParam("@fk_CustID", inv.CustID);
                oDq.AddIntegerParam("@fk_RefundReason", inv.RefundReasonID);
                oDq.AddIntegerParam("@UserID", inv.UserID);
                oDq.AddIntegerParam("@fk_LocID", inv.LocID);
                oDq.AddIntegerParam("@fk_FisYearID", inv.YearID);
                oDq.AddIntegerParam("@billNo", inv.InvoiceID);
                oDq.AddVarcharParam("@BillType", 1, inv.BillType);
                oDq.AddVarcharParam("@MobileNo", 10, inv.MobileNo);
                oDq.AddVarcharParam("@CustName", 100, inv.CustName);
                oDq.AddDateTimeParam("@BillDate", inv.BillDate);
                oDq.AddDecimalParam("@BillAmount", 12, 2, inv.BillAmount);
                oDq.AddDecimalParam("@RoundOff", 12, 2, inv.RoundOff);
                oDq.AddVarcharParam("@PMode", 1, inv.PaymentMode);
                oDq.AddDecimalParam("@DiscPer", 12, 2, inv.DiscountPer);
                oDq.AddIntegerParam("@fk_DiscReason", inv.DiscReasonID);
                oDq.AddIntegerParam("@Result", result, QueryParameterDirection.Output);
                oDq.AddVarcharParam("@MachineName", 100, inv.MachineName);
                oDq.AddDecimalParam("@Cashamt", 12, 2, inv.CashAmount);
                oDq.AddDecimalParam("@Cardamt", 12, 2, inv.CCAmount);
                oDq.AddDecimalParam("@MPamt", 12, 2, inv.MPAmount);
                oDq.AddDecimalParam("@Couponamt", 12, 2, inv.CouponAmt);
                oDq.AddDecimalParam("@EwalletAmt", 12, 2, inv.eWalletAmount);
                oDq.AddIntegerParam("@CouponNo", inv.CouponNo);
                oDq.AddIntegerParam("@CouponID", inv.CouponID);
                oDq.AddVarcharParam("@Prefix", 3, inv.Prefix);
                //passing  table as parameter
                oDq.AddTableValueParam("@MParam", ds.Tables[0], QueryParameterDirection.Input);

                oDq.RunActionQuery();
                result = Convert.ToInt32(oDq.GetParaValue("@Result"));
            }
            return result;
        }

        public static int SaveInvoiceServer(IInvoice inv, string mode, DataSet ds)
        {
            string strExecution = "[pos].[sp_ManageBILLS]";
            int result = 0;
            SqlCommand SqlCmd = new SqlCommand();
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                //ds.Tables[0].Columns.Remove("pk_BillSrl");
                //ds.Tables[0].Columns.Remove("hsncode");
                //ds.Tables[0].Columns.Remove("Canceltag");
                //ds.Tables[0].Columns.Remove("fk_mainitemgroupID");
                //ds.Tables[0].Columns.Remove("itemtype");
                //ds.Tables[0].Columns.Remove("BillType");
                //ds.Tables[0].Columns.Remove("BillDate");

                oDq.AddIntegerParam("@pk_BillID", inv.InvoiceID);
                oDq.AddVarcharParam("@Mode", 1, mode);
                oDq.AddIntegerParam("@fk_CustID", inv.CustID);
                oDq.AddIntegerParam("@fk_RefundReason", inv.RefundReasonID);
                oDq.AddIntegerParam("@UserID", inv.UserID);
                oDq.AddIntegerParam("@fk_LocID", inv.LocID);
                oDq.AddIntegerParam("@fk_FisYearID", inv.YearID);
                oDq.AddIntegerParam("@billNo", inv.InvoiceID);
                oDq.AddVarcharParam("@BillType", 1, inv.BillType);
                oDq.AddVarcharParam("@MobileNo", 10, inv.MobileNo);
                oDq.AddVarcharParam("@CustName", 100, inv.CustName);
                oDq.AddDateTimeParam("@BillDate", inv.BillDate);
                oDq.AddDecimalParam("@BillAmount", 12, 2, inv.BillAmount);
                oDq.AddDecimalParam("@RoundOff", 12, 2, inv.RoundOff);
                oDq.AddVarcharParam("@PMode", 1, inv.PaymentMode);
                oDq.AddDecimalParam("@DiscPer", 12, 2, inv.DiscountPer);
                oDq.AddIntegerParam("@fk_DiscReason", inv.DiscReasonID);
                oDq.AddIntegerParam("@Result", result, QueryParameterDirection.Output);
                oDq.AddVarcharParam("@MachineName", 1, inv.MachineName);
                oDq.AddDecimalParam("@Cashamt", 12, 2, inv.CashAmount);
                oDq.AddDecimalParam("@Cardamt", 12, 2, inv.CCAmount);
                oDq.AddDecimalParam("@CouponAmt", 12, 2, inv.CouponAmt);
                oDq.AddDecimalParam("@MPamt", 12, 2, inv.MPAmount);
                oDq.AddDecimalParam("@EwalletAmt", 12, 2, inv.eWalletAmount);
                //passing  table as parameter
                oDq.AddTableValueParam("@MParam", ds.Tables[0], QueryParameterDirection.Input);

                oDq.RunActionQuery();
                result = Convert.ToInt32(oDq.GetParaValue("@Result"));
            }
            return result;
        }

        public static bool CheckRefundPossible(string CardNoInside, int Serial)
        {
            string strExecution = "[pos].[SP_CheckRefundPossible]";

            DataSet dsCard = new DataSet();

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@CardNoInside", 20, CardNoInside);
                oDq.AddIntegerParam("@Srl", Serial);
                dsCard = oDq.GetTables();
            }
            if (dsCard.Tables[0].Rows.Count > 0)
                return true;
            else
                return false;
        }

        public static DataSet CheckItemRefundPossible(int CardID, int Serial, int fk_ItemID, int RestID, DateTime Cdt)
        {
            string strExecution = "[pos].[SP_CheckItemRefundPossible]";

            DataSet dsCard = new DataSet();

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@fk_ItemID", fk_ItemID);
                oDq.AddDateTimeParam("@CurrentDate", Cdt);
                oDq.AddIntegerParam("@CardID", CardID);
                oDq.AddIntegerParam("@RestID", RestID);
                oDq.AddIntegerParam("@Serial", Serial);

                dsCard = oDq.GetTables();
            }
            return dsCard;
        }

        public static DataSet GetAllReason()
        {
            DataSet dsReason = new DataSet();
            string ProcName = "[admin].[sp_GetRefundReason]";
            DAL.DbManager.DbQuery dquery = new DAL.DbManager.DbQuery(ProcName);
            dsReason = dquery.GetTables();
            return dsReason;
            
        }

        public static DataSet GetDiscountReason()
        {
            DataSet dsReason = new DataSet();
            string ProcName = "[admin].[sp_GetDiscountReason]";
            DAL.DbManager.DbQuery dquery = new DAL.DbManager.DbQuery(ProcName);
            dsReason = dquery.GetTables();
            return dsReason;
        }

        public static DataSet GetAllBills(int LocID)
        {
            DataSet ds = new DataSet();
            using (DbQuery dq = new DbQuery("[pos].[sp_GetAllBIlls]"))
            {
                dq.AddIntegerParam("@LocID", LocID);
                ds = dq.GetTables();
            }
            return ds;

        }

        public static DataTable GetHotItems()
        {
            string ProcName = "[pos].[sp_GetHotItems]";
            DAL.DbManager.DbQuery dquery = new DAL.DbManager.DbQuery(ProcName);
            return dquery.GetTable();
        }

        public static DataSet DisplayDetailItems(SearchCriteria searchCriteria)
        {
            DataSet ds = new DataSet();
            using (DbQuery dq = new DbQuery("[pos].[sp_DisplayDetails]"))
            {
                dq.AddVarcharParam("@BillType", 100, searchCriteria.BillType);
                dq.AddVarcharParam("@BillNo", 100, searchCriteria.BillNo);
                dq.AddIntegerParam("@BillID", searchCriteria.BillID);
                dq.AddIntegerParam("@RESULT", 0, QueryParameterDirection.Output);
                ds = dq.GetTables();
            }
            return ds;
        }

        public static DataSet GetCashTranForPrinting(int CashSlipID)
        {
            DataSet ds = new DataSet();
            using (DbQuery dq = new DbQuery("[pos].[sp_CashTransaction]"))
            {
                dq.AddIntegerParam("@CashSlipid", CashSlipID);
                ds = dq.GetTables();
            }
            return ds;
        }

        public static DataSet GetInvoiceForPrinting(int InvoiceID, string InvType)
        {
            DataSet ds = new DataSet();
            using (DbQuery dq = new DbQuery("[pos].[sp_PrintInvoice]"))
            {
                dq.AddVarcharParam("@BillType", 1, InvType);
                dq.AddIntegerParam("@BillID", InvoiceID);
                dq.AddIntegerParam("@Result", 0);
                ds = dq.GetTables();
            }
            return ds;
        }

        public static DataSet GetCashierSummaryForPrinting(int CashierID)
        {
            DataSet ds = new DataSet();
            using (DbQuery dq = new DbQuery("[pos].[sp_rptCashierSummary]"))
            {
                dq.AddDateTimeParam("@StartDate", DateTime.Now);
                dq.AddDateTimeParam("@EndDate", DateTime.Now);
                dq.AddIntegerParam("@CashierID", CashierID);
                ds = dq.GetTables();
            }
            return ds;
        }

        public static void SavePOSUserLogin(int UserID)
        {
            string strExecution = "[dbo].[SP_insertUserDetails]";
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@UserID", UserID);
                oDq.AddVarcharParam("@UpdateType", 1, "U");
                oDq.RunActionQuery();
            }
        }
        
        public static void UpdateTempCardStatus(string CardNoInside, bool Stat)
        {
            string strExecution = "[dbo].[SP_UpdateCardStatus]";
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@CardNoInside", 11, CardNoInside);
                oDq.AddBooleanParam("@LockStatus", Stat);
                oDq.RunActionQuery();
            }
        }

        public static DataSet ItemWiseSalesRegister(int LocID)
        {

            DataSet ds = new DataSet();
            var dateAndTime = DateTime.Now;
            var date1 = dateAndTime.Date.ToString("yyyy-MM-dd").ToDateTime();
            

            using (DbQuery dq = new DbQuery("[dbo].[sp_rpt_ITEMWISESALE]"))
            {
                dq.AddDateTimeParam("@StartDate", date1);
                dq.AddDateTimeParam("@EndDate", date1);
                dq.AddIntegerParam("@LocID", LocID);
                ds = dq.GetTables();
            }
            return ds;
        }

        public static DataSet ItemWiseSalesRegisterScreen()
        {

            DataSet ds = new DataSet();
            var dateAndTime = DateTime.Now;
            var date1 = dateAndTime.Date.ToString("yyyy-MM-dd").ToDateTime();


            using (DbQuery dq = new DbQuery("[dbo].[sp_rpt_ITEMWISESALESCREEN]"))
            {
                dq.AddDateTimeParam("@StartDate", date1);
                dq.AddDateTimeParam("@EndDate", date1);
                //dq.AddIntegerParam("@ItemGroupID", RestID);
                ds = dq.GetTables();
            }
            return ds;
        }

        public static DataSet GetBillWithDate(DateTime dt)
        {
            DataSet ds = new DataSet();
            using (DbQuery dq = new DbQuery("[pos].[sp_GetAllBIlls]"))
            {
                dq.AddDateTimeParam("@InvDate", dt);
                ds = dq.GetTables();
            }
            return ds;

        }

        public static DataSet GetMobileNo(string MobileNo)
        {
            DataSet ds = new DataSet();
            using (DbQuery dq = new DbQuery("[pos].[sp_GetMobileNo]"))
            {
                dq.AddVarcharParam("@MobileNo", 10, MobileNo);
                ds = dq.GetTables();
            }
            return ds;
        }
    }
}
