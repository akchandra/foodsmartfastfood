﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using FoodSmart.Common;
using FoodSmart.DAL.DbManager;
using FoodSmart.Entity;
using FoodSmart.Utilities;

namespace FoodSmart.DAL
{
    public sealed class LocationDAL
    {
        public static List<ILocation> GetAllWebLoc(SearchCriteria searchCriteria)
        {
            string strExecution = "[admin].[sp_ManageLocation]";
            List<ILocation> lstUser = new List<ILocation>();

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@pk_LocID", searchCriteria.LocID);
                oDq.AddVarcharParam("@LocName", 10, searchCriteria.LocName);
                oDq.AddVarcharParam("@SortExpression", 50, searchCriteria.SortExpression);
                oDq.AddVarcharParam("@SortDirection", 4, searchCriteria.SortDirection);
                oDq.AddVarcharParam("@Mode", 1, searchCriteria.QueryStat);
                oDq.AddIntegerParam("@RESULT", 0, QueryParameterDirection.Output);
                DataTableReader reader = oDq.GetTableReader();

                while (reader.Read())
                {
                    ILocation user = new LocationEntity(reader);
                    lstUser.Add(user);
                }

                reader.Close();
            }

            return lstUser;
        }

        public static DataSet GetAllLoc(SearchCriteria searchCriteria)
        {
            DataSet ds = new DataSet();
            using (DbQuery dq = new DbQuery("[admin].[sp_ManageLocation]"))
            {
                dq.AddVarcharParam("@LocName", 100, searchCriteria.LocName);
                dq.AddVarcharParam("@Mode", 1, "L");
                dq.AddIntegerParam("@IgnoreLocation", searchCriteria.IntegerOption1);
                dq.AddIntegerParam("@RESULT", 0, QueryParameterDirection.Output);
                ds = dq.GetTables();
            }
            return ds;
        }

        public static int SaveLocation(ILocation Loc, int UserID, string mode)
        {
            string strExecution = "[admin].[sp_ManageLocation]";
            int result = 0;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@pk_LocID", Loc.LocID);
                oDq.AddVarcharParam("@Mode", 1, mode);
                oDq.AddVarcharParam("@LocName", 50, Loc.LocName);
                oDq.AddVarcharParam("@LocAddress1", 300, Loc.LocAddress1);
                oDq.AddVarcharParam("@LocAddress2", 300, Loc.LocAddress2);
                oDq.AddIntegerParam("@BillNoLength", Loc.BillNoLength);
                oDq.AddVarcharParam("@ServiceTaxNo", 50, Loc.ServTaxNo);
                oDq.AddVarcharParam("@GSTNo", 50, Loc.GSTNo);
                oDq.AddVarcharParam("@LocType", 1, Loc.LocType);
                oDq.AddVarcharParam("@OwnOrFran", 50, Loc.OwnOrFran);
                oDq.AddVarcharParam("@PrinterName", 50, Loc.PrinterName);
                oDq.AddIntegerParam("@UserID", UserID);
                oDq.AddBooleanParam("@Composit", Loc.Composit);
                oDq.AddDateTimeParam("@OpStkDate", Loc.dtOpStock);
                oDq.AddIntegerParam("@Result", result, QueryParameterDirection.Output);
                oDq.RunActionQuery();
                result = Convert.ToInt32(oDq.GetParaValue("@Result"));
            }
            return result;
        }

        public static LocationEntity GetLocForEdit(int ID, string CalledFrom, int UserID)
        {
            string strExecution = "[admin].[sp_ManageLocation]";
            LocationEntity oLoc = null;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@pk_CardID", ID);
                oDq.AddVarcharParam("@Mode", 1, CalledFrom);
                //oDq.AddIntegerParam("@UserID", UserID);
                oDq.AddIntegerParam("@Result", 0);
                DataTableReader reader = oDq.GetTableReader();

                while (reader.Read())
                {
                    oLoc = new LocationEntity(reader);
                }
                reader.Close();
            }
            return oLoc;
        }

        public static int DeleteLoc(int LocID, int UserID)
        {
            string strExecution = "[admin].[sp_ManageLocation]";
            int ret = 0;
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@pk_LocID", LocID);
                oDq.AddVarcharParam("@Mode", 1, "D");
                oDq.AddBigIntegerParam("@UserID", UserID);
                oDq.AddIntegerParam("@RESULT", 0, QueryParameterDirection.Output);
                ret = Convert.ToInt32(oDq.GetScalar());
            }
            return ret;
        }

        public static int ChkLocDelPossible(int LocID)
        {
            string strExecution = "[admin].[sp_ManageLocation]";
            int Result = 0;

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddBigIntegerParam("@pk_LocID", LocID);
                oDq.AddVarcharParam("@Mode", 1, "C");
                oDq.AddIntegerParam("@RESULT", Result, QueryParameterDirection.Output);
                oDq.RunActionQuery();
                return Convert.ToInt32(oDq.GetParaValue("@RESULT"));
            }
        }

        public static LocationEntity GetLocData(int LocID)
        {
            string strExecution = "[admin].[sp_ManageLocation]";
            LocationEntity Loc = null;

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@Mode", 1, "M");
                oDq.AddIntegerParam("@pk_LocID", LocID);
                oDq.AddIntegerParam("@Result", 0);
                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    Loc = new LocationEntity(reader);
                }
                reader.Close();
            }
            return Loc;
        }

        //+AR 07/06/2017
        public static void SaveAllLocation(DataTable dt, int LocId, string Mode)
        {
            string strExecution = "[admin].[sp_UpdateLocationFromWeb]";
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@DefaultLoc", LocId);
                oDq.AddVarcharParam("@Mode", 1, Mode);
                oDq.AddTableValueParam("@MParam", dt);
                oDq.RunActionQuery();
            }
        }

        public static void SaveAllUsers(DataTable dt, string Mode, int LocID)
        {
            string strExecution = "[admin].[sp_UpdateUserFromWeb]";
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@Mode", 1, Mode);
                oDq.AddTableValueParam("@MParam", dt);
                oDq.AddIntegerParam("@LocID", LocID);
                oDq.RunActionQuery();
            }
        }

        public static void SaveCompCalcMethod(string CalcMethod)
        {
            string strExecution = "[admin].[sp_UpdateCalcMethodFromWeb]";
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@CalcMethod", 1, CalcMethod);
                oDq.RunActionQuery();
            }
        }

    }
}
