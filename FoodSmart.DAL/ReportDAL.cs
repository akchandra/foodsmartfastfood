﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using FoodSmart.DAL.DbManager;
using FoodSmart.Entity;

namespace FoodSmart.DAL
{
    public class ReportDAL
    {
        public DataSet GetBillRefundRegister(DateTime StartDate, DateTime EndDate, int LocationId, string BillType)
        {
            DataSet ds = new DataSet();
            string strExecution = "sp_rpt_SALESREGISTER";

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddDateTimeParam("@StartDate", StartDate);
                oDq.AddDateTimeParam("@EndDate", EndDate);
                oDq.AddIntegerParam("@LocID", LocationId);
                oDq.AddVarcharParam("@BillType", 20, BillType);
                //oDq.AddIntegerParam("@fk_RestID", string.IsNullOrEmpty(Restaurant) ? 0 : Convert.ToInt32(Restaurant));
                ds = oDq.GetTables();
            }
            return ds;
        }
        public DataSet GetBillwiseConsumption(DateTime StartDate, DateTime EndDate, int LocationId, string BillType, string Restaurant)
        {
            DataSet ds = new DataSet();
            string strExecution = "sp_rpt_BillwiseConsumption";

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddDateTimeParam("@StartDate", StartDate);
                oDq.AddDateTimeParam("@EndDate", EndDate);
                oDq.AddIntegerParam("@LocID", LocationId);
                oDq.AddVarcharParam("@BillType", 20, BillType);
                oDq.AddIntegerParam("@fk_RestID", string.IsNullOrEmpty(Restaurant) ? 0 : Convert.ToInt32(Restaurant));
                ds = oDq.GetTables();
            }
            return ds;
        }
        public DataSet GroupItemWiseSalesRegister(DateTime StartDate, DateTime EndDate, int LocationId, string Restaurant)
        {
            DataSet ds = new DataSet();
            string strExecution = "sp_rpt_ITEMWISESALE";

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddDateTimeParam("@StartDate", StartDate);
                oDq.AddDateTimeParam("@EndDate", EndDate);
                oDq.AddIntegerParam("@LocID", LocationId);
                oDq.AddIntegerParam("@RestID", string.IsNullOrEmpty(Restaurant) ? 0 : Convert.ToInt32(Restaurant));
                ds = oDq.GetTables();
            }
            return ds;
        }
        public DataSet GetDaySummary(DateTime StartDate, DateTime EndDate, int LocationId)
        {
            DataSet ds = new DataSet();
            string strExecution = "sp_rpt_DaySummary";

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddDateTimeParam("@StartDay", StartDate);
                oDq.AddDateTimeParam("@EndDay", EndDate);
                oDq.AddIntegerParam("@LocID", LocationId);
                ds = oDq.GetTables();
            }
            return ds;
        }
        public DataSet GetCashierDaySummary(DateTime StartDate, DateTime EndDate, int LocationId)
        {
            DataSet ds = new DataSet();
            string strExecution = "sp_rpt_CashierDaySummary";

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddDateTimeParam("@StartDay", StartDate);
                oDq.AddDateTimeParam("@EndDay", EndDate);
                oDq.AddIntegerParam("@LocID", LocationId);
                ds = oDq.GetTables();
            }
            return ds;
        }

        public DataSet GetCardSummary(DateTime StartDate, DateTime EndDate, int LocationId)
        {
            DataSet ds = new DataSet();
            string strExecution = "sp_rpt_CardSummary";

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                //oDq.AddDateTimeParam("@StartDate", StartDate);
                //oDq.AddDateTimeParam("@EndDate", EndDate);
                //oDq.AddIntegerParam("@LocID", LocationId); 
                ds = oDq.GetTables();
            }
            return ds;
        }

        public DataSet GetLocation(int Id = 0, string LocType = "")
        {
            DataSet ds = new DataSet();
            string strExecution = "proc_Get_Location";

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@Location", Id);
                oDq.AddVarcharParam("@Loctype", 1, LocType);
                ds = oDq.GetTables();
            }
            return ds;
        }

        public DataSet GetOutletLocation(int Id = 0)
        {
            DataSet ds = new DataSet();
            string strExecution = "proc_Get_OutletLocation";

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@Location", Id);
                ds = oDq.GetTables();
            }
            return ds;
        }
        public DataSet GetRestaurant(int Id = 0, int LocId = 0)
        {
            DataSet ds = new DataSet();
            string strExecution = "proc_Get_Restaurants";

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@RestID", Id);
                oDq.AddIntegerParam("@LocId", LocId);
                ds = oDq.GetTables();
            }
            return ds;
        }

        public static List<BillRefundRegEntity> GetBillRefundRegister(ReportCriteria criteria)
        {
            DataSet ds = new DataSet();
            string strExecution = "sp_rpt_SALESREGISTER";
            List<BillRefundRegEntity> lstItems = new List<BillRefundRegEntity>();
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddDateTimeParam("@StartDate", criteria.FromDate);
                oDq.AddDateTimeParam("@EndDate", criteria.ToDate);
                oDq.AddIntegerParam("@LocID", criteria.LocId);
                oDq.AddVarcharParam("@BillType", 20, criteria.TransactionType);
                //oDq.AddIntegerParam("@fk_RestID", string.IsNullOrEmpty(Restaurant) ? 0 : Convert.ToInt32(Restaurant));
                //ds = oDq.GetTables();
                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    var transaction = new BillRefundRegEntity(reader);
                    lstItems.Add(transaction);
                }
                reader.Close();
            }
            return lstItems;
        }

        public static List<TransactionRegisterEntity> GetTransactionRegister(DateTime fromDate, DateTime toDate, string itemType, string tranType, Int32 groupId, int finYr, int locId, int itemId)
        {
            List<TransactionRegisterEntity> lstItems = new List<TransactionRegisterEntity>();
            string strExecution = "[dbo].[sp_rpt_STOCKLedger]";

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@TranType", 1, tranType);
                oDq.AddIntegerParam("@FinYr", finYr);
                oDq.AddIntegerParam("@LocID", locId);
                oDq.AddVarcharParam("@ItemType", 1, itemType);
                oDq.AddIntegerParam("@ItemGroupID", groupId);
                oDq.AddDateTimeParam("@StartDate", fromDate);
                oDq.AddDateTimeParam("@EndDate", toDate);
                oDq.AddIntegerParam("@ItemId", itemId);
                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    var transaction = new TransactionRegisterEntity(reader);
                    lstItems.Add(transaction);
                }
                reader.Close();
            }

            return lstItems;
        }

        public static List<ItemListEntity> GetItemList(ReportCriteria criteria)
        {
            List<ItemListEntity> lstItems = new List<ItemListEntity>();
            string strExecution = "[dbo].[sp_ItemListPrint]";

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddVarcharParam("@ItemType", 1, criteria.TransactionType);
                oDq.AddIntegerParam("@ItemGroupID", criteria.ItemGroupID);
                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    var item = new ItemListEntity(reader);
                    lstItems.Add(item);
                }
                reader.Close();
            }

            return lstItems;
        }

        public static List<StockDetailEntity> GetStockDetail(DateTime fromDate, DateTime toDate, List<int> groups, List<Int64> items)
        {
            List<StockDetailEntity> lstItems = new List<StockDetailEntity>();
            string strExecution = "[dbo].[sp_rpt_STOCKREGISTER]";

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@FinYr", 1);
                oDq.AddIntegerParam("@LocID", 1);
                //oDq.AddVarcharParam("@ItemType", 1, type);
                //oDq.AddDateTimeParam("@StartDate", );
                //oDq.AddDateTimeParam("@EndDate", );
                oDq.AddVarcharParam("@ItemCode", 1000, "");

                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    var transaction = new StockDetailEntity(reader);
                    lstItems.Add(transaction);
                }
                reader.Close();
            }

            return lstItems;
        }

        public static List<StockSummaryEntity> GetStockSummary(DateTime asOnDate, List<int> groups, List<Int64> items)
        {
            List<StockSummaryEntity> lstItems = new List<StockSummaryEntity>();
            string strExecution = "[dbo].[sp_rpt_STOCKLedger";

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddIntegerParam("@FinYr", 1);
                oDq.AddIntegerParam("@LocID", 1);
                //oDq.AddVarcharParam("@ItemType", 1, type);
                //oDq.AddDateTimeParam("@StartDate", );
                //oDq.AddDateTimeParam("@EndDate", );
                oDq.AddVarcharParam("@ItemCode", 1000, "");

                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    var transaction = new StockSummaryEntity(reader);
                    lstItems.Add(transaction);
                }
                reader.Close();
            }

            return lstItems;
        }

        public DataSet Getcompany()
        {
            DataSet ds = new DataSet();
            string strExecution = "[admin].[sp_GetCompanyDetail]";

            using (DbQuery oDq = new DbQuery(strExecution))
            {
                ds = oDq.GetTables();
            }
            return ds;
        }

        public static List<CounterSummaryEntity> GetDaySummaryGST(ReportCriteria criteria)
        {

            DataSet ds = new DataSet();
            string strExecution = "sp_rpt_DaySummaryGST";
            List<CounterSummaryEntity> lstItems = new List<CounterSummaryEntity>();
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddDateTimeParam("@StartDate", criteria.FromDate);
                oDq.AddDateTimeParam("@EndDate", criteria.ToDate);
                oDq.AddIntegerParam("@LocID", criteria.LocId);
                //oDq.AddVarcharParam("@BillType", 20, criteria.TransactionType);
                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    var transaction = new CounterSummaryEntity(reader);
                    lstItems.Add(transaction);
                }
                reader.Close();
            }
            return lstItems;
        }

        public static List<GroupWiseSaleEntity> GetItemGroupSummaryGST(ReportCriteria criteria)
        {

            DataSet ds = new DataSet();
            string strExecution = "sp_rpt_ITEMWISESALEGST";
            List<GroupWiseSaleEntity> lstItems = new List<GroupWiseSaleEntity>();
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddDateTimeParam("@StartDate", criteria.FromDate);
                oDq.AddDateTimeParam("@EndDate", criteria.ToDate);
                oDq.AddIntegerParam("@LocID", criteria.LocId);
                //oDq.AddVarcharParam("@BillType", 20, criteria.TransactionType);
                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    var transaction = new GroupWiseSaleEntity(reader);
                    lstItems.Add(transaction);
                }
                reader.Close();
            }
            return lstItems;
        }

        public static List<BillWiseSaleEntity> GetBillWiseConsumption(ReportCriteria criteria)
        {
            DataSet ds = new DataSet();
            string strExecution = "[dbo].[sp_rpt_BillWiseSale]";
            List<BillWiseSaleEntity> lstItems = new List<BillWiseSaleEntity>();
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddDateTimeParam("@StartDate", criteria.FromDate);
                oDq.AddDateTimeParam("@EndDate", criteria.ToDate);
                oDq.AddIntegerParam("@LocID", criteria.LocId);
                oDq.AddVarcharParam("@BillType", 20, criteria.TransactionType);
                //oDq.AddIntegerParam("@fk_RestID", string.IsNullOrEmpty(Restaurant) ? 0 : Convert.ToInt32(Restaurant));
                //ds = oDq.GetTables();
                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    var transaction = new BillWiseSaleEntity(reader);
                    lstItems.Add(transaction);
                }
                reader.Close();
            }
            return lstItems;
        }

        public static List<CashierSummaryEntity> GetCashierSummary(ReportCriteria criteria)
        {
            DataSet ds = new DataSet();
            string strExecution = "[dbo].[sp_rptCashierSummaryGST]";
            List<CashierSummaryEntity> lstItems = new List<CashierSummaryEntity>();
            using (DbQuery oDq = new DbQuery(strExecution))
            {
                oDq.AddDateTimeParam("@StartDate", criteria.FromDate);
                oDq.AddDateTimeParam("@EndDate", criteria.ToDate);
                oDq.AddIntegerParam("@LocID", criteria.LocId);
                oDq.AddVarcharParam("@BillType", 20, criteria.TransactionType);
                //oDq.AddIntegerParam("@fk_RestID", string.IsNullOrEmpty(Restaurant) ? 0 : Convert.ToInt32(Restaurant));
                //ds = oDq.GetTables();
                DataTableReader reader = oDq.GetTableReader();
                while (reader.Read())
                {
                    var transaction = new CashierSummaryEntity(reader);
                    lstItems.Add(transaction);
                }
                reader.Close();
            }
            return lstItems;
        }

    }
}
